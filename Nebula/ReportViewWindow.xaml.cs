﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Nebula.ViewModels;
using Nebula.Views;

namespace Nebula
{
    /// <summary>
    /// Interaction logic for ReportViewWindow.xaml
    /// </summary>
    public partial class ReportViewWindow : MetroWindow
    {


        MainViewModel vm;

        public ReportViewWindow(MainViewModel PassedViewModel)
        {
            InitializeComponent();

            vm = PassedViewModel;


            this.DataContext = vm;

            //vm.LoadReport = new TestReportFlowDocument();
        }

        private void RepWin1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(vm != null)
            {
                //clear report body and serial
                vm.ReportBody = "";
                vm.SerialNo = "";
            }
           
        }
    }
}
