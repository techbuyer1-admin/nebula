﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class DPCheckPartCodeCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public DPCheckPartCodeCommand(DriveTestViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            return true;


        }

        public void Execute(object parameter)
        {

            //Get array of objects
            var values = (object[])parameter;
            //Split array into specific types

            var lvValue = (ListView)values[0];
            var poDescTXT = (TextBox)values[1];
            var poquantTXT = (TextBox)values[2];

            //Clear Description
            poDescTXT.Text = "";

            //Clear the messages
            //vm.ImportMessage = "";
            //vm.ProcessedMessage = "";
            Console.WriteLine(vm.PartCode);

            if (vm.PartCode.Length < 1)
            {
                //skip partial search
            }
            else
            {


                if (File.Exists(vm.myDocs + @"\Inventory.csv"))
                {


                    //string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv";
                    //Uses a VB class to process the file instead of streamreader
                    using (TextFieldParser parser = new TextFieldParser(vm.myDocs + @"\Inventory.csv"))
                    {
                        parser.TextFieldType = FieldType.Delimited;
                        parser.SetDelimiters(",");

                        while (!parser.EndOfData)
                        {
                            //Processing row
                            string currentLine = parser.ReadLine();
                            //Console.WriteLine(currentLine);

                            //Check if product code already exists in CSV. If not flag part code required as yes
                            if (currentLine != null)
                            {
                                string[] itmValue = currentLine.Split(',');

                                //Product Code
                                if (itmValue[0] != string.Empty)
                                {
                                    //Console.WriteLine(Convert.ToString(itmValue[1]));
                                    if (itmValue[0].Trim() == vm.PartCode.ToUpper().Trim())
                                    {

                                        //Add Part Description
                                        if (itmValue[1] != string.Empty)
                                        {
                                            //MessageBox.Show(vm.PartCode);
                                            vm.PartDescription = Convert.ToString(itmValue[1]);
                                            vm.PartCodeRequired = "No";
                                            //send focus to quantity textbox
                                            //poquantTXT.Focus();
                                            return;
                                        }


                                    }
                                    else
                                    {
                                        //Set focus to description toolbox
                                        //poDescTXT.Focus();
                                        vm.PartCodeRequired = "Yes";

                                    }
                                }

                            }
                        }


                       
                    }
                }




            }




        }
    }
}
