﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Nebula.Paginators;
using Nebula.ViewModels;
using Nebula.Helpers;
using System.Windows.Documents;
using System.Windows.Controls;
using System.IO;
using System.Configuration;
using System.Windows.Media;
using Nebula.Models;
using System.Collections.ObjectModel;

namespace Nebula.Commands
{
    public class PrintSaveFlowDocumentREPCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public PrintSaveFlowDocumentREPCommand(DriveTestViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            var values = (object[])parameter;
            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //if (lv != null)
            //{
            //    if (lv.SelectedItems.Count > 0)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    return true;
            //}
            return true;
        }

        public async void Execute(object parameter)
        {
            var values = (object[])parameter;
            //Split array into specific types
            var flowdoc = values[0];
            var lvValue = (ListView)values[1];




            DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

            //Generate PDF From FlowDocument
            //NEW STRAIGHT THROUGH
            dpw.SaveDocGOTPDF((FlowDocument)flowdoc, vm.myDocs + @"\" + Clipboard.GetText() + @".pdf");

            string PDFReportPath = vm.myDocs + @"\" + Clipboard.GetText() + @".pdf";


            //zip Test Reports and create zip

            if (lvValue.Items.Count != 0)
            {
                //MessageBox.Show("Hmmm");

                //try
                //{
                vm.ProcessedMessageColour = Brushes.MediumPurple;
                vm.ProcessedMessage = "All Reports for current PO are being Archived. Please Wait....";

                vm.ProgressIsActive = true;
                ////Change mouse cursor 
                //Mouse.OverrideCursor = Cursors.Wait;

                //Create Local Folder
                if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                {
                    if (string.IsNullOrEmpty(vm.DBSearch))
                    {
                    }
                    else
                    {
                        //Create local file
                        Directory.CreateDirectory(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports");
                    }

                }



                //Create string array for attachments
                string[] Attachments = new string[] { PDFReportPath };
                //Create outlook object
                EmailClass emailReport = new EmailClass(vm);
                await System.Threading.Tasks.Task.Run(() => emailReport.AttachFilesAndSendMail(Attachments, "Drive Test"));

                //Delete Original Folder?
                //Create Local Folder
                if (Directory.Exists(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports"))
                {
                    //Delete Local Folder
                    Directory.Delete(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports", true);

                }
                

                vm.ProcessedMessageColour = Brushes.Green;


                vm.ProgressIsActive = false;

                //Show message to user
                vm.ProcessedMessage = "Report Attached successfully!";

            }


        }



    }
}

