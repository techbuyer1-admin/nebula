﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data;
using System.Data.OleDb;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;
using Nebula.Views;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Threading;
using System.Windows.Documents;
using System.ComponentModel;
using Nebula.Paginators;
using System.IO.Packaging;
using System.Windows.Xps.Packaging;
using System.Windows.Markup;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Nebula.Commands
{
    public class DPDBCrudCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;


        //SQL Lite DB CONNECTION STRING LINK
        public string cs = "";


        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public DPDBCrudCommand(DriveTestViewModel PassedViewModel)
        {
            this.vm = PassedViewModel;

            //Point DB to Test Databases
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:" + @"C:\Users\n.myers\Desktop\Data\DriveTestSQL.db";
                //cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Backup\United Kingdom\DriveTestSQL.db";
            }
            else
            {

                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/DriveTest/DriveTestSQL.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/DriveTest/DriveTestSQL.db";
                }
                else
                {

                    //Set connection string to Live DB Backup\United Kingdom
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\DriveTestSQL.db";
                }



            }


        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //Get array of objects
            var values = (object[])parameter;





            ////Required if parameter is a control reference
            if (values != null)
            {
                //Split array into specific types
                var request = values[0];
                var lvValue = (ListView)values[1];
                var partcodeTXT = (TextBox)values[2];
                var PItabCtrl = (TabControl)values[3];
                //Clear the messages
                //vm.ImportMessage = "";
                //vm.ProcessedMessage = "";

                //if (lvValue != null) 
                //{
                vm.FocusOnSerialTXT = true;

                //Scan Serials V 
                if (PItabCtrl.SelectedIndex == 0)
                {



                    if (request.ToString() == "SavePartialDB" || request.ToString() == "LoadPartialDB" || request.ToString() == "DeletePartialDB" || request.ToString() == "GetFiles" || request.ToString() == "ImportSerials")
                    {
                        return true;

                    }
                    else if (request.ToString() == "SearchCollection" || (request.ToString() == "SearchImportCollection"))
                    {

                        return true;

                    }
                    else if (vm.POTotal == vm.ProcessedTotal && vm.POTotal != 0 && vm.ProcessedTotal != 0)
                    {

                        return true;

                    }
                    else
                    {

                        if (String.IsNullOrEmpty(vm.ScannedSerial))
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }



                    }
                } //Test Tab
                else if (PItabCtrl.SelectedIndex == 1)
                {

                    if (request.ToString() == "ReadDBTest" || vm.TestedDriveCollection.Count != 0 || (request.ToString() == "DSOIndexQuery"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }



                } //Search Serials SearchCollection 
                else if (PItabCtrl.SelectedIndex == 2)
                {

                    if (request.ToString() == "ReadDB" || request.ToString() == "GenerateFlowDocReport" || request.ToString() == "GenerateCSVReport" || request.ToString() == "GenerateCedarCSVReport" || vm.SearchCollection.Count != 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }

            }
            else
            {



                return false;
            }

        }

        public async void Execute(object parameter)
        {
            try
            {


                //Get array of objects
                var values = (object[])parameter;
                //Split array into specific types
                var request = values[0];
                var lvValue = new ListView();
                var partcodeTXT = new TextBox();
                var PItabCtrl = new TabControl();

                if (values[1] != null)
                {
                    lvValue = (ListView)values[1];
                }

                if (values[2] != null)
                {
                    partcodeTXT = (TextBox)values[2];
                }

                if (values[3] != null)
                {
                    PItabCtrl = (TabControl)values[3];
                }

                //Create stopwatch for timing
                var timer = new Stopwatch();
                timer.Start();
                TimeSpan timeTaken;
                string timetaken;

                ////Get the values from the selected item
                //vm.GetSelectedImportViewItem();


                //Change Mouse Cursor
                Mouse.OverrideCursor = Cursors.Wait;
                vm.ProgressIsActive = true;

                //vm.FocusOnSerialTXT = false;

                //If Listview is empty don't run
                //if(lvValue.Items.Count == 0)
                //{
                vm.ImportMessage = "";
                vm.SerialMessage2 = "";

                switch (request)
                {

                    case "AddSerial":

                        vm.FocusOnSerialTXT = true;
                        vm.FocusOnSerialTXT = false;
                        vm.FocusOnSerialTXT = true;



                        if (vm.ImportLVSelectedItem.SerialsRemaining == 0 || vm.ImportLVSelectedItem.SerialsRemaining < 0)
                        {
                            //Play system sound
                            //  SystemSounds.Question.Play();
                            //  vm.SerialMessage = "No serials remaining";

                            vm.ScannedSerial = "";


                        }
                        else
                        {

                            ////Clear the messages
                            //ImportMessage = "";
                            //CHECK IF A SERIAL ALREADY EXISTS IN COLLECTION
                            if (vm.ScannedSerialCollection.Where(x => x.SerialNumber == vm.ScannedSerial).Any())
                            {
                                //Play system sound
                                //SystemSounds.Beep.Play();
                                // MessageBox.Show("Serial Already Exists");
                                //vm.ImportMessageColour = Brushes.Red;
                                vm.SerialMessage2 = "Serial Number already present in the PO!";

                                //await PutTaskDelay(2000);
                                break;

                            }//Check for hyphen and if length is less than 5, which could indicate a miss scan
                            else if (vm.ScannedSerial.Contains("-") || vm.ScannedSerial.Count() < 5)
                            {
                                if (vm.ScannedSerial.Contains("-"))
                                {
                                    //Play system sound
                                    // SystemSounds.Hand.Play();
                                    vm.SerialMessage2 = "Serial contains a Hyphen, Please correct.";
                                    break;
                                }
                                else if (vm.ScannedSerial.Count() > 0 && vm.ScannedSerial.Count() < 5)
                                {
                                    //Play system sound
                                    //SystemSounds.Hand.Play();
                                    vm.SerialMessage2 = "Serial character count is under 5. Please correct";
                                    break;
                                }
                                else
                                {
                                    vm.SerialMessage2 = "";
                                }

                            }
                            else
                            {
                                if (vm.ImportLVSelectedItem.Complete != "Done")
                                {
                                    if (vm.ScannedSerial != "")
                                    {
                                        //Delay to help with scan
                                        // await PutTaskDelay(300);

                                        //if (vm.SerialsCount > 0)
                                        //{

                                        // Add Serial once delay is finished
                                        NetsuiteInventory ns = new NetsuiteInventory();
                                        ns.ProcessedDate = DateTime.Now;
                                        ns.PONumber = vm.PONumber;

                                        if (vm.ImportLVSelectedItem.LineType != null)
                                        {
                                            if (vm.ImportLVSelectedItem.LineType.Contains("(V)") || vm.ImportLVSelectedItem.LineType.Contains("(E)"))
                                            {
                                                ns.LineType = vm.ImportLVSelectedItem.LineType;
                                            }
                                            else
                                            {
                                                ns.LineType = "";
                                            }
                                        }
                                        else
                                        {
                                            ns.LineType = "";
                                        }

                                        ns.ReceivedByOperative = vm.CurrentUserInitials;
                                        ns.ProductCode = vm.ImportLVSelectedItem.ProductCode;
                                        ns.ProductDescription = vm.ImportLVSelectedItem.ProductDescription;
                                        ns.Quantity = "1";
                                        ns.POQuantity = vm.ImportLVSelectedItem.Quantity;
                                        ns.SerialNumber = vm.ScannedSerial;
                                        ns.POLineUnitPrice = vm.ImportLVSelectedItem.POLineUnitPrice;
                                        ns.POUnitAmount = vm.ImportLVSelectedItem.POUnitAmount;
                                        ns.NewPartCodeRequired = vm.ImportLVSelectedItem.NewPartCodeRequired;
                                        ns.ReportPath = "";
                                        ns.TestedByOperative = "";
                                        ns.DriveFailedReason = "";
                                        ns.DriveFailedReasonSpecify = "";
                                        ns.DriveResult = "PENDING";
                                        ns.WipeSoftware = "";
                                        ns.ITADRef = "";

                                        //Check if part code required   
                                        if (vm.ImportLVSelectedItem.PartCodeRequired == "true")
                                        {
                                            ns.PartCodeRequired = "true";
                                            //PartCodesTotal += 1;

                                        }
                                        else
                                        {
                                            ns.PartCodeRequired = "false";
                                        }

                                        vm.ScannedSerialCollection.Add(ns);


                                        //NETSUITE SCANNED COLLECTION
                                        //Add count items to match import screen 

                                        // Check Totals before discrepancy check


                                        //Add to live comparison list to cross check totals
                                        // await System.Threading.Tasks.Task.Run(() => AddToComparisonList());

                                        int ScannedItems = 0;

                                        //Clear Collection need to use dispatcher thread
                                        //App.Current.Dispatcher.Invoke((System.Action)delegate
                                        //{
                                        vm.NetsuiteScannedCollection.Clear();
                                        //});


                                        //var uiContext = SynchronizationContext.Current;

                                        foreach (NetsuiteInventory itm in vm.ScannedSerialCollection)
                                        {


                                            //Count Quantity in scanned collection 
                                            ScannedItems = vm.ScannedSerialCollection.Where(item => item.ProductCode == itm.ProductCode).Count();


                                            //Check if product code already exists, create new object to add to the comparison lines
                                            if (!vm.NetsuiteScannedCollection.Any(p => p.ProductCode == itm.ProductCode))
                                            {

                                                NetsuiteInventory nsi = new NetsuiteInventory();
                                                nsi.ProductCode = itm.ProductCode;
                                                nsi.ProductDescription = itm.ProductDescription;
                                                nsi.LineType = itm.LineType;
                                                nsi.NewPartCodeRequired = itm.NewPartCodeRequired;
                                                nsi.Quantity = ScannedItems.ToString();
                                                //Use this if the collection cannot be updated other than the UI Thread
                                                //App.Current.Dispatcher.Invoke((System.Action)delegate
                                                //{
                                                //Add to collection
                                                vm.NetsuiteScannedCollection.Add(nsi);
                                                //});
                                                //vm.NetsuiteScannedCollection.Add(nsi);
                                                //uiContext.Send(x => vm.NetsuiteScannedCollection.Add(nsi), null);

                                            }


                                        }


                                        //END NETSUITE SCANNED COLLECTION


                                        // CLEAR COUNT COLLECTION
                                        vm.SerialsTotalsCollection.Clear();



                                        // var query = ScannedSerialCollection.Where(x => x.ProductCode = )


                                        vm.ScannedSerial = "";
                                        //Reduce Couter For Current Item
                                        // vm.SerialsCount -= 1;

                                        //ZERO COUNT
                                        int itemCount = 0;

                                        itemCount = vm.ScannedSerialCollection.Where(x => x.ProductCode == vm.ImportLVSelectedItem.ProductCode).Count();

                                        //MessageBox.Show(vm.ScannedSerialCollection.Where(x => x.ProductCode == vm.ImportLVSelectedItem.ProductCode).Count().ToString());

                                        //    //// LOOP THROUGH CURRENT ITEMS AND COUNT INDIVIDUAL LINES
                                        //    foreach (NetsuiteInventory itm in vm.ScannedSerialCollection.Where(x => x.ProductCode == vm.ImportLVSelectedItem.ProductCode))
                                        //    {
                                        //        itemCount += 1;
                                        //        //Console.WriteLine(itm.ProductCode + " " + itemCount);

                                        //    }

                                        ////Set the Serials Remaing Property
                                        //vm.ImportLVSelectedItem.SerialsRemaining -= 1;
                                        vm.ImportLVSelectedItem.SerialsRemaining = Convert.ToInt32(vm.ImportLVSelectedItem.Quantity) - itemCount;



                                        if (vm.ImportLVSelectedItem.SerialsRemaining == 0)
                                        {
                                            // vm.DPVariousCompleteCommand.Execute("Close");
                                            vm.ShowPopup = false;

                                            //Set Complete Status if all serials are scannned
                                            vm.ImportLVSelectedItem.Complete = "Done";


                                            // lvValue.Items.
                                        }
                                        //} //serials count


                                        if (vm.POTotal == vm.ProcessedTotal)
                                        {
                                            vm.ImportMessageColour = Brushes.Green;
                                            vm.ImportMessage = "Serial Scan is complete! Totals match correctly.";
                                        }
                                        //else if(vm.POTotal != vm.ProcessedTotal)
                                        //{
                                        //    vm.ImportMessageColour = Brushes.Red;
                                        //    vm.ImportMessage = "Serial Scan is complete but the Totals do not match. Please check for missing items?";
                                        //}


                                        //Play system sound
                                        SystemSounds.Hand.Play();

                                        //}

                                    }
                                    else
                                    {
                                        //Item is 0


                                        //  vm.DPVariousCompleteCommand.Execute("Close");
                                    }


                                }

                            }
                            //Send focus to scanned textbox
                            vm.FocusOnSerialTXT = true;


                        }


                        //Save Change to State

                        vm.DPDBCrudCommand.Execute(new object[] { "SavePartialDB", new ListView(), new TextBox(), new TabControl() });


                        break;

                    case "AddDB":

                        //DUPLICATE SERIAL CHECK
                        //Check if serial already present
                        //Check if serial already present
                        vm.SerialMessage2 = "";
                        // NetsuiteInventory sItem = new NetsuiteInventory();
                        if (vm.ScannedSerialCollection.Count > 0)
                        {
                            var Repeteditemlist = new List<string>();


                            List<String> duplicates = vm.ScannedSerialCollection.GroupBy(x => x.SerialNumber).Where(g => g.Count() > 1)
                                .Select(g => g.Key)
                                .ToList();

                            foreach (var item in duplicates)
                            {
                                // Console.WriteLine(item);

                                vm.SerialMessage2 = "Duplicate Serial " + item + " Found!";
                                Mouse.OverrideCursor = Cursors.Arrow;
                                return;
                            }
                        }





                        if (vm.HasDiscrepancyRun == true)
                        {


                            if (vm.POTotal > 0 && vm.POTotal == vm.ProcessedTotal)
                            {
                                //Reset the part code required total
                                vm.PartCodesTotal = 0;
                                //Check for filelock on DB

                                if (StaticFunctions.IsLocked(new FileInfo(@"" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\DriveTestSQL.db")))
                                {
                                    vm.ProcessedMessageColour = Brushes.Red;
                                    vm.ProcessedMessage = "DB File is locked. Please try saving again.";
                                }
                                else
                                {
                                    //CreateTable(sqlite_conn);
                                    //insert all items in table to DB
                                    //OLD METHOD
                                    //InsertData();
                                    //NEW TRANSACTION METHOD
                                    InsertTransactionData();


                                    vm.NetsuiteComparisonCollection.Clear();
                                    //Add to visible collection
                                    vm.NetsuiteImportCollection.Clear();
                                    //Add to visible collection
                                    vm.NetsuiteScannedCollection.Clear();
                                    //Add to visible collection
                                    vm.ScannedSerialCollection.Clear();
                                    //Reset Properties

                                    vm.ImportMessage = "";
                                    vm.POImportTotal = 0;
                                    vm.POTotal = 0;
                                    vm.ProcessedTotal = 0;

                                    vm.ImportLVSelectedItem = null;
                                    vm.ProcessedMessageColour = Brushes.Green;
                                    vm.ProcessedMessage = "Items saved sucessfully to the DB!";

                                    //Delete Partial PO as saved to DB

                                    vm.DPDBCrudCommand.Execute(new object[] { "DeletePartialDBDuringAdd", new ListView(), new TextBox(), new TabControl() });

                                    vm.PONumber = "";




                                }

                                //Reset Discrepancy Run Status
                                vm.HasDiscrepancyRun = false;
                            }
                            else
                            {
                                vm.ProcessedMessageColour = Brushes.Red;
                                vm.ProcessedMessage = "Serial Totals do not Match, Unable to Save!";
                            }
                        }
                        else
                        {
                            vm.ProcessedMessageColour = Brushes.Red;
                            vm.ProcessedMessage = "Unable to Save until Discrepancy Report Has been produced";
                        }


                        //Clear Discrepancy Message
                        vm.DiscrepancyMessage = "";



                        break;
                    case "ReadDB":
                        //Set Message
                        //vm.ProcessedMessage = "Please Wait...";
                        vm.ProcessedMessageColour = Brushes.Green;
                        //CLEAR THE COLLECTION
                        vm.SearchCollection.Clear();


                        //await PutTaskDelay(2000);

                        //Zero Counters
                        vm.SearchCount = "0";
                        vm.PassCount = "0";
                        vm.FailCount = "0";
                        vm.PendingCount = "0";


                        //Call Filter Async
                        if (vm.MatchExactly == true)
                        {


                            //Check if empty search & don't call to upper fucnction
                            if (string.IsNullOrEmpty(vm.DBSearch))
                            {
                                //await FilterSearchMatch(string.Empty, "");
                                vm.ProcessedMessage = "Please Enter Some Search Text!";
                                vm.ProcessedMessageColour = Brushes.Red;

                            }
                            else
                            {

                                await FilterSearchMatch(vm.DBSearch.ToUpper(), "");

                            }

                        }
                        else
                        {

                            //Check if empty search & don't call to upper fucnction
                            if (string.IsNullOrEmpty(vm.DBSearch))
                            {
                                //Search
                                //await FilterSearch(string.Empty, "");
                                vm.ProcessedMessage = "Please Enter Some Search Text!";
                                vm.ProcessedMessageColour = Brushes.Red;
                            }
                            else
                            {
                                //Search
                                await FilterSearch(vm.DBSearch.ToUpper(), "");

                            }


                        }

                        //vm.ProcessedMessage = "";

                        break;

                    case "ReadDBTest":

                        try
                        {

                            vm.TestCount = "0";
                            vm.PassCount = "0";
                            vm.FailCount = "0";
                            vm.PendingCount = "0";
                            vm.PreviousSerialNumber = "";


                            if (vm.DBSearch == string.Empty)
                            {

                                vm.TestCount = "0";
                                vm.PassCount = "0";
                                vm.FailCount = "0";
                                vm.PendingCount = "0";
                                //CLEAR THE COLLECTION
                                vm.TestedDriveCollection.Clear();
                            }
                            else
                            {



                                //Call Filter Async wONT wORK DUE TO BEING ON A DIFFERENT THREAD
                                // await System.Threading.Tasks.Task.Run(() => FilterSearchTest(vm.DBSearch.ToUpper(), ""));
                                if (string.IsNullOrEmpty(vm.DBSearch))
                                {
                                    //MessageBox.Show("Please Enter a PO Number");
                                    vm.ProcessedMessage = "Please Enter a PO Number";
                                    vm.ProcessedMessageColour = Brushes.Green;
                                }
                                else
                                {
                                    vm.ProcessedMessage = "";
                                    await FilterSearchTest(vm.DBSearch.ToUpper(), "");
                                }



                            }
                        }
                        catch (Exception ex)
                        {

                            MessageBox.Show(ex.Message);
                        }

                        //MessageBox.Show(vm.FailCount);
                        //FilterSearch(vm.DBSearch, "PONumber");
                        //vm.ProgressIsActive = false;
                        break;

                    case "SearchCollection":

                        Mouse.OverrideCursor = Cursors.Wait;
                        // vm.TestCount = "0";
                        //Empty Message
                        vm.ProcessedMessage = "";

                        if (vm.SerialSearch == string.Empty)
                        {


                        }
                        else
                        {

                            //Find object matching search
                            NetsuiteInventory nsi = vm.ScannedSerialCollection.ToList().Find(p => p.SerialNumber == vm.SerialSearch);

                            if (nsi != null)
                            {
                                //Get Index 
                                int result = vm.ScannedSerialCollection.IndexOf(nsi);

                                vm.ProcessedLVSelectedItem = nsi;

                                //Scroll to Found Item
                                //lvValue.Items.MoveCurrentToLast();
                                lvValue.ScrollIntoView(lvValue.Items.CurrentItem);

                            }
                            else
                            {
                                vm.ProcessedMessageColour = Brushes.Red;
                                vm.ProcessedMessage = "Serial Not Found";
                            }



                        }

                        Mouse.OverrideCursor = null;

                        //FilterSearch(vm.DBSearch, "PONumber");
                        //vm.ProgressIsActive = false;
                        break;

                    case "SearchImportCollection":



                        // vm.TestCount = "0";
                        //Empty Message
                        vm.ImportMessage = "";

                        if (vm.ProductSearch == string.Empty)
                        {


                        }
                        else
                        {

                            //Find object matching search
                            NetsuiteInventory nsi = vm.NetsuiteImportCollection.ToList().Find(p => p.ProductCode == vm.ProductSearch);

                            if (nsi != null)
                            {
                                //Get Index 
                                int result = vm.NetsuiteImportCollection.IndexOf(nsi);

                                vm.ImportLVSelectedItem = nsi;

                                //Scroll to Found Item
                                //lvValue.Items.MoveCurrentToLast();
                                lvValue.ScrollIntoView(lvValue.Items.CurrentItem);

                            }
                            else
                            {
                                vm.ImportMessageColour = Brushes.Red;
                                vm.ImportMessage = "Serial Not Found";
                            }



                        }


                        break;

                    case "ImportOverwite2Database":
                        Mouse.OverrideCursor = Cursors.Wait;

                        //Check if PO number is entered & the count is greater than zero
                        if (!string.IsNullOrEmpty(vm.DBSearch.Trim()) && vm.TestedDriveCollection.Count > 0)
                        {
                            //Create holding collection
                            ObservableCollection<NetsuiteInventory> CurrentPO = new ObservableCollection<NetsuiteInventory>();

                            List<RedfishKeyValuePair> productList = new List<RedfishKeyValuePair>();

                            //await Task.Run(() => 
                            productList = await Task.Run(() => ReturnProductDescription(""));



                            using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                            {
                                ////Clear Existing Collection
                                //vm.NetsuiteImportCollection.Clear();

                                //Requires GUID for MyComputer Or ThisPC Folder
                                openFileDialog.InitialDirectory = vm.downld; //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                                openFileDialog.Filter = "CSV files (*.csv)|*.csv";
                                openFileDialog.FilterIndex = 0;
                                openFileDialog.Multiselect = true;
                                openFileDialog.RestoreDirectory = true;

                                // string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");

                                //MessageBox.Show(rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime);




                                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                                {
                                    foreach (var file in openFileDialog.FileNames)
                                    {

                                        // MessageBox.Show(file);
                                        if (File.Exists(file))
                                        {

                                            ////Uses a VB class to process the file instead of streamreader
                                            using (TextFieldParser CSVParser = new TextFieldParser(file))
                                            {

                                                CSVParser.SetDelimiters(",");


                                                //CSVParser.TextFieldType = FieldType.Delimited;
                                                //CSVParser.SetDelimiters(",");
                                                while (!CSVParser.EndOfData)
                                                {

                                                    string[] columnField = CSVParser.ReadFields();

                                                    //MessageBox.Show(columnField[0]);
                                                    //MessageBox.Show(columnField[1]);
                                                    NetsuiteInventory itm = new NetsuiteInventory();

                                                    itm.ProcessedDate = DateTime.Now;
                                                    itm.PONumber = vm.DBSearch.Trim();
                                                    itm.LineType = "";
                                                    itm.SerialNumber = columnField[0];
                                                    itm.ProductCode = columnField[1];
                                                    if (string.IsNullOrEmpty(productList.Where(x => x.Key == columnField[1]).FirstOrDefault()?.Value))
                                                    {
                                                        itm.ProductDescription = "No Description Available";
                                                    }
                                                    else
                                                    {
                                                        itm.ProductDescription = productList.Where(x => x.Key == columnField[1]).FirstOrDefault()?.Value;
                                                    }
                                                    itm.Quantity = "1";
                                                    itm.POLineUnitPrice = "";
                                                    itm.POUnitAmount = "";
                                                    itm.ReceivedByOperative = vm.CurrentUserInitials;
                                                    itm.TestedByOperative = "";
                                                    itm.WipeSoftware = "";
                                                    itm.DriveResult = "PENDING";
                                                    itm.DriveFailedReason = "";
                                                    itm.DriveFailedReasonSpecify = "";
                                                    itm.PartCodeRequired = "";
                                                    itm.ReportPath = "";
                                                    itm.ITADRef = "";
                                                    //Add to local collection
                                                    CurrentPO.Add(itm);

                                                }
                                            }

                                        }


                                    }

                                    //If user selected ok run the rest

                                    //Run Importer
                                    await Task.Run(() => ImportReplacePO(vm.DBSearch.Trim(), CurrentPO));


                                    //RELOAD
                                    //Set PO Search to TextBox
                                    vm.DBSearch = vm.DBSearch.Trim();
                                    //Reload new PO Entries
                                    vm.DPDBCrudCommand.Execute(new object[] { "ReadDBTest", new ListView(), new TextBox(), new TabControl() });
                                    //Set message
                                    vm.ProcessedMessageColour = Brushes.Green;
                                    vm.ProcessedMessage = "Imported Successfully!";
                                }




                            }






                        }




                        Mouse.OverrideCursor = null;

                        break;

                    case "UpdateDB":



                        try
                        {


                            if (StaticFunctions.IsLocked(new FileInfo(@"" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\DriveTestSQL.db")))
                            {
                                vm.ProcessedMessageColour = Brushes.Red;
                                vm.ProcessedMessage = "DB File is locked. Please try saving again.";
                            }
                            else
                            {
                                if (vm.TestResultsLVSelectedItem != null && vm.TestedDriveCollection.Count != 0)
                                {


                                    //if(vm.MultiSelect == true)
                                    //{


                                    //Change multiple fail statuses

                                    if (lvValue.SelectedItems.Count > 1)
                                    {

                                        //LOOP THROUGH MULTIPLE SELECTED ITEMS
                                        foreach (NetsuiteInventory nsi in lvValue.SelectedItems)
                                        {
                                            //insert all items in table to DB
                                            MultiUpdateData(nsi);

                                            //Console.WriteLine(nsi.SerialNumber);

                                        }
                                    }
                                    else
                                    {

                                        //Single line update
                                        UpdateData();

                                    }




                                    int sel = lvValue.SelectedIndex;

                                    await FilterSearchTest(vm.DBSearch, "");

                                    //Wait for refresh
                                    await PutTaskDelay(200);

                                    lvValue.SelectedIndex = sel;
                                    //Switch Back To Single Selection Mode
                                    //vm.MultiSelect = false;
                                    //vm.SelectMode = SelectionMode.Single;

                                    vm.ProcessedMessageColour = Brushes.Green;
                                    vm.ProcessedMessage = "Query Finished!";


                                }


                            }
                        }
                        catch (Exception ex)
                        {
                            vm.ProcessedMessageColour = Brushes.Red;
                            vm.ProcessedMessage = "Changes failed to save! " + "Reason:" + ex.Message;

                        }



                        break;
                    case "ReadTestDB":
                        int ItemIndex;
                        ItemIndex = lvValue.SelectedIndex;


                        vm.TestCount = "0";
                        vm.PassCount = "0";
                        vm.FailCount = "0";
                        vm.PendingCount = "0";
                        ////Enable progress Activity
                        //vm.ProgressIsActive = true;
                        //MessageBox.Show(ItemIndex.ToString());
                        if (ItemIndex != -1)
                        {
                            NetsuiteInventory RecordRow = lvValue.Items.GetItemAt(ItemIndex) as NetsuiteInventory;
                            vm.TestResultsLVSelectedItem = RecordRow;
                        }

                        vm.TestCount = lvValue.Items.Count.ToString();
                        vm.PassCount = vm.TestedDriveCollection.Where(f => f.DriveResult == "PASS").Count().ToString();
                        vm.FailCount = vm.TestedDriveCollection.Where(f => f.DriveResult == "FAIL").Count().ToString();
                        vm.PendingCount = vm.TestedDriveCollection.Where(f => f.DriveResult == "PENDING").Count().ToString();
                        ////Disable Activity
                        //vm.ProgressIsActive = false;



                        break;
                    case "DeletePartialDB":

                        Mouse.OverrideCursor = Cursors.Wait;

                        //"Are you sure you want to delete this Partial PO?"
                        System.Windows.Forms.DialogResult dialogResult = System.Windows.Forms.MessageBox.Show(App.Current.FindResource("DeletePartialPO").ToString(), "Delete Partial PO", System.Windows.Forms.MessageBoxButtons.YesNo);
                        if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                        {
                            if (string.IsNullOrEmpty(vm.PONumber))
                            {

                                vm.ProcessedMessage = "Invalid, no PO Number found!";
                                vm.ProcessedMessageColour = Brushes.Red;
                            }
                            else
                            {


                                if (Directory.Exists(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + ""))
                                {
                                    Directory.Delete(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + "", true);


                                    //Clear All Collections
                                    //Add to Comparison collection for original imported state
                                    vm.NetsuiteComparisonCollection.Clear();
                                    //Add to visible collection
                                    vm.NetsuiteImportCollection.Clear();
                                    //Add to visible collection
                                    vm.NetsuiteScannedCollection.Clear();
                                    //Add to visible collection
                                    vm.ScannedSerialCollection.Clear();
                                    vm.POImportTotal = 0;
                                    vm.POTotal = 0;
                                    vm.ProcessedTotal = 0;
                                    vm.PartCodeRequired = "0";

                                    vm.ProcessedMessage = "Partial " + vm.PONumber + " Deleted Successfully. ";
                                    vm.ProcessedMessageColour = Brushes.Green;

                                }
                            }

                        }
                        else if (dialogResult == System.Windows.Forms.DialogResult.No)
                        {

                        }



                        Mouse.OverrideCursor = null;


                        break;

                    case "DeletePartialDBDuringAdd":

                        Mouse.OverrideCursor = Cursors.Wait;
                        if (string.IsNullOrEmpty(vm.PONumber))
                        {

                            vm.ProcessedMessage = "Invalid, no PO Number found!";
                            vm.ProcessedMessageColour = Brushes.Red;
                        }
                        else
                        {


                            if (Directory.Exists(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + ""))
                            {
                                Directory.Delete(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + "", true);


                                //Clear All Collections
                                //Add to Comparison collection for original imported state
                                vm.NetsuiteComparisonCollection.Clear();
                                //Add to visible collection
                                vm.NetsuiteImportCollection.Clear();
                                //Add to visible collection
                                vm.NetsuiteScannedCollection.Clear();
                                //Add to visible collection
                                vm.ScannedSerialCollection.Clear();
                                vm.POImportTotal = 0;
                                vm.POTotal = 0;
                                vm.ProcessedTotal = 0;
                                vm.PartCodeRequired = "0";

                                vm.ProcessedMessage = "Partial " + vm.PONumber + " Deleted Successfully. ";
                                vm.ProcessedMessageColour = Brushes.Green;

                            }
                        }
                        Mouse.OverrideCursor = null;


                        break;

                    case "SavePartialDB":

                        vm.ProcessedMessage = "";
                        Mouse.OverrideCursor = Cursors.Wait;

                        if (Directory.Exists(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + ""))
                        {

                        }
                        else
                        {
                            Directory.CreateDirectory(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + "");
                        }


                        //Serializing the collection  

                        //Set original Total on import
                        //vm.NetsuiteComparisonCollection.FirstOrDefault().ImportTotal = vm.TotalOnImport;

                        ////Serialize Items to Binary Files
                        ///
                        Serialize(vm.NetsuiteOriginalImportCollection, @"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + @"\Original.bin");
                        ////Serializing the collection  
                        Serialize(vm.NetsuiteComparisonCollection, @"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + @"\Comparison.bin");


                        ////Serializing the collection  
                        Serialize(vm.NetsuiteImportCollection, @"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + @"\Import.bin");

                        //////Serializing the collection  
                        Serialize(vm.ScannedSerialCollection, @"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + @"\Serials.bin");





                        //////Serializing import Total  
                        // SerializeVM(vm, @"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + @"\ViewModel.bin");


                        if (lvValue.Items.Count == 0)
                        {

                        }
                        else
                        {
                            vm.ProcessedMessage = "" + vm.PONumber + " Partially Saved. Use the Load Partial PO Button to return to this current state.";
                            vm.ProcessedMessageColour = Brushes.Green;
                        }

                        Mouse.OverrideCursor = null;

                        break;
                    case "LoadPartialDB":

                        Mouse.OverrideCursor = Cursors.Wait;

                        CommonOpenFileDialog dialog = new CommonOpenFileDialog();
                        dialog.InitialDirectory = @"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\";
                        dialog.IsFolderPicker = true;
                        if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                        {

                            vm.PONumber = dialog.FileName.Replace(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\", "");



                            Mouse.OverrideCursor = Cursors.Wait;


                            if (Directory.Exists(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + ""))
                            {
                                // vm = DeserializeVM(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + @"\ViewModel.bin");
                                vm.NetsuiteOriginalImportCollection = Deserialize(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + @"\Original.bin");
                                vm.NetsuiteComparisonCollection = Deserialize(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + @"\Comparison.bin");
                                vm.NetsuiteImportCollection = Deserialize(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + @"\Import.bin");
                                vm.ScannedSerialCollection = Deserialize(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber + @"\Serials.bin");

                                //Set original Total on import
                                //vm.TotalOnImport = vm.NetsuiteComparisonCollection.FirstOrDefault().ImportTotal;

                            }




                            //Scanned Items
                            int ScannedItems2 = 0;
                            //Clear Collection
                            vm.NetsuiteScannedCollection.Clear();

                            foreach (NetsuiteInventory itm in vm.ScannedSerialCollection)
                            {


                                //Count Quantity in scanned collection 
                                ScannedItems2 = vm.ScannedSerialCollection.Where(item => item.ProductCode == itm.ProductCode).Count();


                                //Check if product code already exists, create new object to add to the comparison lines
                                if (!vm.NetsuiteScannedCollection.Any(p => p.ProductCode == itm.ProductCode))
                                {

                                    NetsuiteInventory nsi = new NetsuiteInventory();
                                    nsi.ProductCode = itm.ProductCode;
                                    nsi.ProductDescription = itm.ProductDescription;
                                    nsi.LineType = itm.LineType;
                                    nsi.Quantity = ScannedItems2.ToString();
                                    vm.NetsuiteScannedCollection.Add(nsi);
                                }


                            }

                            if (vm.ImportLVSelectedItem != null)
                            {


                                //Reimport original list first
                                //COUNT IMPORTED TOTAL
                                foreach (NetsuiteInventory ns in vm.NetsuiteOriginalImportCollection)
                                {
                                    //MessageBox.Show(ns.Quantity);

                                    vm.TotalOnImport += Convert.ToInt32(ns.Quantity);
                                }



                                //Set the total on initial import
                                //vm.TotalOnImport = vm.POImportTotal;

                                Console.WriteLine(vm.TotalOnImport.ToString());

                                vm.POImportTotal = 0;

                                //COUNT IMPORTED TOTAL
                                foreach (NetsuiteInventory ns in vm.NetsuiteComparisonCollection)
                                {
                                    //MessageBox.Show(ns.Quantity);

                                    vm.POImportTotal += Convert.ToInt32(ns.Quantity);
                                }




                                vm.POTotal = 0;

                                //COUNT IMPORTED TOTAL
                                foreach (NetsuiteInventory ns in vm.NetsuiteImportCollection)
                                {
                                    //MessageBox.Show(ns.Quantity);

                                    vm.POTotal += Convert.ToInt32(ns.Quantity);

                                    //Check for new part code bool and increase total
                                    if (ns.NewPartCodeRequired == true)
                                    {
                                        //Increase by 1
                                        vm.PartCodesTotal += 1;
                                    }
                                }
                            }

                            Mouse.OverrideCursor = null;

                        }


                        break;

                    case "ImportSerials":

                        if (vm.NetsuiteImportCollection.Count == 0)
                        {
                            vm.ImportMessage = "Please import a Netsuite PO LInes CSV first!";
                            vm.ImportMessageColour = Brushes.Red;
                            Mouse.OverrideCursor = null;
                            //Exit Import
                            return;
                        }
                        else
                        {
                            //IMPORT CSV 
                            //LOOP THROUGH LIST AND QUERY BARCODE using System.Windows.Forms;

                            using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                            {


                                //Requires GUID for MyComputer Or ThisPC Folder
                                openFileDialog.InitialDirectory = vm.downld; //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                                openFileDialog.Filter = "CSV files (*.csv)|*.csv";
                                openFileDialog.FilterIndex = 0;
                                openFileDialog.Multiselect = true;
                                openFileDialog.RestoreDirectory = true;

                                // string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");

                                //MessageBox.Show(rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime);


                                Mouse.OverrideCursor = Cursors.Wait;

                                //Clear import collection
                                vm.ImportSerialsCollection.Clear();

                                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                                {
                                    foreach (var file in openFileDialog.FileNames)
                                    {

                                        // MessageBox.Show(file);
                                        if (File.Exists(file))
                                        {

                                            ////Uses a VB class to process the file instead of streamreader
                                            using (TextFieldParser parser = new TextFieldParser(file))
                                            {
                                                //parser.TextFieldType = FieldType.Delimited;
                                                //parser.SetDelimiters(",");
                                                while (!parser.EndOfData)
                                                {
                                                    //Processing row
                                                    string currentLine = parser.ReadLine();


                                                    //New method to handle commas in field data
                                                    Regex Regexparser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                                                    string[] partserial = Regexparser.Split(currentLine);

                                                    ImportSerials imp = new ImportSerials();
                                                    imp.partcode = partserial[0];
                                                    imp.serialno = partserial[1];

                                                    if (vm.ImportSerialsCollection.Where(x => x.serialno == partserial[1]).Any())
                                                    {
                                                        vm.ImportMessage = "Duplicate Serial found! Please correct the CSV & import again. Serial: " + partserial[1];
                                                        vm.ImportMessageColour = Brushes.Red;
                                                        vm.ProgressIsActive = false;
                                                        vm.ProcessedMessageColour = Brushes.Red;

                                                        //Change mouse cursor 
                                                        Mouse.OverrideCursor = null;
                                                        //Exit Import
                                                        return;
                                                    }
                                                    else
                                                    {
                                                        //Import to the collection
                                                        vm.ImportSerialsCollection.Add(imp);
                                                    }




                                                }
                                            }
                                        }
                                    }
                                }


                                int LineCount = 0;

                                //ADD TO SCANNED SERIAL COLLECTION HERE

                                foreach (NetsuiteInventory nsi in vm.NetsuiteImportCollection)
                                {
                                    //Set as Zero
                                    LineCount = 0;

                                    //Set Serials remaining to Quantity
                                    nsi.SerialsRemaining = Convert.ToInt32(nsi.Quantity);


                                    //Loop through Serial Import and add if 
                                    foreach (ImportSerials imps in vm.ImportSerialsCollection)
                                    {

                                        //Check if import serial partcode matches selected line
                                        if (imps.partcode == nsi.ProductCode)
                                        {
                                            NetsuiteInventory ns = new NetsuiteInventory();
                                            ns.PONumber = vm.PONumber;
                                            ns.ProductCode = nsi.ProductCode;
                                            ns.ProductDescription = nsi.ProductDescription;
                                            ns.SerialNumber = imps.serialno;
                                            ns.Quantity = "1";
                                            ns.ReceivedByOperative = vm.CurrentUserInitials;
                                            ns.POQuantity = nsi.Quantity;
                                            ns.POLineUnitPrice = nsi.POLineUnitPrice;
                                            ns.POUnitAmount = nsi.POUnitAmount;
                                            ns.ReportPath = "";
                                            ns.TestedByOperative = "";
                                            ns.DriveFailedReason = "";
                                            ns.DriveFailedReasonSpecify = "";
                                            ns.DriveResult = "PENDING";
                                            ns.WipeSoftware = "";
                                            ns.ITADRef = "";


                                            //get how many are already scanned
                                            nsi.SerialsRemaining -= 1;
                                            //Increment LineCount
                                            LineCount += 1;


                                            //Check serials remaining of current line and mark as done
                                            if (nsi.SerialsRemaining == 0)
                                            {
                                                //Set Complete Status if all serials are scannned
                                                nsi.Complete = "Done";

                                            }
                                            //Add to Scanned Serial Collection
                                            vm.ScannedSerialCollection.Add(ns);
                                        }


                                    }

                                    //Check if blank count and skip adding
                                    if (LineCount != 0)
                                    {

                                        //Add to Line Comparison Collection
                                        NetsuiteInventory nst = new NetsuiteInventory();
                                        nst.ProductCode = nsi.ProductCode;
                                        nst.ProductDescription = nsi.ProductDescription;
                                        nst.LineType = nsi.LineType;
                                        nst.Quantity = LineCount.ToString();

                                        vm.NetsuiteScannedCollection.Add(nst);

                                    }


                                }


                                Mouse.OverrideCursor = null;

                            }

                            //Deselect item in the list
                            lvValue.SelectedIndex = 0;
                            vm.ImportMessageColour = Brushes.Green;
                            vm.ImportMessage = "Please Add All Extra's before starting the Serial Scan!";

                        }



                        break;

                    case "CheckDiscrepanciesDB":


                        //DUPLICATE SERIAL CHECK
                        ////Check for any serial duplicates
                        //CheckForDuplicateSerials();

                        //Check if serial already present
                        vm.SerialMessage2 = "";
                        // NetsuiteInventory sItem = new NetsuiteInventory();
                        if (vm.ScannedSerialCollection.Count > 0)
                        {
                            var Repeteditemlist = new List<string>();


                            List<String> duplicates = vm.ScannedSerialCollection.GroupBy(x => x.SerialNumber).Where(g => g.Count() > 1)
                                .Select(g => g.Key)
                                .ToList();

                            foreach (var item in duplicates)
                            {
                                // Console.WriteLine(item);

                                vm.SerialMessage2 = "Duplicate Serial " + item + " Found!";
                                Mouse.OverrideCursor = Cursors.Arrow;
                                return;
                            }
                        }



                        int itmIndex = 0;

                        //Clear Current Collection
                        vm.NetsuiteDiscrepancyCollection.Clear();



                        // LOOP THROUGH ORIGINAL  foreach (NetsuiteInventory itm in vm.NetsuiteComparisonCollection)
                        foreach (NetsuiteInventory itm in vm.NetsuiteComparisonCollection)
                        {

                            //Next Item In Recieved List   NetsuiteInventory nsInv = vm.NetsuiteImportCollection[itmIndex];
                            NetsuiteInventory nsInv = vm.NetsuiteImportCollection[itmIndex];

                            //Skip Various and Extra Lines
                            // if(itm.LineType != null) // || itm.LineType.Contains("(V)") || itm.LineType.Contains("(E)")
                            if (itm.ProductCode.Contains("VARIOUS") || itm.ProductCode.Contains("Various") || itm.ProductCode.Contains("various"))
                            {

                                if (nsInv.LineType != null)
                                    if (nsInv.LineType.Contains("(V)"))
                                    {

                                        NetsuiteDiscrepancyReport ndr = new NetsuiteDiscrepancyReport();
                                        ndr.PONumber = vm.PONumber; //nsInv.PONumber;
                                        ndr.LineType = nsInv.LineType;
                                        ndr.ProductCode = nsInv.ProductCode;
                                        ndr.ProductDescription = nsInv.ProductDescription;
                                        ndr.POLineUnitPrice = nsInv.POLineUnitPrice;
                                        ndr.ExpectedQuantity = itm.Quantity;
                                        ndr.ReceivedQuantity = nsInv.Quantity;
                                        ndr.NewPartCode = nsInv.NewPartCodeRequired;

                                        //Workout discrepancy
                                        int disc = Convert.ToInt32(nsInv.Quantity) - Convert.ToInt32(itm.Quantity);

                                        //MessageBox.Show(itm.Quantity + "  VS  " + nsInv.Quantity + "  Discrepancy= " + disc.ToString());


                                        ndr.Discrepancy = disc.ToString();

                                        vm.NetsuiteDiscrepancyCollection.Add(ndr);



                                    }
                            }
                            else
                            {
                                ////Next Item In Recieved List
                                //NetsuiteInventory nsInv = vm.NetsuiteImportCollection[itmIndex];



                                if (nsInv.ProductCode == itm.ProductCode)
                                {

                                    NetsuiteDiscrepancyReport ndr = new NetsuiteDiscrepancyReport();
                                    ndr.PONumber = vm.PONumber; //nsInv.PONumber;
                                    ndr.LineType = nsInv.LineType;
                                    ndr.ProductCode = nsInv.ProductCode;
                                    ndr.ProductDescription = nsInv.ProductDescription;
                                    ndr.POLineUnitPrice = nsInv.POLineUnitPrice;
                                    ndr.ExpectedQuantity = itm.Quantity;
                                    ndr.ReceivedQuantity = nsInv.Quantity;
                                    ndr.NewPartCode = nsInv.NewPartCodeRequired;

                                    //Workout discrepancy
                                    int disc = Convert.ToInt32(nsInv.Quantity) - Convert.ToInt32(itm.Quantity);

                                    //MessageBox.Show(itm.Quantity + "  VS  " + nsInv.Quantity + "  Discrepancy= " + disc.ToString());


                                    ndr.Discrepancy = disc.ToString();

                                    vm.NetsuiteDiscrepancyCollection.Add(ndr);


                                }

                            }

                            //increase index by 1
                            itmIndex += 1;

                        }


                        ////ADD EXTRAS
                        foreach (NetsuiteInventory extras in vm.NetsuiteImportCollection)
                        {

                            ////Next Item In Recieved List
                            //NetsuiteInventory nsInv = vm.NetsuiteImportCollection[itmIndex];

                            if (extras.LineType != null)
                                if (extras.LineType.Contains("(E)"))
                                {

                                    NetsuiteDiscrepancyReport ndr = new NetsuiteDiscrepancyReport();
                                    ndr.PONumber = vm.PONumber; //nsInv.PONumber;
                                    ndr.LineType = extras.LineType;
                                    ndr.ProductCode = extras.ProductCode;
                                    ndr.ProductDescription = extras.ProductDescription;
                                    ndr.POLineUnitPrice = extras.POLineUnitPrice;
                                    ndr.ExpectedQuantity = "0";
                                    ndr.ReceivedQuantity = extras.Quantity;
                                    ndr.NewPartCode = extras.NewPartCodeRequired;
                                    //Workout discrepancy
                                    //int disc = Convert.ToInt32(ndr.ExpectedQuantity) - Convert.ToInt32(extras.Quantity);
                                    int disc = Convert.ToInt32(extras.Quantity);


                                    ndr.Discrepancy = disc.ToString();

                                    vm.NetsuiteDiscrepancyCollection.Add(ndr);

                                }
                        }
                        //END ADD EXTRAS


                        ReportDriveTest rWin = new ReportDriveTest(vm, vm.NetsuiteDiscrepancyCollection);
                        // rWin.Show();

                        //foreach(NetsuiteDiscrepancyReport ns in vm.NetsuiteDiscrepancyCollection)
                        //{
                        //    Console.WriteLine(" ProductCode " + ns.ProductCode + " UnitPrice " + ns.POLineUnitPrice + " ExpQTY " + ns.ExpectedQuantity + " RecQTY " + ns.RecievedQuantity + " Discrepancy " + ns.Discrepancy);
                        //}

                        //Set Discrepancy Run Status
                        vm.HasDiscrepancyRun = true;

                        //Set processed message
                        vm.ProcessedMessage = "Discrepancy report generated!";
                        vm.ProcessedMessageColour = Brushes.Green;

                        //Flash discrepancy reminder
                        await PutTaskDelay(1000);
                        vm.DiscrepancyMessage = "If the discrepancy is correct, please commit the PO to the DataBase!";
                        await PutTaskDelay(800);
                        vm.DiscrepancyMessage = "";
                        await PutTaskDelay(1000);
                        vm.DiscrepancyMessage = "If the discrepancy is correct, please commit the PO to the DataBase!";
                        await PutTaskDelay(800);
                        vm.DiscrepancyMessage = "";
                        await PutTaskDelay(1000);
                        vm.DiscrepancyMessage = "If the discrepancy is correct, please commit the PO to the DataBase!";

                        break;
                    case "CheckFailures":

                        if (lvValue.Items.Count != 0)
                        {
                            //Declare temporary collection
                            ObservableCollection<NetsuiteInventory> nsiFails = new ObservableCollection<NetsuiteInventory>();


                            foreach (NetsuiteInventory nsi in vm.TestedDriveCollection)
                            {
                                if (nsi.DriveResult == "FAIL")
                                {
                                    //add flagged failed item to failed collection
                                    nsiFails.Add(nsi);

                                }
                            }

                            //Sort Collection
                            nsiFails = new ObservableCollection<NetsuiteInventory>(nsiFails.OrderBy(a => a.ProductCode));


                            //Launch report window
                            ReportDriveTest rWin2 = new ReportDriveTest(vm, nsiFails);

                        }


                        break;
                    case "GetFiles":
                        Stopwatch watch = new Stopwatch();
                        watch.Start();

                        //Create Hash Set For Extension
                        var dirHash = new HashSet<string> { ".pdf", ".docx", ".txt", ".xlsx", ".xml" };

                        //Enumerate Files in specified Directory
                        var fileHash = Directory.EnumerateFiles(@"W:\Warehouse General\TB Tools App\XML Product Wipe Profiles", "*.*")
                                                   .AsParallel()
                                                   .Where(f => !new FileInfo(f).Attributes.HasFlag(FileAttributes.System) &&
                                                               dirHash.Contains(Path.GetExtension(f))).ToHashSet();



                        var filesDiscovered = fileHash; //.Where(x => !result1.files.Contains(x));

                        foreach (var finfo in filesDiscovered)
                        {
                            Console.WriteLine(finfo.Replace(@"W:\Warehouse General\TB Tools App\XML Product Wipe Profiles\", ""));
                        }

                        watch.Stop();
                        Console.WriteLine("\n\nTime Taken: " + watch.Elapsed.TotalMilliseconds.ToString());



                        break;
                    case "EmailReports":
                        //Loop Through loaded collection and create local folder in My Docs, Copy each found path to folder from Dropfolder, 
                        //Loop through each pdf in local folder and create a zip. Delete local files but keep zip. Creat Outlook object and add zip attachment.
                        if (lvValue.Items.Count != 0)
                        {
                            //MessageBox.Show("Hmmm");

                            try
                            {

                                //Clear any Documents folders
                                if (Directory.Exists(vm.myDocs + @"\" + vm.DBSearch + "_Reports"))
                                {
                                    //Delete Local Folder
                                    Directory.Delete(vm.myDocs + @"\" + vm.DBSearch + "_Reports", true);

                                }

                                ////Set Busy Indicator to true
                                // vm.ProgressIsActive = true;
                                // //Change mouse cursor 
                                // Mouse.OverrideCursor = Cursors.Wait;

                                vm.ProcessedMessageColour = Brushes.MediumPurple;
                                vm.ProcessedMessage = "All Reports for current PO are being Archived. Please Wait....";



                                //Create Local Folder
                                if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                                {
                                    if (string.IsNullOrEmpty(vm.DBSearch))
                                    {
                                    }
                                    else
                                    {
                                        //Create local file
                                        Directory.CreateDirectory(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports");
                                    }

                                }


                                //Call Bulk Copy Function ASYNC

                                //Check which LV was passed

                                if (lvValue.Name.Contains("TestResults"))
                                {

                                    //Run location specific switch
                                    switch (vm.CurrentUserLocation)
                                    {
                                        case "Australia":
                                            //Call search and await the results
                                            if (Directory.Exists(@"W:\AU Warehouse\Reports\Drop"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + @"W:\AU Warehouse\Reports\Drop", vm.TestedDriveCollection));
                                            }
                                            else if (Directory.Exists(@"W:\Reports\Drop"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + @"W:\Reports\Drop", vm.TestedDriveCollection));
                                            }
                                            break;
                                        case "United States":
                                            //Check Directory Existence
                                            if (Directory.Exists(@"T:\NJ Warehouse\Test Reports\Drop"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + @"T:\NJ Warehouse\Test Reports\Drop", vm.TestedDriveCollection));
                                            }
                                            else if (Directory.Exists(@"T:\Test Reports\Drop"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + @"T:\Test Reports\Drop", vm.TestedDriveCollection));
                                            }
                                            break;
                                        case "United Kingdom":
                                            //Check Directory Existence
                                            if (Directory.Exists(@"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder", vm.TestedDriveCollection));
                                            }

                                            break;
                                        case "France":
                                            //Call search and await the results
                                            if (Directory.Exists(@"W:\FR Warehouse\Drive Test\Test Reports\DropFolder"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + @"W:\FR Warehouse\Drive Test\Test Reports\DropFolder", vm.TestedDriveCollection));
                                            }
                                            break;
                                        case "Germany":
                                            //Call search and await the results
                                            if (Directory.Exists(@"W:\Reports\Drop"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + @"W:\Reports\Drop", vm.TestedDriveCollection));
                                            }
                                            break;

                                    }


                                }
                                else
                                {
                                    //Run location specific switch
                                    switch (vm.CurrentUserLocation)
                                    {
                                        case "Australia":
                                            //Call search and await the results
                                            if (Directory.Exists(@"W:\AU Warehouse\Reports\Drop"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + @"W:\AU Warehouse\Reports\Drop", vm.TestedDriveCollection));
                                            }
                                            else if (Directory.Exists(@"W:\Reports\Drop"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + @"W:\Reports\Drop", vm.TestedDriveCollection));
                                            }
                                            break;
                                        case "United States":
                                            //Check Directory Existence
                                            if (Directory.Exists(@"T:\NJ Warehouse\Test Reports\Drop"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + @"T:\NJ Warehouse\Test Reports\Drop", vm.TestedDriveCollection));
                                            }
                                            else if (Directory.Exists(@"T:\Test Reports\Drop"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + @"T:\Test Reports\Drop", vm.TestedDriveCollection));
                                            }
                                            break;
                                        case "United Kingdom":
                                            //Check Directory Existence
                                            if (Directory.Exists(@"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder", vm.TestedDriveCollection));
                                            }

                                            break;
                                        case "France":
                                            //Call search and await the results
                                            if (Directory.Exists(@"W:\FR Warehouse\Drive Test\Test Reports\DropFolder"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + @"W:\FR Warehouse\Drive Test\Test Reports\DropFolder", vm.TestedDriveCollection));
                                            }
                                            break;
                                        case "Germany":
                                            //Call search and await the results
                                            if (Directory.Exists(@"W:\Reports\Drop"))
                                            {
                                                //Loops Through and copies all files from destination to source
                                                await System.Threading.Tasks.Task.Run(() => CopyBulkReportsZipEmail(@"" + @"W:\Reports\Drop", vm.TestedDriveCollection));
                                            }
                                            break;

                                    }

                                }





                                vm.ProgressIsActive = false;
                                //Stop Timer
                                timer.Stop();
                                timeTaken = timer.Elapsed;
                                timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                //vm.ProcessedMessageColour = Brushes.Green;

                                //vm.ProcessedMessage = "Reports Zipped & Attached successfully! Launching Outlook Email, Please Wait..." + timetaken;

                                ////Change mouse cursor 
                                //Mouse.OverrideCursor = null;




                            }
                            catch (Exception ex)
                            {
                                //Change mouse cursor 

                                Mouse.OverrideCursor = null;
                                vm.ProgressIsActive = false;
                                vm.ProcessedMessageColour = Brushes.Red;
                                vm.ProcessedMessage = ex.Message;
                            }
                        }
                        break;
                    case "WindowsSearch":
                        string connectionString = "Provider=Search.CollatorDSO;Extended Properties=\"Application=Windows\"";
                        OleDbConnection connection = new OleDbConnection(connectionString);

                        break;

                    case "GenerateFlowDoc":
                        try
                        {


                            ////Set Busy Indicator to true
                            vm.ProgressIsActive = true;
                            ////Change mouse cursor 
                            Mouse.OverrideCursor = Cursors.Wait;

                            vm.ProcessedMessageColour = Brushes.MediumPurple;
                            vm.ProcessedMessage = "All Reports for current PO are being Archived. Please Wait....";


                            //Create report for NPVision PDF
                            //Create new Flow Document
                            FlowDocument fd = new FlowDocument();

                            FlowDocument ReportFD = new FlowDocument();
                            Section MainSection = new Section();
                            MainSection.Padding = new Thickness(2);
                            MainSection.Margin = new Thickness(0);
                            Section mySectionReportHeader = new Section();
                            mySectionReportHeader.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#0f89bd");  //(Brush)bc.ConvertFrom("#CC119EDA");


                            BitmapImage bmp0 = new BitmapImage();

                            bmp0.BeginInit();
                            bmp0.UriSource = new Uri("pack://application:,,,/Images/TechBuyerLogoCenteredReports.png", UriKind.Absolute);
                            bmp0.EndInit();

                            //Create Block ui container and add image as a child
                            BlockUIContainer blockUIImage = new BlockUIContainer();
                            blockUIImage.Padding = new Thickness(10);
                            blockUIImage.Margin = new Thickness(10);
                            Image img0 = new Image();
                            //Image.HorizontalAlignmentProperty. = HorizontalAlignment.Center;
                            img0.Source = bmp0;
                            img0.Width = 150;
                            img0.Height = 75;
                            blockUIImage.Child = img0;



                            Paragraph ReportHeader = new Paragraph(new Run("Drive Results"));
                            ReportHeader.Padding = new Thickness(0, 5, 0, 5);
                            ReportHeader.FontSize = 24;
                            ReportHeader.FontFamily = new FontFamily("Segoe UI");
                            ReportHeader.Foreground = Brushes.White;
                            ReportHeader.TextAlignment = TextAlignment.Center;
                            ReportHeader.Margin = new Thickness(0, 5, 0, 5);
                            //Report date or title header
                            Paragraph ReportDateHeader = new Paragraph();

                            ReportDateHeader = new Paragraph(new Run("Total Drives " + vm.SearchCollection.Count().ToString()));
                            ReportDateHeader.Padding = new Thickness(0, 1, 0, 1);
                            ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
                            ReportDateHeader.FontSize = 20;
                            ReportDateHeader.Foreground = Brushes.White;
                            ReportDateHeader.TextAlignment = TextAlignment.Center;
                            ReportDateHeader.Margin = new Thickness(0, 1, 0, 1);
                            //Report date or title header
                            Paragraph ReportPassesFails = new Paragraph();

                            //Count Fails And Passes
                            int passed = 0;
                            int failed = 0;

                            foreach (NetsuiteInventory passfails in vm.SearchCollection)
                            {
                                if (passfails.DriveResult == "PASS")
                                {
                                    passed += 1;
                                }

                                if (passfails.DriveResult == "FAIL")
                                {
                                    failed += 1;
                                }
                            }

                            Run passedDrives = new Run();
                            passedDrives.Text = "Passed " + passed.ToString() + "   ";
                            passedDrives.Foreground = Brushes.Green;


                            Run failedDrives = new Run();
                            failedDrives.Text = "Failed " + failed.ToString() + "   ";
                            failedDrives.Foreground = Brushes.Red;

                            ReportPassesFails = new Paragraph();
                            ReportPassesFails.Inlines.Add(passedDrives);
                            ReportPassesFails.Inlines.Add(failedDrives);
                            ReportPassesFails.Padding = new Thickness(0);
                            ReportPassesFails.FontFamily = new FontFamily("Segoe UI");
                            ReportPassesFails.FontSize = 16;
                            ReportPassesFails.Foreground = Brushes.White;
                            ReportPassesFails.TextAlignment = TextAlignment.Center;
                            ReportPassesFails.Margin = new Thickness(0);

                            //Add TB Logo
                            //Add the country flag in the second cell



                            //Create paragraph block and add bitmap to it
                            //Paragraph ImageParagraph0 = new Paragraph();
                            //ImageParagraph0.Background = new ImageBrush(bmp0);

                            //Add image to section
                            mySectionReportHeader.Blocks.Add(blockUIImage);

                            //Add Paragraphs to Header
                            mySectionReportHeader.Blocks.Add(ReportHeader);
                            mySectionReportHeader.Blocks.Add(ReportDateHeader);


                            //Create new Section to host table
                            Section mySectionRecords = new Section();
                            mySectionRecords.Margin = new Thickness(0);
                            //Add fails and passes count
                            mySectionRecords.Blocks.Add(ReportPassesFails);

                            //Add Table
                            Table RecordTable = new Table();
                            RecordTable.CellSpacing = 2;
                            RecordTable.TextAlignment = TextAlignment.Center;
                            RecordTable.BorderBrush = Brushes.Black;
                            RecordTable.BorderThickness = new Thickness(1);
                            //RecordTable.BorderBrush = Brushes.Black;
                            //RecordTable.BorderThickness = new Thickness(.5);

                            // Create 6 columns and add them to the table's Columns collection.
                            int numberOfColumns = 5;
                            for (int x = 0; x < numberOfColumns; x++)
                            {
                                RecordTable.Columns.Add(new TableColumn());

                                //ALTER THE COLUMN WIDTHS
                                if (x == 0 || x == 1 || x == 2)
                                {
                                    RecordTable.Columns[x].Width = new GridLength(15, GridUnitType.Star);
                                }
                                else if (x == 3)
                                {
                                    RecordTable.Columns[x].Width = new GridLength(15, GridUnitType.Star);
                                }
                                else if (x == 6 || x == 7)
                                {
                                    RecordTable.Columns[x].Width = new GridLength(15, GridUnitType.Star);
                                }
                                else
                                {
                                    RecordTable.Columns[x].Width = new GridLength(15, GridUnitType.Star);
                                }


                            }

                            // Create and add an empty TableRowGroup to hold the table's Rows.
                            RecordTable.RowGroups.Add(new TableRowGroup());

                            // Add the first (title) row.
                            RecordTable.RowGroups[0].Rows.Add(new TableRow());

                            // Alias the current working row for easy reference.
                            TableRow currentRow = RecordTable.RowGroups[0].Rows[0];

                            currentRow = RecordTable.RowGroups[0].Rows[0];
                            // Global formatting for the title row.
                            currentRow.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#0f89bd");
                            currentRow.Foreground = Brushes.White;
                            currentRow.FontSize = 20;

                            currentRow.FontWeight = System.Windows.FontWeights.Bold;

                            // Global formatting for the header row.
                            currentRow.FontSize = 14;
                            currentRow.FontWeight = FontWeights.Normal;
                            currentRow.FontFamily = new FontFamily("Segoe UI");
                            // Add cells with content to the second row.
                            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Serial"))));
                            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Product No"))));
                            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Test Method"))));
                            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Date"))));
                            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Status"))));



                            // Add Headers to table
                            mySectionRecords.Blocks.Add(RecordTable);


                            //Start of Row To start adding collection
                            int RowIndex = 1;
                            string poNumber = "";

                            foreach (NetsuiteInventory itm in vm.SearchCollection)
                            {

                                RecordTable.RowGroups[0].Rows.Add(new TableRow());
                                currentRow = RecordTable.RowGroups[0].Rows[RowIndex];
                                currentRow.FontWeight = FontWeights.Normal;
                                currentRow.FontSize = 12;
                                currentRow.Foreground = Brushes.Black;
                                currentRow.FontFamily = new FontFamily("Segoe UI");

                                // Add cells with content to the third row.
                                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.SerialNumber))));
                                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.ProductCode))));
                                currentRow.Cells.Add(new TableCell(new Paragraph(new Run("WipeDrive"))));
                                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.ProcessedDate.ToShortDateString()))));
                                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.DriveResult))));

                                poNumber = itm.PONumber;

                                //Move to Next Row
                                RowIndex++;
                            }

                            //ADD ROW COLOURS
                            //To be used to create row colouring, needs to target the row groups
                            for (int n = 1; n < RecordTable.RowGroups[0].Rows.Count(); n++)
                            {
                                //for border
                                //currentRow.Cells[n].BorderThickness = new Thickness(0,1,0,1);
                                //currentRow.Cells[n].BorderBrush = Brushes.Black;
                                if (n % 2 == 0)
                                    RecordTable.RowGroups[0].Rows[n].Background = Brushes.AliceBlue;

                                else
                                    RecordTable.RowGroups[0].Rows[n].Background = Brushes.White;
                            }

                            //Add Table To Section
                            mySectionRecords.Blocks.Add(RecordTable);



                            //1 Add Header To Main Section
                            MainSection.Blocks.Add(mySectionReportHeader);
                            //2 Add Records to Main Section
                            MainSection.Blocks.Add(mySectionRecords);


                            //Add Main Section to flowdocument
                            ReportFD.Blocks.Add(MainSection);
                            //Set Clipboard
                            Clipboard.SetText(poNumber + " Report", TextDataFormat.UnicodeText);




                            //Call print to pdf function, pass in flowdoc and listview
                            //async version wont release the flodocument
                            //await Task.Run(() => vm.PrintSaveFlowDocumentDTCommand.Execute(new object[] { ReportFD ,  lvValue}));
                            //Run Sync instead
                            vm.PrintSaveFlowDocumentDTCommand.Execute(new object[] { ReportFD, lvValue });


                        }
                        catch (Exception ex)
                        {

                            vm.ProcessedMessage = "Error: " + ex.Message;
                        }

                        break;
                    case "GenerateFlowDocReport":
                        try
                        {
                            //Clear collection first
                            vm.ReportCollection.Clear();

                            ////Set Busy Indicator to true
                            //vm.ProgressIsActive = true;
                            ////Change mouse cursor 
                            //Mouse.OverrideCursor = Cursors.Wait;





                            //GET DATE VALUES SELECTED ON THE DATEPICKERS
                            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");


                            //MessageBox.Show("Before");

                            //return all items in date range
                            //Get Results with filter
                            await Task.Run(() => ReadBackgroundData("SELECT * FROM DriveInfo WHERE Date BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "' ORDER BY  DriveResult ASC"));

                            //Create Async Task for long running process
                            //await Task.Run(() => GenerateDriveReport(lvValue));
                            if (lvValue.Name.Contains("Test"))
                            {
                                vm.ProcessedMessageColour = Brushes.MediumPurple;
                                vm.ProcessedMessage = "All Reports for current PO are being Archived. Please Wait....";

                                await System.Threading.Tasks.Task.Run(() => TestGenerateDriveReport(lvValue));
                            }
                            else
                            {
                                vm.ProcessedMessageColour = Brushes.MediumPurple;
                                vm.ProcessedMessage = "The Breakdown Report is being Generated. Please Wait....";

                                //search report
                                //await System.Threading.Tasks.Task.Run(() => GenerateDriveReport(lvValue, vm.DateFrom.ToShortDateString() + " - " + vm.DateTo.ToShortDateString()));


                                //create report
                                GenerateDriveReport(lvValue, vm.DateFrom.ToShortDateString() + " - " + vm.DateTo.ToShortDateString());

                            }




                            //Set Message
                            vm.ProcessedMessage = "PDF Generated!";
                            vm.ProcessedMessageColour = Brushes.Green;



                        }
                        catch (Exception ex)
                        {

                            vm.ProcessedMessage = "Error: " + ex.Message;
                        }

                        break;

                    case "GenerateCSVReport":

                        try
                        {

                            //Clear collection first
                            vm.ReportCollection.Clear();

                            //Set Message
                            vm.ProcessedMessage = "Please Wait...";
                            vm.ProcessedMessageColour = Brushes.Green;



                            //GET DATE VALUES SELECTED ON THE DATEPICKERS
                            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

                            if (string.IsNullOrWhiteSpace(vm.DBSearch))
                            {

                                //return all items in date range
                                //Get Results with filter
                                await Task.Run(() => ReadBackgroundData("SELECT * FROM DriveInfo WHERE Date BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "' ORDER BY  DriveResult ASC"));

                                //Export report to csv
                                StaticFunctions.CreateDriveProcessingCSV(vm.ReportCollection.Where(x => x.ProcessedDate >= vm.DateFrom).Where(x => x.ProcessedDate <= vm.DateTo).ToList(), vm.DBSearch);

                            }
                            else
                            {

                                //MessageBox.Show(vm.DBSearch);
                                //return all items specified via search Date BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "' AND
                                //Get Results with filter
                                await Task.Run(() => ReadBackgroundData("SELECT * FROM DriveInfo WHERE RecOperative = '" + vm.DBSearch.ToUpper() + "' OR SerialNo LIKE '" + vm.DBSearch.ToUpper() + "' OR PONumber LIKE '" + vm.DBSearch.ToUpper() + "' ORDER BY  DriveResult ASC"));

                                //Export report to csv
                                StaticFunctions.CreateDriveProcessingCSV(vm.ReportCollection.ToList(), vm.DBSearch);
                            }




                            //Set Message
                            vm.ProcessedMessage = "CSV Generated!";
                            vm.ProcessedMessageColour = Brushes.Green;



                        }
                        catch (Exception ex)
                        {

                            vm.ProcessedMessage = "Error: " + ex.Message;
                        }

                        break;
                    case "GenerateCedarCSVReport":

                        try
                        {

                            //Clear collection first
                            vm.ReportCollection.Clear();

                            //Set Message
                            vm.ProcessedMessage = "Please Wait...";
                            vm.ProcessedMessageColour = Brushes.Green;


                            DateTime customDate = Convert.ToDateTime("01/08/2022");

                            //GET DATE VALUES SELECTED ON THE DATEPICKERS
                            var dateFrom = Convert.ToDateTime(customDate);
                            var dateTo = DateTime.UtcNow;

                            //Create new list
                            List<string> files = new List<string>();

                            switch (StaticFunctions.UserLocation)
                            {
                                case "Australia":
                                    //Call search and await the results
                                    //files = await System.Threading.Tasks.Task.Run(() => RecursiveFileSearch(@"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder", fromDate, toDate, "*.pdf"));
                                    break;
                                case "United States":

                                    break;
                                case "United Kingdom":

                                    //Show Busy Indicator
                                    vm.ProgressIsActive = true;

                                    //Check presence of drop folder
                                    if (Directory.Exists(@"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder"))
                                    {
                                        //Recursive search based on 1st of August to current date
                                        files = await System.Threading.Tasks.Task.Run(() => CedarRecursiveFileSearch(@"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder", dateFrom, dateTo, "*.pdf"));


                                        if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                                        {
                                            File.WriteAllLines(vm.myDocs + @"\Nebula Logs\CedarData_" + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".csv", files);
                                        }
                                    }
                                    else
                                    {

                                        vm.ProgressIsActive = false;
                                        //Stop Timer
                                        timer.Stop();
                                        timeTaken = timer.Elapsed;
                                        timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                        vm.ProcessedMessageColour = Brushes.Red;
                                        vm.ProcessedMessage = "UK Path " + @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder" + " is not available or missing. " + timetaken;

                                        //Change mouse cursor 
                                        Mouse.OverrideCursor = null;

                                        return;
                                    }

                                    break;
                            }


                            //Check file presence 
                            if (File.Exists(vm.myDocs + @"\Nebula Logs\CedarData_" + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".csv"))
                            {
                                //Open the CSV
                                StaticFunctions.OpenFileCommand(vm.myDocs + @"\Nebula Logs\CedarData_" + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".csv", "");
                            }


                            vm.ProgressIsActive = false;
                            //Stop Timer
                            timer.Stop();
                            timeTaken = timer.Elapsed;
                            timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                            //Set Message
                            vm.ProcessedMessage = "Cedar Report Generated!" + timetaken;
                            vm.ProcessedMessageColour = Brushes.Green;



                        }
                        catch (Exception ex)
                        {

                            vm.ProcessedMessage = "Error: " + ex.Message;
                        }

                        break;
                    case "DSOIndexQuery":

                        //foreach(var report in await System.Threading.Tasks.Task.Run(() => DSOIndexerFilesSearch("ukhafs", @"\\ukhafs.pinnacle.local\UK_Warehouse$\Testing\Drive Test\Wipe Drive Test Reports\DropFolder", vm.DBSearch, "*.pdf")))
                        //{
                        //    //Console.WriteLine(report);
                        //}

                        //MessageBox.Show(vm.DBSearch);

                        await System.Threading.Tasks.Task.Run(() => DSOIndexerFilesSearch("ukhafs", @"\\ukhafs.pinnacle.local\UK_Warehouse$\Testing\Drive Test\Wipe Drive Test Reports\DropFolder", vm.DBSearch, "*.pdf"));
                        //await System.Threading.Tasks.Task.Run(() => DSOIndexerFilesSearch("ukhafs", @"\\ukhafs.pinnacle.local\UK_Warehouse$\Testing\Technical\PURCHASE ORDER - TEST REPORTS\2023\01-Jan", vm.DBSearch, "*.pdf"));

                        break;
                    case "ReportLinkDB":
                        if (lvValue.Items.Count != 0)
                        {


                            try
                            {

                                ////Change mouse cursor 
                                Mouse.OverrideCursor = Cursors.Wait;


                                NetsuiteInventory nsi;

                                vm.ProcessedMessageColour = Brushes.MediumPurple;
                                vm.ProcessedMessage = "Reports for current PO are being linked! This can take several mins depending on File Server Load. Please Wait....";

                                vm.ProgressIsActive = true;

                                //Check if items there
                                if (lvValue.Items != null)
                                {
                                    //Get First Item in list and cast to NetsuiteInventory object
                                    if (lvValue.Items.GetItemAt(0) != null)
                                    {
                                        //DirectoryInfo directory = new DirectoryInfo(@"C:\Users\n.myers\Desktop\Test Files"); W:\Warehouse General\
                                        nsi = (NetsuiteInventory)lvValue.Items.GetItemAt(0);

                                        // declare date vars
                                        DateTime fromDate = Convert.ToDateTime(nsi.ProcessedDate.ToShortDateString());
                                        DateTime toDate = Convert.ToDateTime(DateTime.Now);


                                        //Add  check if Start Date Override date is selected
                                        if (string.IsNullOrEmpty(vm.StartDateOverride))
                                        {
                                            //Just use default Start Date from Database
                                            fromDate = Convert.ToDateTime(nsi.ProcessedDate.ToShortDateString());

                                        }
                                        else
                                        {
                                            //MessageBox.Show(vm.StartDateOverride);
                                            //// Get date from override date picker

                                            DateTime OverrideStartDate = DateTime.Parse(vm.StartDateOverride); // Convert.ToDateTime(vm.StartDateOverride);


                                            //Assign to from date
                                            fromDate = OverrideStartDate;

                                            //Apply date format based on location
                                            //switch (vm.CurrentUserLocation)
                                            //    {
                                            //        case "Australia":
                                            //            fromDate = DateTime.ParseExact(OverrideStartDate.ToShortDateString(), "dd/MM/yyyy", null);
                                            //            break;
                                            //        case "United Kingdom":

                                            //            fromDate = DateTime.ParseExact(OverrideStartDate.ToShortDateString(), "dd/MM/yyyy", null);
                                            //            //MessageBox.Show(fromDate.ToString());
                                            //            break;
                                            //        case "United States":
                                            //            //fromDate = DateTime.ParseExact(OverrideStartDate.ToShortDateString(), "dd/MM/yyyy", null);
                                            //            fromDate = Convert.ToDateTime(vm.StartDateOverride);

                                            //            break;
                                            //        case "France":
                                            //            fromDate = DateTime.ParseExact(OverrideStartDate.ToShortDateString(), "dd/MM/yyyy", null);
                                            //            break;
                                            //        case "Germany":
                                            //            fromDate = DateTime.ParseExact(OverrideStartDate.ToShortDateString(), "dd/MM/yyyy", null);
                                            //            break;
                                            //    }

                                            //}
                                            //MessageBox.Show(fromDate.ToShortDateString());


                                            //NEED TO ADD CONNECTION TO WIPE DRIVE DB  IT USES MYSQL
                                            bool USTestpath = false;

                                            //OLDER METHOD IS QUICKER
                                            //Create List to hold files
                                            List<string> files = new List<string>();

                                            switch (StaticFunctions.UserLocation)
                                            {
                                                case "Australia":
                                                    //Call search and await the results
                                                    if (Directory.Exists(@"W:\AU Warehouse\Reports\Drop"))
                                                    {
                                                        files = await System.Threading.Tasks.Task.Run(() => RecursiveFileSearch(@"W:\AU Warehouse\Reports\Drop", fromDate, toDate, "*.pdf"));


                                                        if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                                                        {
                                                            File.WriteAllLines(vm.myDocs + @"\Nebula Logs\DTReportLinkingLog " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".txt", files);
                                                        }

                                                    }
                                                    else if (Directory.Exists(@"W:\Reports\Drop"))
                                                    {
                                                        files = await System.Threading.Tasks.Task.Run(() => RecursiveFileSearch(@"W:\Reports\Drop", fromDate, toDate, "*.pdf"));


                                                        if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                                                        {
                                                            File.WriteAllLines(vm.myDocs + @"\Nebula Logs\DTReportLinkingLog " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".txt", files);
                                                        }

                                                    }
                                                    else
                                                    {

                                                        vm.ProgressIsActive = false;
                                                        //Stop Timer
                                                        timer.Stop();
                                                        timeTaken = timer.Elapsed;
                                                        timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                                        vm.ProcessedMessageColour = Brushes.Red;
                                                        vm.ProcessedMessage = "AUS Path " + @"W:\Reports\Drop" + " is not available or missing. " + timetaken;

                                                        //Change mouse cursor 
                                                        Mouse.OverrideCursor = null;

                                                        return;
                                                    }
                                                    break;
                                                case "United States":

                                                    if (Directory.Exists(@"T:\NJ Warehouse\Test Reports\Drop"))
                                                    {
                                                        files = await System.Threading.Tasks.Task.Run(() => RecursiveFileSearch(@"T:\NJ Warehouse\Test Reports\Drop", fromDate, toDate, "*.pdf"));


                                                        if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                                                        {
                                                            File.WriteAllLines(vm.myDocs + @"\Nebula Logs\DTReportLinkingLog " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".txt", files);
                                                        }

                                                    }
                                                    else if (Directory.Exists(@"T:\Test Reports\Drop"))
                                                    {
                                                        files = await System.Threading.Tasks.Task.Run(() => RecursiveFileSearch(@"T:\Test Reports\Drop", fromDate, toDate, "*.pdf"));


                                                        if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                                                        {
                                                            File.WriteAllLines(vm.myDocs + @"\Nebula Logs\DTReportLinkingLog " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".txt", files);
                                                        }

                                                    }
                                                    else
                                                    {

                                                        vm.ProgressIsActive = false;
                                                        //Stop Timer
                                                        timer.Stop();
                                                        timeTaken = timer.Elapsed;
                                                        timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                                        vm.ProcessedMessageColour = Brushes.Red;
                                                        vm.ProcessedMessage = "US Path " + @"T:\Test Reports\Drop" + " is not available or missing. " + timetaken;

                                                        //Change mouse cursor 
                                                        Mouse.OverrideCursor = null;

                                                        return;
                                                    }

                                                    break;
                                                case "United Kingdom":

                                                    if (Directory.Exists(@"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder"))
                                                    {

                                                        //ORIGINAL METHOD
                                                        files = await System.Threading.Tasks.Task.Run(() => RecursiveFileSearch(@"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder", fromDate, toDate, "*.pdf"));
                                                        //TODO
                                                        //NEW METHOD DSO INDEXER QUERY
                                                        //foreach(NetsuiteInventory serials in vm.TestedDriveCollection)
                                                        //{
                                                        //    string report = await System.Threading.Tasks.Task.Run(() => DSOIndexerFileSearch("ukhafs", @"\\ukhafs.pinnacle.local\UK_Warehouse$\Testing\Drive Test\Wipe Drive Test Reports\DropFolder", serials.SerialNumber, "*.pdf"));

                                                        //    if (!string.IsNullOrEmpty(report))
                                                        //    {
                                                        //        //add only found reports to the collection
                                                        //        if (!files.Contains(report))
                                                        //        {

                                                        //            files.Add(report);
                                                        //        }

                                                        //    }

                                                        //}


                                                        Console.WriteLine(files.Count().ToString());

                                                        if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                                                        {
                                                            File.WriteAllLines(vm.myDocs + @"\Nebula Logs\DTReportLinkingLog " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".txt", files);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        // MessageBox.Show("UK Path " + @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder" + " is not available or missing.");

                                                        vm.ProgressIsActive = false;
                                                        //Stop Timer
                                                        timer.Stop();
                                                        timeTaken = timer.Elapsed;
                                                        timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                                        vm.ProcessedMessageColour = Brushes.Red;
                                                        vm.ProcessedMessage = "UK Path " + @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder" + " is not available or missing. " + timetaken;

                                                        //Change mouse cursor 
                                                        Mouse.OverrideCursor = null;

                                                        return;
                                                    }

                                                    break;
                                                case "France":



                                                    if (Directory.Exists(@"W:\FR Warehouse\Drive Test\Test Reports\DropFolder"))
                                                    {

                                                        files = await System.Threading.Tasks.Task.Run(() => RecursiveFileSearch(@"W:\FR Warehouse\Drive Test\Test Reports\DropFolder", fromDate, toDate, "*.pdf"));


                                                        if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                                                        {
                                                            File.WriteAllLines(vm.myDocs + @"\Nebula Logs\DTReportLinkingLog " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".txt", files);
                                                        }
                                                    }
                                                    else
                                                    {


                                                        vm.ProgressIsActive = false;
                                                        //Stop Timer
                                                        timer.Stop();
                                                        timeTaken = timer.Elapsed;
                                                        timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                                        vm.ProcessedMessageColour = Brushes.Red;
                                                        vm.ProcessedMessage = "FRA Path " + @"W:\FR Warehouse\Drive Test\Test Reports\DropFolder" + " is not available or missing. " + timetaken;

                                                        //Change mouse cursor 
                                                        Mouse.OverrideCursor = null;

                                                        return;
                                                    }

                                                    break;

                                                case "Germany":
                                                    if (Directory.Exists(@"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder"))
                                                    {
                                                        files = await System.Threading.Tasks.Task.Run(() => RecursiveFileSearch(@"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder", fromDate, toDate, "*.pdf"));


                                                        if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                                                        {
                                                            File.WriteAllLines(vm.myDocs + @"\Nebula Logs\DTReportLinkingLog " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".txt", files);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        // MessageBox.Show("UK Path " + @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder" + " is not available or missing.");

                                                        vm.ProgressIsActive = false;
                                                        //Stop Timer
                                                        timer.Stop();
                                                        timeTaken = timer.Elapsed;
                                                        timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                                        vm.ProcessedMessageColour = Brushes.Red;
                                                        vm.ProcessedMessage = "UK Path " + @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", @"") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder" + " is not available or missing. " + timetaken;

                                                        //Change mouse cursor 
                                                        Mouse.OverrideCursor = null;

                                                        return;
                                                    }


                                                    break;

                                            }


                                            //Call DB 
                                            //OLD Method
                                            //await System.Threading.Tasks.Task.Run(() => UpdateDriveStatusDB(lvValue, files));
                                            //New Transaction Method
                                            await System.Threading.Tasks.Task.Run(() => UpdateDriveStatusDBTransaction(lvValue, files));


                                            //Search PO Again
                                            await FilterSearchTest(vm.DBSearch, "");


                                            vm.ProgressIsActive = false;
                                            //Stop Timer
                                            timer.Stop();
                                            timeTaken = timer.Elapsed;
                                            timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                            vm.ProcessedMessageColour = Brushes.Green;
                                            vm.ProcessedMessage = "Reports linked successfully! " + timetaken;


                                            //Select first item
                                            lvValue.SelectedIndex = 0;

                                            //reset override date to empty
                                            vm.StartDateOverride = "";

                                            //Change mouse cursor 
                                            Mouse.OverrideCursor = null;

                                        }

                                        //MessageBox.Show(timetaken);
                                    }

                                }

                            }


                            catch (Exception ex)
                            {
                                vm.ProgressIsActive = false;
                                //Change mouse cursor 
                                Mouse.OverrideCursor = null;
                                vm.ProcessedMessageColour = Brushes.Red;
                                vm.ProcessedMessage = ex.Message;

                                vm.SystemMessageDialog("Drive Processing", ex.Message);

                            }

                        }

                        break;



                }

                //Change Mouse Cursor
                Mouse.OverrideCursor = null;
                vm.ProgressIsActive = false;
                //}

            }
            catch (Exception ex)
            {
                //Change Mouse Cursor
                Mouse.OverrideCursor = null;
                vm.ProgressIsActive = false;

                //Launch custom dialog Standard, YesNo, YesNoCancel for dialogtype
                vm.SystemMessageDialog("Drive Processing", ex.Message);

            }

            //TRY END
        }

        //Copy Reports From Drop box zip and email
        public async void CopyBulkReportsZipEmail(string sourceDir, ObservableCollection<NetsuiteInventory> CollectionToUse)
        {

            ////Create stopwatch for timing
            //var timer = new Stopwatch();
            //timer.Start();
            //TimeSpan timeTaken;
            //string timetaken;
            // string sourceDir = @"W:\Testing\Drive Test\Wipe Drive Test Reports\DropFolder"; // + StaticFunctions.UncPathToUse;



            //Loop through collection and copy file to local folder

            foreach (NetsuiteInventory nsi in CollectionToUse)
            {

                if (File.Exists(nsi.ReportPath))
                {
                    // Console.WriteLine(nsi.ReportPath);
                    //Copy File from Drop Folder to local directory
                    // Remove path from the file name.
                    string fName = nsi.ReportPath.Substring(sourceDir.Length + 1);

                    // Console.WriteLine(fName);
                    // Use the Path.Combine method to safely append the file name to the path.
                    // Will overwrite if the destination file already exists.
                    //MessageBox.Show("FROM " + Path.Combine(sourceDir, fName) + " TO " + Path.Combine(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports", fName));
                    File.Copy(Path.Combine(sourceDir, fName), Path.Combine(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports", fName), true);
                }
            }


            //zip folder and attach to email 

            EmailClass zipemail = new EmailClass(vm);
            await System.Threading.Tasks.Task.Run(() => zipemail.SearchZipFolderAndSendMail(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports"));

            //Delete Original Folder?
            //Create Local Folder
            if (Directory.Exists(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports"))
            {
                //Delete Local Folder
                Directory.Delete(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports", true);

            }


        }

        //Transaction method
        public void UpdateDriveStatusDBTransaction(ListView lvValue, List<string> files)
        {


            try
            {



                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);

                //IEnumerable<string> ReportList;
                List<string> ReportList = new List<string>();
                List<string> myQueries = new List<string>();
                List<SQLiteCommand> mySQLCommandsCollection = new List<SQLiteCommand>();

                //Loop Through All PO Items in List View
                foreach (NetsuiteInventory itm in lvValue.Items)
                {
                    //loop through date restricted list
                    //SQLiteCommand sqlite_cmd;
                    //sqlite_cmd = sqlite_conn.CreateCommand();

                    //sqlite_cmd.CommandText = "UPDATE DriveInfo SET ReportPath = @reportpath,DriveResult = @driveresult,DriveFailiure = @drivefailiure,DriveFailiureSpecify = @drivefailiurespecify,TestOperative = @testoperative WHERE SerialNo = @serialno;";

                    //sqlite_cmd.CommandType = CommandType.Text;

                    if (itm.DriveFailedReasonSpecify.Contains(@"n\a") || itm.LineType.Contains(@"n\a") || itm.DriveFailedReason.Contains(@"n\a") || itm.ReportPath.Contains(@"n\a") || itm.WipeSoftware.Contains(@"n\a"))
                    {

                        myQueries.Add("UPDATE DriveInfo SET ReportPath = '',DriveResult = 'PENDING',DriveFailiure = '',DriveFailiureSpecify = '',TestOperative = '' WHERE SerialNo = '" + itm.SerialNumber + "'");

                    }





                    //Pull in any items that match the serial exactly
                    //ReportList = files.Where(fl => fl.Contains(itm.SerialNumber)).AsParallel().ToList();


                    foreach (string str in files)
                    {
                        if (str.Contains(itm.SerialNumber.Replace(".", "")))
                        {
                            ReportList.Add(str);
                            // Console.WriteLine(str);
                        }

                    }







                    //Check if any items are returned, if not jump to the non detected section and step backwards
                    if (ReportList.Count() != 0)
                    {


                        //If report count is greater than 1 pick only the success
                        //if (ReportList.Count() > 1)
                        //{
                        //Console.WriteLine("\n" + ReportList.Count().ToString());

                        foreach (string item in ReportList)
                        {
                            //Console.WriteLine(item);
                            //Check Success last

                            if (item.Contains("FAILURE"))
                            {

                                if (item.Contains(itm.SerialNumber.Replace(".", "")))
                                {
                                    myQueries.Add("UPDATE DriveInfo SET ReportPath = '" + item.Replace(@"\\ukhafs.pinnacle.local\UK_Warehouse$\", @"W:\") + "',DriveResult = 'FAIL',DriveFailiure = '" + itm.DriveFailedReason + "',DriveFailiureSpecify = '" + itm.DriveFailedReasonSpecify + "',TestOperative = '" + vm.CurrentUserInitials + "' WHERE SerialNo = '" + itm.SerialNumber + "'");


                                }
                            }


                            if (item.Contains("SUCCESS"))
                            {

                                if (item.Contains(itm.SerialNumber.Replace(".", "")))
                                {
                                    myQueries.Add("UPDATE DriveInfo SET ReportPath = '" + item.Replace(@"\\ukhafs.pinnacle.local\UK_Warehouse$\", @"W:\") + "',DriveResult = 'PASS',DriveFailiure = '" + itm.DriveFailedReason + "',DriveFailiureSpecify = '" + itm.DriveFailedReasonSpecify + "',TestOperative = '" + vm.CurrentUserInitials + "' WHERE SerialNo = '" + itm.SerialNumber + "'");

                                }
                            }







                        } //FOR EACH




                    }
                    else
                    {

                        //ANY Remaining PENDING ITEMS HAVE FAILED
                        //SQLiteCommand sqlite_cmd;
                        //sqlite_cmd = sqlite_conn.CreateCommand();

                        //sqlite_cmd.CommandText = "UPDATE DriveInfo SET ReportPath = @reportpath,DriveResult = @driveresult,DriveFailiure = @drivefailiure,TestOperative = @testoperative WHERE SerialNo = @serialno;";

                        ReportList = files.Where(fl => fl.Like(itm.SerialNumber.Substring(0, itm.SerialNumber.Count() - 3))).AsParallel().ToList();



                        foreach (string item in ReportList)
                        {
                            // Console.WriteLine(item);



                            if (item.Contains("FAILURE"))
                            {
                                //Console.WriteLine("FAIL 2 " + itm.SerialNumber);
                                if (itm.SerialNumber.Count() > 11 && item.Contains(itm.SerialNumber.Substring(0, itm.SerialNumber.Count() - 3).Replace(".", "")))
                                {

                                    myQueries.Add("UPDATE DriveInfo SET ReportPath = '" + item.Replace(@"\\ukhafs.pinnacle.local\UK_Warehouse$\", @"W:\") + "',DriveResult = 'FAIL',DriveFailiure = '" + itm.DriveFailedReason + "',DriveFailiureSpecify = '" + itm.DriveFailedReasonSpecify + "',TestOperative = '" + vm.CurrentUserInitials + "' WHERE SerialNo = '" + itm.SerialNumber + "'");

                                }
                            }
                            else if (item.Contains("SUCCESS"))
                            {

                                if (itm.SerialNumber.Count() > 11 && item.Contains(itm.SerialNumber.Substring(0, itm.SerialNumber.Count() - 3).Replace(".", "")))
                                {
                                    myQueries.Add("UPDATE DriveInfo SET ReportPath = '" + item.Replace(@"\\ukhafs.pinnacle.local\UK_Warehouse$\", @"W:\") + "',DriveResult = 'PASS',DriveFailiure = '" + itm.DriveFailedReason + "',DriveFailiureSpecify = '" + itm.DriveFailedReasonSpecify + "',TestOperative = '" + vm.CurrentUserInitials + "' WHERE SerialNo = '" + itm.SerialNumber + "'");

                                }
                            }


                        }

                    }

                    //Console.WriteLine(currentFilePath);
                }




                //Create a transaction
                using (var tra = sqlite_conn.BeginTransaction())
                {
                    try
                    {
                        //Console.WriteLine(myQueries.Count().ToString() + "\n");

                        foreach (var myQuery in myQueries)
                        {
                            //Console.WriteLine(myQuery.ToString());

                            using (var cd = new SQLiteCommand(myQuery, sqlite_conn, tra))
                            {
                                cd.ExecuteNonQuery();
                            }
                        }

                        tra.Commit();
                    }
                    catch (Exception ex)
                    {
                        tra.Rollback();
                        //Console.Error.Writeline("I did nothing, because something wrong happened: {0}", ex);
                        throw;
                    }
                }










                //Close DB
                sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                vm.ProgressIsActive = false;
                vm.ProcessedMessageColour = Brushes.Red;
                vm.ProcessedMessage = "Linking error! " + ex.Message;

                //Change mouse cursor 
                //Mouse.OverrideCursor = null;

                //MessageBox.Show(ex.Message);
            }
        }

        public void UpdateDriveStatusDB(ListView lvValue, List<string> files)
        {


            try
            {



                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);

                //IEnumerable<string> ReportList;
                List<string> ReportList = new List<string>();

                //Loop Through All PO Items in List View
                foreach (NetsuiteInventory itm in lvValue.Items)
                {
                    //loop through date restricted list
                    SQLiteCommand sqlite_cmd;
                    sqlite_cmd = sqlite_conn.CreateCommand();

                    sqlite_cmd.CommandText = "UPDATE DriveInfo SET ReportPath = @reportpath,DriveResult = @driveresult,DriveFailiure = @drivefailiure,DriveFailiureSpecify = @drivefailiurespecify,TestOperative = @testoperative WHERE SerialNo = @serialno;";

                    sqlite_cmd.CommandType = CommandType.Text;

                    if (itm.DriveFailedReasonSpecify.Contains(@"n\a") || itm.LineType.Contains(@"n\a") || itm.DriveFailedReason.Contains(@"n\a") || itm.ReportPath.Contains(@"n\a") || itm.WipeSoftware.Contains(@"n\a"))
                    {
                        //Console.WriteLine(itm.SerialNumber);

                        ////Reset Line
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", itm.SerialNumber));
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", ""));
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiurespecify", ""));
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", "PENDING"));
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportpath", ""));
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@testoperative", ""));

                        sqlite_cmd.ExecuteNonQuery();
                    }





                    //Pull in any items that match the serial exactly
                    //ReportList = files.Where(fl => fl.Contains(itm.SerialNumber)).AsParallel().ToList();


                    foreach (string str in files)
                    {
                        if (str.Contains(itm.SerialNumber.Replace(".", "")))
                        {
                            ReportList.Add(str);
                            // Console.WriteLine(str);
                        }

                    }







                    //Check if any items are returned, if not jump to the non detected section and step backwards
                    if (ReportList.Count() != 0)
                    {




                        sqlite_cmd.CommandType = CommandType.Text;
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", itm.SerialNumber));
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@testoperative", vm.CurrentUserInitials));
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", itm.DriveFailedReason));
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiurespecify", itm.DriveFailedReasonSpecify));


                        //If report count is greater than 1 pick only the success
                        //if (ReportList.Count() > 1)
                        //{


                        foreach (string item in ReportList)
                        {
                            //Console.WriteLine(item);
                            //Check Success last

                            if (item.Contains("FAILURE"))
                            {

                                if (item.Contains(itm.SerialNumber.Replace(".", "")))
                                {
                                    //Console.WriteLine("FAIL " + itm.SerialNumber);
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", itm.DriveFailedReason));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiurespecify", itm.DriveFailedReasonSpecify));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", "FAIL"));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportpath", item));

                                    sqlite_cmd.ExecuteNonQuery();
                                    //break;
                                }
                            }


                            if (item.Contains("SUCCESS"))
                            {

                                if (item.Contains(itm.SerialNumber.Replace(".", "")))
                                {
                                    //Console.WriteLine("SUCCESS " + itm.SerialNumber);
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", ""));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiurespecify", ""));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", "PASS"));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportpath", item));

                                    sqlite_cmd.ExecuteNonQuery();
                                    //break;
                                }
                            }







                        } //FOR EACH




                    }
                    else
                    {

                        //ANY Remaining PENDING ITEMS HAVE FAILED
                        //SQLiteCommand sqlite_cmd;
                        //sqlite_cmd = sqlite_conn.CreateCommand();

                        //sqlite_cmd.CommandText = "UPDATE DriveInfo SET ReportPath = @reportpath,DriveResult = @driveresult,DriveFailiure = @drivefailiure,TestOperative = @testoperative WHERE SerialNo = @serialno;";

                        ReportList = files.Where(fl => fl.Like(itm.SerialNumber.Substring(0, itm.SerialNumber.Count() - 3))).AsParallel().ToList();



                        foreach (string item in ReportList)
                        {
                            // Console.WriteLine(item);



                            if (item.Contains("FAILURE"))
                            {
                                //Console.WriteLine("FAIL 2 " + itm.SerialNumber);
                                if (itm.SerialNumber.Count() > 11 && item.Contains(itm.SerialNumber.Substring(0, itm.SerialNumber.Count() - 3).Replace(".", "")))
                                {
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", itm.SerialNumber));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", itm.DriveFailedReason));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiurespecify", itm.DriveFailedReasonSpecify));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", "FAIL"));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportpath", item));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@testoperative", vm.CurrentUserInitials));

                                    sqlite_cmd.ExecuteNonQuery();

                                    //break;
                                }
                            }
                            else if (item.Contains("SUCCESS"))
                            {

                                if (itm.SerialNumber.Count() > 11 && item.Contains(itm.SerialNumber.Substring(0, itm.SerialNumber.Count() - 3).Replace(".", "")))
                                {
                                    //Console.WriteLine("SUCCESS 2 " + itm.SerialNumber);
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", itm.SerialNumber));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", ""));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiurespecify", ""));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", "PASS"));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportpath", item));
                                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@testoperative", vm.CurrentUserInitials));

                                    sqlite_cmd.ExecuteNonQuery();
                                    //break;
                                }
                            }
                            //else
                            //{
                            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", itm.SerialNumber));
                            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", ""));
                            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiurespecify", ""));
                            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", "PENDING"));
                            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportpath", item));
                            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@testoperative", vm.CurrentUserInitials));

                            //    sqlite_cmd.ExecuteNonQuery();
                            //}


                            //else
                            //{
                            //    // Console.WriteLine("MISMATCH2 " + itm.SerialNumber);
                            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", itm.SerialNumber));
                            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", ""));
                            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiurespecify", ""));
                            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", "SERIAL MISMATCH"));
                            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportpath", ""));
                            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@testoperative", vm.CurrentUserInitials));

                            //    sqlite_cmd.ExecuteNonQuery();
                            //}



                        }

                    }

                    //Console.WriteLine(currentFilePath);
                }



                //Close DB
                sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                vm.ProgressIsActive = false;
                vm.ProcessedMessageColour = Brushes.Red;
                vm.ProcessedMessage = "Linking error! " + ex.Message;

                //Change mouse cursor 
                Mouse.OverrideCursor = null;

                //MessageBox.Show(ex.Message);
            }
        }


        public void UpdateDriveStatusDB(ListView lvValue, HashSet<string> files)
        {

            Console.WriteLine("Out of recursive search Item count = " + files.Count());

            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            //Loop Through All PO Items in List View
            foreach (NetsuiteInventory itm in lvValue.Items)
            {
                var filteredFileList = files.Where(fl => fl.Contains(itm.SerialNumber)).FirstOrDefault();
                // Console.WriteLine(filteredFileList + " = " + itm.SerialNumber);
                //loop through date restricted list

                if (filteredFileList != null)
                {
                    if (filteredFileList.Contains(itm.SerialNumber))
                    {
                        SQLiteCommand sqlite_cmd;
                        sqlite_cmd = sqlite_conn.CreateCommand();

                        sqlite_cmd.CommandText = "UPDATE DriveInfo SET ReportPath = @reportpath,DriveResult = @driveresult,DriveFailiure = @drivefailiure WHERE SerialNo = @serialno;";

                        sqlite_cmd.CommandType = CommandType.Text;
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", itm.SerialNumber));
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportpath", filteredFileList));
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", ""));
                        if (filteredFileList.Contains("SUCCESS"))
                        {
                            sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", "PASS"));
                        }
                        else if (filteredFileList.Contains("FAILURE"))
                        {
                            sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", "FAIL"));
                        }


                        sqlite_cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    //ANY Remaining PENDING ITEMS HAVE FAILED
                    SQLiteCommand sqlite_cmd;
                    sqlite_cmd = sqlite_conn.CreateCommand();

                    sqlite_cmd.CommandText = "UPDATE DriveInfo SET ReportPath = @reportpath,DriveResult = @driveresult,DriveFailiure = @drivefailiure WHERE SerialNo = @serialno;";

                    sqlite_cmd.CommandType = CommandType.Text;
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", itm.SerialNumber));
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportpath", ""));
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", "FAIL"));
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", "Failed To Detect"));

                    sqlite_cmd.ExecuteNonQuery();
                }

                //Console.WriteLine(currentFilePath);
            }



            //Close DB
            sqlite_conn.Close();
        }


        public HashSet<string> GetFilesEnumerator(HashSet<string> Extensions, string dirPath, DateTime fromDate, DateTime toDate, bool includeDate)
        {


            //Create Hash Set For Extension
            var dirHash = Extensions; // new HashSet<string> { ".pdf", ".docx", ".txt", ".xlsx", ".xml" };
            HashSet<string> filesDiscovered = new HashSet<string>();

            switch (includeDate)
            {
                case true:

                    //Enumerate Files in specified Directory  WITH DATE RANGE
                    var fileHashDate = Directory.EnumerateFiles(dirPath, "*.pdf")
                                               .AsParallel()
                                               .Where(f => !new FileInfo(f).Attributes.HasFlag(FileAttributes.System) && new FileInfo(f).LastWriteTime >= fromDate && new FileInfo(f).LastWriteTime <= toDate &&
                                               dirHash.Contains(Path.GetExtension(f))).ToHashSet();

                    filesDiscovered = fileHashDate; //.Where(x => !result1.files.Contains(x));



                    break;


                case false:


                    //WITHOUT DATE RANGE
                    var fileHash = Directory.EnumerateFiles(dirPath, "*.pdf")
                                             .AsParallel()
                                             .Where(f => !new FileInfo(f).Attributes.HasFlag(FileAttributes.System) &&
                                                         dirHash.Contains(Path.GetExtension(f))).ToHashSet();

                    filesDiscovered = fileHash; //.Where(x => !result1.files.Contains(x));





                    break;
            }


            return filesDiscovered;

        }





        //GET HASH FOR ALL FILES IN DIRECTORY
        public static (HashSet<string> dirs, HashSet<(string file, long tick)> files) GetDriveTestInfo(string dir)
        {

            string CleanPath(string path) => path.Substring(dir.Length).TrimStart('/', '\\');

            // replace this with a hashing method if you need
            long GetUnique(string path) => new FileInfo(path).LastWriteTime.Ticks;

            //get all directories in specified path  , "*.*", SearchOption.AllDirectories , SearchOption.AllDirectories
            var directories = EnumerateDirectories(dir, "*", System.IO.SearchOption.TopDirectoryOnly); // Directory.GetDirectories(dir, "*", SearchOption.AllDirectories).Where(d => d != dir + @"\DfsrPrivate");





            //Strip out any DFS Folders from the array
            //directories = directories.Where(o => o != dir + @"\DfsrPrivate").ToArray();


            var dirHash = directories.Select(CleanPath).ToHashSet();

            // this could be paralleled if need be (if using a hash) 
            var fileHash = directories.SelectMany(Directory.EnumerateFiles)
               .Select(file => (name: CleanPath(file), ticks: GetUnique(file)))
               .ToHashSet();






            return (dirHash, fileHash);


        }

        // Enumerate all directories in DFS path, and skip any dfsr private folders
        public static IEnumerable<string> EnumerateDirectories(string parentDirectory, string searchPattern, System.IO.SearchOption searchOpt)
        {
            try
            {
                var directories = Enumerable.Empty<string>();
                if (searchOpt == System.IO.SearchOption.TopDirectoryOnly)
                {
                    directories = Directory.EnumerateDirectories(parentDirectory)
                        .SelectMany(x => EnumerateDirectories(x, searchPattern, searchOpt));
                }
                return directories.Concat(Directory.EnumerateDirectories(parentDirectory, searchPattern));
            }
            catch (UnauthorizedAccessException ex)
            {
                return Enumerable.Empty<string>();
            }
        }


        ////GET HASH FOR ALL FILES IN DIRECTORY
        //public static (HashSet<string> dirs, HashSet<(string file, long tick)> files) GetDriveTestFileInfo(string dir)
        //{

        //    string CleanPath(string path) => path.Substring(dir.Length).TrimStart('/', '\\');

        //    // replace this with a hashing method if you need
        //    long GetUnique(string path) => new FileInfo(path).LastWriteTime.Ticks;

        //    //get all directories in specified path  , "*.*", SearchOption.AllDirectories , SearchOption.AllDirectories
        //    var directories = EnumerateFiles(dir, "*", System.IO.SearchOption.TopDirectoryOnly); // Directory.GetDirectories(dir, "*", SearchOption.AllDirectories).Where(d => d != dir + @"\DfsrPrivate");


        //    var dirHash = new HashSet<string> { ".pdf", ".doc", ".txt" };

        //    var fileHash = Directory.EnumerateFiles(dir, "*.*")
        //                               .Where(f => !new FileInfo(f).Attributes.HasFlag(FileAttributes.System) &&
        //                                           dirHash.Contains(Path.GetExtension(f))).ToHashSet();


        //    //Strip out any DFS Folders from the array
        //    //directories = directories.Where(o => o != dir + @"\DfsrPrivate").ToArray();


        //    //var dirHash = directories.Select(CleanPath).ToHashSet();

        //    //// this could be paralleled if need be (if using a hash) 
        //    //var fileHash = directories.SelectMany(Directory.EnumerateFiles)
        //    //   .Select(file => (name: CleanPath(file), ticks: GetUnique(file)))
        //    //   .ToHashSet();






        //    return (dirHash, fileHash);


        //}



        // Enumerate all directories in DFS path, and skip any dfsr private folders
        public static IEnumerable<string> EnumerateFiles(string searchDirectory, string searchPattern, System.IO.SearchOption searchOpt)
        {
            try
            {
                var directories = Enumerable.Empty<string>();
                if (searchOpt == System.IO.SearchOption.TopDirectoryOnly)
                {
                    directories = Directory.EnumerateDirectories(searchDirectory)
                        .SelectMany(x => EnumerateDirectories(x, searchPattern, searchOpt));
                }
                return directories.Concat(Directory.EnumerateFiles(searchDirectory, searchPattern));
            }
            catch (UnauthorizedAccessException ex)
            {
                return Enumerable.Empty<string>();
            }
        }



        private bool CheckSerialDBCheck(string serialNo)
        {
            try
            {


                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);
                //vm.TestedDriveCollection.Clear();
                //vm.SearchCollection.Clear();
                string SerialCheck = "";

                //GET DATE VALUES SELECTED ON THE DATEPICKERS
                var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = "SELECT * FROM DriveInfo WHERE SerialNo = '" + serialNo + "'";

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    NetsuiteInventory itm = new NetsuiteInventory();
                    itm.ProcessedDate = sqlite_datareader.GetDateTime(1);
                    itm.PONumber = sqlite_datareader.GetString(2);
                    itm.LineType = sqlite_datareader.GetString(3);
                    itm.ProductCode = sqlite_datareader.GetString(4);
                    itm.ProductDescription = sqlite_datareader.GetString(5);
                    itm.Quantity = sqlite_datareader.GetString(6);
                    itm.SerialNumber = sqlite_datareader.GetString(7);
                    itm.POLineUnitPrice = sqlite_datareader.GetString(8);
                    itm.POUnitAmount = sqlite_datareader.GetString(9);
                    itm.ReceivedByOperative = sqlite_datareader.GetString(10);
                    itm.TestedByOperative = sqlite_datareader.GetString(11);
                    itm.WipeSoftware = sqlite_datareader.GetString(12);
                    itm.DriveResult = sqlite_datareader.GetString(13);
                    itm.DriveFailedReason = sqlite_datareader.GetString(14);
                    itm.DriveFailedReasonSpecify = sqlite_datareader.GetString(15);
                    if (sqlite_datareader.GetString(16) != null)
                    {
                        // itm.PartCodeRequired = sqlite_datareader.GetBoolean(14);
                        itm.PartCodeRequired = sqlite_datareader.GetString(16);
                    }

                    itm.ReportPath = sqlite_datareader.GetString(17);
                    itm.ITADRef = sqlite_datareader.GetString(18);


                    SerialCheck = itm.SerialNumber;


                    //vm.TestedDriveCollection.Add(itm);
                    //vm.SearchCollection.Add(itm);

                    // foreach()

                    // string myreader = sqlite_datareader.GetString(0);
                    //  MessageBox.Show(sqlite_datareader.GetString(1).ToString());
                }

                //Close Db
                sqlite_conn.Close();


                if (SerialCheck == serialNo)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return false;
            }
        }



        //Returns a string from DSO Index. Use in conjunction with ReportLinking 
        public string DSOIndexerFileSearch(string fileServer, string directoryPath, string serial2search, string fileEXT)
        {


            string connectionString = "Provider=Search.CollatorDSO;Extended Properties=\"Application=Windows\"";
            OleDbConnection connection = new OleDbConnection(connectionString);
            // DSOIndexerFileSearch("ukhafs", @"\\ukhafs.pinnacle.local\UK_Warehouse$\Testing\Drive Test\Wipe Drive Test Reports\DropFolder", "Serial Number", "*.pdf"
            //LOCAL
            //string query = @"SELECT System.ItemName FROM SystemIndex WHERE DIRECTORY ='file://C:/WinPEx64'";

            //*** WORKS FOR CONTAINS FILENAME ***
            //string query = @"SELECT System.FileName FROM ukhafs.SystemIndex WHERE SCOPE ='file:\\ukhafs.pinnacle.local\UK_Warehouse$\Testing\Drive Test\Wipe Drive Test Reports\DropFolder' AND CONTAINS(System.FileName,'0000090776') AND CONTAINS(System.FileName,'*.pdf')";
            string query = @"SELECT System.FileName, System.ItemPathDisplay FROM " + fileServer + ".SystemIndex WHERE DIRECTORY ='file:" + directoryPath + "' AND CONTAINS(System.FileName,'" + serial2search + "-') AND CONTAINS(System.FileName,'" + serial2search + "') AND CONTAINS(System.FileName,'" + fileEXT + "')";

            OleDbCommand command = new OleDbCommand(query, connection);
            //Doesn't work with that provider
            //command.Parameters.Add(new OleDbParameter("System.FileName", "Nebula"));

            //Open Connection
            connection.Open();

            //await PutTaskDelay(10000);

            //OleDB Reader instance
            OleDbDataReader reader;

            //List<string> result = new List<string>();
            string result = "";

            int reportCount = 0;

            try
            {
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    try
                    {
                        //result.Add(reader.GetString(1));
                        result = reader.GetString(1);
                        //Console.WriteLine(reader.GetString(0));
                        Console.WriteLine(reader.GetString(1));

                        //Console.WriteLine(reader.GetString(1));
                        reportCount += 1;
                    }
                    catch (Exception ex)
                    {
                        if (ex.HResult == -2147467259)
                        {
                            Console.WriteLine(ex.Message);
                            return result;
                        }

                    }
                }



                return result;

            }
            catch (Exception ex)
            {
                //empty result set on execution of reader results in following error now.
                if (ex.HResult == -2147467259)
                {
                    Console.WriteLine(ex.Message);
                    return result;
                }


            }


            //Close connection
            connection.Close();



            Console.WriteLine("Finished");

            return result;


        }

        //Return a list from the DSO Index, to check index coverage 
        public void DSOIndexerFilesSearch(string fileServer, string directoryPath, string serial2search, string fileEXT)
        {


            string connectionString = "Provider=Search.CollatorDSO;Extended Properties=\"Application=Windows\"";
            OleDbConnection connection = new OleDbConnection(connectionString);
            // DSOIndexerFileSearch("ukhafs", @"\\ukhafs.pinnacle.local\UK_Warehouse$\Testing\Drive Test\Wipe Drive Test Reports\DropFolder", "Serial Number", "*.pdf"
            //LOCAL
            //string query = @"SELECT System.ItemName FROM SystemIndex WHERE DIRECTORY ='file://C:/WinPEx64'";

            //*** WORKS FOR CONTAINS FILENAME ***
            //string query = @"SELECT System.FileName FROM ukhafs.SystemIndex WHERE SCOPE ='file:\\ukhafs.pinnacle.local\UK_Warehouse$\Testing\Drive Test\Wipe Drive Test Reports\DropFolder' AND CONTAINS(System.FileName,'0000090776') AND CONTAINS(System.FileName,'" + serial2search + "') AND CONTAINS(System.FileName,'*.pdf')";
            string query = @"SELECT System.FileName, System.ItemPathDisplay FROM " + fileServer + ".SystemIndex WHERE DIRECTORY ='file:" + directoryPath + "' AND CONTAINS(System.FileName,'" + serial2search.ToLower() + "')";

            OleDbCommand command = new OleDbCommand(query, connection);
            //Doesn't work with that provider
            //command.Parameters.Add(new OleDbParameter("System.FileName", "Nebula"));

            //Open Connection
            connection.Open();

            //await PutTaskDelay(10000);

            //OleDB Reader instance
            OleDbDataReader reader;


            //Create List
            List<string> result = new List<string>();


            try
            {
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    //try
                    //{
                    result.Add(reader.GetString(1));
                    //result = reader.GetString(1);
                    Console.WriteLine(reader.GetString(0));
                    Console.WriteLine(reader.GetString(1));

                    //Console.WriteLine(reader.GetString(1));
                    //}
                    //catch (Exception ex)
                    //{
                    //    if (ex.HResult == -2147467259)
                    //    {
                    //        Console.WriteLine(ex.Message);
                    //        //return result;
                    //    }

                    //}
                    //Console.WriteLine("\n" + result.Count().ToString());
                }

                Console.WriteLine("\n" + result.Count().ToString());
                //return result;

            }
            catch (Exception ex)
            {
                //empty result set on execution of reader results in following error now.
                if (ex.HResult == -2147467259)
                {
                    Console.WriteLine(ex.Message);
                    //return result;
                }


            }


            //Close connection
            connection.Close();



            Console.WriteLine("Finished");

            //return result;


        }

        public List<string> RecursiveFileSearch(string directoryPath, DateTime fromDate, DateTime toDate, string fileEXT)
        {
            try
            {



                //Directory Info 
                DirectoryInfo directory = new DirectoryInfo(directoryPath);

                //Change to Specific Dates
                string str = fromDate.ToShortDateString();

                //if (StaticFunctions.UserLocation == "United States")
                //{
                //    fromDate = DateTime.Parse(str, new CultureInfo("en-US"));
                //}
                //else if (StaticFunctions.UserLocation == "France")
                //{
                //    fromDate = DateTime.Parse(str, new CultureInfo("fr-FR"));

                //}
                //else
                //{
                //    fromDate = DateTime.Parse(str, new CultureInfo("en-GB"));
                //}




                // Add Message with date range and path
                vm.ProcessedMessageColour = Brushes.Green;
                vm.ProcessedMessage = "Date Range: " + fromDate.AddDays(-3).ToShortDateString() + " To " + fromDate.AddDays(Convert.ToDouble(vm.SpanDays)).ToShortDateString() + " Path: " + directoryPath + "";


                return directory.GetFiles("*.pdf", System.IO.SearchOption.TopDirectoryOnly).AsParallel()
                                  .Where(file => file.LastWriteTime >= fromDate.AddDays(-3) && file.LastWriteTime <= fromDate.AddDays(Convert.ToDouble(vm.SpanDays)))
                                  .Select(f => f.FullName)
                                  .ToList();



            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return null;
            }

        }

        public List<string> CedarRecursiveFileSearch(string directoryPath, DateTime fromDate, DateTime toDate, string fileEXT)
        {
            try
            {



                //Directory Info 
                DirectoryInfo directory = new DirectoryInfo(directoryPath);

                //Change to Specific Dates
                string str = fromDate.ToShortDateString();


                // Add Message with date range and path
                vm.ProcessedMessageColour = Brushes.Green;
                vm.ProcessedMessage = "Finding Reports: " + fromDate.AddDays(0).ToShortDateString() + " To " + toDate.ToShortDateString() + " Path: " + directoryPath + "";


                return directory.GetFiles("*.pdf", System.IO.SearchOption.TopDirectoryOnly).AsParallel()
                                  .Where(file => file.LastWriteTime >= fromDate.AddDays(0) && file.LastWriteTime <= toDate)
                                  .Select(f => f.LastWriteTime + "," + f.FullName)
                                  .ToList();



            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return null;
            }

        }




        //USES Number of files to return
        public List<string> RecursiveFileSearch2(string directoryPath, DateTime fromDate, DateTime toDate, string fileEXT, int numberOfFilesToReturn, ListView myLV)
        {
            try
            {

                //string foundFile;
                List<string> FileCollection = new List<string>();
                //ParallelQuery<string> pqs;

                var dirInfo = new DirectoryInfo(directoryPath);
                IEnumerable<FileInfo> Files = dirInfo.EnumerateFiles("*.pdf", System.IO.SearchOption.TopDirectoryOnly);
                //var firstFiles = dirInfo.EnumerateFiles("*.pdf", System.IO.SearchOption.TopDirectoryOnly).AsParallel().Take(numberOfFilesToReturn).Where(myfile => myfile.LastWriteTime >= fromDate && myfile.LastWriteTime <= toDate).ToList();
                //return firstFiles.Select(l => l.FullName).ToList();


                //foreach (NetsuiteInventory itm in myLV.Items)
                //{

                foreach (FileInfo file in Files)
                {
                    //if (file.FullName.Contains(itm.SerialNumber))
                    //{
                    FileCollection.Add(file.FullName);
                    Console.WriteLine(file.FullName);
                    //}
                }
                ////MessageBox.Show(itm.SerialNumber);
                //var firstFiles = dirInfo.EnumerateFiles("*.pdf", System.IO.SearchOption.TopDirectoryOnly).AsParallel().Where(myfile => myfile.FullName.Contains(itm.SerialNumber));
                //FileCollection = firstFiles.Select(l => l.FullName).ToList();

                //MessageBox.Show(foundFile);
                // MessageBox.Show(itm.SerialNumber);
                //}




                //foreach (NetsuiteInventory itm in myLV.Items)
                //{


                //   //MessageBox.Show(itm.SerialNumber);
                //   var firstFiles = dirInfo.EnumerateFiles("*.pdf", System.IO.SearchOption.TopDirectoryOnly).AsParallel().Where(myfile => myfile.FullName.Contains(itm.SerialNumber));
                //   FileCollection = firstFiles.Select(l => l.FullName).ToList();
                //    //FileCollection.Add(foundFile);
                //    //MessageBox.Show(foundFile);
                //    MessageBox.Show(itm.SerialNumber);
                //}

                return FileCollection;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return null;
            }

        }

        public List<string> RecursiveFileSearch3(string directoryPath, DateTime fromDate, DateTime toDate, string fileEXT)
        {
            try
            {


                List<string> fileCollection = new List<string>();

                //Directory Info 
                DirectoryInfo directory = new DirectoryInfo(directoryPath);


                string query = @"SELECT Top 5 System.ItemPathDisplay FROM SYSTEMINDEX
                WHERE scope='file:'
                ORDER BY System.Size DESC";

                using (OleDbConnection objConnection =
                    new OleDbConnection
                    ("Provider=Search.CollatorDSO.1;Extended?Properties='Application=Windows';"))
                {
                    objConnection.Open();
                    OleDbCommand cmd = new OleDbCommand(query, objConnection);
                    using (OleDbDataReader rdr = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < rdr.FieldCount; i++)
                        {
                            Console.Write(rdr.GetName(i));
                            Console.Write('\t');
                        }
                        while (rdr.Read())
                        {
                            Console.WriteLine();
                            for (int i = 0; i < rdr.FieldCount; i++)
                            {
                                Console.Write(rdr[i]);
                                Console.Write('\t');
                            }
                        }
                        //  Console.ReadKey();
                    }
                }

                //var connection = new OleDbConnection(@"Provider=Search.CollatorDSO;Extended Properties=""Application=Windows""");

                //// File name search (case insensitive), also searches sub directories //ukhafs.pinnacle.local/UK_Warehouse$/Testing/Drive Test/Wipe Drive Test Reports/DropFolder
                //var query1 = @"SELECT System.ItemName FROM SystemIndex " +
                //            @"WHERE scope ='file:C:/' AND System.ItemName LIKE '%Month%'";

                //// File name search (case insensitive), does not search sub directories
                //var query2 = @"SELECT System.ItemName FROM SystemIndex " +
                //            @"WHERE directory = 'file:C:/' AND System.ItemName LIKE '%Test%' ";

                //// Folder name search (case insensitive)
                //var query3 = @"SELECT System.ItemName FROM SystemIndex " +
                //            @"WHERE scope = 'file:C:/' AND System.ItemType = 'Directory' AND System.Itemname LIKE '%Test%' ";

                //// Folder name search (case insensitive), does not search sub directories
                //var query4 = @"SELECT System.ItemName FROM SystemIndex " +
                //            @"WHERE directory = 'file:C:/' AND System.ItemType = 'Directory' AND System.Itemname LIKE '%Test%' ";

                //connection.Open();

                //var command = new OleDbCommand(query1, connection);

                //using (var r = command.ExecuteReader())
                //{
                //    while (r.Read())
                //    {
                //        Console.WriteLine(r[0]);
                //        fileCollection.Add(r[0].ToString());

                //    }
                //}

                //connection.Close();


                return fileCollection;


                //return directory.GetFiles("*.pdf", System.IO.SearchOption.TopDirectoryOnly)
                //                      .Where(file => file.LastWriteTime >= fromDate && file.LastWriteTime <= toDate)  //  .Where(file => file.CreationTime >= fromDate && file.CreationTime <= toDate)
                //                      .Select(f => f.FullName)
                //                      .ToList();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return null;
            }
        }




        //Create Connection to DB
        static SQLiteConnection CreateConnection(string pathtodb)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            //sqlite_conn = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            //sqlite_conn = new SQLiteConnection("Data Source=" + pathtodb + "; Version = 3; New = True; Compress = True; ");
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                // MessageBox.Show(ex.Message);
            }
            return sqlite_conn;
        }

        //Insert Into Table
        public void InsertTransactionData()
        {

            try
            {

                //Connection to SQL Lite DB
                //if all criteria met create the job
                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);


                List<string> myQueries = new List<string>();
                List<SQLiteCommand> mySQLCommandsCollection = new List<SQLiteCommand>();

                //LOOP THROUGH ADDED COLLECTION
                foreach (NetsuiteInventory itm in vm.ScannedSerialCollection)
                {


                    //Add query to list
                    myQueries.Add("INSERT INTO DriveInfo (Date, PONumber, POLineType, ProductCode, ProductDescription, Quantity, SerialNo, UnitPrice, Amount, RecOperative, TestOperative, WipeSoftware, DriveResult, DriveFailiure, DriveFailiureSpecify, PartCodeRequired, ReportPath, ITADRef)" + " VALUES('" + DateTime.Now.ToString("yyyy-MM-dd") + "','" + itm.PONumber + "','" + itm.LineType + "','" + itm.ProductCode + "','" + itm.ProductDescription + "','" + itm.POQuantity + "','" + itm.SerialNumber + "','" + itm.POLineUnitPrice + "','" + itm.POUnitAmount + "','" + itm.ReceivedByOperative + "','" + itm.TestedByOperative + "','" + itm.WipeSoftware + "','" + itm.DriveResult + "','" + itm.DriveFailedReason + "','" + itm.DriveFailedReasonSpecify + "','" + itm.PartCodeRequired + "','" + itm.ReportPath + "','" + itm.ITADRef + "')");


                }



                //Create a transaction
                using (var tra = sqlite_conn.BeginTransaction())
                {
                    try
                    {
                        //Console.WriteLine(myQueries.Count().ToString() + "\n");

                        foreach (var myQuery in myQueries)
                        {
                            //Console.WriteLine(myQuery.ToString());

                            using (var cd = new SQLiteCommand(myQuery, sqlite_conn, tra))
                            {
                                cd.ExecuteNonQuery();
                            }
                        }

                        tra.Commit();
                    }
                    catch (Exception ex)
                    {
                        tra.Rollback();
                        //Console.Error.Writeline("I did nothing, because something wrong happened: {0}", ex);
                        throw;
                    }
                }


                //Close DB
                sqlite_conn.Close();


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        // OLD METHOD Insert Into Table
        //public void InsertData()
        //{

        //    try
        //    {

        //    //Connection to SQL Lite DB
        //    //if all criteria met create the job
        //    SQLiteConnection conn;
        //    conn = CreateConnection(cs);

        //    SQLiteCommand sqlite_cmd;
        //    sqlite_cmd = conn.CreateCommand();
        //    //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
        //    sqlite_cmd.CommandText = "INSERT INTO DriveInfo (Date, PONumber, POLineType, ProductCode, ProductDescription, Quantity, SerialNo, UnitPrice, Amount, RecOperative, TestOperative, WipeSoftware, DriveResult, DriveFailiure, DriveFailiureSpecify, PartCodeRequired, ReportPath, ITADRef)" + " VALUES(@date,@ponumber,@polinetype,@productcode,@productdescription,@quantity,@serialno,@unitprice,@amount,@recoperative,@testoperative,@wipesoftware,@driveresult,@drivefailiure,@drivefailiurespecify,@partcoderequired,@reportpath,@itadref)";
        //    sqlite_cmd.CommandType = CommandType.Text;

        //    //LOOP THROUGH ADDED COLLECTION
        //    foreach (NetsuiteInventory itm in vm.ScannedSerialCollection)
        //    {

        //        //Date must be in this format to work with SQL Lite
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@date", DateTime.Now.ToString("yyyy-MM-dd")));
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@ponumber", itm.PONumber));
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@polinetype", itm.LineType));
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@productcode", itm.ProductCode));
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@productdescription", itm.ProductDescription));
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@quantity", itm.POQuantity));
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", itm.SerialNumber));
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@unitprice", itm.POLineUnitPrice));
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@amount", itm.POUnitAmount));
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@recoperative", itm.ReceivedByOperative));
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@testoperative", itm.TestedByOperative));
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@wipesoftware", itm.WipeSoftware)); // added later
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", itm.DriveResult)); //added later
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", itm.DriveFailedReason)); //added later
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiurespecify", itm.DriveFailedReasonSpecify)); //added later
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@partcoderequired", itm.PartCodeRequired));
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportpath", itm.ReportPath)); //added later
        //        sqlite_cmd.Parameters.Add(new SQLiteParameter("@itadref", itm.ITADRef)); //added later
        //        sqlite_cmd.ExecuteNonQuery();


        //    }

        //    //Close DB
        //    conn.Close();

        //    }
        //    catch (Exception ex)
        //    {

        //        MessageBox.Show(ex.Message);
        //    }

        //}






        public async void ReadAllData()
        {
            try
            {

                //Connection to SQL Lite DB
                //if all criteria met create the job
                SQLiteConnection conn;
                conn = CreateConnection(cs);

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = "SELECT * FROM DriveInfo";

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    NetsuiteInventory itm = new NetsuiteInventory();
                    if (sqlite_datareader.GetString(1) != null)
                        itm.ProcessedDate = sqlite_datareader.GetDateTime(1);
                    if (sqlite_datareader.GetString(2) != null)
                        itm.PONumber = sqlite_datareader.GetString(2);
                    if (sqlite_datareader.GetString(3) != null)
                        itm.LineType = sqlite_datareader.GetString(3);
                    if (sqlite_datareader.GetString(4) != null)
                        itm.ProductCode = sqlite_datareader.GetString(4);
                    if (sqlite_datareader.GetString(5) != null)
                        itm.ProductDescription = sqlite_datareader.GetString(5);
                    if (sqlite_datareader.GetString(6) != null)
                        itm.Quantity = sqlite_datareader.GetString(6);
                    if (sqlite_datareader.GetString(7) != null)
                        itm.SerialNumber = sqlite_datareader.GetString(7);
                    if (sqlite_datareader.GetString(8) != null)
                        itm.POLineUnitPrice = sqlite_datareader.GetString(8);
                    if (sqlite_datareader.GetString(9) != null)
                        itm.POUnitAmount = sqlite_datareader.GetString(9);
                    if (sqlite_datareader.GetString(10) != null)
                        itm.ReceivedByOperative = sqlite_datareader.GetString(10);
                    if (sqlite_datareader.GetString(11) != null)
                        itm.TestedByOperative = sqlite_datareader.GetString(11);
                    if (sqlite_datareader.GetString(12) != null)
                        itm.WipeSoftware = sqlite_datareader.GetString(12);
                    if (sqlite_datareader.GetString(13) != null)
                        itm.DriveResult = sqlite_datareader.GetString(13);
                    if (sqlite_datareader.GetString(14) != null)
                        itm.DriveFailedReason = sqlite_datareader.GetString(14);
                    if (sqlite_datareader.GetString(15) != null)
                        itm.DriveFailedReasonSpecify = sqlite_datareader.GetString(15);

                    if (sqlite_datareader.GetString(16) != null)
                    {
                        // itm.PartCodeRequired = sqlite_datareader.GetBoolean(14);
                        itm.PartCodeRequired = sqlite_datareader.GetString(16);
                    }

                    if (sqlite_datareader.GetString(17) != null)
                        itm.ReportPath = sqlite_datareader.GetString(17);
                    if (sqlite_datareader.GetString(18) != null)
                        itm.ITADRef = sqlite_datareader.GetString(18);

                    //Update Collection via dispatcher
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        vm.TestedDriveCollection.Add(itm);
                        vm.SearchCollection.Add(itm);
                    });





                }

                await PutTaskDelay(400);
                //Close DB
                conn.Close();


                CalculateTotals();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }





        public async Task ReadData(string sqlquery)
        {
            try
            {
                //Connection to SQL Lite DB
                //if all criteria met create the job
                SQLiteConnection conn;
                conn = CreateConnection(cs);

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    NetsuiteInventory itm = new NetsuiteInventory();
                    if (sqlite_datareader.GetString(1) != null)
                        itm.ProcessedDate = sqlite_datareader.GetDateTime(1);
                    if (sqlite_datareader.GetString(2) != null)
                        itm.PONumber = sqlite_datareader.GetString(2);
                    if (sqlite_datareader.GetString(3) != null)
                        itm.LineType = sqlite_datareader.GetString(3);
                    if (sqlite_datareader.GetString(4) != null)
                        itm.ProductCode = sqlite_datareader.GetString(4);
                    if (sqlite_datareader.GetString(5) != null)
                        itm.ProductDescription = sqlite_datareader.GetString(5);
                    if (sqlite_datareader.GetString(6) != null)
                        itm.Quantity = sqlite_datareader.GetString(6);
                    if (sqlite_datareader.GetString(7) != null)
                        itm.SerialNumber = sqlite_datareader.GetString(7);
                    if (sqlite_datareader.GetString(8) != null)
                        itm.POLineUnitPrice = sqlite_datareader.GetString(8);
                    if (sqlite_datareader.GetString(9) != null)
                        itm.POUnitAmount = sqlite_datareader.GetString(9);
                    if (sqlite_datareader.GetString(10) != null)
                        itm.ReceivedByOperative = sqlite_datareader.GetString(10);
                    if (sqlite_datareader.GetString(11) != null)
                        itm.TestedByOperative = sqlite_datareader.GetString(11);
                    if (sqlite_datareader.GetString(12) != null)
                        itm.WipeSoftware = sqlite_datareader.GetString(12);
                    if (sqlite_datareader.GetString(13) != null)
                        itm.DriveResult = sqlite_datareader.GetString(13);
                    if (sqlite_datareader.GetString(14) != null)
                        itm.DriveFailedReason = sqlite_datareader.GetString(14);
                    if (sqlite_datareader.GetString(15) != null)
                        itm.DriveFailedReasonSpecify = sqlite_datareader.GetString(15);

                    if (sqlite_datareader.GetString(16) != null)
                    {
                        // itm.PartCodeRequired = sqlite_datareader.GetBoolean(14);
                        itm.PartCodeRequired = sqlite_datareader.GetString(16);
                    }

                    if (sqlite_datareader.GetString(17) != null)
                        itm.ReportPath = sqlite_datareader.GetString(17);
                    if (sqlite_datareader.GetString(18) != null)
                        itm.ITADRef = sqlite_datareader.GetString(18);



                    //Update Collection via dispatcher
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        vm.SearchCollection.Add(itm);
                        //Count Total
                        vm.SearchCount = vm.SearchCollection.Count.ToString();
                        vm.PassCount = vm.SearchCollection.Where(f => f.DriveResult == "PASS").Count().ToString();
                        vm.FailCount = vm.SearchCollection.Where(f => f.DriveResult == "FAIL").Count().ToString();
                        vm.PendingCount = vm.SearchCollection.Where(f => f.DriveResult == "PENDING").Count().ToString();
                    });


                }



                //Console.WriteLine("Query Finished");
                //return null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //return null;

            }

            //return null;
        }

        public async Task ReadBackgroundData(string sqlquery)
        {
            try
            {
                //Connection to SQL Lite DB
                //if all criteria met create the job
                SQLiteConnection conn;
                conn = CreateConnection(cs);

                //Declare observable collection

                //ObservableCollection<NetsuiteInventory> obs = new ObservableCollection<NetsuiteInventory>();

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();


                //Create Flow Doc

                //using (conn)
                //{
                //Loop through results set
                while (sqlite_datareader.Read())
                {


                    NetsuiteInventory itm = new NetsuiteInventory();
                    if (sqlite_datareader.GetString(1) != null)
                        itm.ProcessedDate = sqlite_datareader.GetDateTime(1);
                    if (sqlite_datareader.GetString(2) != null)
                        itm.PONumber = sqlite_datareader.GetString(2);
                    if (sqlite_datareader.GetString(3) != null)
                        itm.LineType = sqlite_datareader.GetString(3);
                    if (sqlite_datareader.GetString(4) != null)
                        itm.ProductCode = sqlite_datareader.GetString(4);
                    if (sqlite_datareader.GetString(5) != null)
                        itm.ProductDescription = sqlite_datareader.GetString(5);
                    if (sqlite_datareader.GetString(6) != null)
                        itm.Quantity = sqlite_datareader.GetString(6);
                    if (sqlite_datareader.GetString(7) != null)
                        itm.SerialNumber = sqlite_datareader.GetString(7);
                    if (sqlite_datareader.GetString(8) != null)
                        itm.POLineUnitPrice = sqlite_datareader.GetString(8);
                    if (sqlite_datareader.GetString(9) != null)
                        itm.POUnitAmount = sqlite_datareader.GetString(9);
                    if (sqlite_datareader.GetString(10) != null)
                        itm.ReceivedByOperative = sqlite_datareader.GetString(10);
                    if (sqlite_datareader.GetString(11) != null)
                        itm.TestedByOperative = sqlite_datareader.GetString(11);
                    if (sqlite_datareader.GetString(12) != null)
                        itm.WipeSoftware = sqlite_datareader.GetString(12);
                    if (sqlite_datareader.GetString(13) != null)
                        itm.DriveResult = sqlite_datareader.GetString(13);
                    if (sqlite_datareader.GetString(14) != null)
                        itm.DriveFailedReason = sqlite_datareader.GetString(14);
                    if (sqlite_datareader.GetString(15) != null)
                        itm.DriveFailedReasonSpecify = sqlite_datareader.GetString(15);

                    if (sqlite_datareader.GetString(16) != null)
                    {
                        // itm.PartCodeRequired = sqlite_datareader.GetBoolean(14);
                        itm.PartCodeRequired = sqlite_datareader.GetString(16);
                    }

                    if (sqlite_datareader.GetString(17) != null)
                        itm.ReportPath = sqlite_datareader.GetString(17);
                    if (sqlite_datareader.GetString(18) != null)
                        itm.ITADRef = sqlite_datareader.GetString(18);



                    //Update Collection via dispatcher
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        vm.ReportCollection.Add(itm);
                        vm.SearchCollection.Add(itm);
                        //Count Total
                        vm.SearchCount = vm.ReportCollection.Count.ToString();
                        vm.PassCount = vm.ReportCollection.Where(f => f.DriveResult == "PASS").Count().ToString();
                        vm.FailCount = vm.ReportCollection.Where(f => f.DriveResult == "FAIL").Count().ToString();
                        vm.PendingCount = vm.ReportCollection.Where(f => f.DriveResult == "PENDING").Count().ToString();
                    });


                }

                //}


                conn.Close();
                //Console.WriteLine("Query Finished");
                //return null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);


                //return null;

            }

            //return null;
        }

        public void AddToComparisonList()
        {
            int ScannedItems = 0;

            //Clear Collection need to use dispatcher thread
            App.Current.Dispatcher.Invoke((System.Action)delegate
            {
                vm.NetsuiteScannedCollection.Clear();
            });


            //var uiContext = SynchronizationContext.Current;

            foreach (NetsuiteInventory itm in vm.ScannedSerialCollection)
            {


                //Count Quantity in scanned collection 
                ScannedItems = vm.ScannedSerialCollection.Where(item => item.ProductCode == itm.ProductCode).Count();


                //Check if product code already exists, create new object to add to the comparison lines
                if (!vm.NetsuiteScannedCollection.Any(p => p.ProductCode == itm.ProductCode))
                {

                    NetsuiteInventory nsi = new NetsuiteInventory();
                    nsi.ProductCode = itm.ProductCode;
                    nsi.ProductDescription = itm.ProductDescription;
                    nsi.LineType = itm.LineType;
                    nsi.Quantity = ScannedItems.ToString();
                    //Use this if the collection cannot be updated other than the UI Thread
                    App.Current.Dispatcher.Invoke((System.Action)delegate
                    {
                        vm.NetsuiteScannedCollection.Add(nsi);
                    });


                }


            }
        }



        public void ReadTestData(SQLiteConnection conn, string sqlquery)
        {
            try
            {

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                //Clear Collection before repopulating
                //vm.TestedDriveCollection.Clear();

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {
                    //sqlite_cmd.Parameters.Clear();

                    NetsuiteInventory itm = new NetsuiteInventory();

                    if (sqlite_datareader.GetDateTime(1) != null)
                        itm.ProcessedDate = sqlite_datareader.GetDateTime(1);
                    if (sqlite_datareader.GetString(2) != null)
                        itm.PONumber = sqlite_datareader.GetString(2);
                    if (sqlite_datareader.GetString(3) != null)
                        itm.LineType = sqlite_datareader.GetString(3);
                    if (sqlite_datareader.GetString(4) != null)
                        itm.ProductCode = sqlite_datareader.GetString(4);
                    if (sqlite_datareader.GetString(5) != null)
                        itm.ProductDescription = sqlite_datareader.GetString(5);
                    if (sqlite_datareader.GetString(6) != null)
                        itm.Quantity = sqlite_datareader.GetString(6);
                    if (sqlite_datareader.GetString(7) != null)
                        itm.SerialNumber = sqlite_datareader.GetString(7);
                    if (sqlite_datareader.GetString(8) != null)
                        itm.POLineUnitPrice = sqlite_datareader.GetString(8);
                    if (sqlite_datareader.GetString(9) != null)
                        itm.POUnitAmount = sqlite_datareader.GetString(9);
                    if (sqlite_datareader.GetString(10) != null)
                        itm.ReceivedByOperative = sqlite_datareader.GetString(10);
                    if (sqlite_datareader.GetString(11) != null)
                        itm.TestedByOperative = sqlite_datareader.GetString(11);
                    if (sqlite_datareader.GetString(12) != null)
                        itm.WipeSoftware = sqlite_datareader.GetString(12);
                    if (sqlite_datareader.GetString(13) != null)
                        itm.DriveResult = sqlite_datareader.GetString(13);
                    if (sqlite_datareader.GetString(14) != null)
                        itm.DriveFailedReason = sqlite_datareader.GetString(14);
                    if (sqlite_datareader.GetString(15) != null)
                        itm.DriveFailedReasonSpecify = sqlite_datareader.GetString(15);
                    // Console.WriteLine(sqlite_datareader.GetString(15));
                    if (sqlite_datareader.GetString(16) != null)
                    {
                        // itm.PartCodeRequired = sqlite_datareader.GetBoolean(14);
                        itm.PartCodeRequired = sqlite_datareader.GetString(16);
                    }

                    if (sqlite_datareader.GetString(17) != null)
                        itm.ReportPath = sqlite_datareader.GetString(17);
                    if (sqlite_datareader.GetString(18) != null)
                        itm.ITADRef = sqlite_datareader.GetString(18);

                    //Update Collection via dispatcher
                    App.Current.Dispatcher.Invoke((System.Action)delegate
                    {
                        //vm.ProcessedMessage = "Please Wait...";
                        vm.TestedDriveCollection.Add(itm);
                    });


                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void DeleteData(SQLiteConnection conn)
        {
            try
            {

                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "DELETE FROM DriveInfo WHERE SerialNo = @serialno;";
                sqlite_cmd.CommandType = CommandType.Text;
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", vm.TestResultsLVSelectedItem.SerialNumber));

                sqlite_cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        public void UpdateData()
        {
            try
            {

                //Connection to SQL Lite DB
                //if all criteria met create the job
                SQLiteConnection conn;
                conn = CreateConnection(cs);


                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                //sqlite_cmd.CommandText = "UPDATE Jobs SET Technician = 'Nick', Status = 'Complete' WHERE id = '1'; ";
                sqlite_cmd.CommandText = "UPDATE DriveInfo SET SerialNo = @serialno,TestOperative = @testoperative, WipeSoftware = @wipesoftware, DriveResult = @driveresult, DriveFailiure = @drivefailiure, DriveFailiureSpecify = @drivefailiurespecify, ReportPath = @reportpath WHERE SerialNo = @previousserialno";

                //MessageBox.Show(currentSerial);
                //MessageBox.Show(vm.PreviousSerialNumber);


                sqlite_cmd.CommandType = CommandType.Text;
                //sqlite_cmd.Parameters.Add(new SQLiteParameter("@id", vm.TestResultsLVSelectedItem.InternalID));
                //sqlite_cmd.Parameters.Add(new SQLiteParameter("@day", DateTime.Now));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@previousserialno", vm.PreviousSerialNumber));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", vm.TestResultsLVSelectedItem.SerialNumber));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@testoperative", vm.CurrentUserInitials.Replace("Checked By:", "")));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@wipesoftware", vm.TestResultsLVSelectedItem.WipeSoftware));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", vm.TestResultsLVSelectedItem.DriveResult));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", vm.TestResultsLVSelectedItem.DriveFailedReason));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiurespecify", vm.TestResultsLVSelectedItem.DriveFailedReasonSpecify));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportpath", vm.TestResultsLVSelectedItem.ReportPath));
                sqlite_cmd.ExecuteNonQuery();

                conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        //Serial cannot be edited
        public void MultiUpdateData(NetsuiteInventory nsiItem)
        {
            try
            {

                //Connection to SQL Lite DB
                //if all criteria met create the job
                SQLiteConnection conn;
                conn = CreateConnection(cs);


                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();

                sqlite_cmd.CommandText = "UPDATE DriveInfo SET SerialNo = @serialno,TestOperative = @testoperative, WipeSoftware = @wipesoftware, DriveResult = @driveresult, DriveFailiure = @drivefailiure, DriveFailiureSpecify = @drivefailiurespecify, ReportPath = @reportpath WHERE SerialNo = @serialno";

                //MessageBox.Show(currentSerial); previous
                //MessageBox.Show(vm.PreviousSerialNumber);


                sqlite_cmd.CommandType = CommandType.Text;
                //sqlite_cmd.Parameters.Add(new SQLiteParameter("@id", vm.TestResultsLVSelectedItem.InternalID));
                //sqlite_cmd.Parameters.Add(new SQLiteParameter("@day", DateTime.Now));

                //MessageBox.Show(vm.TestResultsLVSelectedItem.SerialNumber);

                //sqlite_cmd.Parameters.Add(new SQLiteParameter("@previousserialno", vm.PreviousSerialNumber));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", nsiItem.SerialNumber));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@testoperative", vm.CurrentUserInitials.Replace("Checked By:", "")));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@wipesoftware", nsiItem.WipeSoftware));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", nsiItem.DriveResult));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", nsiItem.DriveFailedReason));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiurespecify", nsiItem.DriveFailedReasonSpecify));
                //sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", vm.TestResultsLVSelectedItem.DriveFailedReason));
                //sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiurespecify", vm.TestResultsLVSelectedItem.DriveFailedReasonSpecify));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportpath", nsiItem.ReportPath));
                sqlite_cmd.ExecuteNonQuery();

                conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }



        private async Task FilterSearch(string searchFilter, string whichColumn)
        {
            try
            {


                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);

                //GET DATE VALUES SELECTED ON THE DATEPICKERS
                var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

                //check for empty search filter
                if (string.IsNullOrEmpty(searchFilter))
                {
                    //return all items in date range
                    //Get Results with filter
                    await Task.Run(() => ReadData("SELECT * FROM DriveInfo WHERE Date BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "' ORDER BY  DriveResult ASC"));

                }
                else
                {
                    //Get Results with filter
                    await Task.Run(() => ReadData("SELECT * FROM DriveInfo WHERE Date BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "' AND RecOperative = '" + searchFilter + "' OR SerialNo LIKE '" + searchFilter + "' OR PONumber LIKE '" + searchFilter + "' ORDER BY  DriveResult ASC"));
                }



                //Close Db
                sqlite_conn.Close();





            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private async Task FilterSearchMatch(string searchFilter, string whichColumn)
        {
            try
            {


                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);


                //GET DATE VALUES SELECTED ON THE DATEPICKERS
                var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                var dateTo = vm.DateTo.ToString("yyyy-MM-dd");


                //check for empty search filter
                if (string.IsNullOrEmpty(searchFilter))
                {
                    //return all items in date range
                    //Get Results with filter
                    //Set progress to active
                    await Task.Run(() => ReadData("SELECT * FROM DriveInfo WHERE Date BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "' ORDER BY  DriveResult ASC"));

                }
                else
                {
                    //Get Results with filter
                    await Task.Run(() => ReadData("SELECT * FROM DriveInfo WHERE Date BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "' AND RecOperative = '" + searchFilter + "' OR SerialNo LIKE '" + searchFilter + "' OR PONumber LIKE '" + searchFilter + "' ORDER BY  DriveResult ASC"));
                }





                //Close Db
                sqlite_conn.Close();


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private async Task FilterSearchTest(string searchFilter, string whichColumn)
        {
            try
            {

                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);
                //Clear Collection
                vm.TestedDriveCollection.Clear();

                //GET DATE VALUES SELECTED ON THE DATEPICKERS
                var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                var dateTo = vm.DateTo.ToString("yyyy-MM-dd");
                //Get Results
                await System.Threading.Tasks.Task.Run(() => ReadTestData(sqlite_conn, "SELECT * FROM DriveInfo WHERE PONumber = '" + searchFilter + "' OR SerialNo = '" + searchFilter + "'"));
                //await System.Threading.Tasks.Task.Run(() => ReadTestData(sqlite_conn, "SELECT PONumber FROM DriveInfo WHERE PONumber = '" + searchFilter + "' OR SerialNo = '" + searchFilter + "'"));
                //await System.Threading.Tasks.Task.Run(() => ReadTestData(sqlite_conn, "SELECT PONumber FROM DriveInfo"));
                //Count Total
                vm.TestCount = vm.TestedDriveCollection.Count.ToString();
                vm.PassCount = vm.TestedDriveCollection.Where(f => f.DriveResult == "PASS").Count().ToString();
                vm.FailCount = vm.TestedDriveCollection.Where(f => f.DriveResult == "FAIL").Count().ToString();
                vm.PendingCount = vm.TestedDriveCollection.Where(f => f.DriveResult == "PENDING").Count().ToString();

                //Close Db
                sqlite_conn.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        public void CalculateTotals()
        {


        }



        //BINARY SERIALIZATION/DESERIALIZATION

        public void Serialize(ObservableCollection<NetsuiteInventory> nsi, String filename)
        {
            //Create the stream to add object into it.  
            System.IO.Stream ms = File.OpenWrite(filename);
            //Format the object as Binary  

            BinaryFormatter formatter = new BinaryFormatter();
            //It serialize the employee object  
            formatter.Serialize(ms, nsi);
            ms.Flush();
            ms.Close();
            ms.Dispose();
        }



        //Deserializing the List  
        public ObservableCollection<NetsuiteInventory> Deserialize(String filename)
        {
            try
            {


                //Format the object as Binary  
                BinaryFormatter formatter = new BinaryFormatter();

                //Reading the file from the server  
                FileStream fs = File.Open(filename, FileMode.Open);

                object obj = formatter.Deserialize(fs);
                ObservableCollection<NetsuiteInventory> nsi = (ObservableCollection<NetsuiteInventory>)obj;
                fs.Flush();
                fs.Close();
                fs.Dispose();

                return nsi;

            }
            catch (Exception ex)
            {

                return null;
            }

        }

        public void SerializeVM(DriveTestViewModel nsi, String filename)
        {
            //Create the stream to add object into it.  
            System.IO.Stream ms = File.OpenWrite(filename);
            //Format the object as Binary  

            BinaryFormatter formatter = new BinaryFormatter();
            //It serialize the employee object  
            formatter.Serialize(ms, nsi);
            ms.Flush();
            ms.Close();
            ms.Dispose();
        }

        //Deserializing the List  
        public DriveTestViewModel DeserializeVM(String filename)
        {
            //Format the object as Binary  
            BinaryFormatter formatter = new BinaryFormatter();

            //Reading the file from the server  
            FileStream fs = File.Open(filename, FileMode.Open);

            object obj = formatter.Deserialize(fs);
            DriveTestViewModel nsi = (DriveTestViewModel)obj;
            fs.Flush();
            fs.Close();
            fs.Dispose();

            return nsi;

        }

        private void CheckForDuplicateSerials()
        {
            //Check if serial already present
            vm.SerialMessage2 = "";
            // NetsuiteInventory sItem = new NetsuiteInventory();
            if (vm.ScannedSerialCollection.Count > 0)
            {
                var Repeteditemlist = new List<string>();
                foreach (NetsuiteInventory nis in vm.ScannedSerialCollection)
                {
                    if (vm.ScannedSerialCollection.GroupBy(x => x.SerialNumber).Any(g => g.Count() > 1))
                    {
                        Repeteditemlist.Add(nis.SerialNumber);
                    }
                }
                foreach (var item in Repeteditemlist)
                {
                    //Console.WriteLine(item);

                    vm.SerialMessage2 = "Duplicate Serial " + item + " Found!";
                    Mouse.OverrideCursor = Cursors.Arrow;
                    return;
                }
            }

        }




        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }



        public async void GenerateDriveReport(ListView lvValue, string daterange)
        {


            //Create new Flow Document Async
            FlowDocument ReportFD = new FlowDocument();
            Section MainSection = new Section();
            MainSection.Padding = new Thickness(2);
            MainSection.Margin = new Thickness(0);
            Section mySectionReportHeader = new Section();
            mySectionReportHeader.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#0f89bd");  //(Brush)bc.ConvertFrom("#CC119EDA");



            Paragraph ReportHeader = new Paragraph(new Run("Drive Processing Breakdown"));
            ReportHeader.Padding = new Thickness(0, 5, 0, 5);
            ReportHeader.FontSize = 24;
            ReportHeader.FontFamily = new FontFamily("Segoe UI");
            ReportHeader.Foreground = Brushes.White;
            ReportHeader.TextAlignment = TextAlignment.Center;
            ReportHeader.Margin = new Thickness(0, 5, 0, 5);

            //Report date or title header
            Paragraph ReportDateHeader = new Paragraph();
            ReportDateHeader = new Paragraph(new Run("Total Drives " + vm.SearchCollection.Count().ToString()));
            ReportDateHeader.Padding = new Thickness(0, 1, 0, 1);
            ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
            ReportDateHeader.FontSize = 20;
            ReportDateHeader.Foreground = Brushes.White;
            ReportDateHeader.TextAlignment = TextAlignment.Center;
            ReportDateHeader.Margin = new Thickness(0, 1, 0, 1);

            //Report date or title header
            Paragraph ReportPOHeader = new Paragraph();
            ReportPOHeader = new Paragraph(new Run("Total Orders " + vm.SearchCollection.Select(o => o.PONumber.Trim()).Distinct().Count().ToString()));
            ReportPOHeader.Padding = new Thickness(0, 1, 0, 1);
            ReportPOHeader.FontFamily = new FontFamily("Segoe UI");
            ReportPOHeader.FontSize = 20;
            ReportPOHeader.Foreground = Brushes.White;
            ReportPOHeader.TextAlignment = TextAlignment.Center;
            ReportPOHeader.Margin = new Thickness(0, 1, 0, 1);

            Paragraph ReportDate = new Paragraph();
            ReportDate = new Paragraph(new Run(daterange));
            ReportDate.Padding = new Thickness(0, 1, 0, 1);
            ReportDate.FontFamily = new FontFamily("Segoe UI");
            ReportDate.FontSize = 16;
            ReportDate.Foreground = Brushes.White;
            ReportDate.TextAlignment = TextAlignment.Center;
            ReportDate.Margin = new Thickness(0, 1, 0, 1);


            //Report date or title header
            Paragraph ReportPassesFails = new Paragraph();

            //Count Fails And Passes
            int passed = 0;
            int failed = 0;
            int pending = 0;

            foreach (NetsuiteInventory passfails in vm.SearchCollection)
            {
                if (passfails.DriveResult == "PASS")
                {
                    passed += 1;
                }

                if (passfails.DriveResult == "FAIL")
                {
                    failed += 1;
                }

                if (passfails.DriveResult == "PENDING")
                {
                    pending += 1;
                }
            }

            Run passedDrives = new Run();
            passedDrives.Text = "Passed " + passed.ToString() + "   ";
            passedDrives.Foreground = Brushes.Green;


            Run failedDrives = new Run();
            failedDrives.Text = "Failed " + failed.ToString() + "   ";
            failedDrives.Foreground = Brushes.Red;

            Run pendingDrives = new Run();
            pendingDrives.Text = "Pending " + pending.ToString() + "   ";
            pendingDrives.Foreground = Brushes.MediumPurple;

            ReportPassesFails = new Paragraph();
            ReportPassesFails.Inlines.Add(passedDrives);
            ReportPassesFails.Inlines.Add(failedDrives);
            ReportPassesFails.Inlines.Add(pendingDrives);
            ReportPassesFails.Padding = new Thickness(0);
            ReportPassesFails.FontFamily = new FontFamily("Segoe UI");
            ReportPassesFails.FontSize = 16;
            ReportPassesFails.Foreground = Brushes.White;
            ReportPassesFails.TextAlignment = TextAlignment.Center;
            ReportPassesFails.Margin = new Thickness(0);


            //IMAGE NOT WORKING ON DIFFRENT THREADS
            //Add TB Logo
            //Create Block ui container and add image as a child
            BlockUIContainer blockUIImage = new BlockUIContainer();
            blockUIImage.Padding = new Thickness(10);
            blockUIImage.Margin = new Thickness(10);
            BitmapImage bmp0 = new BitmapImage();

            bmp0.BeginInit();
            bmp0.UriSource = new Uri("pack://application:,,,/Images/TechBuyerLogoCenteredReports.png", UriKind.Absolute);
            bmp0.CacheOption = BitmapCacheOption.OnLoad;
            bmp0.EndInit();
            bmp0.Freeze();



            System.Windows.Controls.Image img0 = new System.Windows.Controls.Image();

            img0.Source = bmp0;
            img0.Width = 150;
            img0.Height = 75;

            blockUIImage.Child = img0;



            //Add image to section
            mySectionReportHeader.Blocks.Add(blockUIImage);

            //IMAGE NOT WORKING ON DIFFRENT THREADS

            //Add Paragraphs to Header
            mySectionReportHeader.Blocks.Add(ReportHeader);
            mySectionReportHeader.Blocks.Add(ReportDateHeader);
            mySectionReportHeader.Blocks.Add(ReportPOHeader);
            mySectionReportHeader.Blocks.Add(ReportDate);


            //Operative count

            List<string> opList = new List<string>();
            List<string> poList = new List<string>();
            //Get distinct value in column
            opList = vm.SearchCollection.Select(o => o.ReceivedByOperative).Distinct().ToList<string>();
            //get all ponumbers operative processed


            //New Paragraph
            Paragraph ReportOperativeTotals = new Paragraph();
            Paragraph POOperativeTotals = new Paragraph();


            //Create new Section to host table
            Section mySectionRecords = new Section();


            mySectionRecords.Margin = new Thickness(0);
            //Add fails and passes count
            mySectionRecords.Blocks.Add(ReportPassesFails);

            string operatives = "";
            string pos = "";

            foreach (var op in opList)
            {
                //Clear list before populating
                poList.Clear();

                //Create new Paragraph Add Operative Ref
                ReportOperativeTotals = new Paragraph(new Run(op));
                ReportOperativeTotals.Padding = new Thickness(1, 1, 1, 1);
                ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
                ReportOperativeTotals.FontSize = 15;
                ReportOperativeTotals.Foreground = Brushes.MediumPurple;
                ReportOperativeTotals.TextAlignment = TextAlignment.Center;
                ReportOperativeTotals.Margin = new Thickness(1, 1, 1, 1);
                //Add to block
                mySectionRecords.Blocks.Add(ReportOperativeTotals);

                //Count the items an operative has processed in collection  + " " + vm.ITADReportCollection.Count(o => o.TestedBy == op).ToString()
                operatives += " Orders: " + vm.SearchCollection.Where(p => p.ReceivedByOperative.Replace(p.ProcessedDate.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.PONumber).Distinct().Count().ToString() + " Items: " + vm.ReportCollection.Count(o => o.ReceivedByOperative.Replace(o.ProcessedDate.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).ToString() + "";

                ReportOperativeTotals = new Paragraph(new Run(operatives));
                ReportOperativeTotals.Padding = new Thickness(1, 1, 1, 1);
                ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
                ReportOperativeTotals.FontSize = 15;
                ReportOperativeTotals.Foreground = Brushes.Green;
                ReportOperativeTotals.TextAlignment = TextAlignment.Center;
                ReportOperativeTotals.Margin = new Thickness(1, 1, 1, 1);

                mySectionRecords.Blocks.Add(ReportOperativeTotals);

                poList = vm.SearchCollection.Where(p => p.ReceivedByOperative == op).Select(p => p.PONumber).Distinct().ToList<string>();

                foreach (var po in poList)
                {
                    pos += " " + po + " (" + vm.SearchCollection.Count(o => o.PONumber == po).ToString() + ")  ";

                }

                //Create new Paragraph
                ReportOperativeTotals = new Paragraph(new Run(pos));
                ReportOperativeTotals.Padding = new Thickness(1, 1, 1, 1);
                ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
                ReportOperativeTotals.FontSize = 10;
                ReportOperativeTotals.Foreground = Brushes.Black;
                ReportOperativeTotals.TextAlignment = TextAlignment.Center;
                ReportOperativeTotals.Margin = new Thickness(1, 1, 1, 1);


                mySectionRecords.Blocks.Add(ReportOperativeTotals);



                operatives = "";
                pos = "";

            }




            //1 Add Header To Main Section
            MainSection.Blocks.Add(mySectionReportHeader);
            //2 Add Records to Main Section
            MainSection.Blocks.Add(mySectionRecords);


            //Add Main Section to flowdocument
            ReportFD.Blocks.Add(MainSection);


            //END OF FLOWDOCUMENT


            String FileName = "";




            if (string.IsNullOrEmpty(vm.DBSearch))
            {
                //Set Clipboard Include date range
                FileName = "Drive Report_" + vm.DateFrom.ToShortDateString().Replace("/", "-") + " To " + vm.DateTo.ToShortDateString().Replace("/", "-");
            }
            else
            {
                //Set Clipboard Include date range
                FileName = vm.DBSearch + "_Drive Report_" + vm.DateFrom.ToShortDateString().Replace("/", "-") + " To " + vm.DateTo.ToShortDateString().Replace("/", "-");

            }


            //SAVE FLOW DOC TO PDF
            DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

            string SavePath = vm.myDocs + @"\" + FileName + @".pdf";

            //Clone the source document
            var str = XamlWriter.Save(ReportFD);
            var stringReader = new System.IO.StringReader(str);
            var xmlReader = XmlReader.Create(stringReader);
            var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

            DocumentPaginator Clonepaginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;

            //A4 Dimensions
            Clonepaginator.PageSize = new Size(1122, 793);

            Thickness t = new Thickness(72);  // copy.PagePadding;
            CloneDoc.PagePadding = new Thickness(
                             Math.Max(0, t.Left),
                               Math.Max(0, t.Top),
                             Math.Max(1122 - (0 + 1122), t.Right),
                             Math.Max(793 - (0 + 793), t.Bottom));



            CloneDoc.ColumnWidth = double.PositiveInfinity;

            //End A4 Dimensions

            //Convert Flow Doc to Fixed First
            var paginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;
            var package = Package.Open(new MemoryStream(), FileMode.Create, FileAccess.ReadWrite);
            var packUri = new Uri("pack://temp.xps");
            PackageStore.RemovePackage(packUri);
            PackageStore.AddPackage(packUri, package);
            var xps = new XpsDocument(package, CompressionOption.NotCompressed, packUri.ToString());
            XpsDocument.CreateXpsDocumentWriter(xps).Write(paginator);
            FixedDocument fddoc = new FixedDocument();


            //Requires Dispatcher
            Application.Current.Dispatcher.Invoke((Action)delegate
            {



                fddoc = xps.GetFixedDocumentSequence().References[0].GetDocument(true);
                // your code



                //Fixed document to PDF
                var ms = new MemoryStream();
                var package2 = Package.Open(ms, FileMode.Create);
                var doc = new XpsDocument(package2);
                var writer = XpsDocument.CreateXpsDocumentWriter(doc);
                writer.Write(fddoc.DocumentPaginator);

                //docWriter.Write(fddoc.DocumentPaginator);
                doc.Close();
                package2.Close();

                // Get XPS file bytes
                var bytes = ms.ToArray();
                ms.Dispose();

                // Print to PDF
                var outputFilePath = SavePath;
                PdfFilePrinter.PrintXpsToPdf(bytes, outputFilePath, "Document Title");


                //Launch report
                ////Launch file for printing
                if (File.Exists(outputFilePath))
                {
                    System.Diagnostics.Process.Start(outputFilePath);

                }

            });


            //create new instance
            vm.SearchCollection = new ObservableCollection<NetsuiteInventory>();

            vm.ProcessedMessageColour = Brushes.Green;
            vm.ProgressIsActive = false;
            //Show message to user
            vm.ProcessedMessage = "Report Completed Successfully!";

        }


        public async void TestGenerateDriveReport(ListView lvValue)
        {

            //Create report collection 
            //Perform sort on test result field, create new collection
            ObservableCollection<NetsuiteInventory> report = new ObservableCollection<NetsuiteInventory>(vm.TestedDriveCollection.OrderByDescending(i => i.DriveResult == "FAIL"));


            ////Check passed in List View Name
            //switch (lvValue.Name)
            //{
            //    case "TestResultsLV":
            //        report = new ObservableCollection<NetsuiteInventory>(vm.TestedDriveCollection.OrderByDescending(i => i.DriveResult == "FAIL"));
            //        break;

            //    case "SearchResultsLV":
            //        report = new ObservableCollection<NetsuiteInventory>(vm.SearchCollection.OrderByDescending(i => i.DriveResult == "FAIL"));
            //        break;
            //}


            //Create new Flow Document Async
            FlowDocument ReportFD = new FlowDocument();
            Section MainSection = new Section();
            MainSection.Padding = new Thickness(2);
            MainSection.Margin = new Thickness(0);
            Section mySectionReportHeader = new Section();
            mySectionReportHeader.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#0f89bd");  //(Brush)bc.ConvertFrom("#CC119EDA");



            Paragraph ReportHeader = new Paragraph(new Run("Drive Results"));
            ReportHeader.Padding = new Thickness(0, 5, 0, 5);
            ReportHeader.FontSize = 24;
            ReportHeader.FontFamily = new FontFamily("Segoe UI");
            ReportHeader.Foreground = Brushes.White;
            ReportHeader.TextAlignment = TextAlignment.Center;
            ReportHeader.Margin = new Thickness(0, 5, 0, 5);

            //Report date or title header
            Paragraph ReportDateHeader = new Paragraph();
            ReportDateHeader = new Paragraph(new Run("Total Drives " + report.Count().ToString()));
            ReportDateHeader.Padding = new Thickness(0, 1, 0, 1);
            ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
            ReportDateHeader.FontSize = 20;
            ReportDateHeader.Foreground = Brushes.White;
            ReportDateHeader.TextAlignment = TextAlignment.Center;
            ReportDateHeader.Margin = new Thickness(0, 1, 0, 1);


            //Operative count

            //List<string> opList = new List<string>();
            ////Get distinct value in column
            //opList = vm.TestedDriveCollection.Select(o => o.ReceivedByOperative).Distinct().ToList<string>();
            //New Paragraph
            Paragraph ReportOperativeTotals = new Paragraph();


            ReportOperativeTotals = new Paragraph(new Run(DateTime.Now.ToShortDateString()));
            ReportOperativeTotals.Padding = new Thickness(0, 1, 0, 1);
            ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
            ReportOperativeTotals.FontSize = 15;
            ReportOperativeTotals.Foreground = Brushes.YellowGreen;
            ReportOperativeTotals.TextAlignment = TextAlignment.Center;
            ReportOperativeTotals.Margin = new Thickness(0, 1, 0, 1);



            //Report date or title header
            Paragraph ReportPassesFails = new Paragraph();

            //Count Fails And Passes
            int passed = 0;
            int failed = 0;





            foreach (NetsuiteInventory passfails in report)
            {
                if (passfails.DriveResult == "PASS")
                {
                    passed += 1;
                }

                if (passfails.DriveResult == "FAIL")
                {
                    failed += 1;
                }
            }

            Run passedDrives = new Run();
            passedDrives.Text = "Passed " + passed.ToString() + "   ";
            passedDrives.Foreground = Brushes.Green;


            Run failedDrives = new Run();
            failedDrives.Text = "Failed " + failed.ToString() + "   ";
            failedDrives.Foreground = Brushes.Red;

            ReportPassesFails = new Paragraph();
            ReportPassesFails.Inlines.Add(passedDrives);
            ReportPassesFails.Inlines.Add(failedDrives);
            ReportPassesFails.Padding = new Thickness(0);
            ReportPassesFails.FontFamily = new FontFamily("Segoe UI");
            ReportPassesFails.FontSize = 16;
            ReportPassesFails.Foreground = Brushes.White;
            ReportPassesFails.TextAlignment = TextAlignment.Center;
            ReportPassesFails.Margin = new Thickness(0);


            //IMAGE NOT WORKING ON DIFFRENT THREADS
            ////Add TB Logo
            ////Create Block ui container and add image as a child
            //BlockUIContainer blockUIImage = new BlockUIContainer();
            //blockUIImage.Padding = new Thickness(10);
            //blockUIImage.Margin = new Thickness(10);
            //BitmapImage bmp0 = new BitmapImage();

            //bmp0.BeginInit();
            //bmp0.UriSource = new Uri("pack://application:,,,/Images/TechBuyerLogoCenteredReports.png", UriKind.Absolute);
            //bmp0.CacheOption = BitmapCacheOption.OnLoad;
            //bmp0.EndInit();
            //bmp0.Freeze();
            //App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
            //{

            //    Image img0 = new Image();
            //    //Image.HorizontalAlignmentProperty. = HorizontalAlignment.Center;
            //    img0.Source = bmp0;
            //    img0.Width = 150;
            //    img0.Height = 75;

            //});

            //blockUIImage.Child = vm.ReportImage;
            ////Add image to section
            //mySectionReportHeader.Blocks.Add(blockUIImage);

            //IMAGE NOT WORKING ON DIFFRENT THREADS

            //Add Paragraphs to Header
            mySectionReportHeader.Blocks.Add(ReportHeader);
            mySectionReportHeader.Blocks.Add(ReportDateHeader);
            mySectionReportHeader.Blocks.Add(ReportOperativeTotals);


            //Create new Section to host table
            Section mySectionRecords = new Section();
            mySectionRecords.Margin = new Thickness(0);
            //Add fails and passes count
            mySectionRecords.Blocks.Add(ReportPassesFails);

            //Add Table
            Table RecordTable = new Table();
            RecordTable.CellSpacing = 2;
            RecordTable.TextAlignment = TextAlignment.Center;
            RecordTable.BorderBrush = Brushes.Black;
            RecordTable.BorderThickness = new Thickness(1);
            //RecordTable.BorderBrush = Brushes.Black;
            //RecordTable.BorderThickness = new Thickness(.5);

            // Create 6 columns and add them to the table's Columns collection.
            int numberOfColumns = 5;
            for (int x = 0; x < numberOfColumns; x++)
            {
                RecordTable.Columns.Add(new TableColumn());

                //ALTER THE COLUMN WIDTHS
                if (x == 0 || x == 1 || x == 2)
                {
                    RecordTable.Columns[x].Width = new GridLength(15, GridUnitType.Star);
                }
                else if (x == 3)
                {
                    RecordTable.Columns[x].Width = new GridLength(15, GridUnitType.Star);
                }
                else if (x == 6 || x == 7)
                {
                    RecordTable.Columns[x].Width = new GridLength(15, GridUnitType.Star);
                }
                else
                {
                    RecordTable.Columns[x].Width = new GridLength(15, GridUnitType.Star);
                }


            }

            // Create and add an empty TableRowGroup to hold the table's Rows.
            RecordTable.RowGroups.Add(new TableRowGroup());

            // Add the first (title) row.
            RecordTable.RowGroups[0].Rows.Add(new TableRow());

            // Alias the current working row for easy reference.
            TableRow currentRow = RecordTable.RowGroups[0].Rows[0];

            currentRow = RecordTable.RowGroups[0].Rows[0];
            // Global formatting for the title row.
            currentRow.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#0f89bd");
            currentRow.Foreground = Brushes.White;
            currentRow.FontSize = 20;

            currentRow.FontWeight = System.Windows.FontWeights.Bold;

            // Global formatting for the header row.
            currentRow.FontSize = 14;
            currentRow.FontWeight = FontWeights.Normal;
            currentRow.FontFamily = new FontFamily("Segoe UI");
            // Add cells with content to the second row.
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("PO Number"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Serial"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Product No"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Date"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Received By"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Tested By"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Status"))));



            // Add Headers to table
            mySectionRecords.Blocks.Add(RecordTable);


            //Start of Row To start adding collection
            int RowIndex = 1;
            string poNumber = "";

            foreach (NetsuiteInventory itm in report)
            {

                RecordTable.RowGroups[0].Rows.Add(new TableRow());
                currentRow = RecordTable.RowGroups[0].Rows[RowIndex];
                currentRow.FontWeight = FontWeights.Normal;
                currentRow.FontSize = 12;
                currentRow.Foreground = Brushes.Black;
                currentRow.FontFamily = new FontFamily("Segoe UI");

                // Add cells with content to the third row.
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.PONumber))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.SerialNumber))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.ProductCode))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.ProcessedDate.ToShortDateString()))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.ReceivedByOperative))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.TestedByOperative))));
                //Check result and color
                Paragraph par = new Paragraph(new Run(itm.DriveResult));
                par.Foreground = Brushes.White;
                switch (itm.DriveResult)
                {
                    case "PASS":
                        par.Background = Brushes.Green;
                        currentRow.Cells.Add(new TableCell(par));
                        break;
                    case "FAIL":
                        par.Background = Brushes.Red;
                        currentRow.Cells.Add(new TableCell(par));
                        break;
                    case "PENDING":
                        par.Background = Brushes.MediumPurple;
                        currentRow.Cells.Add(new TableCell(par));
                        break;
                }


                poNumber = itm.PONumber;

                //Move to Next Row
                RowIndex++;
            }

            //ADD ROW COLOURS
            //To be used to create row colouring, needs to target the row groups
            for (int n = 1; n < RecordTable.RowGroups[0].Rows.Count(); n++)
            {
                //for border
                //currentRow.Cells[n].BorderThickness = new Thickness(0,1,0,1);
                //currentRow.Cells[n].BorderBrush = Brushes.Black;
                if (n % 2 == 0)
                    RecordTable.RowGroups[0].Rows[n].Background = Brushes.AliceBlue;

                else
                    RecordTable.RowGroups[0].Rows[n].Background = Brushes.White;
            }

            //Add Table To Section
            mySectionRecords.Blocks.Add(RecordTable);



            //1 Add Header To Main Section
            MainSection.Blocks.Add(mySectionReportHeader);
            //2 Add Records to Main Section
            MainSection.Blocks.Add(mySectionRecords);


            //Add Main Section to flowdocument
            ReportFD.Blocks.Add(MainSection);


            //END OF FLOWDOCUMENT


            String FileName = "";




            if (string.IsNullOrEmpty(vm.DBSearch))
            {
                //Set Clipboard Include date range
                //Clipboard.SetText("Drive Report_" + vm.DateFrom.ToShortDateString().Replace("/", "-") + " To " + vm.DateTo.ToShortDateString().Replace("/", "-"), TextDataFormat.UnicodeText);

                FileName = "Drive Report_" + vm.DateFrom.ToShortDateString().Replace("/", "-");
            }
            else
            {
                //Set Clipboard Include date range
                //Clipboard.SetText(vm.DBSearch + "_Drive Report_" + vm.DateFrom.ToShortDateString().Replace("/", "-") + " To " + vm.DateTo.ToShortDateString().Replace("/", "-"), TextDataFormat.UnicodeText);
                FileName = vm.DBSearch + "_Drive Report_" + vm.DateFrom.ToShortDateString().Replace("/", "-");

            }


            //SAVE FLOW DOC TO PDF
            DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

            string SavePath = vm.myDocs + @"\" + FileName + @".pdf";

            //Clone the source document
            var str = XamlWriter.Save(ReportFD);
            var stringReader = new System.IO.StringReader(str);
            var xmlReader = XmlReader.Create(stringReader);
            var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

            DocumentPaginator Clonepaginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;

            //A4 Dimensions
            Clonepaginator.PageSize = new Size(1122, 793);

            Thickness t = new Thickness(72);  // copy.PagePadding;
            CloneDoc.PagePadding = new Thickness(
                             Math.Max(0, t.Left),
                               Math.Max(0, t.Top),
                             Math.Max(1122 - (0 + 1122), t.Right),
                             Math.Max(793 - (0 + 793), t.Bottom));



            CloneDoc.ColumnWidth = double.PositiveInfinity;

            //End A4 Dimensions

            //Convert Flow Doc to Fixed First
            var paginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;
            var package = Package.Open(new MemoryStream(), FileMode.Create, FileAccess.ReadWrite);
            var packUri = new Uri("pack://temp.xps");
            PackageStore.RemovePackage(packUri);
            PackageStore.AddPackage(packUri, package);
            var xps = new XpsDocument(package, CompressionOption.NotCompressed, packUri.ToString());
            XpsDocument.CreateXpsDocumentWriter(xps).Write(paginator);
            FixedDocument fddoc = new FixedDocument();


            //Requires Dispatcher
            Application.Current.Dispatcher.Invoke((Action)delegate
            {



                fddoc = xps.GetFixedDocumentSequence().References[0].GetDocument(true);
                // your code



                //Fixed document to PDF
                var ms = new MemoryStream();
                var package2 = Package.Open(ms, FileMode.Create);
                var doc = new XpsDocument(package2);
                var writer = XpsDocument.CreateXpsDocumentWriter(doc);
                writer.Write(fddoc.DocumentPaginator);

                //docWriter.Write(fddoc.DocumentPaginator);
                doc.Close();
                package2.Close();

                // Get XPS file bytes
                var bytes = ms.ToArray();
                ms.Dispose();

                // Print to PDF
                var outputFilePath = SavePath;
                PdfFilePrinter.PrintXpsToPdf(bytes, outputFilePath, "Document Title");
            });




            //EMAIL OBJECT
            string PDFReportPath = vm.myDocs + @"\" + FileName + @".pdf";


            //zip Test Reports and create zip

            if (lvValue.Items.Count != 0)
            {
                //MessageBox.Show("Hmmm");

                //try
                //{
                vm.ProcessedMessageColour = Brushes.MediumPurple;
                vm.ProcessedMessage = "All Reports for current PO are being Archived. Please Wait....";

                vm.ProgressIsActive = true;
                ////Change mouse cursor 
                //Mouse.OverrideCursor = Cursors.Wait;

                //Create Local Folder
                if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                {
                    if (string.IsNullOrEmpty(vm.DBSearch))
                    {
                    }
                    else
                    {
                        //Create local file
                        Directory.CreateDirectory(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports");
                    }

                }



                //Create string array for attachments
                string[] Attachments = new string[] { PDFReportPath };
                //Create outlook object
                EmailClass emailReport = new EmailClass(vm);
                await System.Threading.Tasks.Task.Run(() => emailReport.AttachFilesAndSendMail(Attachments, "Drive Test"));

                //Delete Original Folder?
                //Create Local Folder
                if (Directory.Exists(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports"))
                {
                    //Delete Local Folder
                    Directory.Delete(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports", true);

                }


                vm.ProcessedMessageColour = Brushes.Green;


                vm.ProgressIsActive = false;

                //Show message to user
                vm.ProcessedMessage = "Report Attached successfully!";

            }

            //Update Collection via dispatcher
            //App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
            //{
            //});


        }

        //INDEX CODE
        public void CreateDBIndex(SQLiteConnection conn)
        {
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();

            //create index 2 for TE STats Table
            //using (var sqlCommand = new SQLiteCommand("CREATE UNIQUE INDEX [UNIQUE_CLICK_STATS] ON [tb_teStatistics] ([tbtes_amUserID], [tbtes_isoDate], [tbtes_teDomain]) WHERE tbtes_isReported = 0;", conn))
            //{
            //    sqlCommand.ExecuteNonQuery();
            //}
            using (var sqlCommand = new SQLiteCommand("CREATE INDEX [ProcessedIndex] ON [Processed] ([ProcessedDate]);", conn))
            {
                sqlCommand.ExecuteNonQuery();
            }

        }


        public List<RedfishKeyValuePair> ReturnProductDescription(string product)
        {
            List<RedfishKeyValuePair> productData = new List<RedfishKeyValuePair>();

            if (File.Exists(vm.myDocs + @"\Inventory.csv"))
            {


                //string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv";
                //Uses a VB class to process the file instead of streamreader
                using (TextFieldParser parser = new TextFieldParser(vm.myDocs + @"\Inventory.csv"))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");

                    while (!parser.EndOfData)
                    {
                        //Processing row
                        string currentLine = parser.ReadLine();
                        //Console.WriteLine(currentLine);

                        //Check if product code already exists in CSV. If not flag part code required as yes
                        if (currentLine != null)
                        {
                            string[] itmValue = currentLine.Split(',');




                            //Product Code
                            if (itmValue[0] != string.Empty)
                            {


                                RedfishKeyValuePair productItem = new RedfishKeyValuePair();


                                productItem.Key = itmValue[0].Trim();
                                productItem.Value = itmValue[1].Trim();

                                productData.Add(productItem);
                            }

                        }
                    }

                    return productData;

                }
            }

            return null;
        }



        public async void ImportReplacePO(string PORef, ObservableCollection<NetsuiteInventory> CurrentPO)
        {

            //Connection to SQL Lite DB
            //if all criteria met create the job
            SQLiteConnection conn;
            conn = CreateConnection(cs);

            //Create Query List
            List<string> myQueries = new List<string>();

            //Delete previous PO
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.CommandText = "DELETE FROM DriveInfo WHERE PONumber = '" + PORef.Trim() + "'";
            sqlite_cmd.CommandType = CommandType.Text;
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@ponumber", PORef.Trim()));

            sqlite_cmd.ExecuteNonQuery();



            //ADD TO DB

            try
            {


                // sqlite_cmd = conn.CreateCommand();
                ////sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                //sqlite_cmd.CommandText = "INSERT INTO DriveInfo (Date, PONumber, POLineType, ProductCode, ProductDescription, Quantity, SerialNo, UnitPrice, Amount, RecOperative, TestOperative, WipeSoftware, DriveResult, DriveFailiure, DriveFailiureSpecify, PartCodeRequired, ReportPath, ITADRef)" + " VALUES(@date,@ponumber,@polinetype,@productcode,@productdescription,@quantity,@serialno,@unitprice,@amount,@recoperative,@testoperative,@wipesoftware,@driveresult,@drivefailiure,@drivefailiurespecify,@partcoderequired,@reportpath,@itadref)";
                //sqlite_cmd.CommandType = CommandType.Text;

                //LOOP THROUGH ADDED COLLECTION
                foreach (NetsuiteInventory itm in CurrentPO)
                {

                    //Add query to list
                    myQueries.Add("INSERT INTO DriveInfo (Date, PONumber, POLineType, ProductCode, ProductDescription, Quantity, SerialNo, UnitPrice, Amount, RecOperative, TestOperative, WipeSoftware, DriveResult, DriveFailiure, DriveFailiureSpecify, PartCodeRequired, ReportPath, ITADRef)" +
                        " VALUES('" + DateTime.Now.ToString("yyyy-MM-dd") + "','" + PORef + "','" + "" + "','" + itm.ProductCode + "','" + itm.ProductDescription + "','" + "1" + "','" + itm.SerialNumber + "','" + "0" + "','" + "0" + "','" + itm.ReceivedByOperative + "','" + "" + "','" + "" + "','" + "PENDING" + "','" + "" + "','" + "" + "','" + "" + "','" + "" + "','" + "" + "')");

                    ////Date must be in this format to work with SQL Lite
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@date", DateTime.Now.ToString("yyyy-MM-dd")));
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@ponumber", PORef));
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@polinetype", ""));
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@productcode", itm.ProductCode));
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@productdescription", itm.ProductDescription));
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@quantity", "1"));
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", itm.SerialNumber));
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@unitprice", "0"));
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@amount", "0"));
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@recoperative", itm.ReceivedByOperative));
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@testoperative", ""));
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@wipesoftware", "")); // added later
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@driveresult", "PENDING")); //added later
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiure", "")); //added later
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@drivefailiurespecify", "")); //added later
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@partcoderequired", ""));
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportpath", "")); //added later
                    //sqlite_cmd.Parameters.Add(new SQLiteParameter("@itadref", "")); //added later
                    //sqlite_cmd.ExecuteNonQuery();


                }




                //Create a transaction
                using (var tra = conn.BeginTransaction())
                {
                    try
                    {
                        //Console.WriteLine(myQueries.Count().ToString() + "\n");

                        foreach (var myQuery in myQueries)
                        {
                            //Console.WriteLine(myQuery.ToString());

                            using (var cd = new SQLiteCommand(myQuery, conn, tra))
                            {
                                cd.ExecuteNonQuery();
                            }
                        }

                        tra.Commit();
                    }
                    catch (Exception ex)
                    {
                        tra.Rollback();
                        //Console.Error.Writeline("I did nothing, because something wrong happened: {0}", ex);
                        throw;
                    }
                }


                //Close DB
                conn.Close();


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }




        }


    }
}
