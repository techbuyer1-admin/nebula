﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class UpgradeServerFirmwareCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public UpgradeServerFirmwareCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter != null)
            {
                var values = (object[])parameter;
                var ServerIP = (string)values[0];
                var WhichUpdate = (string)values[1];
                //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
                //if (vm.Server1IPv4 != null || vm.Server2IPv4 != null || vm.Server3IPv4 != null || vm.Server4IPv4 != null
                //    || vm.Server5IPv4 != null || vm.Server6IPv4 != null || vm.Server7IPv4 != null || vm.Server8IPv4 != null
                //     || vm.Server9IPv4 != null || vm.Server10IPv4 != null || vm.Server11IPv4 != null || vm.Server12IPv4 != null
                //      || vm.Server13IPv4 != null || vm.Server14IPv4 != null || vm.Server15IPv4 != null || vm.Server16IPv4 != null)
                //{
                //check if valid ip entered
                //System.Net.IPAddress ipAddress = null;
                //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                //if (vm.Server1IPv4 != string.Empty || vm.Server2IPv4 != string.Empty || vm.Server3IPv4 != string.Empty || vm.Server4IPv4 != string.Empty
                //    || vm.Server5IPv4 != string.Empty || vm.Server6IPv4 != string.Empty || vm.Server7IPv4 != string.Empty || vm.Server8IPv4 != string.Empty
                //     || vm.Server9IPv4 != string.Empty || vm.Server10IPv4 != string.Empty || vm.Server11IPv4 != string.Empty || vm.Server12IPv4 != string.Empty
                //      || vm.Server13IPv4 != string.Empty || vm.Server14IPv4 != string.Empty || vm.Server15IPv4 != string.Empty || vm.Server16IPv4 != string.Empty)
                //    {

                if (ServerIP != string.Empty)
                {

                    return true;
                }
                else
                {
                    return false;
                }
                //}
                //else
                //{
                //    return false;
                //}
            }
            else
            {
                return false;
            }



        }


        public void InitialFolder()
        {

        }




        public void Execute(object parameter)
        {

            var values = (object[])parameter;
            var ServerIP = (string)values[0];
            var WhichUpdate = (string)values[1];

            //MessageBox.Show(ServerIP.ToString());
            //MessageBox.Show(WhichUpdate.ToString());

            string mydocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");


            ////Used to Set US Dell Update Paths
            //if (StaticFunctions.UncPathToUse.Contains(@"T:\"))
            //{
            //    StaticFunctions.Department = "US";
            //}
            //else if (StaticFunctions.UncPathToUse.Contains(@"W:\Nebula"))
            //{
            //    StaticFunctions.Department = "AUS";
            //}
            //else if (StaticFunctions.UncPathToUse.Contains(@"W:\FR Warehouse\Goods In\Test Reports\"))
            //{
            //    StaticFunctions.Department = "FRA";
            //}


            // MessageBox.Show(WhichUpdate);

            switch (WhichUpdate)
            {
                case "BIOSS1":


                    using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                    {


                        //Requires GUID for MyComputer Or ThisPC Folder
                        switch (StaticFunctions.Department)
                        {
                            case "GoodsIn":

                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {

                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                }

                                break;
                            case "GoodsOutTech":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                }
                                break;
                            case "ITAD":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                }
                                break;
                            case "TestDept":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                }
                                break;
                        }


                        //openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";

                        // //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                        switch (vm.ServerType)
                        {
                            case "HPGen8RackMount":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen9RackMount":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen10RackMount":
                                // openFileDialog.Filter = "Fwpkg files (*.fwpkg)|*.fwpkg";
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen8Blade":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen9Blade":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen10Blade":
                                // openFileDialog.Filter = "Fwpkg files (*.fwpkg)|*.fwpkg";
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                        }
                        openFileDialog.FilterIndex = 0;
                        openFileDialog.Multiselect = true;
                        openFileDialog.RestoreDirectory = true;



                        //MessageBox.Show(vm.ServerType);


                        if (vm.ServerType == "HPGen8RackMount" || vm.ServerType == "HPGen9RackMount" || vm.ServerType == "HPGen8Blade" || vm.ServerType == "HPGen9Blade")
                        {
                            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                if (Directory.Exists(@"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL"))
                                {
                                    // Loop through how ever many items are selected and add to the list
                                    foreach (var file in openFileDialog.FileNames)
                                    {
                                        if (File.Exists(file))
                                        {
                                            //Create a local copy
                                            File.Copy(file, @"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "BIOSUpdate.bin", true);

                                            int lastSlash = file.LastIndexOf(@"\");

                                            vm.BIOSUpgradePath1 = file.Substring(lastSlash + 1);

                                            //vm.BIOSUpgradePath1 = file;

                                            //ADD CODE TO CREATE RIBCL WITH NEW UPGRADE PATH
                                            string BIOSFWFile = "<RIBCL VERSION=\"2.0\">\r" +
                                        " <LOGIN USER_LOGIN=\"adminname\" PASSWORD=\"password\">\r" +
                                        "<RIB_INFO MODE=\"write\">\r" +
                                        "<TPM_ENABLED VALUE=\"Yes\"/>\r" +
                                        "<UPDATE_RIB_FIRMWARE IMAGE_LOCATION=\"" + @"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "BIOSUpdate.bin" + "\"/>\r" +
                                        "</RIB_INFO>\r" +
                                        "</LOGIN>\r" +
                                        "</RIBCL>";
                                            File.WriteAllText(@"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "_BIOS_FirmwareUpdate1.xml", BIOSFWFile);
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please enter an IP and click the Get Server Info Button before selecting BIOS or iLO Updates.");
                                }
                            }
                        }
                        else if (vm.ServerType == "HPGen10RackMount" || vm.ServerType == "HPGen10Blade")
                        {
                            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                if (Directory.Exists(@"" + mydocs + @"\HPTOOLS\RESTful Interface Tool"))
                                {
                                    // Loop through how ever many items are selected and add to the list
                                    foreach (var file in openFileDialog.FileNames)
                                    {
                                        if (File.Exists(file))
                                        {
                                            //Create a local copy
                                            File.Copy(file, @"" + mydocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "BIOSUpdate.bin", true);

                                            //vm.BIOSUpgradePath1 = file;

                                            int lastSlash = file.LastIndexOf(@"\");

                                            vm.BIOSUpgradePath1 = file.Substring(lastSlash + 1);

                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please enter an IP and click the Get Server Info Button before selecting BIOS or iLO Updates.");
                                }
                            }
                        }

                    }
                    break;
                case "ILOS1":



                    // MessageBox.Show("Folder Already Exists");
                    //MessageBox.Show(drives);
                    using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                    {

                        //Requires GUID for MyComputer Or ThisPC Folder
                        //Requires GUID for MyComputer Or ThisPC Folder
                        switch (StaticFunctions.Department)
                        {
                            case "GoodsIn":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                }

                                break;
                            case "GoodsOutTech":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                }
                                break;
                            case "ITAD":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                }
                                break;
                            case "TestDept":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                }
                                break;
                        }
                        // //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                        switch (vm.ServerType)
                        {
                            case "HPGen8RackMount":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen9RackMount":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen10RackMount":
                                // openFileDialog.Filter = "Fwpkg files (*.fwpkg)|*.fwpkg";
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen8Blade":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen9Blade":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen10Blade":
                                // openFileDialog.Filter = "Fwpkg files (*.fwpkg)|*.fwpkg";
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                        }

                        openFileDialog.FilterIndex = 0;
                        openFileDialog.Multiselect = true;
                        openFileDialog.RestoreDirectory = true;

                        if (vm.ServerType == "HPGen8RackMount" || vm.ServerType == "HPGen9RackMount" || vm.ServerType == "HPGen8Blade" || vm.ServerType == "HPGen9Blade")
                        {
                            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                if (Directory.Exists(@"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL"))
                                {
                                    // Loop through how ever many items are selected and add to the list
                                    foreach (var file in openFileDialog.FileNames)
                                    {
                                        if (File.Exists(file))
                                        {
                                            //Create a local copy
                                            File.Copy(file, @"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "ILOUpdate.bin", true);


                                            int lastSlash = file.LastIndexOf(@"\");

                                            vm.ILOUpgradePath1 = file.Substring(lastSlash + 1);

                                            //ADD CODE TO CREATE RIBCL WITH NEW UPGRADE PATH
                                            string ILOFWFile = "<RIBCL VERSION=\"2.0\">\r" +
                                        " <LOGIN USER_LOGIN=\"adminname\" PASSWORD=\"password\">\r" +
                                        "<RIB_INFO MODE=\"write\">\r" +
                                        "<TPM_ENABLED VALUE=\"Yes\"/>\r" +
                                        "<UPDATE_RIB_FIRMWARE IMAGE_LOCATION=\"" + @"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "ILOUpdate.bin" + "\"/>\r" +
                                        "</RIB_INFO>\r" +
                                        "</LOGIN>\r" +
                                        "</RIBCL>";
                                            File.WriteAllText(@"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "_ILO_FirmwareUpdate1.xml", ILOFWFile);
                                        }
                                    }
                                    //IMPORTANT!
                                    //If ilo4 isn't version 2.00 or above run the upgrade after selecting the file
                                    //if (vm.ILOMinVersion1 == "Gen8UpgradeNeeded" || vm.ILORestOutput1.Contains("ERROR: 'NoneType' object has no attribute 'obj'"))
                                    //{
                                    //    vm.RunRestfulScriptsCommandGen8S1.Execute(new String[] { "IloUpgradeOnly", "Gen8" });
                                    //}
                                    //else if (vm.ILOMinVersion1 == "Gen9UpgradeNeeded" || vm.ILORestOutput1.Contains("ERROR: 'NoneType' object has no attribute 'obj'"))
                                    //{
                                    //    vm.RunRestfulScriptsCommandGen9S1.Execute(new String[] { "IloUpgradeOnly", "Gen9" });
                                    //}



                                }
                                else
                                {
                                    MessageBox.Show("Please enter an IP and click the Get Server Info Button before selecting BIOS or iLO Updates.");
                                }
                            }
                        }
                        else if (vm.ServerType == "HPGen10RackMount" || vm.ServerType == "HPGen10Blade")
                        {
                            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                if (Directory.Exists(@"" + mydocs + @"\HPTOOLS\RESTful Interface Tool"))
                                {
                                    // Loop through how ever many items are selected and add to the list
                                    foreach (var file in openFileDialog.FileNames)
                                    {

                                        //if (file.Contains(".bin"))
                                        //{
                                        if (File.Exists(file))
                                        {
                                            //Create a local copy
                                            File.Copy(file, @"" + mydocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "ILOUpdate.bin", true);



                                            int lastSlash = file.LastIndexOf(@"\");

                                            vm.ILOUpgradePath1 = file.Substring(lastSlash + 1);

                                        }
                                        //}
                                        //else
                                        //{

                                        //        if (File.Exists(file))
                                        //        {
                                        //            //Create a local copy
                                        //            File.Copy(file, @"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "ILOUpdate.bin", true);


                                        //            int lastSlash = file.LastIndexOf(@"\");

                                        //            vm.ILOUpgradePath1 = file.Substring(lastSlash + 1);

                                        //            //ADD CODE TO CREATE RIBCL WITH NEW UPGRADE PATH
                                        //            string ILOFWFile = "<RIBCL VERSION=\"2.0\">\r" +
                                        //        " <LOGIN USER_LOGIN=\"adminname\" PASSWORD=\"password\">\r" +
                                        //        "<RIB_INFO MODE=\"write\">\r" +
                                        //        "<TPM_ENABLED VALUE=\"Yes\"/>\r" +
                                        //        "<UPDATE_RIB_FIRMWARE IMAGE_LOCATION=\"" + @"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "ILOUpdate.bin" + "\"/>\r" +
                                        //        "</RIB_INFO>\r" +
                                        //        "</LOGIN>\r" +
                                        //        "</RIBCL>";
                                        //            File.WriteAllText(@"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "_ILO_FirmwareUpdate1.xml", ILOFWFile);
                                        //        }

                                        //}


                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please enter an IP and click the Get Server Info Button before selecting BIOS or iLO Updates.");
                                }
                            }
                        }

                    }

                    break;

                case "SPSS1":



                    // MessageBox.Show("Folder Already Exists");
                    //MessageBox.Show(drives);
                    using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                    {

                        //Requires GUID for MyComputer Or ThisPC Folder
                        switch (StaticFunctions.Department)
                        {
                            case "GoodsIn":
                                if (vm.GenGenericServerInfo1 != null)
                                {
                                    if (vm.GenGenericServerInfo1.Model != null)
                                    {
                                        if (vm.GenGenericServerInfo1.Model.Contains("DL20") || vm.GenGenericServerInfo1.Model.Contains("ML30"))
                                        {
                                            vm.WarningMessage1 = "Please upload the fwpkg Package in the VIA REPOSITORY folder through the ilo and run from there!";
                                            //openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP\Gen10\SPS FOR DL20+ML30";

                                        }
                                        else
                                        {
                                            if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                            {
                                                openFileDialog.InitialDirectory = mydocs;
                                            }
                                            else
                                            {
                                                openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                        {
                                            openFileDialog.InitialDirectory = mydocs;
                                        }
                                        else
                                        {
                                            openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                        }
                                    }
                                }
                                else
                                {
                                    if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                    {
                                        openFileDialog.InitialDirectory = mydocs;
                                    }
                                    else
                                    {
                                        openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                    }
                                }

                                break;
                            case "GoodsOutTech":

                                if (vm.GenGenericServerInfo1 != null)
                                {
                                    if (vm.GenGenericServerInfo1.Model != null)
                                    {
                                        if (vm.GenGenericServerInfo1.Model.Contains("DL20") || vm.GenGenericServerInfo1.Model.Contains("ML30"))
                                        {
                                            vm.WarningMessage1 = "Please upload the fwpkg Package in the VIA REPOSITORY folder through the ilo and run from there!";
                                            //openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP\Gen10\SPS FOR DL20+ML30";

                                        }
                                        else
                                        {
                                            if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                            {
                                                openFileDialog.InitialDirectory = mydocs;
                                            }
                                            else
                                            {
                                                openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                        {
                                            openFileDialog.InitialDirectory = mydocs;
                                        }
                                        else
                                        {
                                            openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                        }
                                    }
                                }
                                else
                                {
                                    if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                    {
                                        openFileDialog.InitialDirectory = mydocs;
                                    }
                                    else
                                    {
                                        openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                    }
                                }

                                break;
                            case "ITAD":

                                if (vm.GenGenericServerInfo1 != null)
                                {
                                    if (vm.GenGenericServerInfo1.Model != null)
                                    {
                                        if (vm.GenGenericServerInfo1.Model.Contains("DL20") || vm.GenGenericServerInfo1.Model.Contains("ML30"))
                                        {
                                            vm.WarningMessage1 = "Please upload the fwpkg Package in the VIA REPOSITORY folder through the ilo and run from there!";
                                            //openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP\Gen10\SPS FOR DL20+ML30";

                                        }
                                        else
                                        {
                                            if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                            {
                                                openFileDialog.InitialDirectory = mydocs;
                                            }
                                            else
                                            {
                                                openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                        {
                                            openFileDialog.InitialDirectory = mydocs;
                                        }
                                        else
                                        {
                                            openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                        }
                                    }
                                }
                                else
                                {
                                    if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                    {
                                        openFileDialog.InitialDirectory = mydocs;
                                    }
                                    else
                                    {
                                        openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                    }
                                }

                                break;
                            case "TestDept":

                                if (vm.GenGenericServerInfo1 != null)
                                {
                                    if (vm.GenGenericServerInfo1.Model != null)
                                    {
                                        if (vm.GenGenericServerInfo1.Model.Contains("DL20") || vm.GenGenericServerInfo1.Model.Contains("ML30"))
                                        {
                                            vm.WarningMessage1 = "Please upload the fwpkg Package in the VIA REPOSITORY folder through the ilo and run from there!";
                                            //openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP\Gen10\SPS FOR DL20+ML30";

                                        }
                                        else
                                        {
                                            if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                            {
                                                openFileDialog.InitialDirectory = mydocs;
                                            }
                                            else
                                            {
                                                openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                        {
                                            openFileDialog.InitialDirectory = mydocs;
                                        }
                                        else
                                        {
                                            openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                        }
                                    }
                                }
                                else
                                {
                                    if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                    {
                                        openFileDialog.InitialDirectory = mydocs;
                                    }
                                    else
                                    {
                                        openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                    }
                                }

                                break;

                        }
                        // //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                        switch (vm.ServerType)
                        {
                            case "HPGen8RackMount":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen9RackMount":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen10RackMount":
                                // openFileDialog.Filter = "Fwpkg files (*.fwpkg)|*.fwpkg";
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen8Blade":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen9Blade":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen10Blade":
                                // openFileDialog.Filter = "Fwpkg files (*.fwpkg)|*.fwpkg";
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                        }

                        openFileDialog.FilterIndex = 0;
                        openFileDialog.Multiselect = true;
                        openFileDialog.RestoreDirectory = true;

                        if (vm.ServerType == "HPGen8RackMount" || vm.ServerType == "HPGen9RackMount" || vm.ServerType == "HPGen8Blade" || vm.ServerType == "HPGen9Blade")
                        {
                            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                if (Directory.Exists(@"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL"))
                                {
                                    // Loop through how ever many items are selected and add to the list
                                    foreach (var file in openFileDialog.FileNames)
                                    {
                                        if (File.Exists(file))
                                        {
                                            //Create a local copy
                                            File.Copy(file, @"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "SPSUpdate.bin", true);


                                            int lastSlash = file.LastIndexOf(@"\");

                                            vm.SPSUpgradePath1 = file.Substring(lastSlash + 1);

                                            //ADD CODE TO CREATE RIBCL WITH NEW UPGRADE PATH
                                            string ILOFWFile = "<RIBCL VERSION=\"2.0\">\r" +
                                        " <LOGIN USER_LOGIN=\"adminname\" PASSWORD=\"password\">\r" +
                                        "<RIB_INFO MODE=\"write\">\r" +
                                        "<TPM_ENABLED VALUE=\"Yes\"/>\r" +
                                        "<UPDATE_RIB_FIRMWARE IMAGE_LOCATION=\"" + @"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "SPSUpdate.bin" + "\"/>\r" +
                                        "</RIB_INFO>\r" +
                                        "</LOGIN>\r" +
                                        "</RIBCL>";
                                            File.WriteAllText(@"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "_SPS_FirmwareUpdate1.xml", ILOFWFile);
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please enter an IP and click the Get Server Info Button before selecting BIOS or iLO Updates.");
                                }
                            }
                        }
                        else if (vm.ServerType == "HPGen10RackMount" || vm.ServerType == "HPGen10Blade")
                        {
                            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                if (Directory.Exists(@"" + mydocs + @"\HPTOOLS\RESTful Interface Tool"))
                                {
                                    // Loop through how ever many items are selected and add to the list
                                    foreach (var file in openFileDialog.FileNames)
                                    {
                                        if (File.Exists(file))
                                        {
                                            //Create a local copy
                                            File.Copy(file, @"" + mydocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "SPSUpdate.bin", true);



                                            int lastSlash = file.LastIndexOf(@"\");

                                            vm.SPSUpgradePath1 = file.Substring(lastSlash + 1);

                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please enter an IP and click the Get Server Info Button before selecting BIOS or iLO Updates.");
                                }
                            }
                        }

                    }

                    break;

                case "IENGS1":



                    // MessageBox.Show("Folder Already Exists");
                    //MessageBox.Show(drives);
                    using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                    {

                        //Requires GUID for MyComputer Or ThisPC Folder
                        switch (StaticFunctions.Department)
                        {
                            case "GoodsIn":

                                if (vm.GenGenericServerInfo1 != null)
                                {
                                    if (vm.GenGenericServerInfo1.Model != null)
                                    {
                                        if (vm.GenGenericServerInfo1.Model.Contains("DL20") || vm.GenGenericServerInfo1.Model.Contains("ML30"))
                                        {
                                            vm.WarningMessage1 = "Please upload the fwpkg Package in the VIA REPOSITORY folder through the ilo and run from there!";
                                            //openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP\Gen10\SPS FOR DL20+ML30";

                                        }
                                        else
                                        {
                                            if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                            {
                                                openFileDialog.InitialDirectory = mydocs;
                                            }
                                            else
                                            {
                                                openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                        {
                                            openFileDialog.InitialDirectory = mydocs;
                                        }
                                        else
                                        {
                                            openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                        }
                                    }
                                }
                                else
                                {
                                    if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                    {
                                        openFileDialog.InitialDirectory = mydocs;
                                    }
                                    else
                                    {
                                        openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                    }
                                }

                                break;
                            case "GoodsOutTech":

                                if (vm.GenGenericServerInfo1 != null)
                                {
                                    if (vm.GenGenericServerInfo1.Model != null)
                                    {
                                        if (vm.GenGenericServerInfo1.Model.Contains("DL20") || vm.GenGenericServerInfo1.Model.Contains("ML30"))
                                        {
                                            vm.WarningMessage1 = "Please upload the fwpkg Package in the VIA REPOSITORY folder through the ilo and run from there!";
                                            //openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP\Gen10\SPS FOR DL20+ML30";

                                        }
                                        else
                                        {
                                            if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                            {
                                                openFileDialog.InitialDirectory = mydocs;
                                            }
                                            else
                                            {
                                                openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                        {
                                            openFileDialog.InitialDirectory = mydocs;
                                        }
                                        else
                                        {
                                            openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                        }
                                    }
                                }
                                else
                                {
                                    if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                    {
                                        openFileDialog.InitialDirectory = mydocs;
                                    }
                                    else
                                    {
                                        openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                    }
                                }

                                break;
                            case "ITAD":
                                if (vm.GenGenericServerInfo1 != null)
                                {
                                    if (vm.GenGenericServerInfo1.Model != null)
                                    {
                                        if (vm.GenGenericServerInfo1.Model.Contains("DL20") || vm.GenGenericServerInfo1.Model.Contains("ML30"))
                                        {
                                            vm.WarningMessage1 = "Please upload the fwpkg Package in the VIA REPOSITORY folder through the ilo and run from there!";
                                            //openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP\Gen10\SPS FOR DL20+ML30";

                                        }
                                        else
                                        {
                                            if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                            {
                                                openFileDialog.InitialDirectory = mydocs;
                                            }
                                            else
                                            {
                                                openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                        {
                                            openFileDialog.InitialDirectory = mydocs;
                                        }
                                        else
                                        {
                                            openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                        }
                                    }
                                }
                                else
                                {
                                    if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                    {
                                        openFileDialog.InitialDirectory = mydocs;
                                    }
                                    else
                                    {
                                        openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                    }
                                }

                                break;
                            case "TestDept":

                                if (vm.GenGenericServerInfo1 != null)
                                {
                                    if (vm.GenGenericServerInfo1.Model != null)
                                    {
                                        if (vm.GenGenericServerInfo1.Model.Contains("DL20") || vm.GenGenericServerInfo1.Model.Contains("ML30"))
                                        {
                                            vm.WarningMessage1 = "Please upload the fwpkg Package in the VIA REPOSITORY folder through the ilo and run from there!";
                                            //openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP\Gen10\SPS FOR DL20+ML30";

                                        }
                                        else
                                        {
                                            if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                            {
                                                openFileDialog.InitialDirectory = mydocs;
                                            }
                                            else
                                            {
                                                openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                        {
                                            openFileDialog.InitialDirectory = mydocs;
                                        }
                                        else
                                        {
                                            openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                        }
                                    }
                                }
                                else
                                {
                                    if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                    {
                                        openFileDialog.InitialDirectory = mydocs;
                                    }
                                    else
                                    {
                                        openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\HP";
                                    }
                                }
                                break;

                        }
                        // //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                        switch (vm.ServerType)
                        {
                            case "HPGen8RackMount":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen9RackMount":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen10RackMount":
                                // openFileDialog.Filter = "Fwpkg files (*.fwpkg)|*.fwpkg";
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen8Blade":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen9Blade":
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                            case "HPGen10Blade":
                                // openFileDialog.Filter = "Fwpkg files (*.fwpkg)|*.fwpkg";
                                openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                                break;
                        }

                        openFileDialog.FilterIndex = 0;
                        openFileDialog.Multiselect = true;
                        openFileDialog.RestoreDirectory = true;

                        if (vm.ServerType == "HPGen8RackMount" || vm.ServerType == "HPGen9RackMount" || vm.ServerType == "HPGen8Blade" || vm.ServerType == "HPGen9Blade")
                        {
                            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                if (Directory.Exists(@"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL"))
                                {
                                    // Loop through how ever many items are selected and add to the list
                                    foreach (var file in openFileDialog.FileNames)
                                    {
                                        if (File.Exists(file))
                                        {
                                            //Create a local copy
                                            File.Copy(file, @"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "IENGUpdate.bin", true);


                                            int lastSlash = file.LastIndexOf(@"\");

                                            vm.IENGUpgradePath1 = file.Substring(lastSlash + 1);

                                            //ADD CODE TO CREATE RIBCL WITH NEW UPGRADE PATH
                                            string ILOFWFile = "<RIBCL VERSION=\"2.0\">\r" +
                                        " <LOGIN USER_LOGIN=\"adminname\" PASSWORD=\"password\">\r" +
                                        "<RIB_INFO MODE=\"write\">\r" +
                                        "<TPM_ENABLED VALUE=\"Yes\"/>\r" +
                                        "<UPDATE_RIB_FIRMWARE IMAGE_LOCATION=\"" + @"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "IENGUpdate.bin" + "\"/>\r" +
                                        "</RIB_INFO>\r" +
                                        "</LOGIN>\r" +
                                        "</RIBCL>";
                                            File.WriteAllText(@"" + mydocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "_IENG_FirmwareUpdate1.xml", ILOFWFile);
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please enter an IP and click the Get Server Info Button before selecting updates.");
                                }
                            }
                        }
                        else if (vm.ServerType == "HPGen10RackMount" || vm.ServerType == "HPGen10Blade")
                        {


                            if (vm.GenGenericServerInfo1 != null)
                            {
                                if (vm.GenGenericServerInfo1.Model.Contains("DL20") || vm.GenGenericServerInfo1.Model.Contains("ML30"))
                                {
                                    //Dont allow selection as not required
                                    return;
                                }
                                else
                                {
                                    if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                                    {
                                        if (Directory.Exists(@"" + mydocs + @"\HPTOOLS\RESTful Interface Tool"))
                                        {
                                            // Loop through how ever many items are selected and add to the list
                                            foreach (var file in openFileDialog.FileNames)
                                            {
                                                if (File.Exists(file))
                                                {
                                                    //Create a local copy
                                                    File.Copy(file, @"" + mydocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "IENGUpdate.bin", true);



                                                    int lastSlash = file.LastIndexOf(@"\");

                                                    vm.IENGUpgradePath1 = file.Substring(lastSlash + 1);

                                                }
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Please enter an IP and click the Get Server Info Button before selecting updates.");
                                        }
                                    }

                                }
                            }
                            else
                            {
                                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                                {
                                    if (Directory.Exists(@"" + mydocs + @"\HPTOOLS\RESTful Interface Tool"))
                                    {
                                        // Loop through how ever many items are selected and add to the list
                                        foreach (var file in openFileDialog.FileNames)
                                        {
                                            if (File.Exists(file))
                                            {
                                                //Create a local copy
                                                File.Copy(file, @"" + mydocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "IENGUpdate.bin", true);



                                                int lastSlash = file.LastIndexOf(@"\");

                                                vm.IENGUpgradePath1 = file.Substring(lastSlash + 1);

                                            }
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Please enter an IP and click the Get Server Info Button before selecting updates.");
                                    }

                                }
                            }




                        }

                    }

                    break;


                case "DELLSERVICEPACKS1":


                    vm.DELLServicePackFullPathS1 = String.Empty;
                    vm.DELLServicePackPathS1 = String.Empty;

                    using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                    {

                        //Requires GUID for MyComputer Or ThisPC Folder DELLSERVICEPACKS1
                        switch (StaticFunctions.Department)
                        {
                            case "GoodsIn":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "GoodsOutTech":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "ITAD":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "TestDept":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;

                        }
                        // //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                        //openFileDialog.Filter = "Update files|*.bin;";// *.flash";
                        openFileDialog.FilterIndex = 0;
                        openFileDialog.Multiselect = false;
                        openFileDialog.RestoreDirectory = true;


                        if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {

                            // Loop through how ever many items are selected and add to the list
                            foreach (var file in openFileDialog.FileNames)
                            {
                                if (File.Exists(file))
                                {

                                    //Get IP of DNS Host
                                    // Get the IP of W drive host
                                    string hostIP = Dns.GetHostEntry("UKHAFS").AddressList[0].ToString();



                                    //CHECK IF UK
                                    if (StaticFunctions.Department != "US" || StaticFunctions.Department != "AUS")
                                    {
                                        int lastSlash = file.LastIndexOf(@"\");
                                        vm.DELLServicePackFullPathS1 = file.Replace(@"W:\", @"//" + hostIP + @"/UK_Warehouse$/").Replace(@"\", "/");
                                        vm.DELLServicePackPathS1 = file.Substring(lastSlash + 1);
                                    }


                                }


                            }
                        }

                    }
                    break;


                case "DELLBIOSS1":


                    vm.BIOSUpgradePath1 = String.Empty;

                    using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                    {

                        //Requires GUID for MyComputer Or ThisPC Folder 
                        switch (StaticFunctions.Department)
                        {
                            case "GoodsIn":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "GoodsOutTech":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "ITAD":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "TestDept":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;

                        }
                        // //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                        //openFileDialog.Filter = "Update files|*.bin;";// *.flash";
                        openFileDialog.FilterIndex = 0;
                        openFileDialog.Multiselect = true;
                        openFileDialog.RestoreDirectory = true;


                        if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {

                            // Loop through how ever many items are selected and add to the list
                            foreach (var file in openFileDialog.FileNames)
                            {
                                if (File.Exists(file))
                                {

                                    //Create a local copy
                                    if (file.Contains(".d7"))
                                    {
                                        File.Copy(file, @"" + mydocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + ServerIP + "BIOSUpdate.d7", true);
                                    }
                                    else
                                    {
                                        File.Copy(file, @"" + mydocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + ServerIP + "BIOSUpdate.exe", true);
                                    }
                                    //ADD CODE TO CREATE RIBCL WITH NEW UPGRADE PATH

                                    // just get filename
                                    int lastSlash = file.LastIndexOf(@"\");

                                    vm.BIOSUpgradePath1 = file.Substring(lastSlash + 1);

                                }


                            }
                        }

                    }
                    break;
                case "DELLCPLDS1":


                    vm.CPLDUpgradePath1 = String.Empty;

                    using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                    {

                        //Requires GUID for MyComputer Or ThisPC Folder 
                        switch (StaticFunctions.Department)
                        {
                            case "GoodsIn":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "GoodsOutTech":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "ITAD":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "TestDept":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;

                        }
                        // //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                        //openFileDialog.Filter = "Update files|*.bin;";// *.flash";
                        openFileDialog.FilterIndex = 0;
                        openFileDialog.Multiselect = true;
                        openFileDialog.RestoreDirectory = true;


                        if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {

                            // Loop through how ever many items are selected and add to the list
                            foreach (var file in openFileDialog.FileNames)
                            {
                                if (File.Exists(file))
                                {

                                    //Create a local copy

                                    File.Copy(file, @"" + mydocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + ServerIP + "CPLDUpdate.exe", true);

                                    //ADD CODE TO CREATE RIBCL WITH NEW UPGRADE PATH

                                    // just get filename
                                    int lastSlash = file.LastIndexOf(@"\");

                                    vm.CPLDUpgradePath1 = file.Substring(lastSlash + 1);

                                    vm.WarningMessage1 = @"PCIe\CPLD Updates are to be run by themselves. Any other updates will be ignored. Please click the BIOS\PCie\Cont button to update.";

                                }


                            }
                        }

                    }
                    break;
                case "DELLCONTS1":

                    vm.CONTUpgradePath1 = String.Empty;

                    // MessageBox.Show("Folder Already Exists");
                    //MessageBox.Show(drives);
                    using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                    {

                        //Requires GUID for MyComputer Or ThisPC Folder
                        switch (StaticFunctions.Department)
                        {
                            case "GoodsIn":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "GoodsOutTech":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "ITAD":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "TestDept":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;

                        }
                        // //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                        //openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                        openFileDialog.FilterIndex = 0;
                        openFileDialog.Multiselect = true;
                        openFileDialog.RestoreDirectory = true;


                        //MessageBox.Show(Environment.SpecialFolder.MyDocuments.ToString());

                        if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {

                            // Loop through how ever many items are selected and add to the list
                            foreach (var file in openFileDialog.FileNames)
                            {
                                if (File.Exists(file))
                                {

                                    //vm.CONTUpgradePath1 = file;
                                    //ADD CODE TO CREATE RIBCL WITH NEW UPGRADE PATH
                                    //Create a local copy
                                    if (file.Contains(".d7"))
                                    {
                                        File.Copy(file, @"" + mydocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + ServerIP + "CONTUpdate.d7", true);
                                    }
                                    else
                                    {
                                        File.Copy(file, @"" + mydocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + ServerIP + "CONTUpdate.exe", true);
                                    }

                                    // just get filename
                                    int lastSlash = file.LastIndexOf(@"\");

                                    vm.CONTUpgradePath1 = file.Substring(lastSlash + 1);
                                }

                            }
                        }

                    }

                    break;
                case "DELLCONT2S1":

                    vm.CONT2UpgradePath1 = String.Empty;

                    // MessageBox.Show("Folder Already Exists");
                    //MessageBox.Show(drives);
                    using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                    {

                        //Requires GUID for MyComputer Or ThisPC Folder
                        switch (StaticFunctions.Department)
                        {
                            case "GoodsIn":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "GoodsOutTech":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "ITAD":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "TestDept":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;

                        }
                        // //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                        //openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                        openFileDialog.FilterIndex = 0;
                        openFileDialog.Multiselect = true;
                        openFileDialog.RestoreDirectory = true;


                        //MessageBox.Show(Environment.SpecialFolder.MyDocuments.ToString());

                        if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {

                            // Loop through how ever many items are selected and add to the list
                            foreach (var file in openFileDialog.FileNames)
                            {
                                if (File.Exists(file))
                                {

                                    //vm.CONTUpgradePath1 = file;
                                    //ADD CODE TO CREATE RIBCL WITH NEW UPGRADE PATH
                                    //Create a local copy
                                    if (file.Contains(".d7"))
                                    {
                                        File.Copy(file, @"" + mydocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + ServerIP + "CONT2Update.d7", true);
                                    }
                                    else
                                    {
                                        File.Copy(file, @"" + mydocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + ServerIP + "CONT2Update.exe", true);
                                    }

                                    // just get filename
                                    int lastSlash = file.LastIndexOf(@"\");

                                    vm.CONT2UpgradePath1 = file.Substring(lastSlash + 1);
                                }

                            }
                        }

                    }

                    break;
                case "DELLIDRACS1":

                    vm.IDRACUpgradePath1 = String.Empty;

                    // MessageBox.Show("Folder Already Exists");
                    //MessageBox.Show(drives);
                    using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                    {

                        //Requires GUID for MyComputer Or ThisPC Folder
                        switch (StaticFunctions.Department)
                        {
                            case "GoodsIn":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "GoodsOutTech":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "ITAD":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;
                            case "TestDept":
                                if (vm.Server1IPv4.Contains("172.26.100") || vm.Server1IPv4.Contains("172.26.200"))
                                {
                                    openFileDialog.InitialDirectory = mydocs;
                                }
                                else
                                {
                                    openFileDialog.InitialDirectory = @"\\pinnacle.local\tech_resources\Server_Updates\DELL";
                                }
                                break;

                        }
                        // //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                        //openFileDialog.Filter = "Bin files (*.bin)|*.bin";
                        openFileDialog.FilterIndex = 0;
                        openFileDialog.Multiselect = true;
                        openFileDialog.RestoreDirectory = true;


                        //MessageBox.Show(Environment.SpecialFolder.MyDocuments.ToString());

                        if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {

                            // Loop through how ever many items are selected and add to the list
                            foreach (var file in openFileDialog.FileNames)
                            {
                                if (File.Exists(file))
                                {


                                    //Create a local copy
                                    if (file.Contains(".d7"))
                                    {
                                        File.Copy(file, @"" + mydocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + ServerIP + "IDRACUpdate.d7", true);

                                        //File.Copy(file, @"" + mydocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\firmimg.d7", true);

                                        // just get filename
                                        int lastSlash = file.LastIndexOf(@"\");

                                        vm.IDRACUpgradePath1 = file.Substring(lastSlash + 1);
                                    }
                                    else if (file.Contains(".d9"))
                                    {
                                        File.Copy(file, @"" + mydocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + ServerIP + "IDRACUpdate.d9", true);

                                        //File.Copy(file, @"" + mydocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\firmimg.d7", true);

                                        // just get filename
                                        int lastSlash = file.LastIndexOf(@"\");

                                        vm.IDRACUpgradePath1 = file.Substring(lastSlash + 1);
                                    }
                                    else
                                    {
                                        // just get filename
                                        int lastSlash = file.LastIndexOf(@"\");

                                        vm.IDRACUpgradePath1 = file.Substring(lastSlash + 1);

                                        File.Copy(file, @"" + mydocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + ServerIP + "IDRACUpdate.exe", true);

                                    }







                                }

                            }





                        }

                    }
                    break;

                case "LenovoBMC":

                    break;

                case "LenovoBIOS":

                    break;
                case "LenovoLXPM":

                    break;




            }//CASE END



        }







        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

    }
}


