﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Forms;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;
using System.Collections.ObjectModel;
using Nebula.Views;

namespace Nebula.Commands
{
    public class AddFoldersCommand : ICommand
    {

        //declare viewmodel
        GoodsInOutViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public AddFoldersCommand(GoodsInOutViewModel TheViewModel)
        {

            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            if (!string.IsNullOrEmpty(vm.ChassisSerialNo) && !string.IsNullOrEmpty(vm.PurchaseOrderNumber))
            {
                return true;
            }
            else
            {
                return false;
            }

        }


       

        public void Execute(object parameter)
        {


            var values = (object)parameter;

          



            //Clear the Loaded Checklist
            vm.LoadedChecklist = null;
            vm.ChecklistMessage = String.Empty;
            //Call the static function to return any file with an xml extension
            //vm.ProductList  = StaticFunctions.GetFileNamesOnly(@"" + vm.dsktop + @"\Invoice App\XML Product Wipe Profolder", "*.xml");
            //  vm.LoadedChecklist = StaticFunctions.GetFileNamesOnlyBasedOnValue(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML Product Wipe Profolder", "*.xml", values.ToString());

            //List<string> filePaths = new List<string>();

            //Get the directory for drives
            string mydocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            //Get the root path location from current date
            string rootpath = string.Empty;
          

            if (StaticFunctions.UncPathToUse.Contains(@"T:\"))
            {
                StaticFunctions.Department = "US";
            }
            if (StaticFunctions.UncPathToUse.Contains(@"W:\Nebula"))
            {
                StaticFunctions.Department = "AUS";
            }
            if (StaticFunctions.UncPathToUse.Contains(@"W:\FR Warehouse\Goods In\Test Reports\"))
            {
                StaticFunctions.Department = "FRA";
            }



            //Check if 
            switch (StaticFunctions.Department)
            {
                case "GoodsIn":
                    rootpath = vm.GetFolderPath(DateTime.Now.ToShortDateString());
                    break;
                case "ITAD":
                    rootpath = @"M:\Test Reports\";
                    break;
                case "US":
                    if (Directory.Exists(@"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports"))
                    {
                        StaticFunctions.UncPathToUse = @"T:\NJ Warehouse\Technical Resources\\TB Tools App\Test Reports\";
                    }
                    else if (Directory.Exists(@"T:\Technical Resources\TB Tools App\Test Reports"))
                    {
                        StaticFunctions.UncPathToUse = @"T:\Technical Resources\\TB Tools App\Test Reports\";
                    }

                    break;
                case "AUS":
                    if (Directory.Exists(@"W:\AU Warehouse\Nebula\TB Tools App\Test Reports"))
                    {
                        rootpath = @"W:\AU Warehouse\Nebula\TB Tools App\Test Reports\";
                    }
                    else if (Directory.Exists(@"W:\Nebula\TB Tools App\Test Reports"))
                    {
                        rootpath = @"W:\Nebula\TB Tools App\Test Reports\";
                    }

                    break;

                case "GERMANY":
                    rootpath = @"W:\DE Warehouse\Goods In\Test Reports\";
                    break;
                case "FRANCE":
                    rootpath = @"W:\FR Warehouse\Goods In\Test Reports\";
                    break;

            }


            //rootpath = mydocs + @"\"; // uncomment to enable saving to my documents
            //MessageBox.Show(drives);
            using (System.Windows.Forms.FolderBrowserDialog openFolderDialog = new System.Windows.Forms.FolderBrowserDialog())
            {

                    //Requires GUID for MyComputer Or ThisPC Folder
                //    openFolderDialog.InitialDirectory = "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}"; //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                //openFileDialog.Filter = "All folder (*.*)|*.*";
                //openFileDialog.FilterIndex = 0;
                //openFileDialog.Multiselect = true;
                //openFileDialog.RestoreDirectory = true;

                string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");

                //MessageBox.Show(rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime);

                if (openFolderDialog.ShowDialog() == DialogResult.OK)
                {

                
                        //Get the path of specified file
                        //Create a new folder
                        if (Directory.Exists(rootpath))
                        {
                            //MessageBox.Show(mydocs + @"\" + vm.ChassisSerialNo + " " + currentDateTime);

                            //set the property to be used with generate checklist command, this property is to be written
                            //as a path in the xml file that goods out will use to copy the checklist to
                            vm.DocumentsSavePath = rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime;

                            //Directory.CreateDirectory(rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime);
                            if (Directory.Exists(vm.DocumentsSavePath))
                            {
                                MessageBox.Show("Folder Already Exists!");

                            }
                            else
                            {
                            Directory.CreateDirectory(vm.DocumentsSavePath);

                            //Move Doesn't Work over different volumes, use COPY function instead
                            //Directory.Move(openFolderDialog.SelectedPath, vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo);


                            //Copy
                            DirectoryCopy(openFolderDialog.SelectedPath, vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo, true);



                            //Call the report window  passin list of string
                            ReportWindow rp1 = new ReportWindow(vm.Gen10reportFiles);

                            rp1.Show();


                            //filePaths.Add(file);
                            // MessageBox.Show(file);
                        }
                            }
                        }
                        else
                        {
                            MessageBox.Show("No Folder Exists To Perform This Operation");

                        }


                        //MessageBox.Show(values.ToString());
                        //vm.ChecklistMessage1 = @"Selected folder added to: " + vm.DocumentsSavePath;
                        switch (values.ToString())
                        {
                            case "Server1":
                                vm.ChecklistMessage1 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server2":
                                vm.ChecklistMessage2 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server3":
                                vm.ChecklistMessage3 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server4":
                                vm.ChecklistMessage4 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server5":
                                vm.ChecklistMessage5 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server6":
                                vm.ChecklistMessage6 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server7":
                                vm.ChecklistMessage7 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server8":
                                vm.ChecklistMessage8 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server9":
                                vm.ChecklistMessage9 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server10":
                                vm.ChecklistMessage10 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server11":
                                vm.ChecklistMessage11 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server12":
                                vm.ChecklistMessage12 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server13":
                                vm.ChecklistMessage13 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server14":
                                vm.ChecklistMessage14 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server15":
                                vm.ChecklistMessage15 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server16":
                                vm.ChecklistMessage16 = @"Selected folder added to: " + vm.DocumentsSavePath;
                                break;

                        }
                                  

                }
            }



        private void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.       
            Directory.CreateDirectory(destDirName);

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                //add the list of string
                if(file.Name.Contains("-hvt-survey.xml") || file.Name.Contains("-testlog.xml"))
                {
                    //add the found reports to the list
                    vm.Gen10reportFiles.Add(@"" + sourceDirName + @"\" + file.Name);
                   // MessageBox.Show(@"" + sourceDirName + @"" + file.Name);
                }


                string tempPath = Path.Combine(destDirName, file.Name);
                file.CopyTo(tempPath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string tempPath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, tempPath, copySubDirs);
                }
            }
        }



    }
 }


  
