﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class WebBrowserCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public WebBrowserCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //if (lv != null)
            //{
            //    if (lv.SelectedItems.Count > 0)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    return true;
            //}
            return true;
        }

        public async void Execute(object parameter)
        {
            //Command="{Binding WebBrowserCommand}" CommandParameter="{Binding ElementName=ServerIPTXTB1,Path=Text}"
            var values = (object)parameter;
            WebBrowser web = Helper.GetDescendantFromName(Application.Current.MainWindow, "WebBrowse1") as WebBrowser;
            Button webbtn = Helper.GetDescendantFromName(Application.Current.MainWindow, "LaunchVirtualConsoleS1") as Button;
            //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            //if (vm.ServerIPv4 != String.Empty)
            //{
            web.Navigate("https://" + values);

            web.Focus();
            webbtn.Focus();
            await PutTaskDelay(500);
            web.Focus();
            await PutTaskDelay(200);
            System.Windows.Forms.SendKeys.SendWait("{F5}");
            // await PutTaskDelay(500);

            //}
        }


        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }
    }
}

