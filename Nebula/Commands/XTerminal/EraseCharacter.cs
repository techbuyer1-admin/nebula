﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Commands.XTerminal
{
    public sealed class EraseCharacter : ControlSequence
    {
        public int Count;

        public EraseCharacter(string bufferData) : base(bufferData)
        {
            this.Count = this.Parameters.GetValue(0, defaultValue: 1);
        }
    }
}
