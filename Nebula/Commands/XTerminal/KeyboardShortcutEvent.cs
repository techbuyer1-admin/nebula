﻿using System;


namespace Nebula.Commands.XTerminal
{
    enum KeyboardShortcut
    {
        OpenConfig = 0,
    }

    sealed class KeyboardShortcutEventArgs : EventArgs
    {
        public KeyboardShortcut Shortcut { get; set; }
    }
}

