﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;


namespace Nebula.Commands
{
    public class DPRemoveLVItemsCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public DPRemoveLVItemsCommand(DriveTestViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            ////cast object to ListView
            var lvValue = (ListView)(object)parameter;


            if (lvValue != null)
            {
                if (lvValue.Items.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

          
        }

        public void Execute(object parameter)
        {

            //Get array of objects 
            var lvValue = (ListView)(object)parameter;


            switch (lvValue.Name)
            {
                case "ImportLV":
                    vm.NetsuiteImportCollection.Clear();
                    break;
                case "ProcessedLV":
                    vm.ScannedSerialCollection.Clear();
                    break;
            }



         
          
        }
    }
}

