﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Forms;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class AddFilesDellCommand : ICommand
    {

        //declare viewmodel
        GoodsInOutViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public AddFilesDellCommand(GoodsInOutViewModel TheViewModel)
        {

            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            if (!string.IsNullOrEmpty(vm.ChassisSerialNo) && !string.IsNullOrEmpty(vm.PurchaseOrderNumber))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void Execute(object parameter)
        {


            var values = (object)parameter;



            //Clear the Loaded Checklist
            vm.LoadedChecklist = null;
            vm.ChecklistMessage = String.Empty;
            //Call the static function to return any file with an xml extension
            //vm.ProductList  = StaticFunctions.GetFileNamesOnly(@"" + vm.dsktop + @"\Invoice App\XML Product Wipe Profiles", "*.xml");
            //  vm.LoadedChecklist = StaticFunctions.GetFileNamesOnlyBasedOnValue(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML Product Wipe Profiles", "*.xml", values.ToString());

            //List<string> filePaths = new List<string>();

            //Get the directory for drives
            string mydocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            //Get the root path location from current date
            string rootpath = string.Empty;



           //Check if 
            switch (StaticFunctions.Department)
            {
                case "GoodsIn":
                    rootpath = vm.GetFolderPath(DateTime.Now.ToShortDateString());
                    //Check if US
                    if (StaticFunctions.UncPathToUse.Contains(@"T:\"))
                    {
                        rootpath = @"T:\Technical Resources\TB Tools App\Test Reports\";
                    }

                    //Check if AUS
                    if (StaticFunctions.UncPathToUse.Contains(@"W:\Nebula"))
                    {
                        rootpath = @"W:\Nebula\TB Tools App\Test Reports\";
                    }
                    break;
                    //Check if FRA
                    if (StaticFunctions.UncPathToUse.Contains(@"W:\FR Warehouse\Goods In\Test Reports\"))
                    {
                        rootpath = @"W:\FR Warehouse\ITAD\Test Reports\";
                    }
                    break;

                case "ITAD":
                    rootpath = @"M:\Test Reports\";
                    //Check if US
                    if (StaticFunctions.UncPathToUse.Contains(@"T:\"))
                    {
                        rootpath = @"T:\Technical Resources\TB Tools App\Test Reports\";
                    }

                    //Check if AUS
                    if (StaticFunctions.UncPathToUse.Contains(@"W:\Nebula"))
                    {
                        rootpath = @"W:\Nebula\TB Tools App\Test Reports\";
                    }
                    break;
                    //Check if FRA
                    if (StaticFunctions.UncPathToUse.Contains(@"W:\FR Warehouse\ITAD\Test Reports\"))
                    {
                        rootpath = @"W:\FR Warehouse\ITAD\Test Reports\";
                    }
                    break;

             
            }

            //MessageBox.Show(rootpath);
            //rootpath = mydocs + @"\"; // uncomment to enable saving to my documents
            //MessageBox.Show(drives);
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {

                //Requires GUID for MyComputer Or ThisPC Folder
                openFileDialog.InitialDirectory = "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}"; //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                openFileDialog.Filter = "All files (*.*)|*.*";
                openFileDialog.FilterIndex = 0;
                openFileDialog.Multiselect = true;
                openFileDialog.RestoreDirectory = true;

                string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");

                //MessageBox.Show(rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime);

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {

                    //if (openFileDialog.FileNames.Count() == 2)
                    //{



                        //Get the path of specified file
                        //Create a new folder
                        if (Directory.Exists(rootpath))
                        {
                           // MessageBox.Show(mydocs + @"\" + vm.ChassisSerialNo + " " + currentDateTime);

                            //set the property to be used with generate checklist command, this property is to be written
                            //as a path in the xml file that goods out will use to copy the checklist to
                            vm.DocumentsSavePath = rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime;

                            //Directory.CreateDirectory(rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime);
                            if (Directory.Exists(vm.DocumentsSavePath))
                            {
                                MessageBox.Show("Folder Already Exists!");

                            }
                            else
                            {
                                Directory.CreateDirectory(vm.DocumentsSavePath);

                                // Loop through how ever many items are selected and add to the list
                                foreach (var file in openFileDialog.FileNames)
                                {
                                    if (File.Exists(file))
                                    {

                                        if (File.Exists(vm.DocumentsSavePath + @"\" + new FileInfo(file).Name.ToString()))
                                        {
                                            if(StaticFunctions.Department == "ITAD")
                                            {
                                            //Rename ITAD Files with [PONUMBER] – [ASSET] – [SERIALNO] Test/Survey.html 
                                            File.Delete(vm.DocumentsSavePath + @"\" + new FileInfo(file).Name.ToString());
                                            File.Copy(file, vm.DocumentsSavePath + @"\" + vm.PurchaseOrderNumber + "-" + vm.ChassisSerialNo + ".pdf");
                                            }
                                            else
                                            {
                                            File.Delete(vm.DocumentsSavePath + @"\" + new FileInfo(file).Name.ToString());
                                            File.Copy(file, vm.DocumentsSavePath + @"\" + new FileInfo(file).Name.ToString());
                                            }

                                           
                                        }
                                        else
                                        {
                                        if (StaticFunctions.Department == "ITAD")
                                        {
                                            File.Copy(file, vm.DocumentsSavePath + @"\" + vm.PurchaseOrderNumber + " - " + vm.ChassisSerialNo + ".pdf");
                                        }
                                        else
                                        {
                                            File.Copy(file, vm.DocumentsSavePath + @"\" + new FileInfo(file).Name.ToString());
                                        }
                                      
                                        }
                                        //MessageBox.Show(mydocs + @"\" + vm.ChassisSerialNo + " " + currentDateTime + @"\" + new FileInfo(file).Name.ToString());

                                        //File.Copy(file, rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime + @"\" + new FileInfo(file).Name.ToString());
                                    }

                                    //filePaths.Add(file);
                                    // MessageBox.Show(file);
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("No Folder Exists To Perform This Operation");

                        }


                        //MessageBox.Show(values.ToString());
                        //vm.ChecklistMessage1 = @"Selected files added to: " + vm.DocumentsSavePath;
                        switch (values.ToString())
                        {
                            case "Server1":
                                vm.ChecklistMessage1 = @"Selected files added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server2":
                                vm.ChecklistMessage2 = @"Selected files added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server3":
                                vm.ChecklistMessage3 = @"Selected files added to: " + vm.DocumentsSavePath;
                                break;
                            case "Server4":
                                vm.ChecklistMessage4 = @"Selected files added to: " + vm.DocumentsSavePath;
                                break;
                        case "Server5":
                            vm.ChecklistMessage5 = @"Selected files added to: " + vm.DocumentsSavePath;
                            break;
                        case "Server6":
                            vm.ChecklistMessage6 = @"Selected files added to: " + vm.DocumentsSavePath;
                            break;
                        case "Server7":
                            vm.ChecklistMessage7 = @"Selected files added to: " + vm.DocumentsSavePath;
                            break;
                        case "Server8":
                            vm.ChecklistMessage8 = @"Selected files added to: " + vm.DocumentsSavePath;
                            break;
                        case "Server9":
                            vm.ChecklistMessage9 = @"Selected files added to: " + vm.DocumentsSavePath;
                            break;
                        case "Server10":
                            vm.ChecklistMessage10 = @"Selected files added to: " + vm.DocumentsSavePath;
                            break;
                        case "Server11":
                            vm.ChecklistMessage11 = @"Selected files added to: " + vm.DocumentsSavePath;
                            break;
                        case "Server12":
                            vm.ChecklistMessage12 = @"Selected files added to: " + vm.DocumentsSavePath;
                            break;
                        case "Server13":
                            vm.ChecklistMessage13 = @"Selected files added to: " + vm.DocumentsSavePath;
                            break;
                        case "Server14":
                            vm.ChecklistMessage14 = @"Selected files added to: " + vm.DocumentsSavePath;
                            break;
                        case "Server15":
                            vm.ChecklistMessage15 = @"Selected files added to: " + vm.DocumentsSavePath;
                            break;
                        case "Server16":
                            vm.ChecklistMessage16 = @"Selected files added to: " + vm.DocumentsSavePath;
                            break;

                    }

                    //}//count
                    //else
                    //{
                    //    MessageBox.Show("A minimum of 2 Server Reports are required to complete this operation. Please click the Add Reports button again to select a minimum of 2 reports.");
                    //}


                }
            }



        }
    }
}
