﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Nebula.Paginators;
using Nebula.ViewModels;
using Nebula.Helpers;
using System.Windows.Documents;
using System.Windows.Controls;
using System.IO;
using System.Configuration;
using System.Windows.Threading;
using Nebula.Views;
using Nebula.Models;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Data.SQLite;
using System.Data;

namespace Nebula.Commands
{
    public class PrintSaveDocCommand : ICommand
    {

        //declare viewmodel
        //GoodsInOutViewModel vm = null;

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        //public PrintSaveDocCommand(GoodsInOutViewModel TheViewModel)
        //{
        //    this.vm = TheViewModel;
        //}

        public PrintSaveDocCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }

        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if(String.IsNullOrEmpty(vm.PONumber) && String.IsNullOrEmpty(vm.SONumber) || vm.ChecklistTicked == false)
            {
                return false;

              

            }
            else
            {

                return true;
            }

        }


      



        public async void Execute(object parameter)
        {
            try
            {

                //var values = (object)parameter;

                var values = (object[])parameter;
                //Split array into specific types
                var checklist = values[0];
                var itadreport = values[1];

                //Set Serial Number
                vm.ChassisSerialNo = vm.ChassisSerialNo;
                //Reset Standard Message
                vm.ChecklistMessage = "";
                //Reset Warning Message
                vm.ChecklistWarningMessage = "";

            //GEN10 TEST COMMENT OUT
            //List<string> docList = new List<string>();
            //docList.Add(@"C:\Users\n.myers\Documents\2021-09-22-15_29_08-CZ29100C9G-hvt_files\2021-09-22-15_29_08-CZ29100C9G-testlog.xml");
            ////docList.Add(@"C:\Users\n.myers\Documents\2021-09-22-15_29_08-CZ29100C9G-hvt_files\2021-09-22-15_29_08-CZ29100C9G-testlog.xml");
            //ReadGen10Reports(docList,vm.myDocs);
            //MessageBox.Show("Done");
            //END GEN10 REPORT TEST
            //vm.GenGenericServerInfo1.Model = "DL 380 Gen10";


            if (StaticFunctions.Department == "ITAD")
            {

                    //ADD SPECIFIC ITAD


                    if (string.IsNullOrWhiteSpace(vm.PONumber))
                    {

                    }
                    else
                    {
                        if (vm.PONumber.Contains("lot") || vm.PONumber.Contains("LOT") && vm.PONumber.Contains(" - "))
                        {
                            vm.PONumber = vm.PONumber.ToUpper();
                            //update DB Field
                            vm.POSONumberForDB = vm.PONumber;
                        }
                        else
                        {
                            vm.ChecklistMessage = "Please include the LOT - Asset Prefix. e.g LOT123456 - 12345";
                            return;
                        }
                    }

                }
            else
            {
                    if (string.IsNullOrWhiteSpace(vm.PONumber))
                    {

                    }
                    else
                    {
                        if (vm.PONumber.Contains("po") || vm.PONumber.Contains("PO"))
                        {
                            vm.PONumber = vm.PONumber.ToUpper();
                            //update DB Field
                            vm.POSONumberForDB = vm.PONumber;
                        }
                        else
                        {
                            vm.ChecklistMessage = "Please include the PO Prefix. e.g PO123456";
                            return;
                        }
                    }


                    if (string.IsNullOrWhiteSpace(vm.SONumber))
                    {

                    }
                    else
                    {

                        if (vm.SONumber.Contains("so") || vm.SONumber.Contains("SO") || vm.SONumber.Contains("to") || vm.SONumber.Contains("TO") || vm.SONumber.Contains("sp") || vm.SONumber.Contains("SP"))
                        {
                            vm.SONumber = vm.SONumber.ToUpper();
                            //update DB Field
                            vm.POSONumberForDB = vm.SONumber;
                        }
                        else
                        {
                            vm.ChecklistMessage = @"Please include the SO or TO Prefix. e.g SO123456";
                            return;
                        }
                    }


                }








                //Application.Current.Dispatcher.Invoke(UpdateIndicator, DispatcherPriority.ContextIdle);

                //Set busy indicator
             vm.SaveDocIndicator = true;

            //Get the directory for drives
            string mydocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            //Get the root path location from current date
            string rootpath = string.Empty;
            //CurrentDateTime
            string currentDateTime = DateTime.Now.ToString().Replace(@"/", "-").Replace(":", "_");
            vm.ChecklistMessage = "Saving, Please Wait...";



            await PutTaskDelay(200);

                //CAUSED AN ISSUE WITH US, THE DEPARTMENT WAS RESET
                ////Find Save Location of Department
                //if (StaticFunctions.UncPathToUse.Contains(@"T:\"))
                //{
                //    StaticFunctions.Department = "US";
                //}
                //else if (StaticFunctions.UncPathToUse.Contains(@"W:\Nebula"))
                //{
                //    StaticFunctions.Department = "AUS";
                //}



                //Check if 
           switch (StaticFunctions.Department)
            {
                case "GoodsIn":
                    if(vm.CurrentUserLocation == "United Kingdom")
                        {
                            rootpath = vm.GetFolderPath(DateTime.Now);
                        }

                        //Check if US
                        if (vm.CurrentUserLocation == "United States")
                        {
                            if(Directory.Exists(@"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports"))
                            {
                                rootpath = @"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports\";
                            }
                            else if (Directory.Exists(@"T:\Technical Resources\TB Tools App\Test Reports"))
                            {
                                rootpath = @"T:\Technical Resources\TB Tools App\Test Reports\";
                            }
                            
                        }

                        //Check if AUS
                        if (vm.CurrentUserLocation == "Australia")
                        {
                            if (Directory.Exists(@"W:\AU Warehouse\Reports"))
                            {
                                rootpath = @"W:\AU Warehouse\Reports\";
                            }
                            else if (Directory.Exists(@"W:\Reports"))
                            {
                                rootpath = @"W:\Reports\";
                            }
                           
                        }

                        //Check if FRA
                        if (vm.CurrentUserLocation == "France")
                        {
                            rootpath = @"W:\FR Warehouse\Goods In\Test Reports\";
                        }

                        //Check if de
                        if (vm.CurrentUserLocation == "Germany")
                        {
                            rootpath = @"W:\DE Warehouse\Goods In\Test Reports\";
                        }


                        break;
                case "GoodsOutTech":
                        if (vm.CurrentUserLocation == "United Kingdom")
                        {
                            rootpath = vm.GetGOTFolderPath(DateTime.Now, vm.SONumber);
                        }
                       

                        //Check if US
                        if (vm.CurrentUserLocation == "United States")
                        {
                            if (Directory.Exists(@"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports"))
                            {
                                rootpath = @"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports\";
                            }
                            else if (Directory.Exists(@"T:\Technical Resources\TB Tools App\Test Reports"))
                            {
                                rootpath = @"T:\Technical Resources\TB Tools App\Test Reports\";
                            }
                        }

                        //Check if AUS
                        if (vm.CurrentUserLocation == "Australia")
                        {
                            if (Directory.Exists(@"W:\AU Warehouse\Reports"))
                            {
                                rootpath = @"W:\AU Warehouse\Reports\";
                            }
                            else if (Directory.Exists(@"W:\Reports"))
                            {
                                rootpath = @"W:\Reports\";
                            }
                        }

                        //Check if FRA
                        if (vm.CurrentUserLocation == "France")
                        {
                            rootpath = @"W:\FR Warehouse\Goods Out Tech\TGO Photos\";
                        }

                        //Check if de
                        if (vm.CurrentUserLocation == "Germany")
                        {
                            rootpath = @"W:\DE Warehouse\Goods In\Test Reports\";
                        }

                        break;
                case "TestDept":
                    if (vm.CurrentUserLocation == "United Kingdom")
                    {
                        rootpath = vm.GetFolderPath(DateTime.Now);
                    }

                    //Check if US
                    if (vm.CurrentUserLocation == "United States")
                    {
                            if (Directory.Exists(@"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports"))
                            {
                                rootpath = @"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports\";
                            }
                            else if (Directory.Exists(@"T:\Technical Resources\TB Tools App\Test Reports"))
                            {
                                rootpath = @"T:\Technical Resources\TB Tools App\Test Reports\";
                            }
                    }

                    //Check if AUS
                    if (vm.CurrentUserLocation == "Australia")
                    {
                            if (Directory.Exists(@"W:\AU Warehouse\Reports"))
                            {
                                rootpath = @"W:\AU Warehouse\Reports\";
                            }
                            else if (Directory.Exists(@"W:\Reports"))
                            {
                                rootpath = @"W:\Reports\";
                            }
                        }

                    //Check if FRA
                    if (vm.CurrentUserLocation == "France")
                    {
                        rootpath = @"W:\FR Warehouse\Goods In\Test Reports\";
                    }

                    //Check if de
                    if (vm.CurrentUserLocation == "Germany")
                    {
                        rootpath = @"W:\DE Warehouse\Goods In\Test Reports\";
                    }


                    break;
                case "ITAD":
                        if (vm.CurrentUserLocation == "United Kingdom")
                        {
                            rootpath = @"M:\Technical Test Reports\Nebula Test Reports\";
                        }


                        //Check if US
                        if (vm.CurrentUserLocation == "United States")
                        {
                            if (Directory.Exists(@"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports"))
                            {
                                rootpath = @"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports\";
                            }
                            else if (Directory.Exists(@"T:\Technical Resources\TB Tools App\Test Reports"))
                            {
                                rootpath = @"T:\Technical Resources\TB Tools App\Test Reports\";
                            }
                        }

                        //Check if AUS
                        if (vm.CurrentUserLocation == "Australia")
                        {
                            if (Directory.Exists(@"W:\AU Warehouse\Reports"))
                            {
                                rootpath = @"W:\AU Warehouse\Reports\";
                            }
                            else if (Directory.Exists(@"W:\Reports"))
                            {
                                rootpath = @"W:\Reports\";
                            }
                        }


                        //Check if FRA
                        if (vm.CurrentUserLocation == "France")
                        {
                            rootpath = @"W:\FR Warehouse\ITAD\Test Reports\";
                        }

                        //Check if de
                        if (vm.CurrentUserLocation == "Germany")
                        {
                            rootpath = @"W:\DE Warehouse\Goods In\Test Reports\";
                        }

                        //MessageBox.Show(rootpath);

                        break;



                }


            if(vm.SaveToMyDocs == true)
            {
                //Save to users documents folder
                rootpath = mydocs + @"\"; // uncomment to enable saving to my documents
                                          //MessageBox.Show(drives);
            }


       



                //Check if Goods Out Tech and Skip The HP File Requirement
                if (StaticFunctions.Department != "GoodsOutTech")
                {
                    //HP GEN8 GEN9 SAVE LOCATION
                    if (vm.GenGenericServerInfo1 != null)
                    if (vm.GenGenericServerInfo1.Model.Contains("Gen8") || vm.GenGenericServerInfo1.Model.Contains("Gen9"))
                    {
               

                        vm.ChecklistMessage = "Please select the 2 HP Reports saved from Intelligent Provisioning.";

                        //wait until data populated
                        await PutTaskDelay(1000);

                        vm.ChecklistMessage = @"Saving, Please Wait...";


                        using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                        {

                            //Requires GUID for MyComputer Or ThisPC Folder
                            openFileDialog.InitialDirectory = "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}"; //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                            openFileDialog.Filter = "All files (*.*)|*.*";
                            openFileDialog.FilterIndex = 0;
                            openFileDialog.Multiselect = true;
                            openFileDialog.RestoreDirectory = true;



                            //MessageBox.Show(rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime);

                            //HP Servers Launch the File Prompt  || vm.Gen10ServerInfo1 != null


                            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {

                                if (openFileDialog.FileNames.Count() == 2)
                                {



                                    //Get the path of specified file
                                    //Create a new folder
                                    if (Directory.Exists(rootpath))
                                    {
                                        //MessageBox.Show(mydocs + @"\" + vm.ChassisSerialNo + " " + currentDateTime);


                                        if (StaticFunctions.Department == "ITAD")
                                        {
                                            //set the property to be used with generate checklist command, this property is to be written
                                            //as a path in the xml file that goods out will use to copy the checklist to
                                            vm.DocumentsSavePath = rootpath + vm.POSONumber + " - " + vm.ChassisSerialNo + " Test Reports " + currentDateTime;
                                        }
                                        else
                                        {
                                            //set the property to be used with generate checklist command, this property is to be written
                                            //as a path in the xml file that goods out will use to copy the checklist to
                                            vm.DocumentsSavePath = rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime;
                                        }
                                           

                                        //Directory.CreateDirectory(rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime);
                                        if (Directory.Exists(vm.DocumentsSavePath))
                                        {
                                            
                                            //MessageBox.Show("Folder Already Exists!");
                                            vm.ChecklistMessage = "Folder Already Exists!";

                                            return;
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(vm.DocumentsSavePath);

                                            // Loop through how ever many items are selected and add to the list
                                            foreach (var file in openFileDialog.FileNames)
                                            {
                                                if (File.Exists(file))
                                                {

                                                    if (File.Exists(vm.DocumentsSavePath + @"\" + new FileInfo(file).Name.ToString()))
                                                    {
                                                        if (StaticFunctions.Department == "ITAD")
                                                        {
                                                            //Rename ITAD Files with [PONUMBER] – [ASSET] – [SERIALNO] Test/Survey.html 
                                                            File.Delete(vm.DocumentsSavePath + @"\" + new FileInfo(file).Name.ToString());

                                                            if (new FileInfo(file).Name.ToString().Contains("Survey") || new FileInfo(file).Name.ToString().Contains("survey") || new FileInfo(file).Name.ToString().Contains("overview") || new FileInfo(file).Name.ToString().Contains("Overview") || new FileInfo(file).Name.ToString().Contains("Summary") || new FileInfo(file).Name.ToString().Contains("summary"))
                                                            {
                                                                File.Copy(file, vm.DocumentsSavePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo + " survey.html");
                                                            }
                                                            else if (new FileInfo(file).Name.ToString().Contains("test"))
                                                            {

                                                                File.Copy(file, vm.DocumentsSavePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo + " test.html");
                                                            }

                                                        }
                                                        else
                                                        {
                                                            File.Delete(vm.DocumentsSavePath + @"\" + new FileInfo(file).Name.ToString());
                                                            File.Copy(file, vm.DocumentsSavePath + @"\" + new FileInfo(file).Name.ToString());
                                                        }


                                                    }
                                                    else
                                                    {
                                                        if (StaticFunctions.Department == "ITAD")
                                                        {
                                                            //Rename ITAD Files with [PONUMBER] – [ASSET] – [SERIALNO] Test/Survey.html 


                                                            if (new FileInfo(file).Name.ToString().Contains("Survey") || new FileInfo(file).Name.ToString().Contains("survey") || new FileInfo(file).Name.ToString().Contains("overview") || new FileInfo(file).Name.ToString().Contains("Overview") || new FileInfo(file).Name.ToString().Contains("Summary") || new FileInfo(file).Name.ToString().Contains("summary"))
                                                            {
                                                                File.Copy(file, vm.DocumentsSavePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo + " survey.html");
                                                            }
                                                            else if (new FileInfo(file).Name.ToString().Contains("test"))
                                                            {

                                                                File.Copy(file, vm.DocumentsSavePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo + " test.html");
                                                            }

                                                        }
                                                        else
                                                        {
                                                            File.Copy(file, vm.DocumentsSavePath + @"\" + new FileInfo(file).Name.ToString());
                                                        }



                                                    }
                                                    //MessageBox.Show(mydocs + @"\" + vm.ChassisSerialNo + " " + currentDateTime + @"\" + new FileInfo(file).Name.ToString());

                                                    //File.Copy(file, rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime + @"\" + new FileInfo(file).Name.ToString());
                                                }

                                                //filePaths.Add(file);
                                                // MessageBox.Show(file);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        vm.ChecklistMessage = "No Folder Exists To Perform This Operation";
                                        //MessageBox.Show("No Folder Exists To Perform This Operation");
                                        vm.ChecklistMessage = "";
                                        //reset save to docs 
                                        vm.SaveToMyDocs = false;
                                        return;
                                    }



                                }//count
                                else
                                {
                                    vm.ChecklistMessage = "A minimum of 2 Server Reports are required to complete this operation. Please click the Add Reports button again to select a minimum of 2 reports.";
                                    //MessageBox.Show("A minimum of 2 Server Reports are required to complete this operation. Please click the Add Reports button again to select a minimum of 2 reports.");
                                    vm.ChecklistMessage = "";
                                    //reset save to docs 
                                    vm.SaveToMyDocs = false;
                                    return;
                                }


                            }
                            else
                            {
                                //vm.ChecklistMessage = "";
                                ////reset save to docs 
                                //vm.SaveToMyDocs = false;
                                //return;
                            }



                        }


                    }
                    //HP Gen10 Reports
                    else if (vm.GenGenericServerInfo1.Model.Contains("Gen10"))
                    {

                        using (System.Windows.Forms.FolderBrowserDialog openFolderDialog = new System.Windows.Forms.FolderBrowserDialog())
                        {

                            //Requires GUID for MyComputer Or ThisPC Folder
                            //    openFolderDialog.InitialDirectory = "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}"; //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                            //openFileDialog.Filter = "All folder (*.*)|*.*";
                            //openFileDialog.FilterIndex = 0;
                            //openFileDialog.Multiselect = true;
                            //openFileDialog.RestoreDirectory = true;

                            // string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");

                            vm.ChecklistMessage = @"Please select the Folder containing the HP Gen10 Summary and TestLog reports. Both reports are required!";
                            
                            //wait until data populated
                            await PutTaskDelay(1000);

                            vm.ChecklistMessage = @"Saving, Please Wait...";

                            if (openFolderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                

                                //Get the path of specified file
                                //Create a new folder
                                if (Directory.Exists(rootpath))
                                {
                                    //MessageBox.Show(vm.DocumentsSavePath);

                                    if (StaticFunctions.Department == "ITAD")
                                    {
                                        //set the property to be used with generate checklist command, this property is to be written
                                        //as a path in the xml file that goods out will use to copy the checklist to
                                        vm.DocumentsSavePath = rootpath + vm.POSONumber + " - " + vm.ChassisSerialNo + " Test Reports " + currentDateTime;
                                    }
                                    else
                                    {
                                        //set the property to be used with generate checklist command, this property is to be written
                                        //as a path in the xml file that goods out will use to copy the checklist to
                                        vm.DocumentsSavePath = rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime;
                                    }


                                    //Directory.CreateDirectory(rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime);
                                    if (Directory.Exists(vm.DocumentsSavePath))
                                    {
                                        vm.ChecklistMessage = "Folder Already Exists!";
                                        //MessageBox.Show("Folder Already Exists!");

                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(vm.DocumentsSavePath);

                                        //Move Doesn't Work over different volumes, use COPY function instead
                                        //Directory.Move(openFolderDialog.SelectedPath, vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo);


                                        //Copy
                                        DirectoryCopy(openFolderDialog.SelectedPath, vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo, true);



                                      

                                        //Create pdf from xml data 
                                        ReadGen10Reports(vm.Gen10reportFiles, vm.DocumentsSavePath);

                                    

                                        ////Call the report window  passin list of string
                                        //ReportWindow rp1 = new ReportWindow(vm.Gen10reportFiles);

                                        //rp1.Show();


                                        //filePaths.Add(file);
                                        //MessageBox.Show("GEN10");
                                    }
                                }
                            }
                            else
                            {
                                //MessageBox.Show("No Folder Exists To Perform This Operation");
                                //vm.ChecklistMessage = "";
                                ////reset save to docs 
                                //vm.SaveToMyDocs = false;
                                //return;
                            }





                        }
                    }
                }



                //Check Gen 10 IP Summary files threw an error
                if (vm.ChecklistWarningMessage == "Error in Intelligent Provisioning Files. Returned Data Null! Please check file sizes of hvt-survey.xml & testlog.xml.")
                {
                    Mouse.OverrideCursor = Cursors.Arrow;
                    return;

                }
                else
                {
                  

                    //Change mouse cursor 
                    Mouse.OverrideCursor = Cursors.Wait;

                    //DELL, LENOVO, CISCO
                    if (Directory.Exists(rootpath))
                    {
                        //MessageBox.Show(mydocs + @"\" + vm.ChassisSerialNo + " " + currentDateTime);


                        if (StaticFunctions.Department == "ITAD")
                        {
                            //set the property to be used with generate checklist command, this property is to be written
                            //as a path in the xml file that goods out will use to copy the checklist to
                            vm.DocumentsSavePath = rootpath + vm.POSONumber + " - " + vm.ChassisSerialNo + " Test Reports " + currentDateTime;
                        }
                        else
                        {
                            //set the property to be used with generate checklist command, this property is to be written
                            //as a path in the xml file that goods out will use to copy the checklist to
                            vm.DocumentsSavePath = rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime;
                        }


                        //Directory.CreateDirectory(rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime);
                        if (Directory.Exists(vm.DocumentsSavePath))
                        {
                            //MessageBox.Show("Folder Already Exists!");

                        }
                        else
                        {
                            Directory.CreateDirectory(vm.DocumentsSavePath);

                            await PutTaskDelay(2000);

                        }
                    }
                    else
                    {
                        //MessageBox.Show("No Folder Exists To Perform This Operation");
                        // vm.ChecklistMessage = "";
                        //reset save to docs 
                        //vm.SaveToMyDocs = false;
                        //return;
                    }




                    //PDF FILE OF CHECKLIST & Custom Test Report

                    DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();



                    ////Set the Chassis number to text on the clipboard  Use control v
                    if (vm != null)
                    {
                        if (!string.IsNullOrEmpty(vm.ChassisSerialNo))
                            Clipboard.SetText(vm.ChassisSerialNo.Trim() + " Checklist", TextDataFormat.UnicodeText);
                    }

                    // await PutTaskDelay(1000);
                    //Print to PDF Printer
                    //dpw.SaveDocPDF(vm.FlowDocToPrint);

                    //Anything Else Dell Lenovo Cisco that have custom Test Reports
                    if (vm.DellServerS1 != null || vm.LenovoServerS1 != null || vm.GenGenericServerInfo1 != null)
                    {

                        if (StaticFunctions.Department == "ITAD")
                        {
                            //user to select folder to save too
                            dpw.FlowDocument2PdfServerReports(vm.FlowDocToPrint, vm.DocumentsSavePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_Report.pdf");
                        }
                        else
                        {
                            //ADD GOT Specific items
                            if (StaticFunctions.Department == "GoodsOutTech")
                            {

                                //Save Server Test Report
                                dpw.FlowDocument2PdfServerReports(vm.FlowDocToPrint, vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo.Trim() + "_Report.pdf");
                                //Save Server Certificate
                                dpw.FlowDocument2PdfServerReports(vm.FlowDocToPrint2, vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo.Trim() + "_Certificate.pdf");
                            }
                            else
                            {
                                //Save Server Test Report
                                dpw.FlowDocument2PdfServerReports(vm.FlowDocToPrint, vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo.Trim() + "_Report.pdf");
                            }

                              
                        }



                        //Create instance of Report View
                        if (vm.DellServerS1 != null)
                        {
                            vm.ReportView1 = new DellReportsView(vm);
                        }

                        if (vm.GenGenericServerInfo1 != null)
                        {
                            vm.ReportView1 = new HPReportsView(vm);
                        }

                        if (vm.LenovoServerS1 != null)
                        {
                            vm.ReportView1 = new LenovoReportsView(vm);
                        }

                    }

                //ADD GOT Specific items
                if (StaticFunctions.Department == "GoodsOutTech")
                {

                }
                else
                {
                    //*** Checklist QR CODE ***
                    //Call Embed QR Saves & extracts QR Code to File. This is required for the cloning of the flowdocument as it requires a full path
                    vm.ChecklistQRCode = vm.EmbedChecklistQR(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.ChassisSerialNo.Trim() + ".png", vm.ChecklistQRCode);
                }
              



                    // await PutTaskDelay(2000);

                    //Print Checklist & ITAD Report
                    if (StaticFunctions.Department == "ITAD")
                    {
                        //Create the checklist PDF from the Flow Document and save to a fixed path.
                        dpw.FlowDocument2Pdf((FlowDocument)checklist, vm.DocumentsSavePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_Checklist.pdf");

                        //Create the checklist PDF from the Flow Document and save to a fixed path.
                        dpw.FlowDocument2Pdf((FlowDocument)itadreport, vm.DocumentsSavePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_ITADReport.pdf");

                        //Insert Data to ITAD_Reporting DB
                        //ADD DB ENTRY HERE
                        InsertData();
                    }
                    else
                    {

                        //ADD GOT Specific items
                       if (StaticFunctions.Department == "GoodsOutTech")
                       {
                         //SKIP ANY CHECKLIST
                       }
                       else
                       {

                        //Create the PDF from the Flow Document and save to a fixed path.
                        dpw.FlowDocument2Pdf((FlowDocument)checklist, vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo.Trim() + "_Checklist.pdf");
                       }

                       
                    }



                    // dpw.FlowDocument2PdfServerReports((FlowDocument)values, vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo.Trim() + "_Checklist.pdf");

                    //wait until data populated
                    await PutTaskDelay(2500);




                    vm.ChecklistMessage = "Files Saved To >>> " + vm.DocumentsSavePath;



                    if (StaticFunctions.Department == "ITAD")
                    {
                        ////Launch file for printing
                        if (File.Exists(vm.DocumentsSavePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_Checklist.pdf"))
                        {
                            System.Diagnostics.Process.Start(vm.DocumentsSavePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_Checklist.pdf");

                        }

                        if (File.Exists(vm.DocumentsSavePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_Report.pdf"))
                        {
                            System.Diagnostics.Process.Start(vm.DocumentsSavePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_Report.pdf");

                        }
                        //Launch ITAD Report
                        if (File.Exists(vm.DocumentsSavePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_ITADReport.pdf"))
                        {
                            System.Diagnostics.Process.Start(vm.DocumentsSavePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_ITADReport.pdf");

                        }
                    }
                    else
                    {
                        ////Launch file for printing 
                        if (File.Exists(vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo.Trim() + "_Checklist.pdf"))
                        {
                            System.Diagnostics.Process.Start(vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo.Trim() + "_Checklist.pdf");

                        }


                        if (File.Exists(vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo.Trim() + "_Report.pdf"))
                        {
                            System.Diagnostics.Process.Start(vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo.Trim() + "_Report.pdf");

                        }

                        if (File.Exists(vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo.Trim() + "_Certificate.pdf"))
                        {
                            System.Diagnostics.Process.Start(vm.DocumentsSavePath + @"\" + vm.ChassisSerialNo.Trim() + "_Certificate.pdf");

                        }
                    }

                    //Clear TEMP QR Codes vm.myDocs + @"\Nebula Logs\QR\ITADQR_" + vm.ChassisSerialNo.Trim() + ".png"

                    //Remove Temp QR Code Image
                    if (File.Exists(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.ChassisSerialNo.Trim() + ".png"))
                    {
                        File.Delete(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.ChassisSerialNo.Trim() + ".png");
                    }
                    //Remove ITAD Temp QR Code Image
                    if (File.Exists(vm.myDocs + @"\Nebula Logs\QR\ITADQR_" + vm.ChassisSerialNo.Trim() + ".png"))
                    {
                        File.Delete(vm.myDocs + @"\Nebula Logs\QR\ITADQR_" + vm.ChassisSerialNo.Trim() + ".png");
                    }



                    //reset save to docs 
                    vm.SaveToMyDocs = false;
                    //await PutTaskDelay(2000);
                    ////Move to Next Slide
                    //vm.FlipView1Index = vm.FlipView1Index + 1;
                    vm.SaveDocIndicator = false;

                    //Change back to default
                    Mouse.OverrideCursor = null;
                }


                //Write Log Entry

                //Create a text file to hold log entries for that day
                if (Directory.Exists(@"" + StaticFunctions.UncPathToUse + @"Nebula\Server Logs"))
                {
                    try
                    {
                        File.WriteAllText(@"" + StaticFunctions.UncPathToUse + @"Nebula\Server Logs\DOCSAVE_" + vm.CurrentUserInitials + "_" + vm.POSONumber.Replace(@"\", "").Replace(@"/", "") + "_" + vm.ChassisSerialNo + ".txt", "Server: " + DateTime.Now.ToShortDateString() + " | " + vm.POSONumber + " | " + vm.ChassisSerialNo + " | " + vm.CurrentUserInitials);
                    }
                    catch (IOException ex)
                    {

                        Console.WriteLine(ex.Message);
                    }

                }
                else
                {

                    if (Directory.Exists(@"" + StaticFunctions.UncPathToUse + @"Nebula"))
                    {
                        // Create Directory
                        Directory.CreateDirectory(@"" + StaticFunctions.UncPathToUse + @"Nebula\Server Logs");

                        //Create Log
                        if (Directory.Exists(@"" + StaticFunctions.UncPathToUse + @"Nebula\Server Logs"))
                        {
                            try
                            {
                                File.WriteAllText(@"" + StaticFunctions.UncPathToUse + @"Nebula\Server Logs\DOCSAVE_" + vm.CurrentUserInitials + "_" + vm.POSONumber.Replace(@"\", "").Replace(@"/", "") + "_" + vm.ChassisSerialNo + ".txt", "Server: " + DateTime.Now.ToShortDateString() + " | " + vm.POSONumber + " | " + vm.ChassisSerialNo + " | " + vm.CurrentUserInitials);
                            }
                            catch (IOException ex)
                            {

                                Console.WriteLine(ex.Message);
                            }

                        }
                    }
                    else
                    {
                        // Create Directory
                        Directory.CreateDirectory(@"" + StaticFunctions.UncPathToUse + @"Server Logs");

                        //Create Log
                        if (Directory.Exists(@"" + StaticFunctions.UncPathToUse + @"Server Logs"))
                        {
                            try
                            {

                                File.WriteAllText(@"" + StaticFunctions.UncPathToUse + @"Server Logs\DOCSAVE_" + vm.CurrentUserInitials + "_" + vm.POSONumber.Replace(@"\", "").Replace(@"/", "") + "_" + vm.ChassisSerialNo + ".txt", "Server: " + DateTime.Now.ToShortDateString() + " | " + vm.POSONumber + " | " + vm.ChassisSerialNo + " | " + vm.CurrentUserInitials);
                            }
                            catch (IOException ex)
                            {

                                Console.WriteLine(ex.Message);
                            }

                        }
                    }


                }

            }
            catch (Exception ex)
            {
                vm.SystemMessageDialog("Nebula System Notification", ex.Message);
                //MessageBox.Show(ex.Message);
            }
        }

        //Create Connection to DB
        static SQLiteConnection CreateConnection(string pathtodb)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sqlite_conn;
        }



        public void InsertData()
        {
            try
            {

      
            SQLiteConnection sqlite_conn;
            //SQL Lite DB LINK
            // SET Database Path
            //Production DB
            string cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\ITAD_Reporting.db";
            //Test DB
            //string cs = @"URI=file:C:\Users\n.myers\Desktop\Data\ITAD_Reporting.db";


            //Refresh Current User
            vm.CurrentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, 3).Replace(".", "").ToUpper();

            //Refresh full user
            vm.FullUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").ToUpperUserFullName();



            sqlite_conn = CreateConnection(cs);
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.CommandText = "INSERT INTO ReportRecords (DateCreated, POSONumber, AssetNo, SerialNo, Manufacturer, ModelNo, DeviceCategory, ProcessPerformed, TestingPerformed, Notes, ReportBody, SavePath, TestedBy, VerifiedBy, VerificationPerformed)" + " VALUES(@datecreated,@posonumber,@assetno,@serialno,@manufacturer,@modelno,@devicecategory,@processperformed,@testingperformed,@notes,@reportbody,@savepath,@testedby,@verifiedby,@verificationperformed)";
            sqlite_cmd.CommandType = CommandType.Text;

            //LOOP THROUGH ADDED COLLECTION
            //foreach (NetsuiteInventory itm in vm.ScannedSerialCollection)
            //{


            string lotNumber = "";
            string assetNumber = "";
            //split on hyphen
            var splitLines = vm.POSONumber.Split('-');

            lotNumber = splitLines[0].Trim();
            assetNumber = splitLines[1].Trim();
           
            //Date must be in this format to work with SQL Lite
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@datecreated", DateTime.Now.ToString("yyyy-MM-dd H:mm:ss")));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@posonumber", lotNumber));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@assetno", assetNumber));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", vm.ChassisSerialNo));
            //Check Manufacturer and Model
            if (vm.DellServerS1 != null)
            {
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", vm.DellServerS1.Manufacturer));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@modelno", vm.DellServerS1.Model)); 
            }

            //if (vm.Gen8ServerInfo1 != null)
            //{
            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", vm.Gen8ServerInfo1.Manufacturer));
            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@modelno", vm.Gen8ServerInfo1.Model));
            //}
            //if (vm.Gen9ServerInfo1 != null)
            //{
            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", vm.Gen9ServerInfo1.Manufacturer));
            //    sqlite_cmd.Parameters.Add(new SQLiteParameter("@modelno", vm.Gen9ServerInfo1.Model));
            //}

            if (vm.GenGenericServerInfo1 != null)
            {
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", vm.GenGenericServerInfo1.Manufacturer));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@modelno", vm.GenGenericServerInfo1.Model));
            }

            if (vm.LenovoServerS1 != null)
            {
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", vm.LenovoServerS1.Manufacturer));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@modelno", vm.LenovoServerS1.Model));
            }

            if (vm.CiscoServerS1 != null)
            {
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", vm.CiscoServerS1.Manufacturer));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@modelno", vm.CiscoServerS1.Model));
            }

            sqlite_cmd.Parameters.Add(new SQLiteParameter("@devicecategory", "Server"));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@processperformed", "Recommended GUI Erasure Commands"));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@testingperformed", "Diagnostic"));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@notes", " "));

            //Set the Declaration properties.
            //vm.ITADDeclaration = "This report serves as confirmation that the asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + @" has be sanitised of all user data in accordance to GDPR using Company/Manufactures procedures and guidelines for sanitisation.";
            //vm.ITADDeclaration2 = "Asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + " was sanitised using the following process (" + vm.ITADProcessPerformedSelItem.Process.Replace("Process Performed: ", "") + @") in line with Company & Manufactures procedures.";
            ////Technician Confirmation
            //vm.ITADTechConfirm = vm.FullUser.Replace("Technician Name: ", "") + " hereby confirms that Asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + @" has been sanitised of all user data and is fit for reuse.";
            ////Technician Confirmation
            //vm.ITADVerificationConfirm = "Sanitisation of this asset was verified by " + vm.ITADVerifiedBy + ", using the verification process " + vm.ITADVerificationPerformedSelItem.Process + ".";

            sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportbody", " "));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@savepath", " "));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@testedby", vm.FullUser));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@verifiedby", "D Benyon"));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@verificationperformed", "GUI Commands confirm factory reset"));
            sqlite_cmd.ExecuteNonQuery();


                //}

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }




        public void CreatePDFDocs()
        {

        }


        public async void ReadGen10Reports(List<string> files, string savePath)
        {
            //Holds all properties info
            ObservableCollection<Gen10SummaryReport.diagOutputCategoryStructureProperty> obvSummary = new ObservableCollection<Gen10SummaryReport.diagOutputCategoryStructureProperty>();
            ObservableCollection<Gen10DiagnosticReport.diagOutput> obvDiagNostic = new ObservableCollection<Gen10DiagnosticReport.diagOutput>();

            //report data
            Gen10SummaryReport.diagOutput summaryReport = null;
            Gen10DiagnosticReport.diagOutput diagReport = null;

            JsonParser xmlTest = new JsonParser(vm);


            //loop through any files in the list
            foreach (var file in files)
            {
               // MessageBox.Show(file);

                //check if summary or diagnostic
                if (file.Contains("-hvt-survey.xml"))
                {
                    summaryReport = xmlTest.ReadGen10SummaryXMLReports(file);
                }
                else if (file.Contains("-testlog.xml"))
                {
                    diagReport = xmlTest.ReadGen10DiagnosticXMLReports(file);
                }
            }


            if(summaryReport == null || diagReport == null)
            {
                vm.ChecklistWarningMessage = "Error in Intelligent Provisioning Files. Returned Data Null! Please check file sizes of hvt-survey.xml & testlog.xml.";
                return;
            }
            else
            {

                ////check if summary or diagnostic
                //if (file.Contains("-hvt-survey.xml"))
                //{
                //    summaryReport = xmlTest.ReadGen10SummaryXMLReports(@"" + vm.dsktop + @"\Gen10 Test Reports\2020-12-11-10_26_17-CZ3032YX98-hvt-survey.xml");
                //}
                //else if (file.Contains("-testlog.xml"))
                //{
                //    diagReport = xmlTest.ReadGen10DiagnosticXMLReports(@"" + vm.dsktop + @"\Gen10 Test Reports\2020-12-11-10_26_17-CZ3032YX98-testlog.xml");
                //}


                //diagOutputCategoryDeviceStructureStructureProperty
                if (summaryReport != null)
                    foreach (var itm in summaryReport.category)
                    {


                        if (itm.caption == "Server Information")
                        {
                            var strSI = itm.structure;

                            foreach (var propitm in strSI.property)
                            {
                                if (propitm.GetType().ToString().Contains("diagOutputCategoryStructureProperty"))
                                {
                                    obvSummary.Add(propitm);
                                }
                                //MessageBox.Show(propitm.caption + " : " + propitm.name + " : " + propitm.value);
                            }

                        }

                        if (itm.caption == "Health")
                        {
                            var strSI = itm.structure;

                            foreach (var propitm in strSI.property)
                            {
                                if (propitm.GetType().ToString().Contains("diagOutputCategoryStructureProperty"))
                                {
                                    obvSummary.Add(propitm);
                                }
                                //MessageBox.Show(propitm.caption + " : " + propitm.name + " : " + propitm.value);
                            }

                        }
                        if (itm.caption == "Processor")
                        {
                            var strSI = itm.device;

                            foreach (var propitm in strSI)
                            {
                                if (propitm.Items != null)
                                    foreach (var inneritm in propitm.Items)
                                    {

                                        //MessageBox.Show(inneritm.caption + " : " + propitm.name + " : " + propitm.value);
                                        //Gen10SummaryReport.diagOutputCategoryStructureProperty innerProp = new Gen10SummaryReport.diagOutputCategoryStructureProperty();

                                        if (inneritm.GetType().ToString().Contains("diagOutputCategoryDeviceProperty"))
                                        {

                                            Gen10SummaryReport.diagOutputCategoryStructureProperty innerProp = new Gen10SummaryReport.diagOutputCategoryStructureProperty();
                                            Gen10SummaryReport.diagOutputCategoryDeviceProperty castitm = (Gen10SummaryReport.diagOutputCategoryDeviceProperty)inneritm;

                                            //wont cast so create a new object and set props like for like
                                            innerProp.caption = castitm.caption;
                                            innerProp.name = castitm.name;
                                            innerProp.value = castitm.value;

                                            //add to main collection
                                            obvSummary.Add(innerProp);

                                        }


                                    }


                            }

                        }
                        if (itm.caption == "Memory")
                        {
                            var strSI = itm.device;

                            foreach (var propitm in strSI)
                            {


                                foreach (var inneritm in propitm.Items)
                                {

                                    if (inneritm.GetType().ToString().Contains("diagOutputCategoryDeviceProperty"))
                                    {
                                        Gen10SummaryReport.diagOutputCategoryStructureProperty innerProp = new Gen10SummaryReport.diagOutputCategoryStructureProperty();
                                        Gen10SummaryReport.diagOutputCategoryDeviceProperty castitm = (Gen10SummaryReport.diagOutputCategoryDeviceProperty)inneritm;
                                        //wont cast so create a new object and set props like for like
                                        innerProp.caption = castitm.caption;
                                        innerProp.name = castitm.name;
                                        innerProp.value = castitm.value;

                                        //add to main collection
                                        obvSummary.Add(innerProp);
                                    }


                                }


                            }



                        }
                        if (itm.caption == "Firmware")
                        {
                            var strSI = itm.structure;

                            foreach (var propitm in strSI.property)
                            {
                                if (propitm.GetType().ToString().Contains("diagOutputCategoryStructureProperty"))
                                {
                                    obvSummary.Add(propitm);
                                }
                                //MessageBox.Show(propitm.caption + " : " + propitm.name + " : " + propitm.value);
                            }

                        }


                    }


                //ReportLV1.ItemsSource = obvSummary;
                //ReportLV2.ItemsSource = diagReport.testLogRecord;


                //LOOP THROUGH AND CREATE A FLOWDOCUMENT
                //foreach(var itm in obvSummary)
                //{
                //    itm.iconPacks:PackIconSimpleIcons
                //}

                FlowDocument Gen10FD = new FlowDocument();
                Gen10FD.PageWidth = 1100;
                Gen10FD.PageHeight = 794;

                var bc = new BrushConverter();

                BlockUIContainer bui = new BlockUIContainer();
                StackPanel sp1 = new StackPanel();
                sp1.HorizontalAlignment = HorizontalAlignment.Center;
                MahApps.Metro.IconPacks.PackIconSimpleIcons iconPak = new MahApps.Metro.IconPacks.PackIconSimpleIcons();
                iconPak.Kind = MahApps.Metro.IconPacks.PackIconSimpleIconsKind.Hp;
                iconPak.Width = 40;
                iconPak.Height = 40;
                iconPak.Margin = new Thickness(0, 0, 0, 10);
                iconPak.HorizontalAlignment = HorizontalAlignment.Center;
                iconPak.Foreground = (Brush)bc.ConvertFrom("#CC119EDA");
                sp1.Children.Add(iconPak);

                TextBlock tbl = new TextBlock();
                tbl.Text = "Gen 10 " + vm.ChassisSerialNo.Trim() + " Survey Log " + DateTime.Now.ToShortDateString();
                tbl.Background = (Brush)bc.ConvertFrom("#CC119EDA");
                tbl.Foreground = Brushes.White;
                tbl.FontSize = 20;
                tbl.FontFamily = new FontFamily("Segoe UI");
                tbl.HorizontalAlignment = HorizontalAlignment.Center;
                tbl.TextAlignment = TextAlignment.Center;

                sp1.Children.Add(tbl);
                bui.Child = sp1;

                Gen10FD.Blocks.Add(bui);


                // SUMMARY
                Table summaryRecordTable = new Table();
                summaryRecordTable.CellSpacing = 2;
                summaryRecordTable.FontFamily = new FontFamily("Segoe UI");

                //4 Columns
                for (int i = 0; i < 3; i++)
                {
                    summaryRecordTable.Columns.Add(new TableColumn());

                    //ALTER THE COLUMN WIDTHS
                    //if (i == 0 || i == 2)
                    //{
                    //    summaryRecordTable.Columns[i].Width = new GridLength(23, GridUnitType.Star);
                    //}
                    //else if (i == 3)
                    //{
                    //    summaryRecordTable.Columns[i].Width = new GridLength(6, GridUnitType.Star);
                    //}
                    //else if (i == 6)
                    //{
                    //    summaryRecordTable.Columns[i].Width = new GridLength(10, GridUnitType.Star);
                    //}
                    //else
                    //{
                    //    summaryRecordTable.Columns[i].Width = new GridLength(8, GridUnitType.Star);
                    //}


                }

                TableRow Summaryrow = new TableRow();

                Summaryrow.Background = (Brush)bc.ConvertFrom("#CC119EDA");
                Summaryrow.Foreground = Brushes.White;
                Summaryrow.FontSize = 11;
                Summaryrow.FontWeight = FontWeights.Bold;


                Summaryrow.Cells.Add(new TableCell(new Paragraph(new Run("Caption"))));
                Summaryrow.Cells[0].ColumnSpan = 1;
                Summaryrow.Cells[0].TextAlignment = TextAlignment.Center;
                Summaryrow.Cells.Add(new TableCell(new Paragraph(new Run("Name"))));
                Summaryrow.Cells[1].ColumnSpan = 1;
                Summaryrow.Cells[1].TextAlignment = TextAlignment.Center;
                Summaryrow.Cells.Add(new TableCell(new Paragraph(new Run("Value"))));
                Summaryrow.Cells[2].ColumnSpan = 1;
                Summaryrow.Cells[2].TextAlignment = TextAlignment.Center;
                Summaryrow.Cells.Add(new TableCell(new Paragraph(new Run("Raw"))));
                Summaryrow.Cells[3].ColumnSpan = 1;
                Summaryrow.Cells[3].TextAlignment = TextAlignment.Center;


                var Summaryrg = new TableRowGroup();
                Summaryrg.Rows.Add(Summaryrow);

                //Loop through and create rows
                foreach (var itm in obvSummary)
                {
                    TableRow rowCollection = new TableRow();
                    rowCollection.FontSize = 9;
                    rowCollection.Background = Brushes.AliceBlue;
                    rowCollection.Foreground = Brushes.Black;
                    //Paragraph para = new Paragraph(new Run(itm..TrimEnd()));
                    // para.Foreground = Brushes.White;
                    // para.Background = Brushes.MediumPurple;
                    // para.TextAlignment = TextAlignment.Center;
                    // para.FontSize = 16;
                    // para.Blocks.Add(PowerInventory);

                    rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.caption))));
                    rowCollection.Cells[0].ColumnSpan = 1;
                    rowCollection.Cells[0].TextAlignment = TextAlignment.Center;
                    rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.name))));
                    rowCollection.Cells[1].ColumnSpan = 1;
                    rowCollection.Cells[1].TextAlignment = TextAlignment.Center;
                    rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.value))));
                    rowCollection.Cells[2].ColumnSpan = 1;
                    rowCollection.Cells[2].TextAlignment = TextAlignment.Center;
                    rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.raw))));
                    rowCollection.Cells[3].ColumnSpan = 1;
                    rowCollection.Cells[3].TextAlignment = TextAlignment.Center;

                    //if (itm.summaryCompletion.result == "passed")
                    //{
                    //    rowCollection.Cells[7].Background = Brushes.Green;
                    //    rowCollection.Cells[7].Foreground = Brushes.White;
                    //}
                    //else
                    //{
                    //    rowCollection.Cells[7].Background = Brushes.Goldenrod;
                    //    rowCollection.Cells[7].Foreground = Brushes.Black;
                    //}
                    //rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.summaryCompletion.summaryTime))));
                    //rowCollection.Cells[8].ColumnSpan = 1;
                    //rowCollection.Cells[8].TextAlignment = TextAlignment.Center;



                    Summaryrg.Rows.Add(rowCollection);
                }
                //Add To Table
                summaryRecordTable.RowGroups.Add(Summaryrg);

                //Add Table to Flow Document
                Gen10FD.Blocks.Add(summaryRecordTable);



                //Add Title for Test Log
                BlockUIContainer bui2 = new BlockUIContainer();
                StackPanel sp2 = new StackPanel();
                sp2.HorizontalAlignment = HorizontalAlignment.Center;

                TextBlock tb2 = new TextBlock();
                tb2.Text = "Gen 10 " + vm.ChassisSerialNo.Trim() + " Test Log " + DateTime.Now.ToShortDateString();
                tb2.Background = (Brush)bc.ConvertFrom("#CC119EDA");
                tb2.Foreground = Brushes.White;
                tb2.FontSize = 20;
                tb2.FontFamily = new FontFamily("Segoe UI");
                tb2.HorizontalAlignment = HorizontalAlignment.Center;
                tb2.TextAlignment = TextAlignment.Center;

                sp2.Children.Add(tb2);
                bui2.Child = sp2;

                Gen10FD.Blocks.Add(bui2);



                // TEST LOG
                Table testRecordTable = new Table();
                testRecordTable.CellSpacing = 2;
                testRecordTable.FontFamily = new FontFamily("Segoe UI");

                for (int i = 0; i < 8; i++)
                {
                    testRecordTable.Columns.Add(new TableColumn());

                    //ALTER THE COLUMN WIDTHS
                    if (i == 0 || i == 2)
                    {
                        testRecordTable.Columns[i].Width = new GridLength(23, GridUnitType.Star);
                    }
                    else if (i == 3)
                    {
                        testRecordTable.Columns[i].Width = new GridLength(6, GridUnitType.Star);
                    }
                    else if (i == 6)
                    {
                        testRecordTable.Columns[i].Width = new GridLength(10, GridUnitType.Star);
                    }
                    else
                    {
                        testRecordTable.Columns[i].Width = new GridLength(8, GridUnitType.Star);
                    }


                }

                TableRow row = new TableRow();

                row.Background = (Brush)bc.ConvertFrom("#CC119EDA");
                row.Foreground = Brushes.White;
                row.FontSize = 11;
                row.FontWeight = FontWeights.Bold;


                row.Cells.Add(new TableCell(new Paragraph(new Run("Component"))));
                row.Cells[0].ColumnSpan = 1;
                row.Cells[0].TextAlignment = TextAlignment.Center;
                row.Cells.Add(new TableCell(new Paragraph(new Run("Device"))));
                row.Cells[1].ColumnSpan = 1;
                row.Cells[1].TextAlignment = TextAlignment.Center;
                row.Cells.Add(new TableCell(new Paragraph(new Run("Test"))));
                row.Cells[2].ColumnSpan = 1;
                row.Cells[2].TextAlignment = TextAlignment.Center;
                row.Cells.Add(new TableCell(new Paragraph(new Run("Test Time"))));
                row.Cells[3].ColumnSpan = 1;
                row.Cells[3].TextAlignment = TextAlignment.Center;
                row.Cells.Add(new TableCell(new Paragraph(new Run("Passed Count"))));
                row.Cells[4].ColumnSpan = 1;
                row.Cells[4].TextAlignment = TextAlignment.Center;
                row.Cells.Add(new TableCell(new Paragraph(new Run("Failed Count"))));
                row.Cells[5].ColumnSpan = 1;
                row.Cells[5].TextAlignment = TextAlignment.Center;
                row.Cells.Add(new TableCell(new Paragraph(new Run("Completion Time"))));
                row.Cells[6].ColumnSpan = 1;
                row.Cells[6].TextAlignment = TextAlignment.Center;
                row.Cells.Add(new TableCell(new Paragraph(new Run("Result"))));
                row.Cells[7].ColumnSpan = 1;
                row.Cells[7].TextAlignment = TextAlignment.Center;
                //row.Cells.Add(new TableCell(new Paragraph(new Run("Time"))));
                //row.Cells[8].ColumnSpan = 1;
                //row.Cells[8].TextAlignment = TextAlignment.Center;

                var rg = new TableRowGroup();
                rg.Rows.Add(row);

                //Loop through and create rows
                foreach (var itm in diagReport.testLogRecord)
                {
                    TableRow rowCollection = new TableRow();
                    rowCollection.FontSize = 9;
                    rowCollection.Background = Brushes.AliceBlue;
                    rowCollection.Foreground = Brushes.Black;
                    //Paragraph para = new Paragraph(new Run(itm..TrimEnd()));
                    // para.Foreground = Brushes.White;
                    // para.Background = Brushes.MediumPurple;
                    // para.TextAlignment = TextAlignment.Center;
                    // para.FontSize = 16;
                    // para.Blocks.Add(PowerInventory);

                    rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.component))));
                    rowCollection.Cells[0].ColumnSpan = 1;
                    rowCollection.Cells[0].TextAlignment = TextAlignment.Center;
                    rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.device))));
                    rowCollection.Cells[1].ColumnSpan = 1;
                    rowCollection.Cells[1].TextAlignment = TextAlignment.Center;
                    rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.test))));
                    rowCollection.Cells[2].ColumnSpan = 1;
                    rowCollection.Cells[2].TextAlignment = TextAlignment.Center;
                    rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.testTime))));
                    rowCollection.Cells[3].ColumnSpan = 1;
                    rowCollection.Cells[3].TextAlignment = TextAlignment.Center;
                    rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.passedCount.ToString()))));
                    rowCollection.Cells[4].ColumnSpan = 1;
                    rowCollection.Cells[4].TextAlignment = TextAlignment.Center;
                    rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.failedCount.ToString()))));
                    rowCollection.Cells[5].ColumnSpan = 1;
                    rowCollection.Cells[5].TextAlignment = TextAlignment.Center;
                    rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.testCompletion.completionTime))));
                    rowCollection.Cells[6].ColumnSpan = 1;
                    rowCollection.Cells[6].TextAlignment = TextAlignment.Center;
                    rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.testCompletion.result))));
                    rowCollection.Cells[7].ColumnSpan = 1;
                    rowCollection.Cells[7].TextAlignment = TextAlignment.Center;
                    if (itm.testCompletion.result == "passed")
                    {
                        rowCollection.Cells[7].Background = Brushes.Green;
                        rowCollection.Cells[7].Foreground = Brushes.White;
                    }
                    else
                    {
                        rowCollection.Cells[7].Background = Brushes.Goldenrod;
                        rowCollection.Cells[7].Foreground = Brushes.Black;
                    }
                    //rowCollection.Cells.Add(new TableCell(new Paragraph(new Run(itm.testCompletion.testTime))));
                    //rowCollection.Cells[8].ColumnSpan = 1;
                    //rowCollection.Cells[8].TextAlignment = TextAlignment.Center;



                    rg.Rows.Add(rowCollection);
                }
                //Add To Table
                testRecordTable.RowGroups.Add(rg);

                //Add Table to Flow Document
                Gen10FD.Blocks.Add(testRecordTable);

                //Print Flow Document
                DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

                //Set Clipboard text
                if (vm != null)
                {
                    if (!string.IsNullOrEmpty(vm.ChassisSerialNo))
                        Clipboard.SetText(vm.ChassisSerialNo.Trim() + "_TestLog", TextDataFormat.UnicodeText);
                }


                // delay required for flow document to print




                await PutTaskDelay(2000);
                //Create the PDF from the Flow Document and save to a fixed path.
                if (vm.SaveToMyDocs == true)
                {
                    //MessageBox.Show(vm.myDocs + @"\" + vm.ChassisSerialNo.Trim() + "-TestLog.pdf");

                    if (StaticFunctions.Department == "ITAD")
                    {

                        dpw.FlowDocument2Pdf(Gen10FD, savePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_TestLog.pdf");

                        //Launch file for printing
                        if (File.Exists(savePath + @"\" + vm.ChassisSerialNo.Trim() + "_TestLog.pdf"))
                        {
                            System.Diagnostics.Process.Start(savePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_TestLog.pdf");
                            //wait until data populated
                            await PutTaskDelay(500);
                        }

                    }
                    else
                    {
                        dpw.FlowDocument2Pdf(Gen10FD, savePath + @"\" + vm.ChassisSerialNo.Trim() + "_TestLog.pdf");

                        //Launch file for printing
                        if (File.Exists(savePath + @"\" + vm.ChassisSerialNo.Trim() + "_TestLog.pdf"))
                        {
                            System.Diagnostics.Process.Start(savePath + @"\" + vm.ChassisSerialNo.Trim() + "_TestLog.pdf");
                            //wait until data populated
                            await PutTaskDelay(500);
                        }
                    }



                }
                else
                {


                    if (StaticFunctions.Department == "ITAD")
                    {
                        dpw.FlowDocument2Pdf(Gen10FD, savePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_TestLog.pdf");

                        //Launch file for printing
                        if (File.Exists(savePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_TestLog.pdf"))
                        {
                            System.Diagnostics.Process.Start(savePath + @"\" + vm.POSONumber + " - " + vm.ChassisSerialNo.Trim() + "_TestLog.pdf");
                            //wait until data populated
                            await PutTaskDelay(500);
                        }

                    }
                    else
                    {
                        dpw.FlowDocument2Pdf(Gen10FD, savePath + @"\" + vm.ChassisSerialNo.Trim() + "_TestLog.pdf");

                        //Launch file for printing
                        if (File.Exists(savePath + @"\" + vm.ChassisSerialNo.Trim() + "_TestLog.pdf"))
                        {
                            System.Diagnostics.Process.Start(savePath + @"\" + vm.ChassisSerialNo.Trim() + "_TestLog.pdf");
                            //wait until data populated
                            await PutTaskDelay(500);
                        }
                    }



                }

                ////Change Message
                //vm.ChecklistMessage = "Please Print The Gen 10 Report";






                ////Get Printable Copy
                //dpw.PrintFlowDoc(Gen10FD);

                ////Change Message
                //vm.ChecklistMessage = "";

                //ReportLV1.Height = ReportLV1.ActualHeight;
                //ReportLV2.Height = ReportLV2.ActualHeight;

                // Send both documents to the print functions
                //PrintHelper.ShowPrintPreview(PrintHelper.GetFixedDocument(ReportLV1, ReportLV2, new PrintDialog(), this));


                //this.Close();
                //return Gen10FD;
            }




        }







        private void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.       
            Directory.CreateDirectory(destDirName);

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();

            //Create List
            vm.Gen10reportFiles = new List<string>();


            foreach (FileInfo file in files)
            {
                //add the list of string
                if (file.Name.Contains("-hvt-survey.xml") || file.Name.Contains("-testlog.xml"))
                {

                    //add the found reports to the list
                    vm.Gen10reportFiles.Add(@"" + sourceDirName + @"\" + file.Name);
                    // MessageBox.Show(@"" + sourceDirName + @"" + file.Name);
                }


                string tempPath = Path.Combine(destDirName, file.Name);
                file.CopyTo(tempPath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string tempPath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, tempPath, copySubDirs);
                }
            }
        }




        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }


    }
}

  