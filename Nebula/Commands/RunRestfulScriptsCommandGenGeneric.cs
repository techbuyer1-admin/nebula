﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class RunRestfulScriptsCommandGenGeneric : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public RunRestfulScriptsCommandGenGeneric(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            string ServerIP = "";
            var values = (object[])parameter;
            if(values != null)
            {
                ServerIP = (string)values[0];
            }
            //var ServerIP = (string)values[0];
            //var ScriptChoice = (string)values[1];
            //var ServerGen = (string)values[2];



            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            if (ServerIP != null)
            {
                //check if valid ip entered
                //System.Net.IPAddress ipAddress = null;
                //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                if (ServerIP != string.Empty)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public async void Execute(object parameter)
        {

            var values = (object[])parameter;
            var ServerIP = (string)values[0];
            var ScriptChoice = (string)values[1];
            var ServerGen = (string)values[2];
            var IsServerThere = (string)values[3];
            var ProgressMessage = (string)values[4];
            var WarningMessage = (string)values[5];
            var ProgressPercentage = values[6];
            var ProgressVisibility = values[7];
            var MessageVisibility = values[8];
            var ProgressIsActive = values[9];
            var ILORestOutput = (string)values[10];
            var myDocs = (string)values[11];


            if (ServerIP != null)
            {
                if (ServerIP != string.Empty)
                {
                    MessageBox.Show("Passed ServerIP Check");
                     WarningMessage = "";
                    ProgressMessage = "";
                    //send a ping to the server to check it's available
                    ProgressMessage = "Checking Server IP, Please Wait...";
                    ProgressPercentage = 0;
                    ProgressIsActive = true;
                    ProgressVisibility = Visibility.Visible;
                    sendAsyncPingPacket(ServerIP);

                    await PutTaskDelay(3000);

                    //if it is run the code
                    if (IsServerThere == "Yes")
                    {


                        //Create and copy utility programs for local user use
                        //*Separate into own method
                        if (Directory.Exists(@"" + myDocs + @"\HPTOOLS"))
                        {

                            //MessageBox.Show(@"" + myDocs + @"\HPCONFIGUTIL");  @"\\pinnacle.local\tech_resources\Nebula\
                        }
                        else
                        {
                            //create folder
                            Directory.CreateDirectory(@"" + myDocs + @"\HPTOOLS");
                            Directory.CreateDirectory(@"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL");
                            Directory.CreateDirectory(@"" + myDocs + @"\HPTOOLS\HP iLO Integrated Remote Console");
                            Directory.CreateDirectory(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool");
                            //get all files from the directory on the network share
                            string[] HPConfigfiles = Directory.GetFiles(@"" + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL");
                            string[] RemoteConsolefiles = Directory.GetFiles(@"" + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HP iLO Integrated Remote Console");
                            string[] RestfulAPIfiles = Directory.GetFiles(@"" + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\RESTful Interface Tool");
                            //loop through the array and copy files to the local mydocs location
                            foreach (var item in HPConfigfiles)
                            {

                                //MessageBox.Show(@"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + item);
                                File.Copy(item, @"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + item.Replace(@"" + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL\", ""));
                            }

                            foreach (var item in RemoteConsolefiles)
                            {

                                //MessageBox.Show(@"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + item);
                                File.Copy(item, @"" + myDocs + @"\HPTOOLS\HP iLO Integrated Remote Console\" + item.Replace(@"" + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HP iLO Integrated Remote Console\", ""));
                            }

                            foreach (var item in RestfulAPIfiles)
                            {

                                //MessageBox.Show(@"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + item);
                                File.Copy(item, @"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\" + item.Replace(@"" + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\RESTful Interface Tool\", ""));
                            }


                        }

                        //CLEAR THE DATA FILES THESE WILL BE REPRODUCED LATER IN THIS SCRIPT
                        if (File.Exists(@"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "ServerHealth1.xml"))
                        {
                            File.Delete(@"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "ServerHealth1.xml");
                        }
                        if (File.Exists(@"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "ServerHealthTrimmed1.xml"))
                        {
                            File.Delete(@"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "ServerHealthTrimmed1.xml");
                        }
                        //CLEAR RESTFUL INTERFACE JSON FILE
                        if (File.Exists(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "All.json"))
                        {
                            File.Delete(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "All.json");
                        }
                        ////CLEAR SD CARD INFO
                        //if (File.Exists(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "SDCARD.json"))
                        //{
                        //    File.Delete(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "SDCARD.json");
                        //}
                        //CLEAR ILO FIRMWARE INFO
                        if (File.Exists(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "Firmware.json"))
                        {
                            File.Delete(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "Firmware.json");
                        }

                        //*Separate into own method


                        ProcessPiper pp = new ProcessPiper(vm);

                        //RESET THE COMMAND BOX
                        ILORestOutput = "";


                            switch (ScriptChoice)
                            {
                            case "Test1":
                                MessageBox.Show(ServerIP);
                                break;

                            case "ServerInfo":
                               
                                    ProgressPercentage = 0;
                                    ProgressIsActive = true;
                                    ProgressVisibility = Visibility.Visible;
                                    ProgressMessage = "Receiving Information From Server, Please Wait...";
                                    //EXTRACT ALL SYSTEM INFO TO JSON
                               //     await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + ServerIP + "All.json  --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                await System.Threading.Tasks.Task.Run(() => pp.StartILORestGeneric(new System.IO.FileInfo(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,ComputerSystem.,Manager. -f" + ServerIP + "All.json  --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                ProgressPercentage = 50;
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/managers/1/embeddedmedia -f" + ServerIP + "SDCARD.json  --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    // await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --select fwswversioninventory. -f" + vm.ServerIPv4 + "all.json  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORestGeneric(new System.IO.FileInfo(@"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l " + ServerIP + "ServerHealth1.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                    ProgressPercentage = 60;
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + myDocs + @"\HPTOOLS\HPCONFIGUTIL\Get_Global.xml -l GlobalDetails.xml -s " + vm.ServerIPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL"));

                               //UID SET ON
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORestGeneric(new System.IO.FileInfo(@"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + myDocs + @"\HPTOOLS\HPCONFIGUTIL\UID_Control_ON.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                //Read the Data from the JSON and XML Files pulled from the Server
                                ReadJsonXMLObjects(ServerGen, ServerIP, myDocs);

                                    ProgressVisibility = Visibility.Hidden;
                                    ProgressIsActive = false;
                                    ProgressPercentage = 100;
                                    ProgressMessage = "Received Data Successfully";





                                await PutTaskDelay(10000);



                                //UID SET OFF
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + myDocs + @"\HPTOOLS\HPCONFIGUTIL\UID_Control_OFF.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL"));

                                break;






                            
                            }
                    
                    
                    
                    }//end if to check server ip is not empty
                    else
                    {
                        //reset progress ring to hidden
                        ProgressVisibility = Visibility.Hidden;
                        MessageVisibility = Visibility.Visible;
                        ProgressIsActive = false;
                        ProgressPercentage = 0;
                        WarningMessage = "Cannot contact server with Ip: " + ServerIP + ", Please check the Server has power.";
                        IsServerThere = "No";
                    }

                }// check if server there
                else
                {
                    //vm.ProgressMessage = "Cannot contact server with Ip: " + vm.ServerIPv4 + ", Exited Function";
                    ProgressMessage = "Please Enter A Server IP in the top box!";
                }

            }//end if to check vm is null

        }//end execute




        public void ReadJsonXMLObjects(string ServerGen,string ServerIP, string myDocs)
        {

            try
            {
          
            // GET THE SERVER INFORMATION AGAIN
            string ServerInfoJson;



            //CHECK BOTH SERVER INFORMATION FILES EXIST
            if (File.Exists(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "All.json") && File.Exists(@"" + myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + ServerIP + "ServerHealth1.xml"))
            {
                //Full Info
                using (StreamReader r = new StreamReader(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "All.json"))
                {
                    //STRIP OUT THE INCORRECT HEADERS THAT STOP JSON.NET READING CORRECTLY
                    ServerInfoJson = r.ReadToEnd().Replace("#Bios.v1_0_0.Bios", "Bios")
                                                  .Replace("HpBios.1.1.0", "Bios")
                                                  .Replace("HpBios.1.2.0", "Bios")
                                                  .Replace("/redfish/v1/systems/1/bios/settings/", "Path")
                                                  .Replace("/rest/v1/systems/1/Bios", "Path")
                                                  .Replace("#Chassis.v1_6_0.Chassis", "Chassis")
                                                  .Replace("/redfish/v1/Chassis/1/", "Path")
                                                  .Replace("/rest/v1/Systems/1/FirmwareInventory", "Path")
                                                  .Replace("#ComputerSystem.v1_4_0.ComputerSystem", "ComputerSystem")
                                                  .Replace("ComputerSystem.1.0.1", "ComputerSystem")
                                                  .Replace("ComputerSystem.1.0.2", "ComputerSystem")
                                                  .Replace("/redfish/v1/Systems/1/", "Path")
                                                  .Replace("HpiLOLicense.1.0.0", "Manager")
                                                  .Replace("HpiLOLicense.1.0.0", "Manager")
                                                  .Replace("HpiLOLicense.1.0.0", "Manager")
                                                  .Replace("/rest/v1/Managers/1", "Path")
                                                  .Replace("#Manager.v1_5_1.Manager", "Manager")
                                                  .Replace("/redfish/v1/Managers/1/", "Path")
                                                  .Replace("HpBios.1.0.0", "Bios")
                                                  .Replace("HpBios.1.1.0", "Bios")
                                                  .Replace("HpBios.1.2.0", "Bios")
                                                  .Replace("/rest/v1/Systems/1/Bios", "Path")
                                                  .Replace("/rest/v1/systems/1/Bios", "Path")
                                                  .Replace("Chassis.1.0.0", "Chassis")
                                                  .Replace("/rest/v1/Chassis/1", "Path")
                                                  .Replace("FwSwVersionInventory.1.0.0", "Firmware")
                                                  .Replace("FwSwVersionInventory.1.1.0", "Firmware")
                                                  .Replace("FwSwVersionInventory.1.2.0", "Firmware")
                                                  .Replace("/rest/v1/Systems/1/FirmwareInventory", "Path")
                                                  .Replace("ComputerSystem.1.0.0", "ComputerSystem")
                                                  .Replace("ComputerSystem.1.0.1", "ComputerSystem")
                                                  .Replace("ComputerSystem.1.0.2", "ComputerSystem")
                                                  .Replace("/rest/v1/Systems/1", "Path")
                                                  .Replace("HpiLOLicense.1.0.0", "Manager")
                                                  .Replace("HpiLOLicense.1.0.0", "Manager")
                                                  .Replace("HpiLOLicense.1.0.0", "Manager")
                                                  .Replace("/rest/v1/Managers/1", "Path")
                                                  .Replace("Manager.1.0.0", "Manager")
                                                  .Replace("/rest/v1/Managers/1", "Path");

                }

                //create new json parser
                JsonParser jPar = new JsonParser(vm);

                //vm.GenGenericServerInfo1 = jPar.ReadFullServerInfoJsonGenGeneric(ServerInfoJson, ServerIP, "S1");

                //Write to CSV
                //StaticFunctions.CreateAppendCSV(new ReportServersProcessed(vm.GoodsINRep, vm.ChassisSerialNo, "Goods In"), @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed.csv");

                GetPropertyValues(vm.GenGenericServerInfo1);

                StringBuilder sb = new StringBuilder();


                //foreach(var prop in Gen9Info)
                //{

                //}

                //PropertyInfo[] myPropertyInfo;
                //// Get the properties of 'Type' class object.
                //myPropertyInfo = Type.GetType("System.Type").GetProperties();
                //Console.WriteLine("Properties of System.Type are:");
                //for (int i = 0; i < myPropertyInfo.Length; i++)
                //{
                //    Console.WriteLine(myPropertyInfo[i].ToString());
                //}

                //Type t = typeof(Gen9ServerInfo1);
                //// Get the public properties.
                //PropertyInfo[] propInfos = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                //Console.WriteLine("The number of public properties: {0}.\n",
                //                  propInfos.Length);
                //// Display the public properties.
                //DisplayPropertyInfo(propInfos);

                //// Get the nonpublic properties.
                //PropertyInfo[] propInfos1 = t.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance);
                //Console.WriteLine("The number of non-public properties: {0}.\n",
                //                  propInfos1.Length);
                //// Display all the nonpublic properties.
                //DisplayPropertyInfo(propInfos1);



                //public static void CreateAppendCSV(ReportServersProcessed itemstowrite, string filetocreate)
                //{

                //string TheDate = DateTime.Now.ToShortDateString();
                //    string TheTime = DateTime.Now.ToShortTimeString();
                //    string TheYear = DateTime.Now.Year.ToString();

                //    //Will Create a new file for each year
                //    string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv";

                //    string clientDetails = TheDate + "," + itemstowrite.ChassisSerialNo + "," + itemstowrite.ProcessedBy + "," + itemstowrite.Department + "," + TheTime + Environment.NewLine;


                //    if (!File.Exists(newFileName))
                //    {
                //        string clientHeader = "Date" + "," + "Chassis Serial No" + "," + "Operative" + "," + "Department" + "," + "Time Completed" + Environment.NewLine;

                //        File.WriteAllText(newFileName, clientHeader);
                //    }

                //    File.AppendAllText(newFileName, clientDetails);


                //}








                //check which gen and run the correct item
                switch (ServerGen)
                {
                    case "Gen8":
                        //GEN8
                        //read in info from server and pass to the .net object
                        //  vm.Gen9ServerInfo1 = jPar.ReadFullServerInfoJsonGen9(ServerInfoJson);
                        break;
                    case "Gen9":
                        //GEN9
                        //read in info from server and pass to the .net object
                        //vm.Gen9ServerInfo = jPar.ReadFullServerInfoJsonGen9(ServerInfoJson);
                        break;
                    case "Gen10":
                        //GEN10
                        //read in info from server and pass to the .net object
                        // vm.Gen10ServerInfo = jPar.ReadFullServerInfoJsonGen10(ServerInfoJson);
                        break;
                }



              }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }



        private static void GetPropertyValues(Object obj)
        {
            Type t = obj.GetType();
            Console.WriteLine("Type is: {0}", t.Name);
            PropertyInfo[] props = t.GetProperties();
            Console.WriteLine("Properties (N = {0}):",
                              props.Length);
            foreach (var prop in props)
                if (prop.GetIndexParameters().Length == 0)
                    Console.WriteLine("   {0} ({1}): {2}", prop.Name,
                                      prop.PropertyType.Name,
                                      prop.GetValue(obj));
                else
                    Console.WriteLine("   {0} ({1}): <Indexed>", prop.Name,
                                      prop.PropertyType.Name);
        }






        public async void sendAsyncPingPacket(string hostToPing)
        {
            try
            {
                // int timeout = 5000;
                // AutoResetEvent waiter = new AutoResetEvent(false);
                Ping pingPacket = new Ping();
                //ping completion event reaised
                pingPacket.PingCompleted += new PingCompletedEventHandler(PingCompletedCallback);
                string data = "Ping test check";
                byte[] byteBuffer = Encoding.ASCII.GetBytes(data);
                PingOptions pingOptions = new PingOptions(64, true);

                await pingPacket.SendPingAsync(hostToPing, 3000);
                //pingPacket.Send(hostToPing, 3000);

            }
            catch (PingException pe)
            {
                string exception = pe.Message;
                //IPResults.Add("INVALID IP ADDRESS FOUND");
            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                //IPResults.Add("Exceptin " + ex.Message);
            }

        }


        private void PingCompletedCallback(object sender, PingCompletedEventArgs e)
        {
            try
            {
                if (e.Cancelled)
                {

                }

                // If an error occurred, display the exception to the user.
                if (e.Error != null)
                {

                }

                PingReply reply = e.Reply;

                if (reply == null)
                {
                    vm.IsServerThere1 = "No";
                    return;
                }
                else if (reply.Status == IPStatus.Success)
                {
                    vm.IsServerThere1 = "Yes";
                    return;

                }
            }
            catch (PingException pe)
            {
                string exception = pe.Message;
                //IPResults.Add("INVALID IP ADDRESS");
            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                // MessageBox.Show("Came Here");
                //IPResults.Add("Exception " + ex.Message);
            }
        }

        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

    }
}




