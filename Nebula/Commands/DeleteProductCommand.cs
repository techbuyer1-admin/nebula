﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class DeleteProductCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;


        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public DeleteProductCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            var values = (object[])parameter;
            //Split array into specific types
            var Productcmbo = (ComboBox)values[0];
            var tbBrand = (TextBox)values[1];
            
            //Get a textbox for focusing
           // TextBox tbBrand = Helper.GetDescendantFromName(Application.Current.MainWindow, "BrandTXT") as TextBox;
            tbBrand.Focus();


           // MessageBox.Show(vm.SelectedAutomationProfile);


            if(File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML Product Wipe Profiles\" + Productcmbo.Text.ToString() + @".xml"))
            {
                MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this profile?", "Nebula Product Creator", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No);

                switch (result)
                {
                    case MessageBoxResult.Yes:
                        File.Delete(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML Product Wipe Profiles\" + Productcmbo.Text.ToString() + @".xml");

                       // ComboBox Productcmbo = Helper.GetDescendantFromName(Application.Current.MainWindow, "BasedONCB") as ComboBox;
                        Productcmbo.SelectedIndex = -1;



                        //RESET
                        //Clear the controls on the page
                        vm.ClearAddProduct();
                        //give focus back to the Step description box
                        tbBrand.Focus();

                        //reload items
                        vm.ReadProductFileCommand.Execute("");

                        break;
                    case MessageBoxResult.No:
                       
                        break;
                    //case MessageBoxResult.Cancel:
                    //    MessageBox.Show("Nevermind then...", "My App");
                    //    break;
                }

            }



         

            ////reload items
            //vm.ReadProductFileCommand.Execute("");

        }
    }
}
