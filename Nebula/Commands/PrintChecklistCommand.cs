﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.Paginators;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class PrintChecklistCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public PrintChecklistCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter != null)
            {
                var values = (object[])parameter;
                var IP = (string)values[0];
              


                //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
                if (IP != null)
                {
                    //check if valid ip entered
                    //System.Net.IPAddress ipAddress = null;
                    //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                    if (IP != string.Empty)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public void Execute(object parameter)
        {

            var values = (object[])parameter;
            var IP = (string)values[0];
            var FDoc = (FlowDocument)values[1];



            if (vm.ChassisSerialNo != "" && vm.PONumber != "" || vm.SONumber != "")
            {
            
                //OLD METHOD

                DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


                ////Get the text from the clipboard
                //Clipboard.GetText(TextDataFormat.UnicodeText);

                dpw.SaveDocPDF(FDoc);


                vm.ChassisSerialNo = "";
                vm.PONumber = "";
                vm.SONumber = "";

            

            }



        }
    }
}


