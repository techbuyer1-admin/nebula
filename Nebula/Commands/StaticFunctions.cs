﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Windows.Xps.Packaging;
using System.Windows.Xps;
using System.Reflection;
using Nebula.Helpers;
//using XPS;
using System.Printing;
using System.Runtime.InteropServices;
using System.Diagnostics;
//using Excel = Microsoft.Office.Interop.Excel;
//using Word = Microsoft.Office.Interop.Word;
//using Outlook = Microsoft.Office.Interop.Outlook;
using System.ComponentModel;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Nebula.Views;
using Nebula.ViewModels;
using Nebula.Models;
using System.Threading;
using System.Net.NetworkInformation;
using System.Net;
using Microsoft.VisualBasic.FileIO;

using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Data.SQLite;
using System.Runtime.Serialization.Formatters.Binary;

namespace Nebula.Commands
{
    public static class StaticFunctions
    {
        public static string dsktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);   //@"" + dsktop + @"
        public static string myDocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);   //@"" + myDocs + @"
        //public static string myDocs = @"E:\Documents\";   //@"" + myDocs + @"

        //To put application into Test DB Mode. Connection Will point to local copy of the databases
        public static bool TestMode = false;
        //To hold the remoteConsole Process ID 
        public static int remoteConsolePID;

        // read default path from app.config file
        // public static AppSettingsReader ar = new AppSettingsReader();
        //public static string UncPathToUse = (string)ar.GetValue("filepathstr", typeof(String));
        //Set global variable for default file path

        // used to make sure one instance is launched from the LoginVM
        public static bool userMessageCountFlag = false;

        // For Server processing to determine which view to load Check out the Load Content Command
        public static string Department = "";
        //Used to hold the chassis serial and pass between MainView Model and goodsinout viewmodel
        public static string ChassisNoPassthrough = "";
        //mainpath file
        //public static string UncPathToUse = @"C:\Users\hmcankms\Desktop\output\";
        public static string UncPathToUse;
        // Users Location
        public static string UserLocation;
        //public static string UncPathToUse = @"s:\";
        //USING BELOW THROWS A XAML ERROR, BUT IT IS THE PREFERED METHOD
        //public static string UncPathToUse = (string)ar.GetValue("filepathstr", typeof(String));

        public static bool navigateLoaded = false;
        //Static Page Sizes
        public static Size a4PageSize = new Size(8.27 * 96.0, 11.69 * 96.0);
        //public static Size a3PageSize = new Size(8.27 * 96.0, 11.69 * 96.0);
        //public static Size a5PageSize = new Size(8.27 * 96.0, 11.69 * 96.0);

        //Session variable for Serial Cluster Reports
        public static string SessionOrderRefNumber;


        public static string isIndCreated = "";
        public static string runonce = "1";
        //Count Time Interval
        public static int marketingTimeInterval = 5;
        public static int accountsTimeInterval = 5;
        public static int enquiriesTimeInterval = 5;
        //Counts and Refresh Timers
        public static bool marketingTimer = false;
        public static bool accountsTimer = false;
        public static bool enquiriesTimer = false;

        public static bool fromRefresh = false;
        // For use with irish reprints to identify which path to send it to
        public static bool isIrishReprint = false;
        //Server Fault Logging
        public static string FaultPOSONumber = "";
        public static string FaultReason = "";


        // VIRTUAL PRINTER SETTINGS
        //[DllImport("Winspool.drv")]
        //private static extern bool SetDefaultPrinter(string printerName);

        //Create a static variable to access the xps functions to use with form generation and compiling



        //Virtual Printer settings
        //public static string _printerName = "MyXpsPrinter";
        //public static XPS.VirtualPrinter _printer = null;


        //Static Variables for selection set when a list box item has been selected
        // this is used for automatic refreshing of the listbox then selecting the item on that control, these are set on selecteditem events
        public static string ActiveJobSelectedLBox = "";
        public static string ActiveJobControlNameLBox = "";
        public static string ActiveJobSelectedPath = "";



        public static BitmapImage createBitmap(string path,BitmapImage bitmap)
        {
            //IMPORTANT AS QR THROWS EXCEPTION IF NOT A URI PATH
            //Generate a file of the bitmap
            BitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmap));

            using (FileStream fileStream = new FileStream(path, System.IO.FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                encoder.Save(fileStream);
                fileStream.Flush();
                fileStream.Close();
                
            }



            // Create source.
            BitmapImage bi = new BitmapImage();
            // BitmapImage.UriSource must be in a BeginInit/EndInit block.
            bi.BeginInit();
            bi.UriSource = new Uri(path, UriKind.RelativeOrAbsolute);
            bi.EndInit();
           
            // Set the image source.
            return bi;

        }




        public static void SerializeHttpClient(string resourceUrl)
        {
            System.Net.Http.HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://192.168.101.117");
            client.DefaultRequestHeaders.Add(HttpRequestHeader.ContentType.ToString(), "application/json");

            var task = client.GetAsync(resourceUrl);
            task.Wait();
            var response = task.Result;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                MessageBox.Show("OK");
                var reader = response.Content.ReadAsStringAsync();
                string result = reader.Result;
                reader.Wait();
                // parse the escaped string json response to proper JSON Object
                JToken obj = JToken.Parse(result);
                System.Console.WriteLine(obj.ToString());

                // var listingdata = JsonConvert.DeserializeObject<ListingData>((string)obj);
                //  System.Console.WriteLine("{0}-{1}", listingdata.Activities[0].KodeEmiten, listingdata.Activities[0].NamaEmiten);
            }
            else
                System.Console.WriteLine(response.StatusCode);
        }





        // for sorting an observable colletion, can be modified to use a list
        public static void Sort<T>(this ObservableCollection<T> collection) where T : IComparable
        {
            List<T> sorted = collection.OrderBy(x => x).ToList();
            for (int i = 0; i < sorted.Count(); i++)
                collection.Move(collection.IndexOf(sorted[i]), i);
        }


        //Kill process by name
        public static void KillProcess(string processName)
        {
            foreach (var process in Process.GetProcessesByName(processName))
            {
                process.Kill();
            }

        }


        ///// Generic Method to perform Deep-Copy of a WPF element (e.g. UIElement)
        public static T DeepCopy<T>(T element)
        {
            var xaml = XamlWriter.Save(element);
            var xamlString = new StringReader(xaml);
            var xmlTextReader = new XmlTextReader(xamlString);
            var deepCopyObject = (T)XamlReader.Load(xmlTextReader);
            return deepCopyObject;
        }


        //Deep Clone
        public static T DeepClone<T>(T from)
        {
            using (MemoryStream s = new MemoryStream())
            {
                BinaryFormatter f = new BinaryFormatter();
                f.Serialize(s, from);
                s.Position = 0;
                object clone = f.Deserialize(s);

                return (T)clone;
            }
        }


        // Check for a locked file
        public static bool IsLocked(this FileInfo f)
        {
            //implement FileInfo fi = new FileInfo(@"C:\4067918.XPS");
            //           if (!fi.IsLocked())
            //           { //DO SOMETHING HERE; }

            try
            {
                string fpath = f.FullName;
                FileStream fs = File.OpenWrite(fpath);
                fs.Close();
                return false;
            }

            catch (Exception) { return false; }
        }// end is locked


     
        // ALTERNATIVE FILE LOCK CHECKER
        public static bool IsFileLocked(string filePath)
        {
            FileStream stream = null;

            try
            {
                using (File.Open(filePath, FileMode.Open)) { }

            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }


        //SQL LITE SIMPLE READ\WRITE
        public static void ReadServersProcessedSQLLiteDB(SQLiteConnection conn, string sqlquery)
        {
            //Call Example: StaticFunctions.ReadSQLLiteDB(@"URI=file:" + @"\\pinnacle.local\tech_resources\Nebula\XML Weekly Server Records\new\WeeklyRecords.db");
            //string cs = fileToRead;

            //ReadServersProcessedSQLLiteDB(sqlite_conn, "SELECT * FROM Jobs WHERE date(Day) BETWEEN date('" + vm.DateFrom.ToString("yyyy-MM-dd") + "') AND date('" + vm.DateTo.ToString("yyyy-MM-dd") + "')");
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = sqlquery;

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            //while (sqlite_datareader.Read())
            //{


            //    WeeklyServerRecordsModel job = new WeeklyServerRecordsModel();
            //    job.id = sqlite_datareader.GetInt32(0);
            //    job.Day = Convert.ToDateTime(sqlite_datareader.GetString(1));
            //    job.SONumber = sqlite_datareader.GetString(2);
            //    job.ServerModel = sqlite_datareader.GetString(3);
            //    job.Quantity = sqlite_datareader.GetInt32(4);
            //    job.Technician = sqlite_datareader.GetString(5);
            //    job.SalesRep = sqlite_datareader.GetString(6);
            //    job.JobType = sqlite_datareader.GetString(7);
            //    job.Issues = sqlite_datareader.GetString(8);
            //    job.ActionTaken = sqlite_datareader.GetString(9);
            //    job.Status = sqlite_datareader.GetString(10);
            //    job.BuildQuantity = sqlite_datareader.GetInt32(11);
            //    job.BuildFWQuantity = sqlite_datareader.GetInt32(12);
            //    job.BuildTestQuantity = sqlite_datareader.GetInt32(13);
            //    job.CPUTestQuantity = sqlite_datareader.GetInt32(14);
            //    job.CPUZQuantity = sqlite_datareader.GetInt32(15);
            //    job.CTOQuantity = sqlite_datareader.GetInt32(16);
            //    job.DIMMTestQuantity = sqlite_datareader.GetInt32(17);
            //    job.IOBoardQuantity = sqlite_datareader.GetInt32(18);
            //    job.MotherboardQuantity = sqlite_datareader.GetInt32(19);
            //    job.RAMReplaceQuantity = sqlite_datareader.GetInt32(20);
            //    job.RMAReplaceBuildQuantity = sqlite_datareader.GetInt32(21);
            //    job.PlugAndPlayQuantity = sqlite_datareader.GetInt32(22);
            //    job.POTestingQuantity = sqlite_datareader.GetInt32(23);



            //    vm.WeeklyServerRecordCollection.Add(job);


            //    // string myreader = sqlite_datareader.GetString(0);
            //    //  MessageBox.Show(sqlite_datareader.GetString(1).ToString());
            //}
        }


        public static void UpdateServersProcessedSQLLiteDB(string fileToRead)
        {
            //Call Example: StaticFunctions.ReadSQLLiteDB(@"URI=file:" + @"\\pinnacle.local\tech_resources\Nebula\XML Weekly Server Records\new\WeeklyRecords.db");
            string cs = fileToRead;
        }

        public static void DeleteServersProcessedSQLLiteDB(string fileToRead)
        {
            //Call Example: StaticFunctions.ReadSQLLiteDB(@"URI=file:" + @"\\pinnacle.local\tech_resources\Nebula\XML Weekly Server Records\new\WeeklyRecords.db");
            string cs = fileToRead;
        }







        //END SQL CRUD
        public static void UpdateCSVField(string insertString)
        {
            string TheDate = DateTime.Now.ToShortDateString();
            string TheTime = DateTime.Now.ToShortTimeString();
            string TheYear = DateTime.Now.Year.ToString();

            string filetoread = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv";

            using (TextFieldParser parser = new TextFieldParser(filetoread))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                while (!parser.EndOfData)
                {
                    //Processing row
                    string[] fields = parser.ReadFields();
                    foreach (string field in fields)
                    {
                        if (fields[2] == "100")
                        {
                            fields[2] = "0";
                        }
                        //TODO: Process field


                    }


                }


            }

            //var lines = new string[7];
            //var splitLines = lines.Select(l => l.Split(','));
            //foreach (var splitLine in splitLines)
            //{
            //    if (splitLine[2] == "100")
            //    {
            //        splitLine[2] = "0";
            //    }
            //    var line = string.Join(",", splitLine);

            //    // And then do what you wish with the line.
            //}
        }

        //CSV and Text file

        public static void CreateAppendCSV(ReportServersProcessed itemstowrite, string filetocreate)
        {

            string TheDate = DateTime.Now.ToShortDateString();
            string TheTime = DateTime.Now.ToShortTimeString();
            string TheYear = DateTime.Now.Year.ToString();

            //Will Create a new file for each year
            string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv";

            string clientDetails = TheDate + "," + itemstowrite.ChassisSerialNo + "," + itemstowrite.ProcessedBy + "," + itemstowrite.Department + "," + TheTime + "," + itemstowrite.Status + "," + itemstowrite.Description + "," + itemstowrite.POSONo + Environment.NewLine;


            if (!File.Exists(newFileName))
            {
                string clientHeader = "Date" + "," + "Chassis Serial No" + "," + "Operative" + "," + "Department" + "," + "Time Completed" + "," + "Status" + "," + "Fault Description" + "," + @"PO\SO Number" + Environment.NewLine;

                File.WriteAllText(newFileName, clientHeader);
            }

            File.AppendAllText(newFileName, clientDetails);


        }

        //Weekly server records
        public static void CreateWeeklyServerRecordsCSV(List<WeeklyServerRecordsModel> itemstowrite)
        {

            //Will Create a new file for each year
            string newFileName = @"" + myDocs + @"\Weekly Server Records " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".csv";


            if (!File.Exists(newFileName))
            {
               
                string clientHeader = "Date" + "," + "Order Ref" + "," + "Server Model" + "," + "Build Quantity" + "," + "Build FW Quantity" + "," + "Build Test Quantity" + "," + "CPU Test Quantity" + "," + "CPUZ Quantity" + "," + "CTO Quantity" + "," + "DIMM Test Quantity" + "," + "IO Board Quantity" + "," + "Motherboard Quantity" + "," + "RAM Replace Quantity" + "," + "RMA Replace Build Quantity" + "," + "Plug And Play Quantity" + "," + "PO Testing Quantity" + "," + "Part Testing Quantity" + "," + "Client Gear Quantity" + "," + "Technician" + "," + "Sales Rep" + "," + "Job Type" + "," + "Issues" + "," + "Action Taken" + "," + "Status" + Environment.NewLine;

                File.WriteAllText(newFileName, clientHeader);
            }

            foreach (var itm in itemstowrite)
            {
                string clientDetails = "\"" + itm.Day.ToString().Replace("00:00:00","") + "\",\"" + itm.SONumber.ToUpper() + "\",\"" + itm.ServerModel.ToUpper() + "\",\"" + itm.BuildQuantity + "\",\"" + itm.BuildFWQuantity + "\",\"" + itm.BuildTestQuantity + "\",\"" + itm.CPUTestQuantity + "\",\"" + itm.CPUZQuantity + "\",\"" + itm.CTOQuantity + "\",\"" + itm.DIMMTestQuantity + "\",\"" + itm.IOBoardQuantity + "\",\"" + itm.MotherboardQuantity + "\",\"" + itm.RAMReplaceQuantity + "\",\"" + itm.RMAReplaceBuildQuantity + "\",\"" + itm.PlugAndPlayQuantity + "\",\"" + itm.POTestingQuantity + "\",\"" + itm.PartTestingQuantity + "\",\"" + itm.ClientGearQuantity + "\",\"" + itm.Technician + "\",\"" + itm.SalesRep + "\",\"" + itm.JobType + "\",\"" + itm.Issues.Replace("\"","''") + "\",\"" + itm.ActionTaken + "\",\"" + itm.Status + "\"" + Environment.NewLine;
                File.AppendAllText(newFileName, clientDetails);
            }

            //Launch CSV
            if (File.Exists(newFileName))
            {
                //Open the CSV
                StaticFunctions.OpenFileCommand(newFileName, "");
            }


        }

        public static void CreateDriveProcessingCSV(List<NetsuiteInventory> itemstowrite, string reportNameRef)
        {

            //Will Create a new file for each year
            string newFileName = @"" + myDocs + @"\Drive Processing Report " + reportNameRef + " " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".csv";


            if (!File.Exists(newFileName))
            {

                string clientHeader = "Drive Serial Number" + "," + "Brand" + "," + "PO#s" + "," + "RMA#s" + "," + "Start Date" + "," + "End Date" + "," + "Result" + Environment.NewLine;

                File.WriteAllText(newFileName, clientHeader);
            }

            foreach (var itm in itemstowrite)
            {
                string clientDetails = "\"" + itm.SerialNumber.ToString() + "\",\"\",\"" + itm.PONumber.ToUpper() + "\",\"\",\"" + itm.ProcessedDate.ToString().Replace("00:00:00","") + "\",\"" + DateTime.Now.ToShortDateString() + "\",\"" + itm.DriveResult  + "\"" + Environment.NewLine;
                File.AppendAllText(newFileName, clientDetails);
            }

            //Launch CSV
            if (File.Exists(newFileName))
            {
                //Open the CSV
                StaticFunctions.OpenFileCommand(newFileName, "");
            }


        }


        public static void CreateAppendPartCodeCSV(List<NetsuiteInventory> itemstowrite, string PONumber, string filetocreate)
        {

            //Will Create a new file for each year
            string newFileName = @"" + myDocs + @"\PartCodeReq_" + PONumber + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".csv";


            if (!File.Exists(newFileName))
            {
                //string clientHeader = "PONumber" + "," + "PartCode" + "," + "Part Description" + Environment.NewLine;
                string clientHeader = "PONumber" + "," + "PartCode" + "," + "Part Description" + Environment.NewLine;

                File.WriteAllText(newFileName, clientHeader);
            }

            foreach (var itm in itemstowrite)
            {
                string clientDetails = PONumber + "," + itm.ProductCode + "," + itm.ProductDescription + Environment.NewLine;
                File.AppendAllText(newFileName, clientDetails);
            }

            //Launch CSV
            if (File.Exists(newFileName))
            {
                //Open the CSV
                StaticFunctions.OpenFileCommand(newFileName, "");
            }


        }
        //CSV Creation to import into netsuite. Excludes New Part Codes W:\Warehouse General\Nebula\NetsuiteImport
        public static void CreateNetsuiteImportCSV(List<NetsuiteInventory> itemstowrite, string PONumber, string filetocreate)
        {
            //Check directory is there, otherwise create
            if(Directory.Exists(StaticFunctions.UncPathToUse + @"Nebula\NetsuiteImports"))
            {

            }
            else
            {
                Directory.CreateDirectory(StaticFunctions.UncPathToUse + @"Nebula\NetsuiteImports");
            }


            //Will Create a new file for each year
            string newFileName = StaticFunctions.UncPathToUse + @"Nebula\NetsuiteImports\CSV Blank " + PONumber + ".csv";


            if (!File.Exists(newFileName))
            {
                string clientHeader = "Line ID" + "," + "PO #" + "," + "PN" + "," + "QUANTITY" + Environment.NewLine;

                File.WriteAllText(newFileName, clientHeader);
            }

            foreach (var itm in itemstowrite)
            {
                string clientDetails = "," + PONumber + "," + itm.ProductCode + "," + itm.Quantity + Environment.NewLine;
                File.AppendAllText(newFileName, clientDetails);
            }

            //Launch CSV
            if (File.Exists(newFileName))
            {
                //Open the CSV
                StaticFunctions.OpenFileCommand(newFileName, "");
            }


        }


        public static void ExportServersProcessedeCSV(List<ServersProcessedModel> itemstowrite, string filetocreate)
        {

            //Will Create a new file for each year
            string newFileName = @"" + myDocs + @"\ServersProcessedReport_" + DateTime.Now.ToString().Replace("/","-").Replace(":", "_") + ".csv";


            if (!File.Exists(newFileName))
            {
                string clientHeader = "Date Processed" + "," + "POSONo" + "," + "Chassis Serial No" + "," + "Manufacturer" + "," + "Model" + "," + "Operative" + "," + "Location" + "," + "Department" + "," + "Completed" + "," + "ServerState" + "," + "Factory Reset" + "," + "GDPR Detected" + "," + "Notes" + "," + "Fault Description" + Environment.NewLine;

                File.WriteAllText(newFileName, clientHeader);
            }

            foreach (var itm in itemstowrite)
            {
                string clientDetails = itm.DateProcessed + "," + itm.POSONo + "," + itm.ChassisSerialNo + "," + itm.Manufacturer + "," + itm.ModelNo + "," + itm.Operative + "," + itm.OperativeLocation + "," + itm.Department + "," + itm.TimeCompleted + "," + itm.ServerState + "," + itm.FactoryReset + "," + itm.GDPRRiskDetected + "," + itm.Notes + "," + itm.FaultDescription  + Environment.NewLine;
                File.AppendAllText(newFileName, clientDetails);
            }

            //Launch CSV
            if (File.Exists(newFileName))
            {
                //Open the CSV
                StaticFunctions.OpenFileCommand(newFileName, "");
            }


        }


        public static void CreateQCCSV(List<QCRecordsModel> itemstowrite)
        {
            try
            {

          
            //string TheDate = DateTime.Now.ToShortDateString();
            //string TheTime = DateTime.Now.ToShortTimeString();
            //string TheYear = DateTime.Now.Year.ToString();
            //Check if file already exists
            if (File.Exists(@"" + myDocs + @"\QC_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv"))
            {
                File.Delete(@"" + myDocs + @"\QC_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv");
            }

            //Will Create a new file for each year
            string newFileName = @"" + myDocs + @"\QC_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv";


            if (!File.Exists(newFileName))
            {
                string clientHeader = "Date" + "," + "PO Number" + "," + "Part No" + "," + "Qty" + "," + "Serial No" + "," + "Booked In By" + "," + "Checked By" + "," + "Issue" + "," + "Specify" + "," + "Status" + "," + "Comments" + Environment.NewLine;

                File.WriteAllText(newFileName, clientHeader);
            }

            foreach (var itm in itemstowrite)
            {
                string clientDetails = "\"" + itm.Date + "\",\"" + itm.PONumber + "\",\"" + itm.PartNumber + "\",\"" + itm.Quantity + "\",\"" + itm.SerialNo + "\",\"" + itm.BookedInBy + "\",\"" + itm.QCCheckedBy + "\",\"" + itm.Issue + "\",\"" + itm.Specify + "\",\"" + itm.Status + "\",\"" + itm.Comments + "\"" + Environment.NewLine;
                File.AppendAllText(newFileName, clientDetails);
            }


            //Launch CSV
            if (File.Exists(@"" + myDocs + @"\QC_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv"))
            {
                //Open the CSV
                StaticFunctions.OpenFileCommand(@"" + myDocs + @"\QC_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/","-") + ".csv", "");
            }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }


        public static void CreateITADReportCSV(List<ITADReport> itemstowrite)
        {

            //string TheDate = DateTime.Now.ToShortDateString();
            //string TheTime = DateTime.Now.ToShortTimeString();
            //string TheYear = DateTime.Now.Year.ToString();

            //Check if File Already Present
            if(File.Exists(@"" + myDocs + @"\ITADReport_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv"))
            {
                File.Delete(@"" + myDocs + @"\ITADReport_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv");
            }

            //Will Create a new file for each year
            string newFileName = @"" + myDocs + @"\ITADReport_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv";


            if (!File.Exists(newFileName))
            {
                string clientHeader = "Date" + "," + "LOT Number" + "," + "Asset No" + "," + "Serial No" + "," + "Device Category" + "," + "Erase Performed" + "," + "Testing Performed" + "," + "Notes" + "," + "Tested By" + "," + "Verified By" + "," + "Verification Performed" + Environment.NewLine;

                File.WriteAllText(newFileName, clientHeader);
            }

            foreach (var itm in itemstowrite)
            {
                string clientDetails = "\"" + itm.DateCreated + "\",\"" + itm.POSONumber + "\",\"" + itm.AssetNo + "\",\"" + itm.SerialNo + "\",\"" + itm.DeviceCategory + "\",\"" + itm.ProcessPerformed + "\",\"" + itm.TestingPerformed + "\",\"" + itm.Notes + "\",\"" + itm.TestedBy + "\",\"" + itm.VerifiedBy + "\",\"" + itm.VerificationPerformed + "\"" + Environment.NewLine;
                File.AppendAllText(newFileName, clientDetails);
            }


            //Launch CSV
            if (File.Exists(@"" + myDocs + @"\ITADReport_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv"))
            {
                //Open the CSV
                StaticFunctions.OpenFileCommand(@"" + myDocs + @"\ITADReport_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv", "");
            }


        }

        public static void CreateTestReportCSV(List<ITADReport> itemstowrite)
        {

            //string TheDate = DateTime.Now.ToShortDateString();
            //string TheTime = DateTime.Now.ToShortTimeString();
            //string TheYear = DateTime.Now.Year.ToString();
            //MessageBox.Show(myDocs);
            //Check if File Already Present
            if (File.Exists(@"" + myDocs + @"\TestDeptReport_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv"))
            {
                File.Delete(@"" + myDocs + @"\TestDeptReport_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv");
            }

            //Will Create a new file for each year
            string newFileName = @"" + myDocs + @"\TestDeptReport_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv";


            if (!File.Exists(newFileName))
            {
                string clientHeader = "Date" + "," + "PO/SO Number" + "," + "Serial No" + "," + "Notes" + "," + "Tested By" + Environment.NewLine;

                File.WriteAllText(newFileName, clientHeader);
            }

            foreach (var itm in itemstowrite)
            {
                string clientDetails = "\"" + itm.DateCreated + "\",\"" + itm.POSONumber + "\",\"" + itm.SerialNo  + "\",\"" + itm.Notes + "\",\"" + itm.TestedBy + "\"" + Environment.NewLine;
                File.AppendAllText(newFileName, clientDetails);
            }


            //Launch CSV
            if (File.Exists(@"" + myDocs + @"\TestDeptReport_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv"))
            {
                //Open the CSV
                StaticFunctions.OpenFileCommand(@"" + myDocs + @"\TestDeptReport_Exported_Items_" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".csv", "");
            }


        }




        //public static void CreateAppendCSV(ReportServersProcessed itemstowrite, string filetocreate)
        //{

        //    string TheDate = DateTime.Now.ToShortDateString();
        //    string TheTime = DateTime.Now.ToShortTimeString();
        //    string TheYear = DateTime.Now.Year.ToString();

        //    //Will Create a new file for each year
        //    string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv";

        //    string clientDetails = TheDate + "," + itemstowrite.ChassisSerialNo + "," + itemstowrite.ProcessedBy + "," + itemstowrite.Department + "," + TheTime + Environment.NewLine;


        //    if (!File.Exists(newFileName))
        //    {
        //        string clientHeader = "Date" + "," + "Chassis Serial No" + "," + "Operative" + "," + "Department" + "," + "Time Completed" + Environment.NewLine;

        //        File.WriteAllText(newFileName, clientHeader);
        //    }

        //    File.AppendAllText(newFileName, clientDetails);


        //}

        public static void ReadCSV(string filetoread)
        {

            //string TheYear = DateTime.Now.Year.ToString();

            ////Will Create a new file for each year
            //string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv";
            //string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv";
            //Uses a VB class to process the file instead of streamreader
            using (TextFieldParser parser = new TextFieldParser(filetoread))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                while (!parser.EndOfData)
                {
                    //Processing row
                    string[] fields = parser.ReadFields();
                    foreach (string field in fields)
                    {
                        //TODO: Process field
                    }
                }
            }

          

        }
        public static void ReadCSVServersProcessed(string filetoread)
        {

            string TheYear = DateTime.Now.Year.ToString();

            //Will Create a new file for each year
            string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv";
            //string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv"; 
            //Uses a VB class to process the file instead of streamreader
            using (TextFieldParser parser = new TextFieldParser(newFileName))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                while (!parser.EndOfData)
                {
                    //Processing row
                    string[] fields = parser.ReadFields();
                    foreach (string field in fields)
                    {
                        MessageBox.Show(field);
                        //TODO: Process field
                    }
                }
            }



        }

        public static void CreateLoggedInTxtFile(string user, string path)
        {

           //MessageBox.Show(@"" + path + user + ".txt");
            // Write a txt documment for each checklist produced.
            using (StreamWriter sw = new StreamWriter(@"" + path + user + ".txt"))
            {

                sw.WriteLine(user + " Logged In At: " + DateTime.Now.ToString());

            }
        }


        public static void CreateTxtFile(List<string> itemstowrite)
        {
            // Write a txt documment for each checklist produced.
            //using (StreamWriter sw = new StreamWriter(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\Servers_Processed.txt"))
            //{

            //    sw.WriteLine(vm.GoodsINRep + " Completed " + vm.ChassisSerialNo + " At " + DateTime.Now.ToString().Replace(@"/", "-").Replace(":", "_"));

            //}
        }

        public static void ReadTxtFile(string file)
        {

           // Read and show each line from the file.
            string line = "";
            using (StreamReader sr = new StreamReader(@"" + @"\\pinnacle.local\tech_resources\Nebula\Application\mac_vendors\mac-vendor.txt"))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    Debug.WriteLine(line);
                    //MessageBox.Show(line.Substring(7));
                }
            }
        }


        //PING
        public static void sendAsyncPingPacket(string hostToPing,ListBox lb)
        {
            try
            {
                int timeout = 5000;
                AutoResetEvent waiter = new AutoResetEvent(false);
                Ping pingPacket = new Ping();
                //ping completion event reaised
                pingPacket.PingCompleted += new PingCompletedEventHandler(PingCompletedCallback);
                string data = "Ping test check";
                byte[] byteBuffer = Encoding.ASCII.GetBytes(data);
                PingOptions pingOptions = new PingOptions(64, true);
                Debug.WriteLine("Time to live: {0}", pingOptions.Ttl);
                //("Time to live: {0}", pingOptions.Ttl);
                //Console.WriteLine("Don't fragment: {0}", pingOptions.DontFragment);
                pingPacket.SendAsync(hostToPing, timeout, byteBuffer, pingOptions, waiter);


                //do something useful
                waiter.WaitOne();
                Debug.WriteLine("Ping RoundTrip returned, Do something useful here...");
            }
            catch (PingException pe)
            {
                string exception = pe.Message;
                Debug.WriteLine("INVALID IP ADDRESS FOUND");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exceptin " + ex.Message);
            }

        }

        private static void PingCompletedCallback(object sender, PingCompletedEventArgs e)
        {
            try
            {
                if (e.Cancelled)
                {
                    Debug.WriteLine("Ping canceled.");

                    // Let the main thread resume. 
                    // UserToken is the AutoResetEvent object that the main thread 
                    // is waiting for.
                    ((AutoResetEvent)e.UserState).Set();
                }

                // If an error occurred, display the exception to the user.
                if (e.Error != null)
                {
                    Debug.WriteLine("Ping failed>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
                    //this will print exception
                    //Console.WriteLine (e.Error.ToString ());

                    // Let the main thread resume. 
                    ((AutoResetEvent)e.UserState).Set();
                }

                PingReply reply = e.Reply;

                DisplayReply(reply);

                // Let the main thread resume.
                ((AutoResetEvent)e.UserState).Set();
            }
            catch (PingException pe)
            {
                string exception = pe.Message;
                Debug.WriteLine("INVALID IP ADDRESS");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception " + ex.Message);
            }
        }

        public static void DisplayReply(PingReply reply)
        {
            if (reply == null)
                return;

            Debug.WriteLine("ping status: {0}", reply.Status);
            if (reply.Status == IPStatus.Success)
            {
               
                Debug.WriteLine("Address: {0}", reply.Address.ToString());
                Debug.WriteLine("RoundTrip time: {0}", reply.RoundtripTime);
                Debug.WriteLine("Time to live: {0}", reply.Options.Ttl);
                //Console.WriteLine ("Don't fragment: {0}", reply.Options.DontFragment);
                Debug.WriteLine("Buffer size: {0}", reply.Buffer.Length);
            }
        }

        //private static long ToInt(string addr)
        //{

        //    return (long)(uint)IPAddress.NetworkToHostOrder((int)System.Net.IPAddress.Parse(addr).AddressFamilyAddress);
        //}

        //private static string ToAddr(long address)
        //{
        //    return System.Net.IPAddress.Parse(address.ToString()).ToString();
        //}

        //static int temp = 0;
        //private static void scanLiveHosts(string ipFrom, string ipTo)
        //{
        //    long from = Program.ToInt(ipFrom);
        //    long to = Program.ToInt(ipTo);

        //    long ipLong = Program.ToInt(ipFrom);
        //    while (from < to)
        //    {

        //        string address = Program.ToAddr(ipLong);
        //        Program.sendAsyncPingPacket(address);
        //        ipLong++;
        //    }

        //}





        public static void RunCLICommand(string ExePath, string Args)
        {
            //StaticFunctions.RunCLICommand("","");
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = true;
            //MessageBox.Show(myInput);
            //MessageBox.Show(myOutput);
            //startInfo.FileName = @"GhostScript\gxps915.exe";

            //startInfo.FileName = @"" + StaticFunctions.UncPathToUse + @"nick\ghostscript\gswin64c.exe";

            startInfo.FileName = ExePath;

            //startInfo.FileName = @"" + myDocs + @"\Workflow-User\gswin64c.exe";

            //MessageBox.Show(@"" + myDocs + @"\Workflow-User\gswin64c.exe");

            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            //gxps915.exe -sDEVICE=pdfwrite -sOutputFile=myfile2.pdf -dNOPAUSE maintest.xps

            //startInfo.Arguments = " -dNOPAUSE -dBATCH -r300 -sCompression=none -sDEVICE=tiffg4 -sOutputFile=" + myOutput + " " + myInput + "";
            //Use this as Higher res = smaller djvu file size
            //startInfo.Arguments = "-q -dNOPAUSE -dBATCH -r600 -sDEFAULTPAPERSIZE=a4  -sDEVICE=tiffg4 -sOutputFile=" + myOutput + " " + myInput + "";

            //startInfo.Arguments = "-q -dNOPAUSE -dSAFER -dBATCH -sOutputFile=" + myOutput + " -sDEVICE=pdfwrite watermark.ps -f 5949S.pdf";   @"" + myDocs + @"\Workflow-User\watermark.ps -f 

            //*startInfo.Arguments = "-q -dNOPAUSE -dSAFER -dBATCH -sOutputFile=" + myOutput + " -sDEVICE=pdfwrite " + myDocs + @"\Workflow-User\watermark.ps -f " + myInput + "";

            //startInfo.Arguments = "-q -dNOPAUSE -dSAFER -dBATCH -sOutputFile=" + myOutput + " -sDEVICE=pdfwrite " + StaticFunctions.UncPathToUse + @"nick\ghostscript\watermark.ps -f " + myInput + "";

            startInfo.Arguments = Args;

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                   // exeProcess.WaitForExit();
                }
            }
            catch
            {
                // Log error.
            }

        }// 


        public static void OpenFileCommand(string ExePath, string Args)
        {
            //StaticFunctions.RunCLICommand("","");
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = true;
          
            startInfo.FileName = ExePath;

            startInfo.WindowStyle = ProcessWindowStyle.Normal;
         

            startInfo.Arguments = Args;

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    //exeProcess.WaitForExit();
                }
            }
            catch
            {
                // Log error.
            }

        }// 


        public static void RunRemoteConsoleCommand(string ExePath, string Args)
        {
            //StaticFunctions.RunCLICommand("","");
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = true;
            //MessageBox.Show(myInput);
            //MessageBox.Show(myOutput);
            //startInfo.FileName = @"GhostScript\gxps915.exe";

            //startInfo.FileName = @"" + StaticFunctions.UncPathToUse + @"nick\ghostscript\gswin64c.exe";

            startInfo.FileName = ExePath;

            //startInfo.FileName = @"" + myDocs + @"\Workflow-User\gswin64c.exe";

            //MessageBox.Show(@"" + myDocs + @"\Workflow-User\gswin64c.exe");

            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            //gxps915.exe -sDEVICE=pdfwrite -sOutputFile=myfile2.pdf -dNOPAUSE maintest.xps

            //startInfo.Arguments = " -dNOPAUSE -dBATCH -r300 -sCompression=none -sDEVICE=tiffg4 -sOutputFile=" + myOutput + " " + myInput + "";
            //Use this as Higher res = smaller djvu file size
            //startInfo.Arguments = "-q -dNOPAUSE -dBATCH -r600 -sDEFAULTPAPERSIZE=a4  -sDEVICE=tiffg4 -sOutputFile=" + myOutput + " " + myInput + "";

            //startInfo.Arguments = "-q -dNOPAUSE -dSAFER -dBATCH -sOutputFile=" + myOutput + " -sDEVICE=pdfwrite watermark.ps -f 5949S.pdf";   @"" + myDocs + @"\Workflow-User\watermark.ps -f 

            //*startInfo.Arguments = "-q -dNOPAUSE -dSAFER -dBATCH -sOutputFile=" + myOutput + " -sDEVICE=pdfwrite " + myDocs + @"\Workflow-User\watermark.ps -f " + myInput + "";

            //startInfo.Arguments = "-q -dNOPAUSE -dSAFER -dBATCH -sOutputFile=" + myOutput + " -sDEVICE=pdfwrite " + StaticFunctions.UncPathToUse + @"nick\ghostscript\watermark.ps -f " + myInput + "";

            startInfo.Arguments = Args;

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                   // exeProcess.WaitForExit();
                }
            }
            catch
            {
                // Log error.
            }

        }// 


        //OFFICE INTEROP SECTION

        //EMAIL SECTION

        //public static void CreateMailItem(List<string> attachList, string mailClient, string messageTo, string messageSubject, string messageBody)
        //{

        //    // references required
        //    // Microsoft.Office.Core version 12, although 15 can also be used
        //    // Microsoft.Office.Interop.Outlook
        //    // includes
        //    //using Excel = Microsoft.Office.Interop.Excel;
        //    //using Word = Microsoft.Office.Interop.Word;
        //    //using Outlook = Microsoft.Office.Interop.Outlook;

        //    //Outlook.MailItem mailItem = (Outlook.MailItem)
        //    //Application.CreateItem(Outlook.OlItemType.olMailItem);
        //    //mailItem.Subject = "This is the subject";
        //    //mailItem.To = "someone@example.com";
        //    //mailItem.Body = "This is the message.";
        //    //mailItem.Importance = Outlook.OlImportance.olImportanceLow;
        //    //mailItem.Display(false);

        //    //try
        //    //{


        //    Outlook.Application oApp = new Outlook.Application();
        //    Outlook._MailItem oMailItem = (Outlook._MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
        //    oMailItem.To = messageTo;
        //    oMailItem.CC = "";
        //    oMailItem.BCC = "";
        //    oMailItem.Subject = messageSubject;
        //    //oMailItem.Body = "";




        //    //ADD ATTACHMENT SPECIFIC CODE HERE
        //    // Attach the items from the array list

        //    foreach (var att in attachList)
        //    {
        //        oMailItem.Attachments.Add(att, Outlook.OlAttachmentType.olByValue, 1, att);
        //    }

        //    //for (int i = 1; i <= attachList.Count(); i++)
        //    //{
        //    //   // because of the nature of the array ensure you decrement the i value by one before adding
        //    //    oMailItem.Attachments.Add(attachList[i - 1], Outlook.OlAttachmentType.olByValue, 1, attachList[i - 1]);

        //    //}



        //    BusyDecorator busyInd2 = (BusyDecorator)Helper.GetDescendantFromName(System.Windows.Application.Current.MainWindow, "busyMarketing");
        //    //SET BUSY INDICATOR AND TEXT
        //    // set busy indicator message
        //    //LoginVM.BusyIndicatorMessage = "Please Wait";
        //    // open the busy indicator
        //    if (busyInd2 != null)
        //        busyInd2.IsBusyIndicatorShowing = false;



        //    // oMailItem.Attachments.Add(attachList[0],Outlook.OlAttachmentType.olByValue,1, attachList[0]);
        //    //body, bcc etc...
        //    oMailItem.Display(true);




        //    //}
        //    //catch (Exception ex)
        //    //{

        //    //    DialogYesNo("Outlook is not open, or a mail item is not selected. Please rectify and try again.", "Email Generation Error",false);
        //    //}


        //}


        //public static void SaveMailAttachment()
        //{
        //    try
        //    {


        //        //Outlook.Attachment attachment = default(Outlook.Attachment);
        //        //string attname = null;
        //        //List<string> attachlist = new List<string>();
        //        Outlook.Application olApp = new Outlook.Application();
        //        //string emailinfo = null;
        //        //string body = "";
        //        //olApp = Interaction.CreateObject("Outlook.Application");
        //        //Outlook.MailItem mitem = olApp.ActiveExplorer().Selection.Item(1);



        //        Outlook.MailItem mitem = olApp.ActiveExplorer().Selection[1];



        //        //ADD ATTACHMENT SPECIFIC CODE HERE
        //        if (mitem.Attachments.Count > 0)
        //        {
        //            for (int i = 1; i <= mitem.Attachments.Count; i++)
        //            {
        //                if (mitem.Attachments[i].FileName.Contains(".docx") || mitem.Attachments[i].FileName.Contains(".doc"))
        //                    mitem.Attachments[i].SaveAsFile(@"" + myDocs + @"\Workflow-User\Attachments\" + mitem.Attachments[i].FileName);
        //                //MessageBox.Show("attach");
        //            }
        //        }
        //        // closes outlook
        //        // olApp.Quit();

        //    }
        //    catch (Exception ex)
        //    {

        //        //throw;
        //        DialogYesNo("Outlook is not open, or a mail item is not selected. Please rectify and try again.", "Mail Attachment Capture Error", false);
        //    }


        //    //MessageBox.Show("Hello There");
        //    //do
        //    //{
        //    //}
        //    //while (IsFileLocked(@"" + myDocs + @"\Workflow-User\Email\xEmail.xps") == true);


        //}


        // WORD 
        //public static void CreateWordDocument2()
        //{
        //    object oMissing = System.Reflection.Missing.Value;
        //    object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

        //    //Start Word and create a new document.
        //    Word._Application oWord;
        //    Word._Document oDoc;
        //    oWord = new Word.Application();
        //    oWord.Visible = true;
        //    oDoc = oWord.Documents.Add(ref oMissing, ref oMissing,
        //        ref oMissing, ref oMissing);

        //    //Insert a paragraph at the beginning of the document.
        //    Word.Paragraph oPara1;
        //    oPara1 = oDoc.Content.Paragraphs.Add(ref oMissing);
        //    oPara1.Range.Text = "Heading 1";
        //    oPara1.Range.Font.Bold = 1;
        //    oPara1.Format.SpaceAfter = 24;    //24 pt spacing after paragraph.
        //    oPara1.Range.InsertParagraphAfter();

        //    //Insert a paragraph at the end of the document.
        //    Word.Paragraph oPara2;
        //    object oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //    oPara2 = oDoc.Content.Paragraphs.Add(ref oRng);
        //    oPara2.Range.Text = "Heading 2";
        //    oPara2.Format.SpaceAfter = 6;
        //    oPara2.Range.InsertParagraphAfter();

        //    //Insert another paragraph.
        //    Word.Paragraph oPara3;
        //    oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //    oPara3 = oDoc.Content.Paragraphs.Add(ref oRng);
        //    oPara3.Range.Text = "This is a sentence of normal text. Now here is a table:";
        //    oPara3.Range.Font.Bold = 0;
        //    oPara3.Format.SpaceAfter = 24;
        //    oPara3.Range.InsertParagraphAfter();

        //    //Insert a 3 x 5 table, fill it with data, and make the first row
        //    //bold and italic.
        //    Word.Table oTable;
        //    Word.Range wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //    oTable = oDoc.Tables.Add(wrdRng, 3, 5, ref oMissing, ref oMissing);
        //    oTable.Range.ParagraphFormat.SpaceAfter = 6;
        //    int r, c;
        //    string strText;
        //    for (r = 1; r <= 3; r++)
        //        for (c = 1; c <= 5; c++)
        //        {
        //            strText = "r" + r + "c" + c;
        //            oTable.Cell(r, c).Range.Text = strText;
        //        }
        //    oTable.Rows[1].Range.Font.Bold = 1;
        //    oTable.Rows[1].Range.Font.Italic = 1;

        //    //Add some text after the table.
        //    Word.Paragraph oPara4;
        //    oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //    oPara4 = oDoc.Content.Paragraphs.Add(ref oRng);
        //    oPara4.Range.InsertParagraphBefore();
        //    oPara4.Range.Text = "And here's another table:";
        //    oPara4.Format.SpaceAfter = 24;
        //    oPara4.Range.InsertParagraphAfter();

        //    //Insert a 5 x 2 table, fill it with data, and change the column widths.
        //    wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //    oTable = oDoc.Tables.Add(wrdRng, 5, 2, ref oMissing, ref oMissing);
        //    oTable.Range.ParagraphFormat.SpaceAfter = 6;
        //    for (r = 1; r <= 5; r++)
        //        for (c = 1; c <= 2; c++)
        //        {
        //            strText = "r" + r + "c" + c;
        //            oTable.Cell(r, c).Range.Text = strText;
        //        }
        //    oTable.Columns[1].Width = oWord.InchesToPoints(2); //Change width of columns 1 & 2
        //    oTable.Columns[2].Width = oWord.InchesToPoints(3);

        //    //Keep inserting text. When you get to 7 inches from top of the
        //    //document, insert a hard page break.
        //    object oPos;
        //    double dPos = oWord.InchesToPoints(7);
        //    oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range.InsertParagraphAfter();
        //    do
        //    {
        //        wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //        wrdRng.ParagraphFormat.SpaceAfter = 6;
        //        wrdRng.InsertAfter("A line of text");
        //        wrdRng.InsertParagraphAfter();
        //        oPos = wrdRng.get_Information
        //                       (Word.WdInformation.wdVerticalPositionRelativeToPage);
        //    }
        //    while (dPos >= Convert.ToDouble(oPos));
        //    object oCollapseEnd = Word.WdCollapseDirection.wdCollapseEnd;
        //    object oPageBreak = Word.WdBreakType.wdPageBreak;
        //    wrdRng.Collapse(ref oCollapseEnd);
        //    wrdRng.InsertBreak(ref oPageBreak);
        //    wrdRng.Collapse(ref oCollapseEnd);
        //    wrdRng.InsertAfter("We're now on page 2. Here's my chart:");
        //    wrdRng.InsertParagraphAfter();

        //    //Insert a chart.
        //    Word.InlineShape oShape;
        //    object oClassType = "MSGraph.Chart.8";
        //    wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //    oShape = wrdRng.InlineShapes.AddOLEObject(ref oClassType, ref oMissing,
        //        ref oMissing, ref oMissing, ref oMissing,
        //        ref oMissing, ref oMissing, ref oMissing);

        //    //Demonstrate use of late bound oChart and oChartApp objects to
        //    //manipulate the chart object with MSGraph.
        //    object oChart;
        //    object oChartApp;
        //    oChart = oShape.OLEFormat.Object;
        //    oChartApp = oChart.GetType().InvokeMember("Application",
        //        BindingFlags.GetProperty, null, oChart, null);

        //    //Change the chart type to Line.
        //    object[] Parameters = new Object[1];
        //    Parameters[0] = 4; //xlLine = 4
        //    oChart.GetType().InvokeMember("ChartType", BindingFlags.SetProperty,
        //        null, oChart, Parameters);

        //    //Update the chart image and quit MSGraph.
        //    oChartApp.GetType().InvokeMember("Update",
        //        BindingFlags.InvokeMethod, null, oChartApp, null);
        //    oChartApp.GetType().InvokeMember("Quit",
        //        BindingFlags.InvokeMethod, null, oChartApp, null);
        //    //... If desired, you can proceed from here using the Microsoft Graph 
        //    //Object model on the oChart and oChartApp objects to make additional
        //    //changes to the chart.

        //    //Set the width of the chart.
        //    oShape.Width = oWord.InchesToPoints(6.25f);
        //    oShape.Height = oWord.InchesToPoints(3.57f);

        //    //Add text after the chart.
        //    wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //    wrdRng.InsertParagraphAfter();
        //    wrdRng.InsertAfter("THE END.");

        //    //Close this form.
        //    //this.Close();

        //}

        //EXCEL SPECIFIC FUNCTIONS

        //private static Excel.Workbook MyBook = null;
        //private static Excel.Application MyApp = null;
        //private static Excel.Worksheet MySheet = null;

        //public static void ExcelWriteValues(string excelPath, string excelSheetName, object excelFields)
        //{

        //    // obtain the last number from column a
        //    double NewNumber = ExcelGetLastColumnValue(excelPath, 1);



        //    MyApp = new Excel.Application();
        //    MyApp.Visible = false;



        //    //To overcome the excel prompting to resave simply pass in a 3rd parameter of FALSE to the workBooks.Open(@"C:\\temp\\test.xls", false, false);
        //    MyBook = MyApp.Workbooks.Open(excelPath, false, false, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing); // e.g @"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\media orders store\Media orders.xls"
        //    //MyBook = MyApp.Workbooks.Open(excelPath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing); // e.g @"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\media orders store\Media orders.xls"
        //    MySheet = (Excel.Worksheet)MyBook.Sheets[1];


        //    //Excel.Range last = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
        //    //Excel.Range range = sheet.get_Range("A1", last);

        //    int lastRow = MySheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing).Row;
        //    // find the last row number and incriment by one for the new entry location
        //    lastRow += 1;

        //    //MessageBox.Show(lastRow.ToString());

        //    //MarketingMediaOrderExcel mord1 = new MarketingMediaOrderExcel();

        //    ////mord1.



        //    //for (int i = 0; i < 8; i++)
        //    //{
        //    //    //Console.WriteLine(i);
        //    //    MySheet.Cells[lastRow, i] = NewNumber;
        //    //}
        //    switch (excelSheetName)
        //    {
        //        case "MediaOrder":
        //            MarketingMediaOrderExcel mord1 = (MarketingMediaOrderExcel)excelFields;

        //            MySheet.Cells[lastRow, 1] = NewNumber;
        //            MySheet.Cells[lastRow, 2] = mord1.GroupOrMagazine;
        //            MySheet.Cells[lastRow, 3] = mord1.DeliveryDate;
        //            MySheet.Cells[lastRow, 4] = mord1.MagIssue;
        //            MySheet.Cells[lastRow, 5] = mord1.MailingDate;
        //            MySheet.Cells[lastRow, 6] = mord1.MailingCost;
        //            MySheet.Cells[lastRow, 7] = mord1.ThirdParty;
        //            MySheet.Cells[lastRow, 8] = mord1.Invoice;
        //            MySheet.Cells[lastRow, 9] = mord1.DateInvoiceReceived;
        //            MySheet.Cells[lastRow, 10] = mord1.InvoiceNo;
        //            //EmpList.Add(emp);
        //            MyBook.Save();

        //            MyBook.Close();

        //            break;
        //        case "UKJobSheet":
        //            MarketingUKJobsExcel ukjobs1 = (MarketingUKJobsExcel)excelFields;

        //            MySheet.Cells[lastRow, 1] = NewNumber;
        //            MySheet.Cells[lastRow, 2] = ukjobs1.RoughProof;
        //            MySheet.Cells[lastRow, 3] = ukjobs1.Description;
        //            MySheet.Cells[lastRow, 4] = ukjobs1.Size;
        //            MySheet.Cells[lastRow, 5] = ukjobs1.NoOfSides;
        //            MySheet.Cells[lastRow, 6] = ukjobs1.NoOfColours;
        //            MySheet.Cells[lastRow, 7] = ukjobs1.Fold;
        //            MySheet.Cells[lastRow, 8] = ukjobs1.Category;
        //            MySheet.Cells[lastRow, 9] = ukjobs1.AmendmentsA;
        //            MySheet.Cells[lastRow, 10] = ukjobs1.AmendmentsB;
        //            MySheet.Cells[lastRow, 11] = ukjobs1.AmendmentsC;
        //            MySheet.Cells[lastRow, 12] = ukjobs1.PrintRun;
        //            MySheet.Cells[lastRow, 13] = ukjobs1.DeliveryDate;
        //            MySheet.Cells[lastRow, 14] = ukjobs1.DateSent;
        //            MySheet.Cells[lastRow, 15] = ukjobs1.RPDone;
        //            MySheet.Cells[lastRow, 16] = ukjobs1.Terrys;
        //            MySheet.Cells[lastRow, 17] = ukjobs1.AdditionalInvoicesA;
        //            MySheet.Cells[lastRow, 18] = ukjobs1.AdditionalInvoicesB;
        //            MySheet.Cells[lastRow, 19] = ukjobs1.AdditionalInvoicesC;
        //            MySheet.Cells[lastRow, 20] = ukjobs1.SP;


        //            //EmpList.Add(emp);
        //            MyBook.Save();

        //            MyBook.Close();
        //            break;
        //        case "IRJobSheet":
        //            MarketingIRJobsExcel irjobs1 = (MarketingIRJobsExcel)excelFields;

        //            MySheet.Cells[lastRow, 1] = NewNumber;
        //            MySheet.Cells[lastRow, 2] = irjobs1.Group;
        //            MySheet.Cells[lastRow, 3] = irjobs1.LeafletDescription;
        //            MySheet.Cells[lastRow, 4] = irjobs1.Quantity;
        //            MySheet.Cells[lastRow, 5] = irjobs1.DatePRReq;
        //            MySheet.Cells[lastRow, 6] = irjobs1.DateSentToSPS;
        //            MySheet.Cells[lastRow, 7] = irjobs1.EstimatedMailDate;
        //            MySheet.Cells[lastRow, 8] = irjobs1.ANPostDate;
        //            MySheet.Cells[lastRow, 9] = irjobs1.SleepersReceived;
        //            MySheet.Cells[lastRow, 10] = irjobs1.InvoiceNumbersCategory;
        //            MySheet.Cells[lastRow, 11] = irjobs1.InvoiceNumbersAmends;
        //            MySheet.Cells[lastRow, 12] = irjobs1.InvoiceNumbersArtwork;
        //            MySheet.Cells[lastRow, 13] = irjobs1.InvoiceNumbersPrint;
        //            MySheet.Cells[lastRow, 14] = irjobs1.InvoiceNumbersMail;
        //            MySheet.Cells[lastRow, 15] = irjobs1.MailingDestination;
        //            MySheet.Cells[lastRow, 16] = irjobs1.Notes;

        //            //EmpList.Add(emp);
        //            MyBook.Save();

        //            MyBook.Close();
        //            break;
        //    }



        //IMPLEMENTATION

        //MarketingMediaOrderExcel mord1 = new MarketingMediaOrderExcel();
        //mord1.GroupOrMagazine = "Test Object1";
        //mord1.DeliveryDate = DateTime.Today.ToString("dd-M-yyyy");
        //mord1.MagIssue = "March";
        //mord1.MailingDate = DateTime.Today.ToString("dd-M-yyyy");
        //mord1.MailingCost = "1523.22";
        //mord1.ThirdParty = "40.00";
        //mord1.Invoice = "Arcadia";
        //mord1.DateInvoiceReceived = DateTime.Today.ToString("dd-M-yyyy");
        //mord1.InvoiceNo = "512";



        //MarketingMediaOrderExcel mord2 = new MarketingMediaOrderExcel();
        //mord2.GroupOrMagazine = "Test Object2";
        //mord2.DeliveryDate = DateTime.Today.ToString("dd-M-yyyy");
        //mord2.MagIssue = "May";
        //mord2.MailingDate = DateTime.Today.ToString("dd-M-yyyy");
        //mord2.MailingCost = "2123.22";
        //mord2.ThirdParty = "90.00";
        //mord2.Invoice = "Arcadia";
        //mord2.DateInvoiceReceived = DateTime.Today.ToString("dd-M-yyyy");
        //mord2.InvoiceNo = "513";

        ////StaticFunctions.ExcelGetLastColumnValue(@"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\media orders store\Media orders.xls",1);

        //StaticFunctions.ExcelWriteValues(@"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\media orders store\Media orders.xls", "MediaOrder", mord1);

        //StaticFunctions.ExcelWriteValues(@"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\media orders store\Media orders.xls", "MediaOrder", mord2);



        //}

        //public static void ExcelUpdateRow(string excelPath, int column2Search, string searchValue, string RangeFrom, string RangeTo, string excelSheetName, object excelFields)
        //{

        //    //IMPLIMENTATION

        //    // Call the excel search to find which row a value resides. 
        //    //You can use the ranges at the end to specify the search criteria 
        //    //e.g A1 - A65536 is the max for the first column Change the last range to e.g J65536 or whatever is the last column in the workbook.

        //    //MarketingMediaOrderExcel mord1 = new MarketingMediaOrderExcel();
        //    //mord1.MediaNo = "1511";
        //    //mord1.GroupOrMagazine = "Test Object2";
        //    //mord1.DeliveryDate = DateTime.Today.ToString("dd-M-yyyy");
        //    //mord1.MagIssue = "March";
        //    //mord1.MailingDate = DateTime.Today.ToString("dd-M-yyyy");
        //    //mord1.MailingCost = "1523.22";
        //    //mord1.ThirdParty = "40.00";
        //    //mord1.Invoice = "Arcadia";
        //    //mord1.DateInvoiceReceived = DateTime.Today.ToString("dd-M-yyyy");
        //    //mord1.InvoiceNo = "512";


        //    //StaticFunctions.ExcelUpdateRow(@"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\media orders store\Media orders.xls", 1, "1511", "A1", "A65536", "MediaOrder", mord1);

        //    // END IMPLIMENTATION


        //    MyApp = new Excel.Application();
        //    MyApp.Visible = false;
        //    MyBook = MyApp.Workbooks.Open(excelPath); // e.g @"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\media orders store\Media orders.xls"
        //    MySheet = (Excel.Worksheet)MyBook.Sheets[1];

        //    // Search the workbook in specified column

        //    //FindAFile the last row
        //    Excel.Range lastRow = MySheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);

        //    //MessageBox.Show(lastRow.Row.ToString());
        //    // Search the full range of the column you need
        //    //Excel.Range rng = MySheet.get_Range("A1", "J65536");
        //    Excel.Range rng = MySheet.get_Range(RangeFrom, RangeTo);

        //    Excel.Range findRng = rng.Find(searchValue, Missing.Value, Excel.XlFindLookIn.xlValues, Missing.Value, Missing.Value, Excel.XlSearchDirection.xlNext, false, false, Missing.Value);

        //    //MessageBox.Show(findRng.Row.ToString());



        //    switch (excelSheetName)
        //    {
        //        case "MediaOrder":
        //            MarketingMediaOrderExcel mord1 = (MarketingMediaOrderExcel)excelFields;

        //            MySheet.Cells[findRng.Row.ToString(), 1] = mord1.MediaNo;
        //            MySheet.Cells[findRng.Row.ToString(), 2] = mord1.GroupOrMagazine;
        //            MySheet.Cells[findRng.Row.ToString(), 3] = mord1.DeliveryDate;
        //            MySheet.Cells[findRng.Row.ToString(), 4] = mord1.MagIssue;
        //            MySheet.Cells[findRng.Row.ToString(), 5] = mord1.MailingDate;
        //            MySheet.Cells[findRng.Row.ToString(), 6] = mord1.MailingCost;
        //            MySheet.Cells[findRng.Row.ToString(), 7] = mord1.ThirdParty;
        //            MySheet.Cells[findRng.Row.ToString(), 8] = mord1.Invoice;
        //            MySheet.Cells[findRng.Row.ToString(), 9] = mord1.DateInvoiceReceived;
        //            MySheet.Cells[findRng.Row.ToString(), 10] = mord1.InvoiceNo;
        //            //EmpList.Add(emp);
        //            MyBook.Save();

        //            MyBook.Close();

        //            break;
        //        case "UKJobSheet":
        //            MarketingUKJobsExcel ukjobs1 = (MarketingUKJobsExcel)excelFields;

        //            MySheet.Cells[findRng.Row.ToString(), 1] = ukjobs1.JobNo;
        //            MySheet.Cells[findRng.Row.ToString(), 2] = ukjobs1.RoughProof;
        //            MySheet.Cells[findRng.Row.ToString(), 3] = ukjobs1.Description;
        //            MySheet.Cells[findRng.Row.ToString(), 4] = ukjobs1.Size;
        //            MySheet.Cells[findRng.Row.ToString(), 5] = ukjobs1.NoOfSides;
        //            MySheet.Cells[findRng.Row.ToString(), 6] = ukjobs1.NoOfColours;
        //            MySheet.Cells[findRng.Row.ToString(), 7] = ukjobs1.Fold;
        //            MySheet.Cells[findRng.Row.ToString(), 8] = ukjobs1.Category;
        //            MySheet.Cells[findRng.Row.ToString(), 9] = ukjobs1.AmendmentsA;
        //            MySheet.Cells[findRng.Row.ToString(), 10] = ukjobs1.AmendmentsB;
        //            MySheet.Cells[findRng.Row.ToString(), 11] = ukjobs1.AmendmentsC;
        //            MySheet.Cells[findRng.Row.ToString(), 12] = ukjobs1.PrintRun;
        //            MySheet.Cells[findRng.Row.ToString(), 13] = ukjobs1.DeliveryDate;
        //            MySheet.Cells[findRng.Row.ToString(), 14] = ukjobs1.DateSent;
        //            MySheet.Cells[findRng.Row.ToString(), 15] = ukjobs1.RPDone;
        //            MySheet.Cells[findRng.Row.ToString(), 16] = ukjobs1.Terrys;
        //            MySheet.Cells[findRng.Row.ToString(), 17] = ukjobs1.AdditionalInvoicesA;
        //            MySheet.Cells[findRng.Row.ToString(), 18] = ukjobs1.AdditionalInvoicesB;
        //            MySheet.Cells[findRng.Row.ToString(), 19] = ukjobs1.AdditionalInvoicesC;
        //            MySheet.Cells[findRng.Row.ToString(), 20] = ukjobs1.SP;


        //            //EmpList.Add(emp);
        //            MyBook.Save();

        //            MyBook.Close();
        //            break;
        //        case "IRJobSheet":
        //            MarketingIRJobsExcel irjobs1 = (MarketingIRJobsExcel)excelFields;

        //            MySheet.Cells[findRng.Row.ToString(), 1] = irjobs1.JobNo;
        //            MySheet.Cells[findRng.Row.ToString(), 2] = irjobs1.Group;
        //            MySheet.Cells[findRng.Row.ToString(), 3] = irjobs1.LeafletDescription;
        //            MySheet.Cells[findRng.Row.ToString(), 4] = irjobs1.Quantity;
        //            MySheet.Cells[findRng.Row.ToString(), 5] = irjobs1.DatePRReq;
        //            MySheet.Cells[findRng.Row.ToString(), 6] = irjobs1.DateSentToSPS;
        //            MySheet.Cells[findRng.Row.ToString(), 7] = irjobs1.EstimatedMailDate;
        //            MySheet.Cells[findRng.Row.ToString(), 8] = irjobs1.ANPostDate;
        //            MySheet.Cells[findRng.Row.ToString(), 9] = irjobs1.SleepersReceived;
        //            MySheet.Cells[findRng.Row.ToString(), 10] = irjobs1.InvoiceNumbersCategory;
        //            MySheet.Cells[findRng.Row.ToString(), 11] = irjobs1.InvoiceNumbersAmends;
        //            MySheet.Cells[findRng.Row.ToString(), 12] = irjobs1.InvoiceNumbersArtwork;
        //            MySheet.Cells[findRng.Row.ToString(), 13] = irjobs1.InvoiceNumbersPrint;
        //            MySheet.Cells[findRng.Row.ToString(), 14] = irjobs1.InvoiceNumbersMail;
        //            MySheet.Cells[findRng.Row.ToString(), 15] = irjobs1.MailingDestination;
        //            MySheet.Cells[findRng.Row.ToString(), 16] = irjobs1.Notes;

        //            //EmpList.Add(emp);
        //            MyBook.Save();

        //            MyBook.Close();
        //            break;
        //    }

        //You can use the following code to get the visible range of cells:
        //Excel.Range visibleRange = MyApp.ActiveWindow.VisibleRange;

        //int fullRow = MySheet.Rows.Count;
        //Excel.Range lastRow = MySheet.Cells[fullRow, column2Search].End(Excel.XlDirection.xlUp).Row;

        //MessageBox.Show(lastRow.Range);
        // Excel.Range range = sheet.get_Range("A1", last);


        // get the count of the rows
        // int fullRow = MySheet.Rows.Count;
        // get the last filled in value for that column
        //int lastRow = MySheet.Cells[fullRow, column2Search].End(Excel.XlDirection.xlUp).Row;

        //you can use the range if u need

        //range rng = new range;
        //int64 dblMax = 0; rng = Range("a1", Range("a65536").End(xlUp));
        //dblMax = Application.WorksheetFunction.Max(rng);
        // return dblMax + 1

        //}


        //private static void DemoFind()
        //{
        //    Excel.Range currentFind = null;
        //    Excel.Range firstFind = null;

        //    Excel.Range Fruits = MySheet.get_Range("A1", "B3");
        //    // You should specify all these parameters every time you call this method,
        //    // since they can be overridden in the user interface. 
        //    currentFind = Fruits.Find("apples", Missing.Value,
        //        Excel.XlFindLookIn.xlValues, Excel.XlLookAt.xlPart,
        //        Excel.XlSearchOrder.xlByRows, Excel.XlSearchDirection.xlNext, false,
        //        Missing.Value, Missing.Value);

        //    while (currentFind != null)
        //    {
        //        // Keep track of the first range you find. 
        //        if (firstFind == null)
        //        {
        //            firstFind = currentFind;
        //        }

        //        // If you didn't move to a new range, you are done.
        //        else if (currentFind.get_Address(Excel.XlReferenceStyle.xlA1)
        //              == firstFind.get_Address(Excel.XlReferenceStyle.xlA1))
        //        {
        //            break;
        //        }

        //        currentFind.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
        //        currentFind.Font.Bold = true;

        //        currentFind = Fruits.FindNext(currentFind);
        //    }
        //}





        //public static double ExcelGetLastColumnValue(string excelPath, int column2Search)
        //{

        //    // call example
        //    // StaticFunctions.ExcelGetLastColumnValue(@"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\media orders store\Media orders.xls", 1);


        //    MyApp = new Excel.Application();
        //    MyApp.Visible = false;
        //    MyBook = MyApp.Workbooks.Open(excelPath); // e.g @"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\media orders store\Media orders.xls"
        //    MySheet = (Excel.Worksheet)MyBook.Sheets[1];
        //    // Explicit cast is not required here
        //    // int lastRow = MySheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
        //    // firstColumn = MySheet.Cells.SpecialCells(Excel.XlCellType.).Column;

        //    //var cellValue = (string)(excelWorksheet.Cells[10, 2] as Excel.Range).Value;

        //    // get the count of the rows
        //    int fullRow = MySheet.Rows.Count;
        //    // get the last filled in value for that column
        //    int lastRow = MySheet.Cells[fullRow, column2Search].End(Excel.XlDirection.xlUp).Row;

        //    //Read the first cell
        //    // int range = MySheet.Cells[1, 1].Count();
        //    // MessageBox.Show(range.ToString());

        //    // get string value
        //    //string test = MySheet.Cells[lastRow, column2Search].Value2.ToString();

        //    //get numeric value, returns a double value
        //    double cellValue = MySheet.Cells[lastRow, column2Search].Value2;

        //    cellValue = cellValue + 1;

        //    MyBook.Close();

        //    //MessageBox.Show("Next Logical Number Would Be " + cellValue.ToString() + "  Full Row " + fullRow.ToString());


        //    return cellValue;
        //    //BindingList<Employee> EmpList = new BindingList<Employee>();
        //    // System.Array MyValues = (System.Array)MySheet.get_Range("A" + index.ToString(), "D" + index.ToString()).Cells.Value;

        //    //for (int index = 2; index <= lastRow; index++)
        //    //{
        //    //    System.Array MyValues = (System.Array)MySheet.get_Range("A" +
        //    //       index.ToString(), "D" + index.ToString()).Cells.Value;
        //    //    EmpList.Add(new Employee
        //    //    {
        //    //        Name = MyValues.GetValue(1, 1).ToString(),
        //    //        Employee_ID = MyValues.GetValue(1, 2).ToString(),
        //    //        Email_ID = MyValues.GetValue(1, 3).ToString(),
        //    //        Number = MyValues.GetValue(1, 4).ToString()
        //    //    });
        //    //}
        //}






        //Create document method
        //public static void CreateMediaOrderWordDocument(string publication, string association, string bookinginNo, string issue, string rpjobNo, string leafletSize, string proofCopy)
        //{
        //    try
        //    {
        //        //Create an instance for word app
        //        Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();


        //        //Set animation status for word application
        //        winword.ShowAnimation = false;

        //        //Set status for word application is to be visible or not.
        //        winword.Visible = false;

        //        //Create a missing variable for missing value
        //        object missing = System.Reflection.Missing.Value;

        //        //Create a new document
        //        Microsoft.Office.Interop.Word.Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);

        //        //Add header into the document
        //        foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
        //        {

        //            //Get the header range and add the header details.
        //            Microsoft.Office.Interop.Word.Range headerRange = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
        //            headerRange.Fields.Add(headerRange, Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage);
        //            headerRange.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
        //            headerRange.Font.Name = "Times New Roman";
        //            headerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdBlack;
        //            headerRange.Font.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineSingle;
        //            headerRange.Font.Bold = 1;
        //            headerRange.Font.Size = 14;
        //            headerRange.Text = "LEAFLET FOR APPROVAL AND DELIVERY INSTRUCTIONS";
        //        }

        //        //Add the footers into the document
        //        //foreach (Microsoft.Office.Interop.Word.Section wordSection in document.Sections)
        //        //{

        //        //    //Get the footer range and add the footer details.
        //        //    Microsoft.Office.Interop.Word.Range footerRange = wordSection.Footers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
        //        //    footerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdDarkRed;
        //        //    footerRange.Font.Size = 10;
        //        //    footerRange.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
        //        //    footerRange.Text = "";
        //        //}

        //        //adding text to document
        //        //document.Content.SetRange(0, 0);
        //        //document.Content.Text = "This is test document " + Environment.NewLine;

        //        //Add paragraph with Heading 1 style
        //        Microsoft.Office.Interop.Word.Paragraph para1 = document.Content.Paragraphs.Add(ref missing);
        //        object styleHeading1 = "Normal";
        //        //para1.Range.set_Style(ref styleHeading1);
        //        para1.Range.Font.Name = "Times New Roman";
        //        para1.Range.Font.Size = 12;
        //        para1.Range.Text = "PUBLICATION: " + publication;
        //        para1.Range.InsertParagraphAfter();

        //        //Add paragraph with Heading 2 style
        //        Microsoft.Office.Interop.Word.Paragraph para2 = document.Content.Paragraphs.Add(ref missing);
        //        object styleHeading2 = "Normal";
        //        //para2.Range.set_Style(ref styleHeading2);
        //        para2.Range.Font.Name = "Times New Roman";
        //        para2.Range.Font.Size = 12;
        //        para2.Range.Text = "ASSOCIATION: " + association + " (" + rpjobNo + ")";
        //        para2.Range.InsertParagraphAfter();

        //        //Add paragraph with Heading 3 style
        //        Microsoft.Office.Interop.Word.Paragraph para3 = document.Content.Paragraphs.Add(ref missing);
        //        object styleHeading3 = "Normal";
        //        //para3.Range.set_Style(ref styleHeading2);
        //        para3.Range.Font.Name = "Times New Roman";
        //        para3.Range.Font.Size = 12;
        //        para3.Range.Text = "BOOKING IN NUMBER: (Please use this number when invoicing) " + bookinginNo;
        //        para3.Range.InsertParagraphAfter();

        //        //Add paragraph with Heading 4 style
        //        Microsoft.Office.Interop.Word.Paragraph para4 = document.Content.Paragraphs.Add(ref missing);
        //        object styleHeading4 = "Normal";
        //        //para4.Range.set_Style(ref styleHeading2);
        //        para4.Range.Font.Name = "Times New Roman";
        //        para4.Range.Font.Size = 12;
        //        para4.Range.Text = "ISSUE: " + issue;
        //        para4.Range.InsertParagraphAfter();

        //        //Add paragraph with Heading 4 style
        //        Microsoft.Office.Interop.Word.Paragraph para5 = document.Content.Paragraphs.Add(ref missing);
        //        object styleHeading5 = "Normal";
        //        //para5.Range.set_Style(ref styleHeading2);
        //        para5.Range.Font.Name = "Times New Roman";
        //        para5.Range.Font.Size = 12;
        //        para5.Range.Font.Color = Microsoft.Office.Interop.Word.WdColor.wdColorRed;
        //        para5.Range.Text = "PLEASE NOTE THE SIZE OF THE LEAFLET HAS BEEN FILLED IN, IF THIS IS INCORRECT PLEASE CHANGE ACCORDINGLY.";
        //        para5.Range.InsertParagraphAfter();


        //        //Create a 2X7 table and insert some dummy record
        //        //Table firstTable = document.Tables.Add(para1.Range, 5, 5, ref missing, ref missing);
        //        Word.Table oTable = document.Tables.Add(para1.Range, 7, 2, ref missing, ref missing);
        //        //firstTable.RowGroups
        //        //firstTable.Borders.Enable = 1;
        //        oTable.Borders.Enable = 1;

        //        foreach (Word.Row row in oTable.Rows)
        //        {





        //            foreach (Word.Cell cell in row.Cells)
        //            {

        //                cell.Range.Font.Bold = 0;
        //                //other format properties goes here
        //                cell.Range.Font.Name = "Times New Roman";
        //                cell.Range.Font.Size = 12;
        //                //cell.Range.Font.ColorIndex = WdColorIndex.wdGray25;                            
        //                cell.Shading.BackgroundPatternColor = Word.WdColor.wdColorWhite;
        //                //Center alignment for the Header cells
        //                cell.VerticalAlignment = Word.WdCellVerticalAlignment.wdCellAlignVerticalTop;
        //                cell.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;


        //                //Header row
        //                if (cell.RowIndex == 1 && cell.ColumnIndex == 2)
        //                {
        //                    cell.Range.Text = "AGENCY/GROUP TO COMPLETE ALL SECTIONS BELOW";

        //                }
        //                if (cell.RowIndex == 2 && cell.ColumnIndex == 1)
        //                {
        //                    cell.Row.Height = 80;
        //                    cell.Range.Text = "APPROVAL OF LEAFLET \r\n (Type Yes or No)";
        //                }
        //                if (cell.RowIndex == 3 && cell.ColumnIndex == 1)
        //                {
        //                    cell.Row.Height = 50;
        //                    cell.Range.Text = "SIZE OF LEAFLET";
        //                }
        //                if (cell.RowIndex == 3 && cell.ColumnIndex == 2)
        //                {
        //                    cell.Range.Text = leafletSize;
        //                }
        //                if (cell.RowIndex == 4 && cell.ColumnIndex == 1)
        //                {
        //                    cell.Row.Height = 50;
        //                    cell.Range.Text = "QUANTITY OF LEAFLETS";
        //                }
        //                if (cell.RowIndex == 5 && cell.ColumnIndex == 1)
        //                {
        //                    cell.Row.Height = 100;
        //                    cell.Range.Text = "DATE LEAFLETS REQUIRED";
        //                }
        //                if (cell.RowIndex == 6 && cell.ColumnIndex == 1)
        //                {
        //                    cell.Row.Height = 40;
        //                    cell.Range.Text = "MAILING DATE";
        //                }
        //                if (cell.RowIndex == 7 && cell.ColumnIndex == 1)
        //                {
        //                    cell.Row.Height = 160;
        //                    cell.Range.Text = "DELIVERY ADDRESS";
        //                }
        //                //Data row
        //                else
        //                {
        //                    // cell.Range.Text = (cell.RowIndex - 2 + cell.ColumnIndex).ToString();
        //                }
        //            }
        //        }

        //        //Save the document
        //        // MessageBox.Show(@"" + myDocs + @"mediaorder.docx");
        //        //DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");
        //        //object filename = @"" + myDocs + @"\" + association + @" Media Order " + DateTime.Now.ToString("dd-M-yyyy-HH-mm-ss") + @".docx";
        //        //if (File.Exists(@"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\media orders store\kathleen media orders\media orders word docs\" + bookinginNo + @" " + association + @" " + DateTime.Now.ToString("dd-M-yyyy") + @".docx"))
        //        //{
        //        //    File.Delete(@"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\media orders store\kathleen media orders\media orders word docs\" + bookinginNo + @" " + association + @" " + DateTime.Now.ToString("dd-M-yyyy") + @".docx");
        //        //}


        //        //object filename = @"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\media orders store\kathleen media orders\media orders word docs\" + bookinginNo + @" " + association + @" " + DateTime.Now.ToString("dd-M-yyyy") + @".docx";






        //        //  SAVE MEDIA ORDER TO A SPECIFIED LOCATION

        //        //clear file if it already exists
        //        if (File.Exists(@"" + myDocs + @"\Workflow-User\Docs\" + bookinginNo + @" " + association + @".docx"))
        //        {
        //            File.Delete(@"" + myDocs + @"\Workflow-User\Docs\" + bookinginNo + @" " + association + @".docx");
        //        }


        //        object filename = @"" + myDocs + @"\Workflow-User\Docs\" + bookinginNo + @" " + association + @".docx";
        //        document.SaveAs2(ref filename);
        //        document.Close(ref missing, ref missing, ref missing);
        //        document = null;
        //        winword.Quit(ref missing, ref missing, ref missing);
        //        winword = null;
        //        // END SAVE MEDIA ORDER TO A SPECIFIED LOCATION


        //        //MessageBox.Show(@"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\marketing\design\proofs\" + rpjobNo + @".pdf");


        //        // create an array to hold attachments, then add the attachments and send to the create a new emal static function
        //        //string[] attachmentList = new string[2];
        //        //attachmentList[0] = @"" + filename + @"";
        //        //attachmentList[1] = proofCopy;

        //        List<String> attachmentList = new List<String>();
        //        attachmentList.Add(@"" + filename + @"");
        //        attachmentList.Add(proofCopy);
        //        // REMOVE THE FILE
        //        //if (File.Exists(@"" + myDocs + @"\Workflow-User\Docs\" + bookinginNo + @" " + association + @".docx"))
        //        //{
        //        //    File.Delete(@"" + myDocs + @"\Workflow-User\Docs\" + bookinginNo + @" " + association + @".docx");
        //        //}



        //        // create the email with the generated word document, this is sent to the group for filling out by terry
        //        StaticFunctions.CreateMailItem(attachmentList, "", "terry@hmca.co.uk", "Media Order " + bookinginNo + @" " + association, "");

        //        //once attached to  email, clear the document
        //        if (File.Exists(@"" + myDocs + @"\Workflow-User\Docs\" + bookinginNo + @" " + association + @".docx"))
        //        {
        //            File.Delete(@"" + myDocs + @"\Workflow-User\Docs\" + bookinginNo + @" " + association + @".docx");
        //        }







        //        //MessageBox.Show("Document created successfully !");
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //}
        // EXCEL


        // END OFFICE INTEROP

        //public static void sendToGroup(string pdfproofPath,string originalPath)
        //{
        //    // declare ne string array
        //    string[] attachmentList = new string[1];
        //    // call function to apply rogh proof overlay, which returns the path of the new pdf created
        //    attachmentList[0] = readerXPSFunctions.xps2pdfProofCopy(pdfproofPath, originalPath);



        //    // attach items in email attach to a new email
        //   // StaticFunctions.CreateMailItem(attachmentList, "");

        //}







        //public static void Send2ApprovalIcob(string pdfPath, string approvalPath, string approvalPathOnly, string approvalName, string newfilePath, string JobNumber, bool isIcob)
        //{
        //    //readerXPSFunctions.printPDFTIFF2XPS
        //    //readerXPSFunctions.MergeXpsDocumentInsert
        //    if (Directory.Exists(approvalPathOnly))
        //    {
        //        if (approvalName != null)
        //        {
        //            // uk jobs must all be icobbed
        //            //docED1.docuMents.printPDFTIFF2XPS(@"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\marketing\design\proofs\" + FileSelected + @".pdf", @"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\marketing\design\" + FileSelected + @".xps", "pdf",false);
        //            readerXPSFunctions.printPDFTIFF2XPS(pdfPath, approvalPath, "pdf", false);

        //            // check and wait if file is locked
        //            do
        //            {

        //            } while (StaticFunctions.IsLocked(new FileInfo(approvalPath)));

        //            // MessageBox.Show(StaticFunctions.UncPathToUse + StaticFunctions.ActiveJobSelectedPath + StaticFunctions.ActiveJobSelectedLBox);
        //            // merge job xps with proof form  proof form, job, newfile
        //            readerXPSFunctions.MergeXpsDocumentInsertExternal(approvalPath, StaticFunctions.UncPathToUse + StaticFunctions.ActiveJobSelectedPath + StaticFunctions.ActiveJobSelectedLBox, newfilePath, 0);

        //            // check and wait if file is locked
        //            do
        //            {

        //            } while (StaticFunctions.IsLocked(new FileInfo(newfilePath)));

        //            // if new file is produced, then send to the selected person for approval
        //            if (File.Exists(newfilePath))
        //            {
        //                // if file already there delete before copying
        //                if (File.Exists(approvalPath))
        //                {
        //                    File.Delete(approvalPath);
        //                }
        //                File.Move(newfilePath, approvalPath);

        //                // Check if Icob Required, and send if true
        //                if (isIcob == true)
        //                {
        //                    // if file already there delete before copying
        //                    if (File.Exists(@"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\marketing\icob\" + JobNumber + @".xps"))
        //                    {
        //                        File.Delete(@"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\marketing\icob\" + JobNumber + @".xps");
        //                    }// end if

        //                    // make a copy of the approval file and send for icobbing
        //                    File.Copy(approvalPath, @"" + StaticFunctions.UncPathToUse + @"workflow\hmcaworkflow\marketing\icob\" + JobNumber + @".xps");

        //                }// end if



        //            }//end if





        //        }
        //    }
        //    else
        //    {
        //        ModernDialog.ShowMessage("No approval box is available to that user.", "Error! No Path.", MessageBoxButton.OK);
        //        return;
        //    } //end if
        //}// end send for approval method


        // this works for sorting an observable collection except it deselects the item selected in the observable collection
        public static void SortObservable<T>(this ObservableCollection<T> observable) where T : IComparable<T>, IEquatable<T>
        {

            // add observable to a list of T
            List<T> sorted = observable.OrderBy(x => x).ToList();

            int ptr = 0;
            // loop through the list
            while (ptr < sorted.Count)
            {
                // if not the observable collection equals to the sorted list
                if (!observable[ptr].Equals(sorted[ptr]))
                {
                    // asign the observable item at specified index 
                    T t = observable[ptr];
                    observable.RemoveAt(ptr);
                    observable.Insert(sorted.IndexOf(t), t);
                }
                else
                {
                    // move to the next one
                    ptr++;
                }
            }
        }



        // these are extension methods for an observable collection to provide custom sorting using generics
        public static void Sort<T>(this ObservableCollection<T> collection, Comparison<T> comparison)
        {
            var sortableList = new List<T>(collection);
            sortableList.Sort(comparison);

            for (int i = 0; i < sortableList.Count; i++)
            {
                collection.Move(collection.IndexOf(sortableList[i]), i);
            }
        }




        //COMPARISON DEFINITIONS
        public static int CompareLength(string a, string b)
        {
            // Return result of CompareTo with lengths of both strings.
            return a.Length.CompareTo(b.Length);
        }





















        //***** FUNCTION TO POPULATE AN OBSERVABLE COLLECTION BASED ON FILE PATH
        public static ObservableCollection<string> GetFilesAction(ObservableCollection<String> obsCprop, String directoryPath, string sFilter, string Lboxfrom, string currentSelected)
        {


            // for listboxes with a lot of items purely for viewing, use this method. 
            if (Lboxfrom == "accAnnualLB" || Lboxfrom == "accAnnualServicesLB")
            {
                obsCprop.Clear();

            }
            else
            {
                // use this method for live listbox updates, removing from the 
                //// remove file function
                try
                {
                    foreach (string fileinObs in obsCprop)
                    {
                        if (File.Exists(directoryPath + @"" + fileinObs))
                        {

                        }
                        else
                        {
                            obsCprop.Remove(fileinObs);
                            //StaticFunctions.SortObservable(obsCprop);
                            //Console.WriteLine(fileinObs);
                        }
                    }// end loop
                }
                catch (Exception)
                {

                    // throw;
                }


            }



            // OPTION1
            // string[] files = Directory.GetFiles(directoryPath, sFilter, searchOption);
            DirectoryInfo dinfo = new DirectoryInfo(directoryPath);
            FileInfo[] files = dinfo.GetFiles(sFilter);
            //FileInfo[] files = DirectoryInfo.GetFiles(;
            for (int i = 0; i < files.Length; i++)
            {

                if (obsCprop.Contains(files[i].Name))
                {
                    //do nothing

                }
                else
                {

                    // Set this to true to indicate an item was addedd
                    //StaticFunctions.fromRefresh = true;
                    obsCprop.Add(files[i].Name);
                    //obsCprop.Insert(1,file.Name);
                    //StaticFunctions.SortObservable(obsCprop);
                }



            }





            if (StaticFunctions.ActiveJobControlNameLBox == "")
            {
                StaticFunctions.SortObservable(obsCprop);
            }
            //if the listbox sending in the request equals the active listbox, then run the select index
            else if (StaticFunctions.ActiveJobControlNameLBox == Lboxfrom)
            {

                ListBox LboxRef = (ListBox)Helper.GetDescendantFromName(System.Windows.Application.Current.MainWindow, StaticFunctions.ActiveJobControlNameLBox);
                //ListBox LboxRef = (ListBox)Helper.GetDescendantFromName(System.Windows.Application.Current.MainWindow, "ukmjobsinboxLB");

                //int index = LboxRef.SelectedItem.("60002.xps");
                int itemIndex = obsCprop.IndexOf(currentSelected);

                //MessageBox.Show(itemIndex.ToString());
                //int itemIndex = obsCprop.IndexOf("60002.xps");

                // this is used to auto select the previous item it is disabled because of the issues it can cause, if an item is not selected then skip the item select
                if (itemIndex != -1)
                {
                    // reselect the item
                    //**if (LboxRef != null)
                    //**    LboxRef.SelectedIndex = itemIndex;

                }


                //LboxRef.SelectedItem = "60002.xps";
                //LboxRef.SelectedIndex = 3;

                //LboxRef.SelectedItem = StaticFunctions.ActiveJobSelectedLBox;
            }







            return obsCprop;
        }




        ////***** FUNCTION TO POPULATE AN OBSERVABLE COLLECTION BASED ON FILE PATH
        public static ObservableCollection<String> GetFilesAction2(ObservableCollection<String> obsCprop, String directoryPath, string sFilter, System.IO.SearchOption searchOption, string Lboxfrom, string currentSelected)
        {

            // for listboxes with a lot of items purely for viewing, use this method. 
            if (Lboxfrom == "accAnnualLB" || Lboxfrom == "accAnnualServicesLB")
            {
                obsCprop.Clear();

            }
            else
            {
                // use this method for live listbox updates, removing from the 
                //// remove file function
                try
                {
                    foreach (string fileinObs in obsCprop)
                    {
                        if (File.Exists(directoryPath + @"" + fileinObs))
                        {

                        }
                        else
                        {
                            obsCprop.Remove(fileinObs);
                            //StaticFunctions.SortObservable(obsCprop);
                            //Console.WriteLine(fileinObs);
                        }
                    }// end loop
                }
                catch (Exception)
                {

                    // throw;
                }


            }



            // OPTION1
            // string[] files = Directory.GetFiles(directoryPath, sFilter, searchOption);
            DirectoryInfo dinfo = new DirectoryInfo(directoryPath);
            FileInfo[] files = dinfo.GetFiles(sFilter);
            //FileInfo[] files = DirectoryInfo.GetFiles(;
            for (int i = 0; i < files.Length; i++)
            {

                if (obsCprop.Contains(files[i].Name))
                {
                    //do nothing

                }
                else
                {

                    // Set this to true to indicate an item was addedd
                    //StaticFunctions.fromRefresh = true;
                    obsCprop.Add(files[i].Name);
                    //obsCprop.Insert(1,file.Name);
                    //StaticFunctions.SortObservable(obsCprop);
                }



            }





            if (StaticFunctions.ActiveJobControlNameLBox == "")
            {
                StaticFunctions.SortObservable(obsCprop);
            }
            //if the listbox sending in the request equals the active listbox, then run the select index
            else if (StaticFunctions.ActiveJobControlNameLBox == Lboxfrom)
            {
                ListBox LboxRef = (ListBox)Helper.GetDescendantFromName(System.Windows.Application.Current.MainWindow, StaticFunctions.ActiveJobControlNameLBox);
                //ListBox LboxRef = (ListBox)Helper.GetDescendantFromName(System.Windows.Application.Current.MainWindow, "ukmjobsinboxLB");

                //int index = LboxRef.SelectedItem.("60002.xps");
                int itemIndex = obsCprop.IndexOf(currentSelected);

                //MessageBox.Show(itemIndex.ToString());
                //int itemIndex = obsCprop.IndexOf("60002.xps");

                // this is used to auto select the previous item it is disabled because of the issues it can cause, if an item is not selected then skip the item select
                if (itemIndex != -1)
                {
                    // reselect the item
                    //**if (LboxRef != null)
                    //**    LboxRef.SelectedIndex = itemIndex;

                }


                //LboxRef.SelectedItem = "60002.xps";
                //LboxRef.SelectedIndex = 3;

                //LboxRef.SelectedItem = StaticFunctions.ActiveJobSelectedLBox;
            }







            return obsCprop;

        }


        //public static Task<ObservableCollection<string>> IndexFiles(ObservableCollection<String> obsCprop, String directoryPath, String sFilter)
        //{
        //    DirectoryInfo dinfo = new DirectoryInfo(directoryPath);
        //    FileInfo[] Files = dinfo.GetFiles(sFilter);

        //    try
        //    {



        //        foreach (FileInfo file in dinfo.EnumerateFiles())
        //        {
        //            //    foreach (FileInfo file in Files)
        //            //{
        //            //listbox1.Items.Add(file.Name);
        //            if (obsCprop.Contains(file.Name))
        //            {
        //                //MessageBox.Show("Yes");

        //            }
        //            else
        //            {

        //                // Set this to true to indicate an item was addedd
        //                //StaticFunctions.fromRefresh = true;
        //                obsCprop.Add(file.Name);
        //                //obsCprop.Insert(1,file.Name);
        //                //StaticFunctions.SortObservable(obsCprop);
        //            }


        //            // Order 
        //            //obsCprop.OrderByDescending
        //            //innerObs.Add(file.Name);
        //        }// end loop
        //    }
        //    catch (Exception)
        //    {

        //        // throw;
        //    }

        //    // remove file function
        //    try
        //    {
        //        foreach (string fileinObs in obsCprop)
        //        {
        //            if (File.Exists(directoryPath + @"\" + fileinObs))
        //            {

        //            }
        //            else
        //            {
        //                obsCprop.Remove(fileinObs);
        //                //StaticFunctions.SortObservable(obsCprop);
        //            }
        //        }// end loop
        //    }
        //    catch (Exception)
        //    {

        //        // throw;
        //    }


        //    return obsCprop;
        //}







        // get directory names of folders
        public static List<string> GetDirectoryListing(List<string> listprop, String directoryPath, string sFilter)
        {


            //marketingDocpath = directoryPath;


            DirectoryInfo dinfo = new DirectoryInfo(directoryPath);
            //FileInfo[] Files = dinfo.GetFiles(sFilter);
            DirectoryInfo[] Dirs = dinfo.GetDirectories();
            //Filescollection.Add(new String { Name = "index.html", Path = "C:\\Files" });

            //string folder = @"C:\Users\Ece\Documents\Testings";
            //string[] txtfiles = Directory.GetFiles(folder, "*.txt");

            foreach (DirectoryInfo dir in Dirs)
            {
                //listbox1.Items.Add(file.Name);
                //if (dir.Name != "admin")
                //    listprop.Add(dir.Name);
                //MessageBox.Show(dir.Name);
            }

            return listprop;


        }

        public static string[] FindAFile(string baseDirectory, string FileAndExt)
        {
            //var filePath = (from file in Directory.GetFiles(baseDirectory, FileAndExt, SearchOption.AllDirectories) select file).ToString();
            string[] filePath = Directory.GetFiles(baseDirectory, FileAndExt, System.IO.SearchOption.AllDirectories);
            //if(singleFile == true)
            //{
            //    return filePath.FirstOrDefault();
            //}
            //else if
            //{

            //}
            return filePath;
        }

        //Search at a given path and return only the filenames with no etensions
        public static ObservableCollection<string> GetFileNamesOnly(string baseDirectory, string FileAndExt)
        {
            //var filePath = (from file in Directory.GetFiles(baseDirectory, FileAndExt, SearchOption.AllDirectories) select file).ToString();
            string[] filePath = Directory.GetFiles(baseDirectory, FileAndExt, System.IO.SearchOption.AllDirectories);

            ObservableCollection<string> fileNames = new ObservableCollection<string>();

            //string[] files = new DirectoryInfo(baseDirectory).GetFiles().Select(o => o.Name).ToArray();

            //Loop through and add to a new collection
            foreach (string file in filePath)
            {
                //MessageBox.Show(SelectedBrand);
                //MessageBox.Show(file);
                //only add the selected product brand items

                //add the item
                fileNames.Add(Path.GetFileNameWithoutExtension(file));


            }

            //Use Path.GetFileName to get extensions 
            //Console.WriteLine(Path.GetFileName(file));

            return fileNames;
        }


        //Search at a given path and return only the filenames with no extensions based on a selected value
        public static ObservableCollection<string> GetFileNamesOnlyBasedOnValue(string baseDirectory, string FileAndExt, string ValueToCheck)
        {

            try
            {

          
    //var filePath = (from file in Directory.GetFiles(baseDirectory, FileAndExt, SearchOption.AllDirectories) select file).ToString();
    string[] filePath = Directory.GetFiles(baseDirectory, FileAndExt, System.IO.SearchOption.AllDirectories);

            ObservableCollection<string> fileNames =  new ObservableCollection<string>();

            //string[] files = new DirectoryInfo(baseDirectory).GetFiles().Select(o => o.Name).ToArray();

            //Loop through and add to a new collection
            foreach (string file in filePath)
            {
                //MessageBox.Show(SelectedBrand);
                //MessageBox.Show(file);
                //only add the selected product brand items
                if (file.Contains(ValueToCheck))
                {
                    //add the item
                    fileNames.Add(Path.GetFileNameWithoutExtension(file));
                }
               
            }

                return fileNames;
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                return new ObservableCollection<string>() ;
            }

          
        }



        ////Search at a given path and return only the filenames with no extensions based on a selected value
        //public static ServerB GetFileNamesOnlyBasedOnValue(string baseDirectory, string FileAndExt, string ValueToCheck)
        //{
        //    //var filePath = (from file in Directory.GetFiles(baseDirectory, FileAndExt, SearchOption.AllDirectories) select file).ToString();
        //    string[] filePath = Directory.GetFiles(baseDirectory, FileAndExt, SearchOption.AllDirectories);

        //    ObservableCollection<string> fileNames = new ObservableCollection<string>();

        //    //string[] files = new DirectoryInfo(baseDirectory).GetFiles().Select(o => o.Name).ToArray();

        //    //Loop through and add to a new collection
        //    foreach (string file in filePath)
        //    {
        //        //MessageBox.Show(SelectedBrand);
        //        //MessageBox.Show(file);
        //        //only add the selected product brand items
        //        if (file.Contains(ValueToCheck))
        //        {
        //            //add the item
        //            fileNames.Add(Path.GetFileNameWithoutExtension(file));
        //        }

        //    }

        //    return fileNames;
        //}




        //***** count of xps jobs in a particular folder
        public static int fileCounter(string path, string searchpattern)
        {
            var fileCount = (from file in Directory.EnumerateFiles(path, searchpattern, System.IO.SearchOption.TopDirectoryOnly)
                             select file).Count();

            return fileCount;
        }



        //******** MODERN DIALOG BOXES **********

        public static void DialogYesNo(string msg, string title, bool showCancel)
        {

            //ModernDialog.ShowMessage("Are you sure you want to delete this proof?", "Delete Proof", MessageBoxButton.YesNo);
            //var dlg = new ModernDialog
            //{

            //    Title = title,
            //    Content = msg
            //};
            //MessageBox.Show("Yes came here");

            UserInfoDialog dlg = new UserInfoDialog
            {

               // Title = title,

                //Content = msg
            };
            dlg.txtDescription.Text = msg;
            if (showCancel != true)
                dlg.CancelBut.Visibility = Visibility.Collapsed;
            //dlg.ContentTemplate = (DataTemplate)Application.Current.FindResource("Blah");
            //dlg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            ////dlg.Buttons = new Button[] { dlg.YesButton, dlg.NoButton };
            //dlg.ShowDialog();

            //var dbox = dlg.DialogResult.HasValue ? dlg.DialogResult.ToString() : "<null>";

            //var res = dlg.MessageBoxResult.ToString();


            //MessageBox.Show(dbox.ToString());
            //MessageBox.Show(res.ToString());
            //return dbox;
        }


        //public static String DialogSelectUser(string msg, string title)
        //{

        //    //ModernDialog.ShowMessage("Are you sure you want to delete this proof?", "Delete Proof", MessageBoxButton.YesNo);
        //    UserSelectDialog dlg = new UserSelectDialog
        //    {

        //        Title = title

        //        //Content = new Button()
        //    };
        //    dlg.ContentTemplate = (DataTemplate)Application.Current.FindResource("Blah");
        //    //dlg.
        //    //dlg.Content = Application.Current.FindResource("UKRPFORM");
        //    //dlg.DataContext = this;
        //    dlg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
        //    //dlg.Buttons = new Button[] { dlg.YesButton, dlg.NoButton };
        //    dlg.ShowDialog();

        //    var dbox = dlg.DialogResult.HasValue ? dlg.DialogResult.ToString() : "<null>";

        //    //var res = dlg.MessageBoxResult.ToString();


        //    //MessageBox.Show(dbox.ToString());
        //    //MessageBox.Show(res.ToString());
        //    return dbox;
        //}



        //****** File Copy\Move Function
        public async static void moveFile(string inpath, string outpath)
        {
            try
            {
                await System.Windows.Threading.Dispatcher.Yield();

                if (File.Exists(inpath))
                {
                    if (File.Exists(outpath))
                    {
                        File.Delete(outpath);
                        File.Move(inpath, outpath);
                    }
                    else
                    {
                        File.Move(inpath, outpath);
                    }


                }

                //Loop until file is moved correctly
                do
                {

                } while (File.Exists(outpath) != true);


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }

        }

        // copy file
        public static void copyFile(string inpath, string outpath)
        {
            try
            {


                if (File.Exists(inpath))
                {
                    if (File.Exists(outpath))
                    {
                        File.Delete(outpath);
                        File.Copy(inpath, outpath);
                    }
                    else
                    {
                        File.Copy(inpath, outpath);
                    }



                }

                //Loop until file is moved correctly
                do
                {

                } while (File.Exists(outpath) != true);


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }

        }


        // delete file
        public static void deleteFile(string inpath)
        {
            try
            {


                if (File.Exists(inpath))
                {
                    File.Delete(inpath);
                }

                //Loop until file is moved correctly
                do
                {

                } while (File.Exists(inpath) != false);


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }

        }

        //***** Directory Creation Function
        public static void createDir(string dir2Create)
        {

            try
            {


                if (Directory.Exists(dir2Create) != true)
                {
                    Directory.CreateDirectory(dir2Create);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }

        }

        // USE THIS TO COPY AN OBJECT AND RETURN THE COPYS XAML
        public static T Clone<T>(T source)
        {
            string objXaml = XamlWriter.Save(source);
            StringReader stringReader = new StringReader(objXaml);
            XmlReader xmlReader = XmlReader.Create(stringReader);
            T t = (T)XamlReader.Load(xmlReader);

            //FileStream fs = File.Create(@"C:\Users\hmcankms\Pictures\testfile.xaml");

            //StreamWriter sw = new StreamWriter(fs);

            //sw.Write(objXaml);

            //sw.Close();

            //fs.Close();



            return t;
        }

        public static void ConvertToBitmapSource(UIElement element, String outputPath)
        {
            var target = new RenderTargetBitmap(
                (int)element.RenderSize.Width, (int)element.RenderSize.Height,
                96, 96, PixelFormats.Pbgra32);
            target.Render(element);

            var encoder = new PngBitmapEncoder();
            var outputFrame = BitmapFrame.Create(target);
            encoder.Frames.Add(outputFrame);

            using (var file = File.OpenWrite(outputPath))
            {
                encoder.Save(file);
            }
        }


        public static FixedDocument Form2FixedDocument(Grid myform)
        {


            Transform transform = myform.LayoutTransform;
            // Temporarily reset the layout transform before saving
            myform.LayoutTransform = null;

            FixedDocument fixedDoc = new FixedDocument();
            PageContent pageContent = new PageContent();
            FixedPage fixedPage = new FixedPage();
            //pt.PageMediaSize = new PageMediaSize(v.Width, v.Height);
            //pt.PageBorderless = null;
            //pt.PageOrientation = PageOrientation.Portrait;
            //Size size = new Size((Double)myform.Width, (Double)myform.Height);
            //Size size = new Size((Double)793, (Double)1122);
            VisualBrush myvb = new VisualBrush();
            myvb.Visual = myform;
            myvb.Stretch = Stretch.Uniform;
            //Canvas canvinner = new Canvas();
            Size Newsize = new Size((Double)794, (Double)1123);
            //Size Newsize = new Size((Double)793, (Double)1122);
            Grid mycanv = new Grid();
            //mycanv.VerticalAlignment = VerticalAlignment.Stretch;
            //mycanv.HorizontalAlignment = HorizontalAlignment.Stretch;
            //mycanv.Width = 793;
            //mycanv.Height = 1122;
            mycanv.Margin = new Thickness(0, -180, 0, 0);
            mycanv.Width = Newsize.Width;
            mycanv.Height = Newsize.Height;
            //mycanv.Width = myform.Width;
            //mycanv.Height = myform.Height;
            mycanv.Background = myvb;
            //canvinner.Background = myvb;
            mycanv.Measure(Newsize);
            mycanv.Arrange(new Rect(new Point(0, 0), Newsize));
            //mycanv.Measure(size);
            //mycanv.Arrange(new Rect(size));
            //canvinner.UpdateLayout();
            //mycanv.Measure(size);
            //mycanv.Arrange(new Rect(size));
            // make a copy of the canvas

            //  mycanv = StaticFunctions.Clone<Canvas>(myform);



            //  StaticFunctions.ConvertToBitmapSource(myform, @"C:\Users\hmcankms\Pictures\nicktest.png");

            //TextBlock tx = new TextBlock();
            //tx.Width = 100;
            //tx.Height = 40;
            //tx.Text = "Nick Was Here";
            //tx.Background = Brushes.Crimson;

            // mycanv.Children.Add(tx);
            //mycanv.Children.Add(canvinner);

            fixedPage.Width = Newsize.Width;
            fixedPage.Height = Newsize.Height;
            //Create first page of document
            fixedPage.Children.Add(mycanv);
            fixedPage.Measure(Newsize);
            fixedPage.Arrange(new Rect(new Point(), Newsize));
            fixedPage.UpdateLayout();
            ((System.Windows.Markup.IAddChild)pageContent).AddChild(fixedPage);
            fixedDoc.Pages.Add(pageContent);

            return fixedDoc;

        }


        // Used to create a fixed document to pass through to the virtual xps printer
        public static FixedDocument SaveCurrentViewToXPS(UIElement theView, Size pageSize)
        {

            //https://istacee.wordpress.com/2013/03/19/save-wpf-control-as-xps-and-fit-it-into-an-a4-papersize-page/

            // Initialize the xps document structure
            FixedDocument fixedDoc = new FixedDocument();
            PageContent pageContent = new PageContent();
            FixedPage fixedPage = new FixedPage();

            ////var view = (MarketingIRJobsInbox)theType;
            //var view = (MarketingRP)theType;
            ////var view = type;
            //view.DataContext = dContext;
            //view.UpdateLayout();
            ////}
            ////VisualBrush vb = new VisualBrush(view);
            ////vb.Stretch = Stretch.Uniform;



            ////MarketingIRJobsInbox view = new MarketingIRJobsInbox();

            ////MessageBox.Show(view.Width + " x " + view.Height);

            //// Get the page size of an A4 document
            ////var pageSize = new Size(8.27 * 96.0, 11.69 * 96.0);

            //// We just fit it horizontally, so only the width is set here
            ////view.Height = pageSize.Height;
            ////Canvas canv = new Canvas();
            ////canv.Width = pageSize.Width;
            ////canv.Height = pageSize.Height;
            ////canv.Background = vb;
            ////canv.UpdateLayout();
            //view.Height = pageSize.Height;
            //view.Width = pageSize.Width;
            ////view.Measure(pageSize);
            ////view.Arrange(new Rect(new Point(), pageSize));
            //view.UpdateLayout();
            ////view.UpdateLayout();




            //Create first page of document
            fixedPage.Children.Add(theView);
            //fixedPage.Children.Add(view);
            fixedPage.Measure(pageSize);
            fixedPage.Arrange(new Rect(new Point(), pageSize));
            ((System.Windows.Markup.IAddChild)pageContent).AddChild(fixedPage);
            fixedDoc.Pages.Add(pageContent);







            // Create the xps file and write it

            //XpsDocument xpsd = new XpsDocument(filename, FileAccess.ReadWrite);
            //XpsDocumentWriter xw = XpsDocument.CreateXpsDocumentWriter(xpsd);
            //xw.Write(fixedDoc);
            //xpsd.Close();
            //}
            return fixedDoc;
        }





        public static void SaveCurrentViewToXPSOriginal(MainViewModel dContext, object theview)
        {
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = String.Format("MyFile{0:ddMMyyyyHHmmss}", DateTime.Now); // Default file name
            dlg.DefaultExt = ".xps"; // Default file extension
            dlg.Filter = "XPS Documents (.xps)|*.xps"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;

                // Initialize the xps document structure
                FixedDocument fixedDoc = new FixedDocument();
                PageContent pageContent = new PageContent();
                FixedPage fixedPage = new FixedPage();

                // Create the document and set the datacontext
                var view = (TestReportFlowDocument)theview;
                view.DataContext = dContext;
                view.UpdateLayout();

                // Get the page size of an A4 document
                var pageSize = new Size(8.5 * 96.0, 11.0 * 96.0);

                // We just fit it horizontally, so only the width is set here
                //view.Height = pageSize.Height;
                view.Width = pageSize.Width;
                view.UpdateLayout();

                //Create first page of document
                fixedPage.Children.Add(view);
                ((System.Windows.Markup.IAddChild)pageContent).AddChild(fixedPage);
                fixedDoc.Pages.Add(pageContent);

                // Create the xps file and write it
                XpsDocument xpsd = new XpsDocument(filename, FileAccess.ReadWrite);
                XpsDocumentWriter xw = XpsDocument.CreateXpsDocumentWriter(xpsd);
                xw.Write(fixedDoc);
                xpsd.Close();
            }
        }

        //USED TO NAVIGATE A VIEW TO A TARGETED FRAME

        //public static void modernUIframeNavigate(string targetFrame, FrameworkElement targetPage)
        //{
        //    // Navigates to a specific page as long as it is visible  IE Accounts,  Marketing etc In the main window constructor, add this   Application.Current.MainWindow = this;
        //    // HOW TO CALL
        //    // StaticFunctions.modernUIframeNavigate("/Pages/Accounts/AccountsServices.xaml",Application.Current.MainWindow);

        //    IInputElement target = NavigationHelper.FindFrame("_top", targetPage);
        //    NavigationCommands.GoToPage.Execute(targetFrame, target);


        //}



        //public static void modernframeNavigate(string targetFrame, string targetPage)
        //{
        //    var frame = Helper.GetDescendantFromName(Application.Current.MainWindow, targetFrame) as ModernFrame;

        //    //homeButton.Visibility = Visibility.Visible;

        //    BBCodeBlock bs = new BBCodeBlock();
        //    try
        //    {

        //        bs.LinkNavigator.Navigate(new Uri(targetPage, UriKind.Relative), frame);
        //        //bs.LinkNavigator.
        //        //do
        //        //{
        //        //frame.Content = new XpsEditor();
        //        //} while (bs.IsLoaded != true);
        //        //await Task.Delay(1500);

        //        //bs.Loaded += Bs_Loaded;
        //        // Bs_Loaded.

        //    }
        //    catch (Exception error)
        //    {
        //        //ModernDialog.ShowMessage(error.Message, FirstFloor.ModernUI.Resources.NavigationFailed, MessageBoxButton.OK);
        //    }
        //}


        //private async Task delayedWork()
        //{
        //    await Task.Delay(5000);
        //    this.doMyDelayedWork();
        //}


        public static Boolean send4Print(List<string> printList, string printTitle)
        {
            // Create the print dialog object and set options
            PrintDialog pDialog = new PrintDialog();
            pDialog.PageRangeSelection = PageRangeSelection.AllPages;
            pDialog.UserPageRangeEnabled = true;

            // Display the dialog. This returns true if the user presses the Print button.
            Nullable<Boolean> print = pDialog.ShowDialog();
            if (print == true)
            {

                foreach (string doc2P in printList)
                {
                    XpsDocument xpsDocument = new XpsDocument(doc2P, FileAccess.ReadWrite);
                    FixedDocumentSequence fixedDocSeq = xpsDocument.GetFixedDocumentSequence();
                    pDialog.PrintDocument(fixedDocSeq.DocumentPaginator, printTitle);
                }

                return true;

            }
            else if (print == false)
            {
                // printdialog was cancelled
                return false;
            }

            return false;
        }

        public static Boolean send4PrintFlow(List<string> printList, string printTitle)
        {
            // Create the print dialog object and set options
            PrintDialog pDialog = new PrintDialog();
            pDialog.PageRangeSelection = PageRangeSelection.AllPages;
            pDialog.UserPageRangeEnabled = true;

            // Display the dialog. This returns true if the user presses the Print button.
            Nullable<Boolean> print = pDialog.ShowDialog();
            if (print == true)
            {

                foreach (string doc2P in printList)
                {
                    XpsDocument xpsDocument = new XpsDocument(doc2P, FileAccess.ReadWrite);
                    FixedDocumentSequence fixedDocSeq = xpsDocument.GetFixedDocumentSequence();
                    pDialog.PrintDocument(fixedDocSeq.DocumentPaginator, printTitle);
                }

                return true;

            }
            else if (print == false)
            {
                // printdialog was cancelled
                return false;
            }

            return false;
        }




        private static void Bs_Loaded(object sender, RoutedEventArgs e)
        {
            //throw new NotImplementedException();
            MessageBox.Show("Loaded");
        }



        //LAUNCH PDF IN ACROBAT OR ACROBAT PRO FOR SEPARATIONS
        public static void LaunchAcrobat(string Ppath, string Fname)
        {
            //throw new NotImplementedException();
            //Process.Start(Ppath, Fname);
            //Process.Start(Ppath);

            Process p = new Process();
            ProcessStartInfo s = new ProcessStartInfo(Ppath);
            p.StartInfo = s;
            p.Start();


            //MessageBox.Show("Loaded");
        }


        public static void runGSWinPDFWATERMARK(String myInput, String myOutput)
        {
            //mswinpr2 is the windows printer devices

            // USE THIS ONE TO PRINT DIRECT TO A DEVICE, OUTPUT IS NOT GOOD IF SENT TO AN XPS VIRTUAL PRINTER
            //gswin64c.exe - sDEVICE = mswinpr2 - dBATCH - dNOPAUSE - sOutputFile = "%printer%Microsoft XPS Document Writer" C: \Users\hmcankms\Desktop\6126.pdf

            //gswin64c.exe - sDEVICE = mswinpr2 - dBATCH - dNOPAUSE - sDEFAULTPAPERSIZE = a3 - sOutputFile = "%printer%Microsoft XPS Document Writer" C: \Users\hmcankms\Desktop\5949.pdf

            //additional 
            // - sDEFAULTPAPERSIZE = a4
            // - q - dNOPAUSE - dBATCH - r600 - sDEFAULTPAPERSIZE = a4 - sDEVICE = tiffg4 - sOutputFile = " + myOutput + " " + myInput + ""



            // gswin32c -q -dNOPAUSE -dBATCH -sDEVICE=tiffg4  -r300x300 -sOutputFile=test.tif test.pdf

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            //MessageBox.Show(myInput);
            //MessageBox.Show(myOutput);
            //startInfo.FileName = @"GhostScript\gxps915.exe";

            startInfo.FileName = @"" + StaticFunctions.UncPathToUse + @"nick\ghostscript\gswin64c.exe";

            //startInfo.FileName = @"" + myDocs + @"\Workflow-User\gswin64c.exe";

            //MessageBox.Show(@"" + myDocs + @"\Workflow-User\gswin64c.exe");

            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //gxps915.exe -sDEVICE=pdfwrite -sOutputFile=myfile2.pdf -dNOPAUSE maintest.xps

            //startInfo.Arguments = " -dNOPAUSE -dBATCH -r300 -sCompression=none -sDEVICE=tiffg4 -sOutputFile=" + myOutput + " " + myInput + "";
            //Use this as Higher res = smaller djvu file size
            //startInfo.Arguments = "-q -dNOPAUSE -dBATCH -r600 -sDEFAULTPAPERSIZE=a4  -sDEVICE=tiffg4 -sOutputFile=" + myOutput + " " + myInput + "";

            //startInfo.Arguments = "-q -dNOPAUSE -dSAFER -dBATCH -sOutputFile=" + myOutput + " -sDEVICE=pdfwrite watermark.ps -f 5949S.pdf";   @"" + myDocs + @"\Workflow-User\watermark.ps -f 

            //*startInfo.Arguments = "-q -dNOPAUSE -dSAFER -dBATCH -sOutputFile=" + myOutput + " -sDEVICE=pdfwrite " + myDocs + @"\Workflow-User\watermark.ps -f " + myInput + "";

            startInfo.Arguments = "-q -dNOPAUSE -dSAFER -dBATCH -sOutputFile=" + myOutput + " -sDEVICE=pdfwrite " + StaticFunctions.UncPathToUse + @"nick\ghostscript\watermark.ps -f " + myInput + "";

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                }
            }
            catch
            {
                // Log error.
            }

        }// end of runGXPS




        // check for alpha characters
        private static Boolean alphaNumericCheck(String toCheck)
        {
            //Alpha Numeric check
            //Regex r = new Regex("^[a-zA-Z0-9]*$");
            Regex r = new Regex("^[0-9]*$");
            if (r.IsMatch(toCheck))
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        //enum FilingTypeEnum
        //{
        //    MemberCertsNew,
        //    MemberCertsLoose,
        //    MemberEnquiriesNew,
        //    MemberEnquiriesLoose,
        //    Marketing,
        //}


        //enum PlanTypeEnum
        //{
        //    UKBRKDWN,
        //    UKCRIT,
        //    UKDENT,
        //    UKIPROT,
        //    UKHCAP,
        //    UKMEDPLS,
        //    UKPAPP,
        //    UKTRAVEL,
        //    UKTERM,
        //    UKENQ,
        //    IRBRKDWN,
        //    IRCRIT,
        //    IRDENT,
        //    IRIPROT,
        //    IRHCAP,
        //    IRPAPP,
        //    IRTRAVEL,
        //    IRTERM,

        //}


        // SEND FOR FILING WILL BE USED BY MARKE
        public static String send4Filing(String FileNo, String PlanType, String FilingType)
        {
            //PlanTypes: Member Certs New | Member Certs Loose |  Member Enquiries New | Member Enquiries Loose | Marketing
            //FilingType: UKBRKDWN | UKCRIT | UKDENT | UK

            //local var for filepath memberCertsPath
            String saveFilePath = "";
            //ADD DATE AND TIME TO THE FILENAME
            string timeStamp = DateTime.Now.ToString();

            timeStamp = timeStamp.Replace(":", "-");
            timeStamp = timeStamp.Replace("/", "-");

            switch (FilingType)
            {

                case "Member Certs New":
                    if (alphaNumericCheck(FileNo) == false)
                    {
                        DialogYesNo("This requires a 6 digit numeric filename! e.g 013244", "Filename Issue", false);
                        //MessageBox.Show("This requires a 6 digit numeric filename! e.g 013244");
                        return "";
                    }
                    else if (FileNo.Length < 6)
                    {
                        DialogYesNo("File name length does not meet the required length of 6 digits", "Filename Issue", false);
                        //MessageBox.Show("File name length does not meet the required length of 6 digits");
                        return "";
                    }
                    else
                    {

                        saveFilePath = memberCertsPath(PlanType, FileNo);
                        saveFilePath = saveFilePath + " " + timeStamp;
                    }
                    break;

                case "Member Certs Loose":
                    if (alphaNumericCheck(FileNo) == false)
                    {
                        DialogYesNo("This requires a 6 digit numeric filename! e.g 013244", "Filename Issue", false);
                        //MessageBox.Show("This requires a 6 digit numeric filename! e.g 013244");
                        return "";
                    }
                    else if (FileNo.Length < 6)
                    {
                        DialogYesNo("File name length does not meet the required length of 6 digits", "Filename Issue", false);
                        //MessageBox.Show("File name length does not meet the required length of 6 digits");
                        return "";
                    }
                    else
                    {
                        saveFilePath = memberCertsPath(PlanType, FileNo);
                        saveFilePath = saveFilePath + " " + timeStamp;
                    }
                    break;

                case "Member Enquiries New":
                    if (alphaNumericCheck(FileNo) == false)
                    {
                        DialogYesNo("This requires a 6 digit numeric filename! e.g 013244", "Filename Issue", false);
                        //MessageBox.Show("This requires a 6 digit numeric filename! e.g 013244");
                        return "";
                    }
                    else if (FileNo.Length < 6)
                    {
                        DialogYesNo("File name length does not meet the required length of 6 digits", "Filename Issue", false);
                        //MessageBox.Show("File name length does not meet the required length of 6 digits");
                        return "";
                    }
                    else
                    {
                        saveFilePath = memberCertsPath("UKENQ", FileNo);
                        saveFilePath = saveFilePath + " " + timeStamp;
                    }
                    break;

                case "Member Enquiries Loose":

                    if (alphaNumericCheck(FileNo) == false)
                    {
                        DialogYesNo("This requires a 6 digit numeric filename! e.g 013244", "Filename Issue", false);
                        //MessageBox.Show("This requires a 6 digit numeric filename! e.g 013244");
                        return "";
                    }
                    else if (FileNo.Length < 6)
                    {
                        DialogYesNo("File name length does not meet the required length of 6 digits", "Filename Issue", false);
                        //MessageBox.Show("File name length does not meet the required length of 6 digits");
                        return "";
                    }
                    else
                    {
                        saveFilePath = memberCertsPath("UKENQ", FileNo);
                        saveFilePath = saveFilePath + " " + timeStamp;
                    }

                    break;


                case "Marketing":
                    if (alphaNumericCheck(FileNo) == false)
                    {
                        DialogYesNo("This requires a 6 digit numeric filename! e.g 013244", "Filename Issue", false);
                        //MessageBox.Show("This requires a 6 digit numeric filename! e.g 013244");
                        return "";
                    }
                    else if (FileNo.Length < 6)
                    {
                        DialogYesNo("File name length does not meet the required length of 6 digits", "Filename Issue", false);
                        //MessageBox.Show("File name length does not meet the required length of 6 digits");
                        return "";
                    }
                    else
                    {
                        string markType = PlanType;

                        if (markType == "Correspondence")
                        {
                            markType = "corr";
                        }


                        if (Directory.Exists(UncPathToUse + @"marketing\" + FileNo + @"\"))
                        {


                            if (File.Exists(UncPathToUse + @"marketing\" + FileNo + @"\" + markType + @"\" + FileNo + @".djvu"))
                            {
                                //MessageBox.Show("Yes");
                                //return null;
                                saveFilePath = UncPathToUse + @"scanned work\marketing\existing\" + markType + @"\" + FileNo;

                            }
                            else
                            {
                                saveFilePath = UncPathToUse + @"scanned work\marketing\new\" + markType + @"\" + FileNo;

                            }

                        }
                        else
                        {
                            //NEW FILE PLEASE CREATE DIRECTORIES
                            Directory.CreateDirectory(UncPathToUse + @"marketing\" + FileNo + @"\");
                            Directory.CreateDirectory(UncPathToUse + @"marketing\" + FileNo + @"\ARCHIVE\");
                            Directory.CreateDirectory(UncPathToUse + @"marketing\" + FileNo + @"\CORRESPONDENCE\");
                            Directory.CreateDirectory(UncPathToUse + @"marketing\" + FileNo + @"\MISC\");
                            File.Copy(UncPathToUse + @"nick\summary.xls", UncPathToUse + @"marketing\" + FileNo + @"\summary.xls");
                            saveFilePath = UncPathToUse + @"scanned work\marketing\new\" + markType + @"\" + FileNo;

                        }


                        //saveFilePath = memberCertsPath("UKENQ", fnameTxt.Text);
                        saveFilePath = saveFilePath + " " + timeStamp;


                    }



                    break;

                case "Accounts":


                    if (File.Exists(UncPathToUse + @"nick\scanning london\annual accounts\" + FileNo + @".djvu"))
                    {
                        //save as loose filing

                        saveFilePath = UncPathToUse + @"nick\scanning london\scan\loose\" + FileNo;

                    }
                    else
                    {
                        // save as new document
                        saveFilePath = UncPathToUse + @"nick\scanning london\scan\new\" + FileNo;

                    }



                    //saveFilePath = memberCertsPath("UKENQ", fnameTxt.Text);
                    //saveFilePath = saveFilePath + " " + timeStamp;
                    //no timestamp needs adding to accounts as there is no fixed digit ref that is handled by the conversion script






                    break;


            }




            return saveFilePath;


            //Used to save an xps doc to tif
            //docuMents.saveDocument(path2save);

            // call CreateMailItem
        }






        //Return save path for Member Certs and Member Enquiries
        private static String memberCertsPath(String typeOfPlan, String fname)
        {
            String thePath = "";
            //typeOfPlan = typeOfPlan.Replace("UK", "");
            //typeOfPlan = typeOfPlan.Replace("IR", "");

            switch (typeOfPlan)
            {

                case "UKBRKDWN":
                    if (File.Exists(@"" + UncPathToUse + @"UK\BRKDWN\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\UK\UKBRKDWN\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\UK\UKBRKDWN\" + fname;
                    }
                    break;
                case "UKCRIT":
                    if (File.Exists(@"" + UncPathToUse + @"UK\CRIT\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\UK\UKCRIT\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\UK\UKCRIT\" + fname;
                    }
                    break;
                case "UKDENT":

                    if (File.Exists(@"" + UncPathToUse + @"UK\DENT\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\UK\UKDENT\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\UK\UKDENT\" + fname;
                    }

                    break;
                case "UKIPROT":
                    if (File.Exists(@"" + UncPathToUse + @"UK\IPROT\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\UK\UKIPROT\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\UK\UKIPROT\" + fname;
                    }
                    break;
                case "UKHCAP":
                    if (File.Exists(@"" + UncPathToUse + @"UK\HCAP\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\UK\UKHCAP\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\UK\UKHCAP\" + fname;
                    }
                    break;
                case "UKMEDPLS":
                    if (File.Exists(@"" + UncPathToUse + @"UK\MEDPLS\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\UK\UKMEDPLS\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\UK\UKMEDPLS\" + fname;
                    }
                    break;

                case "UKPAPP":
                    if (File.Exists(@"" + UncPathToUse + @"UK\PAPP\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\UK\UKPAPP\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\UK\UKPAPP\" + fname;
                    }
                    break;
                case "UKTRAVEL": // TRAVEL IS FILED UNDER PAPP
                    if (File.Exists(@"" + UncPathToUse + @"UK\PAPP\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\UK\UKPAPP\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\UK\UKPAPP\" + fname;
                    }
                    break;
                case "UKTERM":
                    if (File.Exists(@"" + UncPathToUse + @"UK\TERM\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\UK\UKTERM\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\UK\UKTERM\" + fname;
                    }
                    break;

                // ENQUIRY SECTION
                case "UKENQ":
                    if (File.Exists(@"" + UncPathToUse + @"UK\ENQ\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\UK\UKENQ\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\UK\UKENQ\" + fname;
                    }
                    break;


                case "IRBRKDWN":
                    if (File.Exists(@"" + UncPathToUse + @"IR\BRKDWN\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\IR\IRBRKDWN\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\IR\IRBRKDWN\" + fname;
                    }
                    break;
                case "IRCRIT":
                    if (File.Exists(@"" + UncPathToUse + @"IR\CRIT\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\IR\IRCRIT\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\IR\IRCRIT\" + fname;
                    }

                    break;

                case "IRDENT":
                    if (File.Exists(@"" + UncPathToUse + @"IR\DENT\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\IR\IRDENT\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\IR\IRDENT\" + fname;
                    }
                    break;

                case "IRHCAP":
                    if (File.Exists(@"" + UncPathToUse + @"IR\HCAP\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\IR\IRHCAP\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\IR\IRHCAP\" + fname;
                    }
                    break;
                case "IPROT":
                    if (File.Exists(@"" + UncPathToUse + @"IR\IPROT\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\IR\IRIPROT\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\IR\IRIPROT\" + fname;
                    }

                    break;
                case "IRPAPP":
                    if (File.Exists(@"" + UncPathToUse + @"IR\PAPP\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\IR\IRPAPP\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\IR\IRPAPP\" + fname;
                    }
                    break;

                case "IRTERM":
                    if (File.Exists(@"" + UncPathToUse + @"IR\TERM\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\IR\IRTERM\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\IR\IRTERM\" + fname;
                    }
                    break;
                case "IRTRAVEL":// TRAVEL IS FILED UNDER PAPP
                    if (File.Exists(@"" + UncPathToUse + @"IR\PAPP\" + fname + @"\" + fname + @".djvu"))
                    {

                        //MessageBox.Show("Yes one there");
                        thePath = @"" + UncPathToUse + @"Scanned Work\Loose Filing\IR\IRPAPP\" + fname;
                    }
                    else
                    {
                        thePath = @"" + UncPathToUse + @"Scanned Work\Full Files\IR\IRPAPP\" + fname;
                    }
                    break;




                default:
                    break;
            }



            return thePath;
        }


        // String manipulation read and place on single line removing breaks
        public static string RemoveLineEndings(this string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return value;
            }
            string lineSeparator = ((char)0x2028).ToString();
            string paragraphSeparator = ((char)0x2029).ToString();

            return value.Replace("\r\n", string.Empty).Replace("\n", string.Empty).Replace("\r", string.Empty).Replace(lineSeparator, string.Empty).Replace(paragraphSeparator, string.Empty);
        }




        // IMAGE METADATA METHODS


        public static void writepngMeta(string input, string description)
        {
            Stream pngStream = new System.IO.FileStream(@"" + dsktop + @"\output\clipboard.png", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            PngBitmapDecoder pngDecoder = new PngBitmapDecoder(pngStream, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
            BitmapFrame pngFrame = pngDecoder.Frames[0];
            //BitmapMetadata metadata = pngDecoder.Frames[0].Metadata.Clone() as BitmapMetadata;
            //metadata.Title = "My PNG Doc";
            InPlaceBitmapMetadataWriter pngInplace = pngFrame.CreateInPlaceBitmapMetadataWriter();
            if (pngInplace.TrySave() == true)
            {
                //pngInplace.SetQuery("/Text/Description", "YES");
                pngInplace.Title = "My PNG Doc";
            }
            pngStream.Close();

            // Add the metadata of the bitmap image to the text block.
            //TextBlock myTextBlock = new TextBlock();
            //myTextBlock.Text = "The Description metadata of this image is: " + pngInplace.GetQuery("/Text/Description").ToString();
            //MessageBox.Show("The Description metadata of this image is: " + pngInplace.GetQuery("/Origin/Description").ToString());
        }

        public static void writetifMeta(string input, string description)
        {
            Stream tifStream = new System.IO.FileStream(@"" + dsktop + @"\output\HMCABG1.tif", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            TiffBitmapDecoder tifDecoder = new TiffBitmapDecoder(tifStream, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
            BitmapFrame tifFrame = tifDecoder.Frames[0];
            //BitmapMetadata metadata = pngDecoder.Frames[0].Metadata.Clone() as BitmapMetadata;
            //metadata.Title = "My PNG Doc";
            InPlaceBitmapMetadataWriter tifInplace = tifFrame.CreateInPlaceBitmapMetadataWriter();
            if (tifInplace.TrySave() == true)
            {
                tifInplace.SetQuery("/Text/Description", "YES");
                tifInplace.Title = "My TIFF Doc";
                tifInplace.Subject = "My TIFF Doc";
            }
            tifStream.Close();

            // Add the metadata of the bitmap image to the text block.
            //TextBlock myTextBlock = new TextBlock();
            //myTextBlock.Text = "The Description metadata of this image is: " + pngInplace.GetQuery("/Text/Description").ToString();
            //MessageBox.Show("The Description metadata of this image is: " + pngInplace.GetQuery("/Origin/Description").ToString());
        }

        public static void writejpgMeta(string input, string description)
        {
            Stream jpgStream = new System.IO.FileStream(@"" + dsktop + @"\output\test.jpg", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            JpegBitmapDecoder jpgDecoder = new JpegBitmapDecoder(jpgStream, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
            BitmapFrame jpgFrame = jpgDecoder.Frames[0];
            //BitmapMetadata metadata = pngDecoder.Frames[0].Metadata.Clone() as BitmapMetadata;
            //metadata.Title = "My PNG Doc";
            InPlaceBitmapMetadataWriter jpgInplace = jpgFrame.CreateInPlaceBitmapMetadataWriter();
            if (jpgInplace.TrySave() == true)
            {
                //pngInplace.SetQuery("/Text/Description", "YES");
                jpgInplace.Subject = "My JPEG Doc";
            }
            jpgStream.Close();

            // Add the metadata of the bitmap image to the text block.
            //TextBlock myTextBlock = new TextBlock();
            //myTextBlock.Text = "The Description metadata of this image is: " + pngInplace.GetQuery("/Text/Description").ToString();
            //MessageBox.Show("The Description metadata of this image is: " + pngInplace.GetQuery("/Origin/Description").ToString());
        }


        static void readpngMeta(string input, string description)
        {



        }




        // USE THIS TO PRINT FORMS OUT
        //public static void VirtualPrinter2XPS(int copyCount, FixedDocument myfd, String outputPath)
        //{
        //    try
        //    {



        //        if (File.Exists(outputPath))
        //        {
        //            File.Delete(outputPath);

        //        }
        //        //if (File.Exists(@"" + myDocs + @"\Workflow-User\Scan\xScanAppend.xps"))
        //        //{
        //        //    File.Delete(@"" + myDocs + @"\Workflow-User\Scan\xScanAppend.xps");

        //        //}




        //        if (_printer == null)
        //        {
        //            //switch (scanType)
        //            //{
        //            //    case "Standard":
        //            _printer = new VirtualPrinter(_printerName, outputPath);
        //            //        break;
        //            //    case "Insert":
        //            //        _printer = new VirtualPrinter(_printerName, @"" + myDocs + @"\Workflow-User\Scan\xScanInsert.xps");
        //            //        break;
        //            //    case "Append":
        //            //        _printer = new VirtualPrinter(_printerName, @"" + myDocs + @"\Workflow-User\Scan\xScanAppend.xps");
        //            //        break;

        //            //    default:
        //            //        break;
        //            //}

        //        }
        //        else
        //        {
        //            _printer.SetPort(@"" + myDocs + @"\Workflow-User\Scan\xScan.xps");
        //        }



        //        var dlg = new PrintDialog();

        //        LocalPrintServer local = new LocalPrintServer();
        //        PrintQueue pq = local.GetPrintQueue("MyXpsPrinter"); //e.g. local.GetPrintQueue("Microsoft XPS Document Writer");
        //        dlg.PrintQueue = pq;
        //        PrintTicket pt = pq.DefaultPrintTicket;
        //        pt.PageMediaSize = new PageMediaSize(PageMediaSizeName.ISOA4);// or we can specify the custom size(width, height) here
        //        dlg.PrintTicket = pt;
        //        // pq.
        //        // e.g local.GetPrintQueue("Microsoft XPS Document Writer");
        //        // local.DefaultPrintQueue("MyXpsPrinter"); 
        //        // dlg.PrintQueue = new PrintServer().GetPrintQueues(); // this will be your printer. any of these: new PrintServer().GetPrintQueues()
        //        dlg.PrintTicket.CopyCount = 1; // number of copies
        //        dlg.PrintTicket.PageOrientation = PageOrientation.Portrait;

        //        // dlg.PrintVisual(canvasEdit, "canvas");

        //        dlg.PrintDocument(myfd.DocumentPaginator, "MyFixedDocument");

        //        //thumbListbox.Items.Clear();
        //        ////myStackPanel.Children.Clear();
        //        //canvasEdit.Children.Clear();


        //        //Dispose of the virtual xps printer
        //        if (_printer != null)
        //        {
        //            _printer.Dispose();
        //            _printer = null;
        //        }


        //    }
        //    catch (Exception)
        //    {
        //        if (_printer != null)
        //        {
        //            _printer.Dispose();
        //            _printer = null;
        //        }
        //        //throw;
        //    }
        //}



        //public static void CreateMyWPFControlReport(MyWPFControlDataSource usefulData)
        //{
        //    //Set up the WPF Control to be printed
        //    MyWPFControl controlToPrint;
        //    controlToPrint = new MyWPFControl();
        //    controlToPrint.DataContext = usefulData;

        //    FixedDocument fixedDoc = new FixedDocument();
        //    PageContent pageContent = new PageContent();
        //    FixedPage fixedPage = new FixedPage();

        //    //Create first page of document
        //    fixedPage.Children.Add(controlToPrint);
        //    ((System.Windows.Markup.IAddChild)pageContent).AddChild(fixedPage);
        //    fixedDoc.Pages.Add(pageContent);
        //    //Create any other required pages here

        //    //View the document
        //    documentViewer1.Document = fixedDoc;
        //}





        //public static FixedDocument SaveCurrentViewToXPS<T>(MarketingVM dContext, object theType, UIElement type)
        //{

        //    //https://istacee.wordpress.com/2013/03/19/save-wpf-control-as-xps-and-fit-it-into-an-a4-papersize-page/

        //    // Initialize the xps document structure
        //    FixedDocument fixedDoc = new FixedDocument();
        //    PageContent pageContent = new PageContent();
        //    FixedPage fixedPage = new FixedPage();

        //    //var view = (MarketingIRJobsInbox)theType;
        //    var view = (MarketingRP)theType;
        //    //var view = type;
        //    view.DataContext = dContext;
        //    view.UpdateLayout();
        //    //}
        //    //VisualBrush vb = new VisualBrush(view);
        //    //vb.Stretch = Stretch.Uniform;



        //    //MarketingIRJobsInbox view = new MarketingIRJobsInbox();

        //    //MessageBox.Show(view.Width + " x " + view.Height);

        //    // Get the page size of an A4 document
        //    var pageSize = new Size(8.27 * 96.0, 11.69 * 96.0);

        //    // We just fit it horizontally, so only the width is set here
        //    //view.Height = pageSize.Height;
        //    //Canvas canv = new Canvas();
        //    //canv.Width = pageSize.Width;
        //    //canv.Height = pageSize.Height;
        //    //canv.Background = vb;
        //    //canv.UpdateLayout();
        //    view.Height = pageSize.Height;
        //    view.Width = pageSize.Width;
        //    //view.Measure(pageSize);
        //    //view.Arrange(new Rect(new Point(), pageSize));
        //    view.UpdateLayout();
        //    //view.UpdateLayout();




        //    //Create first page of document
        //    fixedPage.Children.Add(type);
        //    //fixedPage.Children.Add(view);
        //    fixedPage.Measure(pageSize);
        //    fixedPage.Arrange(new Rect(new Point(), pageSize));
        //    ((System.Windows.Markup.IAddChild)pageContent).AddChild(fixedPage);
        //    fixedDoc.Pages.Add(pageContent);







        //    // Create the xps file and write it

        //    //XpsDocument xpsd = new XpsDocument(filename, FileAccess.ReadWrite);
        //    //XpsDocumentWriter xw = XpsDocument.CreateXpsDocumentWriter(xpsd);
        //    //xw.Write(fixedDoc);
        //    //xpsd.Close();
        //    //}
        //    return fixedDoc;
        //}



    } // end class
} // end ns

