﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Nebula.Paginators;
using Nebula.ViewModels;
using Nebula.Helpers;
using System.Windows.Documents;
using System.Windows.Controls;
using System.IO;
using System.Configuration;
using System.Windows.Media;
using Nebula.Models;
using System.Collections.ObjectModel;

namespace Nebula.Commands
{
    public class PrintSaveFlowDocumentDTCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public PrintSaveFlowDocumentDTCommand(DriveTestViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            var values = (object[])parameter;
            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //if (lv != null)
            //{
            //    if (lv.SelectedItems.Count > 0)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    return true;
            //}
            return true;
        }

        public async void Execute(object parameter)
        {
            var values = (object[])parameter;
            //Split array into specific types
            var flowdoc = values[0];
            var lvValue = (ListView)values[1];


            DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

            //Generate PDF From FlowDocument
            //NEW STRAIGHT THROUGH
            dpw.SaveDocGOTPDF((FlowDocument)flowdoc, vm.myDocs + @"\" + Clipboard.GetText() + @".pdf");

            string PDFReportPath = vm.myDocs + @"\" + Clipboard.GetText() + @".pdf";


            //zip Test Reports and create zip

            if (lvValue.Items.Count != 0)
            {
                //MessageBox.Show("Hmmm");

                //try
                //{
                vm.ProcessedMessageColour = Brushes.MediumPurple;
                vm.ProcessedMessage = "All Reports for current PO are being Archived. Please Wait....";

                //vm.ProgressIsActive = true;
                ////Change mouse cursor 
                //Mouse.OverrideCursor = Cursors.Wait;

                //Create Local Folder
                if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                {
                    if (string.IsNullOrEmpty(vm.DBSearch))
                    {
                    }
                    else
                    {
                        //Create local file
                        Directory.CreateDirectory(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports");
                    }

                }


                //Call Bulk Copy Function ASYNC


                //Check which LV was passed

                if (lvValue.Name.Contains("TestResults"))
                {
                    //Loops Through and copies all files from destination to source
                    await System.Threading.Tasks.Task.Run(() => CopyBulkReportsNVISIONZipEmail(@"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder", vm.TestedDriveCollection, PDFReportPath));
                }
                else
                {
                    //Loops Through and copies all files from destination to source
                    await System.Threading.Tasks.Task.Run(() => CopyBulkReportsNVISIONZipEmail(@"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Drive Test\Wipe Drive Test Reports\DropFolder", vm.SearchCollection, PDFReportPath));
                }



            


            }


        }



        //Method to loop through Test Report location and Zip
        public async void CopyBulkReportsNVISIONZipEmail(string sourceDir, ObservableCollection<NetsuiteInventory> CollectionToUse, string reportPDFPath)
        {

            //Clear any Documents folders
            if (Directory.Exists(vm.myDocs + @"\" + vm.DBSearch + "_Reports"))
            {
                //Delete Local Folder
                Directory.Delete(vm.myDocs + @"\" + vm.DBSearch + "_Reports", true);

            }


            //Loop through collection and copy file to local folder

            foreach (NetsuiteInventory nsi in CollectionToUse)
            {

                if (File.Exists(nsi.ReportPath))
                {
                    // Console.WriteLine(nsi.ReportPath);
                    //Copy File from Drop Folder to local directory
                    // Remove path from the file name.
                    string fName = nsi.ReportPath.Substring(sourceDir.Length + 1);

                    // Console.WriteLine(fName);
                    // Use the Path.Combine method to safely append the file name to the path.
                    // Will overwrite if the destination file already exists.
                    //MessageBox.Show("FROM " + Path.Combine(sourceDir, fName) + " TO " + Path.Combine(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports", fName));
                    File.Copy(Path.Combine(sourceDir, fName), Path.Combine(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports", fName), true);
                }
            }


            //zip folder and attach to email 

            EmailClass zipemail = new EmailClass(vm);
            await System.Threading.Tasks.Task.Run(() => zipemail.SearchZipFolder(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports", reportPDFPath));

            //Delete Original Folder?
            //Create Local Folder
            if (Directory.Exists(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports"))
            {
                //Delete Local Folder
                Directory.Delete(vm.myDocs + @"\Nebula Logs\" + vm.DBSearch + "_Reports", true);

            }
                      


        }

       
    }
}

