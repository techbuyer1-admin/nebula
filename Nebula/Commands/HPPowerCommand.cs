﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.DellServers;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class HPPowerCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public HPPowerCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter != null)
            {
                var values = (object[])parameter;

                var ServerIP = (string)values[0];

                var PowerOption = (string)values[1];
                //MessageBox.Show(ServerIP);
                ////ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
                if (ServerIP != String.Empty)
                {
                    //check if valid ip entered
                    //System.Net.IPAddress ipAddress = null;
                    //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                    if (ServerIP != String.Empty)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
          

            // return true;
        }
         
          
       

        public async void Execute(object parameter)
        {

            var values = (object[])parameter;
            var ServerIP = (string)values[0];
            var PowerOption = (string)values[1];



            if (ServerIP != null)
            {
                if (ServerIP != string.Empty)
                {
                   
                    string address = ServerIP;
                    //send a ping to the server to check it's available
                    vm.ProgressMessage = "Checking Server IP, Please Wait...";
                    vm.ProgressPercentage = 0;
                    vm.ProgressIsActive = true;
                    vm.ProgressVisibility = Visibility.Visible;
                    sendAsyncPingPacket(address);

                    await PutTaskDelay(3000);

                    //if it is run the code
                    if (vm.IsServerThere == "Yes")
                    {

                        ProcessPiper pp = new ProcessPiper(vm);

                        //RESET THE COMMAND BOX
                        vm.ILORestOutput = "";

                        switch (PowerOption)
                        {
                            case "PowerOn":
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressMessage = "Powering Server On!";

                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot On --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                vm.ProgressVisibility = Visibility.Hidden;
                                vm.MessageVisibility = Visibility.Visible;
                                vm.ProgressIsActive = false;
                                vm.ProgressPercentage = 0;
                                vm.ProgressMessage = "Scripts completed successfully!";
                                break;
                            case "PowerOff":
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressMessage = "Powering Server Down";

                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot PressAndHold --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                vm.ProgressVisibility = Visibility.Hidden;
                                vm.MessageVisibility = Visibility.Visible;
                                vm.ProgressIsActive = false;
                                vm.ProgressPercentage = 0;
                                vm.ProgressMessage = "Scripts completed successfully!";
                                break;
                            case "Reset":
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressMessage = "Rebooting Server!";

                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                vm.ProgressVisibility = Visibility.Hidden;
                                vm.MessageVisibility = Visibility.Visible;
                                vm.ProgressIsActive = false;
                                vm.ProgressPercentage = 0;
                                vm.ProgressMessage = "Scripts completed successfully!";
                                break;
                                //SUPPORT FOR MULTI SERVERS
                                //GEN8
                            case "PowerOnGen8S1":
                                vm.TabColorS1 = Brushes.Orange;
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Powering Server On! Please wait...";
                               // MessageBox.Show(ServerIP);
                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot On --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen8" });
                                //await PutTaskDelay(5000);
                                ////EXTRACT ALL SYSTEM INFO TO JSON
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + ServerIP + "All.json  --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                await PutTaskDelay(130000);
                                ////GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l ServerHealth1.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                ////Launch Remote Console
                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen8" });

                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully!";
                                
                                //set colours
                                if (vm.WarningMessage1 != "")
                                {
                                    vm.TabColorS1 = Brushes.Red;
                                }
                                else
                                {
                                    vm.TabColorS1 = Brushes.Green;
                                }
                                break;
                            case "PowerOffGen8S1":
                                vm.TabColorS1 = Brushes.Orange;
                              
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressMessage1 = "Powering Server Down! Please wait...";
                             
                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot PressAndHold --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));


                                //await PutTaskDelay(5000);
                                ////EXTRACT ALL SYSTEM INFO TO JSON
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + ServerIP + "All.json  --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                //// await PutTaskDelay(20000);
                                ////GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l ServerHealth1.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));

                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo" , "Gen8"});


                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully! Please wait...";
                                //vm.TabColorS1 = Brushes.Orange;
                                //set colours
                                if (vm.WarningMessage1 != "")
                                {
                                    vm.TabColorS1 = Brushes.Red;
                                }
                                else
                                {
                                    vm.TabColorS1 = Brushes.Green;
                                }
                                break;
                            
                            case "ResetGen8S1":
                                vm.TabColorS1 = Brushes.Orange;
                               
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Rebooting Server! Please wait...";
                             
                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                //await PutTaskDelay(5000);
                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen8" });

                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Server Booting! Please wait...";
                                await PutTaskDelay(130000);
                                ////GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l ServerHealth1.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));


                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen8" });

                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully!";
                                vm.TabColorS1 = Brushes.Orange;
                                //set colours
                                if (vm.WarningMessage1 != "")
                                {
                                    vm.TabColorS1 = Brushes.Red;
                                }
                                else
                                {
                                    vm.TabColorS1 = Brushes.Green;
                                }
                                break;


                            //GEN9
                            case "PowerOnGen9S1":
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Powering Server On! Please wait...";
                                // MessageBox.Show(ServerIP);
                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot On --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen9" });
                                //await PutTaskDelay(5000);
                                ////EXTRACT ALL SYSTEM INFO TO JSON
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + ServerIP + "All.json  --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Server Booting! Please wait...";
                                await PutTaskDelay(130000);
                                ////GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l ServerHealth1.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                ////Launch Remote Console
                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen9" });

                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully!";
                                break;
                            case "PowerOffGen9S1":
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Powering Server Down! Please wait...";

                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot PressAndHold --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));


                                //await PutTaskDelay(5000);
                                ////EXTRACT ALL SYSTEM INFO TO JSON
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + ServerIP + "All.json  --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                //// await PutTaskDelay(20000);
                                ////GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l ServerHealth1.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));

                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen9" });


                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully!";
                                break;
                            case "ResetGen9S1":
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Rebooting Server! Please wait...";

                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                //await PutTaskDelay(5000);

                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen9" });
                                ////EXTRACT ALL SYSTEM INFO TO JSON
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + ServerIP + "All.json  --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                await PutTaskDelay(130000);
                                ////GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l ServerHealth1.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));


                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen9" });

                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully!";
                                break;
                   

                            //GEN10
                            case "PowerOnGen10S1":
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Powering Server On! Please wait...";
                                // MessageBox.Show(ServerIP);
                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot On --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen10" });
                                // vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen10" });
                                //await PutTaskDelay(5000);
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Server Booting! Please wait...";
                                await PutTaskDelay(130000);
                                ////GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l ServerHealth1.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                ////Launch Remote Console
                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen10" });

                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully!";
                                break;
                            case "PowerOffGen10S1":
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Powering Server Down!";

                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot PressAndHold --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));


                                //await PutTaskDelay(5000);
                                ////EXTRACT ALL SYSTEM INFO TO JSON
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + ServerIP + "All.json  --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                //// await PutTaskDelay(20000);
                                ////GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l ServerHealth1.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));

                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen10" });


                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully! Please wait...";
                                break;
                            case "ResetGen10S1":
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Rebooting Server!";

                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                               //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen10" });
                                
                                
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Server Booting! Please wait...";
                                await PutTaskDelay(130000);
                                ////GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l ServerHealth1.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));


                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen10" });

                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully! Please wait...";
                                break;
                      

                            case "FormatFlashG81":
                                vm.TabColorS1 = Brushes.Orange;
                               
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Formatting Embedded Flash, Please Wait...";
                               
                                //format flash
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\FormatEmbeddedFlash.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                vm.ProgressPercentage1 = 30;

                                await PutTaskDelay(80000);

                                vm.ProgressMessage1 = "Rebooting Server!";
                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                await PutTaskDelay(80000);
                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen8" });
                                await PutTaskDelay(25000);
                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 100;
                                vm.ProgressMessage1 = "Scripts completed successfully! Intelligent Provisioning will need to be installed after this process!";
                               
                                //set colours
                                if (vm.WarningMessage1 != "")
                                {
                                    vm.TabColorS1 = Brushes.Red;
                                }
                                else
                                {
                                    vm.TabColorS1 = Brushes.Green;
                                }


                                break;
                      

                            case "FormatFlashG91":
                                vm.TabColorS1 = Brushes.Orange;

                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Formatting Embedded Flash, Please Wait...";

                                //format flash
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\FormatEmbeddedFlash.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                vm.ProgressPercentage1 = 30;

                                await PutTaskDelay(80000);

                                vm.ProgressMessage1 = "Rebooting Server!";
                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                await PutTaskDelay(80000);
                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen9" });
                                await PutTaskDelay(25000);
                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 100;
                                vm.ProgressMessage1 = "Scripts completed successfully! Intelligent Provisioning will need to be installed after this process!";

                                //set colours
                                if (vm.WarningMessage1 != "")
                                {
                                    vm.TabColorS1 = Brushes.Red;
                                }
                                else
                                {
                                    vm.TabColorS1 = Brushes.Green;
                                }


                                break;
                       
                            case "FormatFlashG101":
                                vm.TabColorS1 = Brushes.Orange;

                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Formatting Embedded Flash, Please Wait...";

                                //format flash
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\FormatEmbeddedFlash.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                vm.ProgressPercentage1 = 30;

                                await PutTaskDelay(80000);

                                vm.ProgressMessage1 = "Rebooting Server!";
                                await Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                await PutTaskDelay(80000);
                                //run server infor update
                                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen10" });
                                await PutTaskDelay(25000);
                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 100;
                                vm.ProgressMessage1 = "Scripts completed successfully! Intelligent Provisioning will need to be installed after this process!";

                                //set colours
                                if (vm.WarningMessage1 != "")
                                {
                                    vm.TabColorS1 = Brushes.Red;
                                }
                                else
                                {
                                    vm.TabColorS1 = Brushes.Green;
                                }


                                break;
                           

                            case "RenameServerAssetTag1":
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";

                                vm.ProgressMessage1 = "Setting Server Name to 'NEW SERVER' & Asset Tag to 'NEW TAG";
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Set_Server_Name_Asset_Tag.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                vm.ProgressPercentage1 = 0;

                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully!";
                                break;
                          
                            //DELL
                            case "PowerOnDELLS1":
                                vm.TabColorS1 = Brushes.Orange;
                               
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Powering Server On! Please wait...";
                                // MessageBox.Show(ServerIP);
                                //acquire the server inventory
                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerup", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  systemerase nvdimm", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                //await PutTaskDelay(5000);
                                ////EXTRACT ALL SYSTEM INFO TO JSON
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + ServerIP + "All.json  --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                //await PutTaskDelay(20000);
                                ////GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l ServerHealth1.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                ////GetServerInventory
                                //vm.RunRACDMScriptsCommandDellPES1.Execute(new String[] { "ServerInfo", "Dell" });
                                //run server infor update
                                // await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerstatus -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully!";
                                
                                //set colours
                                if (vm.WarningMessage1 != "")
                                {
                                    vm.TabColorS1 = Brushes.Red;
                                }
                                else
                                {
                                    vm.TabColorS1 = Brushes.Green;
                                }
                                break;
                            case "PowerOffDELLS1":
                                vm.TabColorS1 = Brushes.Orange;
                               
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressMessage1 = "Powering Server Down! Please wait...";

                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerdown -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerdown", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                //await PutTaskDelay(5000);
                                ////EXTRACT ALL SYSTEM INFO TO JSON
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + ServerIP + "All.json  --url " + ServerIP + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                // await PutTaskDelay(20000);
                                ////GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l ServerHealth1.xml -s " + ServerIP + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                ////Launch Remote Console
                                ////GetServerInventory
                                // vm.RunRACDMScriptsCommandDellPES1.Execute(new String[] { "ServerInfo", "Dell" });
                                //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerstatus", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully! Please wait...";
                               
                                //set colours
                                if (vm.WarningMessage1 != "")
                                {
                                    vm.TabColorS1 = Brushes.Red;
                                }
                                else
                                {
                                    vm.TabColorS1 = Brushes.Green;
                                }
                                break;
                            case "ResetDELLS1":
                                vm.TabColorS1 = Brushes.Orange;
                               
                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Rebooting Server! Please wait...";

                               
                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction hardreset", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                await PutTaskDelay(10000);
                                await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  racreset hard -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                               
                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully!";
                                
                                //set colours
                                if (vm.WarningMessage1 != "")
                                {
                                    vm.TabColorS1 = Brushes.Red;
                                }
                                else
                                {
                                    vm.TabColorS1 = Brushes.Green;
                                }
                                break;

                            case "ClearJobsDELLS1":
                                vm.TabColorS1 = Brushes.Orange;

                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Clearing Job Queue!";

                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn jobqueue delete --all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully!";

                                //set colours
                                if (vm.WarningMessage1 != "")
                                {
                                    vm.TabColorS1 = Brushes.Red;
                                }
                                else
                                {
                                    vm.TabColorS1 = Brushes.Green;
                                }
                                break;

                            //Lenovo
                            case "PowerOnLenovoS1":
                                vm.TabColorS1 = Brushes.Orange;

                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Powering Server On! Please wait...";
                                // MessageBox.Show(ServerIP);
                                //acquire the server inventory
                                //Power On System
                                await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"misc ospower turnon -N --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool"));


                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully!";

                                //set colours
                                if (vm.WarningMessage1 != "")
                                {
                                    vm.TabColorS1 = Brushes.Red;
                                }
                                else
                                {
                                    vm.TabColorS1 = Brushes.Green;
                                }
                                break;
                            case "PowerOffLenovoS1":
                                vm.TabColorS1 = Brushes.Orange;

                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressMessage1 = "Powering Server Down! Please wait...";

                                await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"misc ospower turnoff -N --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool"));


                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully! Please wait...";

                                //set colours
                                if (vm.WarningMessage1 != "")
                                {
                                    vm.TabColorS1 = Brushes.Red;
                                }
                                else
                                {
                                    vm.TabColorS1 = Brushes.Green;
                                }
                                break;
                            case "ResetLenovoS1":
                                vm.TabColorS1 = Brushes.Orange;

                                vm.WarningMessage1 = "";
                                vm.ProgressMessage1 = "";
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressMessage1 = "Rebooting Server! Please wait...";


                                await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"misc ospower reboot -N --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool"));


                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Scripts completed successfully!";

                                //set colours
                                if (vm.WarningMessage1 != "")
                                {
                                    vm.TabColorS1 = Brushes.Red;
                                }
                                else
                                {
                                    vm.TabColorS1 = Brushes.Green;
                                }
                                break;


                        }

                       
                    }
                    else
                    {
                        vm.ProgressMessage = "No response from the Server with IP:" + ServerIP;
                    }

                }

            }


        }


        public async void sendAsyncPingPacket(string hostToPing)
        {
            try
            {
                // int timeout = 5000;
                // AutoResetEvent waiter = new AutoResetEvent(false);
                Ping pingPacket = new Ping();
                //ping completion event reaised
                pingPacket.PingCompleted += new PingCompletedEventHandler(PingCompletedCallback);
                string data = "Ping test check";
                byte[] byteBuffer = Encoding.ASCII.GetBytes(data);
                PingOptions pingOptions = new PingOptions(64, true);

                await pingPacket.SendPingAsync(hostToPing, 3000);
                //pingPacket.Send(hostToPing, 3000);

            }
            catch (PingException pe)
            {
                string exception = pe.Message;
                //IPResults.Add("INVALID IP ADDRESS FOUND");
            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                //IPResults.Add("Exceptin " + ex.Message);
            }

        }


        private void PingCompletedCallback(object sender, PingCompletedEventArgs e)
        {
            try
            {
                if (e.Cancelled)
                {

                }

                // If an error occurred, display the exception to the user.
                if (e.Error != null)
                {

                }

                PingReply reply = e.Reply;

                if (reply == null)
                {
                    vm.IsServerThere = "No";
                    return;
                }
                else if (reply.Status == IPStatus.Success)
                {
                    vm.IsServerThere = "Yes";
                    return;

                }
            }
            catch (PingException pe)
            {
                Console.WriteLine(pe.Message);
                //IPResults.Add("INVALID IP ADDRESS");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                // MessageBox.Show("Came Here");
                //IPResults.Add("Exception " + ex.Message);
            }
        }

        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }


    }

          
}


