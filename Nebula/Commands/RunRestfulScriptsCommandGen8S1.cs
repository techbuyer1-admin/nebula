﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class RunRestfulScriptsCommandGen8S1 : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;
        //To Hold Reference to the hosting tab
        string ServerTabHeader;


        TabItem ServerTab = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public RunRestfulScriptsCommandGen8S1(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            if (vm.Server1IPv4 != null)
            {
                //check if valid ip entered
                //System.Net.IPAddress ipAddress = null;
                //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                if (vm.Server1IPv4 != string.Empty)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public async void Execute(object parameter)
        {


            var values = (object[])parameter;
            var ScriptChoice = (string)values[0];
            var ServerGen = (string)values[1];

            //A: Setup and stuff you don't want timed
            var timer = new Stopwatch();
            timer.Start();
            TimeSpan timeTaken;
            string timetaken;

            string SyncLog = "";

            try
            {

        


            SyncLog += "[Start of Nebula Sync Log]\r\r" + DateTime.Now.ToString() + "\r\r";



            switch (vm.WhichTab)
            {
                case "1":
                    // MessageBox.Show("Server On Tab 1");
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS1") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server1";
                    break;
                case "2":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS2") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server2";
                    // MessageBox.Show("Server On Tab 2");
                    break;
                case "3":
                    //MessageBox.Show("Server On Tab 3");
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS3") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server3";
                    break;
                case "4":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS4") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server4";
                    // MessageBox.Show("Server On Tab 2");
                    break;
                case "5":
                    // MessageBox.Show("Server On Tab 1");
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS5") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server5";
                    break;
                case "6":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS6") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server6";
                    // MessageBox.Show("Server On Tab 2");
                    break;
                case "7":
                    // MessageBox.Show("Server On Tab 1");
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS7") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server7";
                    break;
                case "8":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS8") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server8";
                    // MessageBox.Show("Server On Tab 2");
                    break;
                case "9":
                    // MessageBox.Show("Server On Tab 1");
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS9") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server9";
                    break;
                case "10":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS10") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server10";
                    // MessageBox.Show("Server On Tab 2");
                    break;
                case "11":
                    // MessageBox.Show("Server On Tab 1");
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS11") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server11";
                    break;
                case "12":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS12") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server12";
                    // MessageBox.Show("Server On Tab 2");
                    break;

                case "13":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS13") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server13";
                    // MessageBox.Show("Server On Tab 2");
                    break;

                case "14":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS14") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server14";
                    // MessageBox.Show("Server On Tab 2");
                    break;

                case "15":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS15") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server15";
                    // MessageBox.Show("Server On Tab 2");
                    break;

                case "16":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS16") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server16";
                    // MessageBox.Show("Server On Tab 2");
                    break;


            }


            vm.TabColorS1 = Brushes.Orange;
           

            if (vm.Server1IPv4 != null)
            {
                if (vm.Server1IPv4 != string.Empty)
                {
                    string address = vm.Server1IPv4;
                    vm.WarningMessage1 = "";
                    vm.ProgressMessage1 = "";
                   
                    //send a ping to the server to check it's available
                    vm.ProgressMessage1 = "Checking Server IP, Please Wait...";
                    vm.ProgressPercentage1 = 0;
                    vm.ProgressIsActive1 = true;
                    vm.ProgressVisibility1 = Visibility.Visible;
                    sendAsyncPingPacket(address);

                    await PutTaskDelay(3000);

                    //if it is run the code
                    if (vm.IsServerThere1 == "Yes")
                    {


                        //Clear data files
                        ClearXMLJSONFiles();

                        ProcessPiper pp = new ProcessPiper(vm);

                        //RESET THE COMMAND BOX
                        vm.ILORestOutput1 = "";


                        //CANCEL SCRIPT
                        if (vm.CancelScriptS1 == true)
                        {
                            vm.ProgressPercentage1 = 0;
                            vm.ProgressIsActive1 = false;
                            vm.ProgressVisibility1 = Visibility.Hidden;
                            vm.ProgressMessage1 = "Scripts Cancelled!";
                            vm.WarningMessage1 = "";
                            vm.CancelScriptS1 = false;
                            return;
                        }
                        //END CANCEL SCRIPT



                        //CHECK ILO FOR MINIMUM 2:00 FOR RESTFUL API
                        vm.ILOMinVersion1 = "";
                        //CLEAR RESTFUL INTERFACE JSON FILE
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "FWLevel1.xml"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "FWLevel1.xml");
                        }

                        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Get_FW_Version.xml -l " + vm.Server1IPv4 + "FWLevel1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                       //await PutTaskDelay(20000);

                     
                        vm.IloFWCheck(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "FWLevel1.xml", "2.00", "1", "Gen8");

                        //MessageBox.Show(vm.ILOMinVersion1);

                        // IF UPGRADE NEEDED SKIP RUNNING ANY RESTFUL SCRIPTS
                        if (vm.ILOMinVersion1 != "Gen8UpgradeNeeded")
                        {
                      
                        switch (ScriptChoice)
                        {
                           

                            case "ServerInfo":
                                    //////EXTRACT FIRMWARE JSON
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/systems/1/firmwareinventory --expand -f" + vm.ServerIPv4 + "firmware.json  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //////EXTRACT SYSTEM JSON
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/systems/1 --expand -f" + vm.ServerIPv4 + "fullserverinfo.json  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //////EXTRACT SYSTEM JSON
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/chassis/1 --expand -f" + vm.ServerIPv4 + "chassisinfo.json  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //////EXTRACT SYSTEM JSON
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/chassis/1/power --expand -f" + vm.ServerIPv4 + "power.json  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //////EXTRACT SYSTEM JSON
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/systems --expand -f" + vm.ServerIPv4 + "systems.json  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //POLL SERVER FOR ILO FIRMWARE
                        


                                vm.ProgressPercentage1 = 10;
                                vm.ProgressIsActive1 = true;
                                vm.ProgressVisibility1 = Visibility.Visible;
                                vm.ProgressMessage1 = "Receiving Information From Server, Please Wait...";

                                    //CANCEL SCRIPT
                                    if (vm.CancelScriptS1 == true)
                                    {
                                        vm.ProgressPercentage1 = 10;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                        return;
                                    }
                                    //END CANCEL SCRIPT

                                   

                                //GET THE EMBEDDED HEALTH INFO COMES IN AS XML 
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l " + vm.Server1IPv4 + "ServerHealth1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                        vm.ProgressPercentage1 = 20;
                                //EXTRACT ALL SYSTEM INFO TO JSON  for extended drive info add .,HPSmartStorageDiskDrive
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + vm.Server1IPv4 + "All.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                        vm.ProgressPercentage1 = 30;
                                        //EXTRACT ALL PHYSICAL DISK INFO TO JSON  
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --select HpSmartStorageDiskDrive. -f" + vm.Server1IPv4 + "Disks.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                        vm.ProgressPercentage1 = 50;

                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/managers/1/embeddedmedia -f" + vm.Server1IPv4 + "SDCARD.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                    //Check If One View Managed
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/ --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    // await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --select fwswversioninventory. -f" + vm.ServerIPv4 + "all.json  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                    vm.ProgressPercentage1 = 60;
                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Get_Global.xml -l GlobalDetails.xml -s " + vm.ServerIPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));

                               // await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "virtualmedia 2 HPIP250.2016_1015.169.iso  --bootnextreset --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                //Read the Data from the JSON and XML Files pulled from the Server  virtualmedia 2 http://xx.xx.xx.xx/path/to/vm.iso --bootnextreset
                                ReadJsonXMLObjects(ServerGen);


                                    //DOES NOT MOUNT
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "-v --nologo rawpost ImageDeploy.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //REQUIRES HTTP SERVER THAT HOSTS THE ISO
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "virtualmedia 2 hp-ivb_update-1.80-2.iso --bootnextreset --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                    //virtualmedia 2 http://xx.xx.xx.xx/path/to/vm.iso --bootnextreset

                                    //Stop Timer
                                    timer.Stop();
                                    timeTaken = timer.Elapsed;
                                    timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                    vm.ProgressPercentage1 = 100;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    vm.ProgressMessage1 = "Scripts completed successfully! " + timetaken;

                                    if (vm.ILORestOutput1.Contains("ERROR: 'NoneType' object has no attribute 'obj'"))
                                    {
                                        vm.ProgressMessage1 = "There seems to be an issue with this servers Restful Api! Please select the latest iLO Firmware Update from above.";
                                    }

                                    //Check if links error, could be BIOS OR ILO not a minimum level for Restful api  && vm.ILORestOutput1.Contains("" + vm.Server1IPv4 + "All.json")  != false
                                    if (vm.ILORestOutput1.Contains("ERROR: 'links'") && vm.ILORestOutput1.Contains("" + vm.Server1IPv4 + "All.json") != true)
                                    {
                                        vm.WarningMessage1 = "Json file not produced. Bios Or iLO version below 2.00. Select Required Updates and Click the BIOS\\iLO Update Button";
                                        vm.HPBiosIloButtonVisibilityS1 = Visibility.Visible;
                                    }
                                    else
                                    {
                                        vm.HPBiosIloButtonVisibilityS1 = Visibility.Collapsed;
                                    }


                                    //CHECK SPS VERSION IF V2 UPGRADE REQUIRED
                                    if (vm.Gen8ServerInfo1 != null)
                                    {
                                        vm.FirmwareVersionCheck("2.1.7", "1", vm.Gen8ServerInfo1.SPSVersion, "System Requires V2 Upgrade! Please launch Remote Console to perform the update.");
                                    }
                                   



                                    //CHECK FOR ONE VIEW MANAGEMENT
                                    if (vm.ILORestOutput1.Contains("OneView") || vm.ILORestOutput1.Contains("One View") || vm.ILORestOutput1.Contains("ONEVIEW") || vm.ILORestOutput1.Contains("ONE VIEW"))
                                    {

                                        vm.WarningMessage1 = "System appears to be ONE VIEW Managed! Please Login to the Server Webpage, Select One View and Delete. Or you can use the the Wipe Embedded Flash button (Removes IProv).";

                                    }

                                    //Load Ultima URL
                                    if (vm.Gen8ServerInfo1 != null)
                                    {
                                        if (vm.Gen8ServerInfo1.Manufacturer != null && vm.Gen8ServerInfo1.Model != null)
                                            vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.Gen8ServerInfo1.Manufacturer.Replace("Inc.", "") + " " + vm.Gen8ServerInfo1.Model + " Server &go=Go";
                                   


                                    if (vm.Gen8ServerInfo1.NVRAMSpaceStatus == "Degraded" || vm.Gen8ServerInfo1.NVRAMSpaceStatus != "OK")
                                    {
                                        vm.WarningMessage1 = "iLO Self Test Error Detected";
                                    }
                                    }


                                    //foreach(var itm in vm.Gen8ServerInfo1.)

                                    //MessageBox.Show();



                                    break;
                                case "BiosIlo":

                                    vm.ProgressMessage1 = "Please wait...";
                                    vm.ProgressIsActive1 = true;
                                    vm.ProgressPercentage1 = 0;
                                    vm.ProgressVisibility1 = Visibility.Visible;


                                    //CANCEL SCRIPT
                                    if (vm.CancelScriptS1 == true)
                                    {
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                        return;
                                    }
                                    //END CANCEL SCRIPT

                                    vm.ProgressPercentage1 = 5;
                                    vm.ProgressMessage1 = "Powering Server Down!";
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot ForceOff --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    await PutTaskDelay(15000);


                                    //CANCEL SCRIPT
                                    if (vm.CancelScriptS1 == true)
                                    {
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                        return;
                                    }
                                    //END CANCEL SCRIPT


                                    //ILO BIOS DEFAULTS
                                    vm.ProgressMessage1 = "Resetting BIOS Defaults";
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "biosdefaults --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    await PutTaskDelay(20000);

                                    vm.ProgressPercentage1 = 25;
                                    if (vm.BIOSUpgradePath1 != String.Empty)
                                    {
                                        //BIOS Firmware update
                                        vm.ProgressMessage1 = "BIOS Flash Update";
                                        vm.ProgressPercentage1 = 25;
                                        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_BIOS_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                        await PutTaskDelay(40000);
                                    }


                                    //CANCEL SCRIPT
                                    if (vm.CancelScriptS1 == true)
                                    {
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                        return;
                                    }
                                    //END CANCEL SCRIPT


                                    vm.ProgressPercentage1 = 35;
                                    //Check if ILO FLASH FILE SELECTED
                                    if (vm.ILOUpgradePath1 != String.Empty)
                                    {
                                        //ilo  Firmware update
                                        vm.ProgressMessage1 = "iLO Firmware Update";
                                        // replace with loop on 
                                        vm.ProgressPercentage1 = 35;
                                        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                        await PutTaskDelay(80000);
                                    }


                                    //CANCEL SCRIPT
                                    if (vm.CancelScriptS1 == true)
                                    {
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                        return;
                                    }
                                    //END CANCEL SCRIPT


                                    vm.ProgressMessage1 = "Setting Server Name to 'NEW SERVER' & Asset Tag to 'NEW TAG";
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Set_Server_Name_Asset_Tag.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));

                                    //CANCEL SCRIPT
                                    if (vm.CancelScriptS1 == true)
                                    {
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                        return;
                                    }
                                    //END CANCEL SCRIPT

                                    vm.ProgressMessage1 = "Powering Server On!";
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot On --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //Time to wait for reboot
                                    if (vm.ServerType.Contains("Blade"))
                                    {

                                        await PutTaskDelay(180000);
                                    }
                                    else
                                    {
                                        await PutTaskDelay(180000);
                                    }

                                    //CANCEL SCRIPT
                                    if (vm.CancelScriptS1 == true)
                                    {
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                        return;
                                    }
                                    //END CANCEL SCRIPT


                                    vm.ProgressPercentage1 = 45;
                                    //APPLY ILO EVAL LICENSE
                                    vm.ProgressMessage1 = "Applying Trial iLO License";
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "ilolicense 332N6-VJMMM-MHTPD-L7XNR-29G8B  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    vm.ProgressPercentage1 = 55;

                                    //CANCEL SCRIPT
                                    if (vm.CancelScriptS1 == true)
                                    {
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                        return;
                                    }
                                    //END CANCEL SCRIPT


                                    vm.ProgressPercentage1 = 65;
                                    ////ILO SET SERVER NAME
                                    //vm.ProgressMessage1 = "Setting Server Name to 'NEW SERVER' & Asset Tag to 'NEW TAG";
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Set_Server_Name_Asset_Tag.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                    vm.ProgressPercentage1 = 75;


                                    //Time to wait for reboot
                                    if (vm.ServerType.Contains("Blade"))
                                    {
                                        await PutTaskDelay(260000);
                                    }
                                    else
                                    {
                                        await PutTaskDelay(220000);
                                    }


                                    vm.ProgressPercentage1 = 85;
                                    //vm.ProgressMessage1 = "Reapplying Trial iLO License";
                                    ////RE APPLY ILO EVAL LICENSE
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "ilolicense 332N6-VJMMM-MHTPD-L7XNR-29G8B  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));



                                    vm.ProgressMessage1 = "Extracting Json Objects";
                                    //GET ALL SYSTEM INFO NOW THE SCRIPTS HAVE RUN AND FIRMWARE UPDATES
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + vm.Server1IPv4 + "All.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    // await PutTaskDelay(5000);
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/managers/1/embeddedmedia -f" + vm.Server1IPv4 + "SDCARD.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                    //GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l " + vm.Server1IPv4 + "ServerHealth1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));






                                    vm.ProgressPercentage1 = 95;


                                    //vm.ProgressMessage1 = "Rebooting server";
                                    //////REBOOT
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                    //await PutTaskDelay(10000);

                                    //await PutTaskDelay(5000);

                                    //*******Read the Data from the JSON and XML Files pulled from the Server*********
                                    ReadJsonXMLObjects(ServerGen);

                                    //Stop Timer
                                    timer.Stop();
                                    timeTaken = timer.Elapsed;
                                    timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                    vm.ProgressPercentage1 = 100;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    vm.ProgressMessage1 = "Scripts completed successfully! Please check the Server Details and proceed to the next Slide! " + timetaken;




                                    //Launch Remote Console
                                    //StaticFunctions.RunRemoteConsoleCommand(@"" + vm.myDocs + @"\HPTOOLS\HP iLO Integrated Remote Console\HPLOCONS.exe", @"-addr " + vm.Server1IPv4 + ":443 -name Administrator -password " + vm.OverridePasswordS1.Trim() + " -lang en");



                                    //Clear temp update file
                                    //BIOS
                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin"))
                                    {
                                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin");
                                    }

                                    //ILO
                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.bin"))
                                    {
                                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.bin");
                                    }



                                    //CLEAR Flash File INFORMATION
                                    vm.BIOSUpgradePath1 = String.Empty;
                                    vm.ILOUpgradePath1 = String.Empty;



                                    break;
                            case "IloUpgradeOnly":
                                    if (vm.ILOUpgradePath1 != string.Empty)
                                    {
                                        vm.ProgressMessage1 = "Please wait...";
                                        vm.WarningMessage1 = "";
                                        vm.ProgressIsActive1 = true;
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressVisibility1 = Visibility.Visible;

                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT

                                        vm.ProgressPercentage1 = 50;
                                        //Check if ILO FLASH FILE SELECTED
                                        if (vm.ILOUpgradePath1 != String.Empty)
                                        {
                                            //ilo  Firmware update
                                            vm.ProgressMessage1 = "iLO Firmware Update";
                                            // replace with loop on 
                                            vm.ProgressPercentage1 = 80;
                                            await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                            await PutTaskDelay(80000);
                                        }


                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT


                                        vm.ProgressMessage1 = "Extracting Json Objects";
                                        //GET ALL SYSTEM INFO NOW THE SCRIPTS HAVE RUN AND FIRMWARE UPDATES
                                        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + vm.Server1IPv4 + "All.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                        //EXTRACT ALL PHYSICAL DISK INFO TO JSON  
                                        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --select HpSmartStorageDiskDrive. -f" + vm.Server1IPv4 + "Disks.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                        // await PutTaskDelay(5000);
                                        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/managers/1/embeddedmedia -f" + vm.Server1IPv4 + "SDCARD.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                        //GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l " + vm.Server1IPv4 + "ServerHealth1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));

                                        //*******Read the Data from the JSON and XML Files pulled from the Server*********
                                        ReadJsonXMLObjects(ServerGen);

                                        //Stop Timer
                                        timer.Stop();
                                        timeTaken = timer.Elapsed;
                                        timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                        vm.ProgressPercentage1 = 100;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts completed successfully! iLO now Upgraded to a Restful Compatible Version. " + timetaken;
                                       

                                        vm.ILOMinVersion1 = string.Empty;
                                        vm.ILOUpgradePath1 = string.Empty;

                                    }
                                    ////IF ILO REQUIRES MIN UPGRADE TO SUPPORT RESTFUL
                                    ////clear progress
                                    //vm.ProgressMessage1 = "";
                                    //vm.ProgressPercentage1 = 0;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    ////MessageBox.Show(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL");
                                    //vm.ProgressMessage1 = "Please wait...";
                                    //vm.ProgressIsActive1 = true;
                                    //vm.ProgressVisibility1 = Visibility.Visible;
                                    //vm.ProgressPercentage1 = 0;
                                    ////ILO SET SERVER NAME
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Set_Server_name.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                    //vm.ProgressPercentage1 = 15;
                                    //////APPLY ILO EVAL LICENSE
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "ilolicense 332N6-VJMMM-MHTPD-L7XNR-29G8B  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //vm.ProgressPercentage1 = 35;
                                    ////CLEARING SERVER LOGS
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "serverlogs --selectlog=AHS --clearlog  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //vm.ProgressPercentage1 = 55;
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "serverlogs --selectlog=IML --clearlog  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //vm.ProgressPercentage1 = 65;
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "serverlogs --selectlog=IEL --clearlog  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //vm.ProgressPercentage1 = 80;
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "serverlogs --selectlog=SA --clearlog  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //////EXTRACT FIRMWARE JSON
                                    ////await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/systems/1/firmwareinventory --expand -f firmware.json  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //////EXTRACT SYSTEM JSON
                                    ////await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/systems/1 --expand -f fullserverinfo.json  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));


                                    //vm.ProgressIsActive1 = false;
                                    //vm.ProgressPercentage1 = 100;
                                    //vm.ProgressMessage1 = "Scripts completed successfully!";
                                    //vm.ProgressVisibility1 = Visibility.Hidden;
                                    break;
                           
                            case "WipeScripts":

                                //if (vm.Gen8ServerInfo1 != null)//vm.Gen8ServerInfo1 != null ||
                                //{

                                //    //check if server info was required vm.Gen8ServerInfo.ChassisSerial != string.Empty || 
                                //    if (vm.Gen8ServerInfo1.ChassisSerial != string.Empty)
                                //    {
                                        //check if firmwares were selected
                                        //if (vm.ILOUpgradePath != String.Empty || vm.BIOSUpgradePath != String.Empty)
                                        //  {



                                        vm.ProgressMessage1 = "Please wait...";
                                        vm.ProgressIsActive1 = true;
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressVisibility1 = Visibility.Visible;

                                    vm.ProgressPercentage1 = 10;
                                    vm.ProgressMessage1 = "Resetting BIOS Defaults";
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "biosdefaults --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + " --reboot=ForceRestart", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    await PutTaskDelay(180000);


                                    //CANCEL SCRIPT
                                    if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                                return;
                                            }
                                            //END CANCEL SCRIPT

                                            vm.ProgressPercentage1 = 5;
                                        vm.ProgressMessage1 = "Powering Server Down!";
                                        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot ForceOff --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                        await PutTaskDelay(15000);


                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                                return;
                                            }
                                            //END CANCEL SCRIPT



                                      
                                        if (vm.BIOSUpgradePath1 != String.Empty)
                                        {
                                            //BIOS Firmware update
                                            vm.ProgressMessage1 = "BIOS Flash Update";
                                            vm.ProgressPercentage1 = 25;
                                            await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_BIOS_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                            await PutTaskDelay(40000);
                                        }


                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                                return;
                                            }
                                            //END CANCEL SCRIPT


                                            vm.ProgressPercentage1 = 35;
                                        //Check if ILO FLASH FILE SELECTED
                                        if (vm.ILOUpgradePath1 != String.Empty)
                                        {
                                            //ilo  Firmware update
                                            vm.ProgressMessage1 = "iLO Firmware Update";
                                            // replace with loop on 
                                            vm.ProgressPercentage1 = 35;
                                            await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                            await PutTaskDelay(80000);
                                        }


                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                                return;
                                            }
                                            //END CANCEL SCRIPT


                                            vm.ProgressMessage1 = "Setting Server Name to 'NEW SERVER' & Asset Tag to 'NEW TAG";
                                        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Set_Server_Name_Asset_Tag.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));

                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                                return;
                                            }
                                            //END CANCEL SCRIPT

                                            vm.ProgressMessage1 = "Powering Server On!";
                                        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot On --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                            //Time to wait for reboot
                                            if (vm.ServerType.Contains("Blade"))
                                            {

                                                await PutTaskDelay(200000);
                                            }
                                            else
                                            {
                                                await PutTaskDelay(200000);
                                            }

                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                                return;
                                            }
                                            //END CANCEL SCRIPT


                                            vm.ProgressPercentage1 = 45;
                                        //APPLY ILO EVAL LICENSE
                                        vm.ProgressMessage1 = "Applying Trial iLO License";
                                        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "ilolicense 332N6-VJMMM-MHTPD-L7XNR-29G8B  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                        vm.ProgressPercentage1 = 55;

                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                                vm.WarningMessage1 = "";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }
                                            //END CANCEL SCRIPT


                                            vm.ProgressPercentage1 = 65;
                                        ////ILO SET SERVER NAME
                                        //vm.ProgressMessage1 = "Setting Server Name to 'NEW SERVER' & Asset Tag to 'NEW TAG";
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Set_Server_Name_Asset_Tag.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                        vm.ProgressPercentage1 = 75;
                                       // await PutTaskDelay(15000);



                                        vm.ProgressPercentage1 = 85;
                                    //vm.ProgressMessage1 = "Reapplying Trial iLO License";
                                    ////RE APPLY ILO EVAL LICENSE
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "ilolicense 332N6-VJMMM-MHTPD-L7XNR-29G8B  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                    vm.ProgressMessage1 = "Server Inventory Running";

                                    vm.ProgressMessage1 = "Extracting Json Objects";
                                    //GET ALL SYSTEM INFO NOW THE SCRIPTS HAVE RUN AND FIRMWARE UPDATES
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + vm.Server1IPv4 + "All.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                    //EXTRACT ALL PHYSICAL DISK INFO TO JSON  
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --select HpSmartStorageDiskDrive. -f" + vm.Server1IPv4 + "Disks.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    // await PutTaskDelay(5000);
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/managers/1/embeddedmedia -f" + vm.Server1IPv4 + "SDCARD.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                   //GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l " + vm.Server1IPv4 + "ServerHealth1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));






                                        vm.ProgressPercentage1 = 95;


                                        //vm.ProgressMessage1 = "Rebooting server";
                                        //////REBOOT
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                        //await PutTaskDelay(10000);

                                        //await PutTaskDelay(5000);

                                        //*******Read the Data from the JSON and XML Files pulled from the Server*********
                                        ReadJsonXMLObjects(ServerGen);

                                            //Stop Timer
                                            timer.Stop();
                                            timeTaken = timer.Elapsed;
                                            timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                            vm.ProgressPercentage1 = 100;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts completed successfully! Please check the Server Details and proceed to the next Slide! " + timetaken;

                                      


                                        //Launch Remote Console
                                        //StaticFunctions.RunRemoteConsoleCommand(@"" + vm.myDocs + @"\HPTOOLS\HP iLO Integrated Remote Console\HPLOCONS.exe", @"-addr " + vm.Server1IPv4 + ":443 -name Administrator -password " + vm.OverridePasswordS1.Trim() + " -lang en");
                                     
                                        
                                        
                                        //Clear temp update file
                                        //BIOS
                                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin"))
                                        {
                                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin");
                                        //for test certificate
                                        vm.UpdatesRun += @" [BIOS] ";
                                        }

                                        //ILO
                                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.bin"))
                                        {
                                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.bin");
                                        //for test certificate
                                        vm.UpdatesRun += @" [ILO] ";
                                        }



                                        //CLEAR Flash File INFORMATION
                                        vm.BIOSUpgradePath1 = String.Empty;
                                        vm.ILOUpgradePath1 = String.Empty;



                                break;
                            case "FinalScripts":
                                if (vm.Gen8ServerInfo1 != null)
                                    if (vm.Gen8ServerInfo1.ChassisSerial != string.Empty)
                                    {
                                        //////EXTRACT FIRMWARE JSON
                                        vm.ProgressMessage1 = "Please wait...";
                                        vm.ProgressIsActive1 = true;
                                        vm.ProgressPercentage1 = 0;
                                        vm.MessageVisibility1 = Visibility.Visible;
                                        vm.ProgressVisibility1 = Visibility.Visible;
                                        vm.ProgressPercentage1 = 0;
                                      

                                        


                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                                vm.WarningMessage1 = "";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }
                                            //END CANCEL SCRIPT
                                            vm.ProgressMessage1 = "Resetting iLO to Factory Defaults";
                                            vm.ProgressPercentage1 = 30;
                                            //FOR ILO RESTFUL FACTORY DEFAULTS
                                            await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "factorydefaults --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                        vm.ProgressPercentage1 = 60;
                                        await PutTaskDelay(100000);
                                        vm.ProgressPercentage1 = 60;

                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                                vm.WarningMessage1 = "";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }
                                            //END CANCEL SCRIPT

                                            //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                            //EXTRACT ALL SYSTEM INFO TO JSON
                                            await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.Settings.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + vm.Server1IPv4 + "All.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                        vm.ProgressPercentage1 = 70;

                                        // await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --select fwswversioninventory. -f" + vm.ServerIPv4 + "all.json  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                        //GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l " + vm.Server1IPv4 + "ServerHealth1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                        vm.ProgressPercentage1 = 80;

                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                                vm.WarningMessage1 = "";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }
                                            //END CANCEL SCRIPT


                                            //Read the Data from the JSON and XML Files pulled from the Server
                                            ReadJsonXMLObjects(ServerGen);
                                        //await PutTaskDelay(10000);

                                        ////CLEARING SERVER LOGS
                                        //vm.ProgressMessage = "Clearing Logs";
                                        ////single command
                                        ////await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "serverlogs --selectlog=AHS --selectlog=IML --selectlog=IEL --selectlog=SA --clearlog  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "serverlogs --selectlog=AHS --clearlog  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                        //vm.ProgressPercentage = 62;
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "serverlogs --selectlog=IML --clearlog  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                        //vm.ProgressPercentage = 65;
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "serverlogs --selectlog=IEL --clearlog  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                        //vm.ProgressPercentage = 80;
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "serverlogs --selectlog=SA --clearlog  --url " + vm.ServerIPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                        //await PutTaskDelay(10000);
                                        vm.ProgressMessage1 = "Powering Server Down";
                                        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot ForceOff --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                            //// MessageBox.Show("hmmm");

                                            //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Get_Global.xml -l GlobalDetails.xml -s " + vm.ServerIPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));


                                            //CLEAR THE DATA FILES THESE WILL BE REPRODUCED LATER IN THIS SCRIPT
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml");
                                            }
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml");
                                            }
                                            //CLEAR RESTFUL INTERFACE JSON FILE
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "All.json"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "All.json");
                                            }

                                            //CLEAR RESTFUL INTERFACE JSON FILE
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Disks.json"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Disks.json");
                                            }
                                            //CLEAR SD CARD INFO
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "SDCARD.json"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "SDCARD.json");
                                            }
                                            //CLEAR ILO FIRMWARE INFO
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Firmware.json"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Firmware.json");
                                            }


                                            //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Set_Host_PowerOff.xml -s " + vm.ServerIPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));

                                            vm.ProgressPercentage1 = 100;

                                            //Stop Timer
                                            timer.Stop();
                                            timeTaken = timer.Elapsed;
                                            timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                            vm.ProgressPercentage1 = 100;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts completed successfully! " + timetaken;

                                         
                                            vm.Server1TabHeader = "Server1";

                                            //Set Has System Erase Been Run Flag
                                            vm.WasFactoryReset = "Yes";

                                        }
                                    else
                                    {

                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.MessageVisibility1 = Visibility.Visible;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressPercentage1 = 0;
                                        vm.WarningMessage1 = "Please retrieve server information first";

                                        }

                                break;
                          }


                            //CHECK FOR SD CARD
                            if (vm.Gen8ServerInfo1 != null)
                                if (vm.Gen8ServerInfo1.SDCardInserted == "Absent" || vm.Gen8ServerInfo1.SDCardInserted == "" || String.IsNullOrEmpty(vm.Gen8ServerInfo1.SDCardInserted))
                                {


                                }
                                else
                                {
                                    //vm.ProgressMessage1 = "Powering Server Down!";
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot ForceOff --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //MessageBox.Show("SD CARD Detected! GDPR Risk, Please remove from the system.");
                                    //EXTRACT ALL SYSTEM INFO TO JSON MISSING fwswversioninventory.
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,ComputerSystem.,Manager. -f" + vm.Server1IPv4 + "All.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    //Read the Data from the JSON and XML Files pulled from the Server
                                    ReadJsonXMLObjects(ServerGen);


                                    vm.MessageVisibility1 = Visibility.Visible;
                                    vm.ProgressMessage1 = "";
                                    vm.GDPRRiskDetected = "Yes, Warning was issued.";
                                    vm.WarningMessage1 = "SD CARD Detected! GDPR Risk, Please remove from the system, unless this is a requested server configuration.";
                                }

                        } //MIN ILO FIRMWARE CHECK
                    else
                    {
                           // MessageBox.Show("Came Here");
                            //Attempt to upgrade ilo
                            if(vm.ILOUpgradePath1 != string.Empty)
                            {
                                vm.ProgressMessage1 = "Please wait...";
                                vm.WarningMessage1 = "";
                                vm.ProgressIsActive1 = true;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressVisibility1 = Visibility.Visible;

                                vm.ProgressPercentage1 = 50;
                                //Check if ILO FLASH FILE SELECTED
                                if (vm.ILOUpgradePath1 != String.Empty)
                                {
                                    //ilo  Firmware update
                                    vm.ProgressMessage1 = "iLO Firmware Update";
                                    // replace with loop on 
                                    vm.ProgressPercentage1 = 80;
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                    await PutTaskDelay(80000);
                                }


                                vm.ProgressMessage1 = "Extracting Json Objects";
                                //GET ALL SYSTEM INFO NOW THE SCRIPTS HAVE RUN AND FIRMWARE UPDATES
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + vm.Server1IPv4 + "All.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                //EXTRACT ALL PHYSICAL DISK INFO TO JSON  
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --select HpSmartStorageDiskDrive. -f" + vm.Server1IPv4 + "Disks.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                // await PutTaskDelay(5000);
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/managers/1/embeddedmedia -f" + vm.Server1IPv4 + "SDCARD.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                //GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l " + vm.Server1IPv4 + "ServerHealth1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));

                                //*******Read the Data from the JSON and XML Files pulled from the Server*********
                                ReadJsonXMLObjects(ServerGen);


                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 100;
                                vm.ProgressMessage1 = "Scripts completed successfully! iLO now Upgraded to a Restful Compatible Version";

                                vm.ILOMinVersion1 = string.Empty;
                                vm.ILOUpgradePath1 = string.Empty;
                            }

                            ////IF ILO REQUIRES MIN UPGRADE TO SUPPORT RESTFUL
                            ////clear progress
                            //vm.ProgressMessage1 = "";
                            //vm.ProgressPercentage1 = 0;
                            vm.ProgressIsActive1 = false;
                            vm.ProgressVisibility1 = Visibility.Hidden;
                        }


                        //if(vm.ILORestOutput1.Contains("Unable to locate instance for"))
                        //{
                        //    vm.WarningMessage1 = "Possible issue with Restful Api or iLO Self Test detected. Please follow these steps. 1) Wipe Embedded Flash 2) Upgrade iLO. If issues persist, a MainBoard replacement maybe required.";
                        //}


                    }//end if to check server ip is not empty
                    else
                    {
                        //reset progress ring to hidden
                        vm.ProgressVisibility1 = Visibility.Hidden;
                        vm.MessageVisibility1 = Visibility.Visible;
                        vm.ProgressIsActive1 = false;
                        vm.ProgressPercentage1 = 0;
                        vm.WarningMessage1 = "Cannot contact server with Ip: " + vm.Server1IPv4 + ", Please check the Server has power.";
                        vm.IsServerThere1 = "No";
                    }

                }// check if server there
                else
                {
                    //vm.ProgressMessage = "Cannot contact server with Ip: " + vm.ServerIPv4 + ", Exited Function";
                    vm.ProgressMessage1 = "Please Enter A Server IP in the top box!";
                }

            }//end if to check vm is null



            //WRITE LOG
            if (Directory.Exists(@"" + vm.myDocs + @"\Nebula Logs"))
            {


                //Write out log
                if (vm.Gen8ServerInfo1 != null)
                {
                    if (vm.Gen8ServerInfo1.ChassisSerial!= null)
                        File.WriteAllText(@"" + vm.myDocs + @"\Nebula Logs\Nebula_Server_Log_" + vm.Gen8ServerInfo1.ChassisSerial.Trim() + " " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".txt", SyncLog);
                }

            }





                //set colours
                //if (vm.WarningMessage1 != "")
                //{
                //    vm.TabColorS1 = Brushes.Red;
                //}
                //else
                //{
                //    vm.TabColorS1 = Brushes.Green;
                //}

                //set colours
                if (string.IsNullOrEmpty(vm.WarningMessage1))
                {
                    if (ServerTab != null)
                    {
                        if (string.IsNullOrEmpty(vm.Server1IPv4))
                        {
                            ServerTab.Foreground = Brushes.MediumPurple;
                        }
                        else
                        {
                            ServerTab.Foreground = Brushes.Green;
                        }
                  
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;


                      

                        if (!string.IsNullOrEmpty(vm.WarningMessage1))
                        {
                            ServerTab.Foreground = Brushes.Red;
                        }

                    }
                }

            }
            catch (Exception ex)
            {

                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (string.IsNullOrEmpty(vm.WarningMessage1))
                {
                    if (ServerTab != null)
                    {
                        if (string.IsNullOrEmpty(vm.Server1IPv4))
                        {
                            ServerTab.Foreground = Brushes.MediumPurple;
                        }
                        else
                        {
                            ServerTab.Foreground = Brushes.Green;
                        }

                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;




                        if (!string.IsNullOrEmpty(vm.WarningMessage1))
                        {
                            ServerTab.Foreground = Brushes.Red;
                        }

                    }
                }

                return;
            }



        }//end execute




        public void ReadJsonXMLObjects(string ServerGen)
        {
            //try
            //{

          

            // GET THE SERVER INFORMATION AGAIN
            string ServerInfoJson;



            //CHECK BOTH SERVER INFORMATION FILES EXIST
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "All.json") && File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml"))
            {
                //Full Info
                using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "All.json"))
                {


                        string streamString = "";

                        string HeaderString = "";
                        string PathString = "";

                        // Use while not null pattern in while loop.
                        string line;
                        while ((line = r.ReadLine()) != null)
                        {
                            // Insert logic here.
                            // ... The "line" variable is a line in the file.
                            // ... Add it to our List.



                            if (line.Contains("@odata") || line.Contains("href"))
                            {

                            }
                            else
                            {

                                //Gen8,9
                                //BIOS
                                if (line.Contains("HpBios") || line.Contains("/rest/v1/Systems/1/Bios"))
                                {
                                    HeaderString = "HpBios" + Regex.Match(line, @"HpBios(.+?):").Groups[1].Value.Replace("\"", "").Trim();
                                    PathString = @"/rest/v1/Systems/1/Bios" + Regex.Match(line, @"/rest/v1/Systems/1/Bios(.+?):").Groups[1].Value.Replace("\"", "").Trim();
                                    line = line.Replace(HeaderString, "Bios").Replace(PathString, "Path");
                                }
                                //CHASSIS
                                if (line.Contains("Chassis") || line.Contains("/rest/v1/Chassis/1"))
                                {
                                    HeaderString = "Chassis" + Regex.Match(line, @"Chassis(.+?):").Groups[1].Value.Replace("\"", "").Trim();
                                    PathString = @"/rest/v1/Chassis/1" + Regex.Match(line, @"/rest/v1/Chassis/1(.+?):").Groups[1].Value.Replace("\"", "").Trim();
                                    line = line.Replace(HeaderString, "Chassis").Replace(PathString, "Path");
                                }
                                //FIRMWARE
                                if (line.Contains("FwSwVersionInventory") || line.Contains("/rest/v1/Systems/1/FirmwareInventory"))
                                {
                                    HeaderString = "FwSwVersionInventory" + Regex.Match(line, @"FwSwVersionInventory(.+?):").Groups[1].Value.Replace("\"", "").Trim();
                                    PathString = @"/rest/v1/Systems/1/FirmwareInventory" + Regex.Match(line, @"/rest/v1/Systems/1/FirmwareInventory(.+?):").Groups[1].Value.Replace("\"", "").Trim();
                                    line = line.Replace(HeaderString, "Firmware").Replace(PathString, "Path");
                                }
                                //SYSTEM
                                if (line.Contains("ComputerSystem") || line.Contains("/rest/v1/Systems/1"))
                                {
                                    HeaderString = "ComputerSystem" + Regex.Match(line, @"ComputerSystem(.+?):").Groups[1].Value.Replace("\"", "").Trim();
                                    PathString = @"/rest/v1/Systems/1" + Regex.Match(line, @"/rest/v1/Systems/1(.+?):").Groups[1].Value.Replace("\"", "").Trim();
                                    line = line.Replace(HeaderString, "ComputerSystem").Replace(PathString, "Path");
                                }
                                //Manager
                                if (line.Contains("Manager") || line.Contains("/rest/v1/Systems/1")|| line.Contains("HpiLOLicense") || line.Contains("/rest/v1/Systems/1"))
                                {
                                    //License
                                    HeaderString = "HpiLOLicense" + Regex.Match(line, @"HpiLOLicense(.+?):").Groups[1].Value.Replace("\"", "").Trim();
                                    PathString = @"/rest/v1/Managers/1" + Regex.Match(line, @"/rest/v1/Managers/1(.+?):").Groups[1].Value.Replace("\"", "").Trim();
                                    line = line.Replace(HeaderString, "Manager").Replace(PathString, "Path");
                                    //Managers
                                    HeaderString = "Manager" + Regex.Match(line, @"Manager(.+?):").Groups[1].Value.Replace("\"", "").Trim();
                                    PathString = @"/rest/v1/Managers/1" + Regex.Match(line, @"/rest/v1/Managers/1(.+?):").Groups[1].Value.Replace("\"", "").Trim();
                                    line = line.Replace(HeaderString, "Manager").Replace(PathString, "Path");

                                }




                            }


                           

                            streamString += line;


                        }


                        ServerInfoJson = streamString;


                    }



                 

                    //MessageBox.Show(ServerInfoJson);
                   // Console.WriteLine(ServerInfoJson);
                //create new json parser
                JsonParser jPar = new JsonParser(vm);

                vm.Gen8ServerInfo1 = jPar.ReadFullServerInfoJsonGen8(ServerInfoJson,vm.Server1IPv4,"S1");


                if (vm.Gen8ServerInfo1 != null)
                {
                    //set the tab header
                    //vm.Server1TabHeader = "Server1 [" + vm.Server1IPv4 + "] ILO" + vm.Gen8ServerInfo1.ChassisSerial;
                    vm.Server1TabHeader = vm.Server1TabHeader.Replace(" (ILO" + vm.Gen8ServerInfo1.ChassisSerial + ")", "");
                    vm.Server1TabHeader = vm.Server1TabHeader + " (ILO" + vm.Gen8ServerInfo1.ChassisSerial + ")";

                    //Set Tab Header
                    ServerTab.Header = ServerTab.Header.ToString().Replace(" (ILO" + vm.Gen8ServerInfo1.ChassisSerial + ")", "");
                    ServerTab.Header = ServerTab.Header + " (ILO" + vm.Gen8ServerInfo1.ChassisSerial + ")";



                    // MessageBox.Show(vm.Gen8ServerInfo1.ChassisSerial.Substring(0, 2));
                    //Server Region Check
                    if (vm.Gen8ServerInfo1.ChassisSerial != null && vm.Gen8ServerInfo1.ChassisSerial != String.Empty)
                        if (vm.Gen8ServerInfo1.ChassisSerial.Substring(0, 2).Contains("CZ"))
                        {
                            //MessageBox.Show(vm.Gen8ServerInfo1.ChassisSerial);

                            //vm.Gen8ServerInfo1.OutOfRegionMessage = "Out of Region!";
                        }
                        else
                        {
                            if (vm.Gen8ServerInfo1.ChassisSerial == String.Empty)
                            {
                            }
                            else
                            {
                                vm.Gen8ServerInfo1.OutOfRegionMessage = "Out of Region!";
                            }
                            // vm.Gen8ServerInfo1.OutOfRegionMessage = "In Region!";
                        }

                }


                //check which gen and run the correct item
                switch (ServerGen)
                {
                    case "Gen8":
                        //GEN8
                        //read in info from server and pass to the .net object
                      //  vm.Gen8ServerInfo1 = jPar.ReadFullServerInfoJsonGen8(ServerInfoJson);
                        break;
                    case "Gen9":
                        //GEN9
                        //read in info from server and pass to the .net object
                        //vm.Gen9ServerInfo = jPar.ReadFullServerInfoJsonGen9(ServerInfoJson);
                        break;
                    case "Gen10":
                        //GEN10
                        //read in info from server and pass to the .net object
                        // vm.Gen10ServerInfo = jPar.ReadFullServerInfoJsonGen10(ServerInfoJson);
                        break;
                }



            }

            //}
            //catch (Exception ex)
            //{

            //    Console.WriteLine(ex.Message);
            //}

        }










        public async void sendAsyncPingPacket(string hostToPing)
        {
            try
            {
                // int timeout = 5000;
                // AutoResetEvent waiter = new AutoResetEvent(false);
                Ping pingPacket = new Ping();
                //ping completion event reaised
                pingPacket.PingCompleted += new PingCompletedEventHandler(PingCompletedCallback);
                string data = "Ping test check";
                byte[] byteBuffer = Encoding.ASCII.GetBytes(data);
                PingOptions pingOptions = new PingOptions(64, true);

                await pingPacket.SendPingAsync(hostToPing, 3000);
                //pingPacket.Send(hostToPing, 3000);

            }
            catch (PingException pe)
            {
                string exception = pe.Message;
                //IPResults.Add("INVALID IP ADDRESS FOUND");
            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                //IPResults.Add("Exceptin " + ex.Message);
            }

        }


        private void PingCompletedCallback(object sender, PingCompletedEventArgs e)
        {
            try
            {
                if (e.Cancelled)
                {

                }

                // If an error occurred, display the exception to the user.
                if (e.Error != null)
                {

                }

                PingReply reply = e.Reply;

                if (reply == null)
                {
                    vm.IsServerThere1 = "No";
                    return;
                }
                else if (reply.Status == IPStatus.Success)
                {
                    vm.IsServerThere1 = "Yes";
                    return;

                }
            }
            catch (PingException pe)
            {
                Console.WriteLine(pe.Message);
                //IPResults.Add("INVALID IP ADDRESS");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                // MessageBox.Show("Came Here");
                //IPResults.Add("Exception " + ex.Message);
            }
        }


        public void ClearXMLJSONFiles()
        {
            //CLEAR THE DATA FILES THESE WILL BE REPRODUCED LATER IN THIS SCRIPT
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml");
            }
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml");
            }
            //CLEAR RESTFUL INTERFACE JSON FILE
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "All.json"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "All.json");
            }

            //CLEAR RESTFUL INTERFACE DISKS JSON FILE
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Disks.json"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Disks.json");
            }
            //CLEAR SD CARD INFO
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "SDCARD.json"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "SDCARD.json");
            }
            //CLEAR ILO FIRMWARE INFO
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Firmware.json"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Firmware.json");
            }
            //CLEAR RESTFUL INTERFACE JSON FILE
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "FWLevel1.xml"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "FWLevel1.xml");
            }
        }


        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

    }
}


