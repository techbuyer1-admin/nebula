﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class DPExportCSVCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public DPExportCSVCommand(DriveTestViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            //Get array of objects
            var values = (object[])parameter;
            if (values != null)
            {
                //Split array into specific types
                var lvValue = (ListView)values[0];
                if (lvValue != null)
                {
                    if (vm.PartCodesTotal > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }


            return false;
        }

        public void Execute(object parameter)
        {

            //Get array of objects
            var values = (object[])parameter;
            //Split array into specific types
            var lvValue = (ListView)values[0];
            var sourceValue = (string)values[1];
            List<NetsuiteInventory> pcList = new List<NetsuiteInventory>();

         
            switch (lvValue.Name)
            {
                case "ImportLV":
                    if (lvValue != null)
                    {
                        //MessageBox.Show("IMPORT LV");

                        if (lvValue.Items.Count > 0)
                        {
                            foreach (NetsuiteInventory ns in lvValue.Items)
                            {
                                //Check if new part code export
                                if (sourceValue == "ExportNewPartCodeCSV")
                                {
                                    //Only export new part codes
                                    if (ns.NewPartCodeRequired == true)
                                    {

                                        //Add to local list
                                        pcList.Add(ns);

                                    }
                                }
                                else
                                {
                                    //Only export part codes that are not new
                                    if (ns.NewPartCodeRequired == false)
                                    {
                                        //Only add items that are not a new part code
                                        //Add to local list
                                        pcList.Add(ns);

                                    }
                                }
                                
                               
                            }

                        }


                    }
                    break;

                case "ProcessedLV":
                    if (lvValue != null)
                    {
                        if (lvValue.Items.Count > 0)
                        {
                            foreach (NetsuiteInventory ns in lvValue.Items)
                            {
                                vm.ProcessedTotal += Convert.ToInt32(ns.POQuantity);
                            }

                        }


                    }
                    break;
            }


            if (sourceValue == "ExportNewPartCodeCSV")
            {
                //Write to CSV
                StaticFunctions.CreateAppendPartCodeCSV(pcList, vm.PONumber, "");
            }
            else
            {
                //Write to CSV Create Netsuite 
                StaticFunctions.CreateNetsuiteImportCSV(pcList, vm.PONumber, "");


            }


           


           

        }
    }
}
