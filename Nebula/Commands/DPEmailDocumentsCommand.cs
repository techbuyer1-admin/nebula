﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class DPEmailDocumentsCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;


        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public DPEmailDocumentsCommand(DriveTestViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            //Get array of objects 
            var values =  (object[])parameter;
          

            if (values != null)
            {

                var lvValue = (ListView)values[0];

            if (lvValue != null)
            {
                if (lvValue.SelectedItems.Count > 0)
                {
                   
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
            }
            else
            {
                return false;
            }

        }

        public void Execute(object parameter)
        {

            //Get array of objects 
            var values = (object[])parameter;
            var lvValue = (ListView)values[0];

            EmailClass email = new EmailClass(vm);
            string[] files = new string[4];

            try
            {
                switch (lvValue.Name)
                {
                    case "ProcessedLV":
                      
                            //EmailClass email = new EmailClass();
                            ////email.ZipFolderAndSendMail();
                            //string[] files = new string[0];
                            //files[0] = vm.TestResultsLVSelectedItem.ReportPath;
                            //email.AttachFilesAndSendMail(files, vm.TestResultsLVSelectedItem.SerialNumber);
                       

                        break;

                    case "TestResultsLV":
                    //MessageBox.Show(vm.TestResultsLVSelectedItem.ReportPath);

                    if (vm.TestResultsLVSelectedItem.ReportPath != null)
                    {
                            //email = new EmailClass();
                            ////email.ZipFolderAndSendMail();
                            //files[0] = vm.TestResultsLVSelectedItem.ReportPath;

                            ////loop through PO and select fails
                            //if(vm.TestResultsLVSelectedItem.PONumber != null)
                            //{
                            //    //  string HTMLBody = "\r\r\r Here is a list of Failures from " + vm.TestResultsLVSelectedItem.PONumber + "\r\r";

                                
                            //    //Work Out Cost
                            //    decimal TotalCost = 0.00M;
                            //    int TotalCount = 0;




                            //    //Create HTML       
                            //    string outlookBody = "<!DOCTYPE html>" +
                            //        "<html>" +
                            //        "<head></head>" +
                            //        "<style>" +
                            //        "body {font-size:12px;}" +
                            //        "table { font-family: Arial, Helvetica, sans-serif; width:1200px; text-align: center;}" +
                            //        "table, caption, th, td{border: 0px solid White; text-align: center;}" +
                            //        "th { width:200px padding: 0px; text-align: center;}" +
                            //        "</style><body>" +
                            //        "<p style='font-size:14px;'>Hi,&nbsp;</p><p style='font-size:14px;'>Here is a list of Failures from " + vm.TestResultsLVSelectedItem.PONumber + "&nbsp;</p>" +
                                  
                            //        //Containing Table
                            //        //"<table style='margin-top:20px border: 0px solid black;'>" +
                            //        "<table style='margin-top:20px border: 0px solid black;'>" +
                            //        //"<caption style=' color:#0099ff; background-color:white; ' >Drive Test</caption>" +
                            //        "<tr style=' border:1px solid black; color:black; background-color:#c5ccd3;'>" +
                            //        "<th><b>Part Number</b></th>" +
                            //        "<th><b>Quantity</b></th>" +
                            //        "<th><b>Cost Each</b></th>" +
                            //        "<th><b>Reason For Fail</b></th>" +
                            //        "<th><b>PO</b></th>" +
                            //        "<th><b>Initial</b></th>" +
                            //        "<th><b>Drive Serial</b></th>" +
                            //        "<th><b>HP Serial</b></th>" +
                            //        "<th><b>ATF</b></th>" +
                            //        "<th><b>7 Digit</b></th>" +
                            //        "</tr>";


                            //    foreach (NetsuiteInventory nsi in vm.TestedDriveCollection)
                            //    {
                            //        //MessageBox.Show(nsi.ReportPath);
                            //        if (nsi.DriveResult == "FAIL")
                            //        {
                            //            //Add HTML TABLE

                            //            //Add HTML Elements
                            //            outlookBody += "<tr>" +
                            //            "<td  style='height:20px;'> " + nsi.ProductCode + "</td>" +
                            //            "<td  style='height:20px;'>" + nsi.Quantity + "</td>" +
                            //            "<td  style='height:20px;'> " + nsi.POLineUnitPrice + "</td>" +
                            //            "<td  style='height:20px;'> " + nsi.DriveFailedReason + " " + nsi.DriveFailedReasonSpecify + "</td>" +
                            //            "<td  style='height:20px;'>" + nsi.SerialNumber + "</td>" +
                            //            "<td  style='height:20px;'>" + nsi.HPSerialNumber + "</td>" +
                            //            "<td  style='height:20px;'>" + nsi.PONumber + "</td>" +
                            //            "<td  style='height:20px;'>" + nsi.TestedByOperative + "</td>" +
                            //            "<td  style='height:20px;'>" + nsi.ATF + "</td>" +
                            //            "<td  style='height:20px;'>" + nsi.SevenDigit + "</td>" +
                            //            //"<td  style='height:20px; background-color:#ff6347;'> " + nsi.DriveResult + "</td>" +
                            //            "</tr>";

                            //            //"<td  style=' color:black; background-color:#ff6347;'> " + element.Discrepancy + "</td>" +
                            //            //"<td  style='height:20px;'> " + element.Comments + "</td></tr>";

                            //            TotalCost += Convert.ToDecimal(nsi.POLineUnitPrice);
                            //            TotalCount += 1;
                                        
                            //        }
                            //    }
                               
                            // //Close the discrepancy table in Email Body
                            // outlookBody += "</table>" +
                            //                "</table>" +
                            //                "<p style='font-size:18px; background-color:#ff6347;'>Total Failures = " + TotalCount + "&nbsp;</p>" +
                            //                "<p style='font-size:18px; background-color:#ff6347;'>Total Cost = £" + TotalCost + "&nbsp;</p>" +
                            //                "</div>" +
                            //                "</body>" +
                            //                "</html>";
                            //    //Close HTML


                            //    //MessageBox.Show(files[0]);
                            //    //email.AttachFilesAndSendMail(files, vm.TestResultsLVSelectedItem.SerialNumber);
                            //    email.DrivetestSendMailWithOutlook("Drive Failures " + vm.TestResultsLVSelectedItem.PONumber, outlookBody, "", null, EmailClass.MailSendType.ShowModal);

                            //}
                         

                    }
                    break;
                    case "SearchResultsLV":
                        if (vm.SearchResultsLVSelectedItem.ReportPath != null)
                        {
                            email = new EmailClass(vm);
                            //email.ZipFolderAndSendMail();
                            files[0] = vm.SearchResultsLVSelectedItem.ReportPath;
                            email.AttachFilesAndSendMail(files, vm.SearchResultsLVSelectedItem.SerialNumber);
                        }

                        break;
                }



            }
            catch (Exception ex)
            {

               MessageBox.Show(ex.Message);
            }


}
    }
}
