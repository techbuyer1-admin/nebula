﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class IPScannerSearchCommand : ICommand
    {

        //declare viewmodel
       GoodsInOutViewModel vm = null;

        //use helper to get control
        //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public IPScannerSearchCommand(GoodsInOutViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //if (lv != null)
            //{
            //    if (lv.SelectedItems.Count > 0)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    return true;
            //}
            return true;
        }

        public void Execute(object parameter)
        {

            var values = (object)parameter;

            // MessageBox.Show(values.ToString());

            switch (values.ToString())
            {
                case "Search":

                    // Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new Action(() => ScanIPRange("")));

                    //192.168.101.248:443  GEN8
                    //192.168.101.34   GEN9
                    //await System.Threading.Tasks.Task.Run(() => StaticFunctions.ReadTxtFile(""));
                    //StaticFunctions.ReadTxtFile("");
                    //scan range
                    vm.scanLiveHosts(vm.StartingIP, vm.EndingIP);


                    //Dictionary<String, bool> dictionary = new Dictionary<String, bool>();
                    //dictionary.Add("192.168.101.1", false);
                    //dictionary.Add("192.168.101.2", false);
                    //dictionary.Add("192.168.101.3", false);
                    //dictionary.Add("192.168.101.4", false);
                    //dictionary.Add("192.168.101.5", false);
                    //dictionary.Add("192.168.101.6", false);
                    //dictionary.Add("192.168.101.7", false);
                    //dictionary.Add("192.168.101.8", false);
                    //dictionary.Add("192.168.101.9", false);
                    //dictionary.Add("192.168.101.10", false);
                    //dictionary.Add("192.168.101.11", false);
                    //dictionary.Add("192.168.101.12", false);
                    //dictionary.Add("192.168.101.13", false);
                    //dictionary.Add("192.168.101.14", false);
                    //dictionary.Add("192.168.101.15", false);
                    //dictionary.Add("192.168.101.16", false);
                    //dictionary.Add("192.168.101.17", false);
                    //dictionary.Add("192.168.101.18", false);
                    //dictionary.Add("192.168.101.19", false);
                    //dictionary.Add("192.168.101.20", false);
                    //dictionary.Add("192.168.101.21", false);
                    //dictionary.Add("192.168.101.82", false);

                    //Dictionary<string, bool> pingsReturn = await vm.PingListAsync(dictionary, 3);

                    //foreach(var result in pingsReturn)
                    //{
                    //    MessageBox.Show(result.Key + " " + result.Value.ToString());
                    //}
                    //await System.Threading.Tasks.Task.Run(() => vm.scanLiveHosts(vm.StartingIP, vm.EndingIP));
                    //PING A SINGLE ITEM
                    //vm.sendAsyncPingPacket("192.168.101.34");

                    break;
                case "Clear":
                    vm.IPResults.Clear();

                    break;

            }
            // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //TextBox tbBrand = Helper.GetDescendantFromName(Application.Current.MainWindow, "BrandTXT") as TextBox;
            //TextBox tbCli = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepCliCommandTXT") as TextBox;

            //IPHostEntry ip = Dns.GetHostByName(Dns.GetHostName());
            //ipFrom.Text = ip.AddressList[0].ToString();
            //ipTo.Text = ip.AddressList[0].ToString();

            //Clear the controls on the page
            //vm.ClearAddProduct();
            //give focus back to the Step description box
            //tbBrand.Focus();

          
        }

        //public void ScanIPRange(string hostname)
        //{
        //    try
        //    {
        //        IPAddress from = IPAddress.Parse(vm.StartingIP);
        //        IPAddress to = IPAddress.Parse(vm.EndingIP);
        //    }
        //    catch (FormatException fe)
        //    {
        //        MessageBox.Show(fe.Message);
        //        return;
        //    }


        //    int lastF = vm.StartingIP.LastIndexOf(".");
        //    int lastT = vm.EndingIP.LastIndexOf(".");
        //    string frm = vm.StartingIP.Substring(lastF + 1);
        //    string tto = vm.EndingIP.Substring(lastT + 1);
        //    int result = 0;
        //    System.Diagnostics.Debug.WriteLine(frm + " " + tto);



        //    for (int i = int.Parse(frm); i <= int.Parse(tto); i++)
        //    {
        //        try
        //        {
        //            string address = vm.EndingIP.Substring(0, lastT + 1);
        //            // MessageBox.Show(address);
        //            System.Diagnostics.Debug.WriteLine(vm.EndingIP.Substring(0, lastT + 1) + i);
        //            IPHostEntry ip = Dns.GetHostEntry(address + i);
        //            //IPAddress hostIPAddress = IPAddress.Parse(address + i);




        //            // IPHostEntry ip = Dns.GetHostByAddress(address + i);   //GetHostByName(Dns.GetHostName());
        //            //ipFrom.Text = ip.AddressList[0].ToString();
        //            //ipTo.Text = ip.AddressList[0].ToString();

        //           // vm.IPResults.Add(ip.AddressList[1].ToString());
        //            result += 1;
        //        }
        //        catch (SocketException se)
        //        {
        //            //MessageBox.Show(se.Message);
        //        }
        //        catch (Exception ex)
        //        {
        //            Console.WriteLine(ex.Message);
        //        }
        //    }//end for
        //}

        public static string DoGetHostEntry(string hostname)
        {
            IPHostEntry host = Dns.GetHostEntry("HGT-WS180");
            string returnData = "";
            //Console.WriteLine($"GetHostEntry({hostname}) returns:");

            foreach (IPAddress address in host.AddressList)
            {
                //Console.WriteLine($"    {address}");
                //vm.IPResults.Add(address.ToString());
                returnData = address.ToString();
            }

            return returnData;
        }




    }
}


