﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class DPImportCSVCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;

        //SQL Lite DB CONNECTION STRING LINK
        public string cs = "";

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public DPImportCSVCommand(DriveTestViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            //Get array of objects 
            var values = (object[])parameter;


            if (values != null)
            {

                //Split array into specific types
                var lvImport = (ListView)values[0];
                var poTxt = (TextBox)values[1];

                if (poTxt.Text != "")
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
            //return true;
        }

        public void Execute(object parameter)
        {

            try
            {

          

            //Get array of objects 
            //var potxtValue = (TextBox)(object)parameter;
            //Get array of objects
            var values = (object[])parameter;
            //Split array into specific types
            var lvImport = (ListView)values[0];
            var poTxt = (TextBox)values[1];

            //Clear the messages
            vm.ImportMessage = "";
            vm.ProcessedMessage = "";
            //Zero Counts
            vm.POTotal = 0;
            //Total from raw csv
            vm.POImportTotal = 0;
            //Reset the part code required total
            vm.PartCodesTotal = 0;
            //Disable List from manual selection
            // vm.ImportListEnabled = false;
            //Create New
            vm.NetsuiteComparisonCollection = new ObservableCollection<NetsuiteInventory>();

            //Clear All Collections
            vm.NetsuiteOriginalImportCollection.Clear();
            //Add to Comparison collection for original imported state
            vm.NetsuiteComparisonCollection.Clear();
            //Add to visible collection
            vm.NetsuiteImportCollection.Clear();
            //Add to visible collection
            vm.NetsuiteScannedCollection.Clear();
            //Add to visible collection
            vm.ScannedSerialCollection.Clear();

            //Check for presence of DriveTest folder
            if (Directory.Exists(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest"))
            {
               
            }
            else
            {
                //Create Sub Folder
                Directory.CreateDirectory(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest");
            }




            //Check if PO Exists in DB
            if (CheckPONumber() == true)
            {
                return;
            }

              

            if(Directory.Exists(@"" + StaticFunctions.UncPathToUse + @"Nebula\DriveTest\" + vm.PONumber))
            {
                vm.ImportMessage = "Partial PO Detected, please use the Load Partial PO button.";
                vm.ImportMessageColour = Brushes.Red;
               
                return;
            }
       


            //IMPORT CSV 
            //LOOP THROUGH LIST AND QUERY BARCODE using System.Windows.Forms;

            using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
            {
                //Clear Existing Collection
                vm.NetsuiteImportCollection.Clear();

                //Requires GUID for MyComputer Or ThisPC Folder
                openFileDialog.InitialDirectory = vm.downld; //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                openFileDialog.Filter = "CSV files (*.csv)|*.csv";
                openFileDialog.FilterIndex = 0;
                openFileDialog.Multiselect = true;
                openFileDialog.RestoreDirectory = true;

                // string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");

                //MessageBox.Show(rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime);


                Mouse.OverrideCursor = Cursors.Wait;

                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    foreach (var file in openFileDialog.FileNames)
                    {

                     // MessageBox.Show(file);
                        if (File.Exists(file))
                        {
                          
                            ////Uses a VB class to process the file instead of streamreader
                            using (TextFieldParser parser = new TextFieldParser(file))
                            {
                                //parser.TextFieldType = FieldType.Delimited;
                                //parser.SetDelimiters(",");
                                while (!parser.EndOfData)
                                {
                                    //Processing row
                                    string currentLine = parser.ReadLine();


                                    if (currentLine.Contains("Quantity")) 
                                    {
                                        //Dont include headers  || 
                                    }
                                    else
                                    {

                                        //Needs different objects to work
                                        NetsuiteInventory nsi = FromCsv(currentLine);
                                        NetsuiteInventory nsComp = FromCsv(currentLine);
                                        NetsuiteInventory nsOrigComp = FromCsv(currentLine);

                                        //  MessageBox.Show(currentLine);
                                        //Check if HDD and add to the collection else skip
                                        if (currentLine.ToUpper().Contains("NO HDD") || currentLine.ToUpper().Contains("CONTROLLER"))
                                        {


                                        }
                                        else if (currentLine.Contains(" hdd ") || currentLine.Contains("hdd") || currentLine.Contains(" HDD ") || currentLine.Contains("HDD") || currentLine.Contains(" HDD")
                                        && currentLine.Contains(" SAS ") || currentLine.Contains(" SAS") || currentLine.Contains(" SATA ")|| currentLine.Contains(" M2 ") || currentLine.Contains(" ssd ") || currentLine.Contains(" SSD ") || currentLine.Contains(" HOTSWAP "))
                                        {
                                            //MessageBox.Show(currentLine);
                                            //add item
                                            //vm.NetsuiteImportCollection.Add(FromCsv(currentLine));

                                          
                                            //MessageBox.Show(nsi.ProductDescription);
                                            if (String.IsNullOrEmpty(nsi.ProductDescription))
                                            {
                                                //add counts on mainpage 
                                            }
                                            else
                                            {
                                                //REMOVE DUPLICATES
                                               
                                                //Check if Duplicate already exists, Extract Product Code & Quantity to add to the remaing line
                                                if (vm.NetsuiteComparisonCollection.Any((item) => item.ProductCode == nsComp.ProductCode))
                                                {
                                                    //MessageBox.Show(nsComp.ProductCode + " " + nsComp.Quantity);
                                                    //HighLight Line As Duplicate

                                                    if (vm.NetsuiteImportCollection.Any((item) => item.ProductCode == nsComp.ProductCode))
                                                    {

                                                        //MessageBox.Show(item.ToString());
                                                        //Pull in the Matching Product Code
                                                     NetsuiteInventory importOriginalItm = vm.NetsuiteOriginalImportCollection.Where(n => n.ProductCode == nsComp.ProductCode).First();
                                                     NetsuiteInventory importItm =  vm.NetsuiteImportCollection.Where(n => n.ProductCode == nsComp.ProductCode).First();
                                                     NetsuiteInventory compItm = vm.NetsuiteComparisonCollection.Where(n => n.ProductCode == nsComp.ProductCode).First();
                                                        //Add the quantity to the remaining line
                                                     int newTotal = Convert.ToInt32(importItm.Quantity) + Convert.ToInt32(nsComp.Quantity);

                                                        //Set the Property
                                                     importOriginalItm.Quantity = newTotal.ToString();
                                                     importItm.Quantity = newTotal.ToString();
                                                     compItm.Quantity = newTotal.ToString();


                                                    }
                                                 
                                                }
                                                else
                                                {
                                                    //Add to Comparison collection for original imported state
                                                    vm.NetsuiteOriginalImportCollection.Add(nsOrigComp);
                                                    //Add to Comparison collection for original imported state
                                                    vm.NetsuiteComparisonCollection.Add(nsComp);
                                                    //Add to visible collection
                                                    vm.NetsuiteImportCollection.Add(nsi);
                                                }



                                            }
                                           
                                        }
                                        else if(currentLine.Contains("Various")|| currentLine.Contains("VARIOUS") || currentLine.Contains("various"))
                                            {
                                            //TO FILTER ONLY HDD IN DESCRIPTION
                                            //if (currentLine.ToUpper().Contains("NO HDD") || currentLine.ToUpper().Contains("CONTROLLER"))
                                            //{


                                            //}
                                            //else if (currentLine.Contains(" hdd ") || currentLine.Contains("hdd") || currentLine.Contains(" HDD ") || currentLine.Contains("HDD") || currentLine.Contains(" HDD") && currentLine.Contains(" SAS ")
                                            //|| currentLine.Contains(" SAS") || currentLine.Contains(" SATA ") || currentLine.Contains(" M2 ") || currentLine.Contains(" ssd ") || currentLine.Contains(" SSD ") || currentLine.Contains(" HOTSWAP "))
                                            //{

                                            //    //Add to Comparison collection for original imported state
                                            //    vm.NetsuiteComparisonCollection.Add(nsComp);
                                            //    //Add to visible collection
                                            //    vm.NetsuiteImportCollection.Add(nsi);
                                            //}
                                            //else
                                            //{
                                            //  //Don't add anything
                                            //}

                                            //ALLOW ALL VARIOUS LINES TO BE IMPORTED 
                                            //Add to Comparison collection for original imported state
                                            vm.NetsuiteOriginalImportCollection.Add(nsOrigComp);
                                            //Add to Comparison collection for original imported state
                                            vm.NetsuiteComparisonCollection.Add(nsComp);
                                            //Add to visible collection
                                            vm.NetsuiteImportCollection.Add(nsi);

                                        }
                                        
                                        //add item
                                       // vm.NetsuiteImportCollection.Add(FromCsv(currentLine));
                                    }



                                }
                              }


                            //Assign to original untouched import version
                            //vm.NetsuiteOriginalImportCollection = vm.NetsuiteComparisonCollection;

                            vm.POImportTotal = 0;
                            //COUNT IMPORTED TOTAL
                            foreach (NetsuiteInventory ns in vm.NetsuiteOriginalImportCollection)
                            {
                                //MessageBox.Show(ns.Quantity);

                                vm.POImportTotal += Convert.ToInt32(ns.Quantity);
                            }
                            //Set the total on initial import
                            vm.TotalOnImport = vm.POImportTotal;



                            vm.POImportTotal = 0;
                            //COUNT IMPORTED TOTAL
                            foreach (NetsuiteInventory ns in vm.NetsuiteComparisonCollection)
                            {
                                //MessageBox.Show(ns.Quantity);

                                vm.POImportTotal += Convert.ToInt32(ns.Quantity);
                            }
                          
                      
                            //Console.WriteLine(vm.TotalOnImport.ToString());

                            //vm.NetsuiteComparisonCollection.FirstOrDefault().ImportTotal = vm.TotalOnImport;

                            vm.POTotal = 0;
                            //COUNT IMPORTED TOTAL
                            foreach (NetsuiteInventory ns in vm.NetsuiteImportCollection)
                            {
                                //MessageBox.Show(ns.Quantity);

                                vm.POTotal += Convert.ToInt32(ns.Quantity);
                            }

                            //vm.POImportTotal = 0;
                            ////COUNT COMPARISON TOTAL
                            //foreach (NetsuiteInventory ns in vm.NetsuiteComparisonCollection)
                            //{
                            //    //MessageBox.Show(ns.Quantity);

                            //    vm.POImportTotal += Convert.ToInt32(ns.Quantity);
                            //}



                        }

                    }


                }

                Mouse.OverrideCursor = null;

            }

            //Deselect item in the list
            lvImport.SelectedIndex = 0;
            vm.ImportMessageColour = Brushes.Green;
            vm.ImportMessage = "Please Add All Extra's before starting the Serial Scan!";

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }





        //CSV IMPORT
        public NetsuiteInventory FromCsv(string csvLine)
        {
            try
            {

                //string[] values2 = csvLine.Split(',');

                //MessageBox.Show(values2.Count().ToString());


           if (csvLine != null)
            {


                    //New method to handle commas in field data
                    Regex CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                    string[] values = CSVParser.Split(csvLine);

                    //Original, only handles CSV not commas in field data
                    // string[] values = csvLine.Split(',');
                   //MessageBox.Show(Convert.ToString(values[0]));

                    NetsuiteInventory sproc = new NetsuiteInventory();

                    //MessageBox.Show(values.Length.ToString());

                    switch (values.Length.ToString())
                    {
                        case "5":
                            if (values[0] != string.Empty)
                                sproc.ProductCode = Convert.ToString(values[0]);
                            if (values[1] != string.Empty)
                            {
                                //check if inner description field contains comma
                                if (values[1].ToString().Contains(","))
                                {
                                    //Inner values in field
                                    string[] infield = values[1].ToString().Split(','); 
                                    string newvalue = "";
                                    //loop through inner field to strip out HDD's
                                    foreach(string itm in infield)
                                    {
                                       // MessageBox.Show(itm.ToUpper());
                                       
                                       if (itm.ToUpper().Contains("NO HDD") || itm.ToUpper().Contains("CONTROLLER"))
                                        {
                                          
                                           
                                        }
                                        else
                                        {
                                            if (itm.ToUpper().Contains(" HDD ") || itm.ToUpper().Contains("HDD") || itm.ToUpper().Contains(" HDD") && itm.ToUpper().Contains("SAS ")
                                               || itm.ToUpper().Contains("SATA ") || itm.ToUpper().Contains("M2 ") || itm.ToUpper().Contains("SSD ") || itm.ToUpper().Contains("HOTSWAP "))
                                            {
                                                newvalue += itm;
                                                //Set description as new value
                                                sproc.ProductDescription = Convert.ToString(newvalue.Replace("\"", ""));
                                            }
                                            else
                                            {
                                               
                                            }


                                        }
                                    }
                                  
                                }
                                else
                                {
                                    //just apply as standard
                                    sproc.ProductDescription = Convert.ToString(values[1]);
                                }
                                          
                            }
                              
                            if (values[2] != string.Empty)
                                sproc.POLineUnitPrice = Convert.ToString(values[2]);
                            //Replace any negative vaules
                            if (values[3] != string.Empty)
                            {
                                //Check if Memory and reduce quantity to 1, to reduce

                                sproc.Quantity = Convert.ToString(values[3].Replace("-", ""));
                            }

                            if (values[4] != string.Empty)
                                sproc.POUnitAmount = Convert.ToString(values[4]);

                            break;
                        //Extra Column with PO\SO\RMA Number
                        case "7":

                            if (values[2] != string.Empty)
                                sproc.ProductCode = Convert.ToString(values[2]);
                            if (values[3] != string.Empty)
                            {
                                //check if inner description field contains comma
                                if (values[3].ToString().Contains(","))
                                {
                                    //Inner values in field
                                    string[] infield = values[3].ToString().Split(',');
                                    string newvalue = "";
                                    //loop through inner field to strip out HDD's
                                    foreach (string itm in infield)
                                    {
                                        // MessageBox.Show(itm.ToUpper());

                                        if (itm.ToUpper().Contains("NO HDD") || itm.ToUpper().Contains("CONTROLLER"))
                                        {


                                        }
                                        else
                                        {
                                            if (itm.ToUpper().Contains(" HDD ") || itm.ToUpper().Contains("HDD") || itm.ToUpper().Contains(" HDD") && itm.ToUpper().Contains("SAS ")
                                               || itm.ToUpper().Contains("SATA ") || itm.ToUpper().Contains("M2 ") || itm.ToUpper().Contains("SSD ") || itm.ToUpper().Contains("HOTSWAP "))
                                            {
                                                newvalue += itm;
                                                //Set description as new value
                                                sproc.ProductDescription = Convert.ToString(newvalue.Replace("\"", ""));
                                            }
                                            else
                                            {

                                            }


                                        }
                                    }

                                }
                                else
                                {
                                    //just apply as standard
                                    sproc.ProductDescription = Convert.ToString(values[3]);
                                }

                            }

                            if (values[4] != string.Empty)
                                sproc.POLineUnitPrice = Convert.ToString(values[4]);
                            //Replace any negative vaules
                            if (values[5] != string.Empty)
                            {
                                //Check if Memory and reduce quantity to 1, to reduce

                                sproc.Quantity = Convert.ToString(values[5].Replace("-", ""));
                            }

                            if (values[6] != string.Empty)
                                sproc.POUnitAmount = Convert.ToString(values[6]);

                            break;

                            //OLD CODE
                            //if (values[0] != string.Empty)
                            //    vm.PONumber = Convert.ToString(values[0]);
                            //if (values[1] != string.Empty)
                            //    sproc.ProductCode = Convert.ToString(values[1]);
                            //if (values[2] != string.Empty)
                            //    sproc.ProductDescription = Convert.ToString(values[2]);
                            //if (values[3] != string.Empty)
                            //    sproc.POLineUnitPrice = Convert.ToString(values[3]);
                            ////Replace any negative vaules
                            //if (values[4] != string.Empty)
                            //{
                            //    //Check if Memory and reduce quantity to 1, to reduce

                            //    sproc.Quantity = Convert.ToString(values[4].Replace("-", ""));

                            //}
                            //if (values[5] != string.Empty)
                            //    sproc.POUnitAmount = Convert.ToString(values[5]);
                            //break;
                    }

                    return sproc;



                }

                return null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //Console.WriteLine(ex.Message);
                return null;
            }

        }




        private bool CheckPONumber()
        {

            try
            {


                //Point DB to Test Databases
                if (StaticFunctions.TestMode == true)
                {
                    //Set connection string to Offline Test DB
                    cs = @"URI=file:" + @"C:\Users\n.myers\Desktop\Data\DriveTestSQL.db";
                }
                else
                {
                    if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/DriveTest/DriveTestSQL.db"))
                    {
                        //TEMP Workaround File Server issue
                        Console.WriteLine("Workaround DB");
                        // Set connection string to Live DB
                        cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/DriveTest/DriveTestSQL.db";
                    }
                    else
                    {

                        //Set connection string to Live DB Backup\United Kingdom
                        cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\DriveTestSQL.db";
                    }
                }

            //Check if PO Number Already Exists in DB
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;

            string sqlindecheck = "SELECT * FROM sqlite_master WHERE type = 'index'";
            //string sqlindecheck = "SELECT * FROM DriveInfo WHERE type = 'index' AND name = POSERIALIND";
            string sqlquery = "SELECT * FROM DriveInfo WHERE PONumber LIKE '" + vm.PONumber + "'";
            string sqlIndex = "CREATE INDEX POSERIALIND on DriveInfo (PONumber, ProductCode, SerialNo)";
            sqlite_cmd = sqlite_conn.CreateCommand();


                try
                {
                    //MAIN SEARCH QUERY

                    //CHECK IF INDEX THERE
                    //Hold return values
                    string indexThere = "";
                    sqlite_cmd.CommandText = sqlindecheck;
                    sqlite_datareader = sqlite_cmd.ExecuteReader();
                    
                    //Use read to pull in details
                    while (sqlite_datareader.Read())
                    {
                        indexThere = sqlite_datareader.GetString(0) + " " + sqlite_datareader.GetString(1) + " " + sqlite_datareader.GetString(2);
                    }

                    //Close Data Reader
                    sqlite_datareader.Close();


                    //MessageBox.Show(indexThere);

                    ////Drop INDEX
                    //sqlIndex = "DROP INDEX POSERIALIND";
                    //sqlite_cmd.CommandText = sqlIndex;
                    //sqlite_cmd.ExecuteNonQuery();


                    //If index there then skip index creation
                    if (indexThere.Contains("POSERIALIND"))
                    {

                    }
                    else
                    {
                        //CREATE TEMP INDEX
                        sqlite_cmd.CommandText = sqlIndex;
                        sqlite_cmd.ExecuteNonQuery();
                    }


                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }


            //MAIN SEARCH QUERY
            sqlite_cmd.CommandText = sqlquery;
            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                if (vm.PONumber == sqlite_datareader.GetString(2))
                {
                    vm.ImportMessageColour = Brushes.Red;
                    vm.ImportMessage = "PO Already exists in the DataBase! Import cancelled.";
                      
                    //Close Db
                    sqlite_conn.Close();

                    return true;


                }
                else
                {
                    return false;
                }

            }

            return false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //vm.ImportMessage = ex.Message;
                //Close Db
                //sqlite_conn.Close();
                return false;
            }
        }





        //Create Connection to DB
        SQLiteConnection CreateConnection(string pathtodb)
        {
            try
            {

           
            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            //sqlite_conn = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            //sqlite_conn = new SQLiteConnection("Data Source=" + pathtodb + "; Version = 3; New = True; Compress = True; ");
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return sqlite_conn;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

                return null;
            }
        }
    }

   
}


