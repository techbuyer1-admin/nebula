﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Nebula.Paginators;
using Nebula.ViewModels;
using Nebula.Helpers;
using System.Windows.Documents;
using System.Windows.Controls;
using System.IO;
using System.Configuration;

namespace Nebula.Commands
{
   public class PrintSaveFlowDocumentCommand : ICommand
    {

        //declare viewmodel
        GoodsInOutViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public PrintSaveFlowDocumentCommand(GoodsInOutViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //if (lv != null)
            //{
            //    if (lv.SelectedItems.Count > 0)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    return true;
            //}
            return true;
        }

        public void Execute(object parameter)
        {


            var values = (object)parameter;

            //MessageBox.Show(drives);
            //using (System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog())
            //{

            //    //Requires GUID for MyComputer Or ThisPC Folder
            //    saveFileDialog.InitialDirectory = @"" + @"\\pinnacle.local\tech_resources\Nebula\"; //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
            //    saveFileDialog.Filter = "All files (*.*)|*.*";
            //    saveFileDialog.FilterIndex = 0;
            //    saveFileDialog.RestoreDirectory = true;
            //    saveFileDialog.ShowDialog(;
            //    saveFileDialog.ShowDialog();
            //}




            DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();



            ////Set the Chassis number to text on the clipboard  Use control v

            //if(vm.CurrentFilter == "On Hold Jobs")
            //{
            //    Clipboard.SetText("Current Jobs On Hold Report " + DateTime.Now.ToShortDateString().Replace("/", "-"), TextDataFormat.UnicodeText);
            //}
            //else
            //{
            //    Clipboard.SetText("Weekly Server Totals Report " + vm.DateFrom.ToShortDateString().Replace("/", "-") + " To " + vm.DateTo.ToShortDateString().Replace("/", "-"), TextDataFormat.UnicodeText);
            //}



            //Print to PDF Printer OLD
            //dpw.SaveDocPDF((FlowDocument)values);
            //user to select folder to save too

            //NEW STRAIGHT THROUGH
            dpw.SaveDocGOTPDF((FlowDocument)values, vm.myDocs + @"\" + Clipboard.GetText() + @".pdf");



            //zip folder and attach to email 

            EmailClass emailReport = new EmailClass(vm);
           

            //Create Outlook Object
            emailReport.SendMailWithOutlook(Clipboard.GetText(), "", "", new string[] { vm.myDocs + @"\" + Clipboard.GetText() + @".pdf" }, EmailClass.MailSendType.ShowModal);

            // MessageBox.Show("Report Saved to:" + vm.myDocs + @"\" + Clipboard.GetText() + @".pdf");

            //Create email object and attach

            //return;
            //Close the hosting Window
            //vm.LoadWindow.Close();

            //need to capture where the PDF WAS SAVED AS PATH MISSING YEAR AND MONTH

            //RESINSTATE THIS COMMAND






        }
    }
}

