﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
//using Microsoft.VisualBasic.CompilerServices; // Install-Package Microsoft.VisualBasic

namespace Nebula.Commands
{
    public partial class RichTextBoxEx : RichTextBox
    {
        private PropertyInfo _caretElementProperty;
        private object _caretElement;
        private FieldInfo _caretWidthField;
        private MethodInfo _caretRefreshMethod;

        public RichTextBoxEx()
        {
            _caretElementProperty = Selection.GetType().GetProperty("CaretElement", BindingFlags.NonPublic | BindingFlags.Instance);
            LayoutUpdated += Me_LayoutUpdated;
        }

        public static readonly DependencyProperty CaretWidthProperty = DependencyProperty.Register("CaretWidth", typeof(double), typeof(RichTextBoxEx), new FrameworkPropertyMetadata(1.0d, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.AffectsRender));

        public double CaretWidth
        {
            get
            {
                return Conversions.ToDouble(GetValue(CaretWidthProperty));
            }

            set
            {
                SetValue(CaretWidthProperty, value);
            }
        }

        private void Me_LayoutUpdated(object sender, EventArgs e)
        {
            var currentCaretElement = _caretElementProperty.GetValue(Selection, default);
            if (currentCaretElement is null)
            {
                return;
            }
            else if (!ReferenceEquals(_caretElement, currentCaretElement))
            {
                _caretElement = currentCaretElement;
                if (_caretWidthField is null)
                {
                    _caretWidthField = _caretElement.GetType().GetField("_systemCaretWidth", BindingFlags.NonPublic | BindingFlags.Instance);
                }

                if (_caretRefreshMethod is null)
                {
                    _caretRefreshMethod = _caretElement.GetType().GetMethod("RefreshCaret", BindingFlags.NonPublic | BindingFlags.Instance);
                }
            }

            if (_caretWidthField is null | _caretRefreshMethod is null)
            {
                return;
            }

            double currentCaretWidth = Conversions.ToDouble(_caretWidthField.GetValue(_caretElement));
            if (currentCaretWidth != CaretWidth)
            {
                _caretWidthField.SetValue(_caretElement, CaretWidth);
                var isItalic = default(bool);
                if (!object.ReferenceEquals(Selection.GetPropertyValue(TextBlock.FontStyleProperty), DependencyProperty.UnsetValue))
                {
                    // isItalic = Selection.GetPropertyValue(TextBlock.FontStyleProperty) == FontStyles.Italic;
                }

                _caretRefreshMethod.Invoke(_caretElement, new object[] { isItalic });
            }
        }
    }
}