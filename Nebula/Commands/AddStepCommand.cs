﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class AddStepCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;


        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public AddStepCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
           
            //Get array of objects
            var values = (object[])parameter;

            //Split array into specific types
            if (values != null)
            {
                var lv = (ListView)values[1];
                var tbDesc = (TextBox)values[2];
                var tbCli = (TextBox)values[3];
          
            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

            //TextBox tbDesc = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepDescriptionTXT") as TextBox;
            //TextBox tbCli = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepCliCommandTXT") as TextBox;

            if (tbDesc != null || tbCli != null)
            {


                if (tbDesc.Text == string.Empty && tbCli.Text == string.Empty || tbDesc.Text == string.Empty || tbCli.Text == string.Empty)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }


           }
         else
          {
             return false;
          }

        }

        public void Execute(object parameter)
        {
            //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

            //MessageBox.Show(@"" + vm.dsktop + @"\Invoice App\ClientDetails.xml");
            //@"" + vm.myDocs + @

            //vm.ProductCollectionList = new ObservableCollection<AutomatedCliModel>();
            //vm.StepSequenceCollection = new ObservableCollection<StepItem>();

            //vm.ProductCollectionList.Clear();
            //vm.StepSequenceCollection.Clear();

           // var values = (object)parameter;

            //Get array of objects
            var values = (object[])parameter;
            //Split array into specific types
            var action = (string)values[0];
            var lv = (ListView)values[1];
            var tbDesc = (TextBox)values[2];
            var tbCli = (TextBox)values[3];
           


            //use helper to get control

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //TextBox tbDesc = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepDescriptionTXT") as TextBox;
            //TextBox tbCli = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepCliCommandTXT") as TextBox;






            // check if either textbox is empty
            if (tbDesc.Text != "" && tbCli.Text != "")
            {
                switch (action)
                {
                    case "Add":
                        StepItem stepseqadd = new StepItem();
                        // stepseq.StepNo = 1; //vm.StepSequenceCollection.Count() + 1;
                        stepseqadd.StepNo = vm.StepSequenceCollectionForm.Count() + 1;
                        stepseqadd.StepDescription = tbDesc.Text; //vm.StepDescription;
                        stepseqadd.CliCommandToSend = tbCli.Text; // vm.StepCliCommand;
                        vm.StepSequenceCollectionForm.Add(stepseqadd);

                        //vm.StepSequenceCollection.Where(w => w.StepNo == "height").ToList().ForEach(s => s.Value = vm.StepSequenceCollection.Count + );

                        //vm.StepSequenceCollection.Where(w => w.StepNo == 0).ToList().ForEach(s => s.Value = vm.StepSequenceCollection.Count + )

                        break;


                  
                    default:
                        break;
                }

                //Clear the controls on the page
                vm.ClearSteps();
                //give focus back to the Step description box
                tbDesc.Focus();
            }
            else
            {
                MessageBox.Show("Both Step Fields require a value");
            }
           
        }
    }
}
