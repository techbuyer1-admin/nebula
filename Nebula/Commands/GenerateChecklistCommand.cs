﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;
using Nebula.Views;
using System.Data.SQLite;
using System.Data;

namespace Nebula.Commands
{
    public class GenerateChecklistCommand : ICommand
    {

        //declare viewmodel
        GoodsInOutViewModel vm = null;
        //MainViewModel Mainvm = null;


        //SQL Lite DB LINK
        public string cs = "";


        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public GenerateChecklistCommand(GoodsInOutViewModel TheViewModel)
        {
            //Point DB to Test Databases
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\ServersProcessed.db";
            }
            else
            {
                this.vm = TheViewModel;

                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/ServersProcessed/ServersProcessed.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/ServersProcessed/ServersProcessed.db";
                }
                else
                {

                    //Set connection string to Live DB
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\ServersProcessed.db";
                }
               
            }


         

        }

        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            var values = (object)parameter;

            switch (values.ToString())
            {
                case "TestDept":
                    if(vm.GoodsINRep != "" && vm.PurchaseOrderNumber != "" && vm.ChassisSerialNo != "") //&& vm.GoodsINQ1 == true && vm.GoodsINQ2 == true && vm.GoodsINQ4 == true && vm.GoodsINQ5 == true && vm.GoodsINQ6 == true && vm.GoodsINQ7 == true && vm.GoodsINQ8 == true && vm.GoodsINQ9
                       //&& vm.GoodsINQ10 && vm.GoodsINQ11 && vm.GoodsINQ12 == true && vm.GoodsINQ13 == true && vm.GoodsINQ14 == true && vm.GoodsINQ15 == true  && vm.GoodsINQ17 == true
                       //&& vm.GoodsINQ19 == true && vm.GoodsINQ20 == true && vm.GoodsINQ21 == true && vm.GoodsINQ22 == true && vm.GoodsINQ24 == true && vm.GoodsINQ25 == true)
                    {
                        return true;
                    }

                    break;
                case "GoodsIn":
                    if (vm.GoodsINRep != "" && vm.PurchaseOrderNumber != "" && vm.ChassisSerialNo != "")  //&& vm.GoodsINQ1 == true && vm.GoodsINQ2 == true && vm.GoodsINQ4 == true && vm.GoodsINQ5 == true && vm.GoodsINQ6 == true && vm.GoodsINQ7 == true && vm.GoodsINQ8 == true && vm.GoodsINQ9
                    // && vm.GoodsINQ10 && vm.GoodsINQ11 && vm.GoodsINQ12 == true && vm.GoodsINQ13 == true && vm.GoodsINQ14 == true && vm.GoodsINQ15 == true && vm.GoodsINQ17 == true
                    // && vm.GoodsINQ19 == true && vm.GoodsINQ20 == true && vm.GoodsINQ21 == true && vm.GoodsINQ22 == true && vm.GoodsINQ24 == true && vm.GoodsINQ25 == true
                    {
                        return true;
                    }
                   
                    break;
                case "GoodsOut":
                    try
                    {
                        if(vm.LoadedChecklist != null)
                        if (vm.GoodsOUTRep != String.Empty && vm.SalesOrderNumber != String.Empty && vm.LoadedChecklist.ChassisSerialNo != String.Empty)                 //&& vm.GoodsOUTQ1 == true && vm.GoodsOUTQ2 == true && vm.GoodsOUTQ4 == true && vm.GoodsOUTQ5 == true && vm.GoodsOUTQ6 == true && vm.GoodsOUTQ7 == true && vm.GoodsOUTQ8 == true && vm.GoodsOUTQ9
                                                                                                                                                                        // && vm.GoodsOUTQ10 && vm.GoodsOUTQ11 && vm.GoodsOUTQ12 == true && vm.GoodsOUTQ13 == true && vm.GoodsOUTQ14 == true && vm.GoodsOUTQ15 == true && vm.GoodsOUTQ16 == true && vm.GoodsOUTQ17 == true
                                                                                                                                                                         // && vm.GoodsOUTQ18 == true && vm.GoodsOUTQ19 == true && vm.GoodsOUTQ20 == true && vm.GoodsOUTQ21 == true && vm.GoodsOUTQ22 == true && vm.GoodsOUTQ23 == true && vm.GoodsOUTQ24 == true && vm.GoodsOUTQ25 == true)
                        {
                                return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        string exception = ex.Message;
                        vm.ChecklistMessage = "Please Load a Checklist";
                    }


                    break;

            }

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView; && vm.ChassisSerialNo != String.Empty
            //if (lv != null)
            //{
            //    if (lv.SelectedItems.Count > 0)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    return true;
            //}
            return false;
        }

        public void Execute(object parameter)
        {

            StaticFunctions.ChassisNoPassthrough = "";


            var values = (object)parameter;

            //Connection to SQL Lite DB
            //if all criteria met create the job
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);



            //Create Model Of Details and Assign Value of Properties
            ServerBladeChecklist servCheck = new ServerBladeChecklist();
            try
            {

       

            switch (values.ToString())
            {
                case "TestDept":
                    servCheck.ChassisSerialNo = vm.ChassisSerialNo;
                    //GOODS IN
                    servCheck.GoodsINQ1 = vm.GoodsINQ1;
                    servCheck.GoodsINQ2 = vm.GoodsINQ2;
                    //servCheck.GoodsINQ3 = vm.GoodsINQ3;
                    servCheck.GoodsINQ4 = vm.GoodsINQ4;
                    servCheck.GoodsINQ5 = vm.GoodsINQ5;
                    servCheck.GoodsINQ6 = vm.GoodsINQ6;
                    servCheck.GoodsINQ7 = vm.GoodsINQ7;
                    servCheck.GoodsINQ8 = vm.GoodsINQ8;
                    servCheck.GoodsINQ9 = vm.GoodsINQ9;
                    servCheck.GoodsINQ10 = vm.GoodsINQ10;
                    servCheck.GoodsINQ11 = vm.GoodsINQ11;
                    servCheck.GoodsINQ12 = vm.GoodsINQ12;
                    servCheck.GoodsINQ13 = vm.GoodsINQ13;
                    servCheck.GoodsINQ14 = vm.GoodsINQ14;
                    servCheck.GoodsINQ15 = vm.GoodsINQ15;
                    //Textbox
                    servCheck.GoodsINQ16 = vm.GoodsINQ16TXT;
                    servCheck.GoodsINQ17 = vm.GoodsINQ17;
                    servCheck.GoodsINQ18 = vm.GoodsINQ18;
                    servCheck.GoodsINQ19 = vm.GoodsINQ19;
                    servCheck.GoodsINQ20 = vm.GoodsINQ20;
                    servCheck.GoodsINQ21 = vm.GoodsINQ21;
                    servCheck.GoodsINQ22 = vm.GoodsINQ22;
                    servCheck.GoodsINQ23 = vm.GoodsINQ23;
                    servCheck.GoodsINQ24 = vm.GoodsINQ24;
                    servCheck.GoodsINQ25 = vm.GoodsINQ15;
                    servCheck.PurchaseOrderNo = vm.PurchaseOrderNumber;
                    servCheck.GoodsINRep = vm.GoodsINRep;
                    servCheck.DateCheck = vm.DateCheck;
                    servCheck.GoodsOUTQ1 = vm.GoodsOUTQ1;
                    servCheck.GoodsOUTQ2 = vm.GoodsOUTQ2;
                    servCheck.GoodsOUTQ3 = vm.GoodsOUTQ3;
                    servCheck.GoodsOUTQ4 = vm.GoodsOUTQ4;
                    servCheck.GoodsOUTQ5 = vm.GoodsOUTQ5;
                    servCheck.GoodsOUTQ6 = vm.GoodsOUTQ6;
                    servCheck.GoodsOUTQ7 = vm.GoodsOUTQ7;
                    servCheck.GoodsOUTQ8 = vm.GoodsOUTQ8;
                    servCheck.GoodsOUTQ9 = vm.GoodsOUTQ9;
                    servCheck.GoodsOUTQ10 = vm.GoodsOUTQ10;
                    servCheck.GoodsOUTQ11 = vm.GoodsOUTQ11;
                    servCheck.GoodsOUTQ12 = vm.GoodsOUTQ12;
                    servCheck.GoodsOUTQ13 = vm.GoodsOUTQ13;
                    servCheck.GoodsOUTQ14 = vm.GoodsOUTQ14;
                    servCheck.GoodsOUTQ15 = vm.GoodsOUTQ15;
                    servCheck.GoodsOUTQ16 = vm.GoodsOUTQ16;
                    servCheck.GoodsOUTQ17 = vm.GoodsOUTQ17;
                    //servCheck.GoodsOUTQ18 = vm.GoodsOUTQ18;
                    servCheck.GoodsOUTQ19 = vm.GoodsOUTQ19;
                    servCheck.GoodsOUTQ20 = vm.GoodsOUTQ20;
                    servCheck.GoodsOUTQ21 = vm.GoodsOUTQ21;
                    servCheck.GoodsOUTQ22 = vm.GoodsOUTQ22;
                    //servCheck.GoodsOUTQ23 = vm.GoodsOUTQ23;
                    servCheck.GoodsOUTQ24 = vm.GoodsOUTQ24;
                    servCheck.GoodsOUTQ25 = vm.GoodsOUTQ25;
                    servCheck.GoodsOUTRep = vm.GoodsOUTRep;
                    servCheck.SalesOrderNo = vm.SalesOrderNumber;
                      
                   //Call function to find correct folder in 
                    servCheck.PODocumentPath = vm.DocumentsSavePath;



                        //Insert PO if missing
                        //if (vm.PurchaseOrderNumber.Substring(0, 2) != "PO" || vm.PurchaseOrderNumber.Substring(0, 2) != "po")
                        //if (vm.PurchaseOrderNumber.Contains("PO") || vm.PurchaseOrderNumber.Contains("po"))
                        //{
                        //    vm.PurchaseOrderNumber = "PO" + vm.PurchaseOrderNumber;
                        //}


                        //Write to CSV
                        StaticFunctions.CreateAppendCSV(new ReportServersProcessed(vm.GoodsINRep, vm.PurchaseOrderNumber, vm.ChassisSerialNo, "Goods In", "OK", ""), @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed.csv");

                        //wRITE TO db
                        //wRITE TO db
                        //InsertData(sqlite_conn, new ServersProcessedModel(vm.PurchaseOrderNumber, "", "", vm.ChassisSerialNo, vm.GoodsOUTRep, vm.CurrentUserLocation, "Test Department", "", "OK", "", "", "", ""));


                        //CREATE XML FILE OF THE SETTINGS;

                    if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml"))
                    {
                        //If file already exists, then delete before creating a new one
                        File.Delete(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml");

                        //Create XML File of Settings
                        XMLTools.WriteToXmlFile(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml", servCheck, false);


                    }
                    else
                    {
                        //Create XML File of Settings
                        XMLTools.WriteToXmlFile(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml", servCheck, false);
                    }




                        if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml"))
                        {
                            vm.LoadedChecklist = XMLTools.ReadFromXmlFile<ServerBladeChecklist>(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml");
                            if (vm.LoadedChecklist.PODocumentPath != null)
                                vm.LoadedChecklist.PODocumentPath.Replace(@"\\", @"\");

                        }







                        //Load Checklist Window
                        if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml"))
                        {
                            vm.LoadReport = new ServerBladeInspectionCheckListView(vm);

                            vm.LoadWindow = new ChecklistWindow(vm);

                            vm.LoadWindow.ShowDialog();
                        }


                        //Set the Checklist Message 
                        vm.ChecklistMessage = "Checklist for " + vm.ChassisSerialNo + " Completed!";

                        //Clear the form
                        vm.ClearChecklistControls();




                        break;
                case "GoodsIn":

                    servCheck.ChassisSerialNo = vm.ChassisSerialNo;
                    //GOODS IN
                    servCheck.GoodsINQ1 = vm.GoodsINQ1;
                    servCheck.GoodsINQ2 = vm.GoodsINQ2;
                    //servCheck.GoodsINQ3 = vm.GoodsINQ3;
                    servCheck.GoodsINQ4 = vm.GoodsINQ4;
                    servCheck.GoodsINQ5 = vm.GoodsINQ5;
                    servCheck.GoodsINQ6 = vm.GoodsINQ6;
                    servCheck.GoodsINQ7 = vm.GoodsINQ7;
                    servCheck.GoodsINQ8 = vm.GoodsINQ8;
                    servCheck.GoodsINQ9 = vm.GoodsINQ9;
                    servCheck.GoodsINQ10 = vm.GoodsINQ10;
                    servCheck.GoodsINQ11 = vm.GoodsINQ11;
                    servCheck.GoodsINQ12 = vm.GoodsINQ12;
                    servCheck.GoodsINQ13 = vm.GoodsINQ13;
                    servCheck.GoodsINQ14 = vm.GoodsINQ14;
                    servCheck.GoodsINQ15 = vm.GoodsINQ15;
                    //Textbox
                    servCheck.GoodsINQ16 = vm.GoodsINQ16TXT;
                    servCheck.GoodsINQ17 = vm.GoodsINQ17;
                    //servCheck.GoodsINQ18 = vm.GoodsINQ18;
                    servCheck.GoodsINQ19 = vm.GoodsINQ19;
                    servCheck.GoodsINQ20 = vm.GoodsINQ20;
                    servCheck.GoodsINQ21 = vm.GoodsINQ21;
                    servCheck.GoodsINQ22 = vm.GoodsINQ22;
                    //servCheck.GoodsINQ23 = vm.GoodsINQ23;
                    servCheck.GoodsINQ24 = vm.GoodsINQ24;
                    servCheck.GoodsINQ25 = vm.GoodsINQ15;
                    servCheck.PurchaseOrderNo = vm.PurchaseOrderNumber;
                    servCheck.GoodsINRep = vm.GoodsINRep;
                    servCheck.DateCheck = vm.DateCheck;
                    servCheck.GoodsOUTQ1 = vm.GoodsOUTQ1;
                    servCheck.GoodsOUTQ2 = vm.GoodsOUTQ2;
                    servCheck.GoodsOUTQ3 = vm.GoodsOUTQ3;
                    servCheck.GoodsOUTQ4 = vm.GoodsOUTQ4;
                    servCheck.GoodsOUTQ5 = vm.GoodsOUTQ5;
                    servCheck.GoodsOUTQ6 = vm.GoodsOUTQ6;
                    servCheck.GoodsOUTQ7 = vm.GoodsOUTQ7;
                    servCheck.GoodsOUTQ8 = vm.GoodsOUTQ8;
                    servCheck.GoodsOUTQ9 = vm.GoodsOUTQ9;
                    servCheck.GoodsOUTQ10 = vm.GoodsOUTQ10;
                    servCheck.GoodsOUTQ11 = vm.GoodsOUTQ11;
                    servCheck.GoodsOUTQ12 = vm.GoodsOUTQ12;
                    servCheck.GoodsOUTQ13 = vm.GoodsOUTQ13;
                    servCheck.GoodsOUTQ14 = vm.GoodsOUTQ14;
                    servCheck.GoodsOUTQ15 = vm.GoodsOUTQ15;
                    servCheck.GoodsOUTQ16 = vm.GoodsOUTQ16;
                    servCheck.GoodsOUTQ17 = vm.GoodsOUTQ17;
                    servCheck.GoodsOUTQ18 = vm.GoodsOUTQ18;
                    servCheck.GoodsOUTQ19 = vm.GoodsOUTQ19;
                    servCheck.GoodsOUTQ20 = vm.GoodsOUTQ20;
                    servCheck.GoodsOUTQ21 = vm.GoodsOUTQ21;
                    servCheck.GoodsOUTQ22 = vm.GoodsOUTQ22;
                    servCheck.GoodsOUTQ23 = vm.GoodsOUTQ23;
                    servCheck.GoodsOUTQ24 = vm.GoodsOUTQ24;
                    servCheck.GoodsOUTQ25 = vm.GoodsOUTQ25;
                    servCheck.GoodsOUTRep = vm.GoodsOUTRep;
                    servCheck.SalesOrderNo = vm.SalesOrderNumber;

                    //Call function to find correct folder in 
                    servCheck.PODocumentPath = vm.DocumentsSavePath;


                        ////Insert PO if missing
                        //if (vm.PurchaseOrderNumber.Substring(0, 2) != "PO" || vm.PurchaseOrderNumber.Substring(0, 2) != "po")
                        //{
                        //    vm.PurchaseOrderNumber = "PO" + vm.PurchaseOrderNumber;
                        //}

                    //Write to CSV
                     StaticFunctions.CreateAppendCSV(new ReportServersProcessed(vm.GoodsINRep, vm.PurchaseOrderNumber, vm.ChassisSerialNo, "Goods In","OK",""), @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed.csv");
                      



                        //CREATE XML FILE OF THE SETTINGS; vm.GoodsOUTRep,vm.SalesOrderNumber, vm.LoadedChecklist.ChassisSerialNo, "Goods Out Technical", "OK", ""

                    if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml"))
                    {
                        //If file already exists, then delete before creating a new one
                        File.Delete(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml");

                        //Create XML File of Settings
                        XMLTools.WriteToXmlFile(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml", servCheck, false);


                    }
                    else
                    {
                        //Create XML File of Settings
                        XMLTools.WriteToXmlFile(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml", servCheck, false);
                    }




                        //wRITE TO db
                        //InsertData(sqlite_conn, new ServersProcessedModel(vm.PurchaseOrderNumber, "", "", vm.ChassisSerialNo, vm.GoodsOUTRep, vm.CurrentUserLocation, "Goods In", "", "OK", "", "", "", ""));




                      


                        // LOAD CHECKLIST SETTINGS FROM AN XML FILE;


                        if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml"))
                        {
                            vm.LoadedChecklist = XMLTools.ReadFromXmlFile<ServerBladeChecklist>(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml");
                            if (vm.LoadedChecklist.PODocumentPath != null)
                                vm.LoadedChecklist.PODocumentPath.Replace(@"\\", @"\");

                        }
                    



                        if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml"))
                        {
                            vm.LoadReport = new ServerBladeInspectionCheckListView(vm);

                            vm.LoadWindow = new ChecklistWindow(vm);

                            vm.LoadWindow.ShowDialog();
                        }

                        //Set the Checklist Message 
                        vm.ChecklistMessage = "Checklist for " + vm.ChassisSerialNo + " Completed!";

                        //Clear the form
                        vm.ClearChecklistControls();




                        break;
                case "GoodsOut":

                    //USES THE LOADEDCHECKLIST PROPERTY INSTEAD AS THIS WILL CONTAIN THE LOADED VALUES FROM TEST OR GOODS IN
                    //GOODS OUT
                   vm.LoadedChecklist.GoodsOUTQ1 = vm.GoodsOUTQ1;
                   vm.LoadedChecklist.GoodsOUTQ2 = vm.GoodsOUTQ2;
                    vm.LoadedChecklist.GoodsOUTQ3 = vm.GoodsOUTQ3;
                    vm.LoadedChecklist.GoodsOUTQ4 = vm.GoodsOUTQ4;
                    vm.LoadedChecklist.GoodsOUTQ5 = vm.GoodsOUTQ5;
                    vm.LoadedChecklist.GoodsOUTQ6 = vm.GoodsOUTQ6;
                    vm.LoadedChecklist.GoodsOUTQ7 = vm.GoodsOUTQ7;
                    vm.LoadedChecklist.GoodsOUTQ8 = vm.GoodsOUTQ8;
                    vm.LoadedChecklist.GoodsOUTQ9 = vm.GoodsOUTQ9;
                    vm.LoadedChecklist.GoodsOUTQ10 = vm.GoodsOUTQ10;
                    vm.LoadedChecklist.GoodsOUTQ11 = vm.GoodsOUTQ11;
                    vm.LoadedChecklist.GoodsOUTQ12 = vm.GoodsOUTQ12;
                    vm.LoadedChecklist.GoodsOUTQ13 = vm.GoodsOUTQ13;
                    vm.LoadedChecklist.GoodsOUTQ14 = vm.GoodsOUTQ14;
                    vm.LoadedChecklist.GoodsOUTQ15 = vm.GoodsOUTQ15;
                    vm.LoadedChecklist.GoodsOUTQ16 = vm.GoodsOUTQ16;
                    vm.LoadedChecklist.GoodsOUTQ17 = vm.GoodsOUTQ17;
                    vm.LoadedChecklist.GoodsOUTQ18 = vm.GoodsOUTQ18;
                    vm.LoadedChecklist.GoodsOUTQ19 = vm.GoodsOUTQ19;
                    vm.LoadedChecklist.GoodsOUTQ20 = vm.GoodsOUTQ20;
                    vm.LoadedChecklist.GoodsOUTQ21 = vm.GoodsOUTQ21;
                    vm.LoadedChecklist.GoodsOUTQ22 = vm.GoodsOUTQ22;
                    vm.LoadedChecklist.GoodsOUTQ23 = vm.GoodsOUTQ23;
                    vm.LoadedChecklist.GoodsOUTQ24 = vm.GoodsOUTQ24;
                    vm.LoadedChecklist.GoodsOUTQ25 = vm.GoodsOUTQ25;
                    vm.LoadedChecklist.GoodsOUTRep = vm.GoodsOUTRep;
                    //MessageBox.Show(vm.SalesOrderNumber);
                    vm.LoadedChecklist.SalesOrderNo = vm.SalesOrderNumber;
                        //vm.LoadedChecklist.PODocumentPath = vm.


                        //Insert SO if missing
                        //if (vm.SalesOrderNumber.Substring(0, 2) != "SO" || vm.SalesOrderNumber.Substring(0, 2) != "so")
                        //{
                        //    vm.SalesOrderNumber = "SO" + vm.SalesOrderNumber;
                        //}

                        //Write to CSV
                        StaticFunctions.CreateAppendCSV(new ReportServersProcessed(vm.GoodsOUTRep,vm.SalesOrderNumber, vm.LoadedChecklist.ChassisSerialNo, "Goods Out Technical", "OK", ""), @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed.csv");
                        //Write to DB
                        //InsertData(sqlite_conn,new ServersProcessedModel(vm.SalesOrderNumber,"","", vm.ChassisSerialNo, vm.GoodsOUTRep,vm.CurrentUserLocation, "Goods Out Technical","","OK","","","",""));



                        //UPDATE XML FILE OF THE SETTINGS;

                    if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.LoadedChecklist.ChassisSerialNo + ".xml"))
                    {
                        //If file already exists, then delete before creating a new one
                        File.Delete(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.LoadedChecklist.ChassisSerialNo + ".xml");

                        //Create XML File of Settings
                        XMLTools.WriteToXmlFile(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.LoadedChecklist.ChassisSerialNo + ".xml", vm.LoadedChecklist, false);


                    }
                    else
                    {
                        //Create XML File of Settings
                        XMLTools.WriteToXmlFile(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.LoadedChecklist.ChassisSerialNo + ".xml", vm.LoadedChecklist, false);
                    }



                        //if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml"))
                        //{
                        //    vm.LoadedChecklist = XMLTools.ReadFromXmlFile<ServerBladeChecklist>(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.ChassisSerialNo + ".xml");
                        //    if (vm.LoadedChecklist.PODocumentPath != null)
                        //        vm.LoadedChecklist.PODocumentPath.Replace(@"\\", @"\");

                        //}




                    if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists\" + vm.LoadedChecklist.ChassisSerialNo + ".xml"))
                    {
                        vm.LoadReport = new ServerBladeInspectionCheckListView(vm);

                        vm.LoadWindow = new ChecklistWindow(vm);

                        vm.LoadWindow.ShowDialog();
                    }

                    //Clear the form
                    vm.ClearChecklistControls();
                    break;
                default:
                    break;
            }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }




        //SQL FUNCTIONS

        //Create Connection to DB
        static SQLiteConnection CreateConnection(string pathtodb)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            //sqlite_conn = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            //sqlite_conn = new SQLiteConnection("Data Source=" + pathtodb + "; Version = 3; New = True; Compress = True; ");
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sqlite_conn;
        }




        //Insert Into Table
        public void InsertData(SQLiteConnection conn, ServersProcessedModel itm)
        {

            try
            {

                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "INSERT INTO Processed(ProcessedDate, ChassisSerial, Manufacturer, Model, POSONumber, Operative, OperativeLocation, Department, TimeCompleted, ServerState, FactoryReset, Notes, FaultDescription)" + " VALUES(@processeddate,@chassisserial,@manufacturer,@model,@posonumber,@operative,@operativelocation,@department,@timecompleted,@serverstate,@factoryreset,@notes,@faultdescription)";
                sqlite_cmd.CommandType = CommandType.Text;

                //LOOP THROUGH ADDED COLLECTION

                //Date must be in this format to work with SQL Lite
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@processeddate", DateTime.Now.ToString("yyyy-MM-dd")));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@chassisserial", itm.ChassisSerialNo));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", itm.Manufacturer));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@model", itm.ModelNo));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@posonumber", itm.POSONo));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@operative", itm.Operative));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@operativelocation", itm.OperativeLocation));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@department", itm.Department));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@timecompleted", itm.TimeCompleted));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@serverstate", itm.ServerState));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@factoryreset", itm.FactoryReset));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@notes", itm.Notes));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@faultdescription", itm.FaultDescription));

                sqlite_cmd.ExecuteNonQuery();




            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }






        public void ReadAllData(SQLiteConnection conn)
        {
            try
            {

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = "SELECT * FROM Processed";

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ServersProcessedModel itm = new ServersProcessedModel();
                    itm.DateProcessed = sqlite_datareader.GetDateTime(1);
                    itm.ChassisSerialNo = sqlite_datareader.GetString(2);
                    itm.ModelNo = sqlite_datareader.GetString(3);
                    itm.POSONo = sqlite_datareader.GetString(4);
                    itm.Operative = sqlite_datareader.GetString(5);
                    itm.OperativeLocation = sqlite_datareader.GetString(6);
                    itm.Department = sqlite_datareader.GetString(7);
                    itm.TimeCompleted = sqlite_datareader.GetString(8);
                    itm.ServerState = sqlite_datareader.GetString(9);
                    itm.FactoryReset = sqlite_datareader.GetString(10);
                    itm.Notes = sqlite_datareader.GetString(11);
                    itm.FaultDescription = sqlite_datareader.GetString(12);


                    vm.ServersProcessedCollection.Add(itm);



                }

                // await PutTaskDelay(400);



                // conn.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }



    }
}

