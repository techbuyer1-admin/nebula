﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Nebula.ViewModels;
using Nebula.Views;

namespace Nebula.Commands
{
    public class LoadContentCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;
        GoodsInOutViewModel vm2 = null; // new GoodsInOutViewModel();
        DriveTestViewModel vm3 = null; // new GoodsInOutViewModel();

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public LoadContentCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;

        }
        public LoadContentCommand(GoodsInOutViewModel TheViewModel)
        {
            this.vm2 = TheViewModel;
        }

        public LoadContentCommand(DriveTestViewModel TheViewModel)
        {
            this.vm3 = TheViewModel;
        }

        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }



        public async void Execute(object parameter)
        {


            var value = (object)parameter;
            var ViewToLoad = (string)value;


            // clear the static variable
            if (ViewToLoad.ToString() != "GoodsInChecklistView")
            {
                StaticFunctions.ChassisNoPassthrough = "";
            }


            switch (ViewToLoad.ToString())
            {
                case "MainMenuLandingPageView":

                    // Check loaded View is not landing page
                    if (vm.LoadContent.Name == "LandingPageViewUC")
                    {

                    }
                    else
                    {


                        try
                        {


                            //Works and gets around Context is not registered issue
                            var window = App.Current.Windows.OfType<MetroWindow>().FirstOrDefault(x => x.IsActive);
                            if (window != null)
                            {

                                MessageDialogResult answer = await vm.SystemMessageDialogYesNoCancel("Nebula Notification", App.Current.FindResource("ExitToMainMenu").ToString(), "YesNo");

                                //Check Answer
                                switch (answer.ToString())
                                {
                                    case "Affirmative":
                                        //BACK TO MAIN MENU
                                        vm.LoadContent = new LandingPageView(vm);
                                        //Reset Server Properties
                                        ResetServerProperties();
                                        break;

                                    case "Negative":
                                        //Reset Server Properties
                                        ResetServerProperties();
                                        break;

                                    case "Canceled":
                                        //Reset Server Properties
                                        ResetServerProperties();
                                        break;
                                }

                            }
                            else
                            {
                                //OLD MESSAGE DIALOG
                                vm.SystemMessage1 = App.Current.FindResource("ExitToMainMenu").ToString();
                                vm.ShowOverlay = Visibility.Visible;
                                //Wait for user to 
                                await OverlayDialog();
                            }




                        }
                        catch (Exception ex)
                        {
                            // MessageBox.Show(ex.Message);
                            //OLD MESSAGE DIALOG
                            vm.SystemMessage1 = App.Current.FindResource("ExitToMainMenu").ToString();
                            vm.ShowOverlay = Visibility.Visible;
                            //Wait for user to 
                            await OverlayDialog();

                        }


                    }


                    break;
                case "LandingPageView":

                    //Reset slot properties LandingPageViewUC
                    ResetServerProperties();
                    //Load landing page
                    vm.LoadContent = new LandingPageView(vm);

                    break;
                case "BarCodePrinterView":
                    vm.LoadContent = new BarCodePrinterView(vm);
                    break;

                case "Cedar":
                    vm.LoadContent = new CedarInteractionView(vm);
                    break;

                case "SerialSelectorView":

                    vm.LoadContent = new SerialSelectorView();
                    break;
                case "TestToolsView":
                    vm.LoadContent = new TestToolsView();

                    break;
                case "SyncServerUpdatesView":
                    vm.LoadContent = new SyncServerUpdatesView(vm);
                    break;
                case "SingleSerial2View":
                    vm.LoadContent = new SingleSerial2View(vm);
                    break;
                case "SerialClusterView":
                    vm.LoadContent = new SerialCluster2View(vm);
                    break;
                case "SerialCluster2View":
                    vm.LoadContent = new SerialClusterView(vm);
                    break;
                case "TestReportFlowDocument":
                    vm.LoadReport = new TestReportFlowDocument();
                    break;
                case "DriveProcessing":
                    vm.LoadContent = new DriveProcessing(new DriveTestViewModel(DialogCoordinator.Instance));
                    //vm.LoadContent = new HamburgerNavigation();
                    //vm.LoadContent = new DriveProcessing();
                    break;
                case "AddProductView":
                    vm.LoadContent = new AddProductView();
                    break;

                case "TerminalWindowView":
                    vm.LoadContent = new TerminalWindowView();
                    break;
                case "XTerminalWindow":
                    vm.LoadWindow = new XTerminalWindow();
                    vm.LoadWindow.Show();
                    break;
                case "GoodsInToolsView":
                    //MessageBox.Show(values.ToString());
                    vm.LoadContent = new GoodsInToolsView();
                    //MessageBox.Show(vm.LoadContent.ToString());
                    break;
                case "GoodsOutToolsView":
                    //MessageBox.Show(values.ToString()); 
                    vm.LoadContent = new GoodsOutToolsView();
                    //MessageBox.Show(vm.LoadContent.ToString());
                    break;
                case "ITADToolsView":
                    //MessageBox.Show(values.ToString());
                    vm.LoadContent = new ITADToolsView();
                    //MessageBox.Show(vm.LoadContent.ToString());
                    break;
                case "ITADReportingView":
                    vm.LoadContent = new ITADReportingView(new GoodsInOutViewModel(DialogCoordinator.Instance));
                    break;
                case "TestDepartmentReportsView":
                    vm.LoadContent = new TestDepartmentReportsView(new GoodsInOutViewModel(DialogCoordinator.Instance));
                    break;

                case "GoodsInChecklistView":
                    //vm.LoadContent = new GoodsInChecklistView();
                    vm.LoadContent = new CheckListView(vm, false);
                    break;
                case "GoodsOutChecklistView":
                    //vm.LoadContent = new GoodsOutChecklistView();
                    vm.LoadContent = new CheckListView(vm, false);
                    break;
                case "TestDeptChecklistView":
                    //vm.LoadContent = new TestDeptChecklistView();
                    vm.LoadContent = new CheckListView(vm, false);
                    break;
                case "WeeklyServerRecordsView":
                    vm.LoadContent = new WeeklyServerRecordsView(new GoodsInOutViewModel(DialogCoordinator.Instance));
                    break;
                case "DriveTestToolsView":
                    vm.LoadContent = new DriveTestToolsView();
                    break;
                case "QualityControlView":
                    vm.LoadContent = new QualityControlView(new GoodsInOutViewModel(DialogCoordinator.Instance));
                    break;
                case "IPScannerView":
                    vm.LoadContent = new IPScannerView();
                    break;
                case "ServerProcessingView":
                    vm.LoadContent = new ServerProcessingView(vm);
                    break;
                case "HPGenSelectionView":
                    vm.LoadContent = new HPGenSelectionView();

                    //tab header set back to default

                    //clear server info on exit
                    ResetServerProperties();

                    break;
                case "DellGenSelectionView":
                    vm.LoadContent = new DellGenSelectionView();

                    //clear server info on exit
                    ResetServerProperties();


                    break;
                case "IBMGenSelectionView":
                    vm.LoadContent = new IBMGenSelectionView();

                    //clear server info on exit
                    ResetServerProperties();


                    break;
                case "CiscoGenSelectionView":
                    vm.LoadContent = new CiscoGenSelectionView();
                    //clear server info on exit
                    ResetServerProperties();

                    break;
                case "HPServerGen8WipeView":
                    //single view
                    vm.ServerType = "HPGen8RackMount";

                    if (StaticFunctions.Department == "GoodsOutTech")
                    {

                        //vm.LoadContent = new HPGen8WipeMultiViewGOT(vm);
                        vm.LoadContent = new HPMultiServerView(vm, true, "Gen8");
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {

                        //vm.LoadContent = new HPGen8WipeMultiView(vm);

                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen8");
                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {
                        //vm.LoadContent = new HPGen8WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen8");
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        //vm.LoadContent = new HPGen8WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen8");
                    }

                    break;
                case "HPServerGen9WipeView":
                    vm.ServerType = "HPGen9RackMount";
                    if (StaticFunctions.Department == "GoodsOutTech")
                    {
                        //vm.LoadContent = new HPGen9WipeMultiViewGOT(vm);
                        vm.LoadContent = new HPMultiServerView(vm, true, "Gen9");
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {

                        //vm.LoadContent = new HPGen9WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen9");
                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {

                        //vm.LoadContent = new HPGen9WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen9");
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        //vm.LoadContent = new HPGen9WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen9");
                    }

                    break;
                case "HPServerGen10WipeView":
                    vm.ServerType = "HPGen10RackMount";
                    if (StaticFunctions.Department == "GoodsOutTech")
                    {
                        //vm.LoadContent = new HPGen10WipeMultiViewGOT(vm);
                        vm.LoadContent = new HPMultiServerView(vm, true, "Gen10");
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {

                        //vm.LoadContent = new HPGen10WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen10");
                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {

                        //vm.LoadContent = new HPGen10WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen10");
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        //vm.LoadContent = new HPGen10WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen10");
                    }

                    break;
                case "HPServerGen10AMDWipeView":
                    vm.ServerType = "HPGen10RackMount";
                    if (StaticFunctions.Department == "GoodsOutTech")
                    {
                        //vm.LoadContent = new HPGen10AMDWipeMultiViewGOT(vm);
                        vm.LoadContent = new HPMultiServerView(vm, true, "Gen10AMD");
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {

                        //vm.LoadContent = new HPGen10AMDWipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen10AMD");
                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {

                        //vm.LoadContent = new HPGen10AMDWipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen10AMD");
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        //vm.LoadContent = new HPGen10AMDWipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen10AMD");
                    }

                    break;
                case "HPServerGen8BladeWipeView":
                    //single view
                    vm.ServerType = "HPGen8Blade";

                    if (StaticFunctions.Department == "GoodsOutTech")
                    {

                        //vm.LoadContent = new HPGen8WipeMultiViewGOT(vm);
                        vm.LoadContent = new HPMultiServerView(vm, true, "Gen8");
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {

                        //vm.LoadContent = new HPGen8WipeMultiView(vm);

                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen8");
                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {
                        //vm.LoadContent = new HPGen8WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen8");
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        //vm.LoadContent = new HPGen8WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen8");
                    }
                    //Create View
                    vm.FlyOutContent = new HPEnclosureView(vm);

                    //Show Enclosure Dropdown
                    vm.ShowHPFlyout = true;

                    break;
                case "HPServerGen9BladeWipeView":
                    vm.ServerType = "HPGen9Blade";

                    if (StaticFunctions.Department == "GoodsOutTech")
                    {
                        //vm.LoadContent = new HPGen9WipeMultiViewGOT(vm);
                        vm.LoadContent = new HPMultiServerView(vm, true, "Gen9");
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {

                        //vm.LoadContent = new HPGen9WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen9");
                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {

                        //vm.LoadContent = new HPGen9WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen9");
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        //vm.LoadContent = new HPGen9WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen9");
                    }

                    //Create View
                    vm.FlyOutContent = new HPEnclosureView(vm);

                    //Show Enclosure Dropdown
                    vm.ShowHPFlyout = true;

                    break;
                case "HPServerGen10BladeWipeView":
                    vm.ServerType = "HPGen10Blade";

                    if (StaticFunctions.Department == "GoodsOutTech")
                    {
                        //vm.LoadContent = new HPGen10WipeMultiViewGOT(vm);
                        vm.LoadContent = new HPMultiServerView(vm, true, "Gen10");
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {

                        //vm.LoadContent = new HPGen10WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen10");
                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {

                        //vm.LoadContent = new HPGen10WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen10");
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        //vm.LoadContent = new HPGen10WipeMultiView(vm);
                        vm.LoadContent = new HPMultiServerView(vm, false, "Gen10");
                    }

                    //Create View
                    vm.FlyOutContent = new HPEnclosureView(vm);

                    //Show Enclosure Dropdown
                    vm.ShowHPFlyout = true;

                    break;



                case "GoodsOutDistributionToolsView":
                    //single view
                    //vm.LoadContent = new HPServerGen8WipeView(vm);
                    vm.LoadContent = new GoodsOutDistributionToolsView(vm);
                    break;

                case "TestCertificate":
                    //single view
                    //vm.LoadContent = new HPServerGen8WipeView(vm);
                    vm.LoadContent = new TestCertificateView(vm);
                    break;
                case "GoodsOutDistributionEmailView":
                    //single view
                    //vm.LoadContent = new HPServerGen8WipeView(vm);
                    vm.LoadContent = new GoodsOutDistributionEmailView();
                    break;

                case "CiscoMultiServerProcessingView1":
                    //clear server info on exit
                    ResetServerProperties();
                    vm.ServerType = "CiscoRackmount";

                    if (StaticFunctions.Department == "GoodsOutTech")
                    {
                        //vm.LoadContent = new CiscoMultiServerProcessingViewGOT(vm);
                        vm.LoadContent = new CiscoMultiServerView(vm, false);
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {
                        //vm.LoadContent = new CiscoMultiServerProcessingView(vm);
                        //vm.LoadContent = new CiscoMultiServerProcessingView(vm);
                        vm.LoadContent = new CiscoMultiServerView(vm, false);
                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {
                        vm.LoadContent = new CiscoMultiServerView(vm, false);
                        //vm.LoadContent = new CiscoMultiServerProcessingView(vm);
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        vm.LoadContent = new CiscoMultiServerView(vm, false);
                        //vm.LoadContent = new CiscoMultiServerProcessingView(vm);
                    }

                    break;
                case "CiscoMultiServerBladeProcessingView1":
                    //clear server info on exit
                    ResetServerProperties();
                    vm.ServerType = "CiscoBlade";

                    if (StaticFunctions.Department == "GoodsOutTech")
                    {
                        //vm.LoadContent = new CiscoMultiServerProcessingViewGOT(vm);
                        vm.LoadContent = new CiscoMultiServerView(vm, false);
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {
                        //vm.LoadContent = new CiscoMultiServerProcessingView(vm);
                        vm.LoadContent = new CiscoMultiServerView(vm, false);
                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {
                        //vm.LoadContent = new CiscoMultiServerProcessingView(vm);
                        vm.LoadContent = new CiscoMultiServerView(vm, false);
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        //vm.LoadContent = new CiscoMultiServerProcessingView(vm);
                        vm.LoadContent = new CiscoMultiServerView(vm, false);
                    }

                    break;
                case "DellMultiServerProcessingView":
                    //clear server info on exit
                    ResetServerProperties();
                    vm.ServerType = "DellRackmount";

                    if (StaticFunctions.Department == "GoodsOutTech")
                    {
                        //vm.LoadContent = new DellMultiServerProcessingViewGOT(vm);
                        vm.LoadContent = new DellMultiServerView(vm, false);
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {
                        //vm.LoadContent = new DellMultiServerProcessingView(vm);
                        //vm.LoadContent = new DellMultiServerProcessingView(vm);
                        vm.LoadContent = new DellMultiServerView(vm, false);
                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {
                        vm.LoadContent = new DellMultiServerView(vm, false);
                        //vm.LoadContent = new DellMultiServerProcessingView(vm);
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        vm.LoadContent = new DellMultiServerView(vm, false);
                        //vm.LoadContent = new DellMultiServerProcessingView(vm);
                    }

                    break;
                case "DellMultiServerBladeProcessingView":
                    //clear server info on exit
                    ResetServerProperties();
                    vm.ServerType = "DellBlade";

                    if (StaticFunctions.Department == "GoodsOutTech")
                    {
                        //vm.LoadContent = new DellMultiServerProcessingViewGOT(vm);
                        vm.LoadContent = new DellMultiServerView(vm, false);
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {
                        //vm.LoadContent = new DellMultiServerProcessingView(vm);
                        vm.LoadContent = new DellMultiServerView(vm, false);
                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {
                        //vm.LoadContent = new DellMultiServerProcessingView(vm);
                        vm.LoadContent = new DellMultiServerView(vm, false);
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        //vm.LoadContent = new DellMultiServerProcessingView(vm);
                        vm.LoadContent = new DellMultiServerView(vm, false);
                    }

                    //Create View
                    vm.FlyOutContent2 = new DELLEnclosureView(vm);

                    //Show Enclosure Dropdown
                    vm.ShowDELLFlyout = true;

                    break;
                case "LenovoMultiServerView":
                    //clear server info on exit
                    ResetServerProperties();
                    vm.ServerType = "LenovoRackmount";
                    vm.LenovoServerType = "XSystem";

                    if (StaticFunctions.Department == "GoodsOutTech")
                    {
                        vm.LoadContent = new LenovoMultiServerProcessingView(vm);
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {
                        vm.LoadContent = new LenovoMultiServerProcessingView(vm);

                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {
                        vm.LoadContent = new LenovoMultiServerProcessingView(vm);
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        vm.LoadContent = new LenovoMultiServerProcessingView(vm);
                    }

                    break;
                case "LenovoMultiThinkServerView":
                    //clear server info on exit
                    ResetServerProperties();
                    vm.ServerType = "LenovoRackmount";
                    vm.LenovoServerType = "XClarity";

                    if (StaticFunctions.Department == "GoodsOutTech")
                    {
                        vm.LoadContent = new LenovoMultiServerProcessingView(vm);
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {
                        vm.LoadContent = new LenovoMultiServerProcessingView(vm);

                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {
                        vm.LoadContent = new LenovoMultiServerProcessingView(vm);
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        vm.LoadContent = new LenovoMultiServerProcessingView(vm);
                    }

                    break;
                case "LenovoMultiServerBladeView":
                    //clear server info on exit
                    ResetServerProperties();
                    vm.ServerType = "LenovoBlade";

                    if (StaticFunctions.Department == "GoodsOutTech")
                    {
                        vm.LoadContent = new LenovoMultiServerProcessingView(vm);
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {
                        vm.LoadContent = new LenovoMultiServerProcessingView(vm);

                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {
                        vm.LoadContent = new LenovoMultiServerProcessingView(vm);
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        vm.LoadContent = new LenovoMultiServerProcessingView(vm);
                    }

                    break;
                case "ServersProcessedView":
                    vm.LoadContent = new ServersProcessedView();
                    break;
                case "SecureShellSingleView":
                    vm.LoadContent = new SecureShellSingleView();
                    break;
                case "CommandToolsView":
                    vm.LoadContent = new CommandToolsView();
                    break;
                case "ProcessCommandsView":
                    vm.LoadContent = new ProcessCommandsView(vm);
                    break;
                case "PowershellView":
                    vm.LoadContent = new PowershellView();
                    break;
                case "ReturnToToolsView":
                    if (StaticFunctions.Department == "GoodsOutTech")
                    {
                        vm.LoadContent = new GoodsOutToolsView();
                    }
                    else if (StaticFunctions.Department == "GoodsIn")
                    {
                        vm.LoadContent = new GoodsInToolsView();
                    }
                    else if (StaticFunctions.Department == "ITAD")
                    {
                        vm.LoadContent = new ITADToolsView();
                    }
                    else if (StaticFunctions.Department == "TestDept")
                    {
                        vm.LoadContent = new TestToolsView();
                    }
                    break;
                case "ServerInventoryView":
                    vm.LoadContent = new ServerInventoryView(vm);
                    //vm.LoadContent = new HPGen8WipeMultiView(vm); 
                    break;
                case "SerialEmulator":
                    //MessageBox.Show(values.ToString());
                    vm.LoadContent = new SerialEmulator();
                    break;

                case "OCR":
                    //MessageBox.Show(values.ToString());
                    vm.LoadContent = new OCRDetectionView();
                    break;
                    //case "TestReportFlowDocument":
                    //    vm.LoadContent = new TestReportFlowDocument();
                    //    break;
                    //case "FlowDocument1":
                    //    vm.LoadContent = new FlowDocument1();
                    //    break;

            }





            //vm.ReturnItemsCount(vm.BadgeValue, vm.BadgeValue2);
            // MessageBox.Show("Badge Values are: " + vm.ReturnItemsCount(vm.BadgeValue,vm.BadgeValue2));
        }



        public void ResetServerProperties()
        {
            //SERVER IP'S
            vm.Server1IPv4 = string.Empty;
            vm.Server2IPv4 = string.Empty;
            vm.Server3IPv4 = string.Empty;
            vm.Server4IPv4 = string.Empty;
            vm.Server5IPv4 = string.Empty;
            vm.Server6IPv4 = string.Empty;
            vm.Server7IPv4 = string.Empty;
            vm.Server8IPv4 = string.Empty;
            vm.Server9IPv4 = string.Empty;
            vm.Server10IPv4 = string.Empty;
            vm.Server11IPv4 = string.Empty;
            vm.Server12IPv4 = string.Empty;
            vm.Server13IPv4 = string.Empty;
            vm.Server14IPv4 = string.Empty;
            vm.Server15IPv4 = string.Empty;
            vm.Server16IPv4 = string.Empty;
            //HP OBJECTS
            vm.GenGenericServerInfo1 = null;

            vm.BIOSUpgradePath1 = string.Empty;
            vm.ILOUpgradePath1 = string.Empty;
            vm.SPSUpgradePath1 = string.Empty;
            vm.IENGUpgradePath1 = string.Empty;
            //Dell
            vm.DellServerS1 = null;
            vm.ILORestOutput1 = string.Empty;
            vm.CONTUpgradePath1 = string.Empty;
            vm.IDRACUpgradePath1 = string.Empty;
            vm.DELLFrontLCDS1 = string.Empty;
            //Lenovo
            vm.LenovoServerS1 = null;
            vm.IMMUpgradePath1 = string.Empty;
            vm.DSAUpgradePath1 = string.Empty;
            vm.LenovoServerType = string.Empty;

            //Cisco


            //Tabs & Messages
            vm.ServerType = string.Empty;
            vm.WarningMessage1 = string.Empty;
            vm.ProgressMessage1 = string.Empty;

            vm.Server1TabHeader = "Server1";
            vm.Server2TabHeader = "Server2";
            vm.Server3TabHeader = "Server3";
            vm.Server4TabHeader = "Server4";
            vm.Server5TabHeader = "Server5";
            vm.Server6TabHeader = "Server6";
            vm.Server7TabHeader = "Server7";
            vm.Server8TabHeader = "Server8";
            vm.Server9TabHeader = "Server9";
            vm.Server10TabHeader = "Server10";
            vm.Server11TabHeader = "Server11";
            vm.Server12TabHeader = "Server12";
            vm.Server13TabHeader = "Server13";
            vm.Server14TabHeader = "Server14";
            vm.Server15TabHeader = "Server15";
            vm.Server16TabHeader = "Server16";


            vm.FlipView1Index = 0;


            //Set default colours for tabs
            vm.TabColorS1 = Brushes.MediumPurple;
            vm.TabColorS2 = Brushes.MediumPurple;
            vm.TabColorS3 = Brushes.MediumPurple;
            vm.TabColorS4 = Brushes.MediumPurple;
            vm.TabColorS5 = Brushes.MediumPurple;
            vm.TabColorS6 = Brushes.MediumPurple;
            vm.TabColorS7 = Brushes.MediumPurple;
            vm.TabColorS8 = Brushes.MediumPurple;
            vm.TabColorS9 = Brushes.MediumPurple;
            vm.TabColorS10 = Brushes.MediumPurple;
            vm.TabColorS11 = Brushes.MediumPurple;
            vm.TabColorS12 = Brushes.MediumPurple;
            vm.TabColorS13 = Brushes.MediumPurple;
            vm.TabColorS14 = Brushes.MediumPurple;
            vm.TabColorS15 = Brushes.MediumPurple;
            vm.TabColorS16 = Brushes.MediumPurple;


        }



        //OVERLAY
        //For use with the GridBased Message overlay. Grid overlay is hosted on MainWindow.xaml It uses the MainViewModel Visibility property vm.ShowOverlay
        //Call OverlayDialog calls on Task that waits until the message visibility is collapsed or hidden
        async Task OverlayDialog()
        {
            //await Task.Delay(millSecs);
            //loop until user presses yes
            await Task.Run(() => WaitForOverlay());

        }

        //Task to hold the loop
        async Task WaitForOverlay()
        {
            //Loop and check that the message visibilty has changed or not
            do
            {
                // do nothing, just wait until a button is pressed.
                // If yes, reload the Main Menu, if not just exit

            } while (vm.ShowOverlay == Visibility.Visible);


        }





    }
}
