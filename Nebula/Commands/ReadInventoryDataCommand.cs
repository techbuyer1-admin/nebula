﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class ReadInventoryDataCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        
        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public ReadInventoryDataCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //if (lv != null)
            //{
            //    if (lv.SelectedItems.Count > 0)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    return true;
            //}
            return true;
        }

        public void Execute(object parameter)
        {

            var values = (object)parameter;


            //Read Netsuite Inventory CSV and add to collection
            AddToCollection(values.ToString());

         

        }


        //public bool Like(this string s, string pattern, RegexOptions options = RegexOptions.IgnoreCase)
        //{
        //    return Regex.IsMatch(s, pattern, options);
        //}




        public string AddToCollection(string csvPath)
        {
            try
            {

          

            vm.NetsuiteInventoryData = new System.Collections.ObjectModel.ObservableCollection<NetsuiteInventory>();

            string TheYear = DateTime.Now.Year.ToString();

            if (File.Exists(csvPath))
            {
                //Will Create a new file for each year
                string newFileName = csvPath;
                //string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv";
                //Uses a VB class to process the file instead of streamreader
                using (TextFieldParser parser = new TextFieldParser(newFileName))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        string currentLine = parser.ReadLine();

                     

                        vm.NetsuiteInventoryData.Add(FromCsv(currentLine));

                        //TODO: Process field
                        //}
                    }
                }




            }

            //count items
            vm.NetsuiteInventoryCount = vm.NetsuiteInventoryData.Count();
           // MessageBox.Show(vm.NetsuiteInventoryData.Count().ToString());

            return "";
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                return "";
            }
        }

        public NetsuiteInventory FromCsv(string csvLine)
        {

            if (csvLine != null)
            {
                string[] values = csvLine.Split(',');
                NetsuiteInventory inv = new NetsuiteInventory();
                //Internal ID
                //if (values[0] != string.Empty)
                //    inv.InternalID = Convert.ToString(values[0]);
                //Product Code
                if (values[0] != string.Empty)
                    //MessageBox.Show(values[0]);
                    inv.ProductCode = Convert.ToString(values[0]);
                //Description
                if (values[1] != string.Empty)
                    //MessageBox.Show(values[1]);
                inv.ProductDescription = Convert.ToString(values[1]);
               
                return inv;
            }

            return null;
        }


      



    }
}
