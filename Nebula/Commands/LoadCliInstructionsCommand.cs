﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class LoadCliInstructionsCommand : ICommand
    {
        //declare viewmodel
        MainViewModel vm = null;



        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public LoadCliInstructionsCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //use helper to get control
           // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

            //if (lv != null)
            //{


            //    if (lv.SelectedItems.Count > 0)
            //    {

            //        //MessageBox.Show(string.IsNullOrEmpty(vm.StepDescription).ToString() + "   " + string.IsNullOrEmpty(vm.StepCliCommand).ToString());
            //        //return false;  
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //        //return true;
            //    }


            //}
            //else
            //{
            //    return false;
            //}

            return true;

        }

      


        public void Execute(object parameter)
        {
            //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

            // https://social.technet.microsoft.com/wiki/contents/articles/18199.event-handling-in-an-mvvm-wpf-application.aspx


            var value = (object)parameter;
            //set to blank
            //vm.SelectedAutomationProfile = "";

            ////reset password unlock flags on change of script profile
            //vm.PasswordUnlockRun(null, "Reset");
            Console.WriteLine("CLI Command");


            if (value != null)
            if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML Product Wipe Profiles\" + value.ToString() + ".xml"))
               {

                Console.WriteLine("CLI Command Load");
                // LOAD PREVIOUS SETTINGS FROM AN XML FILE;
                //vm.SelectedProduct = XMLTools.ReadFromXmlFile<AutomatedCliModel>(@"C:\Users\NJM\Desktop\Invoice App" + value.ToString() + ".xml");
                vm.SelectedProduct = XMLTools.ReadFromXmlFile<AutomatedCliModel>(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML Product Wipe Profiles\" + value.ToString() + ".xml");
                //Clear the Container
                vm.ProductCollectionList.Clear();
                vm.StepSequenceCollectionForm.Clear();
                //set the property which states which profile is selected, this is used to get around the erase startup config command to the final report
                vm.SelectedAutomationProfile = value.ToString();
                // MessageBox.Show(adm2.ContactName);
                vm.ProductBrandForm = vm.SelectedProduct.Brand;
                vm.ProductCategoryForm = vm.SelectedProduct.Category;
                vm.ProductModelNoForm = vm.SelectedProduct.ModelNo;
                vm.SequenceTypeForm = vm.SelectedProduct.SequenceType;

               //   MessageBox.Show(vm.SequenceTypeForm);
                
                //USE HELPER TO LOCATE COMBOBOX REQUIRES SELECTED VALUEPATH SETTING TO CONTENT ON THE CONTROL   
                //ComboBox cb1 = Helper.GetDescendantFromName(Application.Current.MainWindow, "TypeCB") as ComboBox;
                //cb1.SelectedValue = vm.SequenceTypeForm;

                foreach (var step in vm.SelectedProduct.Steps)
                {
                    vm.StepSequenceCollectionForm.Add(step);
                }

                    if(vm.ProductBrand != null)
                    if (vm.ProductBrand.Contains("Cisco") && value.ToString().Contains("Switch") || vm.ProductBrand.Contains("Cisco") && value.ToString().Contains("switch") || vm.ProductBrand.Contains("Cisco") && value.ToString().Contains("catalyst") || vm.ProductBrand.Contains("Cisco") && value.ToString().Contains("Catalyst"))
                    {
                        vm.DialogMessage("Possible Stacking Switch!", "Please check if the Switch was a member of a stack by using the 'show switch' command.\n\n" +
                            "If the switch is listed as anything greater than 1 in the previous stack, you will need to renumber the switch.\n\n" +
                            "For Example: (If the Switch was number 2 in the previous stack)\n\n" +
                            "(1) Enter Switch(config) mode.\n" +
                            "(2) switch 2 renumber 1\n" +
                            "(3) write\n" +
                            "(4) reload\n" +
                            "(5) no switch 2 provision\n" +
                            "(6) write\n" +
                            "(7) reload\n" +
                            "(8) Exit config mode.\n\n"+
                            "Please check again using the show switch command. If this has been renumbered correctly, run the Automated Script as Normal\n\n" +
                            "Thank You!");
                    }

                }
            else
            {
                    vm.DialogMessage("Error loading file", "File is unavailable....");
               // MessageBox.Show("File not available");
            }

          


            ////Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

            //// https://social.technet.microsoft.com/wiki/contents/articles/18199.event-handling-in-an-mvvm-wpf-application.aspx


            //var value = (object)parameter;
            ////set to blank
            //vm.SelectedAutomationProfile = "";



            //if (value != null)
            //    if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML Product Wipe Profiles\" + value.ToString() + ".xml"))
            //    {
            //        // LOAD PREVIOUS SETTINGS FROM AN XML FILE;
            //        //vm.SelectedProduct = XMLTools.ReadFromXmlFile<AutomatedCliModel>(@"C:\Users\NJM\Desktop\Invoice App" + value.ToString() + ".xml");
            //        vm.SelectedProduct = XMLTools.ReadFromXmlFile<AutomatedCliModel>(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML Product Wipe Profiles\" + value.ToString() + ".xml");
            //        //Clear the Container
            //        vm.ProductCollectionList.Clear();
            //        vm.StepSequenceCollection.Clear();
            //        //set the property which states which profile is selected, this is used to get around the erase startup config command to the final report
            //        vm.SelectedAutomationProfile = value.ToString();
            //        // MessageBox.Show(adm2.ContactName);
            //        //vm.ProductBrand = autocli.Brand;
            //        //vm.ProductCategory = autocli.Category;
            //        //vm.ProductModelNo = autocli.ModelNo;

            //        foreach (var step in vm.SelectedProduct.Steps)
            //        {
            //            vm.StepSequenceCollection.Add(step);
            //        }




            //    }
            //    else
            //    {
            //        vm.DialogMessage("Error loading file", "File is unavailable....");
            //        // MessageBox.Show("File not available");
            //    }





            //Add the read XML File to the running collection
            // vm.ProductCollectionList.Add();
            // Read Image as Bytes create a new file and load into the control, gets around the printing issue where urisource or streamsource was blank                             
            // vm.ImageLocation = BitmapHelper.ByteToBitmapImage2File(adm2.Image, @"" + vm.dsktop + @"\Invoice App\InvoiceLogo.png");


            //error on print function
            //vm.ImageLocation = BitmapHelper.convertByteToBitmapImage(adm2.Image);














            //////use helper to get control
            //ComboBox cb1 = Helper.GetDescendantFromName(Application.Current.MainWindow, "ProductListCB") as ComboBox;



            ////Call the static function to return any file with an xml extension
            //vm.ProductList = StaticFunctions.GetFileNamesOnly(@"" + vm.dsktop + @"\Invoice App", "*.xml");

            ////foreach (var file in listFiles)
            ////{
            ////    vm.ProductList.Add(file.);
            ////    MessageBox.Show(file);
            ////}


            ////if (File.Exists(@"" + vm.dsktop + @"\Invoice App\Cisco 1801 Router.xml"))
            ////{
            //// LOAD PREVIOUS SETTINGS FROM AN XML FILE;







            ////foreach (var col in vm.ProductCollectionList)
            ////{
            ////    MessageBox.Show(col.Brand + " " + col.Category + " " + col.ModelNo);

            ////    foreach(var step in col.Steps)
            ////    {
            ////        // can add to another steps collection
            ////        MessageBox.Show(step.CliCommandToSend);

            ////    }
            ////}



            ////}

            ////Clear the controls on the page
            //vm.ClearAddProduct();

        }


        }
}
