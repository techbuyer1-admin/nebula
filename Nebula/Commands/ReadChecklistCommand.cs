﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Forms;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class ReadChecklistCommand : ICommand
    {

        //declare viewmodel
        GoodsInOutViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public ReadChecklistCommand(GoodsInOutViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //if (lv != null)
            //{
            //    if (lv.SelectedItems.Count > 0)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    return true;
            //}
            return true;
        }

        public void Execute(object parameter)
        {


            var values = (object)parameter;



            //MessageBox.Show(values.ToString());
            ////use helper to get control
            // ComboBox cb1 = Helper.GetDescendantFromName(Application.Current.MainWindow, "ProductListCB") as ComboBox;

            //Clear the Loaded Checklist
            vm.LoadedChecklist = null;
            vm.ChecklistMessage = String.Empty;
            //Call the static function to return any file with an xml extension
            //vm.ProductList  = StaticFunctions.GetFileNamesOnly(@"" + vm.dsktop + @"\Invoice App\XML Product Wipe Profiles", "*.xml");
            //  vm.LoadedChecklist = StaticFunctions.GetFileNamesOnlyBasedOnValue(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML Product Wipe Profiles", "*.xml", values.ToString());

            string filePath = "";

            //MessageBox.Show(value.ToString());
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = @"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists";
                openFileDialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    //using (StreamReader reader = new StreamReader(fileStream))
                    //{
                    //    fileContent = reader.ReadToEnd();
                    //}

                    if (filePath != null)
                        if (File.Exists(filePath))
                        {
                            // LOAD CHECKLIST SETTINGS FROM AN XML FILE;

                            vm.LoadedChecklist = XMLTools.ReadFromXmlFile<ServerBladeChecklist>(filePath);
                            if(vm.LoadedChecklist.PODocumentPath != null)
                            vm.LoadedChecklist.PODocumentPath.Replace(@"\\", @"\");

                                  //MessageBox.Show(vm.LoadedChecklist.PODocumentPath);
                        }
                        else
                        {
                            // vm.DialogMessage("Error loading file", "File is unavailable....");
                            MessageBox.Show("File not available");
                        }

                    fileStream.Close();
                }
            }


          
        }
    }
}
