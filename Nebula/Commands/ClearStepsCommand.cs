﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class ClearStepsCommand : ICommand
    {
        //declare viewmodel
        MainViewModel vm = null;



        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public ClearStepsCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            ListView lv = null;
            //Get array of objects
            var values = (object[])parameter;
           
            //Split array into specific types
            if(values != null)
            {
               lv = (ListView)values[1];
            }
           
            //var tbDesc = (TextBox)values[2];
            //var tbCli = (TextBox)values[3];
            //use helper to get control
            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

            if (lv != null)
            {


                if (lv.SelectedItems.Count > 0)
                {

                    //MessageBox.Show(string.IsNullOrEmpty(vm.StepDescription).ToString() + "   " + string.IsNullOrEmpty(vm.StepCliCommand).ToString());
                    //return false;  
                    return true;
                }
                else
                {
                    return false;
                    //return true;
                }


            }
            else
            {
                return false;
            }

        }

        public void Execute(object parameter)
        {
            //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

            //MessageBox.Show(@"" + vm.dsktop + @"\Invoice App\ClientDetails.xml");
            //@"" + vm.myDocs + @

            //vm.ProductCollectionList = new ObservableCollection<AutomatedCliModel>();
            //vm.StepSequenceCollection = new ObservableCollection<StepItem>();

            //vm.ProductCollectionList.Clear();
            //vm.StepSequenceCollection.Clear();

            //Get array of objects
            var values = (object[])parameter;
            //Split array into specific types
            var action = (string)values[0];
            var lv = (ListView)values[1];
            //var tbDesc = (TextBox)values[2];
            //var tbCli = (TextBox)values[3];


            //use helper to get control
            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

            switch (action)
            {
                //case "Add":
                //    StepItem stepseqadd = new StepItem();
                //    // stepseq.StepNo = 1; //vm.StepSequenceCollection.Count() + 1;
                //    stepseqadd.StepNo = vm.StepSequenceCollection.Count() + 1;
                //    stepseqadd.StepDescription = vm.StepDescription;
                //    stepseqadd.CliCommandToSend = vm.StepCliCommand;

                //    vm.StepSequenceCollection.Add(stepseqadd);


                //    // MessageBox.Show(@"Added Step");

                //    break;


                //case "Insert":
                //    StepItem stepseqins = new StepItem();
                //    // stepseq.StepNo = 1; //vm.StepSequenceCollection.Count() + 1;
                //    stepseqins.StepNo = lv.SelectedIndex + 1; //vm.StepSequenceCollection.Count() + 1;
                //    stepseqins.StepDescription = vm.StepDescription;
                //    stepseqins.CliCommandToSend = vm.StepCliCommand;

                //    vm.StepSequenceCollection.Insert(lv.SelectedIndex, stepseqins);


                //    // MessageBox.Show(@"Added Step");

                //    break;

                //case "Delete":
                //    // Delete a step

                //    //MessageBox.Show(lv.SelectedItem.ToString());
                //    //lv.ItemsSource..Remove(lv.SelectedItem);

                //    //To remove a bound item you need to modify the collection and not the control directly. you can use the helper function to find which item is selected.
                //    vm.StepSequenceCollection.RemoveAt(lv.SelectedIndex);
                //    // MessageBox.Show(@"Item Removed");
                //    break;

                case "Clear":
                    // Clear the entire collection
                    vm.StepSequenceCollectionForm.Clear();

                    break;


                default:
                    break;
            }

            //Clear the controls on the page
            vm.ClearSteps();

        }
    }
}

