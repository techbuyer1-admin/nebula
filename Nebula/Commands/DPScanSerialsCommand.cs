﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class DPScanSerialsCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public DPScanSerialsCommand(DriveTestViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //Get array of objects
            var values = (object[])parameter;

            //Required if parameter is a control reference
            if (values != null)
            {
                //Split array into specific types
                var lvValue = (ListView)values[0];
                var serialTXT = (TextBox)values[1];
                var anyExtras = (string)values[2];

                if (lvValue != null)
                {
                    if (lvValue.SelectedItems.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                       if(anyExtras == "Add Extras")
                        {
                            if(lvValue.Items.Count != 0)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }

                            
                        }
                        else
                        {
                            return true;
                        }
                       
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }


        }

        public void Execute(object parameter)
        {
            try
            {

          
            //Get array of objects
            var values = (object[])parameter;
            //Split array into specific types
            var lvValue = (ListView)values[0];
            var serialTXT = (TextBox)values[1];
            var anyExtras = (string)values[2];
            var PItabCTRL = (TabControl)values[3];

            //Switch to Serials Tab
            PItabCTRL.SelectedIndex = 0;

            //Clear the messages
            vm.ImportMessage = "";
            vm.ProcessedMessage = "";
            //Disable Import List Whilst Scanning
            //vm.ImportListEnabled = false;

            //Selected
            //if(lvValue.SelectedIndex > 0)
            //{
            //    if(vm.ScannedSerialCollection.Count == 0)
            //    {
            //        lvValue.SelectedIndex = 0;
            //    }

            //}
            //else
            //{
            //    lvValue.SelectedIndex = 0;
            //}


            //vm.ImportLVSelectedItem = vm.ImportListLastIndex


            if (anyExtras == "Add Extras")
            {
                vm.FocusOnProductCodeTXT = true;
                //DESELECT ITEMS
                lvValue.SelectedIndex = -1;
                //Get the values from the selected item
                vm.GetSelectedImportViewItem("Add Extras");
                //vm.FocusOnProductCodeTXT = false;
                //Focus on TextBox
                vm.FocusOnProductCodeTXT = true;
            }
            else
            {
                vm.FocusOnProductCodeTXT = true;
                //Get the values from the selected item
                vm.GetSelectedImportViewItem();

                //Scanserial command
                vm.SerialTBEnabled = true;
                //vm.FocusOnSerialTXT = false;
                //Focus on TextBox
                vm.FocusOnSerialTXT = true;
              
            }




                //serialTXT.IsEnabled = true;
                // serialTXT.Focus();


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }




    }
}




////SetCounter
//foreach (NetsuiteInventory itm in lvValue.SelectedItems)
//{
//    vm.SerialsCount = Convert.ToInt32(itm.Quantity);

//    //Check if various and show inputs
//    if (itm.ProductCode.Contains("VARIOUS") || itm.ProductCode.Contains("various") || itm.ProductCode.Contains("Various"))
//    {
//        vm.ShowVariousSection = Visibility.Visible;
//    }
//    else
//    {
//        vm.ShowVariousSection = Visibility.Collapsed;
//    }
//    //show pop up menu to add custom items
//    vm.ShowPopup = true;
//}