﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;
using Newtonsoft.Json.Linq;

namespace Nebula.Commands
{
    public class GetHPServerDataCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //To Hold Reference to the hosting tab
        string ServerTabHeader;
        TabItem ServerTab = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public GetHPServerDataCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            if (vm.Server1IPv4 != null)
            {
                //check if valid ip entered
                //System.Net.IPAddress ipAddress = null;
                //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                if (vm.Server1IPv4 != string.Empty)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public async void Execute(object parameter)
        {


            var values = (object[])parameter;
            var ScriptChoice = (string)values[0];
            var ServerGen = (string)values[1];

            //A: Setup and stuff you don/"t want timed
            var timer = new Stopwatch();
            timer.Start();
            TimeSpan timeTaken;
            string timetaken;

            string SyncLog = "";

            // Create new redfish instance
            RedfishRestful RedfishCrawler = new RedfishRestful();



            try
            {


                Console.WriteLine(vm.StorageDepth);

                SyncLog += "[Start of Nebula Sync Log]\r\r" + DateTime.Now.ToString() + "\r\r";



                switch (vm.WhichTab)
                {
                    case "1":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS1") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server1";
                        break;
                    case "2":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS2") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server2";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "3":
                        //MessageBox.Show("Server On Tab 3");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS3") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server3";
                        break;
                    case "4":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS4") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server4";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "5":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS5") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server5";
                        break;
                    case "6":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS6") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server6";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "7":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS7") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server7";
                        break;
                    case "8":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS8") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server8";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "9":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS9") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server9";
                        break;
                    case "10":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS10") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server10";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "11":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS11") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server11";
                        break;
                    case "12":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS12") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server12";
                        // MessageBox.Show("Server On Tab 2");
                        break;

                    case "13":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS13") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server13";
                        // MessageBox.Show("Server On Tab 2");
                        break;

                    case "14":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS14") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server14";
                        // MessageBox.Show("Server On Tab 2");
                        break;

                    case "15":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS15") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server15";
                        // MessageBox.Show("Server On Tab 2");
                        break;

                    case "16":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS16") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server16";
                        // MessageBox.Show("Server On Tab 2");
                        break;


                }



                vm.TabColorS1 = Brushes.Orange;


                if (vm.Server1IPv4 != null)
                {
                    if (vm.Server1IPv4 != string.Empty)
                    {
                        string address = vm.Server1IPv4;
                        vm.WarningMessage1 = "";
                        vm.ProgressMessage1 = "";
                        //send a ping to the server to check it's available
                        vm.ProgressMessage1 = "Checking Server IP, Please Wait...";
                        vm.ProgressPercentage1 = 0;
                        vm.ProgressIsActive1 = true;
                        vm.ProgressVisibility1 = Visibility.Visible;
                        sendAsyncPingPacket(address);

                        await PutTaskDelay(3000);

                        //if it is run the code
                        if (vm.IsServerThere1 == "Yes")
                        {



                            //Clear data files
                            ClearXMLJSONFiles();

                            ProcessPiper pp = new ProcessPiper(vm);

                            //RESET THE COMMAND BOX
                            vm.ILORestOutput1 = "";



                            //CANCEL SCRIPT
                            if (vm.CancelScriptS1 == true)
                            {
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                vm.WarningMessage1 = "";
                                vm.CancelScriptS1 = false;
                                return;
                            }
                            //END CANCEL SCRIPT



                            switch (ScriptChoice)
                            {

                                case "ServerInfo":

                                    //Upload to Repository and directly flash it
                                    // await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "uploadcomp --component " + vm.Server1IPv4 + "IENGUpdate.bin --update_repository --update_target  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                    //MessageBox.Show("Here");

                                    vm.ProgressPercentage1 = 0;
                                    vm.ProgressIsActive1 = true;
                                    vm.ProgressVisibility1 = Visibility.Visible;
                                    vm.ProgressMessage1 = "Receiving Information From Server, Please Wait...";

                                    //CANCEL SCRIPT
                                    if (vm.CancelScriptS1 == true)
                                    {
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                        return;
                                    }
                                    //END CANCEL SCRIPT


                                    vm.ProgressPercentage1 = 20;



                                    //**** REDFISH GET SERVER INFO!!! *****
                                    vm.GenGenericServerInfo1 = await RedfishCrawler.GetHPData(vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);

                                    //TPM CHECK DEPENDING ON GENERATION
                                    switch (ServerGen)
                                    {
                                        case "Gen8":

                                            break;
                                        case "Gen9":

                                            break;
                                        case "Gen10":
                                            //TPM CHECK FOR GEN 10
                                            if (vm.GenGenericServerInfo1.TpmVisibility == "Visible")
                                            {
                                                // ********* TPM ENABLE DISABLE ***********
                                                //DISABLE TPM
                                                await RedfishCrawler.RedfishPatchCall("https://" + vm.Server1IPv4 + "/redfish/v1/Systems/1/BIOS/Settings", new HttpMethod("PATCH"), "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"Attributes\":{\"TpmVisibility\":\"Hidden\"}}");

                                                // ENABLE TPM
                                                // await RedfishCrawler.RedfishPatchCall("https://" + vm.Server1IPv4 + "/redfish/v1/Systems/1/BIOS/Settings", HttpMethod.Put, "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"Attributes\":{\"TpmVisibility\":\"Visible\"}}");

                                                // MessageBox.Show("Redfish Reset Power");

                                                await RedfishCrawler.RedfishActionCall("https://" + vm.Server1IPv4 + "/redfish/v1/Systems/1/Actions/ComputerSystem.Reset/", HttpMethod.Post, "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"ResetType\":\"ForceRestart\"}}");

                                                vm.ProgressMessage1 = "TPM set to disabled, Rebooting to apply settings Please Wait...";
                                                vm.WarningMessage1 += "TPM present in system!!! This will need to be removed after the factory reset is complete. ";
                                                //Wait 3 mins for reboot
                                                await PutTaskDelay(200000);

                                                //**** REDFISH GET SERVER INFO!!! *****
                                                vm.GenGenericServerInfo1 = await RedfishCrawler.GetHPData(vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);
                                            }

                                            //// TPM RE-ENABLE
                                            //if (vm.GenGenericServerInfo1.TpmVisibility == "Hidden")
                                            //{
                                            //    // ********* TPM ENABLE DISABLE ***********
                                            //    //DISABLE TPM
                                            //    //await RedfishCrawler.RedfishPatchCall("https://" + vm.Server1IPv4 + "/redfish/v1/Systems/1/BIOS/Settings", HttpMethod.Put, "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"Attributes\":{\"TpmVisibility\":\"Hidden\"}}");

                                            //    // ENABLE TPM
                                            //    await RedfishCrawler.RedfishPatchCall("https://" + vm.Server1IPv4 + "/redfish/v1/Systems/1/BIOS/Settings", HttpMethod.Put, "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"Attributes\":{\"TpmVisibility\":\"Visible\"}}");

                                            //    // MessageBox.Show("Redfish Reset Power");

                                            //    await RedfishCrawler.RedfishActionCall("https://" + vm.Server1IPv4 + "/redfish/v1/Systems/1/Actions/ComputerSystem.Reset/", HttpMethod.Post, "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"ResetType\":\"ForceRestart\"}}");

                                            //    vm.ProgressMessage1 = "TPM set to disabled, Rebooting to apply settings Please Wait...";
                                            //    await PutTaskDelay(200000);

                                            //    //**** REDFISH GET SERVER INFO!!! *****
                                            //    vm.GenGenericServerInfo1 = await RedfishCrawler.GetHPData(vm.Server1IPv4, vm.OverridePasswordS1 , vm, ServerGen);
                                            //}
                                            break;
                                    }




                                    //IF TPM VISIBLE DISABLE /redfish/v1/Systems/1/Actions/ComputerSystem.Reset/
                                    //await RedfishCrawler.DellGetSessionID("https://" + ServerIP + "/redfish/v1/managers/1/embeddedmedia/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);

                                    //CANCEL SCRIPT
                                    if (vm.CancelScriptS1 == true)
                                    {
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                        return;
                                    }
                                    //END CANCEL SCRIPT



                                    //OUT OF REGION CHECK
                                    if (vm.GenGenericServerInfo1 != null)
                                    {
                                        vm.Server1TabHeader = vm.Server1TabHeader.Replace(" (ILO" + vm.GenGenericServerInfo1.ChassisSerial + ")", "");
                                        vm.Server1TabHeader = vm.Server1TabHeader + " (ILO" + vm.GenGenericServerInfo1.ChassisSerial + ")";

                                        //Set Tab Header
                                        ServerTab.Header = ServerTab.Header.ToString().Replace(" (ILO" + vm.GenGenericServerInfo1.ChassisSerial + ")", "");
                                        ServerTab.Header = ServerTab.Header + " (ILO" + vm.GenGenericServerInfo1.ChassisSerial + ")";



                                        // MessageBox.Show(vm.GenGenericServerInfo1.ChassisSerial.Substring(0, 2));
                                        //Server Region Check
                                        if (vm.GenGenericServerInfo1.ChassisSerial != null && vm.GenGenericServerInfo1.ChassisSerial != String.Empty)
                                            if (vm.GenGenericServerInfo1.ChassisSerial.Substring(0, 2).Contains("CZ"))
                                            {
                                                //MessageBox.Show(vm.GenGenericServerInfo1.ChassisSerial);


                                            }
                                            else
                                            {
                                                if (vm.GenGenericServerInfo1.ChassisSerial == String.Empty)
                                                {
                                                }
                                                else
                                                {
                                                    vm.GenGenericServerInfo1.OutOfRegionMessage = "Out of Region!";
                                                }


                                            }

                                    }



                                    //Stop Timer
                                    timer.Stop();
                                    timeTaken = timer.Elapsed;
                                    timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                    vm.ProgressPercentage1 = 100;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    vm.ProgressMessage1 = "Scripts completed successfully! " + timetaken;

                                    //check for DL20 ML30 as different sps update required & no Innovation Engine
                                    if (vm.GenGenericServerInfo1.Model != null)
                                    {
                                        if (ServerGen.Contains("Gen10"))
                                        {
                                            if (vm.GenGenericServerInfo1.Model.Contains("DL20") || vm.GenGenericServerInfo1.Model.Contains("ML30"))
                                            {
                                                vm.WarningMessage1 = "Warning!!! This Model requires no Innovation Engine & a specific SPS. Scripts completed successfully!";
                                            }
                                        }

                                    }

                                    

                                    //CHECK ILO VERSION ON EACH GEN
                                    if (vm.GenGenericServerInfo1 != null)
                                    {
                                        //ILO Firmware Check
                                        //**** SELECT SERVER GEN ****
                                        switch (ServerGen)
                                        {
                                            case "Gen8":
                                                if (vm.GenGenericServerInfo1.iLOVersion != null)
                                                    vm.IloFWCheck(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "FWLevel1.xml", "2.00", "1", "Gen8");
                                                //CHECK SPS VERSION IF V2 UPGRADE REQUIRED
                                                if (vm.GenGenericServerInfo1.SPSVersion != null)
                                                    vm.FirmwareVersionCheck("2.1.7", "1", vm.GenGenericServerInfo1.SPSVersion, "System Requires V2 Upgrade! Please launch Remote Console to perform the update.");

                                                break;
                                            case "Gen9":
                                                if (vm.GenGenericServerInfo1.iLOVersion != null)
                                                    vm.IloFWCheck(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "FWLevel1.xml", "2.00", "1", "Gen9");

                                                //CHECK SPS VERSION IF V4 UPGRADE REQUIRED
                                                if (vm.GenGenericServerInfo1.Model.Contains("580") == false)
                                                {
                                                    //Check SPS Level to determine if V4 Upgrade required
                                                    if (vm.GenGenericServerInfo1.SPSVersion != null)
                                                        vm.FirmwareVersionCheck("3.0.6", "1", vm.GenGenericServerInfo1.SPSVersion, "System Requires V4 Upgrade! Please launch Remote Console to perform the update.");

                                                }


                                                break;
                                            case "Gen10":
                                                if (vm.GenGenericServerInfo1.iLOVersion != null)
                                                    vm.IloGen10FWCheck("1.40", vm.GenGenericServerInfo1.iLOVersion.Replace("iLO 5 v", "").Trim(), "1", "iLO Version is Below 1.40, please update via webpage to iLO 5 1.40, before running any other updates.");
                                                break;
                                        }

                                    }

                                    //Check maintenance switch is off
                                    if (vm.ILORestOutput1.Contains("Response status code does not indicate success: 401 (Unauthorized)."))
                                    {

                                        vm.WarningMessage1 = "Issue extracting data. Please check the Servers Maintenance Switch 1 is set to the on position.";

                                    }

                                                                    

                                    //Load Ultima URL
                                    if (vm.GenGenericServerInfo1 != null)
                                    {

                                        if (vm.GenGenericServerInfo1.NVRAMSpaceStatus == "Degraded")
                                        {
                                            vm.WarningMessage1 = "iLO Self Test Error Detected";
                                        }


                                        if (vm.GenGenericServerInfo1.Manufacturer != null && vm.GenGenericServerInfo1.Model != null)
                                            vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.GenGenericServerInfo1.Manufacturer.Replace("Inc.", "") + " " + vm.GenGenericServerInfo1.Model + " Server &go=Go";
                                    }

                                    //CHECK FOR ONE VIEW MANAGEMENT 
                                    if (vm.ILORestOutput1.Contains("OneView") || vm.ILORestOutput1.Contains("One View") || vm.ILORestOutput1.Contains("ONEVIEW") || vm.ILORestOutput1.Contains("ONE VIEW") || vm.ILORestOutput1.Contains("oneview") || vm.ILORestOutput1.Contains("one view"))
                                    {

                                        vm.WarningMessage1 = "System appears to be ONE VIEW Managed! Please Login to the Server Webpage, Select One View and Delete. Or you can use the the Wipe Embedded Flash button (Removes IProv).";

                                    }
                                    break;
                                case "IloUpgradeOnly":
                                    //Attempt to upgrade ilo
                                    if (vm.ILOUpgradePath1 != string.Empty)
                                    {

                                        vm.WarningMessage1 = string.Empty;
                                        vm.ProgressMessage1 = "Please wait...";
                                        vm.WarningMessage1 = "";
                                        vm.ProgressIsActive1 = true;
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressVisibility1 = Visibility.Visible;
                                        //Check if Script is Cancelled
                                        vm.CancelServerScript("S1");

                                        switch (ServerGen)
                                        {
                                            case "Gen8":


                                                vm.ProgressPercentage1 = 10;
                                                //Check if ILO FLASH FILE SELECTED
                                                if (vm.ILOUpgradePath1 != String.Empty)
                                                {
                                                    //ilo  Firmware update
                                                    vm.ProgressMessage1 = "iLO Firmware Update";
                                                    // replace with loop on 
                                                    vm.ProgressPercentage1 = 35;
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Administrator,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                                    await PutTaskDelay(80000);
                                                }

                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");

                                                break;
                                            case "Gen9":

                                                vm.ProgressPercentage1 = 10;
                                                //Check if ILO FLASH FILE SELECTED
                                                if (vm.ILOUpgradePath1 != String.Empty)
                                                {
                                                    //ilo  Firmware update
                                                    vm.ProgressMessage1 = "iLO Firmware Update";
                                                    // replace with loop on 
                                                    vm.ProgressPercentage1 = 35;
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Administrator,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                                    await PutTaskDelay(80000);
                                                }

                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");


                                                break;
                                            case "Gen10":
                                                //BIOS
                                                if (vm.BIOSUpgradePath1 != String.Empty)
                                                {
                                                    vm.ProgressPercentage1 = 30;
                                                    //BIOS Firmware update  
                                                    vm.ProgressMessage1 = "BIOS Flash Update";
                                                    vm.ProgressPercentage1 = 30;
                                                    //Upload to Repository and directly flash it
                                                    // await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "uploadcomp --component " + vm.Server1IPv4 + "BIOSUpdate.bin --update_repository --update_target  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                                    //Just upload 
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "uploadcomp --component " + vm.Server1IPv4 + "BIOSUpdate.bin --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                                    //await PutTaskDelay(30000);
                                                }

                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");

                                                //Innovation Engine
                                                if (vm.IENGUpgradePath1 != String.Empty)
                                                {
                                                    vm.ProgressPercentage1 = 30;
                                                    //BIOS Firmware update
                                                    vm.ProgressMessage1 = "Uploading Innovation Engine Flash Update";
                                                    vm.ProgressPercentage1 = 30;

                                                    //Just upload 
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "uploadcomp --component " + vm.Server1IPv4 + "IENGUpdate.bin --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                                    //await PutTaskDelay(30000);
                                                }

                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");


                                                //SPS Upgrade
                                                if (vm.SPSUpgradePath1 != String.Empty)
                                                {
                                                    vm.ProgressPercentage1 = 30;
                                                    //BIOS Firmware update
                                                    vm.ProgressMessage1 = "Uploading SPS Flash Update";
                                                    vm.ProgressPercentage1 = 30;
                                                    //Just upload 
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "uploadcomp --component " + vm.Server1IPv4 + "SPSUpdate.bin --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                                    //await PutTaskDelay(30000);
                                                }

                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");

                                                //MessageBox.Show(vm.ILOUpgradePath != String.Empty);

                                                vm.ProgressPercentage1 = 40;
                                                //Check if ILO FLASH FILE SELECTED
                                                if (vm.ILOUpgradePath1 != String.Empty)
                                                {
                                                    vm.ProgressPercentage1 = 40;
                                                    //ilo  Firmware update
                                                    vm.ProgressMessage1 = "Uploading iLO Firmware Update";
                                                    // replace with loop on 
                                                    vm.ProgressPercentage1 = 40;
                                                    //Just upload 
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "uploadcomp --component " + vm.Server1IPv4 + "ILOUpdate.bin --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                                    //await PutTaskDelay(30000);
                                                }


                                                //Delete install set if already present                                                                                                                                        Updates Set 10-12-2020
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "installset delete --name Updates" + DateTime.Now.ToShortDateString().Replace("/", "-") + " --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                                await PutTaskDelay(10000);

                                                // check if any updates were selected, if so create an install set

                                                if (vm.BIOSUpgradePath1 != String.Empty || vm.IENGUpgradePath1 != String.Empty || vm.SPSUpgradePath1 != String.Empty || vm.ILOUpgradePath1 != String.Empty)
                                                {


                                                    //create new json parser  ADD AFTER UPLOADS TO REPOSITORY IS COMPLETE   installset add myinstallset.json
                                                    JsonParser jPar = new JsonParser(vm);
                                                    jPar.CreateGen10InstallSet("S" + vm.WhichTab, vm.Server1IPv4, vm.BIOSUpgradePath1, vm.IENGUpgradePath1, vm.SPSUpgradePath1, vm.ILOUpgradePath1);

                                                    // MessageBox.Show("S" + vm.WhichTab + "InstallSet.json");
                                                    //Important delay
                                                    await PutTaskDelay(10000);
                                                    //Upload the generated install set
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "installset add " + "S" + vm.WhichTab + "InstallSet.json --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                                    //Important delay
                                                    await PutTaskDelay(10000);

                                                    //Then invoke it                                                                                                                                         Updates Set 10-12-2020
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "installset invoke --name Updates" + DateTime.Now.ToShortDateString().Replace("/", "-") + " --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                                }


                                                break;
                                        }



                                        vm.ProgressPercentage1 = 50;


                                        //Check if Script is Cancelled
                                        vm.CancelServerScript("S1");

                                        //await PutTaskDelay(20000);

                                        vm.ProgressMessage1 = "Extracting Json Objects";


                                        //**** REDFISH GET SERVER INFO!!! *****
                                        vm.GenGenericServerInfo1 = await RedfishCrawler.GetHPData(vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);


                                        //Stop Timer
                                        timer.Stop();
                                        timeTaken = timer.Elapsed;
                                        timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                        vm.ProgressPercentage1 = 100;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts completed successfully! iLO now Upgraded to a Restful Compatible Version. " + timetaken;


                                        vm.ILOUpgradePath1 = string.Empty;
                                        vm.ILOMinVersion1 = string.Empty;
                                    }

                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;


                                    //Check Firmware against Server Values
                                    FirmwareUpdateCheck();

                                    break;
                                case "IloOnly":
                                    //Upload to Repository and directly flash it
                                    // await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "uploadcomp --component " + vm.Server1IPv4 + "BIOSUpdate.bin --update_repository --update_target  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    if (vm.ILOUpgradePath1 != String.Empty)
                                    {




                                        //**** SELECT SERVER GEN ****
                                        switch (ServerGen)
                                        {
                                            case "Gen8":

                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");


                                                vm.ProgressPercentage1 = 35;
                                                //Check if ILO FLASH FILE SELECTED
                                                if (vm.ILOUpgradePath1 != String.Empty)
                                                {
                                                    //ilo  Firmware update
                                                    vm.ProgressMessage1 = "iLO Firmware Update";
                                                    // replace with loop on 
                                                    vm.ProgressPercentage1 = 35;
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Administrator,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                                    //Just Flash
                                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "firmwareupdate " + vm.Server1IPv4 + "ILOUpdate.bin --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                                    //await PutTaskDelay(80000);
                                                }

                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");

                                                break;
                                            case "Gen9":
                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");


                                                vm.ProgressPercentage1 = 35;
                                                //Check if ILO FLASH FILE SELECTED
                                                if (vm.ILOUpgradePath1 != String.Empty)
                                                {
                                                    //ilo  Firmware update
                                                    vm.ProgressMessage1 = "iLO Firmware Update";
                                                    // replace with loop on 
                                                    vm.ProgressPercentage1 = 35;
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Administrator,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                                    //Just Flash
                                                    // await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "firmwareupdate " + vm.Server1IPv4 + "ILOUpdate.bin --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                                    // await PutTaskDelay(80000);
                                                }

                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");

                                                break;
                                            case "Gen10":

                                                //CLEAR GEN 10 TASK QUEUE
                                                vm.ProgressMessage1 = "Clearing Task Queue";
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "taskqueue --resetqueue --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                                await PutTaskDelay(5000);

                                                // Clear Repository
                                                vm.ProgressMessage1 = "Clearing Repository";
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "deletecomp --all --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));


                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");

                                                //MessageBox.Show(vm.ILOUpgradePath != String.Empty);

                                                vm.ProgressPercentage1 = 35;
                                                //Check if ILO FLASH FILE SELECTED
                                                if (vm.ILOUpgradePath1 != String.Empty)
                                                {
                                                    vm.ProgressPercentage1 = 35;
                                                    //ilo  Firmware update
                                                    vm.ProgressMessage1 = "Uploading iLO Firmware Update";
                                                    // replace with loop on 
                                                    vm.ProgressPercentage1 = 35;
                                                    //Just upload 
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "uploadcomp --component " + vm.Server1IPv4 + "ILOUpdate.bin --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                                    //Just Flash
                                                    //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "firmwareupdate " + vm.Server1IPv4 + "ILOUpdate.bin --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                                    //await PutTaskDelay(30000);
                                                }

                                                //INSTALL SET
                                                //Delete install set if already present                                                                                                                                        Updates Set 10-12-2020
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "installset delete --name Updates" + DateTime.Now.ToShortDateString().Replace("/", "-") + " --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                                await PutTaskDelay(10000);

                                                // check if any updates were selected, if so create an install set

                                                if (vm.ILOUpgradePath1 != String.Empty)
                                                {


                                                    //create new json parser  ADD AFTER UPLOADS TO REPOSITORY IS COMPLETE   installset add myinstallset.json
                                                    JsonParser jPar = new JsonParser(vm);
                                                    jPar.CreateGen10InstallSet("S" + vm.WhichTab, vm.Server1IPv4, vm.BIOSUpgradePath1, vm.IENGUpgradePath1, vm.SPSUpgradePath1, vm.ILOUpgradePath1);

                                                    // MessageBox.Show("S" + vm.WhichTab + "InstallSet.json");
                                                    //Important delay
                                                    await PutTaskDelay(5000);
                                                    //Upload the generated install set
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "installset add " + "S" + vm.WhichTab + "InstallSet.json --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                                    //Important delay
                                                    await PutTaskDelay(10000);

                                                    //Then invoke it                                                                                                                                         Updates Set 10-12-2020
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "installset invoke --name Updates" + DateTime.Now.ToShortDateString().Replace("/", "-") + " --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                                }
                                                break;
                                        }

                                        //Wait 3 Minutes 
                                        if (vm.GenGenericServerInfo1.Model != null)
                                        {
                                            if (vm.GenGenericServerInfo1.Model.Contains("580"))
                                            {
                                                await PutTaskDelay(185000);

                                            }
                                            else
                                            {
                                                await PutTaskDelay(100000);
                                            }
                                        }
                                        else
                                        {
                                            await PutTaskDelay(100000);
                                        }
                                            

                                        vm.ProgressPercentage1 = 90;
                                        vm.ProgressMessage1 = "Server Inventory Running";
                                        //**** REDFISH GET SERVER INFO!!! *****
                                        vm.GenGenericServerInfo1 = await RedfishCrawler.GetHPData(vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);

                                        vm.ProgressPercentage1 = 95;



                                        // await PutTaskDelay(10000);
                                        //Read the Data from the JSON and XML Files pulled from the Server
                                        //ReadJsonXMLObjects(ServerGen);

                                        vm.ProgressPercentage1 = 95;

                                        //Stop Timer
                                        timer.Stop();
                                        timeTaken = timer.Elapsed;
                                        timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                        vm.ProgressPercentage1 = 100;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts completed successfully! " + timetaken;


                                        //Clear temp update file
                                        //BIOS
                                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin"))
                                        {
                                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin");
                                            //for test certificate
                                            vm.UpdatesRun += @" [BIOS] ";
                                        }

                                        //ILO
                                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.bin"))
                                        {
                                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.bin");
                                            //for test certificate
                                            vm.UpdatesRun += @" [ILO] ";
                                        }
                                        //SPS
                                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.bin"))
                                        {
                                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.bin");
                                            //for test certificate
                                            vm.UpdatesRun += @" [SPS] ";
                                        }

                                        //IE
                                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"IENGUpdate.bin"))
                                        {
                                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"IENGUpdate.bin");
                                            //for test certificate
                                            vm.UpdatesRun += @" [IE] ";
                                        }

                                        //CLEAR Flash File INFORMATION
                                        //vm.BIOSUpgradePath1 = String.Empty;
                                        //vm.ILOUpgradePath1 = String.Empty;
                                        //vm.SPSUpgradePath1 = String.Empty;
                                        //vm.IENGUpgradePath1 = String.Empty;

                                        //Check Firmware against Server Values
                                        FirmwareUpdateCheck();

                                        
                                    }
                                    else
                                    {

                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.MessageVisibility1 = Visibility.Visible;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressPercentage1 = 0;
                                        vm.WarningMessage1 = "Please select a valid iLO Update and retry!";

                                    }





                                    break;

                                case "WipeScripts":


                                    vm.ProgressMessage1 = "Please wait...";
                                    vm.ProgressIsActive1 = true;
                                    vm.ProgressPercentage1 = 0;
                                    vm.ProgressVisibility1 = Visibility.Visible;

                                    vm.ProgressPercentage1 = 10;

                                    //return;
                                    vm.ProgressPercentage1 = 10;

                                    //**** SELECT SERVER GEN ****
                                    //Reset BIOS to Defaults
                                    switch (ServerGen)
                                    {
                                        case "Gen8":
                                            vm.ProgressMessage1 = "Resetting BIOS Defaults";
                                            await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "biosdefaults --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool")); //--reboot=ForceRestart
                                            //await PutTaskDelay(220000);
                                            break;
                                        case "Gen9":
                                            vm.ProgressMessage1 = "Resetting BIOS Defaults";
                                            await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "biosdefaults --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool")); // --reboot=ForceRestart
                                            //await PutTaskDelay(180000);
                                            break;
                                        case "Gen10":
                                            //Not required
                                            break;
                                    }

                                    //Check if Script is Cancelled
                                    vm.CancelServerScript("S1");


                                    vm.ProgressMessage1 = "Setting Asset Tag to 'NEW TAG'";
                                    vm.ProgressPercentage1 = 10;
                                    //RIBCL
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Set_ServerAsset_Tag.xml -s " + vm.Server1IPv4 + @" -t user=Administrator,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));

                                    ////REBOOT SERVER
                                    vm.ProgressMessage1 = "Powering Server Down";
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot PressAndHold --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                    vm.ProgressPercentage1 = 20;

                                    await PutTaskDelay(10000);

                                    vm.ProgressPercentage1 = 20;

                                    //Check if Script is Cancelled
                                    vm.CancelServerScript("S1");

                                    vm.ProgressPercentage1 = 30;

                                    //**** SELECT SERVER GEN ****
                                    switch (ServerGen)
                                    {
                                        case "Gen8":
                                            if (vm.BIOSUpgradePath1 != String.Empty)
                                            {
                                                //BIOS Firmware update
                                                vm.ProgressMessage1 = "BIOS Flash Update";
                                                vm.ProgressPercentage1 = 25;
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_BIOS_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Administrator,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                                await PutTaskDelay(40000);
                                            }


                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");


                                            vm.ProgressPercentage1 = 35;
                                            //Check if ILO FLASH FILE SELECTED
                                            if (vm.ILOUpgradePath1 != String.Empty)
                                            {
                                                //ilo  Firmware update
                                                vm.ProgressMessage1 = "iLO Firmware Update";
                                                // replace with loop on 
                                                vm.ProgressPercentage1 = 35;
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Administrator,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                                await PutTaskDelay(80000);
                                            }

                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            break;
                                        case "Gen9":
                                            if (vm.BIOSUpgradePath1 != String.Empty)
                                            {
                                                //BIOS Firmware update
                                                vm.ProgressMessage1 = "BIOS Flash Update";
                                                vm.ProgressPercentage1 = 25;
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_BIOS_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Administrator,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                                await PutTaskDelay(40000);
                                            }

                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");


                                            vm.ProgressPercentage1 = 35;
                                            //Check if ILO FLASH FILE SELECTED
                                            if (vm.ILOUpgradePath1 != String.Empty)
                                            {
                                                //ilo  Firmware update
                                                vm.ProgressMessage1 = "iLO Firmware Update";
                                                // replace with loop on 
                                                vm.ProgressPercentage1 = 35;
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Administrator,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                                await PutTaskDelay(80000);
                                            }

                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            break;
                                        case "Gen10":

                                            //CLEAR GEN 10 TASK QUEUE
                                            vm.ProgressMessage1 = "Clearing Task Queue";
                                            await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "taskqueue --resetqueue --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                            await PutTaskDelay(5000);

                                            // Clear Repository
                                            vm.ProgressMessage1 = "Clearing Repository";
                                            await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "deletecomp --all --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));


                                            //BIOS
                                            if (vm.BIOSUpgradePath1 != String.Empty)
                                            {
                                                vm.ProgressPercentage1 = 30;
                                                //BIOS Firmware update  
                                                vm.ProgressMessage1 = "BIOS Flash Update";
                                                vm.ProgressPercentage1 = 30;
                                                //Upload to Repository and directly flash it
                                                // await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "uploadcomp --component " + vm.Server1IPv4 + "BIOSUpdate.bin --update_repository --update_target  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                                //Just upload 
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "uploadcomp --component " + vm.Server1IPv4 + "BIOSUpdate.bin --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                                //await PutTaskDelay(30000);
                                            }

                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            //Innovation Engine
                                            if (vm.IENGUpgradePath1 != String.Empty)
                                            {
                                                vm.ProgressPercentage1 = 30;
                                                //BIOS Firmware update
                                                vm.ProgressMessage1 = "Uploading Innovation Engine Flash Update";
                                                vm.ProgressPercentage1 = 30;

                                                //Just upload 
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "uploadcomp --component " + vm.Server1IPv4 + "IENGUpdate.bin --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                                //await PutTaskDelay(30000);
                                            }

                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            //SPS Upgrade
                                            if (vm.SPSUpgradePath1 != String.Empty)
                                            {
                                                vm.ProgressPercentage1 = 30;
                                                //BIOS Firmware update
                                                vm.ProgressMessage1 = "Uploading SPS Flash Update";
                                                vm.ProgressPercentage1 = 30;
                                                //Just upload 
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "uploadcomp --component " + vm.Server1IPv4 + "SPSUpdate.bin --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                                //await PutTaskDelay(30000);
                                            }

                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            //MessageBox.Show(vm.ILOUpgradePath != String.Empty);

                                            vm.ProgressPercentage1 = 40;
                                            //Check if ILO FLASH FILE SELECTED
                                            if (vm.ILOUpgradePath1 != String.Empty)
                                            {
                                                vm.ProgressPercentage1 = 40;
                                                //ilo  Firmware update
                                                vm.ProgressMessage1 = "Uploading iLO Firmware Update";
                                                // replace with loop on 
                                                vm.ProgressPercentage1 = 40;
                                                //Just upload 
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "uploadcomp --component " + vm.Server1IPv4 + "ILOUpdate.bin --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                                //await PutTaskDelay(30000);
                                            }


                                            //Delete install set if already present                                                                                                                                        Updates Set 10-12-2020
                                            await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "installset delete --name Updates" + DateTime.Now.ToShortDateString().Replace("/", "-") + " --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                            await PutTaskDelay(10000);

                                            // check if any updates were selected, if so create an install set

                                            if (vm.BIOSUpgradePath1 != String.Empty || vm.IENGUpgradePath1 != String.Empty || vm.SPSUpgradePath1 != String.Empty || vm.ILOUpgradePath1 != String.Empty)
                                            {


                                                //create new json parser  ADD AFTER UPLOADS TO REPOSITORY IS COMPLETE   installset add myinstallset.json
                                                JsonParser jPar = new JsonParser(vm);
                                                jPar.CreateGen10InstallSet("S" + vm.WhichTab, vm.Server1IPv4, vm.BIOSUpgradePath1, vm.IENGUpgradePath1, vm.SPSUpgradePath1, vm.ILOUpgradePath1);

                                                // MessageBox.Show("S" + vm.WhichTab + "InstallSet.json");
                                                //Important delay
                                                await PutTaskDelay(10000);
                                                //Upload the generated install set
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "installset add " + "S" + vm.WhichTab + "InstallSet.json --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                                //Important delay
                                                await PutTaskDelay(20000);

                                                //Then invoke it                                                                                                                                         Updates Set 10-12-2020
                                                await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "installset invoke --name Updates" + DateTime.Now.ToShortDateString().Replace("/", "-") + " --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                            }


                                            break;
                                    }





                                    //CANCEL SCRIPT
                                    if (vm.CancelScriptS1 == true)
                                    {
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                        return;
                                    }
                                    //END CANCEL SCRIPT


                                    vm.ProgressPercentage1 = 50;

                                    //ILO SET SERVER NAME
                                    vm.ProgressMessage1 = "Setting Server Name to 'NEW SERVER'";
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Set_Server_name.xml -s " + vm.Server1IPv4 + @" -t user=Administrator,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                    await PutTaskDelay(5000);

                                    //Check if Script is Cancelled
                                    vm.CancelServerScript("S1");

                                    vm.ProgressPercentage1 = 60;


                                    //Time to wait for reboot
                                    if (vm.ServerType.Contains("Blade"))
                                    {
                                        //Determine how much time to wait vm.ILOUpgradePath1 != string.Empty && vm.BIOSUpgradePath1 != string.Empty && 
                                        if (vm.SPSUpgradePath1 != string.Empty && vm.IENGUpgradePath1 != string.Empty)
                                        {
                                            vm.ProgressMessage1 = "Rebooting and Applying  Firmware. Please wait...";
                                            await PutTaskDelay(300000);
                                            vm.ProgressPercentage1 = 70;
                                            await PutTaskDelay(300000);
                                            vm.ProgressPercentage1 = 72;
                                            await PutTaskDelay(300000);

                                        }
                                        else if (vm.SPSUpgradePath1 != string.Empty || vm.IENGUpgradePath1 != string.Empty)
                                        {
                                            vm.ProgressMessage1 = "Rebooting and Applying  Firmware. Please wait...";
                                            vm.ProgressPercentage1 = 70;
                                            await PutTaskDelay(700000);
                                        }
                                        else if (vm.ILOUpgradePath1 != string.Empty || vm.BIOSUpgradePath1 != string.Empty && vm.SPSUpgradePath1 == string.Empty && vm.IENGUpgradePath1 == string.Empty)
                                        {
                                            //AMD GEN10
                                            vm.ProgressMessage1 = "Rebooting and Applying  Firmware. Please wait...";
                                            vm.ProgressPercentage1 = 70;
                                            //if only bios and ilo then just wait standard reboot time
                                            await PutTaskDelay(500000);
                                        }
                                        else
                                        {
                                            vm.ProgressMessage1 = "Rebooting and Applying  Firmware. Please wait...";
                                            vm.ProgressPercentage1 = 70;
                                            //if only bios and ilo then just wait standard reboot time
                                            await PutTaskDelay(100000);
                                        }


                                    }
                                    else
                                    {

                                        //Determine how much time to wait vm.ILOUpgradePath1 != string.Empty && vm.BIOSUpgradePath1 != string.Empty && 
                                        if (vm.SPSUpgradePath1 != string.Empty && vm.IENGUpgradePath1 != string.Empty)
                                        {
                                            vm.ProgressMessage1 = "Rebooting and Applying  Firmware. Please wait...";
                                            await PutTaskDelay(300000);
                                            vm.ProgressPercentage1 = 70;
                                            await PutTaskDelay(300000);
                                            vm.ProgressPercentage1 = 72;
                                            await PutTaskDelay(300000);

                                        }
                                        else if (vm.SPSUpgradePath1 != string.Empty || vm.IENGUpgradePath1 != string.Empty)
                                        {
                                            vm.ProgressMessage1 = "Rebooting and Applying  Firmware. Please wait...";
                                            vm.ProgressPercentage1 = 70;
                                            await PutTaskDelay(700000);
                                        }
                                        else if (vm.ILOUpgradePath1 != string.Empty || vm.BIOSUpgradePath1 != string.Empty && vm.SPSUpgradePath1 == string.Empty && vm.IENGUpgradePath1 == string.Empty)
                                        {
                                            //AMD GEN10
                                            vm.ProgressMessage1 = "Rebooting and Applying  Firmware. Please wait...";
                                            vm.ProgressPercentage1 = 70;
                                            //if only bios and ilo then just wait standard reboot time
                                            await PutTaskDelay(500000);
                                        }
                                        else
                                        {
                                            vm.ProgressMessage1 = "Rebooting and Applying  Firmware. Please wait...";
                                            vm.ProgressPercentage1 = 70;
                                            //if only bios and ilo then just wait standard reboot time
                                            await PutTaskDelay(100000);
                                        }
                                    }

                                    vm.ProgressPercentage1 = 75;


                                    //REBOOT SERVER
                                    vm.ProgressMessage1 = "Powering Server On";
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot On --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                    //Check if Script is Cancelled
                                    vm.CancelServerScript("S1");

                                    // await PutTaskDelay(5000);
                                    vm.ProgressPercentage1 = 80;
                                    vm.ProgressMessage1 = "Reapplying Trial iLO License";
                                    ////RE APPLY ILO EVAL LICENSE
                                    await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "ilolicense 332N6-VJMMM-MHTPD-L7XNR-29G8B  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                    //Check if Script is Cancelled
                                    vm.CancelServerScript("S1");


                                    vm.ProgressMessage1 = "Server Rebooting";

                                    //Wait Minutes 
                                    if (vm.GenGenericServerInfo1.Model != null)
                                    {
                                        if (vm.GenGenericServerInfo1.Model.Contains("580"))
                                        {
                                            await PutTaskDelay(350000);

                                        }
                                        else
                                        {
                                            await PutTaskDelay(210000);
                                        }
                                    }
                                    else
                                    {
                                        await PutTaskDelay(210000);
                                    }
                                 

                                    vm.ProgressPercentage1 = 90;
                                    vm.ProgressMessage1 = "Server Inventory Running";
                                    //**** REDFISH GET SERVER INFO!!! *****
                                    vm.GenGenericServerInfo1 = await RedfishCrawler.GetHPData(vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);

                                    vm.ProgressPercentage1 = 95;



                                    await PutTaskDelay(10000);
                                    //Read the Data from the JSON and XML Files pulled from the Server
                                    //ReadJsonXMLObjects(ServerGen);

                                    vm.ProgressPercentage1 = 95;

                                    //Stop Timer
                                    timer.Stop();
                                    timeTaken = timer.Elapsed;
                                    timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                    vm.ProgressPercentage1 = 0;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    vm.ProgressMessage1 = "Scripts completed successfully! Please update Intelligent Provisioning! " + timetaken;


                                    //Clear temp update file
                                    //BIOS
                                    //if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin"))
                                    //{
                                    //    //File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin");
                                    //    //for test certificate
                                    //    vm.UpdatesRun += @" [BIOS] ";
                                    //}

                                    ////ILO
                                    //if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.bin"))
                                    //{
                                    //    //File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.bin");
                                    //    //for test certificate
                                    //    vm.UpdatesRun += @" [ILO] ";
                                    //}
                                    ////SPS
                                    //if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.bin"))
                                    //{
                                    //    //File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.bin");
                                    //    //for test certificate
                                    //    vm.UpdatesRun += @" [SPS] ";
                                    //}

                                    ////IE
                                    //if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"IENGUpdate.bin"))
                                    //{
                                    //    //File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"IENGUpdate.bin");
                                    //    //for test certificate
                                    //    vm.UpdatesRun += @" [IE] ";
                                    //}

                                    //CLEAR Flash File INFORMATION
                                    //vm.BIOSUpgradePath1 = String.Empty;
                                    //vm.ILOUpgradePath1 = String.Empty;
                                    //vm.SPSUpgradePath1 = String.Empty;
                                    //vm.IENGUpgradePath1 = String.Empty;

                                    //Check Firmware against Server Values
                                    FirmwareUpdateCheck();


                                    break;

                                case "FinalScripts":
                                    if (vm.GenGenericServerInfo1 != null)
                                        if (vm.GenGenericServerInfo1.ChassisSerial != string.Empty)
                                        {
                                            //////EXTRACT FIRMWARE JSON
                                            vm.ProgressMessage1 = "Please wait...";
                                            vm.ProgressIsActive1 = true;
                                            vm.ProgressPercentage1 = 0;
                                            vm.MessageVisibility1 = Visibility.Visible;
                                            vm.ProgressVisibility1 = Visibility.Visible;
                                            vm.ProgressPercentage1 = 0;


                                            vm.ProgressMessage1 = "Refreshing Server Data!";
                                            //**** REDFISH GET SERVER INFO!!! *****
                                            vm.GenGenericServerInfo1 = await RedfishCrawler.GetHPData(vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);



                                            //TPM CHECK DEPENDING ON GENERATION
                                            switch (ServerGen)
                                            {
                                                case "Gen8":
                                                    ////Reboot via restful api tool
                                                    ////await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                                                    ////Redfish Reboot
                                                    //await RedfishCrawler.RedfishActionCall("https://" + vm.Server1IPv4 + "/redfish/v1/Systems/1/Actions/ComputerSystem.Reset/", HttpMethod.Post, "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"ResetType\":\"ForceRestart\"}}");

                                                    //vm.ProgressMessage1 = "Rebooting prior to Factory Reset Please Wait...";

                                                    ////Wait 3 mins for reboot
                                                    //await PutTaskDelay(200000);

                                                    ////**** REDFISH GET SERVER INFO!!! *****
                                                    //vm.GenGenericServerInfo1 = await RedfishCrawler.GetHPData(vm.Server1IPv4, vm.OverridePasswordS1 , vm, ServerGen);
                                                    break;
                                                case "Gen9":
                                                    ////Redfish Reboot
                                                    //await RedfishCrawler.RedfishActionCall("https://" + vm.Server1IPv4 + "/redfish/v1/Systems/1/Actions/ComputerSystem.Reset/", HttpMethod.Post, "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"ResetType\":\"ForceRestart\"}}");

                                                    //vm.ProgressMessage1 = "Rebooting prior to Factory Reset Please Wait...";

                                                    ////Wait 3 mins for reboot
                                                    //await PutTaskDelay(200000);

                                                    ////**** REDFISH GET SERVER INFO!!! *****
                                                    //vm.GenGenericServerInfo1 = await RedfishCrawler.GetHPData(vm.Server1IPv4, vm.OverridePasswordS1 , vm, ServerGen);
                                                    break;
                                                case "Gen10":


                                                    // TPM RE-ENABLE
                                                    if (vm.GenGenericServerInfo1.TpmVisibility == "Hidden")
                                                    {
                                                        // ********* TPM ENABLE DISABLE ***********
                                                        //DISABLE TPM
                                                        //await RedfishCrawler.RedfishPatchCall("https://" + vm.Server1IPv4 + "/redfish/v1/Systems/1/BIOS/Settings", HttpMethod.Put, "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"Attributes\":{\"TpmVisibility\":\"Hidden\"}}");

                                                        // ENABLE TPM
                                                        await RedfishCrawler.RedfishPatchCall("https://" + vm.Server1IPv4 + "/redfish/v1/Systems/1/BIOS/Settings", HttpMethod.Put, "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"Attributes\":{\"TpmVisibility\":\"Visible\"}}");

                                                        // MessageBox.Show("Redfish Reset Power");
                                                        //Redfish Reboot
                                                        await RedfishCrawler.RedfishActionCall("https://" + vm.Server1IPv4 + "/redfish/v1/Systems/1/Actions/ComputerSystem.Reset/", HttpMethod.Post, "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"ResetType\":\"ForceRestart\"}}");

                                                        vm.ProgressMessage1 = "TPM set to Enabled, Rebooting to apply settings Please Wait...";
                                                        await PutTaskDelay(200000);

                                                        //**** REDFISH GET SERVER INFO!!! *****
                                                        vm.GenGenericServerInfo1 = await RedfishCrawler.GetHPData(vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);
                                                    }
                                                    else
                                                    {
                                                        // MessageBox.Show("Redfish Reset Power");
                                                        //Redfish Reboot
                                                        //await RedfishCrawler.RedfishActionCall("https://" + vm.Server1IPv4 + "/redfish/v1/Systems/1/Actions/ComputerSystem.Reset/", HttpMethod.Post, "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"ResetType\":\"ForceRestart\"}}");

                                                        //vm.ProgressMessage1 = "Rebooting prior to Factory Defaults! Please Wait...";
                                                        //await PutTaskDelay(200000);

                                                    }
                                                    break;
                                            }



                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            vm.ProgressMessage1 = "Resetting iLO to Factory Defaults";
                                            vm.ProgressPercentage1 = 60;
                                            //FOR ILO RESTFUL FACTORY DEFAULTS
                                            // await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "factorydefaults --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                            //REDFISH FACTORY DEFAULTS

                                            await RedfishCrawler.RedfishActionCall("https://" + vm.Server1IPv4 + "/redfish/v1/Managers/1/Actions/Oem/Hp/HpiLO.ResetToFactoryDefaults/", HttpMethod.Post, "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"Action\": \"ResetToFactoryDefaults\",\"ResetType\": \"Default\"}");

                                            await PutTaskDelay(120000);
                                            vm.ProgressPercentage1 = 70;


                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");



                                            vm.ProgressPercentage1 = 70;


                                            vm.ProgressPercentage1 = 80;



                                            vm.ProgressMessage1 = "Powering Server Down";
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "reboot ForceOff --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                                            await RedfishCrawler.RedfishActionCall("https://" + vm.Server1IPv4 + "/redfish/v1/Systems/1/Actions/ComputerSystem.Reset/", HttpMethod.Post, "Administrator:" + vm.OverridePasswordS1.Trim() + "", vm, "{\"ResetType\":\"ForceOff\"}}");


                                            //TPM CHECK FOR GEN 10
                                            if (vm.GenGenericServerInfo1.TpmVisibility == "Visible" || vm.GenGenericServerInfo1.TpmVisibility == "Hidden")
                                            {

                                                vm.WarningMessage1 += "TPM present in system!!! This will need to be removed after this script has finished.";

                                            }

                                            //REMOVE OLD FILES

                                            //CLEAR THE DATA FILES THESE WILL BE REPRODUCED LATER IN THIS SCRIPT
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml");
                                            }
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml");
                                            }
                                            //CLEAR RESTFUL INTERFACE JSON FILE
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "All.json"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "All.json");
                                            }
                                            //CLEAR RESTFUL INTERFACE JSON FILE
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Disks.json"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Disks.json");
                                            }

                                            //CLEAR SD CARD INFO
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "SDCARD.json"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "SDCARD.json");
                                            }
                                            //CLEAR ILO FIRMWARE INFO
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Firmware.json"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Firmware.json");
                                            }

                                            //CLEAR ILO FIRMWARE INFO
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\S" + vm.WhichTab + "InstallSet.json"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\S" + vm.WhichTab + "InstallSet.json");
                                            }


                                            vm.ProgressPercentage1 = 100;
                                            //Stop Timer
                                            timer.Stop();
                                            timeTaken = timer.Elapsed;
                                            timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressMessage1 = "Scripts completed successfully! " + timetaken;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.MessageVisibility1 = Visibility.Visible;

                                            vm.Server1TabHeader = "Server1";

                                            //Set Has System Erase Been Run Flag
                                            vm.WasFactoryReset = "Yes";

                                        }
                                        else
                                        {

                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.MessageVisibility1 = Visibility.Visible;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressPercentage1 = 0;
                                            vm.WarningMessage1 = "Please retrieve server information first";

                                        }

                                    break;
                            }

                            //CHECK FOR SD CARD
                            if (vm.GenGenericServerInfo1 != null)
                                if (vm.GenGenericServerInfo1.SDCardInserted == "Absent" || vm.GenGenericServerInfo1.SDCardInserted == "" || String.IsNullOrEmpty(vm.GenGenericServerInfo1.SDCardInserted))
                                {


                                }
                                else
                                {

                                    //**** REDFISH GET SERVER INFO!!! *****
                                    //vm.GenGenericServerInfo1 = await RedfishCrawler.GetHPData(vm.Server1IPv4, vm.OverridePasswordS1 , vm, ServerGen);

                                    vm.MessageVisibility1 = Visibility.Visible;
                                    vm.ProgressMessage1 = "";
                                    vm.GDPRRiskDetected = "Yes, Warning was issued.";
                                    vm.WarningMessage1 += " SD CARD Detected! GDPR Risk, Please remove from the system, unless this is a requested server configuration.";
                                }


                        }//end if to check server ip is not empty
                        else
                        {
                            //reset progress ring to hidden
                            vm.ProgressVisibility1 = Visibility.Hidden;
                            vm.MessageVisibility1 = Visibility.Visible;
                            vm.ProgressIsActive1 = false;
                            vm.ProgressPercentage1 = 0;
                            vm.WarningMessage1 = "Cannot contact server with Ip: " + vm.Server1IPv4 + ", Please check the Server has power.";
                            vm.IsServerThere1 = "No";
                        }

                    }// check if server there
                    else
                    {
                        //vm.ProgressMessage = "Cannot contact server with Ip: " + vm.ServerIPv4 + ", Exited Function";
                        vm.ProgressMessage1 = "Please Enter A Server IP in the top box!";
                    }

                }//end if to check vm is null


                //WRITE LOG
                if (Directory.Exists(@"" + vm.myDocs + @"\Nebula Logs"))
                {


                    //Write out log
                    if (vm.GenGenericServerInfo1 != null)
                    {
                        if (vm.GenGenericServerInfo1.ChassisSerial != null)
                            File.WriteAllText(@"" + vm.myDocs + @"\Nebula Logs\Nebula_Server_Log_" + vm.GenGenericServerInfo1.ChassisSerial.Trim() + " " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".txt", SyncLog);
                    }

                }






                //set colours
                if (string.IsNullOrEmpty(vm.WarningMessage1))
                {
                    if (ServerTab != null)
                    {
                        if (string.IsNullOrEmpty(vm.Server1IPv4))
                        {
                            ServerTab.Foreground = Brushes.MediumPurple;
                        }
                        else
                        {
                            ServerTab.Foreground = Brushes.Green;
                        }

                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;




                        if (!string.IsNullOrEmpty(vm.WarningMessage1))
                        {
                            ServerTab.Foreground = Brushes.Red;
                        }

                    }
                }


            }
            catch (Exception ex)
            {

                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (string.IsNullOrEmpty(vm.WarningMessage1))
                {
                    if (ServerTab != null)
                    {
                        if (string.IsNullOrEmpty(vm.Server1IPv4))
                        {
                            ServerTab.Foreground = Brushes.MediumPurple;
                        }
                        else
                        {
                            ServerTab.Foreground = Brushes.Green;
                        }

                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;




                        if (!string.IsNullOrEmpty(vm.WarningMessage1))
                        {
                            ServerTab.Foreground = Brushes.Red;
                        }

                    }
                }
                return;
            }

        }//end execute





        public async void sendAsyncPingPacket(string hostToPing)
        {
            try
            {
                // int timeout = 5000;
                // AutoResetEvent waiter = new AutoResetEvent(false);
                Ping pingPacket = new Ping();
                //ping completion event reaised
                pingPacket.PingCompleted += new PingCompletedEventHandler(PingCompletedCallback);
                string data = "Ping test check";
                byte[] byteBuffer = Encoding.ASCII.GetBytes(data);
                PingOptions pingOptions = new PingOptions(64, true);

                await pingPacket.SendPingAsync(hostToPing, 3000);
                //pingPacket.Send(hostToPing, 3000);

            }
            catch (PingException pe)
            {
                string exception = pe.Message;
                //IPResults.Add("INVALID IP ADDRESS FOUND");
            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                //IPResults.Add("Exceptin " + ex.Message);
            }

        }


        private void PingCompletedCallback(object sender, PingCompletedEventArgs e)
        {
            try
            {
                if (e.Cancelled)
                {

                }

                // If an error occurred, display the exception to the user.
                if (e.Error != null)
                {

                }

                PingReply reply = e.Reply;

                if (reply == null)
                {
                    vm.IsServerThere1 = "No";
                    return;
                }
                else if (reply.Status == IPStatus.Success)
                {
                    vm.IsServerThere1 = "Yes";
                    return;

                }
            }
            catch (PingException pe)
            {
                Console.WriteLine(pe.Message);
                //IPResults.Add("INVALID IP ADDRESS");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                // MessageBox.Show("Came Here");
                //IPResults.Add("Exception " + ex.Message);
            }
        }


        public void ClearXMLJSONFiles()
        {
            //CLEAR THE DATA FILES THESE WILL BE REPRODUCED LATER IN THIS SCRIPT
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml");
            }
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml");
            }
            //CLEAR RESTFUL INTERFACE JSON FILE
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "All.json"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "All.json");
            }

            //CLEAR RESTFUL INTERFACE JSON FILE
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Disks.json"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Disks.json");
            }
            //CLEAR SD CARD INFO
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "SDCARD.json"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "SDCARD.json");
            }
            //CLEAR ILO FIRMWARE INFO
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Firmware.json"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + "Firmware.json");
            }
            //CLEAR RESTFUL INTERFACE JSON FILE
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "FWLevel1.xml"))
            {
                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "FWLevel1.xml");
            }
        }


        public void FirmwareUpdateCheck()
        {


            string bios = vm.BIOSUpgradePath1?.Replace(".signed.bin", "").Replace(".bin", "");

            string ilo = vm.ILOUpgradePath1?.Replace(".signed.bin", "").Replace(".bin", "");

            string sps = vm.SPSUpgradePath1?.Replace(".signed.bin", "").Replace(".bin", "");

            string ie = vm.IENGUpgradePath1?.Replace(".signed.bin", "").Replace(".bin", "");

            vm.WarningMessage1 = "";



            //Check for Server Gen to determine 
            //bios
            if (!string.IsNullOrEmpty(bios) && !string.IsNullOrEmpty(vm.GenGenericServerInfo1.BiosCurrentVersion))
            {



                var biosArray = bios.Split('_');
                bool biosMatch = false;
                int indexPosition = 0;
                //Gen8 Uses date only in Firmware Filename no version info
                if (vm.GenGenericServerInfo1.Model.Contains("Gen8"))
                {

                    //Assign Selected Update
                    string stringWithDate = vm.BIOSUpgradePath1;

                    //Check if hyphen present
                    if (stringWithDate.Contains("-"))
                    {
                        //GEN8 BIOS
                        Match match = Regex.Match(stringWithDate, @"\d{2}-\d{2}-\d{4}");
                        string date = match.Value;

                        DateTime dateValue = new DateTime();

                        if (!string.IsNullOrEmpty(date))
                        {
                            var usCulture = "en-US";

                            dateValue = DateTime.ParseExact(date, "dd-MM-yyyy", new CultureInfo(usCulture, false)).Date;


                            var uk = dateValue.ToString(new CultureInfo("en-GB", true));
                            var us = dateValue.ToString(new CultureInfo("en-US", true).DateTimeFormat.ShortDatePattern);


                            //MessageBox.Show(us.ToString().Replace("00:00:00", "").Trim());


                            if (vm.GenGenericServerInfo1 != null)
                                if (vm.GenGenericServerInfo1.BiosCurrentVersion.Contains(us.ToString().Replace("00:00:00", "").Trim()))
                                {
                                    //Clear Update
                                    vm.BIOSUpgradePath1 = String.Empty;

                                    //CLear Flash fie 
                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin"))
                                    {
                                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin");
                                    }

                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin"))
                                    {
                                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin");
                                    }

                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.bin"))
                                    {
                                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.bin");
                                    }

                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin"))
                                    {
                                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin");
                                    }

                                    vm.UpdatesRun += @" [BIOS] ";
                                    //MessageBox.Show("Match");
                                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.BIOSUpgradePath1 + " doesn't appear to have applied. Please run Get Server Info again to double check. If not please run the update again. ");

                                }
                                else
                                {
                                    vm.WarningMessage1 += "[Firmware " + vm.BIOSUpgradePath1 + " failed! Please rerun the update.]";
                                    //MessageBox.Show("No Match");
                                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.BIOSUpgradePath1 + " doesn't appear to have applied. Please run Get Server Info again to double check. If not please run the update again. ");
                                }
                        }
                    }

                }
                else if (vm.GenGenericServerInfo1.Model.Contains("Gen9") || vm.GenGenericServerInfo1.Model.Contains("Gen10") || vm.GenGenericServerInfo1.Model.Contains("Gen 10") || vm.GenGenericServerInfo1.Model.Contains("Gen11") || vm.GenGenericServerInfo1.Model.Contains("Gen 11"))
                {


                    //Loop through array and find a match
                    foreach (var arrayStr in biosArray)
                    {
                        if (indexPosition > 0)
                        {
                            Console.WriteLine(arrayStr);
                            if (vm.GenGenericServerInfo1 != null)
                                if (vm.GenGenericServerInfo1.BiosCurrentVersion.Contains("v" + arrayStr))
                                {
                                    //As soon as a match is found exit
                                    biosMatch = true;
                                    break;

                                }
                        }

                        indexPosition += 1;


                    }
                    //Check if match was found 
                    if (biosMatch == true)
                    {
                        //Clear Update
                        vm.BIOSUpgradePath1 = String.Empty;

                        //CLear Flash fie 
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin");
                        }

                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin");
                        }

                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.bin"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.bin");
                        }

                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin");
                        }

                        vm.UpdatesRun += @" [BIOS] ";
                    }
                    else
                    {
                        vm.WarningMessage1 += "[Firmware " + vm.BIOSUpgradePath1 + " failed! Please rerun the update.]";
                        //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.BIOSUpgradePath1 + " failed to apply!!! Please rerun the update.");
                    }

                }


            }




            //ILO
            if (!string.IsNullOrEmpty(ilo) && !string.IsNullOrEmpty(vm.GenGenericServerInfo1.iLOVersion))
            {
                //Split update into array
                var iloArray = ilo.Split('_');
                bool iloMatch = false;
                //Loop through array and find a match
                foreach (var arrayStr in iloArray)
                {

                    Console.WriteLine(arrayStr);
                    if (vm.GenGenericServerInfo1 != null)
                        //Take out full stop in version to help match
                        if (vm.GenGenericServerInfo1.iLOVersion.Replace(".", "").Contains(arrayStr))
                        {
                            //As soon as a match is found exit
                            iloMatch = true;
                            break;
                        }

                }

                //Check if match was found
                if (iloMatch == true)
                {
                    //Clear Update 
                    vm.ILOUpgradePath1 = String.Empty;

                    //CLear Flash fie 
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.signed.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.signed.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"ILOUpdate.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"ILOUpdate.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"ILOUpdate.signed.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"ILOUpdate.signed.bin");
                    }

                    vm.UpdatesRun += @" [CONTROLLER] ";
                }
                else
                {
                    vm.WarningMessage1 += "[Firmware " + vm.ILOUpgradePath1 + " failed! Please rerun the update.]";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.CONTUpgradePath1 + " failed to apply!!! Please rerun the update.");
                }

            }






            //SPS

            if (!string.IsNullOrEmpty(sps) && !string.IsNullOrEmpty(vm.GenGenericServerInfo1.SPSVersion))
            {


                //Split update into array
                var spsArray = sps.Split('_');
                bool spsMatch = false;
                //Loop through array and find a match
                foreach (var arrayStr in spsArray)
                {
                    //Strip leading zeros out  (?<=.\d+.)0   ^(?!0\d)\d+(?:\.\d{1,2})?$
                    string arrayReplace = Regex.Replace(arrayStr, @"(?<=.\d+.)0", String.Empty);
                    arrayReplace = arrayReplace.Replace(".0", ".");

                    Console.WriteLine(arrayStr);
                    Console.WriteLine(arrayReplace);
                    Console.WriteLine(arrayReplace.TrimStart('0'));
                    if (vm.GenGenericServerInfo1 != null)//Strip leading zeros out
                        if (vm.GenGenericServerInfo1.SPSVersion.Contains(arrayReplace.TrimStart('0')))
                        {
                            //As soon as a match is found exit
                            spsMatch = true;
                            break;
                        }

                }

                //Check if match was found
                if (spsMatch == true)
                {
                    //Clear Update 
                    vm.SPSUpgradePath1 = String.Empty;

                    //Clear Flash files
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.signed.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.signed.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"SPSUpdate.signed.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"SPSUpdate.signed.bin");
                    }

                    //Update Certificate Field 
                    vm.UpdatesRun += @" [SPS] ";
                }
                else
                {
                    vm.WarningMessage1 += "[Firmware " + vm.SPSUpgradePath1 + " failed! Please rerun the update.]";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.CONT2UpgradePath1 + " failed to apply!!! Please rerun the update.");
                }
            }

            //Innovation Engine
            if (!string.IsNullOrEmpty(ie) && !string.IsNullOrEmpty(vm.GenGenericServerInfo1.InnovationEngine))
            {
                var ieArray = ie.Split('_');
                bool ieMatch = false;
                //Loop through array and find a match
                foreach (var arrayStr in ieArray)
                {
                    Console.WriteLine(arrayStr);
                    if (vm.GenGenericServerInfo1 != null)
                        if (vm.GenGenericServerInfo1.InnovationEngine == arrayStr)
                        {
                            //As soon as a match is found exit
                            ieMatch = true;
                            break;

                        }
                }

                //Check if match was found
                if (ieMatch == true)
                {


                    //CLEAR Flash File INFORMATION
                    vm.IENGUpgradePath1 = String.Empty;

                    //Clear Flash files
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"IENGUpdate.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"IENGUpdate.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"IENGUpdate.signed.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"IENGUpdate.signed.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"IENGUpdate.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"IENGUpdate.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"IENGUpdate.signed.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"IENGUpdate.signed.bin");
                    }
                    //for test certificate
                    vm.UpdatesRun += @" [IEngine] ";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.IDRACUpgradePath1 + " has updated successfully!");
                }
                else
                {
                    vm.WarningMessage1 += "[Firmware " + vm.IENGUpgradePath1 + " failed! Please rerun the update.]";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.IDRACUpgradePath1 + " failed to apply!!! Please rerun the update.");
                }
            }












        }

        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

    }
}

