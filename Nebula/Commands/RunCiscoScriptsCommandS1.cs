﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;
using Nebula.DellServers;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class RunCiscoScriptsCommandS1 : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public RunCiscoScriptsCommandS1(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }

        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {


            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            if (vm.Server1IPv4 != null)
            {
                //check if valid ip entered
                //System.Net.IPAddress ipAddress = null;
                //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                if (vm.Server1IPv4 != string.Empty)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

       
        }

        public async void Execute(object parameter)
        {

            var values = (object[])parameter;
            var ScriptChoice = (string)values[0];
            var ServerGen = (string)values[1];

            //A: Setup and stuff you don't want timed
            var timer = new Stopwatch();
            timer.Start();
            TimeSpan timeTaken;
            string timetaken;


            TabItem ServerTab = null;
            string ServerTabHeader = string.Empty;

            try
            {



                // MessageBox.Show(ServerGen);
                // Set the Tab Header if a normal server, skip if a blade as this already gets named by the Enclosure Function
                //if (vm.IsBla)
                //{
                switch (vm.WhichTab)
                {
                    case "1":
                        // MessageBox.Show("Server On Tab 1");

                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS1") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server1";
                        //vm.Server1TabHeader = ServerGen;
                        break;
                    case "2":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS2") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server2";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "3":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS3") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server3";
                        break;
                    case "4":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS4") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server4";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "5":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS5") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server5";
                        break;
                    case "6":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS6") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server6";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "7":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS7") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server7";
                        break;
                    case "8":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS8") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server8";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "9":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS9") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server9";
                        break;
                    case "10":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS10") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server10";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "11":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS11") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server11";
                        break;
                    case "12":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS12") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server12";
                        // MessageBox.Show("Server On Tab 2");
                        break;

                    case "13":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS13") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server13";
                        // MessageBox.Show("Server On Tab 2");
                        break;

                    case "14":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS14") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server14";
                        // MessageBox.Show("Server On Tab 2");
                        break;

                    case "15":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS15") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server15";
                        // MessageBox.Show("Server On Tab 2");
                        break;

                    case "16":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS16") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server16";
                        // MessageBox.Show("Server On Tab 2");
                        break;


                }

                //MessageBox.Show(vm.OverridePasswordS1);

                if (vm.Server1IPv4 != null)
            {
                if (vm.Server1IPv4 != string.Empty)
                {
                    string address = vm.Server1IPv4;
                    vm.WarningMessage1 = "";
                    vm.ProgressMessage1 = "";
                    //send a ping to the server to check it's available
                    vm.ProgressMessage1 = "Checking Server IP, Please Wait...";
                    vm.ProgressPercentage1 = 0;
                    vm.ProgressIsActive1 = true;
                    vm.ProgressVisibility1 = Visibility.Visible;
                    sendAsyncPingPacket(address);

                    await PutTaskDelay(3000);

                    //if it is run the code
                    if (vm.IsServerThere1 == "Yes")
                    {


                        //Set script running property to true
                        vm.IsScriptRunningS1 = true;



                        //Create and copy utility programs for local user use

                        //CANCEL SCRIPT
                        if (vm.CancelScriptS1 == true)
                        {
                            vm.ProgressPercentage1 = 0;
                            vm.ProgressIsActive1 = false;
                            vm.ProgressVisibility1 = Visibility.Hidden;
                            vm.ProgressMessage1 = "Scripts Cancelled!";
                            vm.WarningMessage1 = "";
                            vm.CancelScriptS1 = false;
                            return;
                        }
                        //END CANCEL SCRIPT



                        ProcessPiper pp = new ProcessPiper(vm);
                        //create new json parser
                        JsonParser jPar = new JsonParser(vm);


                        //RESET THE COMMAND BOX
                        vm.ILORestOutput1 = "";
                    


                        //TRY A FIXED DOCUMENT?

                        switch (ScriptChoice)
                        {


                            case "ServerInfo":


                                //MessageBox.Show(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                string SIDateClicked = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                                vm.ProgressMessage1 = "Please wait...";
                                vm.ProgressIsActive1 = true;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressVisibility1 = Visibility.Visible;

                                vm.OverridePasswordS1 = "C!5c0Password";

                                //RUN CURL REST COMMAND
                                //await System.Threading.Tasks.Task.Run(() => pp.StartCURL1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Cisco\CURL\bin\curl.exe"), @"", @" -k -w '%{json}'  -u admin:" + vm.OverridePasswordS1 + @" https://" + vm.Server1IPv4 + @"/redfish/v1/Managers/CIMC -o cimc.json", @"" + vm.myDocs + @"\HPTOOLS\Cisco\CURL\bin"));

                                //if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\Cisco\CURL\bin\cimc.json"))
                                //{
                                //    jPar.GetRESTCisco(@"" + vm.myDocs + @"\HPTOOLS\Cisco\CURL\bin\cimc.json", vm.Server1IPv4);
                                //}

                                    await System.Threading.Tasks.Task.Run(() => pp.StartCURL1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Cisco\CURL\bin\curl.exe"), @"", @" -k -w '%{json}'  -u admin:" + vm.OverridePasswordS1 + @" https://" + vm.Server1IPv4 + @"/redfish/v1/ -o CiscoRoot.json", @"" + vm.myDocs + @"\HPTOOLS\Cisco\CURL\bin"));

                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\Cisco\CURL\bin\CiscoRoot.json"))
                                    {
                                        //jPar.GetRESTCisco(@"" + vm.myDocs + @"\HPTOOLS\Cisco\CURL\bin\CiscoRoot.json", vm.Server1IPv4);

                                        jPar.GetRESTCisco("https://" + vm.Server1IPv4 + "/redfish/v1/", vm.Server1IPv4);
                                    }



                                    //await System.Threading.Tasks.Task.Run(() => pp.StartCURL1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Cisco\CURL\bin\curl.exe"), @"", @" -k -w '%{json}'  -u admin:" + vm.OverridePasswordS1 + @" https://" + vm.Server1IPv4 + @"/redfish/v1/Systems/FCH1924V2YH/Processors -o cpu.json", @"" + vm.myDocs + @"\HPTOOLS\Cisco\CURL\bin"));
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartCURL1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Cisco\CURL\bin\curl.exe"), @"", @" -k -w '%{json}'  -u admin:" + vm.OverridePasswordS1 + @" https://" + vm.Server1IPv4 + @"/redfish/v1/Systems/FCH1924V2YH/Memory -o memory.json", @"" + vm.myDocs + @"\HPTOOLS\Cisco\CURL\bin"));



                                    vm.ProgressPercentage1 = 10;


                                vm.ProgressMessage1 = "Server Inventory in progress, Please wait...";

                             

                                //check job queue
                                vm.ProgressMessage1 = "Checking Job Queue";

                            
                                vm.ProgressPercentage1 = 60;
                               
                                vm.ProgressPercentage1 = 70;
                                if (vm.CancelScriptS1 == true)
                                {
                                    vm.ProgressPercentage1 = 0;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    vm.ProgressMessage1 = "Scripts Cancelled!";
                                    vm.WarningMessage1 = "";
                                    vm.CancelScriptS1 = false;
                                    return;
                                }

                          
                            

                                

                                if (vm.CancelScriptS1 == true)
                                {
                                    vm.ProgressPercentage1 = 0;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    vm.ProgressMessage1 = "Scripts Cancelled!";
                                    vm.WarningMessage1 = "";
                                    vm.CancelScriptS1 = false;
                                    return;
                                }

                             

                                vm.ProgressPercentage1 = 80;
                             

                                // readfs data from collection
                              //vm.LenovoGetServerInventory(vm.DellServerS1, vm.DellDataCollectionS1);
                 

                            


                                //Stop Timer
                                timer.Stop();
                                timeTaken = timer.Elapsed;
                                timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");


                                                 
                            


                                vm.ProgressPercentage1 = 0;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.ProgressMessage1 = "Server Inventory complete! " + timetaken;


                         


                                break;
                            case "WipeScripts":

                                if (vm.DellServerS1 != null)//vm.Gen9ServerInfo1 != null ||
                                {

                                    //check if server info was required vm.Gen9ServerInfo.ChassisSerial != string.Empty || 
                                    if (vm.DellServerS1.BoardSerialNumber != string.Empty)
                                    {
                                        //check if firmwares were selected
                                        //if (vm.ILOUpgradePath != String.Empty || vm.BIOSUpgradePath != String.Empty)
                                        //  {


                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }


                                        vm.ProgressMessage1 = "Please wait...";
                                        vm.ProgressIsActive1 = true;
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressVisibility1 = Visibility.Visible;

                                        vm.ProgressPercentage1 = 10;
                                        //rac reset
                                        // await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  racreset soft -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                        vm.ProgressMessage1 = "Clearing Jobqueue";
                                        //CLEAR SERVER JOBS
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue delete --all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue delete -i JID_CLEARALL_FORCE", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        await PutTaskDelay(2000);

                                        //CLEAR HOSTNAME SERVERNAME
                                        vm.ProgressMessage1 = "Clearing Server Name";

                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  set System.ServerOS.HostName NewServer", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  config  cfgServerInfo.cfgServerName ''", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        vm.ProgressPercentage1 = 10;



                                        //return;


                                        //CANCEL SCRIPT  racadm set iDRAC.Info.Name idrac-server100
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT


                                        vm.ProgressMessage1 = "Clearing Asset Tag";
                                        //CLEAR ASSET TAG AND CREATE JOB
                                        // await PutTaskDelay(10000);
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  set BIOS.MiscSettings.AssetTag NewTag", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        //await PutTaskDelay(10000);
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue create BIOS.Setup.1-1", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        await PutTaskDelay(10000);


                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT

                                        vm.ProgressPercentage1 = 20;
                                        //await PutTaskDelay(10000);
                                        vm.ProgressMessage1 = "Shutting Server Down";
                                        //SHUT DOWN SERVER
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " serveraction powerdown -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        await PutTaskDelay(10000);
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " serveraction powerdown", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT


                                        vm.ProgressPercentage1 = 30;

                                        //BIOS UPDATE
                                        if (vm.BIOSUpgradePath1 != String.Empty)
                                        {
                                            vm.ProgressPercentage1 = 30;
                                            //BIOS Firmware update
                                            vm.ProgressMessage1 = "BIOS Flash Update";

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe"))
                                            {
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }


                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            await PutTaskDelay(50000);

                                        }

                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT


                                        //Check if Script is Cancelled
                                        vm.CancelServerScript("S1");

                                        vm.ProgressPercentage1 = 40;


                                        //Embedded Controller

                                        if (vm.CONTUpgradePath1 != String.Empty)
                                        {
                                            vm.ProgressPercentage1 = 40;
                                            //ilo  Firmware update
                                            vm.ProgressMessage1 = "Controller Firmware Update";
                                            // replace with loop on 
                                            vm.ProgressPercentage1 = 40;

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe"))
                                            {
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"CONTUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }



                                            await PutTaskDelay(50000);

                                        }


                                        //Additional Controller
                                        if (vm.CONT2UpgradePath1 != String.Empty)
                                        {
                                            vm.ProgressPercentage1 = 40;
                                            //ilo  Firmware update
                                            vm.ProgressMessage1 = "Controller Firmware Update";
                                            // replace with loop on 
                                            vm.ProgressPercentage1 = 40;

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe"))
                                            {
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"CONT2Update.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }


                                            await PutTaskDelay(50000);

                                        }



                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT

                                        vm.ProgressMessage1 = "Powering Server On";
                                        vm.ProgressPercentage1 = 50;
                                        //power server back on
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction hardreset", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerup", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT


                                        //Check if updates were selected
                                        if (vm.BIOSUpgradePath1 != String.Empty || vm.CONTUpgradePath1 != String.Empty || vm.CONT2UpgradePath1 != String.Empty)
                                        {




                                            //check job queue
                                            vm.ProgressMessage1 = "Checking Job Queue";


                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await PutTaskDelay(10000);
                                            vm.ProgressMessage1 = "Applying BIOS & Controller Updates!";
                                            vm.ProgressPercentage1 = 75;


                                            //bool spinUntil;


                                            //if()

                                            //spinUntil = System.Threading.SpinWait.SpinUntil(() => vm.BackOnlineS1, TimeSpan.FromSeconds(5));

                                            if (vm.DellServerS1.SystemGeneration != null)
                                            {
                                                //check if blade
                                                if (vm.DellServerS1.SystemGeneration.Contains("Modular"))
                                                {
                                                    if (vm.CONT2UpgradePath1 != String.Empty)
                                                    {
                                                        await PutTaskDelay(1000000);
                                                    }
                                                    else
                                                    {
                                                        await PutTaskDelay(1080000);
                                                    }

                                                }
                                                else
                                                {
                                                    if (vm.DellServerS1.SystemGeneration.Contains("12G"))
                                                    {
                                                        if (vm.CONT2UpgradePath1 != String.Empty)
                                                        {
                                                            await PutTaskDelay(1000000);
                                                        }
                                                        else
                                                        {
                                                            await PutTaskDelay(1080000);
                                                        }

                                                        //await PutTaskDelay(380000);
                                                    }
                                                    else if (vm.DellServerS1.SystemGeneration.Contains("13G"))
                                                    {
                                                        if (vm.CONT2UpgradePath1 != String.Empty)
                                                        {
                                                            await PutTaskDelay(1000000);
                                                        }
                                                        else
                                                        {
                                                            await PutTaskDelay(1080000);
                                                        }

                                                    }
                                                    else if (vm.DellServerS1.SystemGeneration.Contains("14G"))
                                                    {
                                                        if (vm.CONT2UpgradePath1 != String.Empty)
                                                        {
                                                            await PutTaskDelay(1000000);
                                                        }
                                                        else
                                                        {
                                                            await PutTaskDelay(1080000);
                                                        }

                                                    }
                                                    else if (vm.DellServerS1.SystemGeneration.Contains("15G"))
                                                    {
                                                        if (vm.CONT2UpgradePath1 != String.Empty)
                                                        {
                                                            await PutTaskDelay(1000000);
                                                        }
                                                        else
                                                        {
                                                            await PutTaskDelay(1080000);
                                                        }

                                                    }
                                                }
                                            }

                                            // await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            vm.ProgressPercentage1 = 80;
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            // await PutTaskDelay(200000);
                                        }
                                        else
                                        {
                                            //NO UPDATES SELECTED BUT REBOOT REQUIRED


                                            await PutTaskDelay(300000);

                                            vm.ProgressMessage1 = "Checking Job Queue";
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.ProgressPercentage1 = 75;
                                            //await PutTaskDelay(30000);
                                            vm.ProgressPercentage1 = 80;
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await PutTaskDelay(30000);
                                        }

                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT

                                        //CLEAR HOSTNAME SERVERNAME
                                        // vm.ProgressMessage1 = "Clearing Server Name";

                                        //  await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  set System.ServerOS.HostName ''", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        vm.ProgressPercentage1 = 90;
                                        //await PutTaskDelay(10000);


                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT

                                        ////REFRESH DATA

                                        vm.ProgressPercentage1 = 95;
                                        //acquire the server inventory 
                                        await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        vm.ProgressPercentage1 = 95;
                                        //Check for SD Card
                                        await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  vflashsd status", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                        //get lcd
                                        await System.Threading.Tasks.Task.Run(() => pp.StartSensorInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn get System.LCD", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT

                                        //REFRESH DATA
                                        vm.ProgressMessage1 = "Server Inventory";
                                        vm.DellDataCollectionS1 = null;
                                        vm.DellDataCollectionS1 = new ObservableCollection<DELLDataClasses>();
                                        // captures the data from the vm. DELLInfos string and adds to collection
                                        vm.DELLCaptureInventoryData(vm.DELLInfoS1, vm.DellDataCollectionS1);
                                        vm.ProgressPercentage1 = 97;
                                        //SET HOLDING OBJECT TO NULL
                                        vm.DellServerS1 = null;
                                        //CREATE A FRESH ITEM
                                        vm.DellServerS1 = new DELLServerInfo();

                                        //Clear data files
                                        ////SET HOLDING OBJECT TO NULL


                                        // readfs data from collection
                                        vm.DELLGetServerInventory(vm.DellServerS1, vm.DellDataCollectionS1);

                                        ////Clear data files
                                        //////SET HOLDING OBJECT TO NULL


                                        //// readfs data from collection
                                        //vm.DELLGetServerInventory(vm.DellServerS1, vm.DellDataCollectionS1);
                                        ////vm.DELLGetServerInventoryS1(vm.DellDataCollectionS1);
                                        ///

                                        //Stop Timer
                                        timer.Stop();
                                        timeTaken = timer.Elapsed;
                                        timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                        vm.ProgressPercentage1 = 98;

                                        // if(vm.)
                                        //check job queue
                                        vm.ProgressMessage1 = "Checking Job Queue";
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressPercentage1 = 100;
                                        vm.ProgressMessage1 = "Scripts completed successfully! Please check the Server Information is correct before moving to the next stage! " + timetaken;



                                        // //StaticFunctions.RunRemoteConsoleCommand(@"" + vm.myDocs + @"\HPTOOLS\HP iLO Integrated Remote Console\HPLOCONS.exe", @"-addr " + vm.Server1IPv4 + ":443 -name Administrator -password " + vm.OverridePasswordS1.Trim() + " -lang en");









                                        ///AQUIRE SERVER INVENTORY
                                        //vm.DELLGetServerInventory(vm.DELLInventoryS1, vm.DellServerS1, "S1");

                                        ////GetServerInventory
                                        //vm.RunRACDMScriptsCommandDellPES1.Execute(new String[] { "ServerInfo", "Dell" });
                                        //run server infor update

                                        //CHECK IF BIOS OR CONTROLLER FAILED

                                        if (vm.BIOSUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("Job Name=Firmware Update: BIOS") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "BIOSUpdate.exe is initiated.") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "BIOSUpdate.d7 is initiated."))
                                        {
                                            //ok
                                            //BIOS
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe");
                                            }

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.d7"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.d7");
                                            }
                                            //CLEAR Flash File INFORMATION
                                            vm.BIOSUpgradePath1 = String.Empty;

                                            //Check Update Signature Hasn't failed
                                            if (vm.ILORestOutput1.Contains("Unable to verify Update Package signature"))
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "";
                                                vm.WarningMessage1 = "BIOS Package signature failed! Please select an update closer to the current firmware version and try again.";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }

                                        }
                                        else if (vm.BIOSUpgradePath1 != String.Empty)
                                        {
                                            vm.WarningMessage1 = vm.WarningMessage1 + "Bios Update Failed! ";

                                            //set colours
                                            if (vm.WarningMessage1 != "")
                                            {
                                                vm.TabColorS1 = Brushes.Red;
                                            }
                                            else
                                            {
                                                vm.TabColorS1 = Brushes.Green;
                                            }

                                            return;
                                        }




                                        if (vm.CONTUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("Job Name=Firmware Update: SAS RAID") || vm.ILORestOutput1.Contains("Job Name=Firmware Update: RAID") || vm.CONT2UpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("Job Name=Firmware Update: SAS RAID") || vm.ILORestOutput1.Contains("Job Name=Firmware Update: RAID") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONTUpdate.exe is initiated.") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONT2Update.exe is initiated.") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONT2Update.exe is initiated."))
                                        {
                                            //ok
                                            //CONTROLLER  192.168.101.159CONTUpdate.exe is initiated
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe");
                                            }

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe");
                                            }
                                            vm.CONTUpgradePath1 = String.Empty;
                                            vm.CONT2UpgradePath1 = String.Empty;

                                            //Check Update Signature Hasn't failed
                                            if (vm.ILORestOutput1.Contains("Unable to verify Update Package signature"))
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "";
                                                vm.WarningMessage1 = "Controller Package signature failed! Please select an update closer to the current firmware version and try again.";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }

                                        }
                                        else if (vm.CONTUpgradePath1 != "" || vm.CONT2UpgradePath1 != "")
                                        {
                                            // vm.WarningMessage1 = vm.WarningMessage1 + " Controller Update Failed! ";

                                            //set colours
                                            if (vm.WarningMessage1 != "")
                                            {
                                                vm.TabColorS1 = Brushes.Red;
                                            }
                                            else
                                            {
                                                vm.TabColorS1 = Brushes.Green;
                                            }


                                            return;
                                        }



                                        //Clear temp update file






                                        //vm.IDRACUpgradePath1 = String.Empty;

                                        ////IDRAC
                                        //if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe"))
                                        //{
                                        //    File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe");
                                        //}


                                    }//end check if server information collected

                                }// end check if null check
                                else
                                {
                                    vm.ProgressMessage1 = "Please retrieve server information first. Click the Get Server Info Button.";
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    vm.MessageVisibility1 = Visibility.Visible;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressPercentage1 = 0;

                                }


                                break;
                            case "IDRAC":


                                if (vm.DellServerS1 != null)//vm.Gen9ServerInfo1 != null ||
                                {

                                    //check if server info was required vm.Gen9ServerInfo.ChassisSerial != string.Empty || 
                                    if (vm.DellServerS1.BoardSerialNumber != string.Empty)
                                    {

                                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"IDRACUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        //break;


                                        vm.ProgressMessage1 = "Please wait...";
                                        vm.ProgressIsActive1 = true;
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressVisibility1 = Visibility.Visible;

                                        vm.ProgressPercentage1 = 10;




                                        //RACRESET
                                        //await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  racreset hard -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        // await PutTaskDelay(40000);

                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT

                                        vm.ProgressMessage1 = "Clearing Jobqueue";
                                        //CLEAR SERVER JOBS
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue delete --all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        // await PutTaskDelay(10000);


                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT

                                        if (vm.IDRACUpgradePath1 != String.Empty)
                                        {
                                            //vm.ProgressMessage1 = "Shutting Server Down";
                                            ////SHUT DOWN SERVER
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerdown", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await PutTaskDelay(10000);

                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                                vm.WarningMessage1 = "";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }


                                            // MessageBox.Show("Slipped In");
                                            vm.ProgressPercentage1 = 30;
                                            //ilo  Firmware update
                                            vm.ProgressMessage1 = "iDRAC Firmware Update";
                                            // replace with loop on 
                                            vm.ProgressPercentage1 = 30;
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                            //UPDATE METHOD
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe"))
                                            {
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"IDRACUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }
                                            else if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7"))
                                            {
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  fwupdate -p -u -d " + vm.Server1IPv4 + @"IDRACUpdate.d7", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }
                                            else if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9"))
                                            {
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  fwupdate -p -u -d " + vm.Server1IPv4 + @"IDRACUpdate.d9", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }




                                            //FWUPDATE METHOD
                                            //System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  fwupdate -p -u -d firmimg.d9", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            // System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  fwupdate -p -u -d " + vm.Server1IPv4 + @"IDRACUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            // await PutTaskDelay(30000);
                                            //check jobqueue fwupdate -p -u -d /tmp/images
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await PutTaskDelay(480000);
                                            ////check jobqueue
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await PutTaskDelay(30000);


                                        }


                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT

                                        //Power ON Server
                                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7"))
                                        {

                                        }
                                        else
                                        {

                                            //check job queue
                                            vm.ProgressMessage1 = "Checking Job Queue";
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            //vm.ProgressMessage1 = "Powering Server On";
                                            //vm.ProgressPercentage1 = 50;
                                            ////power server back on
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerup", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await PutTaskDelay(10000);
                                        }




                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT

                                        //hard reset required to trigger 
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction hardreset", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                        if (vm.IDRACUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("IDRACUpdate.exe is initiated.") || vm.ILORestOutput1.Contains("IDRACUpdate.d7 is initiated.") || vm.IDRACUpgradePath1.Contains(".d7") || vm.ILORestOutput1.Contains("IDRACUpdate.d9 is initiated.") || vm.IDRACUpgradePath1.Contains(".d9"))
                                        {



                                            // await PutTaskDelay(32000);

                                            vm.ProgressPercentage1 = 60;
                                            vm.ProgressMessage1 = "Applying iDrac Update!";
                                            await PutTaskDelay(280000);


                                            //Power ON Server
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7"))
                                            {
                                                //check job queue
                                                vm.ProgressMessage1 = "Checking Job Queue";
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                                //vm.ProgressMessage1 = "Powering Server On";
                                                //vm.ProgressPercentage1 = 60;
                                                ////power server back on
                                                //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerup", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                //await PutTaskDelay(10000);
                                            }
                                            else
                                            {

                                            }

                                            ////await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction hardreset", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            //if (vm.DellServerS1.SystemGeneration != null)
                                            //{
                                            //    //check if blade
                                            //    if (vm.DellServerS1.SystemGeneration.Contains("Modular"))
                                            //    {
                                            //        await PutTaskDelay(440000);
                                            //    }
                                            //    else
                                            //    {
                                            //        if (vm.DellServerS1.SystemGeneration.Contains("12G"))
                                            //        {

                                            //            await PutTaskDelay(440000);
                                            //        }
                                            //        else if (vm.DellServerS1.SystemGeneration.Contains("13G"))
                                            //        {
                                            //            await PutTaskDelay(440000);
                                            //        }
                                            //        else if (vm.DellServerS1.SystemGeneration.Contains("14G"))
                                            //        {
                                            //            await PutTaskDelay(380000);
                                            //        }
                                            //        else if (vm.DellServerS1.SystemGeneration.Contains("15G"))
                                            //        {
                                            //            await PutTaskDelay(260000);
                                            //        }
                                            //    }


                                            //}

                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                            //Check Update Signature Hasn't failed
                                            if (vm.ILORestOutput1.Contains("Unable to verify Update Package signature"))
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "";
                                                vm.WarningMessage1 = "iDrac Package signature failed! Please select an update closer to the current firmware version and try again.";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }


                                            vm.ProgressPercentage1 = 65;
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await PutTaskDelay(480000);
                                            vm.ProgressPercentage1 = 70;
                                            // vm.ProgressMessage1 = "Rebooting Server";
                                            //reset server to kickstart bios controller installs
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction hardreset", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await PutTaskDelay(120000);


                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                                vm.WarningMessage1 = "";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }
                                            //END CANCEL SCRIPT

                                            ////REFRESH DATA
                                            vm.ProgressMessage1 = "Server Inventory!";
                                            vm.ProgressPercentage1 = 95;
                                            //acquire the server inventory 
                                            await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.ProgressPercentage1 = 95;
                                            //Check for SD Card
                                            await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  vflashsd status", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                            //get lcd
                                            await System.Threading.Tasks.Task.Run(() => pp.StartSensorInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn get System.LCD", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));



                                            //REFRESH DATA
                                            vm.DellDataCollectionS1 = null;
                                            vm.DellDataCollectionS1 = new ObservableCollection<DELLDataClasses>();
                                            // captures the data from the vm. DELLInfos string and adds to collection
                                            vm.DELLCaptureInventoryData(vm.DELLInfoS1, vm.DellDataCollectionS1);
                                            vm.ProgressPercentage1 = 97;
                                            //SET HOLDING OBJECT TO NULL
                                            vm.DellServerS1 = null;
                                            //CREATE A FRESH ITEM
                                            vm.DellServerS1 = new DELLServerInfo();

                                            // readfs data from collection
                                            vm.DELLGetServerInventory(vm.DellServerS1, vm.DellDataCollectionS1);


                                            //Stop Timer
                                            timer.Stop();
                                            timeTaken = timer.Elapsed;
                                            timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                            vm.ProgressPercentage1 = 98;

                                            // if(vm.)

                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressPercentage1 = 100;
                                            vm.ProgressMessage1 = "iDRAC Scripts completed successfully! Please launch Virtual Console and check for any POST issues before updating the Bios & Controller! " + timetaken;




                                            //CLEAR Flash File INFORMATION
                                            vm.IDRACUpgradePath1 = String.Empty;


                                            //IDRAC
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe");
                                            }

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7");
                                            }

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9");
                                            }

                                        }
                                        else
                                        {
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressPercentage1 = 100;
                                            vm.ProgressMessage1 = "";


                                            vm.WarningMessage1 = "iDrac Update Failed! Rerun or select another update!";
                                            //set colours
                                            if (vm.WarningMessage1 != "")
                                            {
                                                vm.TabColorS1 = Brushes.Red;
                                            }
                                            else
                                            {
                                                vm.TabColorS1 = Brushes.Green;
                                            }

                                        }







                                    }
                                }


                                break;
                            case "Combined Updates":

                                if (vm.DellServerS1 != null)//vm.Gen9ServerInfo1 != null ||
                                {

                                    //check if server info was required vm.Gen9ServerInfo.ChassisSerial != string.Empty || 
                                    if (vm.DellServerS1.BoardSerialNumber != string.Empty)
                                    {

                                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"IDRACUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        //break;


                                        vm.ProgressMessage1 = "Please wait...";
                                        vm.ProgressIsActive1 = true;
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressVisibility1 = Visibility.Visible;

                                        vm.ProgressPercentage1 = 10;




                                        //RACRESET
                                        //await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  racreset hard -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        // await PutTaskDelay(40000);

                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT






                                        vm.ProgressMessage1 = "Clearing Jobqueue";
                                        //CLEAR SERVER JOBS
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue delete --all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue delete -i JID_CLEARALL_FORCE", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        await PutTaskDelay(2000);

                                        //CLEAR HOSTNAME SERVERNAME
                                        vm.ProgressMessage1 = "Clearing Server Name";

                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  set System.ServerOS.HostName NewServer", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  config  cfgServerInfo.cfgServerName ''", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        vm.ProgressPercentage1 = 10;



                                        //return;


                                        //CANCEL SCRIPT  racadm set iDRAC.Info.Name idrac-server100
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT


                                        vm.ProgressMessage1 = "Clearing Asset Tag";
                                        //CLEAR ASSET TAG AND CREATE JOB
                                        // await PutTaskDelay(10000);
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  set BIOS.MiscSettings.AssetTag NewTag", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        //await PutTaskDelay(10000);
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue create BIOS.Setup.1-1", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        await PutTaskDelay(10000);












                                        if (vm.IDRACUpgradePath1 != String.Empty)
                                        {


                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                                vm.WarningMessage1 = "";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }


                                            // MessageBox.Show("Slipped In");
                                            vm.ProgressPercentage1 = 30;
                                            //ilo  Firmware update
                                            vm.ProgressMessage1 = "iDRAC Firmware Update";
                                            // replace with loop on 
                                            vm.ProgressPercentage1 = 30;
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                            //UPDATE METHOD
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe"))
                                            {
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"IDRACUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }
                                            else if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7"))
                                            {
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  fwupdate -p -u -d " + vm.Server1IPv4 + @"IDRACUpdate.d7", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }
                                            else if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9"))
                                            {
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  fwupdate -p -u -d " + vm.Server1IPv4 + @"IDRACUpdate.d9", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }

                                            await PutTaskDelay(30000);



                                        }
                                        else
                                        {
                                            //Reboot System
                                            ////power server back on
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerup", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await PutTaskDelay(10000);
                                        }


                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT





                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                        //hard reset required to trigger 
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction hardreset", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                        if (vm.IDRACUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("IDRACUpdate.exe is initiated.") || vm.ILORestOutput1.Contains("IDRACUpdate.d7 is initiated.") || vm.IDRACUpgradePath1.Contains(".d7"))
                                        {



                                            // await PutTaskDelay(32000);

                                            vm.ProgressPercentage1 = 60;
                                            vm.ProgressMessage1 = "Applying iDrac Update!";
                                            //Wait for RAC Reset
                                            await PutTaskDelay(300000);

                                            ////Restart Server
                                            //  await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction hardreset", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                            await PutTaskDelay(10000);

                                            // await PutTaskDelay(120000);





                                            //Power ON Server
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7"))
                                            {
                                                //check job queue
                                                vm.ProgressMessage1 = "Checking Job Queue";
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                                //vm.ProgressMessage1 = "Powering Server On";
                                                //vm.ProgressPercentage1 = 60;
                                                ////power server back on
                                                // await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerup", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                await PutTaskDelay(10000);
                                            }
                                            else
                                            {


                                                ////power server back on
                                                // await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerup", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                await PutTaskDelay(10000);

                                            }


                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                                vm.WarningMessage1 = "";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }
                                            //END CANCEL SCRIPT




                                            //CLEAR Flash File INFORMATION
                                            vm.IDRACUpgradePath1 = String.Empty;


                                            //IDRAC
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe");
                                            }

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7");
                                            }

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9");
                                            }
                                        }


                                        //BIOS & CONTROLLER

                                        vm.ProgressMessage1 = "Shutting Server Down";
                                        //SHUT DOWN SERVER
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerdown -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        await PutTaskDelay(10000);
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerdown", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }


                                        if (vm.BIOSUpgradePath1 != String.Empty)
                                        {
                                            vm.ProgressPercentage1 = 70;
                                            //BIOS Firmware update
                                            vm.ProgressMessage1 = "BIOS Flash Update";

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe"))
                                            {
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }
                                            else if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.d7"))
                                            {
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  fwupdate -p -u -d " + vm.Server1IPv4 + @"BIOSUpdate.d7", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }

                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            await PutTaskDelay(60000);

                                        }

                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT


                                        //Check if Script is Cancelled
                                        vm.CancelServerScript("S1");
                                        await PutTaskDelay(10000);

                                        vm.ProgressPercentage1 = 80;

                                        //Embedded Controller

                                        if (vm.CONTUpgradePath1 != String.Empty)
                                        {
                                            vm.ProgressPercentage1 = 40;
                                            //ilo  Firmware update
                                            vm.ProgressMessage1 = "Controller Firmware Update";
                                            // replace with loop on 
                                            vm.ProgressPercentage1 = 40;

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe"))
                                            {
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"CONTUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }



                                            await PutTaskDelay(60000);

                                        }


                                        //Additional Controller
                                        if (vm.CONT2UpgradePath1 != String.Empty)
                                        {
                                            vm.ProgressPercentage1 = 40;
                                            //ilo  Firmware update
                                            vm.ProgressMessage1 = "Controller Firmware Update";
                                            // replace with loop on 
                                            vm.ProgressPercentage1 = 40;

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe"))
                                            {
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"CONT2Update.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }


                                            await PutTaskDelay(60000);

                                        }


                                        vm.ProgressMessage1 = "Shutting Server Down";
                                        //SHUT DOWN SERVER
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerup -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        await PutTaskDelay(10000);



                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }









                                        //if(vm.IDRACUpgradePath1 != String.Empty)
                                        if (vm.DellServerS1.SystemGeneration != null)
                                        {
                                            //check if blade
                                            if (vm.DellServerS1.SystemGeneration.Contains("Modular"))
                                            {
                                                await PutTaskDelay(1000000);
                                            }
                                            else
                                            {
                                                if (vm.DellServerS1.SystemGeneration.Contains("12G"))
                                                {

                                                    await PutTaskDelay(1000000);
                                                }
                                                else if (vm.DellServerS1.SystemGeneration.Contains("13G"))
                                                {
                                                    await PutTaskDelay(1000000);
                                                }
                                                else if (vm.DellServerS1.SystemGeneration.Contains("14G"))
                                                {
                                                    await PutTaskDelay(1000000);
                                                }

                                            }


                                        }


                                        //CHECK IF BIOS OR CONTROLLER FAILED

                                        if (vm.BIOSUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("Job Name=Firmware Update: BIOS") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "BIOSUpdate.exe is initiated.") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "BIOSUpdate.d7 is initiated."))
                                        {
                                            //ok
                                            //BIOS
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe");
                                            }

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.d7"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.d7");
                                            }
                                            //CLEAR Flash File INFORMATION
                                            vm.BIOSUpgradePath1 = String.Empty;

                                            //Check Update Signature Hasn't failed
                                            if (vm.ILORestOutput1.Contains("Unable to verify Update Package signature"))
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "";
                                                vm.WarningMessage1 = "BIOS Package signature failed! Please select an update closer to the current firmware version and try again.";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }

                                        }
                                        else if (vm.BIOSUpgradePath1 != String.Empty)
                                        {
                                            vm.WarningMessage1 = vm.WarningMessage1 + "Bios Update Failed! ";

                                            //set colours
                                            if (vm.WarningMessage1 != "")
                                            {
                                                vm.TabColorS1 = Brushes.Red;
                                            }
                                            else
                                            {
                                                vm.TabColorS1 = Brushes.Green;
                                            }

                                            return;
                                        }




                                        if (vm.CONTUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("Job Name=Firmware Update: SAS RAID") || vm.ILORestOutput1.Contains("Job Name=Firmware Update: RAID") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONTUpdate.exe is initiated.") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONT2Update.exe is initiated."))
                                        {
                                            //ok
                                            //CONTROLLER  192.168.101.159CONTUpdate.exe is initiated
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe");
                                            }

                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe"))
                                            {
                                                File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe");
                                            }
                                            vm.CONTUpgradePath1 = String.Empty;


                                            //Check Update Signature Hasn't failed
                                            if (vm.ILORestOutput1.Contains("Unable to verify Update Package signature"))
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "";
                                                vm.WarningMessage1 = "Controller Package signature failed! Please select an update closer to the current firmware version and try again.";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }

                                        }
                                        else if (vm.CONTUpgradePath1 != String.Empty)
                                        {
                                            //vm.WarningMessage1 = vm.WarningMessage1 + " Controller Update Failed! ";

                                            //set colours
                                            if (vm.WarningMessage1 != "")
                                            {
                                                vm.TabColorS1 = Brushes.Red;
                                            }
                                            else
                                            {
                                                vm.TabColorS1 = Brushes.Green;
                                            }


                                            return;
                                        }




                                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction hardreset", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));







                                        vm.ProgressPercentage1 = 65;
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        //await PutTaskDelay(480000);
                                        vm.ProgressPercentage1 = 70;
                                        // vm.ProgressMessage1 = "Rebooting Server";
                                        //reset server to kickstart bios controller installs
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction hardreset", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        //await PutTaskDelay(120000);




                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT

                                        ////REFRESH DATA
                                        vm.ProgressMessage1 = "Server Inventory!";
                                        vm.ProgressPercentage1 = 95;
                                        //acquire the server inventory 
                                        await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        vm.ProgressPercentage1 = 95;
                                        //Check for SD Card
                                        await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  vflashsd status", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                        //get lcd
                                        await System.Threading.Tasks.Task.Run(() => pp.StartSensorInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn get System.LCD", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));



                                        //REFRESH DATA
                                        vm.DellDataCollectionS1 = null;
                                        vm.DellDataCollectionS1 = new ObservableCollection<DELLDataClasses>();
                                        // captures the data from the vm. DELLInfos string and adds to collection
                                        vm.DELLCaptureInventoryData(vm.DELLInfoS1, vm.DellDataCollectionS1);
                                        vm.ProgressPercentage1 = 97;
                                        //SET HOLDING OBJECT TO NULL
                                        vm.DellServerS1 = null;
                                        //CREATE A FRESH ITEM
                                        vm.DellServerS1 = new DELLServerInfo();

                                        // readfs data from collection
                                        vm.DELLGetServerInventory(vm.DellServerS1, vm.DellDataCollectionS1);


                                        //Stop Timer
                                        timer.Stop();
                                        timeTaken = timer.Elapsed;
                                        timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                        vm.ProgressPercentage1 = 98;

                                        // if(vm.)

                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressPercentage1 = 100;
                                        vm.ProgressMessage1 = "iDRAC Scripts completed successfully! Please launch Virtual Console and check for any POST issues before updating the Bios & Controller! " + timetaken;










                                        //else
                                        //{




                                        //    vm.ProgressVisibility1 = Visibility.Hidden;
                                        //    vm.ProgressIsActive1 = false;
                                        //    vm.ProgressPercentage1 = 100;
                                        //    vm.ProgressMessage1 = "";


                                        //    vm.WarningMessage1 = "iDrac Update Failed! Rerun or select another update!";
                                        //    //set colours
                                        //    if (vm.WarningMessage1 != "")
                                        //    {
                                        //        vm.TabColorS1 = Brushes.Red;
                                        //    }
                                        //    else
                                        //    {
                                        //        vm.TabColorS1 = Brushes.Green;
                                        //    }

                                        //}







                                    }
                                }


                                break;
                            case "FinalScripts":
                                if (vm.DellServerS1 != null)
                                    if (vm.DellServerS1.BoardSerialNumber != string.Empty)
                                    {
                                        //////EXTRACT FIRMWARE JSON
                                        vm.ProgressMessage1 = "Please wait...";
                                        vm.ProgressIsActive1 = true;
                                        vm.ProgressPercentage1 = 0;
                                        vm.MessageVisibility1 = Visibility.Visible;
                                        vm.ProgressVisibility1 = Visibility.Visible;
                                        vm.ProgressPercentage1 = 0;



                                        vm.ProgressMessage1 = "Clearing Jobqueue";
                                        //CLEAR SERVER JOBS
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue delete --all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        // await PutTaskDelay(10000);



                                        vm.ProgressPercentage1 = 20;
                                        //await PutTaskDelay(10000);
                                        vm.ProgressMessage1 = "Shutting Server Down";
                                        //SHUT DOWN SERVER
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerdown -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        await PutTaskDelay(10000);
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerdown", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT

                                        //RAC RESET CONFIG
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  racresetcfg -all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));systemerase secureerasepd,vflash,percnvcache
                                        //SYSTEM ERASE INCLUDES BIOS\LCLOG\RACRESETCONFIG
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  systemerase bios,idrac,lcdata", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        //TO RESET SYSTEM



                                        //LICENSE CHECK
                                        vm.ProgressMessage1 = "Checking License, Please wait...";

                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " license view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                        //To Place in Factory Defaults Capture 
                                        if (vm.ILORestOutput1.Contains("Transaction ID       =") && vm.ILORestOutput1.Contains("License Type         = EVALUATION"))
                                        {
                                            vm.ProgressMessage1 = "Removing Evaluation License, Please wait...";

                                            //Capture ID
                                            string tranID = vm.ILORestOutput1.Substring(vm.ILORestOutput1.IndexOf("Transaction ID       ="), 27).Replace("Transaction ID       =", "").Trim();

                                            //Remove Eval License
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " license delete -t" + tranID, @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            //Reset Flag if true
                                            vm.DellBasicLicS1 = false;
                                        }
                                        //LICENSE END



                                        //racadm systemerase <component>,<component>,<component>
                                        // <component>—the valid types of components are:
                                        //○ bios—To reset the BIOS to default.
                                        //○ diag—To erase embedded diagnostics. 
                                        //○ drvpack—To erase embedded OS driver pack.
                                        //○ idrac—To reset the iDRAC to default. 
                                        //○ lcdata—To erase Lifecycle Controller data. 
                                        //○ allaps—To reset all apps.
                                        //○ secureerasepd—To erase the physical disk.This supports SED, NVMe drives, and PCIe cards
                                        //○ overwritepd—To overwrite physical disk. This supports SAS and SATA drives.
                                        //○ percnvcache—To erase NV cache. ○ vflash—To erase vFlash. 
                                        //○ nvdimm—To erase all NonVolatileMemory. 

                                        //CANCEL SCRIPT
                                        if (vm.CancelScriptS1 == true)
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "Scripts Cancelled!";
                                            vm.WarningMessage1 = "";
                                            vm.CancelScriptS1 = false;
                                            return;
                                        }
                                        //END CANCEL SCRIPT

                                        //FOR ILO RESTFUL FACTORY DEFAULTS
                                        vm.ProgressMessage1 = "Resetting iDrac to Factory Defaults";
                                        vm.ProgressPercentage1 = 50;
                                        await PutTaskDelay(280000);
                                        vm.ProgressPercentage1 = 80;
                                        await PutTaskDelay(240000);
                                        vm.ProgressPercentage1 = 100;

                                        if (vm.ILORestOutput1.Contains("SystemErase operation initiated successfully."))
                                        {
                                            //Add CSV WRITE HERE Instead of 
                                            // StaticFunctions.CreateAppendCSV(new ReportServersProcessed(vm.CurrentUser, vm.PurchaseOrderNumber, vm.ChassisSerialNo, "Goods In", "OK", ""), @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed.csv");
                                        }
                                        else
                                        {
                                            vm.WarningMessage1 = "Factory Reset appears to have failed! Please run the Factory Reset again.";
                                        }




                                        //Stop Timer
                                        timer.Stop();
                                        timeTaken = timer.Elapsed;
                                        timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.MessageVisibility1 = Visibility.Visible;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressMessage1 = "Scripts completed successfully! " + timetaken;
                                        vm.Server1TabHeader = "Server1";

                                    }
                                    else
                                    {

                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.MessageVisibility1 = Visibility.Visible;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressPercentage1 = 0;
                                        vm.WarningMessage1 = "Please retrieve server information first";

                                    }

                                break;
                        }





                        //    //CHECK SPS VERSION IF V4 UPGRADE REQUIRED
                        //    if (vm.Gen9ServerInfo1 != null)
                        //        vm.FirmwareVersionCheck("3.0.6", "1", vm.Gen9ServerInfo1.SPSVersion, "System Requires V4 Upgrade! Please launch Remote Console to perform the update.");


                        //} //MIN ILO FIRMWARE CHECK
                        //else
                        //{
                        //    //Attempt to upgrade ilo
                        //    if (vm.ILOUpgradePath1 != string.Empty)
                        //    {

                        //        vm.WarningMessage1 = string.Empty;
                        //        vm.ProgressMessage1 = "Please wait...";
                        //        vm.WarningMessage1 = "";
                        //        vm.ProgressIsActive1 = true;
                        //        vm.ProgressPercentage1 = 0;
                        //        vm.ProgressVisibility1 = Visibility.Visible;

                        //        vm.ProgressPercentage1 = 50;
                        //        //Check if ILO FLASH FILE SELECTED
                        //        if (vm.ILOUpgradePath1 != String.Empty)
                        //        {
                        //            //ilo  Firmware update
                        //            vm.ProgressMessage1 = "iLO Firmware Update";
                        //            // replace with loop on 
                        //            vm.ProgressPercentage1 = 80;
                        //            await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                        //            await PutTaskDelay(80000);
                        //        }


                        //        vm.ProgressMessage1 = "Extracting Json Objects";
                        //        //GET ALL SYSTEM INFO NOW THE SCRIPTS HAVE RUN AND FIRMWARE UPDATES
                        //        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "save --multisave Bios.,Chassis.,fwswversioninventory.,ComputerSystem.,Manager. -f" + vm.Server1IPv4 + "All.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
                        //        // await PutTaskDelay(5000);
                        //        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "rawget /redfish/v1/managers/1/embeddedmedia -f" + vm.Server1IPv4 + "SDCARD.json  --url " + vm.Server1IPv4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

                        //        //GET THE EMBEDDED HEALTH INFO COMES IN AS XML
                        //        await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\GetEmbeddedHealth.xml -l " + vm.Server1IPv4 + "ServerHealth1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));

                        //        //*******Read the Data from the JSON and XML Files pulled from the Server*********
                        //        ReadJsonXMLObjects(ServerGen);


                        //        vm.ProgressVisibility1 = Visibility.Hidden;
                        //        vm.ProgressIsActive1 = false;
                        //        vm.ProgressPercentage1 = 100;
                        //        vm.ProgressMessage1 = "Scripts completed successfully! iLO now Upgraded, please proceed to run the main scripts.";

                        //        vm.ILOUpgradePath1 = string.Empty;
                        //        vm.ILOMinVersion1 = string.Empty;
                        //    }

                        //    ////IF ILO REQUIRES MIN UPGRADE TO SUPPORT RESTFUL
                        //    ////clear progress
                        //    //vm.ProgressMessage1 = "";
                        //    //vm.ProgressPercentage1 = 0;
                        //    vm.ProgressIsActive1 = false;
                        //    vm.ProgressVisibility1 = Visibility.Hidden;
                        //}

                    }//end if to check server ip is not empty
                    else
                    {
                        //reset progress ring to hidden
                        vm.ProgressVisibility1 = Visibility.Hidden;
                        vm.MessageVisibility1 = Visibility.Visible;
                        vm.ProgressIsActive1 = false;
                        vm.ProgressPercentage1 = 0;
                        vm.WarningMessage1 = "Cannot contact server with Ip: " + vm.Server1IPv4 + ", Please check the Server has power.";
                        vm.IsServerThere1 = "No";
                    }

                }// check if server there
                else
                {
                    //vm.ProgressMessage = "Cannot contact server with Ip: " + vm.ServerIPv4 + ", Exited Function";
                    vm.ProgressMessage1 = "Please Enter A Server IP in the top box!";
                }

            }//end if to check vm is null

            //Clear data files
            // ClearXMLJSONFiles();





            //Set script running property to false
            vm.IsScriptRunningS1 = false;

                //set colours
                if (string.IsNullOrEmpty(vm.WarningMessage1))
                {
                    if (ServerTab != null)
                    {
                        if (string.IsNullOrEmpty(vm.Server1IPv4))
                        {
                            ServerTab.Foreground = Brushes.MediumPurple;
                        }
                        else
                        {
                            ServerTab.Foreground = Brushes.Green;
                        }

                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;




                        if (!string.IsNullOrEmpty(vm.WarningMessage1))
                        {
                            ServerTab.Foreground = Brushes.Red;
                        }

                    }
                }

            }
            catch (Exception ex)
            {

                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (string.IsNullOrEmpty(vm.WarningMessage1))
                {
                    if (ServerTab != null)
                    {
                        if (string.IsNullOrEmpty(vm.Server1IPv4))
                        {
                            ServerTab.Foreground = Brushes.MediumPurple;
                        }
                        else
                        {
                            ServerTab.Foreground = Brushes.Green;
                        }

                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;




                        if (!string.IsNullOrEmpty(vm.WarningMessage1))
                        {
                            ServerTab.Foreground = Brushes.Red;
                        }

                    }
                }
                return;
            }


        }//end execute






        public async void sendAsyncPingPacket(string hostToPing)
        {
            try
            {
                // int timeout = 5000;
                // AutoResetEvent waiter = new AutoResetEvent(false);
                Ping pingPacket = new Ping();
                //ping completion event reaised
                pingPacket.PingCompleted += new PingCompletedEventHandler(PingCompletedCallback);
                string data = "Ping test check";
                byte[] byteBuffer = Encoding.ASCII.GetBytes(data);
                PingOptions pingOptions = new PingOptions(64, true);

                await pingPacket.SendPingAsync(hostToPing, 3000);
                //pingPacket.Send(hostToPing, 3000);

            }
            catch (PingException pe)
            {
                string exception = pe.Message;
                //IPResults.Add("INVALID IP ADDRESS FOUND");
            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                //IPResults.Add("Exceptin " + ex.Message);
            }

        }


        private void PingCompletedCallback(object sender, PingCompletedEventArgs e)
        {
            try
            {
                if (e.Cancelled)
                {

                }

                // If an error occurred, display the exception to the user.
                if (e.Error != null)
                {

                }

                PingReply reply = e.Reply;

                if (reply == null)
                {
                    vm.IsServerThere1 = "No";
                    return;
                }
                else if (reply.Status == IPStatus.Success)
                {
                    vm.IsServerThere1 = "Yes";
                    return;

                }
            }
            catch (PingException pe)
            {
                Console.WriteLine(pe.Message);
                //IPResults.Add("INVALID IP ADDRESS");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                // MessageBox.Show("Came Here");
                //IPResults.Add("Exception " + ex.Message);
            }
        }

        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }


    }
}
