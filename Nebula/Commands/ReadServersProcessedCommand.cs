﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Windows.Input;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;
using System.Windows.Data;
using System.ComponentModel;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Data.SQLite;

namespace Nebula.Commands
{
    public class ReadServersProcessedCommand : ICommand
    {
        //declare viewmodel
        GoodsInOutViewModel vm = null;



        //SQL Lite DB LINK
        public string cs = "";

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public ReadServersProcessedCommand(GoodsInOutViewModel TheViewModel)
        {
            this.vm = TheViewModel;

            //Check if Test Mode Checked
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\ServersProcessed.db";
            }
            else
            {
                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/ServersProcessed/ServersProcessed.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/ServersProcessed/ServersProcessed.db";
                }
                else
                {

                    //Set connection string to Live DB
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\ServersProcessed.db";
                }
            }
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            ////use helper to get control
            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

            //if (lv != null)
            //{


            //    if (lv.SelectedItems.Count > 0)
            //    {

            //        //MessageBox.Show(string.IsNullOrEmpty(vm.StepDescription).ToString() + "   " + string.IsNullOrEmpty(vm.StepCliCommand).ToString());
            //        //return false;  
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //        //return true;
            //    }


            //}
            //else
            //{
            //    return false;
            //}
            return true;

        }

        public async void Execute(object parameter)
        {
          

            var values = (object)parameter;

            //MessageBox.Show("Here");
            //use helper to get control
            // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "ServersProcessedLV") as ListView;

           

            //Override Mouse Cursor
            Mouse.OverrideCursor = Cursors.Wait;
            vm.ProgressIsActive1 = true;
            vm.ProgressVisibility1 = Visibility.Visible;

            vm.ServersProcessedCount = "0";

            switch (values)
            {
                case "All":
                    vm.ServersProcessedCollection = new ObservableCollection<ServersProcessedModel>();

                    vm.ServersProcessedCollection.Clear();

                    await System.Threading.Tasks.Task.Run(() => ReadAllData(CreateConnection(cs)));

                    break;
                case "Monthly":
                    vm.ServersProcessedCollection = new ObservableCollection<ServersProcessedModel>();

                    vm.ServersProcessedCollection.Clear();

                    await System.Threading.Tasks.Task.Run(() => ReadMonthlyData(CreateConnection(cs)));
                  
                    break;


                case "Weekly":
                    vm.ServersProcessedCollection = new ObservableCollection<ServersProcessedModel>();

                    vm.ServersProcessedCollection.Clear();
                    await System.Threading.Tasks.Task.Run(() => ReadWeeklyData(CreateConnection(cs)));
                
                    break;

                case "Search":
                    vm.ServersProcessedCollection = new ObservableCollection<ServersProcessedModel>();

                    vm.ServersProcessedCollection.Clear();

                    await System.Threading.Tasks.Task.Run(() => ReadSearchData(CreateConnection(cs)));
                   
                    break;

                case "Yearly":
                    vm.ServersProcessedCollection = new ObservableCollection<ServersProcessedModel>();

                    vm.ServersProcessedCollection.Clear();

                    await System.Threading.Tasks.Task.Run(() => ReadYearlyData(CreateConnection(cs)));
              
                    break;

                case "ExportCSV":

                    ExportCSV();

                    break;

                default:
                    break;
            }

            //Add Count of collection
            if(vm.ServersProcessedCollection != null)
            {
                vm.ServersProcessedCount = vm.ServersProcessedCollection.Count.ToString();
            }

            vm.ProgressIsActive1 = false;
            vm.ProgressVisibility1 = Visibility.Hidden;
            //Override Mouse Cursor
            Mouse.OverrideCursor = null;
        }




        public void ReadAllData(SQLiteConnection conn)
        {
            try
            {

                //Override Mouse Cursor
               // Mouse.OverrideCursor = Cursors.Wait;

                vm.ServersProcessedCollection = new ObservableCollection<ServersProcessedModel>();

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = "SELECT * FROM Processed";

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ServersProcessedModel itm = new ServersProcessedModel();
                    if (sqlite_datareader[1].GetType() != typeof(DBNull))
                        itm.DateProcessed = sqlite_datareader.GetDateTime(1);
                    if (sqlite_datareader[2].GetType() != typeof(DBNull))
                        itm.ChassisSerialNo = sqlite_datareader.GetString(2);
                    if (sqlite_datareader[3].GetType() != typeof(DBNull))
                        itm.Manufacturer = sqlite_datareader.GetString(3);
                    if (sqlite_datareader[4].GetType() != typeof(DBNull))
                        itm.ModelNo = sqlite_datareader.GetString(4);
                    if (sqlite_datareader[5].GetType() != typeof(DBNull))
                        itm.POSONo = sqlite_datareader.GetString(5);
                    if (sqlite_datareader[6].GetType() != typeof(DBNull))
                        itm.Operative = sqlite_datareader.GetString(6);
                    if (sqlite_datareader[7].GetType() != typeof(DBNull))
                        itm.OperativeLocation = sqlite_datareader.GetString(7);
                    if (sqlite_datareader[8].GetType() != typeof(DBNull))
                        itm.Department = sqlite_datareader.GetString(8);
                    if (sqlite_datareader[9].GetType() != typeof(DBNull))
                        itm.TimeCompleted = sqlite_datareader.GetString(9);
                    if (sqlite_datareader[10].GetType() != typeof(DBNull))
                        itm.ServerState = sqlite_datareader.GetString(10);
                    if (sqlite_datareader[11].GetType() != typeof(DBNull))
                        itm.FactoryReset = sqlite_datareader.GetString(11);
                    if (sqlite_datareader[12].GetType() != typeof(DBNull))
                        itm.Notes = sqlite_datareader.GetString(12);
                    if (sqlite_datareader[13].GetType() != typeof(DBNull))
                        itm.FaultDescription = sqlite_datareader.GetString(13);
                    if (sqlite_datareader[14].GetType() != typeof(DBNull))
                        itm.GDPRRiskDetected = sqlite_datareader.GetString(14);


                   

                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        vm.ServersProcessedCollection.Add(itm);
                    });

                }

                // await PutTaskDelay(400);

                //Override Mouse Cursor
               // Mouse.OverrideCursor = null;

                // conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //MessageBox.Show(ex.Message);
            }
        }


        public void ReadSearchData(SQLiteConnection conn)
        {
            try
            {


                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();

                             
                    vm.DateFrom = vm.DateFrom;
                    vm.DateTo = vm.DateTo;

           
                //Check if date range tickbox checked
                if (vm.ServersProcessedDateSearch == true)
                {

                    //Query With Date Range
                    //Search based off start of week
                    if (!string.IsNullOrEmpty(vm.ServersProcessedSearch))
                    {
                        sqlite_cmd.CommandText = "SELECT * FROM Processed WHERE ProcessedDate BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "' AND Operative LIKE '" + vm.ServersProcessedSearch.ToUpper() + "' || '%' OR ChassisSerial = '" + vm.ServersProcessedSearch.ToUpper() + "' || '%' OR POSONumber LIKE '" + vm.ServersProcessedSearch.ToUpper() + "' || '%'";
                    }
                    else
                    {

                        sqlite_cmd.CommandText = "SELECT * FROM Processed WHERE ProcessedDate BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "'";
                    }

                }
                else
                {

                    if (!string.IsNullOrEmpty(vm.ServersProcessedSearch))
                    {

                        //Normal query
                        sqlite_cmd.CommandText = "SELECT * FROM Processed WHERE POSONumber LIKE '" + vm.ServersProcessedSearch.ToUpper() + "' || '%' OR ChassisSerial LIKE '" + vm.ServersProcessedSearch.ToUpper() + "' || '%' || '%' OR Operative LIKE '" + vm.ServersProcessedSearch.ToUpper() + "' || '%'";
                    }


                }
                //sqlite_cmd.CommandText = "SELECT * FROM Processed WHERE Operative LIKE '" + vm.CurrentUser + " " + "'";

                //"SELECT * FROM DriveInfo WHERE PONumber LIKE '" + searchFilter + "' || '%' OR SerialNo LIKE '" + searchFilter + "' || '%'");

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ServersProcessedModel itm = new ServersProcessedModel();
                    if (sqlite_datareader[1].GetType() != typeof(DBNull))
                        itm.DateProcessed = sqlite_datareader.GetDateTime(1);
                    if (sqlite_datareader[2].GetType() != typeof(DBNull))
                        itm.ChassisSerialNo = sqlite_datareader.GetString(2);
                    if (sqlite_datareader[3].GetType() != typeof(DBNull))
                        itm.Manufacturer = sqlite_datareader.GetString(3);
                    if (sqlite_datareader[4].GetType() != typeof(DBNull))
                        itm.ModelNo = sqlite_datareader.GetString(4);
                    if (sqlite_datareader[5].GetType() != typeof(DBNull))
                        itm.POSONo = sqlite_datareader.GetString(5);
                    if (sqlite_datareader[6].GetType() != typeof(DBNull))
                        itm.Operative = sqlite_datareader.GetString(6);
                    if (sqlite_datareader[7].GetType() != typeof(DBNull))
                        itm.OperativeLocation = sqlite_datareader.GetString(7);
                    if (sqlite_datareader[8].GetType() != typeof(DBNull))
                        itm.Department = sqlite_datareader.GetString(8);
                    if (sqlite_datareader[9].GetType() != typeof(DBNull))
                        itm.TimeCompleted = sqlite_datareader.GetString(9);
                    if (sqlite_datareader[10].GetType() != typeof(DBNull))
                        itm.ServerState = sqlite_datareader.GetString(10);
                    if (sqlite_datareader[11].GetType() != typeof(DBNull))
                        itm.FactoryReset = sqlite_datareader.GetString(11);
                    if (sqlite_datareader[12].GetType() != typeof(DBNull))
                        itm.Notes = sqlite_datareader.GetString(12);
                    if (sqlite_datareader[13].GetType() != typeof(DBNull))
                        itm.FaultDescription = sqlite_datareader.GetString(13);
                    if (sqlite_datareader[14].GetType() != typeof(DBNull))
                        itm.GDPRRiskDetected = sqlite_datareader.GetString(14);




                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        vm.ServersProcessedCollection.Add(itm);
                    });

                    //vm.ServersProcessedCollection.Add(itm);
                    //vm.ServersInternalCollection.Add(itm);


                }

           
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //MessageBox.Show(ex.Message);

                //return null;
            }
}

        public void ReadWeeklyData(SQLiteConnection conn)
        {
            try
            {

                //Override Mouse Cursor
              
               
                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();

                //Set Initial Dates for Weekly Records
                vm.DateFrom = DateTimeExtensions.StartOfWeek(DateTime.Now, DayOfWeek.Monday);
                vm.DateTo = DateTime.Now;
              
                //Search based off start of week             
                sqlite_cmd.CommandText = "SELECT * FROM Processed WHERE ProcessedDate BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "'";
             

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ServersProcessedModel itm = new ServersProcessedModel();
                    if (sqlite_datareader[1].GetType() != typeof(DBNull))
                        itm.DateProcessed = sqlite_datareader.GetDateTime(1);
                    if (sqlite_datareader[2].GetType() != typeof(DBNull))
                        itm.ChassisSerialNo = sqlite_datareader.GetString(2);
                    if (sqlite_datareader[3].GetType() != typeof(DBNull))
                        itm.Manufacturer = sqlite_datareader.GetString(3);
                    if (sqlite_datareader[4].GetType() != typeof(DBNull))
                        itm.ModelNo = sqlite_datareader.GetString(4);
                    if (sqlite_datareader[5].GetType() != typeof(DBNull))
                        itm.POSONo = sqlite_datareader.GetString(5);
                    if (sqlite_datareader[6].GetType() != typeof(DBNull))
                        itm.Operative = sqlite_datareader.GetString(6);
                    if (sqlite_datareader[7].GetType() != typeof(DBNull))
                        itm.OperativeLocation = sqlite_datareader.GetString(7);
                    if (sqlite_datareader[8].GetType() != typeof(DBNull))
                        itm.Department = sqlite_datareader.GetString(8);
                    if (sqlite_datareader[9].GetType() != typeof(DBNull))
                        itm.TimeCompleted = sqlite_datareader.GetString(9);
                    if (sqlite_datareader[10].GetType() != typeof(DBNull))
                        itm.ServerState = sqlite_datareader.GetString(10);
                    if (sqlite_datareader[11].GetType() != typeof(DBNull))
                        itm.FactoryReset = sqlite_datareader.GetString(11);
                    if (sqlite_datareader[12].GetType() != typeof(DBNull))
                        itm.Notes = sqlite_datareader.GetString(12);
                    if (sqlite_datareader[13].GetType() != typeof(DBNull))
                        itm.FaultDescription = sqlite_datareader.GetString(13);
                    if (sqlite_datareader[14].GetType() != typeof(DBNull))
                        itm.GDPRRiskDetected = sqlite_datareader.GetString(14);


                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        vm.ServersProcessedCollection.Add(itm);
                    });



                }

                // await PutTaskDelay(400);

                //Override Mouse Cursor
                Mouse.OverrideCursor = null;

                // conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //MessageBox.Show(ex.Message);
            }
        }




        public void ReadMonthlyData(SQLiteConnection conn)
        {
            try
            {

             

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                //Set Initial Dates for Weekly Records

                DateTime date = DateTime.Today;
                vm.DateFrom = new DateTime(date.Year, date.Month, 1);
                vm.DateTo = DateTime.Now;

                //Search based off start of week             
                sqlite_cmd.CommandText = "SELECT * FROM Processed WHERE ProcessedDate BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "'";

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ServersProcessedModel itm = new ServersProcessedModel();
                    if (sqlite_datareader[1].GetType() != typeof(DBNull))
                        itm.DateProcessed = sqlite_datareader.GetDateTime(1);
                    if (sqlite_datareader[2].GetType() != typeof(DBNull))
                        itm.ChassisSerialNo = sqlite_datareader.GetString(2);
                    if (sqlite_datareader[3].GetType() != typeof(DBNull))
                        itm.Manufacturer = sqlite_datareader.GetString(3);
                    if (sqlite_datareader[4].GetType() != typeof(DBNull))
                        itm.ModelNo = sqlite_datareader.GetString(4);
                    if (sqlite_datareader[5].GetType() != typeof(DBNull))
                        itm.POSONo = sqlite_datareader.GetString(5);
                    if (sqlite_datareader[6].GetType() != typeof(DBNull))
                        itm.Operative = sqlite_datareader.GetString(6);
                    if (sqlite_datareader[7].GetType() != typeof(DBNull))
                        itm.OperativeLocation = sqlite_datareader.GetString(7);
                    if (sqlite_datareader[8].GetType() != typeof(DBNull))
                        itm.Department = sqlite_datareader.GetString(8);
                    if (sqlite_datareader[9].GetType() != typeof(DBNull))
                        itm.TimeCompleted = sqlite_datareader.GetString(9);
                    if (sqlite_datareader[10].GetType() != typeof(DBNull))
                        itm.ServerState = sqlite_datareader.GetString(10);
                    if (sqlite_datareader[11].GetType() != typeof(DBNull))
                        itm.FactoryReset = sqlite_datareader.GetString(11);
                    if (sqlite_datareader[12].GetType() != typeof(DBNull))
                        itm.Notes = sqlite_datareader.GetString(12);
                    if (sqlite_datareader[13].GetType() != typeof(DBNull))
                        itm.FaultDescription = sqlite_datareader.GetString(13);
                    if (sqlite_datareader[14].GetType() != typeof(DBNull))
                        itm.GDPRRiskDetected = sqlite_datareader.GetString(14);


                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        vm.ServersProcessedCollection.Add(itm);
                    });



                }

                // await PutTaskDelay(400);

                //Override Mouse Cursor
                //Mouse.OverrideCursor = null;

                // conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //MessageBox.Show(ex.Message);
            }
        }

        public void ReadYearlyData(SQLiteConnection conn)
        {
            try
            {
             

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                //Set Initial Dates for Weekly Records

                DateTime date = DateTime.Today;
                vm.DateFrom = new DateTime(date.Year, 1, 1);
                vm.DateTo = DateTime.Now;

                //Search based off start of week             
                sqlite_cmd.CommandText = "SELECT * FROM Processed WHERE ProcessedDate BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "'";

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ServersProcessedModel itm = new ServersProcessedModel();
                    if (sqlite_datareader[1].GetType() != typeof(DBNull))
                        itm.DateProcessed = sqlite_datareader.GetDateTime(1);
                    if (sqlite_datareader[2].GetType() != typeof(DBNull))
                        itm.ChassisSerialNo = sqlite_datareader.GetString(2);
                    if (sqlite_datareader[3].GetType() != typeof(DBNull))
                        itm.Manufacturer = sqlite_datareader.GetString(3);
                    if (sqlite_datareader[4].GetType() != typeof(DBNull))
                        itm.ModelNo = sqlite_datareader.GetString(4);
                    if (sqlite_datareader[5].GetType() != typeof(DBNull))
                        itm.POSONo = sqlite_datareader.GetString(5);
                    if (sqlite_datareader[6].GetType() != typeof(DBNull))
                        itm.Operative = sqlite_datareader.GetString(6);
                    if (sqlite_datareader[7].GetType() != typeof(DBNull))
                        itm.OperativeLocation = sqlite_datareader.GetString(7);
                    if (sqlite_datareader[8].GetType() != typeof(DBNull))
                        itm.Department = sqlite_datareader.GetString(8);
                    if (sqlite_datareader[9].GetType() != typeof(DBNull))
                        itm.TimeCompleted = sqlite_datareader.GetString(9);
                    if (sqlite_datareader[10].GetType() != typeof(DBNull))
                        itm.ServerState = sqlite_datareader.GetString(10);
                    if (sqlite_datareader[11].GetType() != typeof(DBNull))
                        itm.FactoryReset = sqlite_datareader.GetString(11);
                    if (sqlite_datareader[12].GetType() != typeof(DBNull))
                        itm.Notes = sqlite_datareader.GetString(12);
                    if (sqlite_datareader[13].GetType() != typeof(DBNull))
                        itm.FaultDescription = sqlite_datareader.GetString(13);
                    if (sqlite_datareader[14].GetType() != typeof(DBNull))
                        itm.GDPRRiskDetected = sqlite_datareader.GetString(14);


                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        vm.ServersProcessedCollection.Add(itm);
                    });

                   

                }

                // await PutTaskDelay(400);



                // conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //MessageBox.Show(ex.Message);
            }
        }

        public void ExportCSV()
        {
            List<ServersProcessedModel> serverlst = new List<ServersProcessedModel>();

            if (vm.ServersProcessedCollection != null)
            {
                //MessageBox.Show("IMPORT LV");

                if (vm.ServersProcessedCollection.Count > 0)
                {
                    foreach (ServersProcessedModel ps in vm.ServersProcessedCollection)
                    {
                        //Add to local list
                        serverlst.Add(ps);
          

                    }

                }


                //Write to CSV
                StaticFunctions.ExportServersProcessedeCSV(serverlst, "");

            }


        }

        //SQL FUNCTIONS

        //Create Connection to DB
        static SQLiteConnection CreateConnection(string pathtodb)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            //sqlite_conn = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            //sqlite_conn = new SQLiteConnection("Data Source=" + pathtodb + "; Version = 3; New = True; Compress = True; ");
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sqlite_conn;
        }





   


        public string AddToCollection(string itemtofilter)
        {
            vm.ServersProcessedCollection = new System.Collections.ObjectModel.ObservableCollection<ServersProcessedModel>();

            string TheYear = DateTime.Now.Year.ToString();

            if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv"))
            {
                //Will Create a new file for each year
                string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv";
                //string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv";
                //Uses a VB class to process the file instead of streamreader
                using (TextFieldParser parser = new TextFieldParser(newFileName))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        string currentLine = parser.ReadLine();

                        //MessageBox.Show(lines);
                        //string[] fields = parser.ReadFields();
                        //foreach (string field in fields)
                        //{
                        //    MessageBox.Show(field);
                        //check if the item for department should be added Goods In, Goods Out Technical, Test Department
                        
                        if (currentLine.Contains(itemtofilter) || itemtofilter == "All")
                        {
                            ////Roll Back 2 months
                            //DateTime dTCurrent = DateTime.Now.AddDays(-60); /// Convert.ToDateTime("01/05/2021");// 
                            //DateTime inputDate = FromCsv(currentLine).DateProcessed; // DateTime.ParseExact(input, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                            //int result = DateTime.Compare(dTCurrent, inputDate);

                            ////Check if Date of 
                            //if (result < 0)
                            //{
                            //    vm.ServersProcessedCollection.Add(FromCsv(currentLine));
                            //}
                            vm.ServersProcessedCollection.Add(FromCsv(currentLine));

                        }
                        else 
                        {
                           
                        }

                        //    //TODO: Process field
                        //}
                    }
                }

              
                // set the default sort
                //CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lv.ItemsSource);
                //view.SortDescriptions.Add(new SortDescription("DateProcessed", ListSortDirection.Descending));
                //view.SortDescriptions.Add(new SortDescription("Description", ListSortDirection.Ascending));
            }
            return "";
        }

        public ServersProcessedModel FromCsv(string csvLine)
        {

            if(csvLine != null)
            {
                string[] values = csvLine.Split(',');
                ServersProcessedModel sproc = new ServersProcessedModel();
                if(values[0] != string.Empty)
                sproc.DateProcessed = Convert.ToDateTime(values[0]);
                if (values[1] != string.Empty)
                 sproc.ChassisSerialNo = Convert.ToString(values[1]);
                if (values[2] != string.Empty)
                    sproc.Operative = Convert.ToString(values[2]);
                if (values[3] != string.Empty)
                    sproc.Department = Convert.ToString(values[3]);
                if (values[4] != string.Empty)
                    sproc.TimeCompleted = Convert.ToString(values[4]);
                //if (values[5] != string.Empty)
                    sproc.ServerState = Convert.ToString(values[5]);
               //// if (values[6] != string.Empty)
                    sproc.Description = Convert.ToString(values[6]);
                //if (values[7] != string.Empty)
                    sproc.POSONo = Convert.ToString(values[7]);
                return sproc;
            }
          
            return null;
        }


        ///Weekly Records
        public void AddToCollectionWeeklyRecords(string itemtofilter)
        {
            vm.WeeklyServerRecordCollection = new System.Collections.ObjectModel.ObservableCollection<WeeklyServerRecordsModel>();

            ObservableCollection<WeeklyServerRecordsModel> RecordsCollection = new ObservableCollection<WeeklyServerRecordsModel>();

            string TheYear = DateTime.Now.Year.ToString();

            if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\Weekly Server Records " + TheYear + ".csv"))
            {
                //Will Create a new file for each year
                string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Weekly Server Records " + TheYear + ".csv";
                //string newFileName = @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed_" + TheYear + ".csv";
                //Uses a VB class to process the file instead of streamreader
                using (TextFieldParser parser = new TextFieldParser(newFileName))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        string currentLine = parser.ReadLine();

                        //MessageBox.Show(lines);
                        //string[] fields = parser.ReadFields();
                        //foreach (string field in fields)
                        //{
                        //    MessageBox.Show(field);
                        //check if the item for department should be added Goods In, Goods Out Technical, Test Department
                        
                        if (currentLine.Contains(itemtofilter) || itemtofilter == "All")
                        {
                          
                            RecordsCollection.Add(FromWeeklyRecordCsv(currentLine));
                        }
                        else 
                        {
                           
                        }

                        //    //TODO: Process field
                        //}
                    }
                }

                // Update main binding property
                vm.WeeklyServerRecordCollection = RecordsCollection;


                // set the default sort
                //CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lv.ItemsSource);
                //view.SortDescriptions.Add(new SortDescription("DateProcessed", ListSortDirection.Descending));
                //view.SortDescriptions.Add(new SortDescription("Description", ListSortDirection.Ascending));
            }
          
        }


        public WeeklyServerRecordsModel FromWeeklyRecordCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            WeeklyServerRecordsModel sproc = new WeeklyServerRecordsModel();
            sproc.Day = Convert.ToDateTime(values[0]);
            sproc.SONumber = Convert.ToString(values[1]);
            sproc.ServerModel = Convert.ToString(values[2]);
            sproc.Quantity = Convert.ToInt32(values[3]);
            sproc.Technician = Convert.ToString(values[4]);
            sproc.SalesRep = Convert.ToString(values[5]);
            sproc.JobType = Convert.ToString(values[6]);
            sproc.Issues = Convert.ToString(values[7]);
            sproc.ActionTaken = Convert.ToString(values[8]);
            sproc.Status = Convert.ToString(values[9]);
            return sproc;
        }




    }
}

