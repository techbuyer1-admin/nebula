﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class CancelScriptsCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public CancelScriptsCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if(parameter != null)
            {
                var values = (object[])parameter;
                var IP = (string)values[0];
                var WhichServer = (string)values[1];


                //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
                if (IP != null)
                {
                    //check if valid ip entered
                    //System.Net.IPAddress ipAddress = null;
                    //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                    if (IP != string.Empty)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
           
        }

        public void Execute(object parameter)
        {

            var values = (object[])parameter;
            var IP = (string)values[0];
            var WhichServer = (string)values[1];


            switch (WhichServer)
            {
                case "S1":
                    vm.CancelScriptS1 = true;
                    vm.ProgressMessage1 = "Cancelling Scripts, Please Wait.....";
                    vm.TabColorS1 = Brushes.MediumPurple;
                    break;
             
            }



        }
    }
}

 
