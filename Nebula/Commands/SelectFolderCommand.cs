﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Forms;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class SelectFolderCommand : ICommand
    {

        //declare viewmodel
        GoodsInOutViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public SelectFolderCommand(GoodsInOutViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //if (lv != null)
            //{
            //    if (lv.SelectedItems.Count > 0)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    return true;
            //}
            return true;
        }

        public void Execute(object parameter)
        {

            var values = (object)parameter;


            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                dialog.RootFolder = Environment.SpecialFolder.MyComputer;

                if(dialog.ShowDialog() == DialogResult.OK)
                {
                    // MessageBox.Show(dialog.SelectedPath);
                    int lastslash = dialog.SelectedPath.LastIndexOf(@"\");
                    vm.SalesOrderNumberPath = dialog.SelectedPath;
                    vm.SalesOrderNumber = dialog.SelectedPath.Substring(lastslash + 1);
                   // MessageBox.Show(vm.SalesOrderNumber);
                }

              

               
            }

        }
    }
}