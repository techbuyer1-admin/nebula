﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;
using Nebula.Helpers;
using Nebula.LenovoServers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class RunLenovoScriptsCommandS1 : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;
        string ServerTabHeader = "";

        //ServerTabViewModel vm2 = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public RunLenovoScriptsCommandS1(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
            //vm2 = ServerHost;
        }

        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            var values = (object[])parameter;
            var IP = (string)values[0];
            var WhichServer = (string)values[1];


            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            if (IP != null)
            {
                //check if valid ip entered
                //System.Net.IPAddress ipAddress = null;
                //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                if (IP != string.Empty)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public async void Execute(object parameter)
        {

            var values = (object[])parameter;
            var ScriptChoice = (string)values[0];
            var ServerGen = (string)values[1];

            //A: Setup and stuff you don't want timed
            var timer = new Stopwatch();
            timer.Start();
            TimeSpan timeTaken;
            string timetaken;

            TabItem ServerTab = null;

            string SyncLog = "";

            //try
            //{

         
            SyncLog += "[Start of Nebula Sync Log]\r\r" + DateTime.Now.ToString() + "\r\r";


            switch (vm.WhichTab)
            {
                case "1":
                   // MessageBox.Show("Server On Tab 1");
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS1") as TabItem;
                    if(ServerTab != null)
                    ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server1";
                    break;
                case "2":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS2") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server2";
                    // MessageBox.Show("Server On Tab 2");
                    break;
                case "3":
                    // MessageBox.Show("Server On Tab 1");
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS3") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server3";
                    break;
                case "4":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS4") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server4";
                    // MessageBox.Show("Server On Tab 2");
                    break;
                case "5":
                    // MessageBox.Show("Server On Tab 1");
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS5") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server5";
                    break;
                case "6":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS6") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server6";
                    // MessageBox.Show("Server On Tab 2");
                    break;
                case "7":
                    // MessageBox.Show("Server On Tab 1");
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS7") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server7";
                    break;
                case "8":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS8") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server8";
                    // MessageBox.Show("Server On Tab 2");
                    break;
                case "9":
                    // MessageBox.Show("Server On Tab 1");
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS9") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server9";
                    break;
                case "10":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS10") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server10";
                    // MessageBox.Show("Server On Tab 2");
                    break;
                case "11":
                    // MessageBox.Show("Server On Tab 1");
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS11") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server11";
                    break;
                case "12":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS12") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server12";
                    // MessageBox.Show("Server On Tab 2");
                    break;

                case "13":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS13") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server13";
                    // MessageBox.Show("Server On Tab 2");
                    break;

                case "14":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS14") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server14";
                    // MessageBox.Show("Server On Tab 2");
                    break;

                case "15":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS15") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server15";
                    // MessageBox.Show("Server On Tab 2");
                    break;

                case "16":
                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS16") as TabItem;
                    if (ServerTab != null)
                        ServerTab.Foreground = Brushes.Orange;
                    ServerTabHeader = "Server16";
                    // MessageBox.Show("Server On Tab 2");
                    break;
                default:

                    ServerTab.Foreground = Brushes.MediumPurple;
                    break;

            }

                   

            //vm.TabColorS1 = Brushes.Orange;


            //MessageBox.Show("Lenovo");

            if (vm.Server1IPv4 != null)
            {
                if (vm.Server1IPv4 != string.Empty)
                {
                    string address = vm.Server1IPv4;
                    vm.WarningMessage1 = "";
                    vm.ProgressMessage1 = "";
                    //send a ping to the server to check it's available
                    vm.ProgressMessage1 = "Checking Server IP, Please Wait...";
                    vm.ProgressPercentage1 = 0;
                    vm.ProgressIsActive1 = true;
                    vm.ProgressVisibility1 = Visibility.Visible;
                    sendAsyncPingPacket(address);

                    await PutTaskDelay(3000);

                    //if it is run the code
                    if (vm.IsServerThere1 == "Yes")
                    {


                        //Set script running property to true
                        vm.IsScriptRunningS1 = true;


               
                        //CANCEL SCRIPT
                        if (vm.CancelScriptS1 == true)
                        {
                            vm.ProgressPercentage1 = 0;
                            vm.ProgressIsActive1 = false;
                            vm.ProgressVisibility1 = Visibility.Hidden;
                            vm.ProgressMessage1 = "Scripts Cancelled!";
                            vm.WarningMessage1 = "";
                            vm.CancelScriptS1 = false;
                            return;
                        }
                        //END CANCEL SCRIPT


                        //Read XML Returned Data From Server
                        JsonParser LenovoData = new JsonParser(vm);
                        ProcessPiper pp = new ProcessPiper(vm);

                        //RESET THE COMMAND BOX
                        vm.ILORestOutput1 = "";
                        //IMPORTANT Otherwise duplicates
                        //vm.LenovoInfoS1.Clear();

                        if (vm.LenovoServerS1 != null)
                        {

                            //MessageBox.Show("Yep");
                            vm.LenovoServerS1.FansInventory = String.Empty;
                            vm.LenovoServerS1.CPUInventory = String.Empty;
                            vm.LenovoServerS1.PowerInventory = String.Empty;
                            vm.LenovoServerS1.StorageInventory = String.Empty;
                            vm.LenovoServerS1.MemoryInventory = String.Empty;
                            vm.LenovoServerS1.NetworkInventory = String.Empty;
                           
                            vm.LenovoServerS1.LenovoCPUCollection.Clear();
                            vm.LenovoServerS1.LenovoMemoryCollection.Clear();
                            vm.LenovoServerS1.LenovoNetworkCollection.Clear();
                            vm.LenovoServerS1.LenovoPowerCollection.Clear();
                            vm.LenovoServerS1.LenovoStorageCollection.Clear();
                            vm.LenovoServerS1.LenovoStorageSoftwareCollection.Clear();
                            vm.LenovoServerS1.LenovoFanCollection.Clear();
                        }




                        //TRY A FIXED DOCUMENT?

                        switch (ScriptChoice)
                        {


                            case "ServerInfo":


                                //MessageBox.Show(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                string SIDateClicked = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                                vm.ProgressMessage1 = "Please wait...";
                                vm.ProgressIsActive1 = true;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressVisibility1 = Visibility.Visible;

                            

                                vm.ProgressPercentage1 = 10;


                                vm.ProgressMessage1 = "Server Inventory in progress, Please wait...";

                                //Check if Directory Already exists

                                if (Directory.Exists(vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\" + vm.Server1IPv4 + @"Inventory"))
                                {
                                    //Delete Folder and files
                                    Directory.Delete(vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\" + vm.Server1IPv4 + @"Inventory",true);
                                }






                                 await PutTaskDelay(4000);

                                vm.ProgressPercentage1 = 40;

                                vm.ProgressPercentage1 = 70;
                                if (vm.CancelScriptS1 == true)
                                {
                                    vm.ProgressPercentage1 = 0;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    vm.ProgressMessage1 = "Scripts Cancelled!";
                                    vm.WarningMessage1 = "";
                                    vm.CancelScriptS1 = false;
                                    return;
                                }



                                vm.ProgressPercentage1 = 60;
                                //acquire the server inventory 
                                if (StaticFunctions.Department.Contains("GoodsOutTech"))
                                {
                                    await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"inventory getinfor --device system_overview,hardware_inventory,firmware_vpd,persistent_memory,pci_adapters,storage_devices -N --output " + vm.Server1IPv4 + @"Inventory --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool"));
                                }
                                else
                                {       //Original
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"inventory getinfor --device system_overview,hardware_inventory,firmware_vpd,bmc_environmental,pci_adapters -N --output " + vm.Server1IPv4 + @"Inventory --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool"));


                                        await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"inventory getinfor --device system_overview,hardware_inventory,firmware_vpd,bmc_environmental -N --output " + vm.Server1IPv4 + @"Inventory --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool"));

                                        //wORKS WITH LOCAL PC FOR INVENTORY
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"inventory getinfor USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool"));
                                        //await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"inventory getinfor --device system_overview,hardware_inventory,pci_adapters -N --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool"));
                                    }

                                    //await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"inventory getinfor --device system_overview,hardware_inventory,firmware_vpd,bmc_environmental,storage_devices -N --output " + vm.Server1IPv4 + @"Inventory --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\\HPTOOLS\Lenovo\Lenovo One CLI Tool"));




                                    //await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"inventory getinfor --device system_overview,hardware_inventory,firmware_vpd,persistent_memory,pci_adapters,storage_devices -N --output " + vm.Server1IPv4 + @"Inventory --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\\HPTOOLS\Lenovo\Lenovo One CLI Tool"));

                                    //full inventory 19 mins too much
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"inventory getinfor -N --output " + vm.Server1IPv4 + @"Inventory --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\\HPTOOLS\Lenovo\Lenovo One CLI Tool"));

                                    vm.ProgressPercentage1 = 70;
                                if (vm.CancelScriptS1 == true)
                                {
                                    vm.ProgressPercentage1 = 0;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    vm.ProgressMessage1 = "Scripts Cancelled!";
                                    vm.WarningMessage1 = "";
                                    vm.CancelScriptS1 = false;
                                    return;
                                }

                                                  


                                vm.ProgressPercentage1 = 80;
                                //SET HOLDING OBJECT TO NULL
                                vm.LenovoServerS1 = null;
                                //CREATE A FRESH ITEM
                                vm.LenovoServerS1 = new LenovoServerInfo();

                            


                                //Stop Timer
                                timer.Stop();
                                timeTaken = timer.Elapsed;
                                timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");


                                //CHECK FOR XML INVENTORY FILE
                                string XMLDataFile = "";
                                //Serach for XML in Folder
                                if (Directory.Exists(vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\" + vm.Server1IPv4 + @"Inventory"))
                                {


                                    //Check all the files in Directory Path
                                   string[] innerFiles = Directory.GetFiles(vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\" + vm.Server1IPv4 + @"Inventory");

                                    //Loop through and find the XML File
                                    foreach(var fileName in innerFiles)
                                    {
                                        if (fileName.Contains(".xml"))
                                        {
                                            //Assign to variable
                                            XMLDataFile = fileName;
                                        }
                                    }

                                }


                                if (vm.CancelScriptS1 == true)
                                {
                                    vm.ProgressPercentage1 = 0;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    vm.ProgressMessage1 = "Scripts Cancelled!";
                                    vm.WarningMessage1 = "";
                                    vm.CancelScriptS1 = false;
                                    return;
                                }

                              

                                //ADD SSH IMM2 FOR HEALTH SUMMARY
                                string HealthData = await System.Threading.Tasks.Task.Run(() => LenovoData.SendSSHCommand(vm.Server1IPv4, "USERID", "PASSW0RD", "syshealth summary"));

                                string VPDData = await System.Threading.Tasks.Task.Run(() => LenovoData.SendSSHCommand(vm.Server1IPv4, "USERID", "PASSW0RD", "vpd comp uefi"));

                                //MessageBox.Show(VPDData);

                                //********SERVER INFO CAPTURE*********
                                //Assign Data to Server Data Source
                                vm.LenovoServerS1 = LenovoData.GetLenovoServerData(vm.Server1IPv4,"1", XMLDataFile, HealthData);


                                if (vm.CancelScriptS1 == true)
                                {
                                    vm.ProgressPercentage1 = 0;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    vm.ProgressMessage1 = "Scripts Cancelled!";
                                    vm.WarningMessage1 = "";
                                    vm.CancelScriptS1 = false;
                                    return;
                                }

                                //Set Tab Header

                                ServerTab.Header = ServerTab.Header.ToString().Replace("(" + vm.LenovoServerS1.SystemSerialNumber + ")", "");
                                ServerTab.Header = ServerTab.Header + " (" + vm.LenovoServerS1.SystemSerialNumber + ")";

                                //vm.Server1TabHeader = vm.Server1TabHeader.Replace("(" + vm.LenovoServerS1.SystemSerialNumber + ")", "");
                                //vm.Server1TabHeader = vm.Server1TabHeader + " (" + vm.LenovoServerS1.SystemSerialNumber + ")";



                                //CHECK BIOS & IMM VERSIONS
                                if (vm.LenovoServerS1 != null)
                                {

                                   //     string[] immversion = vm.LenovoServerS1.IMMCurrentVersion.Split('-');
                                   //     string[] biosversion = vm.LenovoServerS1.BiosCurrentVersion.Split('-');


                                   //vm.LenovoFirmwareVersionCheck("1.20", "1", biosversion[1], "BIOS Version too low, please update via the servers webpage.");

                                   // vm.LenovoFirmwareVersionCheck("1.80", "1", immversion[1], "IMM Version too low, please update via the servers webpage.");
                                }




                                vm.ProgressPercentage1 = 0;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.ProgressMessage1 = "Server Inventory complete! " + timetaken;



                                break;
                            case "WipeScripts":

                                if (vm.LenovoServerS1 != null)//vm.Gen9ServerInfo1 != null ||
                                {


                                }// end check if null check
                                else
                                {
                                    vm.ProgressMessage1 = "Please retrieve server information first. Click the Get Server Info Button.";
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    vm.MessageVisibility1 = Visibility.Visible;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressPercentage1 = 0;

                                }

                                ////for test certificate
                                //vm.UpdatesRun += @" [BIOS] ";
                                ////for test certificate
                                //vm.UpdatesRun += @" [IMM] ";
                                ////for test certificate
                                //vm.UpdatesRun += @" [IDRAC] ";

                                break;
                      
                            case "FinalScripts":

                                vm.ProgressMessage1 = "Please wait...";
                                vm.ProgressIsActive1 = true;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressVisibility1 = Visibility.Visible;


                                vm.ProgressPercentage1 = 30;
                                vm.ProgressMessage1 = "Powering Server Off";

                                //Power Off System
                                await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"misc ospower turnoff -N --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\\HPTOOLS\Lenovo\Lenovo One CLI Tool"));

                                await PutTaskDelay(10000);

                                // await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"inventory getinfor --device system_overview,hardware_inventory,firmware_vpd,storage_devices -N --output " + vm.Server1IPv4 + @"Inventory --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\\HPTOOLS\Lenovo\Lenovo One CLI Tool"));
                                vm.ProgressPercentage1 = 50;
                                vm.ProgressMessage1 = "Clearing Server Logs";

                                //Clear Logs
                                await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"misc logmgr clearall -N --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\\HPTOOLS\Lenovo\Lenovo One CLI Tool"));

                                await PutTaskDelay(10000);

                                //FACTORY RESET VIA IMM COMMAND misc logmgr clearall
                                vm.ProgressPercentage1 = 80;
                                vm.ProgressMessage1 = "Server Reset to Defaults.";
                                //load default settings
                                await System.Threading.Tasks.Task.Run(() => pp.StartLenovo1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\Lenovo One CLI Tool\OneCli.exe"), @"", @"config loaddefault -N --bmc USERID:" + vm.OverridePasswordS1 + "@" + vm.Server1IPv4, @"" + vm.myDocs + @"\\HPTOOLS\Lenovo\Lenovo One CLI Tool"));


                                // vm.ILORestOutput1 += await System.Threading.Tasks.Task.Run(() => LenovoData.SendSSHCommand(vm.Server1IPv4, "USERID", "PASSW0RD", "syshealth summary"));

                                //await PutTaskDelay(120000);

                                //vm.ILORestOutput1 += await System.Threading.Tasks.Task.Run(() => LenovoData.SendSSHCommand(vm.Server1IPv4, "USERID", "PASSW0RD", "yes"));

                                //Stop Timer
                                timer.Stop();
                                timeTaken = timer.Elapsed;
                                timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\");
                                //timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.MessageVisibility1 = Visibility.Visible;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressMessage1 = "Script Completed! Server Reset to Defaults. " + timeTaken;
                                //Set Has System Erase Been Run Flag
                                vm.WasFactoryReset = "Yes";

                                break;

                            //case "ResetSlide":
                            //    //MessageBox.Show("ResetSlide");
                            //    //tab header set back to default value
                            //    ServerTab.Header = ServerTabHeader;
                            //    ServerTab.Foreground = Brushes.MediumPurple;
                            //    vm.Server1IPv4 = "";

                            //    break;



                        }


                    }//end if to check server ip is not empty
                    else
                    {
                        //reset progress ring to hidden
                        vm.ProgressVisibility1 = Visibility.Hidden;
                        vm.MessageVisibility1 = Visibility.Visible;
                        vm.ProgressIsActive1 = false;
                        vm.ProgressPercentage1 = 0;
                        vm.WarningMessage1 = "Cannot contact server with Ip: " + vm.Server1IPv4 + ", Please check the Server has power.";
                        vm.IsServerThere1 = "No";
                    }

                }// check if server there
                else
                {
                    //vm.ProgressMessage = "Cannot contact server with Ip: " + vm.ServerIPv4 + ", Exited Function";
                    vm.ProgressMessage1 = "Please Enter A Server IP in the top box!";
                }

            }//end if to check vm is null

            //Clear data files
            // ClearXMLJSONFiles();


            //WRITE LOG
            if (Directory.Exists(@"" + vm.myDocs + @"\Nebula Logs"))
            {


                //Write out log
                if (vm.LenovoServerS1 != null)
                {
                    if (vm.LenovoServerS1.SystemSerialNumber != null)
                        File.WriteAllText(@"" + vm.myDocs + @"\Nebula Logs\Nebula_Server_Log_" + vm.LenovoServerS1.SystemSerialNumber + " " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".txt", SyncLog);
                }

            }


            //Set script running property to false
            vm.IsScriptRunningS1 = false;

                //set colours
                if (string.IsNullOrEmpty(vm.WarningMessage1))
                {
                    if (ServerTab != null)
                    {
                        if (string.IsNullOrEmpty(vm.Server1IPv4))
                        {
                            ServerTab.Foreground = Brushes.MediumPurple;
                        }
                        else
                        {
                            ServerTab.Foreground = Brushes.Green;
                        }

                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;




                        if (!string.IsNullOrEmpty(vm.WarningMessage1))
                        {
                            ServerTab.Foreground = Brushes.Red;
                        }

                    }
                }

            //}
            //catch (Exception ex)
            //{

            //    vm.WarningMessage1 = "Error: " + ex.Message;
            //    vm.ProgressPercentage1 = 0;
            //    vm.ProgressIsActive1 = false;
            //    vm.ProgressVisibility1 = Visibility.Hidden;
            //    vm.ProgressMessage1 = "Scripts Cancelled!";
            //    vm.CancelScriptS1 = false;
            //    //set colours
            //    if (string.IsNullOrEmpty(vm.WarningMessage1))
            //    {
            //        if (ServerTab != null)
            //        {
            //            if (string.IsNullOrEmpty(vm.Server1IPv4))
            //            {
            //                ServerTab.Foreground = Brushes.MediumPurple;
            //            }
            //            else
            //            {
            //                ServerTab.Foreground = Brushes.Green;
            //            }

            //        }
            //    }
            //    else
            //    {
            //        if (ServerTab != null)
            //        {
            //            ServerTab.Foreground = Brushes.MediumPurple;




            //            if (!string.IsNullOrEmpty(vm.WarningMessage1))
            //            {
            //                ServerTab.Foreground = Brushes.Red;
            //            }

            //        }
            //    }
            //    return;
            //}


        }//end execute






        public async void sendAsyncPingPacket(string hostToPing)
        {
            try
            {
                // int timeout = 5000;
                // AutoResetEvent waiter = new AutoResetEvent(false);
                Ping pingPacket = new Ping();
                //ping completion event reaised
                pingPacket.PingCompleted += new PingCompletedEventHandler(PingCompletedCallback);
                string data = "Ping test check";
                byte[] byteBuffer = Encoding.ASCII.GetBytes(data);
                PingOptions pingOptions = new PingOptions(64, true);

                await pingPacket.SendPingAsync(hostToPing, 3000);
                //pingPacket.Send(hostToPing, 3000);

            }
            catch (PingException pe)
            {
                string exception = pe.Message;
                //IPResults.Add("INVALID IP ADDRESS FOUND");
            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                //IPResults.Add("Exceptin " + ex.Message);
            }

        }


        private void PingCompletedCallback(object sender, PingCompletedEventArgs e)
        {
            try
            {
                if (e.Cancelled)
                {

                }

                // If an error occurred, display the exception to the user.
                if (e.Error != null)
                {

                }

                PingReply reply = e.Reply;

                if (reply == null)
                {
                    vm.IsServerThere1 = "No";
                    return;
                }
                else if (reply.Status == IPStatus.Success)
                {
                    vm.IsServerThere1 = "Yes";
                    return;

                }
            }
            catch (PingException pe)
            {
                Console.WriteLine(pe.Message);
                //IPResults.Add("INVALID IP ADDRESS");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                // MessageBox.Show("Came Here");
                //IPResults.Add("Exception " + ex.Message);
            }
        }

        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }


    }
}
