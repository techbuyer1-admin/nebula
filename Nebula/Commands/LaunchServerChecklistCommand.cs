﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;
using MahApps.Metro.Controls;


namespace Nebula.Commands
{
    public class LaunchServerChecklistCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public LaunchServerChecklistCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //if (lv != null)
            //{
            //    if (lv.SelectedItems.Count > 0)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    return true;
            //}
            return true;
        }

        public void Execute(object parameter)
        {

            var values = (object)parameter;

            FlipView fv = Helper.GetDescendantFromName(Application.Current.MainWindow, "FlipView") as FlipView;
            FlipView fv1 = Helper.GetDescendantFromName(Application.Current.MainWindow, "FlipView1") as FlipView;
            FlipView fv2 = Helper.GetDescendantFromName(Application.Current.MainWindow, "FlipView2") as FlipView;
            FlipView fv3 = Helper.GetDescendantFromName(Application.Current.MainWindow, "FlipView3") as FlipView;
            FlipView fv4 = Helper.GetDescendantFromName(Application.Current.MainWindow, "FlipView4") as FlipView;
            FlipView fv5 = Helper.GetDescendantFromName(Application.Current.MainWindow, "FlipView5") as FlipView;
            FlipView fv6 = Helper.GetDescendantFromName(Application.Current.MainWindow, "FlipView6") as FlipView;
            FlipView fv7 = Helper.GetDescendantFromName(Application.Current.MainWindow, "FlipView7") as FlipView;
            FlipView fv8 = Helper.GetDescendantFromName(Application.Current.MainWindow, "FlipView8") as FlipView;
            FlipView fv9 = Helper.GetDescendantFromName(Application.Current.MainWindow, "FlipView9") as FlipView;
            FlipView fv10 = Helper.GetDescendantFromName(Application.Current.MainWindow, "FlipView10") as FlipView;
            TextBox ChassisTXT = Helper.GetDescendantFromName(Application.Current.MainWindow, "ChassisNoTXTB") as TextBox;


            // set a static variable that  
            if (vm != null)
            {

                switch (values)
                {
                    case "HPGen8":

                        //if(vm.Gen8ServerInfo != null)
                        //if(vm.Gen8ServerInfo.ChassisSerial != "")
                        //{
                        //    StaticFunctions.ChassisNoPassthrough = vm.Gen8ServerInfo.ChassisSerial;
                        //    ChassisTXT.Text = vm.Gen8ServerInfo.ChassisSerial;
                        //    fv.SelectedIndex = 9;
                        //}

                        if (vm.GenGenericServerInfo1 != null)
                            if (vm.GenGenericServerInfo1.ChassisSerial != "")
                            {
                                StaticFunctions.ChassisNoPassthrough = vm.GenGenericServerInfo1.ChassisSerial;
                            }



                        break;
                    case "HPGen9":
                        //if (vm.Gen9ServerInfo != null)
                        //    if (vm.Gen9ServerInfo.ChassisSerial != "")
                        //    {
                        //        StaticFunctions.ChassisNoPassthrough = vm.Gen9ServerInfo.ChassisSerial;
                        //    }

                        if (vm.GenGenericServerInfo1 != null)
                            if (vm.GenGenericServerInfo1.ChassisSerial != "")
                            {
                                StaticFunctions.ChassisNoPassthrough = vm.GenGenericServerInfo1.ChassisSerial;
                            }
                         

                        break;
                    case "HPGen10":
                        //if (vm.Gen10ServerInfo != null)
                        //    if (vm.Gen10ServerInfo.ChassisSerial != "")
                        //    {
                        //        StaticFunctions.ChassisNoPassthrough = vm.Gen10ServerInfo.ChassisSerial;
                        //    }

                        if (vm.GenGenericServerInfo1 != null)
                            if (vm.GenGenericServerInfo1.ChassisSerial != "")
                            {
                                StaticFunctions.ChassisNoPassthrough = vm.GenGenericServerInfo1.ChassisSerial;
                            }

                     

                        break;
                }
           
                //hidden as adding direct into slider
               // vm.LoadContentCommand.Execute("GoodsInChecklistView");


               

            }
            else
            {
                MessageBox.Show("No Chassis Serial Number Found. Please refresh server data and try again");
            }
         
        
        }
    }
}

