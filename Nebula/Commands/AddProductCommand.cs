﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class AddProductCommand :ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;


        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public AddProductCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            //Get array of objects
            var values = (object[])parameter;
            //Split array into specific types
            var tbBrand = (TextBox)values[0];
            var Productcmbo = (ComboBox)values[1];
            //Get a textbox for focusing
            //TextBox tbBrand = Helper.GetDescendantFromName(Application.Current.MainWindow, "BrandTXT") as TextBox;
            tbBrand.Focus();
            //MessageBox.Show(vm.SequenceTypeForm);
          if(vm.SequenceTypeForm != string.Empty)
            {

                //Create Model Of Details and Assign Value of Properties
                AutomatedCliModel autocli = new AutomatedCliModel();
                autocli.Brand = vm.ProductBrandForm;
                autocli.Category = vm.ProductCategoryForm;
                autocli.ModelNo = vm.ProductModelNoForm;
                autocli.SequenceType = vm.SequenceTypeForm;
                autocli.Steps = new List<StepItem>();

                foreach (var step in vm.StepSequenceCollectionForm)
                {
                    autocli.Steps.Add(step);
                }


                //if(vm.ImageLocation != null)
                //adm.Image = BitmapHelper.convertBitmapImageToBytestream(vm.ImageLocation);

                //Create XML File of Settings
                XMLTools.WriteToXmlFile(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML Product Wipe Profiles\" + autocli.Brand + " " + autocli.ModelNo + " " + autocli.Category + " " + autocli.SequenceType + ".xml", autocli, false);
                //XMLTools.WriteToXmlFile(@"" + vm.dsktop + @"\Invoice App\XML Product Wipe Profiles\" + autocli.Brand + " " + autocli.ModelNo  + " " + autocli.Category + ".xml", autocli, false);
                //  }

               // ComboBox Productcmbo = Helper.GetDescendantFromName(Application.Current.MainWindow, "BasedONCB") as ComboBox;
                Productcmbo.SelectedIndex = -1;

                
                //Clear the controls on the page
                vm.ClearAddProduct();
                //give focus back to the Step description box
                tbBrand.Focus();

                //reload items
                vm.ReadProductFileCommand.Execute("");
            }
            else
            {
                MessageBox.Show("Please Select A Type Of Profile!");
            }


            //MessageBox.Show("Menu Control Clicked");
        }
    }
}
