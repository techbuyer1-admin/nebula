﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
   public class LaunchWebCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public LaunchWebCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter != null)
            {
                //Get array of objects
                var values = (object[])parameter;
                //Split array into specific types
                var WebLink = values[0];
                var ServerBrand = values[1];

                //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
                if (values[0] != null)
                {
                    //check if valid ip entered
                    //System.Net.IPAddress ipAddress = null;
                    //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                    if ((string)values[0] != string.Empty)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (vm.UltimaURL != string.Empty)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }

        }

        public void Execute(object parameter)
        {
          
            //Get array of objects
            var values = (object[])parameter;
            //Split array into specific types
            var WebLink = values[0];
            var ServerBrand = values[1];



            if (WebLink != null)
                {
                //check if valid ip entered
            
                if (ServerBrand.ToString() == "HP")
                    {
                   // MessageBox.Show(values.ToString());
                 //System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };


                    //Open In IE
                    System.Diagnostics.Process.Start(@"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe", @"https://" + WebLink.ToString());


                    }
                    else
                    {
                    //Just open in Chrome
                    System.Diagnostics.Process.Start(@"https://" + WebLink.ToString());
                    }
                }



        }
    }
}

