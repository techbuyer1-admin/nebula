﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class ReadProductFileCommand : ICommand
    {
        //declare viewmodel
        MainViewModel vm = null;



        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public ReadProductFileCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //use helper to get control
            ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

            //if (lv != null)
            //{


            //    if (lv.SelectedItems.Count > 0)
            //    {

            //        //MessageBox.Show(string.IsNullOrEmpty(vm.StepDescription).ToString() + "   " + string.IsNullOrEmpty(vm.StepCliCommand).ToString());
            //        //return false;  
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //        //return true;
            //    }


            //}
            //else
            //{
            //    return false;
            //}

            return true;

        }

        

        public void Execute(object parameter)
        {
        //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

       // https://social.technet.microsoft.com/wiki/contents/articles/18199.event-handling-in-an-mvvm-wpf-application.aspx


            var values = (object)parameter;


            try
            {

            
            //MessageBox.Show(values.ToString());
            ////use helper to get control
            // ComboBox cb1 = Helper.GetDescendantFromName(Application.Current.MainWindow, "ProductListCB") as ComboBox;

            //Clear the form controls just in case
            vm.ProductBrandForm = "";
            vm.ProductCategoryForm = "";
            vm.ProductModelNoForm = "";
            vm.SequenceTypeForm = "";
            vm.StepSequenceCollectionForm.Clear();
            //Call the static function to return any file with an xml extension
            //vm.ProductList  = StaticFunctions.GetFileNamesOnly(@"" + vm.dsktop + @"\Invoice App\XML Product Wipe Profiles", "*.xml");
            vm.ProductList = StaticFunctions.GetFileNamesOnlyBasedOnValue(@"" + @"\\pinnacle.local\tech_resources\Nebula\XML Product Wipe Profiles", "*.xml", values.ToString());
                //vm.ProductList = StaticFunctions.GetFileNamesOnlyBasedOnValue(@"W:\Warehouse General\TB Tools App\XML Product Wipe Profiles", "*.xml", values.ToString());
                //foreach (var file in listFiles)
                //{
                //    vm.ProductList.Add(file.);
                //    MessageBox.Show(file);
                //}


                //if (File.Exists(@"" + vm.dsktop + @"\Invoice App\Cisco 1801 Router.xml"))
                //{
                // LOAD PREVIOUS SETTINGS FROM AN XML FILE;







                //foreach (var col in vm.ProductCollectionList)
                //{
                //    MessageBox.Show(col.Brand + " " + col.Category + " " + col.ModelNo);

                //    foreach(var step in col.Steps)
                //    {
                //        // can add to another steps collection
                //        MessageBox.Show(step.CliCommandToSend);

                //    }
                //}



                //}

                //Clear the controls on the page
                //vm.ClearAddProduct();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

     
    }
}