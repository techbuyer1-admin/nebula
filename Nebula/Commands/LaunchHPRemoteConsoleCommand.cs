﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class LaunchHPRemoteConsoleCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public LaunchHPRemoteConsoleCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            var values = (object[])parameter;

                    
            var ServerIP = (string)values[0];
            var ServerNumber = (string)values[1];

            if (values[0] != null)
            {
                if (values[0].ToString() != string.Empty)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public void Execute(object parameter)
        {

            var values = (object[])parameter;


            var ServerIP = (string)values[0];
            var ServerNumber = (string)values[1];
            var Mode = (string)values[2];


            //Drill down and find Remote console host and hide it
            //use helper to get control handle
            WindowsFormsHost RemoteConsoleHost = Helper.FindChild<WindowsFormsHost>(Application.Current.MainWindow, "WFHS1");

            if (RemoteConsoleHost != null)
                RemoteConsoleHost.Visibility = Visibility.Visible;


            switch (Mode)
            {
               case "CloseConsole":

                   

                    //Set static to call from other views if required
                    StaticFunctions.remoteConsolePID = vm.HPRemoteConsolePIDS1;
                    //Close already opened remoteconsole
                    //MessageBox.Show(vm.HPRemoteConsolePIDS1.ToString());
                    CloseAppByPid(vm.HPRemoteConsolePIDS1);

                    //RemoteConsoleHost.Child.Refresh();



                    break;

                case "LaunchEmbedded":

                    //Select the remote console tab
                    //Gen8Tab1.SelectedIndex = 9;
                    //close previous if there
                    CloseAppByPid(vm.HPRemoteConsolePIDS1);
                    StaticFunctions.remoteConsolePID = vm.HPRemoteConsolePIDS1;
                    //MessageBox.Show(vm.HPRemoteConsolePIDS1.ToString());
                    //await PutTaskDelay(3000);

                    vm.HostExternalEXE(@"" + vm.myDocs + @"\HPTools\HP iLO Integrated Remote Console\HPLOCONS.exe", @"-addr " + vm.Server1IPv4 + ":443 -name Administrator -password " + vm.OverridePasswordS1.Trim() + " -lang en", RemoteConsoleHost);


                    break;

                case "LaunchExternal":
                    //Set static to call from other views if required
                    StaticFunctions.remoteConsolePID = vm.HPRemoteConsolePIDS1;
                    //Close already opened remoteconsole
                    //MessageBox.Show(vm.HPRemoteConsolePIDS1.ToString());
                    CloseAppByPid(vm.HPRemoteConsolePIDS1);

                  

                    //Delay before launching
                    Thread.Sleep(500);


                    StaticFunctions.RunRemoteConsoleCommand(@"" + vm.myDocs + @"\HPTools\HP iLO Integrated Remote Console\HPLOCONS.exe", @"-addr " + ServerIP.ToString() + ":443 -name Administrator -password " + vm.OverridePasswordS1.Trim() + " -lang en");
                    //StaticFunctions.RunRemoteConsoleCommand(@"" + vm.myDocs + @"\HPTools\HP iLO Integrated Remote Console\HPLOCONS.exe", @"-addr " + vm.ServerIPv4 + ":443 -name Administrator -password " + vm.OverridePasswordS1.Trim() + " -lang en");
                   

                    break;


            }

        }



        private void CloseAppByPid(int processID)
        {
            try
            {
                Process proc = Process.GetProcessById(processID);
                if (proc.ProcessName.Contains("HPLOCONS"))
                {
                    proc.Kill();
                }

            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                // Process already exited.
            }
        }

    }
}

