﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;
using Nebula.DellServers;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;
using Nebula.Views;
using PuppeteerSharp;

namespace Nebula.Commands
{
    public class GetDellServerDataCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public GetDellServerDataCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            if (vm.Server1IPv4 != null)
            {
                //check if valid ip entered
                //System.Net.IPAddress ipAddress = null;
                //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                if (vm.Server1IPv4 != string.Empty)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public async void Execute(object parameter)
        {



            var values = (object[])parameter;
            var ScriptChoice = (string)values[0];
            var ServerGen = (string)values[1];


            //A: Setup and stuff you don't want timed
            var timer = new Stopwatch();
            timer.Start();
            TimeSpan timeTaken;
            string timetaken;

            // Create new redfish instance
            RedfishRestful RedfishCrawler = new RedfishRestful();



            TabItem ServerTab = null;
            string ServerTabHeader = string.Empty;
            //Hide any overlays
            vm.ShowOverlay = Visibility.Collapsed;


            string SyncLog = "";

            try
            {
                

                SyncLog += "[Start of Nebula Sync Log]\r\r" + DateTime.Now.ToString() + "\r\r";


                // MessageBox.Show(ServerGen);
                // Set the Tab Header if a normal server, skip if a blade as this already gets named by the Enclosure Function
                //if (vm.IsBla)
                //{
                switch (vm.WhichTab)
                {
                    case "1":
                        // MessageBox.Show("Server On Tab 1");

                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS1") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server1";
                        //vm.Server1TabHeader = ServerGen;
                        break;
                    case "2":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS2") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server2";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "3":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS3") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server3";
                        break;
                    case "4":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS4") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server4";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "5":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS5") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server5";
                        break;
                    case "6":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS6") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server6";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "7":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS7") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server7";
                        break;
                    case "8":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS8") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server8";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "9":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS9") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server9";
                        break;
                    case "10":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS10") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server10";
                        // MessageBox.Show("Server On Tab 2");
                        break;
                    case "11":
                        // MessageBox.Show("Server On Tab 1");
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS11") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server11";
                        break;
                    case "12":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS12") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server12";
                        // MessageBox.Show("Server On Tab 2");
                        break;

                    case "13":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS13") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server13";
                        // MessageBox.Show("Server On Tab 2");
                        break;

                    case "14":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS14") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server14";
                        // MessageBox.Show("Server On Tab 2");
                        break;

                    case "15":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS15") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server15";
                        // MessageBox.Show("Server On Tab 2");
                        break;

                    case "16":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS16") as TabItem;
                        if (ServerTab != null)
                            ServerTab.Foreground = Brushes.Orange;
                        ServerTabHeader = "Server16";
                        // MessageBox.Show("Server On Tab 2");
                        break;


                }


                // vm.TabColorS1 = Brushes.Orange;

                // MessageBox.Show(vm.OverridePasswordS1);

                if (vm.Server1IPv4 != null)
                {
                    if (vm.Server1IPv4 != string.Empty)
                    {
                        string address = vm.Server1IPv4;
                        vm.WarningMessage1 = "";
                        vm.ShowPopup = false;
                        vm.ProgressMessage1 = "";
                        //send a ping to the server to check it's available
                        vm.ProgressMessage1 = "Checking Server IP, Please Wait...";
                        vm.ProgressPercentage1 = 0;
                        vm.ProgressIsActive1 = true;
                        vm.ProgressVisibility1 = Visibility.Visible;
                        //To hold count of total updates
                        int updateCount = 0;
                        //Send a ping to check if server ip is there
                        await System.Threading.Tasks.Task.Run(() => sendAsyncPingPacket(address));
                        //add a delay
                        await PutTaskDelay(1000);

                        //if it is run the code
                        if (vm.IsServerThere1 == "Yes")
                        {


                            //Set script running property to true
                            vm.IsScriptRunningS1 = true;


                            //CANCEL SCRIPT
                            if (vm.CancelScriptS1 == true)
                            {
                                vm.ProgressPercentage1 = 0;
                                vm.ProgressIsActive1 = false;
                                vm.ProgressVisibility1 = Visibility.Hidden;
                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                vm.WarningMessage1 = "";
                                vm.CancelScriptS1 = false;
                                return;
                            }
                            //END CANCEL SCRIPT



                            ProcessPiper pp = new ProcessPiper(vm);

                            //RESET THE COMMAND BOX
                            vm.ILORestOutput1 = "";
                            //IMPORTANT Otherwise duplicates
                            vm.DELLInfoS1.Clear();


                            if (vm.DellServerS1 != null)
                            {

                                //MessageBox.Show("Yep");
                                vm.DellServerS1.FansInventory = String.Empty;
                                vm.DellServerS1.CPUInventory = String.Empty;
                                vm.DellServerS1.PowerInventory = String.Empty;
                                vm.DellServerS1.StorageInventory = String.Empty;
                                vm.DellServerS1.MemoryInventory = String.Empty;
                                vm.DellServerS1.NetworkInventory = String.Empty;
                                vm.DellServerS1.DELLSystemCollection.Clear();
                                vm.DellServerS1.DELLiDracCollection.Clear();
                                vm.DellServerS1.DELLCpuCollection.Clear();
                                vm.DellServerS1.DELLMemoryCollection.Clear();
                                vm.DellServerS1.DELLNetworkCollection.Clear();
                                vm.DellServerS1.DELLPowerCollection.Clear();
                                vm.DellServerS1.DELLStorageCollection.Clear();
                                vm.DellServerS1.DELLFanCollection.Clear();
                            }



                            //TRY A FIXED DOCUMENT?

                            switch (ScriptChoice)
                            {


                                case "ServerInfo":



                                    //Run initially to obtain embedded license info
                                    if (vm.GetServerInfoInitialRun == false || vm.GetServerInfoInitialRun == true && string.IsNullOrEmpty(vm.DellServerS1.iDRACLicense))
                                    {
                                        //Disable default password warning
                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn set iDRAC.Tuning.DefaultCredentialWarning Disabled", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        //*** LICENSE CHECK ***
                                        vm.ProgressMessage1 = "Checking License, Please wait...";

                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn license view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                        //Uninstall any eval licenses if already present
                                        vm.ProgressPercentage1 = 10;


                                        //REMOVE EVAL LICENSE IF PRESENT CHECK
                                        //To Place in Factory Defaults Capture 
                                        if (vm.ILORestOutput1.Contains("Transaction ID       =") && vm.ILORestOutput1.Contains("License Type         = EVALUATION") && vm.DellEvalInstalledS1 == false)
                                        {
                                            vm.ProgressMessage1 = "Removing Evaluation License, Please wait...";

                                            //Capture ID
                                            string tranID = vm.ILORestOutput1.Substring(vm.ILORestOutput1.IndexOf("Transaction ID       ="), 27).Replace("Transaction ID       =", "").Trim();

                                            //Remove Eval License
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn license delete -t" + tranID, @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            //Reset Flag if true
                                            vm.DellBasicLicS1 = false;
                                            vm.DellEvalInstalledS1 = true;

                                        }


                                        ////Get Embedded License if not captured first time
                                        //if (vm.DellEmbeddedLicense == "")
                                        //{
                                        //    //Call embedded license. Uses puppeteer to access webpage using a headless browser
                                        //    await GetEmbeddedLicense();

                                        //    //Pull in Idrac license
                                        //    vm.DellServerS1.iDRACLicense = vm.DellEmbeddedLicense;
                                        //}
                                        vm.ILORestOutput1 += "*** Server Inventory in progress ***" + "\n\n";

                                        vm.ProgressPercentage1 = 30;
                                        vm.ProgressMessage1 = "Server Inventory in progress, please wait!";


                                        if (vm.DellGeneration == "iDRAC9")
                                        {
                                            //*** RACADM SERVER INVENTORY ***
                                            //acquire initial server inventory
                                            //await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory | findstr /i Model", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.DellServerS1 = null;

                                            //CREATE A FRESH ITEM & CLEAR Data Collection
                                            vm.DellServerS1 = new DELLServerInfo();
                                            vm.DellDataCollectionS1.Clear();
                                            ////Capture and map data
                                            vm.DELLCaptureInventoryData(vm.DELLInfoS1, vm.DellDataCollectionS1);
                                            vm.DELLGetServerInventory(vm.DellServerS1, vm.DellDataCollectionS1);
                                            //END RACADM INVENTORY
                                            //*** SERVER INVENTORY END ***
                                        }
                                        else
                                        {
                                            //*** RACADM SERVER INVENTORY ***
                                            //acquire initial server inventory
                                            //await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory | findstr /i Model", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.DellServerS1 = null;

                                            //CREATE A FRESH ITEM & CLEAR Data Collection
                                            vm.DellServerS1 = new DELLServerInfo();
                                            vm.DellDataCollectionS1.Clear();
                                            ////Capture and map data
                                            vm.DELLCaptureInventoryData(vm.DELLInfoS1, vm.DellDataCollectionS1);
                                            vm.DELLGetServerInventory(vm.DellServerS1, vm.DellDataCollectionS1);




                                            //END RACADM INVENTORY
                                            //*** SERVER INVENTORY END ***
                                            //idrac 7 & 8
                                            //*** SERVER INVENTORY ***
                                            //REDFISH SERVER INVENTORY
                                            //*** vm.DellServerS1 = await RedfishCrawler.GetDellData(vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);
                                            //END REDFISH INVENTORY
                                        }




                                        //String to hold Model Number Of Server. Used to determine Default License
                                        string SystemModelNumber = vm.DellServerS1.Model;
                                        //Grab embedded license from inventory
                                        vm.DellEmbeddedLicense = vm.DellServerS1.iDRACLicense;

                                        //Set flag to true
                                        vm.GetServerInfoInitialRun = true;
                                        //Clear initials output
                                        //vm.ILORestOutput1 = "";
                                    }
                                    else
                                    {
                                        vm.ProgressPercentage1 = 30;



                                        vm.ILORestOutput1 += "*** Server Inventory in progress ***" + "\n\n";

                                        vm.ProgressMessage1 = "Server Inventory in progress, please wait!";

                                        if (vm.DellGeneration == "iDRAC9")
                                        {

                                            //*** RACADM SERVER INVENTORY ***
                                            //acquire initial server inventory
                                            //await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory | findstr /i Model", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.DellServerS1 = null;

                                            //CREATE A FRESH ITEM & CLEAR Data Collection
                                            vm.DellServerS1 = new DELLServerInfo();
                                            vm.DellDataCollectionS1.Clear();
                                            ////Capture and map data
                                            vm.DELLCaptureInventoryData(vm.DELLInfoS1, vm.DellDataCollectionS1);
                                            vm.DELLGetServerInventory(vm.DellServerS1, vm.DellDataCollectionS1);
                                            //END RACADM INVENTORY
                                            //*** SERVER INVENTORY END ***
                                        }
                                        else
                                        {
                                            //idrac 7 & 8
                                            //*** RACADM SERVER INVENTORY ***
                                            //acquire initial server inventory
                                            //await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory | findstr /i Model", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.DellServerS1 = null;

                                            //CREATE A FRESH ITEM & CLEAR Data Collection
                                            vm.DellServerS1 = new DELLServerInfo();
                                            vm.DellDataCollectionS1.Clear();
                                            ////Capture and map data
                                            vm.DELLCaptureInventoryData(vm.DELLInfoS1, vm.DellDataCollectionS1);
                                            vm.DELLGetServerInventory(vm.DellServerS1, vm.DellDataCollectionS1);
                                            //END RACADM INVENTORY

                                            //*** SERVER INVENTORY ***
                                            //REDFISH SERVER INVENTORY
                                            //vm.DellServerS1 = await RedfishCrawler.GetDellData(vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);
                                            //END REDFISH INVENTORY
                                        }


                                        //String to hold Model Number Of Server. Used to determine Default License
                                        string SystemModelNumber = vm.DellServerS1.Model;
                                        ////Pull in Idrac license from embedded captured on first run
                                        vm.DellServerS1.iDRACLicense = vm.DellEmbeddedLicense;


                                    }
                                    //Output inventory to console for debugging
                                    //foreach (var itm in vm.DELLInfoS1)
                                    //{
                                    //    Console.WriteLine(itm);
                                    //}
                                    //Server Inventory Complete

                                    vm.ILORestOutput1 += "Server Inventory Complete!\n\n";


                                    vm.ProgressPercentage1 = 40;

                                    //Proceed with rest of the code
                                    string SIDateClicked = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                                    vm.ProgressMessage1 = "Please wait...";
                                    vm.ProgressIsActive1 = true;
                                    vm.ProgressPercentage1 = 50;
                                    vm.ProgressVisibility1 = Visibility.Visible;

                                    //LICENSE CHECK
                                    vm.ProgressMessage1 = "Checking License, Please wait...";


                                    //SERVER LICENSE, DETERMINE IF 
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " license view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                    if (vm.ILORestOutput1.Contains("Entitlement ID       ="))
                                    {


                                        //Use a regular expression to pluck out the value
                                        //var match = Regex.Match(vm.ILORestOutput1, @"Entitlement ID       =(.+?)Expiration").Groups[1].Value;

                                        var match = Regex.Match(vm.ILORestOutput1, @"Status(.+?)License Description").Groups[1].Value;


                                        if (match.Contains("The license has expired.") && match.Contains("Transaction ID"))
                                        {
                                            vm.DellEvaluationID = Regex.Match(vm.ILORestOutput1, @"Transaction ID       =(.+?)License Description").Groups[1].Value.Replace("Transaction ID       =", "").Trim();
                                        }


                                        //License Present, Express Or Enterprise or Evaluation of Either
                                        //Needs a check to see if eval license was installed
                                        if (vm.ILORestOutput1.Contains("License Type         = EVALUATION"))
                                        {
                                            //Evaluation previously installed
                                            vm.DellBasicLicS1 = true;
                                        }



                                    }
                                    //else
                                    //{
                                    if (vm.DellEmbeddedLicense == "Basic" || vm.DellEmbeddedLicense == "Express" && !vm.ILORestOutput1.Contains("License Type         = PERPETUAL"))
                                    {
                                        if (vm.ILORestOutput1.Contains("Device Description   = iDRAC7"))
                                        {
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " license import -f iDRAC7_Ent-Eval.xml -c idrac.embedded.1", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.DellBasicLicS1 = true;
                                        }

                                        if (vm.ILORestOutput1.Contains("Device Description   = iDRAC8"))
                                        {
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " license import -f iDRAC8_Ent-Eval.xml -c idrac.embedded.1", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.DellBasicLicS1 = true;
                                        }

                                        if (vm.ILORestOutput1.Contains("Device Description   = iDRAC9"))
                                        {
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " license import -f iDRAC9_Ent-Eval.xml -c idrac.embedded.1", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.DellBasicLicS1 = true;
                                        }
                                    }


                                    //}

                                    //END LICENSE


                                    vm.ProgressPercentage1 = 50;



                                    //vm.ProgressMessage1 = "Server Inventory in progress, Please wait...";

                                    // **vm.ProgressMessage1 = "Clearing Jobqueue";  await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                    ////CLEAR SERVER JOBS
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue delete --all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue delete -i JID_CLEARALL_FORCE", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                    //await PutTaskDelay(1000);

                                    //check job queue
                                    //vm.ProgressMessage1 = "Checking Job Queue";

                                    //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                    //Set Virtual Console to HTML5
                                    // ** await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   set iDRAC.VirtualConsole.PluginType 2", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                    vm.ProgressPercentage1 = 60;
                                    //Set Enable SSH
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   set iDRAC.SSH 1", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                    //Disable Default Password option  idrac.tuning.DefaultCredentialWarning
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   set iDRAC.Tuning.DefaultCredentialWarning 0", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                    //Set inventory to be created on system start
                                    //vm.ProgressMessage1 = "Enable Server Inventory";
                                    //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   set LifecycleController.LCAttributes.CollectSystemInventoryOnRestart 1", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                    //await PutTaskDelay(1000);
                                    // await System.Threading.Tasks.Task.Run(() => vm.HTTPRequestTest(@"https://" + vm.Server1IPv4 + @"/console/", "POST", "root", "calvin"));




                                    //await System.Threading.Tasks.Task.Run(() => pp.StartHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn set iDRAC.Tuning.DefaultCredentialWarning Disabled", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                    //await System.Threading.Tasks.Task.Run(() => pp.StartHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn get iDRAC.Info", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                    // await System.Threading.Tasks.Task.Run(() => pp.StartHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn getsysinfo -d", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                    //original that worked with xml until structure changes
                                    //await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  hwinventory export -f " + vm.Server1IPv4 + @"Inventory.xml", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                    //Check if Script is Cancelled
                                    //vm.CancelServerScript("S1");

                                    vm.ProgressPercentage1 = 80;

                                    if (vm.CancelScriptS1 == true)
                                    {
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                        return;
                                    }



                                    //ENDS HERE STARTS HERE
                                    vm.ProgressPercentage1 = 90;



                                    vm.ProgressPercentage1 = 100;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;


                                    //Stop Timer
                                    timer.Stop();
                                    timeTaken = timer.Elapsed;
                                    timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");



                                    //End of log
                                    SyncLog += vm.ILORestOutput1;
                                    SyncLog += "\rTime taken:  " + timeTaken.ToString(@"m\:ss\.fff") + "\r";
                                    SyncLog += "\r[End of Nebula Sync Log]\r\r";





                                    //LCD & SDCARD CHECKS
                                    vm.ProgressMessage1 = "Checking SD Card presence.";
                                    //get lcd
                                    await System.Threading.Tasks.Task.Run(() => pp.StartSensorInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn get System.LCD", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                    //Check for SD Card
                                    await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn vflashsd status", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                    //await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  getversion -i -f idsdm", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                    //await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  iDRAC.vflashsd.AvailableSize", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                    //CANCEL SCRIPT
                                    if (vm.CancelScriptS1 == true)
                                    {
                                        vm.ProgressPercentage1 = 0;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.ProgressMessage1 = "Scripts Cancelled!";
                                        vm.WarningMessage1 = "";
                                        vm.CancelScriptS1 = false;
                                        return;
                                    }
                                    //END CANCEL SCRIPT


                                    //Check if SD CARD Message is present in the script window

                                    if (vm.ILORestOutput1.Contains("SD card is not present"))
                                    {
                                        vm.DellServerS1.SDCardInserted = "No";
                                    }
                                    else
                                    {
                                        vm.DellServerS1.SDCardInserted = "Yes";
                                    }





                                    //ERROR CHECKS AND FIRMWARE MESSAGES

                                    if (vm.DellServerS1.SystemGeneration != null)
                                    {



                                        //vm.Server1TabHeader = vm.Server1TabHeader.Replace(" (" + vm.DellServerS1.ServiceTag + ")", "");
                                        //vm.Server1TabHeader = vm.Server1TabHeader + " (" + vm.DellServerS1.ServiceTag + ")";

                                        //Set Tab Header
                                        if (vm.DellServerS1.ServiceTag != null)
                                        {
                                            ServerTab.Header = ServerTab.Header.ToString().Trim().Replace("(" + vm.DellServerS1.ServiceTag + ")", "");
                                            ServerTab.Header = ServerTab.Header.ToString().Trim() + " (" + vm.DellServerS1.ServiceTag.Trim() + ")";
                                        }
                                        else
                                        {
                                            vm.WarningMessage1 += "Service Tag Blank or Missing. Please correct before further processing.";
                                        }






                                        //    //vm.DellServerS1.SDCardInserted = "No"; ERROR: vFlash is not enabled. 3.30.30.30
                                        //}

                                        //Check Firmware Versions 2.40.40.40 
                                        if (vm.DellServerS1.SystemGeneration != null)
                                        {


                                            if (vm.DellServerS1.SystemGeneration.Contains("12G"))
                                            {
                                                if (vm.DellServerS1.Model != null)
                                                    if (vm.DellServerS1.Model.Contains("820"))
                                                    {

                                                    }
                                                    else
                                                    {
                                                        //BIOS Check
                                                        vm.DELLFirmwareVersionCheck(vm.DellServerS1.BiosCurrentVersion, "2.0.0", "S1", "bios", "BIOS version under 2.0.0! iDRAC is below the minimum signature level 2.0.0! Use the firmimg.d7 or .d9 update to jump to the required level.");
                                                    }

                                                //IDRAC Check
                                                vm.DELLFirmwareVersionCheck(vm.DellServerS1.iDRACVersion, "2.60.60.60", "S1", "idrac", "iDRAC is below the minimum signature level 2.60.60.60! Use the firmimg.d7 or .d9 update to jump to the required level.");
                                            }
                                            else if (vm.DellServerS1.SystemGeneration.Contains("13G"))
                                            {
                                                if (vm.DellServerS1.Model != null)
                                                    if (vm.DellServerS1.Model.Contains("830"))
                                                    {

                                                    }
                                                    else
                                                    {
                                                        //BIOS Check
                                                        vm.DELLFirmwareVersionCheck(vm.DellServerS1.BiosCurrentVersion, "2.0.0", "S1", "bios", "BIOS version under 2.0.0! iDRAC is below the minimum signature level 2.0.0! Use the firmimg.d7 or .d9 update to jump to the required level.");
                                                    }

                                                //IDRAC Check
                                                vm.DELLFirmwareVersionCheck(vm.DellServerS1.iDRACVersion, "2.40.40.40", "S1", "idrac", "iDRAC is below the minimum signature level 2.40.40.40! Use the firmimg.d7 or .d9 update to jump to the required level.");
                                            }
                                            else if (vm.DellServerS1.SystemGeneration.Contains("14G"))
                                            {
                                                if (vm.DellServerS1.Model != null)
                                                    if (vm.DellServerS1.Model.Contains("840") || vm.DellServerS1.Model.Contains("6415") || vm.DellServerS1.Model.Contains("7415"))
                                                    {

                                                    }
                                                    else
                                                    {
                                                        //BIOS Check
                                                        vm.DELLFirmwareVersionCheck(vm.DellServerS1.BiosCurrentVersion, "2.0.0", "S1", "bios", "BIOS version under 2.0.0! iDRAC is below the minimum signature level 2.0.0! Use the firmimg.d7 or .d9 update to jump to the required level.");

                                                    }






                                                //IDRAC Check
                                                vm.DELLFirmwareVersionCheck(vm.DellServerS1.iDRACVersion, "3.30.30.30", "S1", "idrac", "iDRAC is below the minimum signature level 3.30.30.30! Use the firmimg.d7 or .d9 update to jump to the required level.");
                                            }
                                        }



                                        //Load Ultima URL
                                        if (vm.DellServerS1 != null)
                                        {
                                            if (vm.DellServerS1.Manufacturer != null && vm.DellServerS1.Model != null)
                                                vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.DellServerS1.Manufacturer.Replace("Inc.", "") + " " + vm.DellServerS1.Model + " Server &go=Go";
                                        }




                                    }
                                    else
                                    {
                                        //check for licensing issues
                                        if (vm.ILORestOutput1.Contains("ERROR: SWC0242 : A required license is missing or expired."))
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.WarningMessage1 += @"No Data Returned! License has expired. Trial license could not be imported.";
                                        }
                                        else
                                        {

                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.WarningMessage1 += @"No Data Returned! Select the latest iDrac update in .d7 .d9 format and click the 1)iDrac Update button to apply";
                                        }



                                    }





                                    //Check if License was originally Basic and had an evaluation put on License Type         = EVALUATION
                                    //if (vm.DellBasicLicS1 == true && vm.ILORestOutput1.Contains("License Type         = EVALUATION"))
                                    //{
                                    //    //Append Eval onto license
                                    //    vm.DellServerS1.iDRACLicense = vm.DellEmbeddedLicense;
                                    //    vm.DellServerS1.iDracInfo.Model = vm.DellEmbeddedLicense;
                                    //    vm.DellServerS1.iDracInfo.DeviceDescription = vm.DellEmbeddedLicense;

                                    //}
                                    //else
                                    //{
                                    //    //Append Eval onto license
                                    //    vm.DellServerS1.iDRACLicense = vm.DellEmbeddedLicense;
                                    //    vm.DellServerS1.iDracInfo.Model = vm.DellEmbeddedLicense;
                                    //    vm.DellServerS1.iDracInfo.DeviceDescription = vm.DellEmbeddedLicense;
                                    //}





                                    //Check Server Logs
                                    //vm.ProgressMessage1 = "Checking Sensors.";
                                    vm.ILORestOutput1 += "********** SENSOR INFORMATION, PLEASE CHECK BEFORE APPLYING UPDATES **********\n\n";
                                    //await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  lclog view -c storage,system -s critical", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                    //await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  lclog view -c storage,system -s critical, warning -r \"" + SIDateClicked + " 0:00:00\" -e \"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\"", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                    //SENSOR COMMENTED OUT FOR SPEED
                                    // ##### await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn getsensorinfo", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                    //SD CARD
                                    if (vm.ILORestOutput1.Contains("Required License -- Virtual Flash Partitions") || vm.ILORestOutput1.Contains("SD card is not present"))
                                    {
                                        if (vm.WarningMessage1 != "")
                                        {

                                        }
                                        else
                                        {
                                            vm.WarningMessage1 = "";
                                        }


                                    }
                                    else if (vm.ILORestOutput1.Contains("Unknown error.") || vm.ILORestOutput1.Contains("ERROR: vFlash is not enabled.") || vm.ILORestOutput1.Contains("ERROR: vFlash is not enabled.") || vm.ILORestOutput1.Contains("ERROR: vFlash is not enabled."))
                                    {

                                        vm.GDPRRiskDetected = "Yes, Warning was issued.";
                                        vm.WarningMessage1 += "SD CARD Detected! GDPR Risk, Please remove from the system.";
                                    }
                                    else if (vm.ILORestOutput1.Contains("ERROR: Session is not valid."))
                                    {


                                        vm.WarningMessage1 += "Unable to run Server Inventory. Please allow Server to finish post then try again";
                                    }

                                    if (vm.ILORestOutput1.Contains("IDSDM SD1                       Good") || vm.ILORestOutput1.Contains("IDSDM SD2                       Good") || vm.ILORestOutput1.Contains("IDSDM SD1                       Failed") || vm.ILORestOutput1.Contains("IDSDM SD2                       Failed"))
                                    {
                                        vm.WarningMessage1 += "SD CARD Detected! GDPR Risk, Please remove from the system.";
                                    }





                                    //POST Information
                                    //if (vm.DellServerS1.SystemGeneration != null)
                                    //{
                                    //    if (vm.DellServerS1.SystemGeneration.Contains("14G"))
                                    //    {
                                    //        vm.ProgressMessage1 = "POST Information!";
                                    //        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn getremoteservicestatus", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                    //    }

                                    //}


                                    //Check if license is degraded
                                    if (vm.DellServerS1.LicensingRollupStatus == "Degraded")
                                    {
                                        vm.DeleteDellLicenseBTN = Visibility.Visible;
                                        vm.WarningMessage1 = "License degraded, please use the Remove License button";
                                    }

                                    //check if blade
                                    if (vm.DellServerS1.SystemGeneration != null)
                                        if (vm.DellServerS1.SystemGeneration.Contains("Modular"))
                                        {
                                            if (string.IsNullOrEmpty(vm.DellServerS1.ControllerType) && string.IsNullOrEmpty(vm.DellServerS1.ControllerVersion))
                                            {
                                                vm.WarningMessage1 += "Embedded controller not listed, please set to RAID in BIOS.";
                                            }

                                        }

                                    //Check if BOSS Card registered. If not throw warning
                                    if (vm.DellServerS1.ControllerType2 != null)
                                        if (vm.DellServerS1.ControllerType2.Contains("BOSS"))
                                        {

                                            if (vm.DellServerS1.DeviceCardSlotType == "Unknown")
                                            {
                                                vm.WarningMessage1 += "BOSS Card Issue! Please run latest Service Pack To Resolve!!!";
                                            }


                                        }




                                    //Check PCIe\CLPD version
                                    if (vm.DellServerS1.Model != null)
                                        if (vm.DellServerS1.Model.Contains("6415") || vm.DellServerS1.Model.Contains("7415"))
                                        {

                                            //Check PCIe\CLPD version
                                            if (vm.DellServerS1.CPLDVersion != null)
                                            {
                                                vm.DELLFirmwareVersionCheck(vm.DellServerS1.CPLDVersion, "1.0.14", "S1", @"PCIe\CPLD", @"Warning! PCIe\CPLD Update is required before applying any other firmware updates. This requires version 1.0.14. Make sure to only select this update and click the BIOS\Pcie\Cont button once done.");
                                            }

                                        }


                                    vm.ProgressPercentage1 = 0;
                                    vm.ProgressIsActive1 = false;
                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                    vm.ProgressMessage1 = "Server Inventory complete! " + timetaken;


                                    // DIALOG MESSAGE

                                    //if (vm.DellServerS1.SystemGeneration.Contains("Modular"))
                                    //{
                                    //   //Skip as no Blade contains an optical drive
                                    //}
                                    //else
                                    //{
                                    //    vm.CDROMWarning(@"If this system contains an Optical Drive, please check now for any Media in the CD\DVD Rom tray. This can be a GDPR Risk!");
                                    //    //vm.WarningMessage1 = @"If this system contains an Optical Drive, please check now for any Media in the CD\DVD Rom tray. This can be a GDPR Risk!";

                                    //    //if (!string.IsNullOrEmpty(vm.WarningMessage1))
                                    //    //{
                                    //    //    vm.WarningMessage1 = @"If this system contains an Optical Drive, please check now for any Media in the CD\DVD Rom tray. This can be a GDPR Risk!";
                                    //    //}
                                    //}



                                    break;
                                case "WipeScripts":

                                    if (vm.DellServerS1 != null)//vm.Gen9ServerInfo1 != null ||
                                    {

                                        //check if server info was required vm.Gen9ServerInfo.ChassisSerial != string.Empty || 
                                        if (vm.DellServerS1.ServiceTag != string.Empty)
                                        {
                                            //check if firmwares were selected
                                            //if (vm.ILOUpgradePath != String.Empty || vm.BIOSUpgradePath != String.Empty)
                                            //  {


                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");


                                            vm.ProgressMessage1 = "Please wait...";
                                            vm.ProgressIsActive1 = true;
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressVisibility1 = Visibility.Visible;

                                            vm.ProgressPercentage1 = 10;
                                            //rac reset
                                            // await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  racreset soft -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                            vm.ProgressMessage1 = "Clearing Jobqueue";
                                            //CLEAR SERVER JOBS
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn jobqueue delete --all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue delete -i JID_CLEARALL_FORCE", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await PutTaskDelay(2000);

                                            //CLEAR HOSTNAME SERVERNAME
                                            vm.ProgressMessage1 = "Clearing Server Name";

                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " set System.ServerOS.HostName NewServer", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  config  cfgServerInfo.cfgServerName ''", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.ProgressPercentage1 = 10;


                                            //if pcie\cpld update is selected then skip Server Name change as you can't run that update with another one
                                            if (String.IsNullOrEmpty(vm.CPLDUpgradePath1))
                                            {
                                                //Check Asset tag, if already changed skip job creation
                                                if (vm.DellServerS1.AssetTag != "NewTag")
                                                {
                                                    vm.ProgressMessage1 = "Clearing Asset Tag";
                                                    //CLEAR ASSET TAG AND CREATE JOB
                                                    // await PutTaskDelay(10000);
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  set BIOS.MiscSettings.AssetTag NewTag", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                    //await PutTaskDelay(10000);
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue create BIOS.Setup.1-1", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                    await PutTaskDelay(10000);

                                                    //Check task view via redfish
                                                    if (vm.ILORestOutput1.Contains("Successfully scheduled a job"))
                                                    {
                                                        //Increase total count of updates
                                                        updateCount += 1;
                                                        //Wait for any tasks to finish, check every 10 seconds
                                                        await RedfishCrawler.WaitForTasksToFinish(10000, updateCount.ToString(), vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);
                                                    }

                                                }
                                            }
                                            else
                                            {

                                                //PCIE CPLD UPDATE
                                                if (vm.CPLDUpgradePath1 != String.Empty)
                                                {
                                                    vm.WarningMessage1 += "Warning! CPLD Updates are required to be run by themselves. All other updates will be ignored.";

                                                    vm.ProgressPercentage1 = 30;
                                                    //BIOS Firmware update
                                                    vm.ProgressMessage1 = @"PCIe\CPLD Flash Update";

                                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CPLDUpdate.exe"))
                                                    {
                                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn update -f " + vm.Server1IPv4 + @"CPLDUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                    }

                                                }

                                            }


                                            vm.ProgressPercentage1 = 10;





                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");




                                            vm.ProgressPercentage1 = 20;
                                            //await PutTaskDelay(10000);
                                            vm.ProgressMessage1 = "Shutting Server Down";
                                            //SHUT DOWN SERVER
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn serveraction powerdown -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await PutTaskDelay(10000);
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn serveraction powerdown", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");


                                            vm.ProgressPercentage1 = 30;




                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            vm.ProgressPercentage1 = 40;
                                            //await PutTaskDelay(10000);

                                            //CONTROLLER UPDATES
                                            //SKIP PAST ANY OTHER SELECTED UPDATES IF CPLD SELECTED
                                            if (String.IsNullOrEmpty(vm.CPLDUpgradePath1))
                                            {

                                                if (vm.BIOSUpgradePath1 != String.Empty)
                                                {
                                                    vm.ProgressPercentage1 = 40;
                                                    //BIOS Firmware update
                                                    vm.ProgressMessage1 = "BIOS Flash Update";

                                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe"))
                                                    {
                                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn update -f " + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                    }
                                                    else if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.d7"))
                                                    {
                                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn fwupdate -p -u -d " + vm.Server1IPv4 + @"BIOSUpdate.d7", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                    }

                                                    //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                    if (vm.ILORestOutput1.Contains(vm.Server1IPv4 + "BIOSUpdate.exe is initiated."))
                                                    {
                                                        //Increase total count of updates
                                                        updateCount += 1;
                                                        //Wait for any tasks to finish, check every 10 seconds
                                                        await RedfishCrawler.WaitForTasksToFinish(10000, updateCount.ToString(), vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);
                                                        await PutTaskDelay(30000);
                                                    }



                                                }




                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");
                                                //await PutTaskDelay(10000);

                                                vm.ProgressPercentage1 = 50;

                                                //Embedded Controller

                                                if (vm.CONTUpgradePath1 != String.Empty)
                                                {
                                                    vm.ProgressPercentage1 = 40;
                                                    //ilo  Firmware update
                                                    vm.ProgressMessage1 = "Controller Firmware Update";
                                                    // replace with loop on 
                                                    vm.ProgressPercentage1 = 40;

                                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe"))
                                                    {
                                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn update -f " + vm.Server1IPv4 + @"CONTUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                    }

                                                    //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                    if (vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONTUpdate.exe is initiated."))
                                                    {
                                                        //Increase total count of updates
                                                        updateCount += 1;
                                                        //Wait for any tasks to finish, check every 10 seconds
                                                        await RedfishCrawler.WaitForTasksToFinish(10000, updateCount.ToString(), vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);
                                                        await PutTaskDelay(90000);
                                                    }



                                                }


                                                //Additional Controller
                                                if (vm.CONT2UpgradePath1 != String.Empty)
                                                {
                                                    vm.ProgressPercentage1 = 40;
                                                    //ilo  Firmware update
                                                    vm.ProgressMessage1 = "Controller Firmware Update";
                                                    // replace with loop on 
                                                    vm.ProgressPercentage1 = 40;

                                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe"))
                                                    {
                                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn update -f " + vm.Server1IPv4 + @"CONT2Update.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                    }

                                                    //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                    if (vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONT2Update.exe is initiated."))
                                                    {
                                                        //Increase total count of updates
                                                        updateCount += 1;
                                                        //Wait for any tasks to finish, check every 10 seconds
                                                        await RedfishCrawler.WaitForTasksToFinish(10000, updateCount.ToString(), vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);
                                                        await PutTaskDelay(90000);
                                                    }


                                                }

                                            }


                                            //CHECK NO TRANSFER ERRORS CANCEL SCRIPT IF SO
                                            if (vm.ILORestOutput1.Contains("ERROR:  The Copy Operation Failed.") || vm.ILORestOutput1.Contains("ERROR: Unable to transfer image file") || vm.ILORestOutput1.Contains("ERROR: Remote host is not reachable or connection is interrupted."))
                                            {


                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled, Transfer Error Detected!";
                                                vm.WarningMessage1 += "Update Transfer Errors detected.";
                                                vm.CancelScriptS1 = false;

                                                vm.ProgressMessage1 = "Clearing Jobqueue";
                                                //CLEAR SERVER JOBS
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn jobqueue delete --all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue delete -i JID_CLEARALL_FORCE", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                await PutTaskDelay(2000);
                                                return;
                                            }




                                            vm.ProgressMessage1 = "Powering Server On";
                                            vm.ProgressPercentage1 = 50;
                                            //power server back on
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction hardreset", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " serveraction powerup", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");


                                            //Check if updates were selected
                                            if (vm.BIOSUpgradePath1 != String.Empty || vm.CONTUpgradePath1 != String.Empty || vm.CONT2UpgradePath1 != String.Empty || vm.CPLDUpgradePath1 != String.Empty)
                                            {




                                                //check job queue
                                                vm.ProgressMessage1 = "Checking Job Queue";



                                                vm.ProgressMessage1 = "Applying BIOS & Controller Updates!";
                                                vm.ProgressPercentage1 = 75;


                                                vm.ProgressPercentage1 = 80;

                                            }


                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");


                                            //  await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  set System.ServerOS.HostName ''", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.ProgressPercentage1 = 90;
                                            //await PutTaskDelay(10000);


                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            //IMPORTANT!!!!!!!!
                                            //Wait for any tasks to finish, check every 10 seconds
                                            await RedfishCrawler.WaitForTasksToFinish(10000, "0", vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);



                                            vm.ProgressPercentage1 = 95;


                                            //REFRESH DATA
                                            vm.ProgressMessage1 = "Server Inventory Running";

                                            //*** RACADM SERVER INVENTORY ***
                                            //acquire server inventory
                                            //await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory | findstr /i Model", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.DellServerS1 = null;

                                            //CREATE A FRESH ITEM & CLEAR Data Collection
                                            vm.DellServerS1 = new DELLServerInfo();
                                            vm.DellDataCollectionS1.Clear();
                                            ////Capture and map data
                                            vm.DELLCaptureInventoryData(vm.DELLInfoS1, vm.DellDataCollectionS1);
                                            vm.DELLGetServerInventory(vm.DellServerS1, vm.DellDataCollectionS1);
                                            //END RACADM INVENTORY

                                            //*** SERVER INVENTORY ***
                                            //REDFISH SERVER INVENTORY
                                            //vm.DellServerS1 = await RedfishCrawler.GetDellData(vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);
                                            //END REDFISH INVENTORY

                                            vm.ProgressPercentage1 = 95;
                                            //Check for SD Card
                                            await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn vflashsd status", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                            //get lcd
                                            await System.Threading.Tasks.Task.Run(() => pp.StartSensorInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn get System.LCD", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                                vm.WarningMessage1 = "";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }
                                            //END CANCEL SCRIPT




                                            //Apply original license license
                                            vm.DellServerS1.iDRACLicense = vm.DellEmbeddedLicense;
                                            vm.DellServerS1.iDracInfo.Model = vm.DellEmbeddedLicense;
                                            vm.DellServerS1.iDracInfo.DeviceDescription = vm.DellEmbeddedLicense;


                                            vm.ProgressPercentage1 = 98;



                                            //Stop Timer
                                            timer.Stop();
                                            timeTaken = timer.Elapsed;
                                            timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                            //End of log
                                            SyncLog += vm.ILORestOutput1;
                                            SyncLog += "\rTime taken:  " + timeTaken.ToString(@"m\:ss\.fff") + "\r";
                                            SyncLog += "\r[End of Nebula Sync Log]\r\r";


                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressPercentage1 = 100;
                                            vm.ProgressMessage1 = "Scripts completed successfully! Please check the Server Information is correct before moving to the next stage! " + timetaken;
                                            // //StaticFunctions.RunRemoteConsoleCommand(@"" + vm.myDocs + @"\HPTOOLS\HP iLO Integrated Remote Console\HPLOCONS.exe", @"-addr " + vm.Server1IPv4 + ":443 -name Administrator -password " + vm.OverridePasswordS1.Trim() + " -lang en");

                                            //CHECK IF BIOS OR CONTROLLER FAILED

                                            //BIOS CHECKS

                                            if (vm.BIOSUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("Job Name=Firmware Update: BIOS") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "BIOSUpdate.exe is initiated.") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "BIOSUpdate.d7 is initiated."))
                                            {


                                                //Check Update Signature Hasn't failed
                                                if (vm.ILORestOutput1.Contains("Unable to verify Update Package signature"))
                                                {
                                                    vm.ProgressPercentage1 = 0;
                                                    vm.ProgressIsActive1 = false;
                                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                                    vm.ProgressMessage1 = "";
                                                    vm.WarningMessage1 = "BIOS Package signature failed! Please select an update closer to the current firmware version and try again.";
                                                    vm.CancelScriptS1 = false;
                                                    return;
                                                }

                                            }
                                            else if (vm.BIOSUpgradePath1 != String.Empty)
                                            {
                                                vm.WarningMessage1 += "Bios Update Failed! ";

                                                //set colours
                                                if (vm.WarningMessage1 != "")
                                                {
                                                    //ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS1") as TabItem;
                                                    if (ServerTab != null)
                                                        ServerTab.Foreground = Brushes.Red;
                                                }
                                                else
                                                {
                                                    if (ServerTab != null)
                                                        ServerTab.Foreground = Brushes.Green;
                                                }

                                                return;
                                            }



                                            //if (vm.BIOSUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("Job Name=Firmware Update: BIOS") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "BIOSUpdate.exe is initiated.") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "BIOSUpdate.d7 is initiated."))
                                            //{
                                            //    //ok
                                            //    //BIOS
                                            //    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe"))
                                            //    {
                                            //        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe");
                                            //    }

                                            //    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.d7"))
                                            //    {
                                            //        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.d7");
                                            //    }
                                            //    //CLEAR Flash File INFORMATION
                                            //    vm.BIOSUpgradePath1 = String.Empty;


                                            //}


                                            //CPLD CHECKS

                                            if (vm.CPLDUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("Job Name=Firmware Update: CPLD") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CPLDUpdate.exe is initiated.") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CPLDUpdate.d7 is initiated."))
                                            {
                                                //ok
                                                //CPLD
                                                if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CPLDUpdate.exe"))
                                                {
                                                    File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CPLDUpdate.exe");
                                                }

                                                if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CPLDUpdate.d7"))
                                                {
                                                    File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CPLDUpdate.d7");
                                                }
                                                //CLEAR Flash File INFORMATION
                                                vm.CPLDUpgradePath1 = String.Empty;








                                                //Set Certificate Property
                                                vm.UpdatesRun += @" [CPLD] ";

                                                //Check Update Signature Hasn't failed
                                                if (vm.ILORestOutput1.Contains("Unable to verify Update Package signature"))
                                                {
                                                    vm.ProgressPercentage1 = 0;
                                                    vm.ProgressIsActive1 = false;
                                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                                    vm.ProgressMessage1 = "";
                                                    vm.WarningMessage1 += "CPLD Package signature failed!";
                                                    vm.CancelScriptS1 = false;
                                                    return;
                                                }

                                            }
                                            else if (vm.CPLDUpgradePath1 != String.Empty)
                                            {
                                                vm.WarningMessage1 += "CPLD Update Failed! ";

                                                //set colours
                                                if (vm.WarningMessage1 != "")
                                                {
                                                    //ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS1") as TabItem;
                                                    if (ServerTab != null)
                                                        ServerTab.Foreground = Brushes.Red;
                                                }
                                                else
                                                {
                                                    if (ServerTab != null)
                                                        ServerTab.Foreground = Brushes.Green;
                                                }

                                                return;
                                            }



                                            //if (vm.CPLDUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("Job Name=Firmware Update: CPLD") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CPLDUpdate.exe is initiated.") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CPLDUpdate.d7 is initiated."))
                                            //{
                                            //    //ok
                                            //    //CPLD
                                            //    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CPLDUpdate.exe"))
                                            //    {
                                            //        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CPLDUpdate.exe");
                                            //    }

                                            //    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CPLDUpdate.d7"))
                                            //    {
                                            //        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CPLDUpdate.d7");
                                            //    }
                                            //    //CLEAR Flash File INFORMATION
                                            //    vm.CPLDUpgradePath1 = String.Empty;


                                            //}

                                            //if (vm.CONTUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("Job Name=Firmware Update: SAS RAID") || vm.ILORestOutput1.Contains("Job Name=Firmware Update: RAID") || vm.CONT2UpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("Job Name=Firmware Update: SAS RAID") || vm.ILORestOutput1.Contains("Job Name=Firmware Update: RAID") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONTUpdate.exe is initiated.") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONT2Update.exe is initiated.") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONT2Update.exe is initiated."))
                                            //{
                                            //    //ok
                                            //    //CONTROLLER  192.168.101.159CONTUpdate.exe is initiated
                                            //    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe"))
                                            //    {
                                            //        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe");

                                            //    }

                                            //    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe"))
                                            //    {
                                            //        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe");

                                            //    }
                                            //    vm.CONTUpgradePath1 = String.Empty;
                                            //    vm.CONT2UpgradePath1 = String.Empty;



                                            //    vm.UpdatesRun += @" [Controller] ";

                                            //    //Check Update Signature Hasn't failed
                                            //    if (vm.ILORestOutput1.Contains("Unable to verify Update Package signature"))
                                            //    {
                                            //        vm.ProgressPercentage1 = 0;
                                            //        vm.ProgressIsActive1 = false;
                                            //        vm.ProgressVisibility1 = Visibility.Hidden;
                                            //        vm.ProgressMessage1 = "";
                                            //        vm.WarningMessage1 += "Controller Package signature failed!";
                                            //        vm.CancelScriptS1 = false;
                                            //        return;
                                            //    }

                                            //}
                                            //else if (vm.CONTUpgradePath1 != "" || vm.CONT2UpgradePath1 != "")
                                            //{
                                            //    // vm.WarningMessage1 = vm.WarningMessage1 + " Controller Update Failed! ";

                                            //    //set colours
                                            //    if (vm.WarningMessage1 != "")
                                            //    {
                                            //        if (ServerTab != null)
                                            //            ServerTab.Foreground = Brushes.Red;
                                            //    }
                                            //    else
                                            //    {
                                            //        if (ServerTab != null)
                                            //            ServerTab.Foreground = Brushes.Green;
                                            //    }


                                            //    return;
                                            //}


                                        }//end check if server information collected
                                        else
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "";
                                            vm.WarningMessage1 = "Service Tag is Missing!!!";
                                            vm.CancelScriptS1 = false;
                                        }


                                        //Check Firmware
                                        FirmwareUpdateCheck();

                                        if (vm.WarningMessage1 != "")
                                        {
                                            if (ServerTab != null)
                                                ServerTab.Foreground = Brushes.Red;
                                        }
                                        else
                                        {
                                            if (ServerTab != null)
                                                ServerTab.Foreground = Brushes.Green;
                                        }

                                    }// end check if null check
                                    else
                                    {
                                        vm.ProgressMessage1 = "Please retrieve server information first. Click the Get Server Info Button.";
                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.MessageVisibility1 = Visibility.Visible;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressPercentage1 = 0;

                                    }






                                    break;
                                case "IDRAC":


                                    if (vm.DellServerS1 != null)//vm.Gen9ServerInfo1 != null ||
                                    {

                                        //check if server info was required vm.Gen9ServerInfo.ChassisSerial != string.Empty || 
                                        if (vm.DellServerS1.ServiceTag != string.Empty)
                                        {

                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"IDRACUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //break;


                                            vm.ProgressMessage1 = "Please wait...";
                                            vm.ProgressIsActive1 = true;
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressVisibility1 = Visibility.Visible;

                                            vm.ProgressPercentage1 = 10;




                                            //RACRESET
                                            //await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  racreset hard -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            // await PutTaskDelay(40000);

                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            vm.ProgressMessage1 = "Clearing Jobqueue";
                                            //CLEAR SERVER JOBS
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " jobqueue delete --all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            // await PutTaskDelay(10000);


                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            if (vm.IDRACUpgradePath1 != String.Empty)
                                            {
                                                //vm.ProgressMessage1 = "Shutting Server Down";
                                                ////SHUT DOWN SERVER
                                                //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerdown", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                //await PutTaskDelay(10000);

                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");


                                                // MessageBox.Show("Slipped In");
                                                vm.ProgressPercentage1 = 30;
                                                //ilo  Firmware update
                                                vm.ProgressMessage1 = "iDRAC Firmware Update";
                                                // replace with loop on 
                                                vm.ProgressPercentage1 = 30;
                                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                                //UPDATE METHOD
                                                if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe"))
                                                {
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn update -f " + vm.Server1IPv4 + @"IDRACUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                }
                                                else if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7"))
                                                {
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn fwupdate -p -u -d " + vm.Server1IPv4 + @"IDRACUpdate.d7", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                }
                                                else if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9"))
                                                {
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn fwupdate -p -u -d " + vm.Server1IPv4 + @"IDRACUpdate.d9", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                }


                                                //CHECK NO TRANSFER ERRORS CANCEL SCRIPT IF SO
                                                if (vm.ILORestOutput1.Contains("ERROR:  The Copy Operation Failed.") || vm.ILORestOutput1.Contains("ERROR: Unable to transfer image file") || vm.ILORestOutput1.Contains("ERROR: Remote host is not reachable or connection is interrupted."))
                                                {


                                                    vm.ProgressPercentage1 = 0;
                                                    vm.ProgressIsActive1 = false;
                                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                                    vm.ProgressMessage1 = "Scripts Cancelled, Transfer Error Detected!";
                                                    vm.WarningMessage1 += "Transfer Errors detected, please attempt to run the scripts again.";
                                                    vm.CancelScriptS1 = false;

                                                    vm.ProgressMessage1 = "Clearing Jobqueue";
                                                    //CLEAR SERVER JOBS
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn jobqueue delete --all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                    //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue delete -i JID_CLEARALL_FORCE", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                    await PutTaskDelay(2000);
                                                    return;
                                                }


                                            }


                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            //Power ON Server
                                            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7"))
                                            {

                                            }
                                            else
                                            {

                                                //await PutTaskDelay(10000);
                                                ////BOOT THE SERVER BACK UP
                                                //vm.ProgressMessage1 = "Rebooting Server! Please wait...";
                                                //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " serveraction powerup", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                //await PutTaskDelay(5000);
                                            }




                                            //Check if Script is Cancelled Firmware update completed successfully. The RAC is in the process of resetting.
                                            vm.CancelServerScript("S1");



                                            if (vm.IDRACUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("IDRACUpdate.exe is initiated.") || vm.ILORestOutput1.Contains("IDRACUpdate.d7 is initiated.") || vm.IDRACUpgradePath1.Contains(".d7") || vm.ILORestOutput1.Contains("IDRACUpdate.d9 is initiated.") || vm.IDRACUpgradePath1.Contains(".d9"))
                                            {



                                                // await PutTaskDelay(32000);

                                                vm.ProgressPercentage1 = 60;
                                                vm.ProgressMessage1 = "Applying iDrac Update!";

                                                // Await set amount of time for update to apply
                                                await PutTaskDelay(300000);




                                                vm.ProgressPercentage1 = 65;




                                                //Check Update Signature Hasn't failed
                                                if (vm.ILORestOutput1.Contains("Unable to verify Update Package signature"))
                                                {
                                                    vm.ProgressPercentage1 = 0;
                                                    vm.ProgressIsActive1 = false;
                                                    vm.ProgressVisibility1 = Visibility.Hidden;
                                                    vm.ProgressMessage1 = "";
                                                    vm.WarningMessage1 += "iDrac Package signature failed! Please select an update closer to the current firmware version and try again.";
                                                    vm.CancelScriptS1 = false;
                                                    return;
                                                }


                                                vm.ProgressPercentage1 = 70;

                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");


                                                //NOT REQUIRED AS IDRAC DOESN'T SHOW AS TASK
                                                //IMPORTANT!!!!!!!!
                                                //Wait for any tasks to finish, check every 10 seconds
                                                //await RedfishCrawler.WaitForTasksToFinish(10000, "0", vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);


                                                ////REFRESH DATA
                                                vm.ProgressMessage1 = "Server Inventory!";
                                                vm.ProgressPercentage1 = 95;
                                                //REFRESH DATA
                                                vm.ProgressMessage1 = "Server Inventory Running";

                                                //*** RACADM SERVER INVENTORY ***
                                                //acquire server inventory
                                                //await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory | findstr /i Model", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                vm.DellServerS1 = null;

                                                //CREATE A FRESH ITEM & CLEAR Data Collection
                                                vm.DellServerS1 = new DELLServerInfo();
                                                vm.DellDataCollectionS1.Clear();
                                                ////Capture and map data
                                                vm.DELLCaptureInventoryData(vm.DELLInfoS1, vm.DellDataCollectionS1);
                                                vm.DELLGetServerInventory(vm.DellServerS1, vm.DellDataCollectionS1);
                                                //END RACADM INVENTORY

                                                //*** SERVER INVENTORY ***
                                                //REDFISH SERVER INVENTORY
                                                //vm.DellServerS1 = await RedfishCrawler.GetDellData(vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);
                                                //END REDFISH INVENTORY


                                                vm.ProgressPercentage1 = 95;
                                                //Check for SD Card
                                                await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn vflashsd status", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                                //get lcd
                                                await System.Threading.Tasks.Task.Run(() => pp.StartSensorInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn get System.LCD", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));



                                                //Apply original license license
                                                vm.DellServerS1.iDRACLicense = vm.DellEmbeddedLicense;
                                                vm.DellServerS1.iDracInfo.Model = vm.DellEmbeddedLicense;
                                                vm.DellServerS1.iDracInfo.DeviceDescription = vm.DellEmbeddedLicense;



                                                vm.ProgressPercentage1 = 98;



                                            }
                                            else
                                            {
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressPercentage1 = 100;
                                                vm.ProgressMessage1 = "";


                                                vm.WarningMessage1 += "iDrac Update Failed! Rerun or select another update!";
                                                //set colours
                                                if (vm.WarningMessage1 != "")
                                                {
                                                    if (ServerTab != null)
                                                        ServerTab.Foreground = Brushes.Red;
                                                }
                                                else
                                                {
                                                    if (ServerTab != null)
                                                        ServerTab.Foreground = Brushes.Green;
                                                }


                                            }



                                            //Stop Timer
                                            timer.Stop();
                                            timeTaken = timer.Elapsed;
                                            timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");


                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressPercentage1 = 100;
                                            vm.ProgressMessage1 = "iDRAC Scripts completed successfully! Please launch Virtual Console and check for any POST issues before updating the Bios & Controller! " + timetaken;


                                            //End of log
                                            SyncLog += vm.ILORestOutput1;
                                            SyncLog += "\rTime taken:  " + timeTaken.ToString(@"m\:ss\.fff") + "\r";
                                            SyncLog += "\r[End of Nebula Sync Log]\r\r";




                                        }
                                        else
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "";
                                            vm.WarningMessage1 = "Service Tag is Missing!!!";
                                            vm.CancelScriptS1 = false;
                                        }


                                        //Check Firmware
                                        FirmwareUpdateCheck();
                                        //Set Tab Colour if 
                                        if (vm.WarningMessage1 != "")
                                        {
                                            if (ServerTab != null)
                                                ServerTab.Foreground = Brushes.Red;
                                        }
                                        else
                                        {
                                            if (ServerTab != null)
                                                ServerTab.Foreground = Brushes.Green;
                                        }


                                    }


                                    break;
                                case "Combined Updates":

                                    if (vm.DellServerS1 != null)//vm.Gen9ServerInfo1 != null ||
                                    {

                                        //check if server info was required vm.Gen9ServerInfo.ChassisSerial != string.Empty || 
                                        if (vm.DellServerS1.ServiceTag != string.Empty)
                                        {

                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"IDRACUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //break;


                                            vm.ProgressMessage1 = "Please wait...";
                                            vm.ProgressIsActive1 = true;
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressVisibility1 = Visibility.Visible;

                                            vm.ProgressPercentage1 = 10;




                                            //RACRESET
                                            //await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  racreset hard -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            // await PutTaskDelay(40000);

                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");






                                            vm.ProgressMessage1 = "Clearing Jobqueue";
                                            //CLEAR SERVER JOBS
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn jobqueue delete --all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue delete -i JID_CLEARALL_FORCE", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await PutTaskDelay(2000);

                                            //CLEAR HOSTNAME SERVERNAME
                                            vm.ProgressMessage1 = "Clearing Server Name";

                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " set System.ServerOS.HostName NewServer", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  config  cfgServerInfo.cfgServerName ''", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.ProgressPercentage1 = 10;



                                            //return;


                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            //Check Asset tag, if already changed skip job creation
                                            if (vm.DellServerS1.AssetTag != "NewTag")
                                            {
                                                vm.ProgressMessage1 = "Clearing Asset Tag";
                                                //CLEAR ASSET TAG AND CREATE JOB
                                                // await PutTaskDelay(10000);
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  set BIOS.MiscSettings.AssetTag NewTag", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                //await PutTaskDelay(10000);
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue create BIOS.Setup.1-1", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                await PutTaskDelay(10000);

                                                //Check task view via redfish
                                                if (vm.ILORestOutput1.Contains("Successfully scheduled a job"))
                                                {
                                                    //Increase total count of updates
                                                    updateCount += 1;
                                                    //Wait for any tasks to finish, check every 10 seconds
                                                    await RedfishCrawler.WaitForTasksToFinish(10000, updateCount.ToString(), vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);
                                                }

                                            }


                                            //BIOS & CONTROLLER

                                            vm.ProgressMessage1 = "Shutting Server Down";
                                            //SHUT DOWN SERVER
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn serveraction powerdown -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await PutTaskDelay(5000);
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn serveraction powerdown", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            //CANCEL SCRIPT
                                            if (vm.CancelScriptS1 == true)
                                            {
                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled!";
                                                vm.WarningMessage1 = "";
                                                vm.CancelScriptS1 = false;
                                                return;
                                            }


                                            if (vm.BIOSUpgradePath1 != String.Empty)
                                            {
                                                vm.ProgressPercentage1 = 40;
                                                //BIOS Firmware update
                                                vm.ProgressMessage1 = "BIOS Flash Update";

                                                if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe"))
                                                {
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn update -f " + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                }
                                                else if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.d7"))
                                                {
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn fwupdate -p -u -d " + vm.Server1IPv4 + @"BIOSUpdate.d7", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                }

                                                //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                if (vm.ILORestOutput1.Contains(vm.Server1IPv4 + "BIOSUpdate.exe is initiated."))
                                                {
                                                    //Increase total count of updates
                                                    updateCount += 1;
                                                    //Wait for any tasks to finish, check every 10 seconds
                                                    await RedfishCrawler.WaitForTasksToFinish(10000, updateCount.ToString(), vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);
                                                    if (vm.ILORestOutput1.Contains("ERROR: RAC989: Unable to apply the " + vm.Server1IPv4 + "BIOSUpdate.exe"))
                                                    {
                                                        vm.WarningMessage1 += "BIOS Failed to apply! ";
                                                    }
                                                    else
                                                    {
                                                        //Wait 50 seconds
                                                        await PutTaskDelay(60000);
                                                    }

                                                }


                                            }




                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");
                                            //await PutTaskDelay(10000);

                                            vm.ProgressPercentage1 = 50;

                                            //Embedded Controller

                                            if (vm.CONTUpgradePath1 != String.Empty)
                                            {
                                                vm.ProgressPercentage1 = 40;
                                                //ilo  Firmware update
                                                vm.ProgressMessage1 = "Controller Firmware Update";
                                                // replace with loop on 
                                                vm.ProgressPercentage1 = 40;

                                                if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe"))
                                                {
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn update -f " + vm.Server1IPv4 + @"CONTUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                }

                                                //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                if (vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONTUpdate.exe is initiated."))
                                                {
                                                    //Increase total count of updates
                                                    updateCount += 1;
                                                    //Wait for any tasks to finish, check every 10 seconds
                                                    await RedfishCrawler.WaitForTasksToFinish(10000, updateCount.ToString(), vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);

                                                    if (vm.ILORestOutput1.Contains("ERROR: RAC989: Unable to apply the " + vm.Server1IPv4 + "CONTUpdate.exe"))
                                                    {
                                                        vm.WarningMessage1 += "Controller Failed to apply! ";
                                                    }
                                                    else
                                                    {
                                                        //Wait 90 seconds
                                                        await PutTaskDelay(90000);
                                                    }

                                                }



                                            }


                                            //Additional Controller
                                            if (vm.CONT2UpgradePath1 != String.Empty)
                                            {
                                                vm.ProgressPercentage1 = 40;
                                                //ilo  Firmware update
                                                vm.ProgressMessage1 = "Controller Firmware Update";
                                                // replace with loop on 
                                                vm.ProgressPercentage1 = 40;

                                                if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe"))
                                                {
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn update -f " + vm.Server1IPv4 + @"CONT2Update.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                }

                                                //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                if (vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONT2Update.exe is initiated."))
                                                {
                                                    //Increase total count of updates
                                                    updateCount += 1;
                                                    //Wait for any tasks to finish, check every 10 seconds
                                                    await RedfishCrawler.WaitForTasksToFinish(10000, updateCount.ToString(), vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);

                                                    if (vm.ILORestOutput1.Contains("ERROR: RAC989: Unable to apply the " + vm.Server1IPv4 + "CONT2Update.exe"))
                                                    {
                                                        vm.WarningMessage1 += "Controller2 Failed to apply! ";
                                                    }
                                                    else
                                                    {
                                                        //Wait 90 seconds
                                                        await PutTaskDelay(90000);
                                                    }

                                                }


                                            }

                                            ////CHECK NO TRANSFER ERRORS CANCEL SCRIPT IF SO
                                            if (vm.ILORestOutput1.Contains("ERROR:  The Copy Operation Failed.") || vm.ILORestOutput1.Contains("ERROR: Unable to transfer image file") || vm.ILORestOutput1.Contains("ERROR: Remote host is not reachable or connection is interrupted."))
                                            {


                                                vm.ProgressPercentage1 = 0;
                                                vm.ProgressIsActive1 = false;
                                                vm.ProgressVisibility1 = Visibility.Hidden;
                                                vm.ProgressMessage1 = "Scripts Cancelled, Transfer Error Detected!";
                                                vm.WarningMessage1 = "Transfer Errors detected, please attempt to run the scripts again.";
                                                vm.CancelScriptS1 = false;

                                                vm.ProgressMessage1 = "Clearing Jobqueue";
                                                //CLEAR SERVER JOBS
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn jobqueue delete --all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  jobqueue delete -i JID_CLEARALL_FORCE", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                await PutTaskDelay(2000);
                                                return;
                                            }



                                            if (vm.IDRACUpgradePath1 != String.Empty)
                                            {


                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");


                                                // MessageBox.Show("Slipped In");
                                                vm.ProgressPercentage1 = 50;
                                                //ilo  Firmware update
                                                vm.ProgressMessage1 = "iDRAC Firmware Update";
                                                // replace with loop on 
                                                vm.ProgressPercentage1 = 50;
                                                //await System.Threading.Tasks.Task.Run(() => pp.StartILORest1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"", @"-f " + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "_iLO_FirmwareUpdate1.xml -s " + vm.Server1IPv4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL"));
                                                //UPDATE METHOD
                                                if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe"))
                                                {
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  update -f " + vm.Server1IPv4 + @"IDRACUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                }
                                                else if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7"))
                                                {
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  fwupdate -p -u -d " + vm.Server1IPv4 + @"IDRACUpdate.d7", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                }
                                                else if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9"))
                                                {
                                                    await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  fwupdate -p -u -d " + vm.Server1IPv4 + @"IDRACUpdate.d9", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                }

                                                await PutTaskDelay(10000);



                                            }
                                            else
                                            {
                                            }


                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");


                                            if (vm.IDRACUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("IDRACUpdate.exe is initiated.") || vm.ILORestOutput1.Contains("IDRACUpdate.d7 is initiated.") || vm.IDRACUpgradePath1.Contains(".d7") || vm.ILORestOutput1.Contains("IDRACUpdate.d9 is initiated.") || vm.IDRACUpgradePath1.Contains(".d9"))
                                            {



                                                // await PutTaskDelay(32000);

                                                vm.ProgressPercentage1 = 60;
                                                vm.ProgressMessage1 = "Applying iDrac Update!";
                                                //Wait for RAC Reset //6 mins
                                                if (vm.DellServerS1.SystemGeneration.Contains("14G"))
                                                {
                                                    if (vm.ILORestOutput1.Contains("ERROR: RAC989: Unable to apply the " + vm.Server1IPv4 + "IDRACUpdate.exe"))
                                                    {
                                                        vm.WarningMessage1 += "iDRAC Failed to apply! ";
                                                    }
                                                    else
                                                    {

                                                        //Wait for racreset to complete
                                                        await PutTaskDelay(300000);
                                                    }

                                                }
                                                else
                                                {
                                                    if (vm.ILORestOutput1.Contains("ERROR: RAC989: Unable to apply the " + vm.Server1IPv4 + "IDRACUpdate.exe"))
                                                    {
                                                        vm.WarningMessage1 += "iDRAC Failed to apply! ";
                                                    }
                                                    else
                                                    {
                                                        //Wait for racreset to complete
                                                        await PutTaskDelay(300000);
                                                    }

                                                }


                                                //Check if Script is Cancelled
                                                vm.CancelServerScript("S1");


                                            }

                                            await PutTaskDelay(10000);
                                            //BOOT THE SERVER BACK UP
                                            vm.ProgressMessage1 = "Rebooting Server! Please wait...";
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " serveraction powerup", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await PutTaskDelay(50000);


                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            //IMPORTANT!!!!!!!!
                                            //Wait for any tasks to finish, check every 10 seconds
                                            await RedfishCrawler.WaitForTasksToFinish(10000, "0", vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);





                                            ////CHECK IF BIOS OR CONTROLLER FAILED

                                            //if (vm.BIOSUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("Job Name=Firmware Update: BIOS") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "BIOSUpdate.exe is initiated.") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "BIOSUpdate.d7 is initiated."))
                                            //{
                                            //    //ok
                                            //    //BIOS
                                            //    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe"))
                                            //    {
                                            //        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe");
                                            //    }

                                            //    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.d7"))
                                            //    {
                                            //        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.d7");
                                            //    }


                                            //    vm.UpdatesRun += @" [BIOS] ";

                                            //    //Check Update Signature Hasn't failed
                                            //    if (vm.ILORestOutput1.Contains("Unable to verify Update Package signature"))
                                            //    {
                                            //        vm.ProgressPercentage1 = 0;
                                            //        vm.ProgressIsActive1 = false;
                                            //        vm.ProgressVisibility1 = Visibility.Hidden;
                                            //        vm.ProgressMessage1 = "";
                                            //        vm.WarningMessage1 = "BIOS Package signature failed! Please select an update closer to the current firmware version and try again.";
                                            //        vm.CancelScriptS1 = false;
                                            //        return;
                                            //    }

                                            //}
                                            //else if (vm.BIOSUpgradePath1 != String.Empty)
                                            //{
                                            //    vm.WarningMessage1 += "Bios Update Failed! ";

                                            //    //set colours
                                            //    if (vm.WarningMessage1 != "")
                                            //    {
                                            //        if (ServerTab != null)
                                            //            ServerTab.Foreground = Brushes.Red;
                                            //    }
                                            //    else
                                            //    {
                                            //        if (ServerTab != null)
                                            //            ServerTab.Foreground = Brushes.Green;
                                            //    }

                                            //    return;
                                            //}




                                            //if (vm.CONTUpgradePath1 != String.Empty && vm.ILORestOutput1.Contains("Job Name=Firmware Update: SAS RAID") || vm.ILORestOutput1.Contains("Job Name=Firmware Update: RAID") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONTUpdate.exe is initiated.") || vm.ILORestOutput1.Contains(vm.Server1IPv4 + "CONT2Update.exe is initiated."))
                                            //{
                                            //    //ok
                                            //    //CONTROLLER  192.168.101.159CONTUpdate.exe is initiated
                                            //    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe"))
                                            //    {
                                            //        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe");
                                            //    }

                                            //    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe"))
                                            //    {
                                            //        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe");
                                            //    }


                                            //    vm.UpdatesRun += @" [Controller] ";
                                            //    //Check Update Signature Hasn't failed
                                            //    if (vm.ILORestOutput1.Contains("Unable to verify Update Package signature"))
                                            //    {
                                            //        vm.ProgressPercentage1 = 0;
                                            //        vm.ProgressIsActive1 = false;
                                            //        vm.ProgressVisibility1 = Visibility.Hidden;
                                            //        vm.ProgressMessage1 = "";
                                            //        vm.WarningMessage1 += "Controller Package signature failed! Please select an update closer to the current firmware version and try again.";
                                            //        vm.CancelScriptS1 = false;
                                            //        return;
                                            //    }

                                            //}
                                            //else if (vm.CONTUpgradePath1 != String.Empty)
                                            //{
                                            //    //vm.WarningMessage1 = vm.WarningMessage1 + " Controller Update Failed! ";

                                            //    //set colours
                                            //    if (vm.WarningMessage1 != "")
                                            //    {
                                            //        if (ServerTab != null)
                                            //            ServerTab.Foreground = Brushes.Red;
                                            //    }
                                            //    else
                                            //    {
                                            //        if (ServerTab != null)
                                            //            ServerTab.Foreground = Brushes.Green;
                                            //    }


                                            //    return;
                                            //}



                                            vm.ProgressPercentage1 = 65;
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "   jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await PutTaskDelay(480000);
                                            vm.ProgressPercentage1 = 70;
                                            // vm.ProgressMessage1 = "Rebooting Server";
                                            //reset server to kickstart bios controller installs
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction hardreset", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await PutTaskDelay(120000);



                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            ////REFRESH DATA

                                            vm.ProgressPercentage1 = 95;
                                            //REFRESH DATA
                                            vm.ProgressMessage1 = "Server Inventory Running";

                                            //*** RACADM SERVER INVENTORY ***
                                            //acquire server inventory
                                            //await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory | findstr /i Model", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await System.Threading.Tasks.Task.Run(() => pp.CollectDellHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn hwinventory", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            vm.DellServerS1 = null;

                                            //CREATE A FRESH ITEM & CLEAR Data Collection
                                            vm.DellServerS1 = new DELLServerInfo();
                                            vm.DellDataCollectionS1.Clear();
                                            ////Capture and map data
                                            vm.DELLCaptureInventoryData(vm.DELLInfoS1, vm.DellDataCollectionS1);
                                            vm.DELLGetServerInventory(vm.DellServerS1, vm.DellDataCollectionS1);
                                            //END RACADM INVENTORY

                                            //*** SERVER INVENTORY ***
                                            //REDFISH SERVER INVENTORY
                                            //vm.DellServerS1 = await RedfishCrawler.GetDellData(vm.Server1IPv4, vm.OverridePasswordS1, vm, ServerGen);
                                            //END REDFISH INVENTORY
                                            vm.ProgressPercentage1 = 95;
                                            //Check for SD Card
                                            await System.Threading.Tasks.Task.Run(() => pp.CollectHardwareInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn vflashsd status", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                            //get lcd
                                            await System.Threading.Tasks.Task.Run(() => pp.StartSensorInventoryRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn get System.LCD", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                            //Apply original license license
                                            vm.DellServerS1.iDRACLicense = vm.DellEmbeddedLicense;
                                            vm.DellServerS1.iDracInfo.Model = vm.DellEmbeddedLicense;
                                            vm.DellServerS1.iDracInfo.DeviceDescription = vm.DellEmbeddedLicense;


                                            //Stop Timer
                                            timer.Stop();
                                            timeTaken = timer.Elapsed;
                                            timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                            //End of log
                                            SyncLog += vm.ILORestOutput1;
                                            SyncLog += "\rTime taken:  " + timeTaken.ToString(@"m\:ss\.fff") + "\r";
                                            SyncLog += "\r[End of Nebula Sync Log]\r\r";

                                            vm.ProgressPercentage1 = 98;

                                            // if(vm.)

                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressPercentage1 = 100;
                                            vm.ProgressMessage1 = "All Updates completed! Please check the firmware levels. Run Get Server Info if properties have not updated. " + timetaken;

                                        }
                                        else
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "";
                                            vm.WarningMessage1 = "Service Tag is Missing!!!";
                                            vm.CancelScriptS1 = false;
                                        }

                                        //Check Firmware
                                        FirmwareUpdateCheck();

                                        if (vm.WarningMessage1 != "")
                                        {
                                            if (ServerTab != null)
                                                ServerTab.Foreground = Brushes.Red;
                                        }
                                        else
                                        {
                                            if (ServerTab != null)
                                                ServerTab.Foreground = Brushes.Green;
                                        }
                                    }
                                    //                                  Status = The license has expired.

                                    //Transaction ID = 2


                                    //          License Description = iDRAC8 Enterprise Evaluation License


                                    break;
                                case "DeleteLicense":

                                    if (vm.DellServerS1.LicensingRollupStatus == "Degraded")
                                    {
                                        //MessageBox.Show("About to delete license"); 

                                        //CLEAR SERVER JOBS
                                        // await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " license delete -e " + vm.DellEvaluationID + "", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                        await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " license delete -t " + vm.DellEvaluationID + "", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                        //Hide Button
                                        vm.DeleteDellLicenseBTN = Visibility.Hidden;

                                        vm.ProgressMessage1 = "License removed, running Get Server Info! Please wait...";
                                        vm.WarningMessage1 = "";

                                        await PutTaskDelay(5000);

                                        vm.RunRACDMScriptsCommandDellPES1.Execute(new String[] { "ServerInfo", "Dell" });

                                    }


                                    break;
                                case "FinalScripts":
                                    if (vm.DellServerS1 != null)
                                        if (vm.DellServerS1.ServiceTag != string.Empty)
                                        {
                                            //////EXTRACT FIRMWARE JSON
                                            vm.ProgressMessage1 = "Please wait...";
                                            vm.ProgressIsActive1 = true;
                                            vm.ProgressPercentage1 = 0;
                                            vm.MessageVisibility1 = Visibility.Visible;
                                            vm.ProgressVisibility1 = Visibility.Visible;
                                            vm.ProgressPercentage1 = 0;



                                            vm.ProgressMessage1 = "Clearing Jobqueue";
                                            //CLEAR SERVER JOBS
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn jobqueue delete --all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            // await PutTaskDelay(10000);



                                            vm.ProgressPercentage1 = 20;

                                            vm.ProgressMessage1 = "Shutting Server Down";
                                            //SHUT DOWN SERVER
                                            //Needed for some systems
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn serveraction powerdown -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            await PutTaskDelay(10000);
                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn serveraction powerdown", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            //RAC RESET CONFIG
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  racresetcfg -all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));systemerase secureerasepd,vflash,percnvcache
                                            //SYSTEM ERASE INCLUDES BIOS\LCLOG\RACRESETCONFIG

                                            //IF SYSTEM OEM THEN ATTEMPT OLD METHOD
                                            if (vm.DellServerS1.Manufacturer == "OEM")
                                            {
                                                //SYSTEM ERASE
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn systemerase bios,idrac,lcdata", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                                //TO RESET SYSTEM
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn racresetcfg -all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }
                                            else
                                            {
                                                //SYSTEM ERASE
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn systemerase bios,idrac,lcdata", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            }



                                            //LICENSE CHECK
                                            vm.ProgressMessage1 = "Checking License, Please wait...";

                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn license view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                            //To Place in Factory Defaults Capture 
                                            if (vm.ILORestOutput1.Contains("Transaction ID       =") && vm.ILORestOutput1.Contains("License Type         = EVALUATION"))
                                            {
                                                vm.ProgressMessage1 = "Removing Evaluation License, Please wait...";

                                                //Capture ID
                                                string tranID = vm.ILORestOutput1.Substring(vm.ILORestOutput1.IndexOf("Transaction ID       ="), 27).Replace("Transaction ID       =", "").Trim();

                                                //Remove Eval License
                                                await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + " --nocertwarn license delete -t" + tranID, @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                                                //Reset Flag if true
                                                vm.DellBasicLicS1 = false;
                                            }
                                            //LICENSE END



                                            //racadm systemerase <component>,<component>,<component>
                                            // <component>—the valid types of components are:
                                            //○ bios—To reset the BIOS to default.
                                            //○ diag—To erase embedded diagnostics. 
                                            //○ drvpack—To erase embedded OS driver pack.
                                            //○ idrac—To reset the iDRAC to default. 
                                            //○ lcdata—To erase Lifecycle Controller data. 
                                            //○ allaps—To reset all apps.
                                            //○ secureerasepd—To erase the physical disk.This supports SED, NVMe drives, and PCIe cards
                                            //○ overwritepd—To overwrite physical disk. This supports SAS and SATA drives.
                                            //○ percnvcache—To erase NV cache. ○ vflash—To erase vFlash. 
                                            //○ nvdimm—To erase all NonVolatileMemory. 

                                            //Check if Script is Cancelled
                                            vm.CancelServerScript("S1");

                                            //FOR ILO RESTFUL FACTORY DEFAULTS
                                            vm.ProgressMessage1 = "Resetting iDrac to Factory Defaults";
                                            vm.ProgressPercentage1 = 50;
                                            //Await alloted time for reset to finish
                                            await PutTaskDelay(280000);
                                            vm.ProgressPercentage1 = 80;
                                            await PutTaskDelay(250000);
                                            vm.ProgressPercentage1 = 100;


                                            //check job queue
                                            vm.ProgressMessage1 = "Checking Job Queue";



                                            await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  --nocertwarn jobqueue view", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                            if (vm.ILORestOutput1.Contains("SystemErase operation initiated successfully."))
                                            {

                                                //if(vm.ILORestOutput1.Contains("Job Name=System_Erase") && vm.ILORestOutput1.Contains("Status=Failed"))
                                                //{
                                                //    //RAC RESET CONFIG
                                                //    //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  racresetcfg -all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm")); //systemerase secureerasepd,vflash,percnvcache


                                                //    vm.WarningMessage1 = "Factory Reset failed! If the Server is OEM it could contain a custom BIOS. Log into the iDrac and roll back the BIOS version. Update firmware, then run the Factory Reset again. If this fails again, log into the Servers Webpage and run the Factory Reset from there.";
                                                //}
                                                //Add CSV WRITE HERE Instead of 
                                                // StaticFunctions.CreateAppendCSV(new ReportServersProcessed(vm.CurrentUser, vm.PurchaseOrderNumber, vm.ChassisSerialNo, "Goods In", "OK", ""), @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed.csv");
                                            }
                                            else
                                            {
                                                if (vm.ILORestOutput1.Contains("Job Name=System_Erase") && vm.ILORestOutput1.Contains("Status=Failed"))
                                                {
                                                    //RAC RESET CONFIG
                                                    //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  racresetcfg -all", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm")); //systemerase secureerasepd,vflash,percnvcache


                                                    vm.WarningMessage1 = "Factory Reset failed! If the Server is OEM it could contain a custom BIOS. Log into the iDrac and roll back the BIOS version. If this fails again, log into the Servers Webpage and run the Factory Reset from there.";
                                                }

                                                //vm.WarningMessage1 = "Factory Reset failed! If the Server is OEM it could contain a custom BIOS. Log into the iDrac and roll back the BIOS version. Update firmware, then run the Factory Reset again. If this fails again, log into the Servers Webpage and run the Factory Reset from there.";
                                            }

                                            //SHUT DOWN SERVER
                                            //await PutTaskDelay(10000);
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerdown -f", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));
                                            //await PutTaskDelay(10000);
                                            //await System.Threading.Tasks.Task.Run(() => pp.StartRACDM1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p " + vm.OverridePasswordS1 + "  serveraction powerdown", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));


                                            //Stop Timer
                                            timer.Stop();
                                            timeTaken = timer.Elapsed;
                                            timetaken = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");

                                            //End of log
                                            SyncLog += vm.ILORestOutput1;
                                            SyncLog += "\rTime taken:  " + timeTaken.ToString(@"m\:ss\.fff") + "\r";
                                            SyncLog += "\r[End of Nebula Sync Log]\r\r";

                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.MessageVisibility1 = Visibility.Visible;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressMessage1 = "Scripts completed successfully! " + timetaken;
                                            vm.Server1TabHeader = "Server1";

                                            //Set Has System Erase Been Run Flag
                                            vm.WasFactoryReset = "Yes";
                                            vm.DellEvalInstalledS1 = false;
                                        }
                                        else
                                        {
                                            vm.ProgressPercentage1 = 0;
                                            vm.ProgressIsActive1 = false;
                                            vm.ProgressVisibility1 = Visibility.Hidden;
                                            vm.ProgressMessage1 = "";
                                            vm.WarningMessage1 = "Service Tag is Missing!!!";
                                            vm.CancelScriptS1 = false;
                                        }
                                    else
                                    {

                                        vm.ProgressVisibility1 = Visibility.Hidden;
                                        vm.MessageVisibility1 = Visibility.Visible;
                                        vm.ProgressIsActive1 = false;
                                        vm.ProgressPercentage1 = 0;
                                        vm.WarningMessage1 = "Board Serial appears to be missing.";

                                    }

                                    //Reset license properties
                                    vm.GetServerInfoInitialRun = false;
                                    vm.DellEmbeddedLicense = "";

                                    break;
                            }



                        }//end if to check server ip is not empty
                        else
                        {
                            //reset progress ring to hidden
                            vm.ProgressVisibility1 = Visibility.Hidden;
                            vm.MessageVisibility1 = Visibility.Visible;
                            vm.ProgressIsActive1 = false;
                            vm.ProgressPercentage1 = 0;
                            vm.WarningMessage1 = "Cannot contact server with Ip: " + vm.Server1IPv4 + ", Please check the Server has power.";
                            vm.IsServerThere1 = "No";
                        }

                    }// check if server there
                    else
                    {
                        //vm.ProgressMessage = "Cannot contact server with Ip: " + vm.ServerIPv4 + ", Exited Function";
                        vm.ProgressMessage1 = "Please Enter A Server IP in the top box!";
                    }

                }//end if to check vm is null

                //Clear data files
                // ClearXMLJSONFiles();





                //Set script running property to false
                vm.IsScriptRunningS1 = false;


                //WRITE LOG
                if (Directory.Exists(@"" + vm.myDocs + @"\Nebula Logs"))
                {


                    //Write out log
                    if (vm.DellServerS1 != null)
                    {
                        if (vm.DellServerS1.ServiceTag != null)
                            File.WriteAllText(@"" + vm.myDocs + @"\Nebula Logs\Nebula_Server_Log_" + vm.DellServerS1.ServiceTag + " " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "_") + ".txt", SyncLog);
                    }

                }

                //set colours
                if (string.IsNullOrEmpty(vm.WarningMessage1))
                {
                    if (ServerTab != null)
                    {
                        if (string.IsNullOrEmpty(vm.Server1IPv4))
                        {
                            ServerTab.Foreground = Brushes.MediumPurple;
                        }
                        else
                        {
                            ServerTab.Foreground = Brushes.Green;
                        }

                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;




                        if (!string.IsNullOrEmpty(vm.WarningMessage1))
                        {
                            ServerTab.Foreground = Brushes.Red;
                        }

                    }
                }




            }
            catch (Exception ex)
            {

                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (string.IsNullOrEmpty(vm.WarningMessage1))
                {
                    if (ServerTab != null)
                    {
                        if (string.IsNullOrEmpty(vm.Server1IPv4))
                        {
                            ServerTab.Foreground = Brushes.MediumPurple;
                        }
                        else
                        {
                            ServerTab.Foreground = Brushes.Green;
                        }

                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;




                        if (!string.IsNullOrEmpty(vm.WarningMessage1))
                        {
                            ServerTab.Foreground = Brushes.Red;
                        }

                    }
                }
                return;
            }







        }//end execute

        //Check that the updates selected have been applied 
        public void FirmwareUpdateCheck()
        {
            string bios = vm.BIOSUpgradePath1?.Replace(".EXE", "").Replace(".exe", "");

            string cpld = vm.CPLDUpgradePath1?.Replace(".EXE", "").Replace(".exe", "");

            string controller1 = vm.CONTUpgradePath1?.Replace(".EXE", "").Replace(".exe", "");

            string controller2 = vm.CONT2UpgradePath1?.Replace(".EXE", "").Replace(".exe", "");

            string idrac = vm.IDRACUpgradePath1?.Replace(".d7", "").Replace(".d8", "").Replace(".d9", "").Replace(".d10", "");

            vm.WarningMessage1 = "";

            //vm.DellServerS1 = new DELLServerInfo();
            //vm.DellServerS1.ControllerVersion = "25.5.9.0001";

            //bios
            if (!string.IsNullOrEmpty(bios) && !string.IsNullOrEmpty(vm.DellServerS1.CPLDVersion))
            {
                var biosArray = bios.Split('-');
                bool biosMatch = false;

                //Loop through array and find a match
                foreach (var arrayStr in biosArray)
                {
                    Console.WriteLine(arrayStr);
                    if (vm.DellServerS1 != null)
                        if (vm.DellServerS1.BiosCurrentVersion == arrayStr)
                        {
                            //As soon as a match is found exit
                            biosMatch = true;
                            break;

                        }
                }
                //Check if match was found
                if (biosMatch == true)
                {
                    //Clear Update
                    vm.BIOSUpgradePath1 = String.Empty;

                    //CLear Flash fie
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe");
                    }

                    vm.UpdatesRun += @" [BIOS] ";
                }
                else
                {
                    vm.WarningMessage1 += "[Firmware " + vm.BIOSUpgradePath1 + " failed! Please rerun the update.]";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.BIOSUpgradePath1 + " failed to apply!!! Please rerun the update.");
                }


            }
            else
            {
                if (vm.DellServerS1 != null)
                    if (string.IsNullOrEmpty(bios) && !string.IsNullOrEmpty(vm.DellServerS1.BiosCurrentVersion))
                    {

                    }
                    else
                    {
                        //vm.WarningMessage1 += "[Blank Server Field detected for Bios update.]";
                    }

            }


            //cpld
            if (vm.DellServerS1 != null)
                if (!string.IsNullOrEmpty(cpld) && !string.IsNullOrEmpty(vm.DellServerS1.CPLDVersion))
                {
                    var cpldArray = cpld.Split('-');
                    bool cpldMatch = false;
                    //Loop through array and find a match
                    foreach (var arrayStr in cpldArray)
                    {
                        Console.WriteLine(arrayStr);
                        if (vm.DellServerS1 != null)
                            if (vm.DellServerS1.CPLDVersion == arrayStr)
                            {
                                //As soon as a match is found exit
                                cpldMatch = true;
                                break;

                            }
                    }
                    //Check if match was found
                    if (cpldMatch == true)
                    {
                        //Clear Update
                        vm.CPLDUpgradePath1 = String.Empty;

                        //CLear Flash fie
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CPLDUpdate.exe"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CPLDUpdate.exe");
                        }

                        vm.UpdatesRun += @" [CPLD] ";
                    }
                    else
                    {
                        vm.WarningMessage1 += "[Firmware " + vm.CPLDUpgradePath1 + " failed! Please rerun the update.]";
                        //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.BIOSUpgradePath1 + " failed to apply!!! Please rerun the update.");
                    }


                }
                else
                {
                    if (vm.DellServerS1 != null)
                        if (string.IsNullOrEmpty(cpld) && !string.IsNullOrEmpty(vm.DellServerS1.CPLDVersion))
                        {

                        }
                        else
                        {
                            //vm.WarningMessage1 += "[Blank Server Field detected for CPLD update.]";
                        }


                }

            //Controller 1
            if (vm.DellServerS1 != null)
                if (!string.IsNullOrEmpty(controller1) && !string.IsNullOrEmpty(vm.DellServerS1.ControllerVersion))
                {
                    //Split update into array
                    var cont1Array = controller1.Split('-');
                    bool cont1Match = false;
                    //Loop through array and find a match
                    foreach (var arrayStr in cont1Array)
                    {

                        Console.WriteLine(arrayStr);
                        if (vm.DellServerS1 != null)
                            if (vm.DellServerS1.ControllerVersion.Contains(arrayStr))
                            {
                                //As soon as a match is found exit
                                cont1Match = true;
                                break;
                            }//Check the other way round
                            else if (arrayStr.Contains(vm.DellServerS1.ControllerVersion))
                            {
                                //As soon as a match is found exit
                                cont1Match = true;
                                break;
                            }

                    }

                    //Check if match was found
                    if (cont1Match == true)
                    {
                        //Clear Update 
                        vm.CONTUpgradePath1 = String.Empty;

                        //CLear Flash fie
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe");
                        }

                        vm.UpdatesRun += @" [CONTROLLER] ";
                    }
                    else
                    {
                        vm.WarningMessage1 += "[Firmware " + vm.CONTUpgradePath1 + " failed! Please rerun the update.]";
                        //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.CONTUpgradePath1 + " failed to apply!!! Please rerun the update.");
                    }

                }
                else
                {
                    if (vm.DellServerS1 != null)
                        if (string.IsNullOrEmpty(controller2) && !string.IsNullOrEmpty(vm.DellServerS1.ControllerVersion))
                        {

                        }
                        else
                        {
                            //vm.WarningMessage1 += "[Blank Server Field detected for Integrated Controller update.]";
                        }

                }
            //Controller 2
            if (vm.DellServerS1 != null)
                if (!string.IsNullOrEmpty(controller2) && !string.IsNullOrEmpty(vm.DellServerS1.ControllerVersion2))
                {
                    //Split update into array
                    var cont2Array = controller2.Split('-');
                    bool cont2Match = false;
                    //Loop through array and find a match
                    foreach (var arrayStr in cont2Array)
                    {
                        Console.WriteLine(arrayStr);

                        if (vm.DellServerS1 != null)
                            if (vm.DellServerS1.ControllerVersion2.Contains(arrayStr))
                            {
                                //As soon as a match is found exit
                                cont2Match = true;
                                break;
                            }//Check the other way round
                            else if (arrayStr.Contains(vm.DellServerS1.ControllerVersion2))
                            {
                                //As soon as a match is found exit
                                cont2Match = true;
                                break;
                            }



                    }

                    //Check if match was found
                    if (cont2Match == true)
                    {
                        //Clear Update 
                        vm.CONT2UpgradePath1 = String.Empty;

                        //CLear Flash fie
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe");
                        }
                        //Update Certificate Field
                        vm.UpdatesRun += @" [CONTROLLER] ";
                    }
                    else
                    {
                        vm.WarningMessage1 += "[Firmware " + vm.CONT2UpgradePath1 + " failed! Please rerun the update.]";
                        //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.CONT2UpgradePath1 + " failed to apply!!! Please rerun the update.");
                    }
                }
                else
                {
                    if (vm.DellServerS1 != null)
                        if (string.IsNullOrEmpty(controller2) && !string.IsNullOrEmpty(vm.DellServerS1.ControllerVersion2))
                        {

                        }
                        else
                        {
                            //vm.WarningMessage1 += "[Blank Server Field detected for Additional Controller update.]";
                        }

                }

            //Idrac
            if (vm.DellServerS1 != null)
                if (!string.IsNullOrEmpty(idrac) && !string.IsNullOrEmpty(vm.DellServerS1.iDRACVersion))
                {
                    var idracArray = idrac.Split('-');
                    bool idracMatch = false;
                    //Loop through array and find a match
                    foreach (var arrayStr in idracArray)
                    {
                        Console.WriteLine(arrayStr);
                        if (vm.DellServerS1 != null)
                            if (vm.DellServerS1.iDRACVersion == arrayStr)
                            {
                                //As soon as a match is found exit
                                idracMatch = true;
                                break;

                            }
                    }

                    //Check if match was found
                    if (idracMatch == true)
                    {


                        //CLEAR Flash File INFORMATION
                        vm.IDRACUpgradePath1 = String.Empty;

                        //IDRAC
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe");
                        }

                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7");
                        }

                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9");
                        }
                        //for test certificate
                        vm.UpdatesRun += @" [IDRAC] ";
                        //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.IDRACUpgradePath1 + " has updated successfully!");
                    }
                    else
                    {
                        vm.WarningMessage1 += "[Firmware " + vm.IDRACUpgradePath1 + " failed! Please rerun the update.]";
                        //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.IDRACUpgradePath1 + " failed to apply!!! Please rerun the update.");
                    }
                }
                else
                {
                    if (vm.DellServerS1 != null)
                        if (string.IsNullOrEmpty(idrac) && !string.IsNullOrEmpty(vm.DellServerS1.iDRACVersion))
                        {

                        }
                        else
                        {
                            //vm.WarningMessage1 += "[Blank Server Field detected for iDrac update.]";
                        }

                }

        }



        public async void sendAsyncPingPacket(string hostToPing)
        {
            try
            {
                // int timeout = 5000;
                // AutoResetEvent waiter = new AutoResetEvent(false);
                Ping pingPacket = new Ping();
                //ping completion event reaised
                pingPacket.PingCompleted += new PingCompletedEventHandler(PingCompletedCallback);
                string data = "Ping test check";
                byte[] byteBuffer = Encoding.ASCII.GetBytes(data);
                PingOptions pingOptions = new PingOptions(64, true);

                await pingPacket.SendPingAsync(hostToPing, 3000);
                //pingPacket.Send(hostToPing, 3000);

            }
            catch (PingException pe)
            {
                string exception = pe.Message;
                //IPResults.Add("INVALID IP ADDRESS FOUND");
            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                //IPResults.Add("Exceptin " + ex.Message);
            }

        }

        //Gets the embedded license from the webpage
        private async Task GetEmbeddedLicense()
        {
            //CAPTURE EMBEDDED LICENSE
            //ADD PUPPETEER CODE HERE TO CAPTURE LICENSE FROM WEBPAGE
            //PUPPETEER HEADLESS BROWSER

            string fullUrl = "";
            //embedded license for gen 12 & 13 capture using puppeteer headless browser /login.html
            if (vm.ILORestOutput1.Contains("iDRAC9"))
            {
                vm.DellGeneration = "iDRAC9";
                fullUrl = "https://" + vm.Server1IPv4 + "/restgui/start.html";
            }
            else
            {
                vm.DellGeneration = "iDRAC78";
                //idrac 7 & 8
                fullUrl = "https://" + vm.Server1IPv4 + "/login.html";
            }


            List<string> programmerLinks = new List<string>();

            var options = new LaunchOptions()
            {
                Headless = true, // set to true to hide browser ?console
                IgnoreHTTPSErrors = true,
                Args = new[] { "--disable-extensions", "--disable-popups", "--disable-web-security", "--disable-features=IsolateOrigins,site-per-process", },
                ExecutablePath = @"C:\Program Files\Google\Chrome\Application\chrome.exe"

            };


            var browser = await Puppeteer.LaunchAsync(options);
            var page = await browser.NewPageAsync();
            await page.GoToAsync(fullUrl);

            await PutTaskDelay(5000);

            string content = await page.GetContentAsync();




            // var innerText = await page.EvaluateExpressionAsync<string>($"document.querySelector({"'div'"}).getAttribute('prodClassName')");
            //var pageHeaderHandle = await page.WaitForSelectorAsync("div");
            //var innerTextHandle = await pageHeaderHandle.GetPropertyAsync("'prodClassName");
            //var innerText = await innerTextHandle.JsonValueAsync();





            // Console.WriteLine(content);
            if (vm.ILORestOutput1.Contains("iDRAC9"))
            {
                //Select from content
                switch (content)
                {

                    //GEN 14
                    case var s when content.Contains(">Basic"):
                        vm.ILORestOutput1 += "Embedded License Model = Basic\n\n";
                        vm.DellEmbeddedLicense = "Basic";
                        break;
                    case var s when content.Contains(">Express"):
                        vm.ILORestOutput1 += "Embedded License Model = Express\n\n";
                        vm.DellEmbeddedLicense = "Express";
                        break;
                    case var s when content.Contains(">Enterprise"):
                        vm.ILORestOutput1 += "Embedded License Model = Enterprise\n\n";
                        vm.DellEmbeddedLicense = "Enterprise";
                        break;
                }
            }
            else
            {
                //Select from content
                switch (content)
                {
                    //GEN 12, 13
                    case var s when content.Contains("prodClassName\">Basic"):
                        vm.ILORestOutput1 += "Embedded License Model = Basic\n\n";
                        vm.DellEmbeddedLicense = "Basic";
                        break;
                    case var s when content.Contains("prodClassName\">Express"):
                        vm.ILORestOutput1 += "Embedded License Model = Express\n\n";
                        vm.DellEmbeddedLicense = "Express";
                        break;
                    case var s when content.Contains("prodClassName\">Enterprise"):
                        vm.ILORestOutput1 += "Embedded License Model = Enterprise\n\n";
                        vm.DellEmbeddedLicense = "Enterprise";
                        break;
                }
            }





            //Grab a screenshot of the page
            //await page.ScreenshotAsync(@"C:\Users\n.myers\Documents\DellScreenshot.png");

            //End of embedded license capture
        }


        private void PingCompletedCallback(object sender, PingCompletedEventArgs e)
        {
            try
            {
                if (e.Cancelled)
                {

                }

                // If an error occurred, display the exception to the user.
                if (e.Error != null)
                {

                }

                PingReply reply = e.Reply;

                if (reply == null)
                {
                    vm.IsServerThere1 = "No";
                    return;
                }
                else if (reply.Status == IPStatus.Success)
                {
                    vm.IsServerThere1 = "Yes";
                    return;

                }
            }
            catch (PingException pe)
            {
                Console.WriteLine(pe.Message);
                //IPResults.Add("INVALID IP ADDRESS");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                // MessageBox.Show("Came Here");
                //IPResults.Add("Exception " + ex.Message);
            }
        }

        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }








    }
}

