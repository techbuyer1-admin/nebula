﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Nebula.Helpers;
using Nebula.ViewModels;
using Nebula.Views;

namespace Nebula.Commands
{
    public class SearchCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public SearchCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if (vm != null)
            {
                //check if valid ip entered
                //System.Net.IPAddress ipAddress = null;
                //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                if (vm.SalesOrderFolderThere != string.Empty || vm.SalesOrderNumber != string.Empty)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
         
          
        }

        public async void Execute(object parameter)
        {

            var values = (object)parameter;

            

            switch (values.ToString())
            {
                case "Files":
                    break;

                case "GetFiles":
                    break;

                case "SearchFolders":

                    vm.ProgressIsActive = true;
                    vm.ProgressMessage = "Searching, please wait...";
                    vm.MessageVisibility = Visibility.Visible;
                    vm.ProgressVisibility = Visibility.Visible;

                    await PutTaskDelay(5000);



                    await System.Threading.Tasks.Task.Run(() => SearchForFolders(""));




                    //reset progress items
                    vm.ProgressMessage = "";
                    vm.ProgressIsActive = false;
                    vm.ProgressVisibility = Visibility.Hidden;
                    vm.MessageVisibility = Visibility.Hidden;
                    //string[] dirs = Directory.GetDirectories(@"W:\Goods Out\TGO Photos\2020\06_June", vm.SalesOrderNumber, SearchOption.AllDirectories);
                    ////Console.WriteLine("The number of directories starting with p is {0}.", dirs.Length);
                    //foreach (string dir in dirs)
                    //{
                    //    //if(dir)
                    //    //MessageBox.Show(dir);
                    //    vm.SalesOrderFolderThere = dir;
                    //}


                    break;

                case "EmailFolders":
                    if(vm.SalesOrderFolderThere != string.Empty)
                    {
                        //Get the directory for drives
                        string mydocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        //Get the root path location from current date
                        // string rootpath = vm.GetFolderPath(DateTime.Now.ToShortDateString());
                        //rootpath = mydocs + @"\";
                        //MessageBox.Show(drives);


                        EmailClass email = new EmailClass(vm);
                        //email.ZipFolderAndSendMail();
                        email.SearchZipFolderAndSendMail(vm.SalesOrderFolderThere);

                        vm.SalesOrderNumber = string.Empty;
                        vm.SalesOrderFolderThere = string.Empty;
                    }
                  
                    break;
            }

            //// ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //TextBox tbBrand = Helper.GetDescendantFromName(Application.Current.MainWindow, "BrandTXT") as TextBox;
            ////TextBox tbCli = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepCliCommandTXT") as TextBox;

            //ComboBox Productcmbo = Helper.GetDescendantFromName(Application.Current.MainWindow, "BasedONCB") as ComboBox;
            //Productcmbo.SelectedIndex = -1;

            ////Clear the controls on the page
            //vm.ClearAddProduct();
            ////give focus back to the Step description box
            //tbBrand.Focus();


           


          
        }

        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }


        public void SearchForFolders(string rootFolder)
        {
            //List<string> sofolders = StaticFunctions.GetDirectoryListing() W:\Goods Out\TGO Photos\2020\06_June
            //MessageBox.Show(@"" + StaticFunctions.UncPathToUse.Replace("Warehouse General", "") + @"Goods Out\TGO Photos\2020"); @"" + StaticFunctions.UncPathToUse.Replace("Warehouse General", "") + @"Goods Out\TGO Photos\2020"
            // Only get subdirectories that begin with the letter "p."
            string year = DateTime.Now.Year.ToString();

            if(Directory.Exists(@"" + StaticFunctions.UncPathToUse.Replace("Warehouse General", "") + @"Goods Out\TGO Photos\" + year))
            {
                //MAYBE CHANGE TO MONTH TO MAKE QUICKER, NEED MONTH OVERLAPS THOUGH
                DirectoryInfo dinfo = new DirectoryInfo(@"" + StaticFunctions.UncPathToUse.Replace("Warehouse General", "") + @"Goods Out\TGO Photos\" + year);


                //FileInfo[] Files = dinfo.GetFiles(sFilter);
                DirectoryInfo[] Dirs = dinfo.GetDirectories(vm.SalesOrderNumber, SearchOption.AllDirectories);

                foreach (DirectoryInfo dir in Dirs)
                {
                    //listbox1.Items.Add(file.Name);
                    //if (dir.Name != "admin")
                    //    listprop.Add(dir.Name);
                    //MessageBox.Show(dir.Name);
                    vm.SalesOrderFolderThere = dir.FullName;
                }
            }
            else
            {
                MessageBox.Show(@"" + StaticFunctions.UncPathToUse.Replace("Warehouse General", "") + @"Goods Out\TGO Photos\" + year + "  Does Not Exist");
            }
          
        }
    }
}


