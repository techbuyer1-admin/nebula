﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class DPDeleteSelectedItemCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;

       
        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public DPDeleteSelectedItemCommand(DriveTestViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            ////cast object to ListView
            var lvValue = (ListView)(object)parameter;


            if (lvValue != null)
            {
                if (lvValue.SelectedItems.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

           
        }

        public void Execute(object parameter)
        {

            //Get array of objects 
            var lvValue = (ListView)(object)parameter;


            //switch (lvValue.Name)
            //{
            //    case "ImportLV":
            //        vm.NetsuiteImportCollection.Clear();
            //        break;
            //    case "ProcessedLV":
            //        vm.ScannedSerialCollection.Clear();
            //        break;
            //}


            try
            {


                List<NetsuiteInventory> DeleteList = new List<NetsuiteInventory>();
              

                foreach (NetsuiteInventory itm in lvValue.SelectedItems)
                {

                    DeleteList.Add(itm);
                    // MessageBox.Show(itm.ProductCode);
                }

             
                switch (lvValue.Name)
                {
                    case "ImportLV":

                        string product2R = "";
                        int loops2P = 0;

                        foreach (NetsuiteInventory itm in DeleteList)
                        {

                           //Remove from Working List
                           vm.NetsuiteImportCollection.Remove(itm);  //.Remove(vm.NetsuiteComparisonCollection.Where(i => i.ProductDescription == itm.ProductDescription).Single()); //(itm);
                           //Remove from Comparison Collection
                           vm.NetsuiteComparisonCollection.Remove(vm.NetsuiteComparisonCollection.Where(i => i.ProductDescription == itm.ProductDescription).FirstOrDefault());

                            //Remove Quantity from Import Total. This allows for any lines imported that should be removed.
                            vm.TotalOnImport -= Convert.ToInt32(itm.Quantity);
                            loops2P = Convert.ToInt32(itm.Quantity);

                            //vm.NetsuiteComparisonCollection.RemoveAt(lvValue.SelectedIndex);
                            product2R = itm.ProductCode;
                          // MessageBox.Show(itm.ProductCode + " " + itm.ProductDescription + " " + itm.Quantity);
                        }

                        //Loop through scanned collection and remove any matching product items 
                        for (int i = 0; i < loops2P; i++)
                        {
                            vm.ScannedSerialCollection.Remove(vm.ScannedSerialCollection.Where(p => p.ProductCode == product2R).FirstOrDefault());
                        }

                        ////loop through and remove all matching product codes from Scan Serial Side
                        //foreach (NetsuiteInventory Scanneditm in vm.ScannedSerialCollection)
                        //{
                        //    vm.ScannedSerialCollection.Remove(vm.ScannedSerialCollection.Where(i => i.ProductCode == product2R).FirstOrDefault());
                        //}

                        //Save Change to State

                        vm.DPDBCrudCommand.Execute(new object[] { "SavePartialDB", new ListView(), new TextBox(), new TabControl() });



                        break;
                    case "ProcessedLV":


                        //Loop through Import collection and find the matching index and load it
                        int IndexPosition = 0;
                        string ProductToDelete = "";

                        //Loop through list and delete item
                        foreach (NetsuiteInventory itm in DeleteList)
                        {
                            //Assign string 
                            ProductToDelete = itm.ProductCode;

                            vm.ScannedSerialCollection.Remove(itm);


                      


                            //vm.NetsuiteImportCollection.Where(items => items.ProductCode == itm.ProductCode).;

                            //MessageBox.Show(itm.ProductCode);
                        }



                        foreach (NetsuiteInventory item in vm.NetsuiteImportCollection)
                        {

                            if (item.ProductCode == ProductToDelete)
                            {
                              
                                //Assign Index
                                vm.ImportListViewIndex = IndexPosition;
                                //Set Done flag to nothing
                                item.Complete = "";
                            }

                            IndexPosition += 1;

                        }




                        //Clear Collection
                        vm.NetsuiteScannedCollection.Clear();


                        int ScannedItems = 0;

                        foreach (NetsuiteInventory item in vm.ScannedSerialCollection)
                        {


                            //Count Quantity in scanned collection 
                            ScannedItems = vm.ScannedSerialCollection.Where(items => items.ProductCode == item.ProductCode).Count();


                            //Check if product code already exists, create new object to add to the comparison lines
                            if (!vm.NetsuiteScannedCollection.Any(p => p.ProductCode == item.ProductCode))
                            {

                                NetsuiteInventory nsi = new NetsuiteInventory();
                                nsi.ProductCode = item.ProductCode;
                                nsi.ProductDescription = item.ProductDescription;
                                nsi.LineType = item.LineType;
                                nsi.Quantity = ScannedItems.ToString();
                                vm.NetsuiteScannedCollection.Add(nsi);
                            }


                        }


                        //Save Change to State

                        vm.DPDBCrudCommand.Execute(new object[] { "SavePartialDB", new ListView(), new TextBox(), new TabControl() });



                        break;
                }

                vm.POTotal = 0;
                //COUNT IMPORTED TOTAL
                foreach (NetsuiteInventory ns in vm.NetsuiteImportCollection)
                {
                    //MessageBox.Show(ns.Quantity);

                    vm.POTotal += Convert.ToInt32(ns.Quantity);
                }

                vm.POImportTotal = 0;
                //COUNT COMPARISON TOTAL
                foreach (NetsuiteInventory ns in vm.NetsuiteComparisonCollection)
                {
                    //MessageBox.Show(ns.Quantity);

                    vm.POImportTotal += Convert.ToInt32(ns.Quantity);
                }


                DeleteList.Clear();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }
    }
}

