﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class DPExtraItemsCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public DPExtraItemsCommand(DriveTestViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return true;


        }

        public void Execute(object parameter)
        {

            //Get array of objects
            var values = (object[])parameter;
            //Split array into specific types
            var switchValue = values[0];
            var serialTXTBX = (TextBox)values[1];

            vm.ImportMessage = "";
            vm.ImportMessageColour = Brushes.Green;


            if (vm.ImportLVSelectedItem != null)
                if (string.IsNullOrEmpty(vm.ImportLVSelectedItem.ProductCode))
                {

                }
                else
                {


                    ////ZERO COUNT
                    //int itemCount = 0;

                    switch (switchValue)
                    {
                        case "Increase":

                            if (vm.ImportLVSelectedItem != null)
                            {
                                //Enable Scanning
                                vm.SerialTBEnabled = true;
                                //Set Complete Status if all serials are scannned
                                vm.ImportLVSelectedItem.Complete = "";
                                //Increase Serial Scanning Count by 1
                                //vm.SerialsCount += 1;
                                //vm.SerialsCount = ushort.Parse(vm.POQuantity);
                                //vm.SerialsCount = ushort.Parse(vm.Quantity);
                                ////// LOOP THROUGH CURRENT ITEMS AND COUNT INDIVIDUAL LINES
                                //foreach (NetsuiteInventory itm in vm.ScannedSerialCollection.Where(x => x.ProductCode == vm.ImportLVSelectedItem.ProductCode))
                                //{
                                //    itemCount += 1;
                                //    //Console.WriteLine(itm.ProductCode + " " + itemCount);

                                //}

                                //vm.ImportLVSelectedItem.SerialsRemaining = Convert.ToInt32(vm.ImportLVSelectedItem.Quantity) - itemCount;
                                //Serials Remaining
                                vm.ImportLVSelectedItem.SerialsRemaining += 1;

                                //SerialsRemainingCheck();


                                //Increase selected item quantity by 1
                                int itemInc = Int32.Parse(vm.ImportLVSelectedItem.Quantity) + 1;
                                vm.ImportLVSelectedItem.Quantity = itemInc.ToString(); // vm.SerialsCount.ToString();
                                                                                       //Increase overall total by 1
                                vm.POTotal = vm.POTotal + 1;
                                //Focus on Scanning Textbox
                                serialTXTBX.Focus();
                            }

                            break;
                        case "Decrease":


                            //REDUCE 
                            //Serials Remaining

                            //SerialsRemainingCheck();
                            if (vm.ImportLVSelectedItem != null)
                            {
                                //Set Complete Status if all serials are scannned
                                //vm.ImportLVSelectedItem.Complete = "";
                                if (vm.ImportLVSelectedItem.Complete == "Done")
                                {

                                }
                                else
                                {

                                    if (vm.ImportLVSelectedItem.Quantity == "0")
                                    {
                                        vm.ImportLVSelectedItem.SerialsRemaining = 0;


                                        vm.SerialsCount = 0;
                                        //if (Int32.Parse(vm.ImportLVSelectedItem.Quantity) == 0)
                                        //{

                                        //}
                                        //else
                                        //{
                                        //    //Decrease selected item quantity by 1
                                        //    int itemDec = Int32.Parse(vm.ImportLVSelectedItem.Quantity) - 1;
                                        //    vm.ImportLVSelectedItem.Quantity = itemDec.ToString(); //vm.SerialsCount.ToString();
                                        //                                                           //Decrease overall total by 1
                                        //    vm.POTotal = vm.POTotal - 1;
                                        //}

                                    }
                                    else
                                    {
                                        //REDUCE 
                                        //Serials Remaining
                                        vm.ImportLVSelectedItem.SerialsRemaining = vm.ImportLVSelectedItem.SerialsRemaining - 1;
                                        //SerialsRemainingCheck();


                                        //Decrease selected item quantity by 1
                                        int itemDec = Int32.Parse(vm.ImportLVSelectedItem.Quantity) - 1;
                                        vm.ImportLVSelectedItem.Quantity = itemDec.ToString(); //vm.SerialsCount.ToString();
                                        vm.SerialsCount = vm.SerialsCount - 1; //Decrease overall total by 1
                                        vm.POTotal = vm.POTotal - 1;

                                        //vm.SerialsCount = ushort.Parse(vm.POQuantity);

                                        if (vm.ImportLVSelectedItem.SerialsRemaining == 0)
                                        {
                                            vm.SerialTBEnabled = false;
                                        }
                                        else
                                        {
                                            vm.SerialTBEnabled = true;
                                        }
                                    }
                                }



                            }



                            break;
                    }

                    if (vm.ImportLVSelectedItem != null)
                    {
                        vm.POImportTotal = 0;
                        //COUNT IMPORTED TOTAL
                        foreach (NetsuiteInventory ns in vm.NetsuiteComparisonCollection)
                        {
                            //MessageBox.Show(ns.Quantity);

                            vm.POImportTotal += Convert.ToInt32(ns.Quantity);
                        }


                        vm.POTotal = 0;
                        ////COUNT IMPORTED TOTAL
                        //COUNT IMPORTED TOTAL
                        foreach (NetsuiteInventory ns in vm.NetsuiteImportCollection)
                        {
                            //MessageBox.Show(ns.Quantity);

                            vm.POTotal += Convert.ToInt32(ns.Quantity);
                        }
                    }

                    //CheckRemaining Serials To Scan
                    SerialsRemainingCheck();

                }

        }


        private void SerialsRemainingCheck()
        {

            //ZERO COUNT
            int itemCount = 0;
            //// LOOP THROUGH CURRENT ITEMS AND COUNT INDIVIDUAL LINES
            foreach (NetsuiteInventory itm in vm.ScannedSerialCollection.Where(x => x.ProductCode == vm.ImportLVSelectedItem.ProductCode))
            {
                itemCount += 1;
                //Console.WriteLine(itm.ProductCode + " " + itemCount);

            }


            if (vm.ImportLVSelectedItem.SerialsRemaining == 0)
            {



                ////Set the Serials Remaing Property
                //vm.ImportLVSelectedItem.SerialsRemaining -= 1;
                if (itemCount == 0)
                {
                    //MessageBox.Show("Here");
                    //Update the quantity property
                    vm.SerialsCount = ushort.Parse(vm.ImportLVSelectedItem.Quantity);
                    //Set the Serials Remaining Property on the object

                    vm.ImportLVSelectedItem.SerialsRemaining = vm.SerialsCount;
                    //get how many are already scanned
                    //vm.ImportLVSelectedItem.SerialsRemaining = Convert.ToInt32(vm.ImportLVSelectedItem.Quantity) - itemCount;
                }
                else
                {
                    //get how many are already scanned
                    vm.ImportLVSelectedItem.SerialsRemaining = Convert.ToInt32(vm.ImportLVSelectedItem.Quantity) - itemCount;
                }



                ////Update the quantity property
                //SerialsCount = ushort.Parse(ImportLVSelectedItem.Quantity);
                ////Set the Serials Remaining Property on the object

                //ImportLVSelectedItem.SerialsRemaining = SerialsCount;
                //////Close the Pop Up
                //DPVariousCompleteCommand.Execute("Close");
            }
            else
            {



                //do nothing;
            }
        }



    }
}
