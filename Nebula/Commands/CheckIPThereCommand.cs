﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class CheckIPThereCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public CheckIPThereCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            var values = (object[])parameter;


            var ServerIP = (string)values[0];
            var ServerNumber = (string)values[1];

            if (values[0] != null)
            {
                if (values[0].ToString() != string.Empty)
                {

                  

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public void Execute(object parameter)
        {

            var values = (object[])parameter;


            var ServerIP = (string)values[0];
            var ServerNumber = (string)values[1];



          


            //StaticFunctions.RunRemoteConsoleCommand(@"" + vm.myDocs + @"\HPTools\HP iLO Integrated Remote Console\HPLOCONS.exe", @"-addr " + ServerIP.ToString() + ":443 -name Administrator -password " + vm.OverridePasswordS1.Trim() + " -lang en");
            //StaticFunctions.RunRemoteConsoleCommand(@"" + vm.myDocs + @"\HPTools\HP iLO Integrated Remote Console\HPLOCONS.exe", @"-addr " + vm.ServerIPv4 + ":443 -name Administrator -password " + vm.OverridePasswordS1.Trim() + " -lang en");
        }



    

    }
}
