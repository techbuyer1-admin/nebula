﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;
using Nebula.Views;
using System.Data.SQLite;
using System.Data;
using MahApps.Metro.Controls.Dialogs;
using System.Windows.Media;

namespace Nebula.Commands
{
    public class ServersProcessedCRUDCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;
        // Variable for MAHAPPS DIALOGS
      

        //MainViewModel Mainvm = null;


        //SQL Lite DB LINK
        public string cs = "";
       
        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public ServersProcessedCRUDCommand(MainViewModel PassedViewModel)
        {
            this.vm = PassedViewModel;

            //Check if Test Mode Checked
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\ServersProcessed.db";
            }
            else
            {

                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/ServersProcessed/ServersProcessed.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/ServersProcessed/ServersProcessed.db";
                }
                else
                {

                    //Set connection string to Live DB
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\ServersProcessed.db";
                }
               
            }

        }


        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            if (parameter != null)
            {
                var values = (object[])parameter;
                var ServerIP = (string)values[0];
                var ServerNumber = (string)values[1];

                //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
                if (ServerIP != null)
                {
                    //check if valid ip entered
                    //System.Net.IPAddress ipAddress = null;
                    //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                    if (ServerIP != string.Empty)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

           
        }

        public void Execute(object parameter)
        {
            try
            {

         

            var values = (object[])parameter;
            //var ServerIP = (string)values[0];
            var IsFaulty = (string)values[1];

            //Connection to SQL Lite DB
            //if all criteria met create the job
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);

         

            if (IsFaulty.Contains("Faulty"))
            {

                ////Call and wait for user input
                //vm.CreateErrorEntryDB("Server Fault", @"Please enter the Purchase Order\Sales Order Number and a description of the fault");


                ////vm.CreateErrorEntryDB("Server Fault", @"Please enter the Purchase Order\Sales Order Number and a description of the fault", vm);
                //MessageBox.Show("bACK IN FUNCTION" + StaticFunctions.FaultPOSONumber + " " + StaticFunctions.FaultReason);
                if (vm.DellServerS1 != null)
                {
                    if (vm.FaultPOSONumber != "" && vm.FaultReason != "")
                    {
                        InsertData(sqlite_conn, new ServersProcessedModel(vm.FaultPOSONumber, vm.DellServerS1.Manufacturer, vm.DellServerS1.Model, vm.DellServerS1.ServiceTag, vm.FullUser, vm.CurrentUserLocation, StaticFunctions.Department, DateTime.Now.ToShortTimeString(), "FAULTY", "", vm.WasFactoryReset, "", vm.FaultReason, vm.GDPRRiskDetected));
                        //Reset Fault Properties
                        vm.FaultPOSONumber = "";
                        vm.FaultReason = "";
                        vm.OverridePasswordS1 = "calvin";
                    }
                }
                // HP Gen8 Gen9 Gen10
                if (vm.GenGenericServerInfo1 != null)
                {
                    //Write to DB
                    InsertData(sqlite_conn, new ServersProcessedModel(vm.FaultPOSONumber, vm.GenGenericServerInfo1.Manufacturer, vm.GenGenericServerInfo1.Model, vm.ChassisSerialNo, vm.FullUser, vm.CurrentUserLocation, StaticFunctions.Department, DateTime.Now.ToShortTimeString(), "FAULTY", "", vm.WasFactoryReset, "", vm.FaultReason, vm.GDPRRiskDetected));
                    //Reset Fault Properties
                    vm.FaultPOSONumber = "";
                    vm.FaultReason = "";
                }

            
                if (vm.LenovoServerS1 != null)
                {
                    if (vm.FaultPOSONumber != "" && vm.FaultReason != "")
                    {
                        InsertData(sqlite_conn, new ServersProcessedModel(StaticFunctions.FaultPOSONumber, vm.LenovoServerS1.Manufacturer, vm.LenovoServerS1.Model, vm.LenovoServerS1.SystemSerialNumber, vm.FullUser, vm.CurrentUserLocation, StaticFunctions.Department, DateTime.Now.ToShortTimeString(), "FAULTY", "", vm.WasFactoryReset, "", StaticFunctions.FaultReason, vm.GDPRRiskDetected));
                        //Reset Fault Properties
                        vm.FaultPOSONumber = "";
                        vm.FaultReason = "";
                        vm.OverridePasswordS1 = "PASSW0RD";
                    }
                }




                //Zero the properties
                vm.LoadedChecklist = null;
                vm.ProcessedDate = "";
                vm.ChecklistMessage = "";
                vm.WasFactoryReset = "";
                vm.ChassisSerialNo = "";
                vm.DocumentsSavePath = "";
                vm.POSONumberForDB = ""; 
                vm.POSONumber = "";
                vm.PONumber = "";
                vm.SONumber = "";
                vm.GoodsInRep = "";
                vm.GoodsOutRep = "";
                vm.FlowDocToPrint = null;
                vm.GDPRRiskDetected = "";
                //Reset Fault Properties
                vm.FaultPOSONumber = "";
                vm.FaultReason = "";

                //Clear Server Objects
                //delete HP server object
                vm.GenGenericServerInfo1 = null;
                //delete server object
                vm.DellServerS1 = null;
                //delete server object
                vm.LenovoServerS1 = null;

                //reset flipview to start
                vm.FlipView1Index = 0;
               
                vm.DELLFrontLCDS1 = string.Empty;
                //ChassisNoTXTBS1.Text = "";

                //clear server ip
                vm.Server1IPv4 = string.Empty;
                vm.BIOSUpgradePath1 = string.Empty;
                vm.CONTUpgradePath1 = string.Empty;
                vm.IDRACUpgradePath1 = string.Empty;
                vm.ILORestOutput1 = string.Empty;
                vm.ProgressIsActive1 = false;
                vm.ProgressMessage1 = string.Empty;
                vm.ProgressPercentage1 = 0;
                //AddTestMsg1.Text = string.Empty;

                //  //tab header set back to default
                //vm.Server1TabHeader = "Server" + vm.WhichTab;

                //Reset Test Certificate properties
                vm.TestCertificateS1 = null;
                vm.CertificateView1 = null;
                vm.TestCertificatePrinted1 = false;
                vm.UpdatesRun = "";

                TabItem ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + vm.WhichTab) as TabItem;
                if (ServerTab != null)
                {
                    ServerTab.Header = "Server" + vm.WhichTab;
                    ServerTab.Foreground = Brushes.MediumPurple;
                }




            }
            else
            {
                Console.WriteLine("DEPARTMENT = " + StaticFunctions.Department);


                if (StaticFunctions.Department.Contains("GoodsIn") || StaticFunctions.Department.Contains("TestDept") || StaticFunctions.Department.Contains("ITAD"))
                {
                 

                    //DELL
                    if (vm.DellServerS1 != null)
                    {
                        //Write to DB
                        if (string.IsNullOrEmpty(vm.POSONumberForDB))
                        {

                        }
                        else
                        {
                            InsertData(sqlite_conn, new ServersProcessedModel(vm.POSONumberForDB, vm.DellServerS1.Manufacturer, vm.DellServerS1.Model, vm.ChassisSerialNo, vm.GoodsInRep, vm.CurrentUserLocation, StaticFunctions.Department, DateTime.Now.ToShortTimeString(), "OK", "", vm.WasFactoryReset, "", "", vm.GDPRRiskDetected));
                            vm.OverridePasswordS1 = "calvin";
                        }
                       

                    }

                    //HP

                    // ChassisNoTXTBS1.Text = vm.DellServerS1.ServiceTag;
                    if (vm.GenGenericServerInfo1 != null)
                    {

                        //Write to DB
                        if (string.IsNullOrEmpty(vm.POSONumberForDB))
                        {

                        }
                        else
                        {
                            InsertData(sqlite_conn, new ServersProcessedModel(vm.POSONumberForDB, vm.GenGenericServerInfo1.Manufacturer, vm.GenGenericServerInfo1.Model, vm.ChassisSerialNo, vm.GoodsInRep, vm.CurrentUserLocation, StaticFunctions.Department, DateTime.Now.ToShortTimeString(), "OK", "", vm.WasFactoryReset, "", "", vm.GDPRRiskDetected));
                        }

                    }

                    //Lenovo
                    if (vm.LenovoServerS1 != null)
                    {

                        //Write to DB
                        if (string.IsNullOrEmpty(vm.POSONumberForDB))
                        {

                        }
                        else
                        {
                            InsertData(sqlite_conn, new ServersProcessedModel(vm.POSONumberForDB, vm.LenovoServerS1.Manufacturer, vm.LenovoServerS1.Model, vm.ChassisSerialNo, vm.GoodsInRep, vm.CurrentUserLocation, StaticFunctions.Department, DateTime.Now.ToShortTimeString(), "OK", "", vm.WasFactoryReset, "", "", vm.GDPRRiskDetected));
                            vm.OverridePasswordS1 = "PASSW0RD";
                        }
                    
                    }
                    //Cisco
                    //if (vm.CiscoServerS1 != null)
                    //{

                    //    //Write to DB
                    //    InsertData(sqlite_conn, new ServersProcessedModel(vm.POSONumber, vm.LenovoServerS1.Manufacturer, vm.LenovoServerS1.Model, vm.ChassisSerialNo, vm.GoodsOutRep, vm.CurrentUserLocation, StaticFunctions.Department, DateTime.Now.ToShortTimeString(), "OK", "", vm.WasFactoryReset, "", ""));
                    //}




                }
                else if (StaticFunctions.Department.Contains("GoodsOutTech"))
                {


                
                    //DELL
                    if (vm.DellServerS1 != null)
                    {
                        

                        //Write to DB
                        if (string.IsNullOrEmpty(vm.POSONumberForDB))
                        {

                        }
                        else
                        {
                           
                            InsertData(sqlite_conn, new ServersProcessedModel(vm.POSONumberForDB, vm.DellServerS1.Manufacturer, vm.DellServerS1.Model, vm.ChassisSerialNo, vm.GoodsOutRep, vm.CurrentUserLocation, StaticFunctions.Department, DateTime.Now.ToShortTimeString(), "OK", "", vm.WasFactoryReset, "", "", vm.GDPRRiskDetected));
                            vm.OverridePasswordS1 = "calvin";
                        }
                    }

                    //HP
                    // ChassisNoTXTBS1.Text = vm.DellServerS1.ServiceTag;
                    if (vm.GenGenericServerInfo1 != null)
                    {

                        //Write to DB
                        if (string.IsNullOrEmpty(vm.POSONumberForDB))
                        {

                        }
                        else
                        {
                            InsertData(sqlite_conn, new ServersProcessedModel(vm.POSONumberForDB, vm.GenGenericServerInfo1.Manufacturer, vm.GenGenericServerInfo1.Model, vm.ChassisSerialNo, vm.GoodsOutRep, vm.CurrentUserLocation, StaticFunctions.Department, DateTime.Now.ToShortTimeString(), "OK", "", vm.WasFactoryReset, "", "", vm.GDPRRiskDetected));
                        }
                        
                    }

                    //Lenovo
                    if (vm.LenovoServerS1 != null)
                    {

                        //Write to DB
                        if (string.IsNullOrEmpty(vm.POSONumberForDB))
                        {

                        }
                        else
                        {
                            InsertData(sqlite_conn, new ServersProcessedModel(vm.POSONumberForDB, vm.LenovoServerS1.Manufacturer, vm.LenovoServerS1.Model, vm.ChassisSerialNo, vm.GoodsOutRep, vm.CurrentUserLocation, StaticFunctions.Department, DateTime.Now.ToShortTimeString(), "OK", "", vm.WasFactoryReset, "", "", vm.GDPRRiskDetected));
                            vm.OverridePasswordS1 = "PASSW0RD";
                        }
                    }
                    //Cisco
                    //if (vm.CiscoServerS1 != null)
                    //{

                    //    //Write to DB
                    //    InsertData(sqlite_conn, new ServersProcessedModel(vm.POSONumber, vm.LenovoServerS1.Manufacturer, vm.LenovoServerS1.Model, vm.ChassisSerialNo, vm.GoodsOutRep, vm.CurrentUserLocation, StaticFunctions.Department, DateTime.Now.ToShortTimeString(), "OK", "", vm.WasFactoryReset, "", ""));
                    //}


                }





                //Zero the properties
                vm.LoadedChecklist = null;
                vm.ProcessedDate = "";
                vm.ChecklistMessage = "";
                vm.WasFactoryReset = "";
                vm.ChassisSerialNo = "";
                vm.DocumentsSavePath = "";
                vm.POSONumberForDB = "";
                vm.POSONumber = "";
                vm.PONumber = "";
                vm.SONumber = "";
                vm.GoodsInRep = "";
                vm.GoodsOutRep = "";
                vm.FlowDocToPrint = null;
                vm.GDPRRiskDetected = "";
                //Reset Fault Properties
                vm.FaultPOSONumber = "";
                vm.FaultReason = "";

                //Clear Server Objects
                //delete HP server object
                vm.GenGenericServerInfo1 = null;
                //delete DELL  server object
                vm.DellServerS1 = null;
                //delete LENOVO server object
                vm.LenovoServerS1 = null;

                //delete LENOVO server object
                vm.CiscoServerS1 = null;

                //Clear Report view
                vm.ReportView1 = null;

                //reset flipview to start
                vm.FlipView1Index = 0;

                vm.DELLFrontLCDS1 = string.Empty;
                //ChassisNoTXTBS1.Text = "";

                //clear server ip
                vm.Server1IPv4 = string.Empty;
                vm.BIOSUpgradePath1 = string.Empty;
                vm.CONTUpgradePath1 = string.Empty;
                vm.IDRACUpgradePath1 = string.Empty;
                vm.ILORestOutput1 = string.Empty;
                vm.ProgressIsActive1 = false;
                vm.ProgressMessage1 = string.Empty;
                vm.ProgressPercentage1 = 0;
                //Clear Reports
                //vm.FlowDocToPrint = null;
                //  //tab header set back to default
                //vm.Server1TabHeader = "Server" + vm.WhichTab;

                //Reset Test Certificate properties
                vm.TestCertificateS1 = null;
                vm.CertificateView1 = null;
                vm.TestCertificatePrinted1 = false;
                vm.UpdatesRun = "";

                TabItem ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + vm.WhichTab) as TabItem;
                if (ServerTab != null)
                {
                    ServerTab.Header = "Server" + vm.WhichTab;
                    ServerTab.Foreground = Brushes.MediumPurple;
                }



               





                //END CHECKLIST PROPERTIES
            }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }




        //SQL FUNCTIONS

        //Create Connection to DB
        static SQLiteConnection CreateConnection(string pathtodb)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            //sqlite_conn = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            //sqlite_conn = new SQLiteConnection("Data Source=" + pathtodb + "; Version = 3; New = True; Compress = True; ");
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return sqlite_conn;
        }




        //Insert Into Table
        public void InsertData(SQLiteConnection conn, ServersProcessedModel itm)
        {

            try
            {
                //Create a text file to hold log entries for that day
                if (Directory.Exists(@"" + StaticFunctions.UncPathToUse + @"Nebula\Server Logs"))
                {
                    try
                    {
                        File.WriteAllText(@"" + StaticFunctions.UncPathToUse + @"Nebula\Server Logs\" + itm.Operative.Replace(@"\", "-").Replace(@"/", "-") + "_" + itm.POSONo.Replace(@"\", "").Replace(@"/", "") + "_" + itm.ChassisSerialNo + ".txt", "Server: " + DateTime.Now.ToString() + " | " + itm.POSONo + " | " + itm.ChassisSerialNo + " | " + itm.Manufacturer + " | " + itm.ModelNo + " | " + itm.Department + " | " + itm.TimeCompleted + " | " + itm.Operative + " | " + itm.OperativeLocation + " | " + itm.ServerState + " | " + itm.GDPRRiskDetected + " | " + itm.FactoryReset + " | " + itm.FaultDescription + " | " + itm.ServerState);
                    }
                    catch (IOException ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                                       
                }
                else
                {
                    //Create directory
                    Directory.CreateDirectory(@"" + StaticFunctions.UncPathToUse + @"Nebula\Server Logs");

                    //Write log file
                    if (Directory.Exists(@"" + StaticFunctions.UncPathToUse + @"Nebula\Server Logs"))
                    {
                        try
                        {
                            File.WriteAllText(@"" + StaticFunctions.UncPathToUse + @"Nebula\Server Logs\" + itm.Operative.Replace(@"\", "-").Replace(@"/", "-") + "_" + itm.POSONo.Replace(@"\", "").Replace(@"/", "") + "_" + itm.ChassisSerialNo + ".txt", "Server: " + DateTime.Now.ToString() + " | " + itm.POSONo + " | " + itm.ChassisSerialNo + " | " + itm.Manufacturer + " | " + itm.ModelNo + " | " + itm.Department + " | " + itm.TimeCompleted + " | " + itm.Operative + " | " + itm.OperativeLocation + " | " + itm.ServerState + " | " + itm.GDPRRiskDetected + " | " + itm.FactoryReset + " | " + itm.FaultDescription + " | " + itm.ServerState);
                        }
                        catch (IOException ex)
                        {

                            MessageBox.Show(ex.Message);
                        }

                    }


                }
             
             

              

                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "INSERT INTO Processed(ProcessedDate, ChassisSerial, Manufacturer, Model, POSONumber, Operative, OperativeLocation, Department, TimeCompleted, ServerState, FactoryReset, Notes, FaultDescription, GDPRRiskDetected)" + " VALUES(@processeddate,@chassisserial,@manufacturer,@model,@posonumber,@operative,@operativelocation,@department,@timecompleted,@serverstate,@factoryreset,@notes,@faultdescription,@gdprriskdetected)";
                sqlite_cmd.CommandType = CommandType.Text;

                //LOOP THROUGH ADDED COLLECTION

                //Date must be in this format to work with SQL Lite
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@processeddate", DateTime.Now.ToString("yyyy-MM-dd")));
                    if (itm.ChassisSerialNo != "")
                    {
                        sqlite_cmd.Parameters.Add(new SQLiteParameter("@chassisserial", itm.ChassisSerialNo));
                    }
                    else
                    {
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@chassisserial", ""));
                    }

                sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", itm.Manufacturer));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@model", itm.ModelNo));
                if (itm.POSONo != "")
                {
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@posonumber", itm.POSONo));
                }
                else
                {
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@posonumber", ""));
                }
              
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@operative", itm.Operative));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@operativelocation", itm.OperativeLocation));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@department", itm.Department));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@timecompleted", itm.TimeCompleted));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@serverstate", itm.ServerState));

                if(string.IsNullOrWhiteSpace(itm.FactoryReset))
                {
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@factoryreset", "No"));
                }
                else
                {
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@factoryreset", itm.FactoryReset));
                }
              
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@notes", itm.Notes));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@faultdescription", itm.FaultDescription));
                if (string.IsNullOrWhiteSpace(itm.GDPRRiskDetected))
                {
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@gdprriskdetected", "No"));
                }
                else
                {
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@gdprriskdetected", itm.GDPRRiskDetected));
                }
              
                

                sqlite_cmd.ExecuteNonQuery();

              

            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message); _"+ DateTime.Now.ToShortDateString().Replace("/", "-") 
                //MessageBox.Show(ex.Message);
                //Create a text file to hold log entries for that day

                MessageBox.Show(ex.Message);

                if (Directory.Exists(@"" + StaticFunctions.UncPathToUse + @"Nebula\Server Logs"))
                {
                    try
                    {
                        File.WriteAllText(@"" + StaticFunctions.UncPathToUse + @"Nebula\Server Logs\" + itm.Operative.Replace(@"\", "-").Replace(@"/", "-") + "_" + itm.POSONo.Replace(@"\", "").Replace(@"/", "") + "_" + itm.ChassisSerialNo + ".txt", "Server: " + DateTime.Now.ToString() + " | " + itm.POSONo + " | " + itm.ChassisSerialNo + " | " + itm.Manufacturer + " | " + itm.ModelNo + " | " + itm.Department + " | " + itm.TimeCompleted + " | " + itm.Operative + " | " + itm.OperativeLocation + " | " + itm.ServerState + " | " + itm.GDPRRiskDetected + " | " + itm.FactoryReset + " | " + itm.FaultDescription + " | " + itm.ServerState);
                    }
                    catch (IOException exError)
                    {
                        File.WriteAllText(@"" + StaticFunctions.UncPathToUse + @"Nebula\Server Logs\" + itm.Operative.Replace(@"\", "-").Replace(@"/", "-") + "_" + itm.POSONo.Replace(@"\", "").Replace(@"/", "") + "_" + itm.ChassisSerialNo + ".txt", "IOError: " + exError.Message + itm.DateProcessed + " | " + itm.POSONo + " | " + itm.ChassisSerialNo + " | " + itm.Manufacturer + " | " + itm.ModelNo + " | " + itm.Department + " | " + itm.TimeCompleted + " | " + itm.Operative + " | " + itm.OperativeLocation + " | " + itm.ServerState + " | " + itm.GDPRRiskDetected + " | " + itm.FactoryReset + " | " + itm.FaultDescription + " | " + itm.ServerState);
                        MessageBox.Show(ex.Message);
                    }

                }

            }

        }









        public void ReadAllData(SQLiteConnection conn)
        {
            try
            {

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = "SELECT * FROM Processed";

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ServersProcessedModel itm = new ServersProcessedModel();
                    if (sqlite_datareader[1].GetType() != typeof(DBNull))
                        itm.DateProcessed = sqlite_datareader.GetDateTime(1);
                    if (sqlite_datareader[2].GetType() != typeof(DBNull))
                        itm.ChassisSerialNo = sqlite_datareader.GetString(2);
                    if (sqlite_datareader[3].GetType() != typeof(DBNull))
                        itm.ModelNo = sqlite_datareader.GetString(3);
                    if (sqlite_datareader[4].GetType() != typeof(DBNull))
                        itm.POSONo = sqlite_datareader.GetString(4);
                    if (sqlite_datareader[5].GetType() != typeof(DBNull))
                        itm.Operative = sqlite_datareader.GetString(5);
                    if (sqlite_datareader[6].GetType() != typeof(DBNull))
                        itm.OperativeLocation = sqlite_datareader.GetString(6);
                    if (sqlite_datareader[7].GetType() != typeof(DBNull))
                        itm.Department = sqlite_datareader.GetString(7);
                    if (sqlite_datareader[8].GetType() != typeof(DBNull))
                        itm.TimeCompleted = sqlite_datareader.GetString(8);
                    if (sqlite_datareader[9].GetType() != typeof(DBNull))
                        itm.ServerState = sqlite_datareader.GetString(9);
                    if (sqlite_datareader[10].GetType() != typeof(DBNull))
                        itm.FactoryReset = sqlite_datareader.GetString(10);
                    if (sqlite_datareader[11].GetType() != typeof(DBNull))
                        itm.Notes = sqlite_datareader.GetString(11);
                    if (sqlite_datareader[12].GetType() != typeof(DBNull))
                        itm.FaultDescription = sqlite_datareader.GetString(12);
                    if (sqlite_datareader[13].GetType() != typeof(DBNull))
                        itm.GDPRRiskDetected = sqlite_datareader.GetString(13);


                    // vm.ServersProcessedCollection.Add(itm);



                }

                // await PutTaskDelay(400);



                // conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //MessageBox.Show(ex.Message);
            }
        }



    }
}

