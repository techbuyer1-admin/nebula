﻿using Renci.SshNet;
using Renci.SshNet.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
   public class CommandPromptCommand : ICommand
    {

        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public CommandPromptCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //if (lv != null)
            //{
            //    if (lv.SelectedItems.Count > 0)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    return true;
            //}
            return true;
        }

        public async void Execute(object parameter)
        {

            ProcessPiper pp = new ProcessPiper(vm);
            string cmdtosend = String.Empty;

           var values = (object)parameter;

            //check if it's a clear instruction or command
            switch (values)
            {
                case "Command":
                 

                    cmdtosend = vm.CMDInstruction;
                    vm.CMDOutput = "";
                    vm.CMDInstruction = "";
                    //MessageBox.Show("Here");
                    //FOR COMMAND LINE EXE
                    //pp.Start(new System.IO.FileInfo(@"C:\Windows\System32\cmd.exe"), "", "ping 192.168.0.12 -n 4", @"C:\Windows\System32");

                    await System.Threading.Tasks.Task.Run(() => pp.Start(new System.IO.FileInfo(@"C:\Windows\System32\cmd.exe"), "", cmdtosend, @"C:\Windows\System32"));
                    break;
                case "SSH":

                    //Connection information
                    string user = "Administrator";
                    string pass = "password";
                    string host = "192.168.100.11";

                    List<string> ipList = new List<string>();
                    string aLine = "";
                    string output;


                    //Set up the SSH connection
                    using (var client = new SshClient(host, user, pass))
                    {

                        //Accept Host key
                        client.HostKeyReceived += delegate (object sender, HostKeyEventArgs e)
                        {
                            e.CanTrust = true;
                        };

                        //Start the connection  var output = client.RunCommand("show device details").Result;
                        client.Connect();

                        output = client.RunCommand("SHOW EBIPA").Result;

                        StringReader strReader = new StringReader(output);

                        while (strReader.ReadLine() != null)
                        {
                            aLine = strReader.ReadLine();
                            if (aLine.Contains("192.168."))
                            {
                                ipList.Add(aLine.Trim());
                            }

                            //do what you want to do with the line;
                        };



                        client.Disconnect();
                        Console.WriteLine(output.ToString());
                        foreach (var itm in ipList)
                        {
                            Console.WriteLine(itm.ToString());
                        }
                        //halt console
                        Console.ReadLine();

                    }


                        cmdtosend = vm.CMDInstruction;
                    vm.CMDOutput = "";
                    vm.CMDInstruction = "";
                    //MessageBox.Show("Here");
                    //FOR COMMAND LINE EXE
                    //pp.Start(new System.IO.FileInfo(@"C:\Windows\System32\cmd.exe"), "", "ping 192.168.0.12 -n 4", @"C:\Windows\System32");

                    await System.Threading.Tasks.Task.Run(() => pp.Start(new System.IO.FileInfo(@"C:\Windows\System32\cmd.exe"), "", cmdtosend, @"C:\Windows\System32"));
                    break;
                case "Telnet":

                    cmdtosend = vm.CMDInstruction;
                    vm.CMDOutput = "";
                    vm.CMDInstruction = "";
                    //MessageBox.Show("Here");
                    //FOR COMMAND LINE EXE
                    //pp.Start(new System.IO.FileInfo(@"C:\Windows\System32\cmd.exe"), "", "ping 192.168.0.12 -n 4", @"C:\Windows\System32");

                    await System.Threading.Tasks.Task.Run(() => pp.Start(new System.IO.FileInfo(@"C:\Windows\System32\cmd.exe"), "", cmdtosend, @"C:\Windows\System32"));
                    break;
                case "Clear":
                    vm.CMDOutput = "";
                    vm.CMDInstruction = "";
                    break;
            }

         


           
            //// ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //TextBox tbBrand = Helper.GetDescendantFromName(Application.Current.MainWindow, "BrandTXT") as TextBox;
            ////TextBox tbCli = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepCliCommandTXT") as TextBox;

            //ComboBox Productcmbo = Helper.GetDescendantFromName(Application.Current.MainWindow, "BasedONCB") as ComboBox;
            //Productcmbo.SelectedIndex = -1;

            ////Clear the controls on the page
            //vm.ClearAddProduct();
            ////give focus back to the Step description box
            //tbBrand.Focus();
        }
    }
}

