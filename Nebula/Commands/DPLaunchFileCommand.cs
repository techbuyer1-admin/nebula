﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class DPLaunchFileCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;

       


        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public DPLaunchFileCommand(DriveTestViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //Get array of objects
            var values = (object[])parameter;
            //Split array into specific types
            var location = (string)values[0];


            switch (location)
            {
               case "Test":
                    if (vm.TestResultsLVSelectedItem != null)
                    {
                        if (vm.TestResultsLVSelectedItem.ReportPath != null)
                        {
                            if (vm.TestResultsLVSelectedItem.ReportPath != "")
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }

                        }
                    }
                        break;
                case "Search":
                    if (vm.SearchResultsLVSelectedItem != null)
                    {
                        if (vm.SearchResultsLVSelectedItem.ReportPath != null)
                        {
                            if (vm.SearchResultsLVSelectedItem.ReportPath != "")
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }

                        }
                    }
                    break;
            }


            return false;

        }

        public void Execute(object parameter)
        {
            //Get array of objects
            var values = (object[])parameter;
            //Split array into specific types
            var location = (string)values[0];


            switch (location)
            {
                case "Test":
                    //Launch FILE 
                    if (File.Exists(vm.TestResultsLVSelectedItem.ReportPath))
                    {
                        //Open the CSV
                        StaticFunctions.OpenFileCommand(@"" + vm.TestResultsLVSelectedItem.ReportPath, "");
                    }
                    break;
                case "Search":
                    //Launch FILE
                    if (File.Exists(vm.SearchResultsLVSelectedItem.ReportPath))
                    {
                        //Open the CSV
                        StaticFunctions.OpenFileCommand(@"" + vm.SearchResultsLVSelectedItem.ReportPath, "");
                    }
                    break;
            }
          

          


        }
    }
}

