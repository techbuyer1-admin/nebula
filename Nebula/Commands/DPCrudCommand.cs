﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class DPCrudCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;

     

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public DPCrudCommand(DriveTestViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //Get array of objects
            var values = (object[])parameter;

           



            //Required if parameter is a control reference
            if (values != null)
            {
                //Split array into specific types
                var lvValue = (ListView)values[1];
                TextBox serialTXT = (TextBox)values[2];
                //Clear the messages
                //vm.ImportMessage = "";
                //vm.ProcessedMessage = "";

                if (lvValue != null)
                {
                    //if (vm.PartCode == "" || vm.POQuantity == "")
                    //{
                    //    return false;
                    //}
                    //else
                    //{
                    //    return true;
                    //}

                    if (lvValue.SelectedItems.Count >= 0)
                    {


                        if (vm.PONumber == String.Empty || vm.PartCode == "" || vm.POQuantity == "")
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }

                    }
                    else
                    {
                        //if (lvValue.Items.Count == 0 && vm.PONumber == String.Empty && vm.PartCode == "" || vm.POQuantity == "")
                        //{
                        //    return false;
                        //}
                        //else
                        //{
                        //    return true;
                        //}
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }


        }

        public void Execute(object parameter)
        {
            try
            {

            //Get array of objects
            var values = (object[])parameter;
            //Split array into specific types
            var request = values[0];
            var lvValue = (ListView)values[1];
            var partcodeTXT = (TextBox)values[2];

            ////Get the values from the selected item
            //vm.GetSelectedImportViewItem();



            switch (request)
            {
             
                case "Add":
                  

                    if(String.IsNullOrEmpty(vm.PartCode) || String.IsNullOrEmpty(vm.POQuantity) || String.IsNullOrEmpty(vm.PartDescription))
                    {
                        //int.TryParse(vm.POQuantity, out _)
                        //MessageBox.Show("Unable to Add! Please fill in all fields."); ) 
                        vm.POMessage = "Unable to Add! Please fill in all fields.";
                    }
                    else
                    {
                        //Check if Quantity is a number
                        if (Regex.IsMatch(vm.POQuantity, @"^\d+$"))
                        {
                            //Create New Object
                            NetsuiteInventory ns = new NetsuiteInventory();
                            ns.ProductCode = vm.PartCode.ToUpper().Trim();
                            ns.ProductDescription = vm.PartDescription;
                            ns.Quantity = vm.POQuantity;
                            //If changed update the underlying serial count property
                            vm.SerialsCount = ushort.Parse(vm.POQuantity);
                            ns.POLineUnitPrice = "0";
                            ns.POUnitAmount = "0";
                            //Check Selected Line if Various, if not mark as Extra
                            if (vm.ImportLVSelectedItem != null)
                            {
                                if (vm.ImportLVSelectedItem.ProductCode.Contains("Various") || vm.ImportLVSelectedItem.ProductCode.Contains("VARIOUS") || vm.ImportLVSelectedItem.ProductCode.Contains("various"))
                                {
                                    //various line
                                    //MessageBox.Show("V");
                                    ns.LineType = "(V)";
                                    ns.VariousIndex = lvValue.SelectedIndex;


                                    //Add the Item
                                    //if(vm.ImportListLastIndex != null)
                                    //{
                                    vm.NetsuiteImportCollection.Insert(lvValue.SelectedIndex + 1, ns);
                                    //}


                                    List<NetsuiteInventory> DeleteList = new List<NetsuiteInventory>();

                                    foreach (NetsuiteInventory itm in lvValue.SelectedItems)
                                    {

                                        DeleteList.Add(itm);
                                        // MessageBox.Show(itm.ProductCode);
                                    }

                                    foreach (NetsuiteInventory itm in DeleteList)
                                    {

                                        if (itm.ProductCode.Contains("various") || itm.ProductCode.Contains("Various") || itm.ProductCode.Contains("VARIOUS"))
                                        {
                                            vm.NetsuiteImportCollection.Remove(itm);
                                        }
                                        else
                                        {

                                        }

                                        // MessageBox.Show(itm.ProductCode);
                                    }

                                    //Close PopUp
                                    vm.ShowPopup = false;

                                    DeleteList.Clear();

                                    if (lvValue.SelectedItems.Count > 0)
                                    {
                                        vm.GetSelectedImportViewItem();
                                    }
                                    else
                                    {

                                        //Set to first item to enable scan serial button
                                        lvValue.SelectedIndex = 0;
                                        //vm.GetSelectedImportViewItem("Add Extras");
                                    }

                                }
                                //else
                                //{
                                //    //extra
                                //    ns.LineType = "(E)";
                                //}
                            }
                            else
                            {
                                //extra
                                ns.LineType = "(E)";
                                //Add the Item
                                vm.NetsuiteImportCollection.Add(ns);
                            }


                            //Check if part required and setproperty to true
                            if (vm.PartCodeRequired == "Yes")
                            {
                                ns.PartCodeRequired = "true";
                                //Increase Part Codes Total by one
                                vm.PartCodesTotal += 1;
                            }
                            else
                            {

                            }



                            //Clear Fields
                            vm.PartCode = "";
                            vm.PartDescription = "";
                            vm.POQuantity = "";
                            vm.PartCodeRequired = "";
                            vm.POMessage = "";
                        }


                    }

                        //Save Change to State

                        vm.DPDBCrudCommand.Execute(new object[] { "SavePartialDB", new ListView(), new TextBox(), new TabControl() });

                        break;
                case "Add Extra":

                    if (String.IsNullOrEmpty(vm.PartCode) || String.IsNullOrEmpty(vm.POQuantity))
                    {
                        //int.TryParse(vm.POQuantity, out _)
                        //MessageBox.Show("Unable to Add! Please fill in all fields."); ) 
                        vm.POMessage = "Unable to Add! Please fill in all fields.";
                    }
                    else
                    {
                        try
                        {


                           
                        //Check if product code already in the list
                         var query = vm.NetsuiteImportCollection.Where(x => x.ProductCode == vm.PartCode.ToUpper().Trim());
                         
                        // if it doesn't then add
                        if(query.FirstOrDefault() != null)
                        {
                                 
                                        //Loop Through Collection and find specified item

                                        foreach (NetsuiteInventory itm in vm.NetsuiteImportCollection.Where(x => x.ProductCode == vm.PartCode.ToUpper().Trim()))
                                        {
                                            //MessageBox.Show(itm.ProductCode + "Product Code already exists! Please use the Plus & Minus buttons to add extras. ");

                                            int existsItm = lvValue.Items.IndexOf(itm);
                                            //MessageBox.Show(existsItm.ToString()); 619291-B21B-REF
                                            lvValue.SelectedIndex = existsItm;
                                        }

                                        //Display Message
                                        vm.ImportMessageColour = Brushes.Red;
                                        vm.ImportMessage = vm.PartCode.ToUpper().Trim() + " already exists! Use the + & - buttons to add extras to the existing line.";
                                        vm.PartCode = "";
                                        vm.POQuantity = "";

                                  

                                }
                                else
                                {
                                    //Just Add the extra

                                
                                        //Check if Quantity is a number
                                        if (Regex.IsMatch(vm.POQuantity, @"^\d+$"))
                                        {
                                            //Create New Object
                                            NetsuiteInventory ns = new NetsuiteInventory();
                                            ns.ProductCode = vm.PartCode.ToUpper().Trim();
                                            ns.ProductDescription = "Added Extras. No Description";//vm.PartDescription;
                                            ns.Quantity = vm.POQuantity;
                                            ns.POLineUnitPrice = "0";
                                            ns.POUnitAmount = "0";

                                            //extra
                                            ns.LineType = "(E)";
                                            //Add the Item
                                            vm.NetsuiteImportCollection.Add(ns);



                                            //Check if part required and setproperty to true
                                            if (vm.PartCodeRequired == "Yes")
                                            {
                                                ns.PartCodeRequired = "true";
                                                //Increase Part Codes Total by one
                                                vm.PartCodesTotal += 1;
                                            }
                                            else
                                            {

                                            }



                                            //Clear Fields
                                            vm.PartCode = "";
                                            vm.PartCode = string.Empty;
                                            vm.PartDescription = "";
                                            vm.POQuantity = "";
                                            vm.PartCodeRequired = "";
                                            vm.POMessage = "";
                                        }
                                   
                                }

                            }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                           
                        }
                    }


                        //Save Change to State

                        vm.DPDBCrudCommand.Execute(new object[] { "SavePartialDB", new ListView(), new TextBox(), new TabControl() });

                        break;
                case "Edit Serial":
                    //Save the change
                    vm.ProcessedLVSelectedItem.SerialNumber = vm.ProcessSerialNo;

                    break;

                case "Read":

                    break;
              
                case "Update":

                    break;
                case "Delete":

                    break;
             

            }


            //return focus to part code textbox
            //partcodeTXT = (AutoCompleteTextBoxUserControl)partcodeTXT;
            partcodeTXT.Focus();



                ////Scanserial command
                //vm.SerialTBEnabled = true;
                ////serialTXT.IsEnabled = true;


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }



    }
}