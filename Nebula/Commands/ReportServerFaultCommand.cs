﻿using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class ReportServerFaultCommand : ICommand
    {
   
        //declare viewmodel
        MainViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public ReportServerFaultCommand(MainViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if(parameter != null)
            {
                var values = (object[])parameter;
                var ServerIP = (string)values[0];
                var ServerNumber = (string)values[1];

                //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
                if (ServerIP != null)
                {
                    //check if valid ip entered
                    //System.Net.IPAddress ipAddress = null;
                    //bool isValidIp = System.Net.IPAddress.TryParse(vm.ServerIPv4, out ipAddress);

                    if (ServerIP != string.Empty)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
         
        }

        public void Execute(object parameter)
        {
            var values = (object[])parameter;
            var ServerIP = (string)values[0];
            var ServerNumber = (string)values[1];
            // Variable for MAHAPPS DIALOGS


            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HP Tools\HPCONFIGUTIL\UID_Control_ON.xml"))
            {

            }
            else
            {
                //MessageBox.Show(@"" + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL\UID_Control_ON.xml");
                if(File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL\UID_Control_ON.xml"))
                {

                }
                else
                {
                    File.Copy(@"" + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL\UID_Control_ON.xml", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\UID_Control_ON.xml");
                }
               
            }


            // Add a input dialog, detect if ok was pressed and create an entry in the Servers process csv. Set the UID to ON then reset the slide to beginning

            vm.CreateErrorEntryDB("Server Fault", @"Please enter the Purchase Order\Sales Order Number and a description of the fault");

            //vm.CreateErrorEntry("Server Fault", @"Please enter the Purchase Order\Sales Order Number and a description of the fault", ServerIP, ServerNumber);

            //if (result == null)
            //    return;

            //await this.ShowMessageAsync("Test", "You entered " + result + "!"

            //StaticFunctions.CreateAppendCSV(new ReportServersProcessed("", StaticFunctions.ChassisNoPassthrough, "Fault", "", "Fault", StaticFunctions.Department), @"" + @"\\pinnacle.local\tech_resources\Nebula\Servers_Processed.csv");

            //if (vm.InputDialogResult == "")
            //{
            //    //User pressed cancel
            //}
            //else
            //{
            //    MessageBox.Show(vm.InputDialogResult);
            //}
        }

    }
}

