﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Commands
{
   public class DPVariousCompleteCommand : ICommand
    {

        //declare viewmodel
        DriveTestViewModel vm = null;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public DPVariousCompleteCommand(DriveTestViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //Get array of objects
            var values = (object[])parameter;

            //Required if parameter is a control reference
            if (values != null)
            {
                //Split array into specific types
                var lvValue = (ListView)values[1];
                var serialTXT = (TextBox)values[2];


                if (lvValue != null)
                {
                    if (lvValue.SelectedItems.Count >= 0)
                    {
                      
                        return true;

                    }
                    else
                    {

                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }


        }

        public void Execute(object parameter)
        {

            //Get array of objects
            var values = (object[])parameter;
            //Split array into specific types
            var request = values[0];
            var lvValue = (ListView)values[1];
            var serialTXT = (TextBox)values[2];

            ////Get the values from the selected item
            //vm.GetSelectedImportViewItem();
            //Clear the messages
            //vm.ImportMessage = "";
            //vm.ProcessedMessage = "";


            switch (request)
            {

              case "RemoveVariousLine":
                    //Close PopUp
                    try
                    {


                        List<NetsuiteInventory> DeleteList = new List<NetsuiteInventory>();

                        foreach (NetsuiteInventory itm in lvValue.SelectedItems)
                        {

                            DeleteList.Add(itm);
                            // MessageBox.Show(itm.ProductCode);
                        }

                        foreach (NetsuiteInventory itm in DeleteList)
                        {

                            if (itm.ProductCode.Contains("various") || itm.ProductCode.Contains("Various") || itm.ProductCode.Contains("VARIOUS"))
                            {
                                vm.NetsuiteImportCollection.Remove(itm);
                            }
                            else
                            {

                            }

                            // MessageBox.Show(itm.ProductCode);
                        }


                        DeleteList.Clear();
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex.Message);
                    }

                    vm.ShowPopup = false;
                    //Increase Selected Index To Next Item
                    //ImportListViewIndex = ImportListViewIndex + 1;
                    //Launch next item in list Get the values from the selected item

                   // await PutTaskDelay(1000);



                    if (lvValue.SelectedItems.Count > 0)
                    {
                        //vm.SerialsCount = ushort.Parse(vm.ImportLVSelectedItem.Quantity);
                        vm.GetSelectedImportViewItem();
                    }
                    else
                    {

                        //Set to first item to enable scan serial button
                        lvValue.SelectedIndex = 0;
                        //vm.GetSelectedImportViewItem("Add Extras");
                    }




                    break;

         

                case "Close":


                    //Close PopUp
                    //Set to first item to enable scan serial button
                    //if (vm.ImportLVSelectedItem != null)
                    //    if (vm.ImportLVSelectedItem.Quantity == "0")
                    //    {
                    //        // check if last item in serial scan is zero
                    //        if (lvValue.SelectedIndex == lvValue.Items.Count - 1)
                    //        {
                    //            // lvValue.SelectedIndex = -1;
                    //            //vm.SerialsCount = Int32.Parse(vm.ImportLVSelectedItem.Quantity);
                    //            ////Get the values from the selected item
                    //            vm.GetSelectedImportViewItem();
                    //            //MessageBox.Show(vm.SerialsCount.ToString());
                    //            vm.ShowPopup = false;
                    //        }
                    //        else if (lvValue.SelectedIndex != lvValue.Items.Count - 1)
                    //        {

                    //            //     lvValue.SelectedIndex = lvValue.SelectedIndex + 1;

                    //            //MessageBox.Show(vm.SerialsCount.ToString());
                    //            //vm.SerialsCount = Int32.Parse(vm.ImportLVSelectedItem.Quantity);
                    //            //Get the values from the selected item
                    //            vm.GetSelectedImportViewItem();

                    //            vm.ShowPopup = true;



                    //        }
                        


                    //    }
                    //    else
                    //    {
                    //        //MessageBox.Show("Various end line");
                    //        //vm.GetSelectedImportViewItem();
                    //        vm.ShowPopup = false;

                    //    }

                    vm.ShowPopup = false;
                    vm.ScannedSerial = "";
                    vm.SerialMessage2 = "";


                    break;
            }





        }


        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

    }
}