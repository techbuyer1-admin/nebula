﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Nebula.Helpers;
using Nebula.ViewModels;

namespace Nebula.Commands
{
    public class SigCommand : ICommand
    {

        //declare viewmodel
        GoodsInOutViewModel vm = null;

        //use helper to get control
        // ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

        //CONSTRUCTOR Pass an instance of the ViewModel into the constructor
        public SigCommand(GoodsInOutViewModel TheViewModel)
        {
            this.vm = TheViewModel;
        }



        // Used to add or remove the command from the command manager
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;
            //if (lv != null)
            //{
            //    if (lv.SelectedItems.Count > 0)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //else
            //{
            //    return true;
            //}
            return true;
        }

        public void Execute(object parameter)
        {

           TextBlock signformTB = Helper.GetDescendantFromName(Application.Current.MainWindow, "SignFormTB") as TextBlock;
            var values = (object)parameter;

           
           if(values.ToString() == "GoodsIn")
            {
                vm.GoodsINRep = vm.CurrentUser;
                vm.ChecklistMessage = String.Empty;
                //signformTB.Focus();

            }
           else if (values.ToString() == "GoodsOut")
            {

                vm.GoodsOUTRep = vm.CurrentUser;
                vm.ChecklistMessage = String.Empty;
                //signformTB.Focus();
            }
         
         

            //MessageBox.Show(vm.GoodsINRep);
        }
    }
}



