﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.LenovoServers
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class onecli
    {

        private onecliRepository repositoryField;

        private onecliSCAN sCANField;

        /// <remarks/>
        public onecliRepository repository
        {
            get
            {
                return this.repositoryField;
            }
            set
            {
                this.repositoryField = value;
            }
        }

        /// <remarks/>
        public onecliSCAN SCAN
        {
            get
            {
                return this.sCANField;
            }
            set
            {
                this.sCANField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepository
    {

        private string modeField;

        private string buildversionField;

        private onecliRepositoryDataSource dataSourceField;

        private onecliRepositoryComputerSystem_IPMI computerSystem_IPMIField;

        private onecliRepositoryCPU[] cPUField;

        private onecliRepositoryDIMM[] dIMMField;

        private onecliRepositoryMemoryStateOOB memoryStateOOBField;

        private onecliRepositoryFan[] fanField;

        private onecliRepositoryPower[] powerField;

        private onecliRepositorySystemBoard[] systemBoardField;

        private onecliRepositoryIMMNumericSensor[] iMMNumericSensorField;

        private onecliRepositoryXFirmwareBuildID[] xFirmwareBuildIDField;

        private onecliRepositoryXFirmwareOtherVPD[] xFirmwareOtherVPDField;

        private onecliRepositoryPCIAdapter pCIAdapterField;

        private onecliRepositoryPCIPort[] pCIPortField;

        private onecliRepositoryPCISoftwareIdentity pCISoftwareIdentityField;

        private onecliRepositoryStorageDevice storageDeviceField;

        private onecliRepositoryStorageSoftwareIdentity[] storageSoftwareIdentityField;

        /// <remarks/>
        public string mode
        {
            get
            {
                return this.modeField;
            }
            set
            {
                this.modeField = value;
            }
        }

        /// <remarks/>
        public string buildversion
        {
            get
            {
                return this.buildversionField;
            }
            set
            {
                this.buildversionField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryDataSource DataSource
        {
            get
            {
                return this.dataSourceField;
            }
            set
            {
                this.dataSourceField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryComputerSystem_IPMI ComputerSystem_IPMI
        {
            get
            {
                return this.computerSystem_IPMIField;
            }
            set
            {
                this.computerSystem_IPMIField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CPU")]
        public onecliRepositoryCPU[] CPU
        {
            get
            {
                return this.cPUField;
            }
            set
            {
                this.cPUField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DIMM")]
        public onecliRepositoryDIMM[] DIMM
        {
            get
            {
                return this.dIMMField;
            }
            set
            {
                this.dIMMField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryMemoryStateOOB MemoryStateOOB
        {
            get
            {
                return this.memoryStateOOBField;
            }
            set
            {
                this.memoryStateOOBField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Fan")]
        public onecliRepositoryFan[] Fan
        {
            get
            {
                return this.fanField;
            }
            set
            {
                this.fanField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Power")]
        public onecliRepositoryPower[] Power
        {
            get
            {
                return this.powerField;
            }
            set
            {
                this.powerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SystemBoard")]
        public onecliRepositorySystemBoard[] SystemBoard
        {
            get
            {
                return this.systemBoardField;
            }
            set
            {
                this.systemBoardField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("IMMNumericSensor")]
        public onecliRepositoryIMMNumericSensor[] IMMNumericSensor
        {
            get
            {
                return this.iMMNumericSensorField;
            }
            set
            {
                this.iMMNumericSensorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("XFirmwareBuildID")]
        public onecliRepositoryXFirmwareBuildID[] XFirmwareBuildID
        {
            get
            {
                return this.xFirmwareBuildIDField;
            }
            set
            {
                this.xFirmwareBuildIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("XFirmwareOtherVPD")]
        public onecliRepositoryXFirmwareOtherVPD[] XFirmwareOtherVPD
        {
            get
            {
                return this.xFirmwareOtherVPDField;
            }
            set
            {
                this.xFirmwareOtherVPDField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIAdapter PCIAdapter
        {
            get
            {
                return this.pCIAdapterField;
            }
            set
            {
                this.pCIAdapterField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PCIPort")]
        public onecliRepositoryPCIPort[] PCIPort
        {
            get
            {
                return this.pCIPortField;
            }
            set
            {
                this.pCIPortField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentity PCISoftwareIdentity
        {
            get
            {
                return this.pCISoftwareIdentityField;
            }
            set
            {
                this.pCISoftwareIdentityField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageDevice StorageDevice
        {
            get
            {
                return this.storageDeviceField;
            }
            set
            {
                this.storageDeviceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("StorageSoftwareIdentity")]
        public onecliRepositoryStorageSoftwareIdentity[] StorageSoftwareIdentity
        {
            get
            {
                return this.storageSoftwareIdentityField;
            }
            set
            {
                this.storageSoftwareIdentityField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDataSource
    {

        private string displayField;

        private onecliRepositoryDataSourceName nameField;

        private onecliRepositoryDataSourceBuild buildField;

        private onecliRepositoryDataSourceVersion versionField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryDataSourceName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryDataSourceBuild Build
        {
            get
            {
                return this.buildField;
            }
            set
            {
                this.buildField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryDataSourceVersion Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDataSourceName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDataSourceBuild
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDataSourceVersion
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryComputerSystem_IPMI
    {

        private string displayField;

        private onecliRepositoryComputerSystem_IPMIManufacturer manufacturerField;

        private onecliRepositoryComputerSystem_IPMIProductName productNameField;

        private onecliRepositoryComputerSystem_IPMIModel modelField;

        private onecliRepositoryComputerSystem_IPMISerialNumber serialNumberField;

        private onecliRepositoryComputerSystem_IPMISystemUUID systemUUIDField;

        private onecliRepositoryComputerSystem_IPMIVersion versionField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryComputerSystem_IPMIManufacturer Manufacturer
        {
            get
            {
                return this.manufacturerField;
            }
            set
            {
                this.manufacturerField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryComputerSystem_IPMIProductName ProductName
        {
            get
            {
                return this.productNameField;
            }
            set
            {
                this.productNameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryComputerSystem_IPMIModel Model
        {
            get
            {
                return this.modelField;
            }
            set
            {
                this.modelField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryComputerSystem_IPMISerialNumber SerialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryComputerSystem_IPMISystemUUID SystemUUID
        {
            get
            {
                return this.systemUUIDField;
            }
            set
            {
                this.systemUUIDField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryComputerSystem_IPMIVersion Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryComputerSystem_IPMIManufacturer
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryComputerSystem_IPMIProductName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryComputerSystem_IPMIModel
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryComputerSystem_IPMISerialNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryComputerSystem_IPMISystemUUID
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryComputerSystem_IPMIVersion
    {

        private string displayField;

        private object valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public object value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryCPU
    {

        private string displayField;

        private onecliRepositoryCPUName nameField;

        private onecliRepositoryCPUFamily familyField;

        private onecliRepositoryCPUHealthState healthStateField;

        private onecliRepositoryCPUCorePerCPU corePerCPUField;

        private onecliRepositoryCPUThreadPerCore threadPerCoreField;

        private onecliRepositoryCPUCurrentClockSpeed currentClockSpeedField;

        private onecliRepositoryCPUMaxClockSpeed maxClockSpeedField;

        private onecliRepositoryCPUL1Cache l1CacheField;

        private onecliRepositoryCPUL2Cache l2CacheField;

        private onecliRepositoryCPUL3Cache l3CacheField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryCPUName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryCPUFamily Family
        {
            get
            {
                return this.familyField;
            }
            set
            {
                this.familyField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryCPUHealthState HealthState
        {
            get
            {
                return this.healthStateField;
            }
            set
            {
                this.healthStateField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryCPUCorePerCPU CorePerCPU
        {
            get
            {
                return this.corePerCPUField;
            }
            set
            {
                this.corePerCPUField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryCPUThreadPerCore ThreadPerCore
        {
            get
            {
                return this.threadPerCoreField;
            }
            set
            {
                this.threadPerCoreField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryCPUCurrentClockSpeed CurrentClockSpeed
        {
            get
            {
                return this.currentClockSpeedField;
            }
            set
            {
                this.currentClockSpeedField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryCPUMaxClockSpeed MaxClockSpeed
        {
            get
            {
                return this.maxClockSpeedField;
            }
            set
            {
                this.maxClockSpeedField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryCPUL1Cache L1Cache
        {
            get
            {
                return this.l1CacheField;
            }
            set
            {
                this.l1CacheField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryCPUL2Cache L2Cache
        {
            get
            {
                return this.l2CacheField;
            }
            set
            {
                this.l2CacheField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryCPUL3Cache L3Cache
        {
            get
            {
                return this.l3CacheField;
            }
            set
            {
                this.l3CacheField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryCPUName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryCPUFamily
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryCPUHealthState
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryCPUCorePerCPU
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryCPUThreadPerCore
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryCPUCurrentClockSpeed
    {

        private string displayField;

        private ushort valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public ushort value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryCPUMaxClockSpeed
    {

        private string displayField;

        private ushort valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public ushort value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryCPUL1Cache
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryCPUL2Cache
    {

        private string displayField;

        private ushort valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public ushort value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryCPUL3Cache
    {

        private string displayField;

        private ushort valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public ushort value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDIMM
    {

        private string displayField;

        private onecliRepositoryDIMMBankLabel bankLabelField;

        private onecliRepositoryDIMMElementName elementNameField;

        private onecliRepositoryDIMMManufacturer manufacturerField;

        private onecliRepositoryDIMMModel modelField;

        private onecliRepositoryDIMMPartNumber partNumberField;

        private onecliRepositoryDIMMSerialNumber serialNumberField;

        private onecliRepositoryDIMMMaxMemorySpeed maxMemorySpeedField;

        private onecliRepositoryDIMMCapacity capacityField;

        private onecliRepositoryDIMMFRUPartNumber fRUPartNumberField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryDIMMBankLabel BankLabel
        {
            get
            {
                return this.bankLabelField;
            }
            set
            {
                this.bankLabelField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryDIMMElementName ElementName
        {
            get
            {
                return this.elementNameField;
            }
            set
            {
                this.elementNameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryDIMMManufacturer Manufacturer
        {
            get
            {
                return this.manufacturerField;
            }
            set
            {
                this.manufacturerField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryDIMMModel Model
        {
            get
            {
                return this.modelField;
            }
            set
            {
                this.modelField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryDIMMPartNumber PartNumber
        {
            get
            {
                return this.partNumberField;
            }
            set
            {
                this.partNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryDIMMSerialNumber SerialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryDIMMMaxMemorySpeed MaxMemorySpeed
        {
            get
            {
                return this.maxMemorySpeedField;
            }
            set
            {
                this.maxMemorySpeedField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryDIMMCapacity Capacity
        {
            get
            {
                return this.capacityField;
            }
            set
            {
                this.capacityField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryDIMMFRUPartNumber FRUPartNumber
        {
            get
            {
                return this.fRUPartNumberField;
            }
            set
            {
                this.fRUPartNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDIMMBankLabel
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDIMMElementName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDIMMManufacturer
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDIMMModel
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDIMMPartNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDIMMSerialNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDIMMMaxMemorySpeed
    {

        private string displayField;

        private ushort valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public ushort value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDIMMCapacity
    {

        private string displayField;

        private ushort valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public ushort value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryDIMMFRUPartNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryMemoryStateOOB
    {

        private string displayField;

        private onecliRepositoryMemoryStateOOBName nameField;

        private onecliRepositoryMemoryStateOOBHealthState healthStateField;

        private onecliRepositoryMemoryStateOOBCapacity capacityField;

        private onecliRepositoryMemoryStateOOBBlockSize blockSizeField;

        private onecliRepositoryMemoryStateOOBNumberOfBlocks numberOfBlocksField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryMemoryStateOOBName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryMemoryStateOOBHealthState HealthState
        {
            get
            {
                return this.healthStateField;
            }
            set
            {
                this.healthStateField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryMemoryStateOOBCapacity Capacity
        {
            get
            {
                return this.capacityField;
            }
            set
            {
                this.capacityField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryMemoryStateOOBBlockSize BlockSize
        {
            get
            {
                return this.blockSizeField;
            }
            set
            {
                this.blockSizeField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryMemoryStateOOBNumberOfBlocks NumberOfBlocks
        {
            get
            {
                return this.numberOfBlocksField;
            }
            set
            {
                this.numberOfBlocksField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryMemoryStateOOBName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryMemoryStateOOBHealthState
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryMemoryStateOOBCapacity
    {

        private string displayField;

        private uint valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public uint value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryMemoryStateOOBBlockSize
    {

        private string displayField;

        private ushort valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public ushort value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryMemoryStateOOBNumberOfBlocks
    {

        private string displayField;

        private uint valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public uint value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryFan
    {

        private string displayField;

        private onecliRepositoryFanElementName elementNameField;

        private onecliRepositoryFanHealthState healthStateField;

        private onecliRepositoryFanDesiredSpeed desiredSpeedField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryFanElementName ElementName
        {
            get
            {
                return this.elementNameField;
            }
            set
            {
                this.elementNameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryFanHealthState HealthState
        {
            get
            {
                return this.healthStateField;
            }
            set
            {
                this.healthStateField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryFanDesiredSpeed DesiredSpeed
        {
            get
            {
                return this.desiredSpeedField;
            }
            set
            {
                this.desiredSpeedField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryFanElementName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryFanHealthState
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryFanDesiredSpeed
    {

        private string displayField;

        private ushort valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public ushort value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPower
    {

        private string displayField;

        private onecliRepositoryPowerElementName elementNameField;

        private onecliRepositoryPowerHealthState healthStateField;

        private onecliRepositoryPowerTotalOutputPower totalOutputPowerField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPowerElementName ElementName
        {
            get
            {
                return this.elementNameField;
            }
            set
            {
                this.elementNameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPowerHealthState HealthState
        {
            get
            {
                return this.healthStateField;
            }
            set
            {
                this.healthStateField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPowerTotalOutputPower TotalOutputPower
        {
            get
            {
                return this.totalOutputPowerField;
            }
            set
            {
                this.totalOutputPowerField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPowerElementName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPowerHealthState
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPowerTotalOutputPower
    {

        private string displayField;

        private ushort valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public ushort value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositorySystemBoard
    {

        private string displayField;

        private string tooltipField;

        private onecliRepositorySystemBoardElementName elementNameField;

        private onecliRepositorySystemBoardManufacturer manufacturerField;

        private onecliRepositorySystemBoardModel modelField;

        private onecliRepositorySystemBoardPartNumber partNumberField;

        private onecliRepositorySystemBoardFRUNumber fRUNumberField;

        private onecliRepositorySystemBoardSerialNumber serialNumberField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string tooltip
        {
            get
            {
                return this.tooltipField;
            }
            set
            {
                this.tooltipField = value;
            }
        }

        /// <remarks/>
        public onecliRepositorySystemBoardElementName ElementName
        {
            get
            {
                return this.elementNameField;
            }
            set
            {
                this.elementNameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositorySystemBoardManufacturer Manufacturer
        {
            get
            {
                return this.manufacturerField;
            }
            set
            {
                this.manufacturerField = value;
            }
        }

        /// <remarks/>
        public onecliRepositorySystemBoardModel Model
        {
            get
            {
                return this.modelField;
            }
            set
            {
                this.modelField = value;
            }
        }

        /// <remarks/>
        public onecliRepositorySystemBoardPartNumber PartNumber
        {
            get
            {
                return this.partNumberField;
            }
            set
            {
                this.partNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositorySystemBoardFRUNumber FRUNumber
        {
            get
            {
                return this.fRUNumberField;
            }
            set
            {
                this.fRUNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositorySystemBoardSerialNumber SerialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositorySystemBoardElementName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositorySystemBoardManufacturer
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositorySystemBoardModel
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositorySystemBoardPartNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositorySystemBoardFRUNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositorySystemBoardSerialNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensor
    {

        private string displayField;

        private onecliRepositoryIMMNumericSensorNodeID nodeIDField;

        private onecliRepositoryIMMNumericSensorBaseUnit baseUnitField;

        private onecliRepositoryIMMNumericSensorCurrentReading currentReadingField;

        private onecliRepositoryIMMNumericSensorLowerThresholdCritical lowerThresholdCriticalField;

        private onecliRepositoryIMMNumericSensorLowerThresholdFatal lowerThresholdFatalField;

        private onecliRepositoryIMMNumericSensorLowerThresholdNonCritical lowerThresholdNonCriticalField;

        private onecliRepositoryIMMNumericSensorUpperThresholdCritical upperThresholdCriticalField;

        private onecliRepositoryIMMNumericSensorUpperThresholdFatal upperThresholdFatalField;

        private onecliRepositoryIMMNumericSensorUpperThresholdNonCritical upperThresholdNonCriticalField;

        private onecliRepositoryIMMNumericSensorCurrentState currentStateField;

        private onecliRepositoryIMMNumericSensorDeviceID deviceIDField;

        private onecliRepositoryIMMNumericSensorCaption captionField;

        private onecliRepositoryIMMNumericSensorDescription descriptionField;

        private onecliRepositoryIMMNumericSensorElementName elementNameField;

        private onecliRepositoryIMMNumericSensorHealthState healthStateField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorNodeID NodeID
        {
            get
            {
                return this.nodeIDField;
            }
            set
            {
                this.nodeIDField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorBaseUnit BaseUnit
        {
            get
            {
                return this.baseUnitField;
            }
            set
            {
                this.baseUnitField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorCurrentReading CurrentReading
        {
            get
            {
                return this.currentReadingField;
            }
            set
            {
                this.currentReadingField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorLowerThresholdCritical LowerThresholdCritical
        {
            get
            {
                return this.lowerThresholdCriticalField;
            }
            set
            {
                this.lowerThresholdCriticalField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorLowerThresholdFatal LowerThresholdFatal
        {
            get
            {
                return this.lowerThresholdFatalField;
            }
            set
            {
                this.lowerThresholdFatalField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorLowerThresholdNonCritical LowerThresholdNonCritical
        {
            get
            {
                return this.lowerThresholdNonCriticalField;
            }
            set
            {
                this.lowerThresholdNonCriticalField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorUpperThresholdCritical UpperThresholdCritical
        {
            get
            {
                return this.upperThresholdCriticalField;
            }
            set
            {
                this.upperThresholdCriticalField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorUpperThresholdFatal UpperThresholdFatal
        {
            get
            {
                return this.upperThresholdFatalField;
            }
            set
            {
                this.upperThresholdFatalField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorUpperThresholdNonCritical UpperThresholdNonCritical
        {
            get
            {
                return this.upperThresholdNonCriticalField;
            }
            set
            {
                this.upperThresholdNonCriticalField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorCurrentState CurrentState
        {
            get
            {
                return this.currentStateField;
            }
            set
            {
                this.currentStateField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorDeviceID DeviceID
        {
            get
            {
                return this.deviceIDField;
            }
            set
            {
                this.deviceIDField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorCaption Caption
        {
            get
            {
                return this.captionField;
            }
            set
            {
                this.captionField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorDescription Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorElementName ElementName
        {
            get
            {
                return this.elementNameField;
            }
            set
            {
                this.elementNameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryIMMNumericSensorHealthState HealthState
        {
            get
            {
                return this.healthStateField;
            }
            set
            {
                this.healthStateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorNodeID
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorBaseUnit
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorCurrentReading
    {

        private string displayField;

        private decimal valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public decimal value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorLowerThresholdCritical
    {

        private string displayField;

        private ushort valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public ushort value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorLowerThresholdFatal
    {

        private string displayField;

        private float valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public float value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorLowerThresholdNonCritical
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorUpperThresholdCritical
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorUpperThresholdFatal
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorUpperThresholdNonCritical
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorCurrentState
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorDeviceID
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorCaption
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorDescription
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorElementName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryIMMNumericSensorHealthState
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryXFirmwareBuildID
    {

        private string displayField;

        private onecliRepositoryXFirmwareBuildIDName nameField;

        private onecliRepositoryXFirmwareBuildIDBuildID buildIDField;

        private onecliRepositoryXFirmwareBuildIDBuildDate buildDateField;

        private onecliRepositoryXFirmwareBuildIDBuildIDBackup buildIDBackupField;

        private onecliRepositoryXFirmwareBuildIDBuildDateBackup buildDateBackupField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryXFirmwareBuildIDName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryXFirmwareBuildIDBuildID BuildID
        {
            get
            {
                return this.buildIDField;
            }
            set
            {
                this.buildIDField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryXFirmwareBuildIDBuildDate BuildDate
        {
            get
            {
                return this.buildDateField;
            }
            set
            {
                this.buildDateField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryXFirmwareBuildIDBuildIDBackup BuildIDBackup
        {
            get
            {
                return this.buildIDBackupField;
            }
            set
            {
                this.buildIDBackupField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryXFirmwareBuildIDBuildDateBackup BuildDateBackup
        {
            get
            {
                return this.buildDateBackupField;
            }
            set
            {
                this.buildDateBackupField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryXFirmwareBuildIDName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryXFirmwareBuildIDBuildID
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryXFirmwareBuildIDBuildDate
    {

        private string displayField;

        private uint valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public uint value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryXFirmwareBuildIDBuildIDBackup
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryXFirmwareBuildIDBuildDateBackup
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryXFirmwareOtherVPD
    {

        private string displayField;

        private onecliRepositoryXFirmwareOtherVPDName nameField;

        private onecliRepositoryXFirmwareOtherVPDFRUNumber fRUNumberField;

        private onecliRepositoryXFirmwareOtherVPDPartNumber partNumberField;

        private onecliRepositoryXFirmwareOtherVPDSerialNumber serialNumberField;

        private onecliRepositoryXFirmwareOtherVPDVendor vendorField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryXFirmwareOtherVPDName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryXFirmwareOtherVPDFRUNumber FRUNumber
        {
            get
            {
                return this.fRUNumberField;
            }
            set
            {
                this.fRUNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryXFirmwareOtherVPDPartNumber PartNumber
        {
            get
            {
                return this.partNumberField;
            }
            set
            {
                this.partNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryXFirmwareOtherVPDSerialNumber SerialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryXFirmwareOtherVPDVendor Vendor
        {
            get
            {
                return this.vendorField;
            }
            set
            {
                this.vendorField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryXFirmwareOtherVPDName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryXFirmwareOtherVPDFRUNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryXFirmwareOtherVPDPartNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryXFirmwareOtherVPDSerialNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryXFirmwareOtherVPDVendor
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIAdapter
    {

        private string displayField;

        private byte idField;

        private string tooltipField;

        private onecliRepositoryPCIAdapterCaption captionField;

        private onecliRepositoryPCIAdapterName nameField;

        private onecliRepositoryPCIAdapterTag tagField;

        private onecliRepositoryPCIAdapterManufacturer manufacturerField;

        private onecliRepositoryPCIAdapterModel modelField;

        private onecliRepositoryPCIAdapterSerialNumber serialNumberField;

        private onecliRepositoryPCIAdapterPartNumber partNumberField;

        private onecliRepositoryPCIAdapterFRUNumber fRUNumberField;

        private onecliRepositoryPCIAdapterOperationalStatus operationalStatusField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string tooltip
        {
            get
            {
                return this.tooltipField;
            }
            set
            {
                this.tooltipField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIAdapterCaption Caption
        {
            get
            {
                return this.captionField;
            }
            set
            {
                this.captionField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIAdapterName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIAdapterTag Tag
        {
            get
            {
                return this.tagField;
            }
            set
            {
                this.tagField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIAdapterManufacturer Manufacturer
        {
            get
            {
                return this.manufacturerField;
            }
            set
            {
                this.manufacturerField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIAdapterModel Model
        {
            get
            {
                return this.modelField;
            }
            set
            {
                this.modelField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIAdapterSerialNumber SerialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIAdapterPartNumber PartNumber
        {
            get
            {
                return this.partNumberField;
            }
            set
            {
                this.partNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIAdapterFRUNumber FRUNumber
        {
            get
            {
                return this.fRUNumberField;
            }
            set
            {
                this.fRUNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIAdapterOperationalStatus OperationalStatus
        {
            get
            {
                return this.operationalStatusField;
            }
            set
            {
                this.operationalStatusField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIAdapterCaption
    {

        private string displayField;

        private object valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public object value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIAdapterName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIAdapterTag
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIAdapterManufacturer
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIAdapterModel
    {

        private string displayField;

        private ushort valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public ushort value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIAdapterSerialNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIAdapterPartNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIAdapterFRUNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIAdapterOperationalStatus
    {

        private string displayField;

        private ushort valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public ushort value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPort
    {

        private string displayField;

        private string tooltipField;

        private string referenceField;

        private onecliRepositoryPCIPortCaption captionField;

        private onecliRepositoryPCIPortPortNumber portNumberField;

        private onecliRepositoryPCIPortName nameField;

        private onecliRepositoryPCIPortLinkTechnology linkTechnologyField;

        private onecliRepositoryPCIPortLinkStatus linkStatusField;

        private onecliRepositoryPCIPortPermanentAddress permanentAddressField;

        private onecliRepositoryPCIPortMaxSpeed maxSpeedField;

        private onecliRepositoryPCIPortSpeed speedField;

        private onecliRepositoryPCIPortEnabledState enabledStateField;

        private onecliRepositoryPCIPortPortAvailability portAvailabilityField;

        private onecliRepositoryPCIPortRequestedState requestedStateField;

        private onecliRepositoryPCIPortTransitioningToState transitioningToStateField;

        private onecliRepositoryPCIPortOtherIdentifyingInfo otherIdentifyingInfoField;

        private onecliRepositoryPCIPortBurnedInMAC burnedInMACField;

        private onecliRepositoryPCIPortConfiguredLAMA configuredLAMAField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string tooltip
        {
            get
            {
                return this.tooltipField;
            }
            set
            {
                this.tooltipField = value;
            }
        }

        /// <remarks/>
        public string reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortCaption Caption
        {
            get
            {
                return this.captionField;
            }
            set
            {
                this.captionField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortPortNumber PortNumber
        {
            get
            {
                return this.portNumberField;
            }
            set
            {
                this.portNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortLinkTechnology LinkTechnology
        {
            get
            {
                return this.linkTechnologyField;
            }
            set
            {
                this.linkTechnologyField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortLinkStatus LinkStatus
        {
            get
            {
                return this.linkStatusField;
            }
            set
            {
                this.linkStatusField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortPermanentAddress PermanentAddress
        {
            get
            {
                return this.permanentAddressField;
            }
            set
            {
                this.permanentAddressField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortMaxSpeed MaxSpeed
        {
            get
            {
                return this.maxSpeedField;
            }
            set
            {
                this.maxSpeedField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortSpeed Speed
        {
            get
            {
                return this.speedField;
            }
            set
            {
                this.speedField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortEnabledState EnabledState
        {
            get
            {
                return this.enabledStateField;
            }
            set
            {
                this.enabledStateField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortPortAvailability PortAvailability
        {
            get
            {
                return this.portAvailabilityField;
            }
            set
            {
                this.portAvailabilityField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortRequestedState RequestedState
        {
            get
            {
                return this.requestedStateField;
            }
            set
            {
                this.requestedStateField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortTransitioningToState TransitioningToState
        {
            get
            {
                return this.transitioningToStateField;
            }
            set
            {
                this.transitioningToStateField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortOtherIdentifyingInfo OtherIdentifyingInfo
        {
            get
            {
                return this.otherIdentifyingInfoField;
            }
            set
            {
                this.otherIdentifyingInfoField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortBurnedInMAC BurnedInMAC
        {
            get
            {
                return this.burnedInMACField;
            }
            set
            {
                this.burnedInMACField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCIPortConfiguredLAMA ConfiguredLAMA
        {
            get
            {
                return this.configuredLAMAField;
            }
            set
            {
                this.configuredLAMAField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortCaption
    {

        private string displayField;

        private object valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public object value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortPortNumber
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortLinkTechnology
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortLinkStatus
    {

        private string displayField;

        private object valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public object value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortPermanentAddress
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortMaxSpeed
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortSpeed
    {

        private string displayField;

        private object valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public object value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortEnabledState
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortPortAvailability
    {

        private string displayField;

        private object valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public object value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortRequestedState
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortTransitioningToState
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortOtherIdentifyingInfo
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortBurnedInMAC
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCIPortConfiguredLAMA
    {

        private string displayField;

        private object valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public object value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentity
    {

        private string displayField;

        private object tooltipField;

        private string referenceField;

        private onecliRepositoryPCISoftwareIdentityDescription descriptionField;

        private onecliRepositoryPCISoftwareIdentityElementName elementNameField;

        private onecliRepositoryPCISoftwareIdentityProductName productNameField;

        private onecliRepositoryPCISoftwareIdentityManufacturer manufacturerField;

        private onecliRepositoryPCISoftwareIdentityName nameField;

        private onecliRepositoryPCISoftwareIdentityVersionString versionStringField;

        private onecliRepositoryPCISoftwareIdentityClassifications classificationsField;

        private onecliRepositoryPCISoftwareIdentityClassificationDescriptions classificationDescriptionsField;

        private onecliRepositoryPCISoftwareIdentityIdentityInfoValue identityInfoValueField;

        private onecliRepositoryPCISoftwareIdentitySoftwareID softwareIDField;

        private onecliRepositoryPCISoftwareIdentitySubDeviceID subDeviceIDField;

        private onecliRepositoryPCISoftwareIdentityReleaseDate releaseDateField;

        private onecliRepositoryPCISoftwareIdentitySoftwareStatus softwareStatusField;

        private onecliRepositoryPCISoftwareIdentitySoftwareRole softwareRoleField;

        private onecliRepositoryPCISoftwareIdentityIdentityInfoType identityInfoTypeField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public object tooltip
        {
            get
            {
                return this.tooltipField;
            }
            set
            {
                this.tooltipField = value;
            }
        }

        /// <remarks/>
        public string reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentityDescription Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentityElementName ElementName
        {
            get
            {
                return this.elementNameField;
            }
            set
            {
                this.elementNameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentityProductName ProductName
        {
            get
            {
                return this.productNameField;
            }
            set
            {
                this.productNameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentityManufacturer Manufacturer
        {
            get
            {
                return this.manufacturerField;
            }
            set
            {
                this.manufacturerField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentityName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentityVersionString VersionString
        {
            get
            {
                return this.versionStringField;
            }
            set
            {
                this.versionStringField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentityClassifications Classifications
        {
            get
            {
                return this.classificationsField;
            }
            set
            {
                this.classificationsField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentityClassificationDescriptions ClassificationDescriptions
        {
            get
            {
                return this.classificationDescriptionsField;
            }
            set
            {
                this.classificationDescriptionsField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentityIdentityInfoValue IdentityInfoValue
        {
            get
            {
                return this.identityInfoValueField;
            }
            set
            {
                this.identityInfoValueField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentitySoftwareID SoftwareID
        {
            get
            {
                return this.softwareIDField;
            }
            set
            {
                this.softwareIDField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentitySubDeviceID SubDeviceID
        {
            get
            {
                return this.subDeviceIDField;
            }
            set
            {
                this.subDeviceIDField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentityReleaseDate ReleaseDate
        {
            get
            {
                return this.releaseDateField;
            }
            set
            {
                this.releaseDateField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentitySoftwareStatus SoftwareStatus
        {
            get
            {
                return this.softwareStatusField;
            }
            set
            {
                this.softwareStatusField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentitySoftwareRole SoftwareRole
        {
            get
            {
                return this.softwareRoleField;
            }
            set
            {
                this.softwareRoleField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryPCISoftwareIdentityIdentityInfoType IdentityInfoType
        {
            get
            {
                return this.identityInfoTypeField;
            }
            set
            {
                this.identityInfoTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentityDescription
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentityElementName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentityProductName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentityManufacturer
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentityName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentityVersionString
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentityClassifications
    {

        private string displayField;

        private ushort valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public ushort value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentityClassificationDescriptions
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentityIdentityInfoValue
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentitySoftwareID
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentitySubDeviceID
    {

        private string displayField;

        private object valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public object value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentityReleaseDate
    {

        private string displayField;

        private object valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public object value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentitySoftwareStatus
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentitySoftwareRole
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryPCISoftwareIdentityIdentityInfoType
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageDevice
    {

        private string displayField;

        private byte idField;

        private string tooltipField;

        private onecliRepositoryStorageDeviceName nameField;

        private onecliRepositoryStorageDeviceModel modelField;

        private onecliRepositoryStorageDeviceSerialNum serialNumField;

        private onecliRepositoryStorageDeviceManufacturer manufacturerField;

        private onecliRepositoryStorageDevicePartNumber partNumberField;

        private onecliRepositoryStorageDeviceFRUNumber fRUNumberField;

        private onecliRepositoryStorageDeviceUUID uUIDField;

        private onecliRepositoryStorageDeviceCacheStatus cacheStatusField;

        private onecliRepositoryStorageDeviceCacheSerialNum cacheSerialNumField;

        private onecliRepositoryStorageDeviceCacheMemorySize cacheMemorySizeField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string tooltip
        {
            get
            {
                return this.tooltipField;
            }
            set
            {
                this.tooltipField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageDeviceName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageDeviceModel Model
        {
            get
            {
                return this.modelField;
            }
            set
            {
                this.modelField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageDeviceSerialNum SerialNum
        {
            get
            {
                return this.serialNumField;
            }
            set
            {
                this.serialNumField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageDeviceManufacturer Manufacturer
        {
            get
            {
                return this.manufacturerField;
            }
            set
            {
                this.manufacturerField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageDevicePartNumber PartNumber
        {
            get
            {
                return this.partNumberField;
            }
            set
            {
                this.partNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageDeviceFRUNumber FRUNumber
        {
            get
            {
                return this.fRUNumberField;
            }
            set
            {
                this.fRUNumberField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageDeviceUUID UUID
        {
            get
            {
                return this.uUIDField;
            }
            set
            {
                this.uUIDField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageDeviceCacheStatus CacheStatus
        {
            get
            {
                return this.cacheStatusField;
            }
            set
            {
                this.cacheStatusField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageDeviceCacheSerialNum CacheSerialNum
        {
            get
            {
                return this.cacheSerialNumField;
            }
            set
            {
                this.cacheSerialNumField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageDeviceCacheMemorySize CacheMemorySize
        {
            get
            {
                return this.cacheMemorySizeField;
            }
            set
            {
                this.cacheMemorySizeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageDeviceName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageDeviceModel
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageDeviceSerialNum
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageDeviceManufacturer
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageDevicePartNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageDeviceFRUNumber
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageDeviceUUID
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageDeviceCacheStatus
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageDeviceCacheSerialNum
    {

        private string displayField;

        private object valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public object value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageDeviceCacheMemorySize
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentity
    {

        private string displayField;

        private string tooltipField;

        private string referenceField;

        private onecliRepositoryStorageSoftwareIdentityDescription descriptionField;

        private onecliRepositoryStorageSoftwareIdentityElementName elementNameField;

        private onecliRepositoryStorageSoftwareIdentityProductName productNameField;

        private onecliRepositoryStorageSoftwareIdentityManufacturer manufacturerField;

        private onecliRepositoryStorageSoftwareIdentityName nameField;

        private onecliRepositoryStorageSoftwareIdentityVersionString versionStringField;

        private onecliRepositoryStorageSoftwareIdentityClassifications classificationsField;

        private onecliRepositoryStorageSoftwareIdentityClassificationDescriptions classificationDescriptionsField;

        private onecliRepositoryStorageSoftwareIdentityIdentityInfoValue identityInfoValueField;

        private onecliRepositoryStorageSoftwareIdentitySoftwareID softwareIDField;

        private onecliRepositoryStorageSoftwareIdentitySubDeviceID subDeviceIDField;

        private onecliRepositoryStorageSoftwareIdentityReleaseDate releaseDateField;

        private onecliRepositoryStorageSoftwareIdentitySoftwareStatus softwareStatusField;

        private onecliRepositoryStorageSoftwareIdentitySoftwareRole softwareRoleField;

        private onecliRepositoryStorageSoftwareIdentityIdentityInfoType identityInfoTypeField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string tooltip
        {
            get
            {
                return this.tooltipField;
            }
            set
            {
                this.tooltipField = value;
            }
        }

        /// <remarks/>
        public string reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentityDescription Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentityElementName ElementName
        {
            get
            {
                return this.elementNameField;
            }
            set
            {
                this.elementNameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentityProductName ProductName
        {
            get
            {
                return this.productNameField;
            }
            set
            {
                this.productNameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentityManufacturer Manufacturer
        {
            get
            {
                return this.manufacturerField;
            }
            set
            {
                this.manufacturerField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentityName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentityVersionString VersionString
        {
            get
            {
                return this.versionStringField;
            }
            set
            {
                this.versionStringField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentityClassifications Classifications
        {
            get
            {
                return this.classificationsField;
            }
            set
            {
                this.classificationsField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentityClassificationDescriptions ClassificationDescriptions
        {
            get
            {
                return this.classificationDescriptionsField;
            }
            set
            {
                this.classificationDescriptionsField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentityIdentityInfoValue IdentityInfoValue
        {
            get
            {
                return this.identityInfoValueField;
            }
            set
            {
                this.identityInfoValueField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentitySoftwareID SoftwareID
        {
            get
            {
                return this.softwareIDField;
            }
            set
            {
                this.softwareIDField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentitySubDeviceID SubDeviceID
        {
            get
            {
                return this.subDeviceIDField;
            }
            set
            {
                this.subDeviceIDField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentityReleaseDate ReleaseDate
        {
            get
            {
                return this.releaseDateField;
            }
            set
            {
                this.releaseDateField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentitySoftwareStatus SoftwareStatus
        {
            get
            {
                return this.softwareStatusField;
            }
            set
            {
                this.softwareStatusField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentitySoftwareRole SoftwareRole
        {
            get
            {
                return this.softwareRoleField;
            }
            set
            {
                this.softwareRoleField = value;
            }
        }

        /// <remarks/>
        public onecliRepositoryStorageSoftwareIdentityIdentityInfoType IdentityInfoType
        {
            get
            {
                return this.identityInfoTypeField;
            }
            set
            {
                this.identityInfoTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentityDescription
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentityElementName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentityProductName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentityManufacturer
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentityName
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentityVersionString
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentityClassifications
    {

        private string displayField;

        private byte valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public byte value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentityClassificationDescriptions
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentityIdentityInfoValue
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentitySoftwareID
    {

        private string displayField;

        private uint valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public uint value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentitySubDeviceID
    {

        private string displayField;

        private object valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public object value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentityReleaseDate
    {

        private string displayField;

        private uint valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public uint value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentitySoftwareStatus
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentitySoftwareRole
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliRepositoryStorageSoftwareIdentityIdentityInfoType
    {

        private string displayField;

        private string valueField;

        /// <remarks/>
        public string display
        {
            get
            {
                return this.displayField;
            }
            set
            {
                this.displayField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliSCAN
    {

        private decimal xMLVERSIONField;

        private onecliSCANCONTENT cONTENTField;

        /// <remarks/>
        public decimal XMLVERSION
        {
            get
            {
                return this.xMLVERSIONField;
            }
            set
            {
                this.xMLVERSIONField = value;
            }
        }

        /// <remarks/>
        public onecliSCANCONTENT CONTENT
        {
            get
            {
                return this.cONTENTField;
            }
            set
            {
                this.cONTENTField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliSCANCONTENT
    {

        private string mtField;

        private string snField;

        private string bMCTYPEField;

        private string osField;

        private string aRCHField;

        private byte iSPARTITIONField;

        private byte tOTALField;

        private onecliSCANCONTENTPACKAGE[] pACKAGESField;

        /// <remarks/>
        public string MT
        {
            get
            {
                return this.mtField;
            }
            set
            {
                this.mtField = value;
            }
        }

        /// <remarks/>
        public string SN
        {
            get
            {
                return this.snField;
            }
            set
            {
                this.snField = value;
            }
        }

        /// <remarks/>
        public string BMCTYPE
        {
            get
            {
                return this.bMCTYPEField;
            }
            set
            {
                this.bMCTYPEField = value;
            }
        }

        /// <remarks/>
        public string OS
        {
            get
            {
                return this.osField;
            }
            set
            {
                this.osField = value;
            }
        }

        /// <remarks/>
        public string ARCH
        {
            get
            {
                return this.aRCHField;
            }
            set
            {
                this.aRCHField = value;
            }
        }

        /// <remarks/>
        public byte ISPARTITION
        {
            get
            {
                return this.iSPARTITIONField;
            }
            set
            {
                this.iSPARTITIONField = value;
            }
        }

        /// <remarks/>
        public byte TOTAL
        {
            get
            {
                return this.tOTALField;
            }
            set
            {
                this.tOTALField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("PACKAGE", IsNullable = false)]
        public onecliSCANCONTENTPACKAGE[] PACKAGES
        {
            get
            {
                return this.pACKAGESField;
            }
            set
            {
                this.pACKAGESField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class onecliSCANCONTENTPACKAGE
    {

        private string nAMEField;

        private string sOFTWAREIDField;

        private string vERSIONField;

        private string cLASSIFICATIONField;

        private string dESCRIPTIONField;

        private string iNSTANCEIDField;

        private string sLOTNUMField;

        private byte sLOTTYPEField;

        private byte aGENTLESSSUPPORTField;

        /// <remarks/>
        public string NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }

        /// <remarks/>
        public string SOFTWAREID
        {
            get
            {
                return this.sOFTWAREIDField;
            }
            set
            {
                this.sOFTWAREIDField = value;
            }
        }

        /// <remarks/>
        public string VERSION
        {
            get
            {
                return this.vERSIONField;
            }
            set
            {
                this.vERSIONField = value;
            }
        }

        /// <remarks/>
        public string CLASSIFICATION
        {
            get
            {
                return this.cLASSIFICATIONField;
            }
            set
            {
                this.cLASSIFICATIONField = value;
            }
        }

        /// <remarks/>
        public string DESCRIPTION
        {
            get
            {
                return this.dESCRIPTIONField;
            }
            set
            {
                this.dESCRIPTIONField = value;
            }
        }

        /// <remarks/>
        public string INSTANCEID
        {
            get
            {
                return this.iNSTANCEIDField;
            }
            set
            {
                this.iNSTANCEIDField = value;
            }
        }

        /// <remarks/>
        public string SLOTNUM
        {
            get
            {
                return this.sLOTNUMField;
            }
            set
            {
                this.sLOTNUMField = value;
            }
        }

        /// <remarks/>
        public byte SLOTTYPE
        {
            get
            {
                return this.sLOTTYPEField;
            }
            set
            {
                this.sLOTTYPEField = value;
            }
        }

        /// <remarks/>
        public byte AGENTLESSSUPPORT
        {
            get
            {
                return this.aGENTLESSSUPPORTField;
            }
            set
            {
                this.aGENTLESSSUPPORTField = value;
            }
        }
    }


}