﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;
using Nebula.Views;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms.Integration;
using System.Threading;

//using TikaOnDotNet.TextExtraction;

namespace Nebula
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {

        // Here we create the viewmodel with the current DialogCoordinator instance 
        MainViewModel vm = new MainViewModel(DialogCoordinator.Instance);
        MetroDialogSettings mySettings;
        //GoodsInOutViewModel vm2 = new GoodsInOutViewModel();
        //DriveTestViewModel vm3 = new DriveTestViewModel();

        //app location \\pinnacle.local\tech_resources\Nebula


        public MainWindow()
        {
            try
            {


                DataContext = vm;
                InitializeComponent();



                ////Set language based on location
                SetLanguageDictionary();

                //Get GUID
                Console.WriteLine(Assembly.GetExecutingAssembly().GetCustomAttribute<GuidAttribute>().Value.ToUpper());

                // SET THE GLOBAL VARIABLE FOR FILE PATH FROM THE APP.CONFIG FILE APPLICATION SETTINGS REQUIRES REF TO SYSTEM.CONFIGURATION TO USE CONFIGURATIONMANAGER
                var appSettings = ConfigurationManager.AppSettings;
                StaticFunctions.UncPathToUse = appSettings.Get("AppPathStr");
                //string version = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();
                //System.Reflection.Assembly executingAssembly = System.Reflection.Assembly.GetExecutingAssembly();
                //var fieVersionInfo = FileVersionInfo.GetVersionInfo(executingAssembly.Location);
                //var version = fieVersionInfo.FileVersion;

                //var version = Windows.ApplicationModel.Package.Current.Id.Version;
                //applicationVersion = string.Format("{0}.{1}.{2}.{3}",
                //    version.Major,
                //    version.Minor,
                //    version.Build,
                //    version.Revision);

                //Animate the star
                //AnimateStar();

                ////MessageBox.Show(version);
                //this.Title = "Nebula (build 1.0.93)";
                BuildInfo.Text = "NEBULA (BUILD 1.0.141) " + vm.FullUser.Replace("Technician Name: ", "").Replace(DateTime.Now.ToShortDateString().ToUpper(), "") + " " + vm.CurrentUserLocation.Trim();


                // ***** SHOW OR HIDE TEST MODE OPTION *****
                // Hide Test Mode to other users
                if (vm.FullUser.Contains("N.Myers"))
                {
                    //Show the checkbox
                    TestModeCB.Visibility = Visibility.Visible;
                    //Enable Cedar option for Testing
                    CedarInteractBTN.Visibility = Visibility.Visible;
                }
                else
                {
                    //Hide for all other users
                    TestModeCB.Visibility = Visibility.Hidden;
                }


                // CLEAR NEBULA LOGS AND SHOW SYNC SERVER UPDATES BUTTON Width="1360" Height="768"
                //Show Server Update Sync Button Restrict To George Gallasso and Myself
                if (vm.CurrentUser.Contains("NM") || vm.CurrentUser.Contains("GG") || vm.FullUser.Contains("N.Myers") || vm.FullUser.Contains("G.Galasso"))
                {
                    FolderSyncBTN.Visibility = Visibility.Visible;

                    if (vm.FullUser.Contains("G.Galasso"))
                    {
                        //Reduce font size for zoomed user
                        //MainWin1.FontSize = 8;
                        //Set label flyout size based on zoom user
                        LabelPrintingFlyout.Width = 1360;
                        LabelPrintingFlyout.Height = 768;

                    }
                    else
                    {
                        //Set label flyout size based on zoom user
                        LabelPrintingFlyout.Width = 1650;
                        LabelPrintingFlyout.Height = 900;
                    }


                    if (Directory.Exists(@"E:\Nebula_Sync_Logs"))
                    {
                        vm.ClearNebulaLogs(@"E:\Nebula_Sync_Logs");
                    }
                    else
                    {
                        vm.ClearNebulaLogs(@"" + vm.myDocs + @"\Nebula Logs");
                    }

                }
                else
                {
                    //Set label flyout size based on zoom user
                    LabelPrintingFlyout.Width = 1650;
                    LabelPrintingFlyout.Height = 900;
                    vm.ClearNebulaLogs(@"" + vm.myDocs + @"\Nebula Logs");
                }


                //Check if HPTOOLS Exists
                if (Directory.Exists(@"" + vm.myDocs + @"\HPTOOLS"))
                {

                }
                else
                {
                    Directory.CreateDirectory(@"" + vm.myDocs + @"\HPTOOLS");
                }


                //Check if Logs Folder Exists
                if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                {

                }
                else
                {
                    Directory.CreateDirectory(vm.myDocs + @"\Nebula Logs");
                }

                //Sync the tools in folder
                SyncNebulaTools();


                //Delete any temporary QR Codes
                if (Directory.Exists(vm.myDocs + @"\Nebula Logs\QR"))
                {
                    foreach (var file in Directory.GetFiles(vm.myDocs + @"\Nebula Logs\QR"))
                    {
                        //MessageBox.Show(file);
                        //Remove temp QR
                        if (File.Exists(file))
                        {
                            File.Delete(file);

                        }
                    }
                }
                else
                {
                    if (Directory.Exists(vm.myDocs + @"\Nebula Logs"))
                    {
                        //Create QR Folder under Nebula Logs to store temp QR Images
                        Directory.CreateDirectory(vm.myDocs + @"\Nebula Logs\QR");
                    }
                }




                //Create instance of part code generator
                vm.FlyOutContent3 = new PartCodeGeneratorView(vm);

                //Create new instance of label printing
                vm.FlyOutContent4 = new BarCodePrinterView(vm);

                //Create new instance of Sync Server Updates 
                vm.FlyOutContent5 = new SyncServerUpdatesView(vm);


                //Read and extract Pdf Text (USES TIKA ON DET NET)
                // ExtractPDFText();



            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
            // MessageBox.Show(StaticFunctions.UncPathToUse);
            // this.Flyout1.IsOpen = !this.Flyout1.IsOpen;
        }


        private void KillIlorestProcesses()
        {

            try
            {
                Process[] hpUtil = Process.GetProcessesByName("ilorest");
                //MessageBox.Show(proc.ProcessName);
                foreach (var process in hpUtil)
                {
                    process.Kill();
                }

                Process[] hpRCUtil = Process.GetProcessesByName("HPLOCONS");
                //MessageBox.Show(proc.ProcessName);
                foreach (var process in hpUtil)
                {
                    process.Kill();
                }

                Process[] dellUtil = Process.GetProcessesByName("racadm");
                //MessageBox.Show(proc.ProcessName);
                foreach (var process in dellUtil)
                {
                    process.Kill();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }



        //public async void ExtractPDFText()
        //{

        //    // using TikaOnDotNet.TextExtraction;

        //    var textExtractor = new TextExtractor();

        //    //Examples
        //    //var PDFContents = textExtractor.Extract(@"C:\Users\n.myers\Documents\Josh Blanco PDF\3Par.pdf");
        //    //var webPageContents = textExtractor.Extract(new Uri("https://google.com"));
        //    // Console.WriteLine(PDFContents.Text);


        //    //PDFContents.Text += textExtractor.Extract(@"C:\Users\n.myers\Documents\Josh Blanco PDF\3ParResume.pdf");
        //    //File To Write Too
        //    string fileName = @"C:\Users\n.myers\Documents\Josh Blanco PDF\Output\TextInPdfs.txt";


        //    if(File.Exists(@"C:\Users\n.myers\Documents\Josh Blanco PDF\Output\TextInPdfs.txt"))
        //    {
        //        File.Delete(@"C:\Users\n.myers\Documents\Josh Blanco PDF\Output\TextInPdfs.txt");
        //    }
        //    else
        //    {
        //        File.Create(@"C:\Users\n.myers\Documents\Josh Blanco PDF\Output\TextInPdfs.txt");
        //    }

        //    string Contents = "";

        //    foreach (string file in Directory.GetFiles(@"C:\Users\n.myers\Documents\Josh Blanco PDF"))
        //    {
        //        var PDFContents = textExtractor.Extract(file);


        //        Contents += PDFContents.Text.ToString();

        //        await PutTaskDelay(300);

        //        Console.WriteLine(file);
        //    }

        //   // File.WriteAllText(fileName, PDFContents.Text.ToString());
        //   //write all extracted text
        //    File.WriteAllText(fileName, Contents);


        //    //Reset
        //    string CSVContents = "";

        //    string CSVfileName = @"C:\Users\n.myers\Documents\Josh Blanco PDF\Output\BlancoDataExtract.csv";

        //    //Add first line as headers
        //    CSVContents = "Date,Time,Result,Serial,Capacity,Size,Software,Software,Software,\n";

        //    // Read and show each line from the file.
        //    string line = "";
        //    using (StreamReader sr = new StreamReader(@"C:\Users\n.myers\Documents\Josh Blanco PDF\Output\TextInPdfs.txt"))
        //    {
        //        while ((line = sr.ReadLine()) != null)
        //        {
        //            if (line.Contains("Blancco Drive Eraser") && !line.Contains("Version:"))
        //            {
        //                if (line.Contains("/ failed failed") || line.Contains("/ Successful failed") || line.Contains("/ failed Successful") || line.Contains("/ Successful Successful"))
        //                {
        //                    //Console.WriteLine(line.Replace(" ",",")); failed,Successful
        //                    //Any double spaces replace with Comma
        //                    CSVContents += line.Replace(@" / ", "")
        //                        .Replace(@"Successful failed", "Successful/Failed")
        //                        .Replace(@"failed failed", "Failed/Failed")
        //                        .Replace(@"failed Successful", "Failed/Successful")
        //                        .Replace(@"Successful Successful", "Successful/Successful")
        //                        .Replace(" ", ",")
        //                        + "\n";

        //                }
        //                else
        //                {
        //                    //Normal Pass
        //                    //Any double spaces replace with Comma
        //                    CSVContents += line.Replace(" ", ",").Replace(@"/", "") + "\n";
        //                }

        //            }


        //            //MessageBox.Show(line.Substring(7));
        //        }
        //    }

        //    //Write out as CSV
        //    File.WriteAllText(CSVfileName, CSVContents);

        //    if(File.Exists(@"C:\Users\n.myers\Documents\Josh Blanco PDF\Output\TextInPdfs.txt"))
        //    {
        //        File.Delete(@"C:\Users\n.myers\Documents\Josh Blanco PDF\Output\TextInPdfs.txt");
        //    }

        //}

        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }





        private void ClearToolsBTN_Click(object sender, RoutedEventArgs e)
        {
            //DELETE HPTOOLS ROOT FOLDER AND COPY NEW INSTANCE OF TOOLS OVER
            //OLD
            //await System.Threading.Tasks.Task.Run(() => vm.DataToolsCopy(true));

            SyncNebulaTools();
            //Kill any locked processes running
            KillIlorestProcesses();

        }


        private async void SyncNebulaTools()
        {
            // Update the Tools Folder
            await System.Threading.Tasks.Task.Run(() => vm.SyncDirectory(@"" + @"\\pinnacle.local\tech_resources\Nebula\HP Tools", @"" + vm.myDocs + @"\HPTOOLS", true));

            vm.SyncLog = "";
        }




        private async void NebulaStars()
        {
            //Call the main animation on a background thread
            await System.Threading.Tasks.Task.Run(() => AnimateStar());
        }


        private void AnimateStar()
        {
            ColorAnimation animation;
            ColorAnimation animation2;
            ColorAnimation animation3;
            ColorAnimation animation4;
            ColorAnimation animation5;
            ColorAnimation animation6;
            ColorAnimation animation7;
            ColorAnimation animation8;
            animation = new ColorAnimation();
            animation2 = new ColorAnimation();
            animation3 = new ColorAnimation();
            animation4 = new ColorAnimation();
            animation5 = new ColorAnimation();
            animation6 = new ColorAnimation();
            animation7 = new ColorAnimation();
            animation8 = new ColorAnimation();
            //this.starPath.Stroke = new SolidColorBrush(Colors.Transparent);
            //this.starPath.Fill = new SolidColorBrush(Colors.MediumPurple);
            this.Star1.Stroke = new SolidColorBrush(Colors.Transparent);
            this.Star1.Fill = new SolidColorBrush(Colors.Purple);
            this.Star2.Stroke = new SolidColorBrush(Colors.Transparent);
            this.Star2.Fill = new SolidColorBrush(Colors.Purple);
            this.Star3.Stroke = new SolidColorBrush(Colors.Transparent);
            this.Star3.Fill = new SolidColorBrush(Colors.Purple);
            this.Star4.Stroke = new SolidColorBrush(Colors.Transparent);
            this.Star4.Fill = new SolidColorBrush(Colors.Purple);
            this.Star5.Stroke = new SolidColorBrush(Colors.Transparent);
            this.Star5.Fill = new SolidColorBrush(Colors.Purple);
            this.Star6.Stroke = new SolidColorBrush(Colors.Transparent);
            this.Star6.Fill = new SolidColorBrush(Colors.Purple);
            this.Star7.Stroke = new SolidColorBrush(Colors.Transparent);
            this.Star7.Fill = new SolidColorBrush(Colors.Purple);
            this.Star8.Stroke = new SolidColorBrush(Colors.Transparent);
            this.Star8.Fill = new SolidColorBrush(Colors.Purple);
            //this.Star9.Stroke = new SolidColorBrush(Colors.Transparent);
            //this.Star9.Fill = new SolidColorBrush(Colors.Purple);
            //this.Star10.Stroke = new SolidColorBrush(Colors.Transparent);
            //this.Star10.Fill = new SolidColorBrush(Colors.Purple);
            //this.Star11.Stroke = new SolidColorBrush(Colors.Transparent);
            //this.Star11.Fill = new SolidColorBrush(Colors.Purple);
            //this.Star12.Stroke = new SolidColorBrush(Colors.Transparent);
            //this.Star12.Fill = new SolidColorBrush(Colors.Purple);
            //this.Star13.Stroke = new SolidColorBrush(Colors.Transparent);
            //this.Star13.Fill = new SolidColorBrush(Colors.Purple);
            animation.To = Colors.White;
            animation.Duration = new Duration(TimeSpan.FromMilliseconds(1100));
            animation.RepeatBehavior = RepeatBehavior.Forever;

            animation2.To = Colors.White;
            animation2.Duration = new Duration(TimeSpan.FromMilliseconds(1150));
            animation2.RepeatBehavior = RepeatBehavior.Forever;


            animation3.To = Colors.White;
            animation3.Duration = new Duration(TimeSpan.FromMilliseconds(1200));
            animation3.RepeatBehavior = RepeatBehavior.Forever;

            animation4.To = Colors.White;
            animation4.Duration = new Duration(TimeSpan.FromMilliseconds(1250));
            animation4.RepeatBehavior = RepeatBehavior.Forever;

            animation5.To = Colors.White;
            animation5.Duration = new Duration(TimeSpan.FromMilliseconds(1350));
            animation5.RepeatBehavior = RepeatBehavior.Forever;

            animation6.To = Colors.White;
            animation6.Duration = new Duration(TimeSpan.FromMilliseconds(1400));
            animation6.RepeatBehavior = RepeatBehavior.Forever;


            animation7.To = Colors.White;
            animation7.Duration = new Duration(TimeSpan.FromMilliseconds(1450));
            animation7.RepeatBehavior = RepeatBehavior.Forever;

            animation8.To = Colors.White;
            animation8.Duration = new Duration(TimeSpan.FromMilliseconds(1500));
            animation8.RepeatBehavior = RepeatBehavior.Forever;


            //this.starPath.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation);
            //this.starPath.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation);
            this.Star1.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation);
            this.Star1.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation);
            this.Star2.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation2);
            this.Star2.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation2);
            this.Star3.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation3);
            this.Star3.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation3);
            this.Star4.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation4);
            this.Star4.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation4);
            this.Star5.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation5);
            this.Star5.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation5);
            this.Star6.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation6);
            this.Star6.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation6);
            this.Star7.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation7);
            this.Star7.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation7);
            this.Star8.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation8);
            this.Star8.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation8);
            //this.Star9.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation);
            //this.Star9.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation);
            //this.Star10.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation2);
            //this.Star10.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation2);
            //this.Star11.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation3);
            //this.Star11.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation3);
            //this.Star12.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation4);
            //this.Star12.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation4);
            //this.Star13.Stroke.BeginAnimation(SolidColorBrush.ColorProperty, animation);
            //this.Star13.Fill.BeginAnimation(SolidColorBrush.ColorProperty, animation);


            animation.AutoReverse = true;
        }


        //public MetroDialogSettings mySettings = new MetroDialogSettings()
        //{
        //    AffirmativeButtonText = App.Current.FindResource("Yes").ToString(), //"Yes",
        //    NegativeButtonText = App.Current.FindResource("No").ToString(), //"No",
        //    FirstAuxiliaryButtonText = App.Current.FindResource("Cancel").ToString(), //"Cancel",
        //                                                                              //ColorScheme = UseAccentForDialogsMenuItem.IsChecked ? MetroDialogColorScheme.Accented : MetroDialogColorScheme.Theme
        //};

        public MetroDialogSettings GetMetroDialogSettings()
        {

            // Create new instance of Dialog Settings to support different languages
            mySettings = new MetroDialogSettings();
            mySettings.AffirmativeButtonText = App.Current.FindResource("Yes").ToString(); //"Yes"
            mySettings.NegativeButtonText = App.Current.FindResource("No").ToString(); //"No"
            mySettings.FirstAuxiliaryButtonText = App.Current.FindResource("Cancel").ToString(); //"Cancel"

            return mySettings;
        }


        //Intermediate bool for showing mahapp dialog on shutdown.
        bool want2Close = false;

        private async void MainWin1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            //if the remote console is visible then hide due to z order issue
            HideRemoteConsole();


            if (want2Close == false)
            {
                e.Cancel = true;

                //"Are you sure you want to exit?"
                MessageDialogResult result = await this.ShowMessageAsync(App.Current.FindResource("ExitAppTitle").ToString(), App.Current.FindResource("ExitApp").ToString(), MessageDialogStyle.AffirmativeAndNegative, mySettings);



                //YES CARRY ON AND CLOSE
                if (result != MessageDialogResult.Negative)
                {
                    want2Close = true;
                    //e.Cancel = false;
                    this.Close();
                }//NO CANCEL
                else if (result != MessageDialogResult.Affirmative)
                {
                    e.Cancel = true;
                }
            }
            else
            {
                //Close app
            }


            //OLD Message box method
            ////vm.QuestionDialogMessage("Close Nebula", "Are you sure you want to close the app?",e);
            //if (MessageBox.Show("Are you sure you want to exit Nebula?", "Close App", MessageBoxButton.YesNo) == MessageBoxResult.No)
            //{
            //    // e.Cancel = true;
            //}


        }

        private void MainWin1_Loaded(object sender, RoutedEventArgs e)
        {

            // vm.QuestionDialogMessage("Close Nebula", "Are you sure you want to close the app?");
        }

        private void HPEnclosureBTN_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void HPEnclosureBTN_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }
        private void HPEnclosureBTN_Click(object sender, RoutedEventArgs e)
        {
            //if the remote console is visible then hide due to z order issue
            HideRemoteConsole();

            if (vm.ServerType != null)
                if (vm.ServerType.Contains("HP") && vm.ServerType.Contains("Blade"))
                {
                    vm.FlyOutContent = new HPEnclosureView(vm);
                    vm.ShowHPFlyout = true;
                    //HPServerEnclosureFlyout.IsOpen = true;
                }


        }

        private void DELLEnclosureBTN_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void DELLEnclosureBTN_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void DELLEnclosureBTN_Click(object sender, RoutedEventArgs e)
        {
            //if the remote console is visible then hide due to z order issue
            HideRemoteConsole();

            if (vm.ServerType != null)
                if (vm.ServerType.Contains("DellBlade"))
                {
                    vm.FlyOutContent2 = new DELLEnclosureView(vm);
                    vm.ShowDELLFlyout = true;
                    //HPServerEnclosureFlyout.IsOpen = true;
                }

        }


        private void FolderSyncBTN_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void FolderSyncBTN_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void FolderSyncBTN_Click(object sender, RoutedEventArgs e)
        {
            //if the remote console is visible then hide due to z order issue
            HideRemoteConsole();
            //When clicked 
            //Clear SyncLog
            vm.SyncLog = "";
            //Clear Message
            vm.SyncServerUpdatesMessage = "";
            //Clear Message
            vm.SyncTimeElapsed = "Time Elapsed";

            vm.ShowSyncServerUpdatesLayout = true;
        }

        private void PartNumberGeneratorBTN_Click(object sender, RoutedEventArgs e)
        {

            //if the remote console is visible then hide due to z order issue
            HideRemoteConsole();


            vm.ShowPartNumberGeneratorLayout = true;
            //HPServerEnclosureFlyout.IsOpen = true;

        }

        private void PartNumberGeneratorBTN_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void PartNumberGeneratorBTN_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void LabelPrintingBTN_Click(object sender, RoutedEventArgs e)
        {
            //if the remote console is visible then hide due to z order issue
            HideRemoteConsole();

            vm.ShowLabelPrintingLayout = true;

        }

        private void LabelPrintingBTN_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void LabelPrintingBTN_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //test xml deserialize
            var HealthSum = XMLTools.XmlParserGeneric<Gen8test.GET_EMBEDDED_HEALTH_DATA>(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\192.168.101.45ServerHealthTrimmed1.xml");
        }


        private void OkBTN_Click(object sender, RoutedEventArgs e)
        {
            //Reset slot properties LandingPageViewUC
            //ResetServerProperties();
            //Hide the Overlay
            vm.ShowOverlay = Visibility.Collapsed;

            //Is navigation required after selecting OK
            if (vm.MainMenuSelected == true)
            {
                //Load landing page
                vm.LoadContent = new LandingPageView();
                //reset flag to false
                vm.MainMenuSelected = false;
            }


        }

        private void CloseBTN_Click(object sender, RoutedEventArgs e)
        {
            //Hide the Overlay
            vm.ShowOverlay = Visibility.Collapsed;
        }

        private void TestModeCB_Checked(object sender, RoutedEventArgs e)
        {
            //Test Mode Set global value to true 
            StaticFunctions.TestMode = true;
        }

        private void TestModeCB_Unchecked(object sender, RoutedEventArgs e)
        {
            //Test Mode Set global value to true 
            StaticFunctions.TestMode = false;
        }

        private void MainMenuBTN_Click(object sender, RoutedEventArgs e)
        {
            //set property to true, used to identify if it should navigate or not.
            vm.MainMenuSelected = true;
            //if the remote console is visible then hide due to z order issue
            HideRemoteConsole();
        }



        //Hide Remote console function as causes issues with flyouts due to the extrernal z-order 
        private async void HideRemoteConsole()
        {
            try
            {


                //Close RC via PID Need to use static variable
                // vm.CloseAppByPid(StaticFunctions.remoteConsolePID);


                //Drill down and find Remote console host and hide it
                //use helper to get control handle
                WindowsFormsHost RemoteConsoleHost = Helper.FindChild<WindowsFormsHost>(Application.Current.MainWindow, "WFHS1");

                if (RemoteConsoleHost != null)
                    RemoteConsoleHost.Visibility = Visibility.Hidden;

                // RemoteConsoleHost.Child.Controls.Clear();



            }
            catch (Exception ex)
            {

                //**** custom messages pipe through custom dialog instead of message boxes ****
                // Set System Message and show Custom Dialog
                //vm.SystemMessage1 = "Testing internal message dialog";
                //vm.ShowOverlay = Visibility.Visible;

                ////Wait for user to 
                //await vm.OverlayDialog();

                // Set System Message and show Custom Dialog
                vm.SystemMessage1 = ex.Message;
                vm.ShowOverlay = Visibility.Visible;

                //Wait for user to 
                await vm.OverlayDialog();


            }
        }


        private void EnglishFonts()
        {
            //Add George zoom specific fonts
            if (vm.FullUser.Contains("G.Galasso"))
            {
                //Set Fonts
                vm.MenuFont = 22;
                vm.TopMenuFont = 8;
                vm.ListViewHeadFont = 9;
                vm.ServerTitleFont = 9;
                vm.ServerChecklistFont = 11;
                vm.ServerChecklistSmallFont = 10;
                vm.ServerChecklistTinyFont = 9;
                vm.StandardMessageFont = 14;
                vm.StandardLabelFont = 11;
                vm.SerialStandardLabelFont = 12;
                vm.SerialSmallLabelFont = 10;
                vm.SerialTinyLabelFont = 10;
            }
            else
            {

                //Set Fonts
                vm.MenuFont = 26;
                vm.TopMenuFont = 11;
                vm.ListViewHeadFont = 11;
                vm.ServerTitleFont = 11;
                vm.ServerChecklistFont = 14;
                vm.ServerChecklistSmallFont = 13;
                vm.ServerChecklistTinyFont = 11;
                vm.StandardMessageFont = 16;
                vm.StandardLabelFont = 14;
                vm.SerialStandardLabelFont = 15;
                vm.SerialSmallLabelFont = 12;
                vm.SerialTinyLabelFont = 12;
            }


        }

        private void FrenchFonts()
        {
            if (vm.FullUser.Contains("G.Galasso"))
            {

                //Set Fonts
                vm.MenuFont = 17;
                vm.TopMenuFont = 6;
                vm.ListViewHeadFont = 7;
                vm.ServerTitleFont = 7;
                vm.ServerChecklistFont = 9;
                vm.ServerChecklistSmallFont = 8;
                vm.ServerChecklistTinyFont = 7;
                vm.StandardMessageFont = 11;
                vm.StandardLabelFont = 8;
                vm.SerialStandardLabelFont = 9;
                vm.SerialSmallLabelFont = 8;
                vm.SerialTinyLabelFont = 8;
            }
            else
            {
                //Set Fonts
                vm.MenuFont = 22;
                vm.TopMenuFont = 11;
                vm.ListViewHeadFont = 11;
                vm.ServerTitleFont = 11;
                vm.ServerChecklistFont = 10;
                vm.ServerChecklistSmallFont = 8;
                vm.ServerChecklistTinyFont = 7;
                vm.StandardMessageFont = 16;
                vm.StandardLabelFont = 12;
                vm.SerialStandardLabelFont = 14;
                vm.SerialSmallLabelFont = 12;
                vm.SerialTinyLabelFont = 10;
            }
        }

        private void GermanFonts()
        {
            if (vm.FullUser.Contains("G.Galasso"))
            {
                //Set Fonts
                vm.MenuFont = 18;
                vm.TopMenuFont = 7;
                vm.ListViewHeadFont = 8;
                vm.ServerTitleFont = 8;
                vm.ServerChecklistFont = 10;
                vm.ServerChecklistSmallFont = 9;
                vm.ServerChecklistTinyFont = 8;
                vm.StandardMessageFont = 12;
                vm.StandardLabelFont = 9;
                vm.SerialStandardLabelFont = 10;
                vm.SerialSmallLabelFont = 9;
                vm.SerialTinyLabelFont = 9;
            }
            else
            {
                //Set Fonts
                vm.MenuFont = 24;
                vm.TopMenuFont = 11;
                vm.ListViewHeadFont = 11;
                vm.ServerTitleFont = 11;
                vm.ServerChecklistFont = 11;
                vm.ServerChecklistSmallFont = 10;
                vm.ServerChecklistTinyFont = 9;
                vm.StandardMessageFont = 16;
                vm.StandardLabelFont = 12;
                vm.SerialStandardLabelFont = 14;
                vm.SerialSmallLabelFont = 12;
                vm.SerialTinyLabelFont = 12;
            }
        }



        private void SetLanguageDictionary()
        {
            //CultureInfo culture; 

            //Console.WriteLine("\n*******" + Thread.CurrentThread.CurrentCulture.ToString() + "*******");

            ResourceDictionary dict = new ResourceDictionary();
            switch (Thread.CurrentThread.CurrentCulture.ToString())
            {
                case "en-US":
                    dict.Source = new Uri("..\\Resources\\English.xaml", UriKind.Relative);
                    //Set Fonts for Lang
                    EnglishFonts();

                    //culture = new System.Globalization.CultureInfo("en");
                    //Thread.CurrentThread.CurrentCulture = culture;
                    //Thread.CurrentThread.CurrentUICulture = culture;
                    break;
                case "en-GB":
                    dict.Source = new Uri("..\\Resources\\English.xaml", UriKind.Relative);
                    //Set Fonts for Lang
                    EnglishFonts();
                    //culture = new System.Globalization.CultureInfo("en");
                    //Thread.CurrentThread.CurrentCulture = culture;
                    //Thread.CurrentThread.CurrentUICulture = culture;
                    break;
                case "de":
                    dict.Source = new Uri("..\\Resources\\German.xaml", UriKind.Relative);
                    //Set Fonts for Lang
                    GermanFonts();
                    //culture = new System.Globalization.CultureInfo("de");
                    //Thread.CurrentThread.CurrentCulture = culture;
                    //Thread.CurrentThread.CurrentUICulture = culture;
                    break;
                case "fr":
                    dict.Source = new Uri("..\\Resources\\French.xaml", UriKind.Relative);
                    //Set Fonts for Lang
                    FrenchFonts();
                    //culture = new System.Globalization.CultureInfo("fr"); 
                    //Thread.CurrentThread.CurrentCulture = culture;
                    //Thread.CurrentThread.CurrentUICulture = culture;
                    break;
                default:
                    dict.Source = new Uri("..\\Resources\\English.xaml", UriKind.Relative);
                    //Set Fonts for Lang
                    EnglishFonts();
                    //culture = new System.Globalization.CultureInfo("en");
                    //Thread.CurrentThread.CurrentCulture = culture;
                    //Thread.CurrentThread.CurrentUICulture = culture;
                    break;
            }
            //Add Merged Dictionaries Globally
            Application.Current.Resources.MergedDictionaries.Add(dict);

            //Reload Dialog LANG
            GetMetroDialogSettings();

        }




        private void LangCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Call language set
            //CultureInfo culture; //German region

            ResourceDictionary dict = new ResourceDictionary();
            CultureInfo ci;


            switch (LangCB.SelectedIndex)
            {
                case 0:
                    dict.Source = new Uri("..\\Resources\\English.xaml", UriKind.Relative);

                    //Set Fonts for Lang
                    EnglishFonts();

                    //culture = new System.Globalization.CultureInfo("en");
                    //Thread.CurrentThread.CurrentCulture = culture;
                    //Thread.CurrentThread.CurrentUICulture = culture;
                    break;
                case 1:
                    dict.Source = new Uri("..\\Resources\\French.xaml", UriKind.Relative);
                    //Set Fonts for Lang
                    FrenchFonts();

                    //culture = new System.Globalization.CultureInfo("fr");
                    //Thread.CurrentThread.CurrentCulture = culture;
                    //Thread.CurrentThread.CurrentUICulture = culture;
                    break;
                case 2:
                    dict.Source = new Uri("..\\Resources\\German.xaml", UriKind.Relative);
                    //Set Fonts for Lang
                    GermanFonts();

                    //culture = new System.Globalization.CultureInfo("de"); ///German region
                    //Thread.CurrentThread.CurrentCulture = culture;
                    //Thread.CurrentThread.CurrentUICulture = culture;
                    break;
            }
            //Add Merged Dictionaries Globally
            Application.Current.Resources.MergedDictionaries.Add(dict);

            //Reload Dialog LANG
            GetMetroDialogSettings();

        }
    }
}
