﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.DellServers
{
    //class DELLInventory
    //{
    //}


    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Inventory
    {

        private DELLInventoryComponent[] componentField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Component")]
        public DELLInventoryComponent[] Component
        {
            get
            {
                return this.componentField;
            }
            set
            {
                this.componentField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class DELLInventoryComponent
    {

        private object[] itemsField;

        private string classnameField;

        private string keyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PROPERTY", typeof(DELLInventoryComponentPROPERTY))]
        [System.Xml.Serialization.XmlElementAttribute("PROPERTY.ARRAY", typeof(DELLInventoryComponentPROPERTYARRAY))]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Classname
        {
            get
            {
                return this.classnameField;
            }
            set
            {
                this.classnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class DELLInventoryComponentPROPERTY
    {

        private string vALUEField;

        private string displayValueField;

        private string nAMEField;

        private string tYPEField;

        /// <remarks/>
        public string VALUE
        {
            get
            {
                return this.vALUEField;
            }
            set
            {
                this.vALUEField = value;
            }
        }

        /// <remarks/>
        public string DisplayValue
        {
            get
            {
                return this.displayValueField;
            }
            set
            {
                this.displayValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = false)]
    public partial class DELLInventoryComponentPROPERTYARRAY
    {

        private DELLInventoryComponentPROPERTYARRAYVALUEARRAY vALUEARRAYField;

        private string nAMEField;

        private string tYPEField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("VALUE.ARRAY")]
        public DELLInventoryComponentPROPERTYARRAYVALUEARRAY VALUEARRAY
        {
            get
            {
                return this.vALUEARRAYField;
            }
            set
            {
                this.vALUEARRAYField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = false)]
    public partial class DELLInventoryComponentPROPERTYARRAYVALUEARRAY
    {

        private byte[] vALUEField;

        private string[] displayValueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("VALUE")]
        public byte[] VALUE
        {
            get
            {
                return this.vALUEField;
            }
            set
            {
                this.vALUEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DisplayValue")]
        public string[] DisplayValue
        {
            get
            {
                return this.displayValueField;
            }
            set
            {
                this.displayValueField = value;
            }
        }
    }

   
    public class DellProperty
    {
        public string NAME { get; set; }
        public string TYPE { get; set; }
        public string VALUE { get; set; }

        public string DisplayValue { get; set; }

    }


}