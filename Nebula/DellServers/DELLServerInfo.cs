﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nebula.Helpers;

namespace Nebula.DellServers
{
    public class DELLServerInfo : HasPropertyChanged
    {


        public DELLServerInfo()
        {
           
            //single collection properties
            iDracInfo = new DelliDrac();
            SystemInfo = new DellSystem();
            //multi collection properties
            DELLCpuCollection = new ObservableCollection<DellCpu>();
            DELLFanCollection = new ObservableCollection<DellFans>();
            DELLPowerCollection = new ObservableCollection<DellPower>();
            DELLMemoryCollection = new ObservableCollection<DellMemory>();
            DELLNetworkCollection = new ObservableCollection<DellNetwork>();
            DELLStorageCollection = new ObservableCollection<DellStorage>();
            DELLVirtualDiskCollection = new ObservableCollection<DellVirtualDisk>();
            DELLPhysicalDiskCollection = new ObservableCollection<DellPhysicalDisk>();
            DELLSystemCollection = new ObservableCollection<DellSystem>();
            DELLiDracCollection = new ObservableCollection<DelliDrac>();
            DELLThermalCollection = new ObservableCollection<DellThermal>();

        }

        private string _TestedBy;

        public string TestedBy
        {
            get { return _TestedBy; }
            set
            {
                _TestedBy = value;
                OnPropertyChanged("TestedBy");
            }
        }

        private string _SDCardInserted;

        public string SDCardInserted
        {
            get { return _SDCardInserted; }
            set
            {
                _SDCardInserted = value;
                OnPropertyChanged("SDCardInserted");
            }
        }

        private DelliDrac _iDracInfo;

        public DelliDrac iDracInfo
        {
            get { return _iDracInfo; }
            set
            {
                _iDracInfo = value;
                OnPropertyChanged("iDracInfo");
            }
        }

        private DellSystem _SystemInfo;

        public DellSystem SystemInfo
        {
            get { return _SystemInfo; }
            set
            {
                _SystemInfo = value;
                OnPropertyChanged("SystemInfo");
            }
        }
        //OEM Branded

        private string _IsOem;

        public string IsOem
        {
            get { return _IsOem; }
            set
            {
                _IsOem = value;
                OnPropertyChanged("IsOem");
            }
        }

        //SD CARD Presence

        //FIRMWARE

        private string _BiosCurrentVersion;

        public string BiosCurrentVersion
        {
            get { return _BiosCurrentVersion; }
            set
            {
                _BiosCurrentVersion = value;
                OnPropertyChanged("BiosCurrentVersion");
            }
        }

        private string _BiosReleaseDate;

        public string BiosReleaseDate
        {
            get { return _BiosReleaseDate; }
            set
            {
                _BiosReleaseDate = value;
                OnPropertyChanged("BiosReleaseDate");
            }
        }

        private string _iDRACVersion;

        public string iDRACVersion
        {
            get { return _iDRACVersion; }
            set
            {
                _iDRACVersion = value;
                OnPropertyChanged("iDRACVersion");
            }
        }

        private string _ControllerType;

        public string ControllerType
        {
            get { return _ControllerType; }
            set
            {
                _ControllerType = value;
                OnPropertyChanged("ControllerType");
            }
        }

        private string _ControllerVersion;

        public string ControllerVersion
        {
            get { return _ControllerVersion; }
            set
            {
                _ControllerVersion = value;
                OnPropertyChanged("ControllerVersion");
            }
        }

        private string _ControllerType2;

        public string ControllerType2
        {
            get { return _ControllerType2; }
            set
            {
                _ControllerType2 = value;
                OnPropertyChanged("ControllerType2");
            }
        }

        private string _ControllerVersion2;

        public string ControllerVersion2
        {
            get { return _ControllerVersion2; }
            set
            {
                _ControllerVersion2 = value;
                OnPropertyChanged("ControllerVersion2");
            }
        }

        private string _ControllerType3;

        public string ControllerType3
        {
            get { return _ControllerType3; }
            set
            {
                _ControllerType3 = value;
                OnPropertyChanged("ControllerType3");
            }
        }

        private string _ControllerVersion3;

        public string ControllerVersion3
        {
            get { return _ControllerVersion3; }
            set
            {
                _ControllerVersion3 = value;
                OnPropertyChanged("ControllerVersion3");
            }
        }


        //CPLD PCIe version
        private string _CPLDVersion;

        public string CPLDVersion
        {
            get { return _CPLDVersion; }
            set
            {
                _CPLDVersion = value;
                OnPropertyChanged("CPLDVersion");
            }
        }


        //BOSS DeviceCardSlotType version
        private string _DeviceCardSlotType;

        public string DeviceCardSlotType
        {
            get { return _DeviceCardSlotType; }
            set
            {
                _DeviceCardSlotType = value;
                OnPropertyChanged("DeviceCardSlotType");
            }
        }


        //SERVER GENERAL INFO

        private string _Manufacturer;

        public string Manufacturer
        {
            get { return _Manufacturer; }
            set
            {
                _Manufacturer = value;
                OnPropertyChanged("Manufacturer");
            }
        }

        private string _Model;

        public string Model
        {
            get { return _Model; }
            set
            {
                _Model = value;
                OnPropertyChanged("Model");
            }
        }

        private string _SystemGeneration;

        public string SystemGeneration
        {
            get { return _SystemGeneration; }
            set
            {
                _SystemGeneration = value;
                OnPropertyChanged("SystemGeneration");
            }
        }

        private string _BoardSerialNumber;

        public string BoardSerialNumber
        {
            get { return _BoardSerialNumber; }
            set
            {
                _BoardSerialNumber = value;
                OnPropertyChanged("BoardSerialNumber");
            }
        }

        private string _AssetTag;

        public string AssetTag
        {
            get { return _AssetTag; }
            set
            {
                _AssetTag = value;
                OnPropertyChanged("AssetTag");
            }
        }


        private string _HostName;

        public string HostName
        {
            get { return _HostName; }
            set
            {
                _HostName = value;
                OnPropertyChanged("HostName");
            }
        }



        private string _ServiceTag;

        public string ServiceTag
        {
            get { return _ServiceTag; }
            set
            {
                _ServiceTag = value;
                OnPropertyChanged("ServiceTag");
            }
        }

        private string _BoardPartNumber;

        public string BoardPartNumber
        {
            get { return _BoardPartNumber; }
            set
            {
                _BoardPartNumber = value;
                OnPropertyChanged("BoardPartNumber");
            }
        }

        private string _EstimatedExhaustTemperature;

        public string EstimatedExhaustTemperature
        {
            get { return _EstimatedExhaustTemperature; }
            set
            {
                _EstimatedExhaustTemperature = value;
                OnPropertyChanged("EstimatedExhaustTemperature");
            }
        }

        private string _SysMemErrorMethodology;

        public string SysMemErrorMethodology
        {
            get { return _SysMemErrorMethodology; }
            set
            {
                _SysMemErrorMethodology = value;
                OnPropertyChanged("SysMemErrorMethodology");
            }
        }

        private string _ChassisSystemHeight;

        public string ChassisSystemHeight
        {
            get { return _ChassisSystemHeight; }
            set
            {
                _ChassisSystemHeight = value;
                OnPropertyChanged("ChassisSystemHeight");
            }
        }

        private string _ExpressServiceCode;

        public string ExpressServiceCode
        {
            get { return _ExpressServiceCode; }
            set
            {
                _ExpressServiceCode = value;
                OnPropertyChanged("ExpressServiceCode");
            }
        }


        private string _iDRACLicense;

        public string iDRACLicense
        {
            get { return _iDRACLicense; }
            set
            {
                _iDRACLicense = value;
                OnPropertyChanged("iDRACLicense");
            }
        }

        private string _iDRACLicenseEntitlementID;

        public string iDRACLicenseEntitlementID
        {
            get { return _iDRACLicenseEntitlementID; }
            set
            {
                _iDRACLicenseEntitlementID = value;
                OnPropertyChanged("iDRACLicenseEntitlementID");
            }
        }

        //HEALTH AT A GLANCE
        private string _RollupStatus;

        public string RollupStatus
        {
            get { return _RollupStatus; }
            set
            {
                _RollupStatus = value;
                OnPropertyChanged("RollupStatus");
            }
        }

        private string _CurrentRollupStatus;

        public string CurrentRollupStatus
        {
            get { return _CurrentRollupStatus; }
            set
            {
                _CurrentRollupStatus = value;
                OnPropertyChanged("CurrentRollupStatus");
            }
        }


        private string _BatteryRollupStatus;

        public string BatteryRollupStatus
        {
            get { return _BatteryRollupStatus; }
            set
            {
                _BatteryRollupStatus = value;
                OnPropertyChanged("BatteryRollupStatus");
            }
        }


        private string _IntrusionRollupStatus;

        public string IntrusionRollupStatus
        {
            get { return _IntrusionRollupStatus; }
            set
            {
                _IntrusionRollupStatus = value;
                OnPropertyChanged("IntrusionRollupStatus");
            }
        }

        private string _FanRollupStatus;

        public string FanRollupStatus
        {
            get { return _FanRollupStatus; }
            set
            {
                _FanRollupStatus = value;
                OnPropertyChanged("FanRollupStatus");
            }
        }


        private string _PSRollupStatus;

        public string PSRollupStatus
        {
            get { return _PSRollupStatus; }
            set
            {
                _PSRollupStatus = value;
                OnPropertyChanged("PSRollupStatus");
            }
        }



        private string _MemoryRollupStatus;

        public string MemoryRollupStatus
        {
            get { return _MemoryRollupStatus; }
            set
            {
                _MemoryRollupStatus = value;
                OnPropertyChanged("MemoryRollupStatus");
            }
        }

      

        private string _CPURollupStatus;

        public string CPURollupStatus
        {
            get { return _CPURollupStatus; }
            set
            {
                _CPURollupStatus = value;
                OnPropertyChanged("CPURollupStatus");
            }
        }


        private string _TempRollupStatus;

        public string TempRollupStatus
        {
            get { return _TempRollupStatus; }
            set
            {
                _TempRollupStatus = value;
                OnPropertyChanged("TempRollupStatus");
            }
        }



        private string _VoltRollupStatus;

        public string VoltRollupStatus
        {
            get { return _VoltRollupStatus; }
            set
            {
                _VoltRollupStatus = value;
                OnPropertyChanged("VoltRollupStatus");
            }
        }



        private string _StorageRollupStatus;

        public string StorageRollupStatus
        {
            get { return _StorageRollupStatus; }
            set
            {
                _StorageRollupStatus = value;
                OnPropertyChanged("StorageRollupStatus");
            }
        }


        private string _SDCardRollupStatus;

        public string SDCardRollupStatus
        {
            get { return _SDCardRollupStatus; }
            set
            {
                _SDCardRollupStatus = value;
                OnPropertyChanged("SDCardRollupStatus");
            }
        }

        private string _LicensingRollupStatus;

        public string LicensingRollupStatus
        {
            get { return _LicensingRollupStatus; }
            set
            {
                _LicensingRollupStatus = value;
                OnPropertyChanged("LicensingRollupStatus");
            }
        }


        private string _OutOfRegionMessage;

        public string OutOfRegionMessage
        {
            get { return _OutOfRegionMessage; }
            set
            {
                _OutOfRegionMessage = value;
                OnPropertyChanged("OutOfRegionMessage");
            }
        }

        //MEMORY

        private string _TotalMemory;

        public string TotalMemory
        {
            get { return _TotalMemory; }
            set
            {
                _TotalMemory = value;
                OnPropertyChanged("TotalMemory");
            }
        }

        private string _MaxDIMMSlots;

        public string MaxDIMMSlots
        {
            get { return _MaxDIMMSlots; }
            set
            {
                _MaxDIMMSlots = value;
                OnPropertyChanged("MaxDIMMSlots");
            }
        }

        private string _PopulatedDIMMSlots;

        public string PopulatedDIMMSlots
        {
            get { return _PopulatedDIMMSlots; }
            set
            {
                _PopulatedDIMMSlots = value;
                OnPropertyChanged("PopulatedDIMMSlots");
            }
        }

        private string _MemoryOperationMode;

        public string MemoryOperationMode
        {
            get { return _MemoryOperationMode; }
            set
            {
                _MemoryOperationMode = value;
                OnPropertyChanged("MemorySlotsUsed");
            }
        }

        private string _SysMemMaxCapacitySize;

        public string SysMemMaxCapacitySize
        {
            get { return _SysMemMaxCapacitySize; }
            set
            {
                _SysMemMaxCapacitySize = value;
                OnPropertyChanged("SysMemMaxCapacitySize");
            }
        }


        //CPU

        private string _CPUFamily;

        public string CPUFamily
        {
            get { return _CPUFamily; }
            set
            {
                _CPUFamily = value;
                OnPropertyChanged("CPUFamily");
            }
        }



        private string _CPUCount;

        public string CPUCount
        {
            get { return _CPUCount; }
            set
            {
                _CPUCount = value;
                OnPropertyChanged("CPUCount");
            }
        }


        private string _CurrentClockSpeed;

        public string CurrentClockSpeed
        {
            get { return _CurrentClockSpeed; }
            set
            {
                _CurrentClockSpeed = value;
                OnPropertyChanged("CurrentClockSpeed");
            }
        }

        private string _MaxClockSpeed;

        public string MaxClockSpeed
        {
            get { return _MaxClockSpeed; }
            set
            {
                _MaxClockSpeed = value;
                OnPropertyChanged("MaxClockSpeed");
            }
        }

        private string _Voltage;

        public string Voltage
        {
            get { return _Voltage; }
            set
            {
                _Voltage = value;
                OnPropertyChanged("Voltage");
            }
        }


        //POWER

        private string _PowerState;

        public string PowerState
        {
            get { return _PowerState; }
            set
            {
                _PowerState = value;
                OnPropertyChanged("PowerState");
            }
        }





        //TO Hold data as string Values
        //IDRAC
        private string _iDracInventory;

        public string iDracInventory
        {
            get { return _iDracInventory; }
            set
            {
                _iDracInventory = value;
                OnPropertyChanged("iDracInventory");
            }
        }

        //SYSTEM
        private string _SystemInventory;

        public string SystemInventory
        {
            get { return _SystemInventory; }
            set
            {
                _SystemInventory = value;
                OnPropertyChanged("SystemInventory");
            }
        }

        //FANS
        private string _FansInventory;

        public string FansInventory
        {
            get { return _FansInventory; }
            set
            {
                _FansInventory = value;
                OnPropertyChanged("FansInventory");
            }
        }
        //CPU
        private string _CPUInventory;

        public string CPUInventory
        {
            get { return _CPUInventory; }
            set
            {
                _CPUInventory = value;
                OnPropertyChanged("CPUInventory");
            }
        }
        //MEMORY
        private string _MemoryInventory;

        public string MemoryInventory
        {
            get { return _MemoryInventory; }
            set
            {
                _MemoryInventory = value;
                OnPropertyChanged("MemoryInventory");
            }
        }

        //NETWORK
        private string _NetworkInventory;

        public string NetworkInventory
        {
            get { return _NetworkInventory; }
            set
            {
                _NetworkInventory = value;
                OnPropertyChanged("NetworkInventory");
            }
        }

        //STORAGE
        private string _StorageInventory;

        public string StorageInventory
        {
            get { return _StorageInventory; }
            set
            {
                _StorageInventory = value;
                OnPropertyChanged("StorageInventory");
            }
        }

        //VIRTUAL DISK
        private string _VirtualDiskInventory;

        public string VirtualDiskInventory
        {
            get { return _VirtualDiskInventory; }
            set
            {
                _VirtualDiskInventory = value;
                OnPropertyChanged("VirtualDiskInventory");
            }
        }

        //PHYSICAL DISK
        private string _PhysicalDiskInventory;

        public string PhysicalDiskInventory
        {
            get { return _PhysicalDiskInventory; }
            set
            {
                _PhysicalDiskInventory = value;
                OnPropertyChanged("PhysicalDiskInventory");
            }
        }

        //POWER
        private string _PowerInventory;

        public string PowerInventory
        {
            get { return _PowerInventory; }
            set
            {
                _PowerInventory = value;
                OnPropertyChanged("PowerInventory");
            }
        }






        //public ObservableCollection<DellSystem> DELLSystemCollection { get; set; }
        //public ObservableCollection<DelliDrac> DELLiDracCollection { get; set; }
        public ObservableCollection<DellCpu> DELLCpuCollection { get; set; }
        public ObservableCollection<DellFans> DELLFanCollection { get; set; }
        public ObservableCollection<DellPower> DELLPowerCollection { get; set; }
        public ObservableCollection<DellMemory> DELLMemoryCollection { get; set; }
        public ObservableCollection<DellNetwork> DELLNetworkCollection { get; set; }
        public ObservableCollection<DellStorage> DELLStorageCollection { get; set; }
        public ObservableCollection<DellVirtualDisk> DELLVirtualDiskCollection { get; set; }
        public ObservableCollection<DellPhysicalDisk> DELLPhysicalDiskCollection { get; set; }
        public ObservableCollection<DelliDrac> DELLiDracCollection { get; set; }
        public ObservableCollection<DellSystem> DELLSystemCollection { get; set; }
        public ObservableCollection<DellThermal> DELLThermalCollection { get; set; }




    }






    public class DellCpu : HasPropertyChanged
    {

        public DellCpu()
        {

        }

        public DellCpu(string model, string manufacturer, string cpuFamily, string deviceDescription, string instanceID, string cache1Location, string cache2Location, string cache3Location, string cache3Associativity, string cache2Associativity, string cache1Associativity,
            string cache3Type, string cache2Type, string cache1Type, string cache3ErrorMethodology, string cache2ErrorMethodology, string cache1ErrorMethodology, string cache3SRAMType, string cache2SRAMType, string cache1SRAMType, string cache3WritePolicy, string cache2WritePolicy, string cache1WritePolicy,
            string cache3Level, string cache2Level, string cache1Level, string cache3PrimaryStatus, string cache2PrimaryStatus, string cache1PrimaryStatus, string cache3InstalledSize, string cache2InstalledSize, string cache1InstalledSize, string cache3Size, string cache2Size, string cache1Size, 
            string turboModeCapable, string turboModeEnabled, string executeDisabledCapable, string executeDisabledEnabled, string virtualizationTechnologyCapable, string virtualizationTechnologyEnabled, string hyperThreadingCapable, string hyperThreadingEnabled, string characteristics, string cpuStatus,
            string voltage, string numberOfProcessorCores, string numberOfEnabledThreads, string numberOfEnabledCores, string primaryStatus, string externalBusClockSpeed, string maxClockSpeed, string currentClockSpeed)
        {


            Model = model;
            Manufacturer = manufacturer;
            CPUFamily = cpuFamily;
            DeviceDescription = deviceDescription;
            InstanceID = instanceID;
            Cache3Location = cache3Location;
            Cache2Location = cache2Location;
            Cache1Location = cache1Location;
            Cache3Associativity = cache3Associativity;
            Cache2Associativity = cache2Associativity;
            Cache1Associativity = cache1Associativity;
            Cache3Type = cache3Type;
            Cache2Type = cache2Type;
            Cache1Type = cache1Type;
            Cache3ErrorMethodology = cache3ErrorMethodology;
            Cache2ErrorMethodology = cache2ErrorMethodology;
            Cache1ErrorMethodology = cache1ErrorMethodology;


            Cache3SRAMType = cache3SRAMType;
            Cache2SRAMType = cache2SRAMType;
            Cache1SRAMType = cache1SRAMType;
            Cache3WritePolicy = cache3WritePolicy;
            Cache2WritePolicy = cache2WritePolicy;
            Cache1WritePolicy = cache1WritePolicy;
            Cache3Level = cache3Level;
            Cache2Level = cache2Level;
            Cache1Level = cache1Level;
            Cache3PrimaryStatus = cache3PrimaryStatus;
            Cache2PrimaryStatus = cache2PrimaryStatus;
            Cache1PrimaryStatus = cache1PrimaryStatus;
            Cache3InstalledSize = cache3InstalledSize;
            Cache2InstalledSize = cache2InstalledSize;
            Cache1InstalledSize = cache1InstalledSize;
            Cache3Size = cache3Size;
            Cache2Size = cache2Size;
            Cache1Size = cache1Size;

            TurboModeCapable = turboModeCapable;
            TurboModeEnabled = turboModeEnabled;
            ExecuteDisabledCapable = executeDisabledCapable;
            ExecuteDisabledEnabled = executeDisabledEnabled;
            VirtualizationTechnologyCapable = virtualizationTechnologyCapable;
            VirtualizationTechnologyEnabled = virtualizationTechnologyEnabled;
            HyperThreadingCapable = hyperThreadingCapable;
            HyperThreadingEnabled = hyperThreadingEnabled;
            Characteristics = characteristics;
            CPUStatus = cpuStatus;
            Voltage = voltage;
            NumberOfProcessorCores = numberOfProcessorCores;
            NumberOfEnabledThreads = numberOfEnabledThreads;
            NumberOfEnabledCores = numberOfEnabledCores;
            PrimaryStatus = primaryStatus;
            ExternalBusClockSpeed = externalBusClockSpeed;
            MaxClockSpeed = maxClockSpeed;
            CurrentClockSpeed = currentClockSpeed;

        }




        public string DeviceDescription { get; set; }
        public string CPUFamily { get; set; }
        public string Model { get; set; }
        public string Manufacturer { get; set; }
        public string InstanceID { get; set; }
        public string Cache3Location { get; set; }
        public string Cache2Location { get; set; }
        public string Cache1Location { get; set; }
        public string Cache3Associativity { get; set; }
        public string Cache2Associativity { get; set; }
        public string Cache1Associativity { get; set; }
        public string Cache3Type { get; set; }
        public string Cache2Type { get; set; }
        public string Cache1Type { get; set; }
        public string Cache3ErrorMethodology { get; set; }
        public string Cache2ErrorMethodology { get; set; }
        public string Cache1ErrorMethodology { get; set; }
        public string Cache3SRAMType { get; set; }
        public string Cache2SRAMType { get; set; }
        public string Cache1SRAMType { get; set; }
        public string Cache3WritePolicy { get; set; }
        public string Cache2WritePolicy { get; set; }
        public string Cache1WritePolicy { get; set; }
        public string Cache3Level { get; set; }
        public string Cache2Level { get; set; }
        public string Cache1Level { get; set; }
        public string Cache3PrimaryStatus { get; set; }
        public string Cache2PrimaryStatus { get; set; }
        public string Cache1PrimaryStatus { get; set; }
        public string Cache3InstalledSize { get; set; }
        public string Cache2InstalledSize { get; set; }
        public string Cache1InstalledSize { get; set; }
        public string Cache3Size { get; set; }
        public string Cache2Size { get; set; }
        public string Cache1Size { get; set; }

        public string TurboModeCapable { get; set; }
        public string TurboModeEnabled { get; set; }
        public string ExecuteDisabledCapable { get; set; }
        public string ExecuteDisabledEnabled { get; set; }
        public string VirtualizationTechnologyCapable { get; set; }
        public string VirtualizationTechnologyEnabled { get; set; }
        public string HyperThreadingCapable { get; set; }
        public string HyperThreadingEnabled { get; set; }
        public string Characteristics { get; set; }
        public string CPUStatus { get; set; }
        public string Voltage { get; set; }
        public string NumberOfProcessorCores{ get; set; }
        public string NumberOfEnabledThreads { get; set; }
        public string NumberOfEnabledCores { get; set; }
        public string PrimaryStatus { get; set; }

        public string ExternalBusClockSpeed { get; set; }
        public string MaxClockSpeed { get; set; }
        public string CurrentClockSpeed { get; set; }

       // public string CPUStatus { get; set; }


    }

    public class DellMemory : HasPropertyChanged
    {

        public DellMemory()
        {

        }
        public DellMemory(string rank, string primaryStatus, string manufactureDate, string model, string partNumber, string serialNumber, string manufacturer,
            string bankLabel, string size, string currentOperatingSpeed, string speed, string memoryType, string deviceDescription, string instanceID)
        {
            Rank = rank;
            PrimaryStatus = primaryStatus;
            ManufactureDate = manufactureDate;
            Model = model;
            PartNumber = partNumber;
            SerialNumber = serialNumber;
            Manufacturer = manufacturer;
            BankLabel = bankLabel;
            Size = size;
            CurrentOperatingSpeed = currentOperatingSpeed;
            Speed = speed;
            MemoryType = memoryType;
            DeviceDescription = deviceDescription;
            InstanceID = instanceID;


        }


        //InstanceID
        private string _InstanceID;

        public string InstanceID
        {
            get { return _InstanceID; }
            set
            {
                _InstanceID = value;
                OnPropertyChanged("InstanceID");
            }
        }
        //DeviceDescription
        private string _DeviceDescription;

        public string DeviceDescription
        {
            get { return _DeviceDescription; }
            set
            {
                _DeviceDescription = value;
                OnPropertyChanged("DeviceDescription");
            }
        }

        //MemoryType
        private string _MemoryType;

        public string MemoryType
        {
            get { return _MemoryType; }
            set
            {
                _MemoryType = value;
                OnPropertyChanged("MemoryType");
            }
        }
        //Speed
        private string _Speed;

        public string Speed
        {
            get { return _Speed; }
            set
            {
                _Speed = value;
                OnPropertyChanged("Speed");
            }
        }
        //CurrentOperatingSpeed
        private string _CurrentOperatingSpeed;

        public string CurrentOperatingSpeed
        {
            get { return _CurrentOperatingSpeed; }
            set
            {
                _CurrentOperatingSpeed = value;
                OnPropertyChanged("CurrentOperatingSpeed");
            }
        }
        //Size
        private string _Size;

        public string Size
        {
            get { return _Size; }
            set
            {
                _Size = value;
                OnPropertyChanged("Size");
            }
        }

        //BankLabel
        private string _BankLabel;

        public string BankLabel
        {
            get { return _BankLabel; }
            set
            {
                _BankLabel = value;
                OnPropertyChanged("BankLabel");
            }
        }

        //Manufacturer
        private string _Manufacturer;

        public string Manufacturer
        {
            get { return _Manufacturer; }
            set
            {
                _Manufacturer = value;
                OnPropertyChanged("Manufacturer");
            }
        }

        //Manufacturer
        private string _SerialNumber;

        public string SerialNumber
        {
            get { return _SerialNumber; }
            set
            {
                _SerialNumber = value;
                OnPropertyChanged("SerialNumber");
            }
        }

        //PartNumber
        private string _PartNumber;

        public string PartNumber
        {
            get { return _PartNumber; }
            set
            {
                _PartNumber = value;
                OnPropertyChanged("PartNumber");
            }
        }

        //Model
        private string _Model;

        public string Model
        {
            get { return _Model; }
            set
            {
                _Model = value;
                OnPropertyChanged("Model");
            }
        }

        //ManufactureDate
        private string _ManufactureDate;

        public string ManufactureDate
        {
            get { return _ManufactureDate; }
            set
            {
                _ManufactureDate = value;
                OnPropertyChanged("ManufactureDate");
            }
        }

        //PrimaryStatus
        private string _PrimaryStatus;

        public string PrimaryStatus
        {
            get { return _PrimaryStatus; }
            set
            {
                _PrimaryStatus = value;
                OnPropertyChanged("PrimaryStatus");
            }
        }

        //PrimaryStatus
        private string _Rank;

        public string Rank
        {
            get { return _Rank; }
            set
            {
                _Rank = value;
                OnPropertyChanged("Rank");
            }
        }

       
      
       
      
       
       
       
      
       


    }


    public class DellControllerBattery : HasPropertyChanged
    {
        public DellControllerBattery(string raidState, string primaryStatus, string deviceDescription, string instanceID)
        {
            RAIDState = raidState;
            PrimaryStatus = primaryStatus;
            DeviceDescription = deviceDescription;
            InstanceID = instanceID;


        }



        public string RAIDState { get; set; }
        public string PrimaryStatus { get; set; }
        public string DeviceDescription { get; set; }
        public string InstanceID { get; set; }


    }



    public class DellSystem : HasPropertyChanged
    {
    public DellSystem()
    {

    }

        public DellSystem(string estimatedExhaustTemperature, string estimatedSystemAirflow, string licensingRollupStatus, string storageRollupStatus, string rollupStatus, string memoryRollupStatus, string tempStatisticsRollupStatus,
             string sdCardRollupStatus, string currentRollupStatus, string batteryRollupStatus, string fanRollupStatus, string intrusionRollupStatus, string voltRollupStatus, string tempRollupStatus, string psRollupStatus, string cpuRollupStatus,
              string populatedPCIeSlots, string maxPCIeSlots, string populatedCPUSockets, string maxCPUSockets, string primaryStatus, string bladeGeometry, string cpldVersion, string boardPartNumber, string boardSerialNumber, string chassisName, string powerCap,
               string powerCapEnabledState, string powerState, string baseBoardChassisSlot, string populatedDIMMSlots, string memoryOperationMode, string sysMemFailOverState, string sysMemPrimaryStatus, string uuid, string smbiosGUID, string platformGUID,
                string systemID, string biosReleaseDate, string biosVersionString, string maxDIMMSlots, string sysMemErrorMethodology, string sysMemLocation, string sysMemMaxCapacitySize, string sysMemTotalSize, string chassisSystemHeight, string nodeID, 
                string chassisServiceTag, string expressServiceCode, string serviceTag, string manufacturer, string model, string lifecycleControllerVersion, string systemGeneration, string deviceDescription, string instanceID)
        {
            EstimatedExhaustTemperature = estimatedExhaustTemperature;
            EstimatedSystemAirflow = estimatedSystemAirflow;
            LicensingRollupStatus = licensingRollupStatus;
            StorageRollupStatus = storageRollupStatus;
            RollupStatus = rollupStatus;
            MemoryRollupStatus = memoryRollupStatus;
            TempStatisticsRollupStatus = tempStatisticsRollupStatus;
            SDCardRollupStatus = sdCardRollupStatus;
            CurrentRollupStatus = currentRollupStatus;
            BatteryRollupStatus = batteryRollupStatus;
            FanRollupStatus = fanRollupStatus;
            IntrusionRollupStatus = intrusionRollupStatus;
            VoltRollupStatus = voltRollupStatus;
            TempRollupStatus = tempRollupStatus;
            PSRollupStatus = psRollupStatus;
            CPURollupStatus = cpuRollupStatus;
            PopulatedPCIeSlots = populatedPCIeSlots;
            MaxPCIeSlots = maxPCIeSlots;
            PopulatedCPUSockets = populatedCPUSockets;
            MaxCPUSockets = maxCPUSockets;
            PrimaryStatus = primaryStatus;
            BladeGeometry = bladeGeometry;
            CPLDVersion = cpldVersion;
            BoardPartNumber = boardPartNumber;
            BoardSerialNumber = boardSerialNumber;
            ChassisName = chassisName;
            PowerCap = powerCap;
            PowerCapEnabledState = powerCapEnabledState;
            PowerState = powerState;
            BaseBoardChassisSlot = baseBoardChassisSlot;
            PopulatedDIMMSlots = populatedDIMMSlots;
            MemoryOperationMode = memoryOperationMode;
            SysMemFailOverState = sysMemFailOverState;
            SysMemPrimaryStatus = sysMemPrimaryStatus;
            UUID = uuid;
            SMbiosGUID = smbiosGUID;
            PlatformGUID = platformGUID;
            SystemID = systemID;
            BIOSReleaseDate = biosReleaseDate;
            BIOSVersionString = biosVersionString;
            MaxDIMMSlots = maxDIMMSlots;
            SysMemErrorMethodology = sysMemErrorMethodology;
            SysMemLocation = sysMemLocation;
            SysMemMaxCapacitySize = sysMemMaxCapacitySize;
            SysMemTotalSize = sysMemTotalSize;
            ChassisSystemHeight = chassisSystemHeight;
            NodeID = nodeID;
            ChassisServiceTag = chassisServiceTag;
            ExpressServiceCode = expressServiceCode;
            ServiceTag = serviceTag;
            Manufacturer = manufacturer;
            Model = model;
            LifecycleControllerVersion = lifecycleControllerVersion;
            SystemGeneration = systemGeneration;
            DeviceDescription = deviceDescription;
            InstanceID = instanceID;


        }



        public string EstimatedExhaustTemperature { get; set; }
        public string EstimatedSystemAirflow { get; set; }
        public string LicensingRollupStatus { get; set; }
        public string StorageRollupStatus { get; set; }
        public string RollupStatus { get; set; }
        public string MemoryRollupStatus { get; set; }
        public string TempStatisticsRollupStatus { get; set; }
        public string SDCardRollupStatus { get; set; }
        public string CurrentRollupStatus { get; set; }
        public string BatteryRollupStatus { get; set; }
        public string FanRollupStatus { get; set; }

        public string IntrusionRollupStatus { get; set; }
        public string VoltRollupStatus { get; set; }
        public string TempRollupStatus { get; set; }
        public string PSRollupStatus { get; set; }
        public string CPURollupStatus { get; set; }
        public string PopulatedPCIeSlots { get; set; }
        public string MaxPCIeSlots { get; set; }
        public string PopulatedCPUSockets { get; set; }
        public string MaxCPUSockets { get; set; }
        public string PrimaryStatus { get; set; }
        public string BladeGeometry { get; set; }
        public string CPLDVersion { get; set; }

        public string BoardPartNumber { get; set; }
        public string BoardSerialNumber { get; set; }
        public string ChassisName { get; set; }
        public string PowerCap { get; set; }
        public string PowerCapEnabledState { get; set; }
        public string PowerState { get; set; }
        public string BaseBoardChassisSlot { get; set; }
        public string PopulatedDIMMSlots { get; set; }
        public string MemoryOperationMode { get; set; }
        public string SysMemFailOverState { get; set; }
        public string SysMemPrimaryStatus { get; set; }
        public string UUID { get; set; }
        public string SMbiosGUID { get; set; }
        public string PlatformGUID { get; set; }

        public string SystemID { get; set; }
        public string BIOSReleaseDate { get; set; }
        public string BIOSVersionString { get; set; }
        public string MaxDIMMSlots { get; set; }
        public string SysMemErrorMethodology { get; set; }
        public string SysMemLocation { get; set; }
        public string SysMemMaxCapacitySize { get; set; }
        public string SysMemTotalSize { get; set; }
        public string ChassisSystemHeight { get; set; }
        public string NodeID { get; set; }
        public string ChassisServiceTag { get; set; }
        public string ExpressServiceCode { get; set; }
        public string ServiceTag { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string LifecycleControllerVersion { get; set; }
        public string SystemGeneration { get; set; }
        public string DeviceDescription { get; set; }
        public string InstanceID { get; set; }
       

    }





    public class DelliDrac : HasPropertyChanged
    {
        public DelliDrac()
        {

        }
        public DelliDrac(string dnsRacName, string solEnabledState, string lanEnabledState, string urlString, string guid, string permanentMACAddress, string model,
            string firmwareVersion, string deviceDescription, string instanceID)
        {
            DNSRacName = dnsRacName;
            SOLEnabledState = solEnabledState;
            LANEnabledState = lanEnabledState;
            URLString = urlString;
            GUID = guid;
            PermanentMACAddress = permanentMACAddress;
            Model = model;
            FirmwareVersion = firmwareVersion;
            IPMIVersion = IPMIVersion;
            DeviceDescription = deviceDescription;
            InstanceID = instanceID;


        }

        //DNSRacName
        private string _DNSRacName;

        public string DNSRacName
        {
            get { return _DNSRacName; }
            set
            {
                _DNSRacName = value;
                OnPropertyChanged("DNSRacName");
            }
        }

        //SOLEnabledState
        private string _SOLEnabledState;

        public string SOLEnabledState
        {
            get { return _SOLEnabledState; }
            set
            {
                _SOLEnabledState = value;
                OnPropertyChanged("SOLEnabledState");
            }
        }

        //LANEnabledState
        private string _LANEnabledState;

        public string LANEnabledState
        {
            get { return _LANEnabledState; }
            set
            {
                _LANEnabledState = value;
                OnPropertyChanged("LANEnabledState");
            }
        }

        //URLString
        private string _URLString;

        public string URLString
        {
            get { return _URLString; }
            set
            {
                _URLString = value;
                OnPropertyChanged("URLString");
            }
        }

        //GUID
        private string _GUID;

        public string GUID
        {
            get { return _GUID; }
            set
            {
                _GUID = value;
                OnPropertyChanged("GUID");
            }
        }

        //PermanentMACAddress
        private string _PermanentMACAddress;

        public string PermanentMACAddress
        {
            get { return _PermanentMACAddress; }
            set
            {
                _PermanentMACAddress = value;
                OnPropertyChanged("PermanentMACAddress");
            }
        }

        //Model
        private string _Model;

        public string Model
        {
            get { return _Model; }
            set
            {
                _Model = value;
                OnPropertyChanged("Model");
            }
        }

        //FirmwareVersion
        private string _FirmwareVersion;

        public string FirmwareVersion
        {
            get { return _FirmwareVersion; }
            set
            {
                _FirmwareVersion = value;
                OnPropertyChanged("FirmwareVersion");
            }
        }

        //DeviceDescription
        private string _DeviceDescription;

        public string DeviceDescription
        {
            get { return _DeviceDescription; }
            set
            {
                _DeviceDescription = value;
                OnPropertyChanged("DeviceDescription");
            }
        }

        //IPMIVersion
        private string _IPMIVersion;

        public string IPMIVersion
        {
            get { return _IPMIVersion; }
            set
            {
                _IPMIVersion = value;
                OnPropertyChanged("IPMIVersion");
            }
        }

        //InstanceID
        private string _InstanceID;

        public string InstanceID
        {
            get { return _InstanceID; }
            set
            {
                _InstanceID = value;
                OnPropertyChanged("InstanceID");
            }
        }

        


    }



    public class DellNetwork : HasPropertyChanged
    {
        public DellNetwork()
        {

        }
        public DellNetwork(string slotType, string slotLength, string dataBusWidth, string description, string manufacturer, string pciSubDeviceID, string pciSubVendorID, string pciDeviceID, string pciVendorID, string functionNumber,
            string deviceNumber, string busNumber, string deviceDescription, string instanceID)
        {
            //SlotType = slotType;
            //SlotLength = slotLength;
            //DataBusWidth = dataBusWidth;
            //Description = description;
            //Manufacturer = manufacturer;
            //PCISubDeviceID = pciSubDeviceID;
            //PCISubVendorID = pciSubVendorID;
            //PCIDeviceID = pciDeviceID;
            //PCIVendorID = pciVendorID;
            //FunctionNumber = functionNumber;
            //DeviceNumber = deviceNumber;
            //BusNumber = busNumber;
            //DeviceDescription = deviceDescription;
            //InstanceID = instanceID;


        }


        //public string SlotType { get; set; }
        //public string SlotLength { get; set; }
        //public string DataBusWidth { get; set; }
        //public string Description { get; set; }
        //public string Manufacturer { get; set; }
        //public string PCISubDeviceID { get; set; }
        //public string PCISubVendorID { get; set; }
        //public string PCIDeviceID { get; set; }
        //public string PCIVendorID { get; set; }
        //public string FunctionNumber { get; set; }
        //public string DeviceNumber { get; set; }
        //public string BusNumber { get; set; }
        //public string DeviceDescription { get; set; }
        //public string InstanceID { get; set; }

        public string Protocol { get; set; }
        public string MediaType { get; set; }
        public string ReceiveFlowControl { get; set; }
        public string TransmitFlowControl { get; set; }
        public string AutoNegotiation { get; set; }
        public string LinkDuplex { get; set; }
        public string SlotType { get; set; }
        public string SlotLength { get; set; }
        public string DataBusWidth { get; set; }
        public string PCISubDeviceID { get; set; }
        public string PCISubVendorID { get; set; }
        public string PCIDeviceID { get; set; }
        public string PCIVendorID { get; set; }
        public string FunctionNumber { get; set; }
        public string DeviceNumber { get; set; }
        public string BusNumber { get; set; }
        public string PermanentMACAddress { get; set; }
        public string CurrentMACAddress { get; set; }
        public string ProductName { get; set; }
        public string EFIVersion { get; set; }
        public string FamilyVersion { get; set; }
        public string MinBandwidth { get; set; }
        public string MaxBandwidth { get; set; }
        public string iScsiOffloadMode { get; set; }
        public string FCoEOffloadMode { get; set; }
        public string NicMode { get; set; }
        public string Manufacturer { get; set; }
        public string DeviceDescription { get; set; }
        public string InstanceID { get; set; }
        public string Health { get; set; }
        public string State { get; set; }



    }


    public class DellStorage : HasPropertyChanged
    {

        public DellStorage()
        {

        }

        public DellStorage(string realtimeCapability, string supportControllerBootMode, string supportEnhancedAutoForeignImport, string maxAvailablePCILinkSpeed, string maxPossiblePCILinkSpeed, string patrolReadState, string driverVersion,
            string cacheSizeInMB, string supportRAID10UnevenSpans, string t10PICapability, string slicedVDCapability, string cachecadeCapability, string encryptionCapability, string encryptionMode, string securityStatus, string sasAddress,
            string productName, string deviceCardSlotType, string deviceCardSlotLength, string deviceCardDataBusWidth, string deviceCardManufacturer, string pciSubDeviceID, string pciDeviceID, string pciSubVendorID, string pciVendorID,
            string function, string device, string bus, string controllerFirmwareVersion, string persistentHotspare, string pciSlot, string rollupStatus, string primaryStatus, string deviceDescription, string instanceID)
        {
            RealtimeCapability = realtimeCapability;
            SupportControllerBootMode = supportControllerBootMode;
            SupportEnhancedAutoForeignImport = supportEnhancedAutoForeignImport;
            MaxAvailablePCILinkSpeed = maxAvailablePCILinkSpeed;
            MaxPossiblePCILinkSpeed = maxPossiblePCILinkSpeed;
            PatrolReadState = patrolReadState;
            DriverVersion = driverVersion;
            CacheSizeInMB = cacheSizeInMB;
            SupportRAID10UnevenSpans = supportRAID10UnevenSpans;
            T10PICapability = t10PICapability;
            SlicedVDCapability = slicedVDCapability;
            CachecadeCapability = cachecadeCapability;
            EncryptionCapability = encryptionCapability;
            EncryptionMode = encryptionMode;
            SecurityStatus = securityStatus;
            SASAddress = sasAddress;
            ProductName = productName;
            DeviceCardSlotType = deviceCardSlotType;
            DeviceCardSlotLength = deviceCardSlotLength;
            DeviceCardDataBusWidth = deviceCardDataBusWidth;
            DeviceCardManufacturer = deviceCardManufacturer;
            PCISubDeviceID = pciSubDeviceID;
            PCIDeviceID = pciDeviceID;
            PCISubVendorID = pciSubVendorID;
            PCIVendorID = pciVendorID;
            Function = function;
            Device = device;
            Bus = bus;
            ControllerFirmwareVersion = controllerFirmwareVersion;
            PersistentHotspare = persistentHotspare;
            PCISlot = pciSlot;
            RollupStatus = rollupStatus;
            PrimaryStatus = primaryStatus;
            DeviceDescription = deviceDescription;
            InstanceID = instanceID;


        }



        public string RealtimeCapability { get; set; }
        public string SupportControllerBootMode { get; set; }
        public string SupportEnhancedAutoForeignImport { get; set; }
        public string MaxAvailablePCILinkSpeed { get; set; }
        public string MaxPossiblePCILinkSpeed { get; set; }
        public string PatrolReadState { get; set; }
        public string DriverVersion { get; set; }
        public string CacheSizeInMB { get; set; }
        public string SupportRAID10UnevenSpans { get; set; }
        public string T10PICapability { get; set; }
        public string SlicedVDCapability { get; set; }
        public string CachecadeCapability { get; set; }
        public string EncryptionCapability { get; set; }
        public string EncryptionMode { get; set; }
        public string SecurityStatus { get; set; }
        public string SASAddress { get; set; }
        public string ProductName { get; set; }
        public string DeviceCardSlotType { get; set; }
        public string DeviceCardSlotLength { get; set; }
        public string DeviceCardDataBusWidth { get; set; }
        public string DeviceCardManufacturer { get; set; }
        public string PCISubDeviceID { get; set; }
        public string PCIDeviceID { get; set; }
        public string PCISubVendorID { get; set; }
        public string PCIVendorID { get; set; }
        public string Function { get; set; }
        public string Device { get; set; }
        public string Bus { get; set; }
        public string ControllerFirmwareVersion { get; set; }
        public string PersistentHotspare { get; set; }
        public string PCISlot { get; set; }
        public string RollupStatus { get; set; }
        public string PrimaryStatus { get; set; }
        public string DeviceDescription { get; set; }
        public string InstanceID { get; set; }
        public string State { get; set; }
        public string Manufacturer { get; set; }



    }

    public class DellVirtualDisk : HasPropertyChanged
    {

        public DellVirtualDisk()
        {

        }

        public DellVirtualDisk(string deviceType, string blockSizeInBytes, string busProtocol, string cachecade, string diskCachePolicy, string deviceDescription, string fqdd, string instanceID,
            string lastSystemInventoryTime, string lastUpdateTime, string lockStatus, string mediaType, string name,  string objectStatus, string operationName, string operationPercentComplete,
            string primaryStatus, string raidStatus, string raidTypes, string readCachePolicy, string remainingRedundancy, string rollupStatus, string sizeInBytes, string spanDepth,
            string spanLength, string startingLBAinBlocks, string stripeSize, string t10PIStatus, string virtualDiskTargetID, string writeCachePolicy)
        {
            DeviceType = deviceType;
            BlockSizeInBytes = blockSizeInBytes;
            BusProtocol = busProtocol;
            Cachecade = cachecade;
            DeviceDescription = deviceDescription;
            DiskCachePolicy = diskCachePolicy;
            FQDD = fqdd;
            InstanceID = instanceID;
            LastSystemInventoryTime = lastSystemInventoryTime;
            LastUpdateTime = lastUpdateTime;
            LockStatus = lockStatus;
            MediaType = mediaType;
            Name = name;
            ObjectStatus = objectStatus;
            OperationName = operationName;
            OperationPercentComplete = operationPercentComplete;
            PrimaryStatus = primaryStatus;
            RAIDStatus = raidStatus;
            RAIDTypes = raidTypes;
            ReadCachePolicy = readCachePolicy;
            RemainingRedundancy = remainingRedundancy;
            RollupStatus = rollupStatus;
            SizeInBytes = sizeInBytes;
            SpanDepth = spanDepth;
            SpanLength = spanLength;
            StartingLBAinBlocks = startingLBAinBlocks;
            StripeSize = stripeSize;
            T10PIStatus = t10PIStatus;
            VirtualDiskTargetID = virtualDiskTargetID;
            WriteCachePolicy = writeCachePolicy;

        }



        public string DeviceType { get; set; }
        public string BlockSizeInBytes { get; set; }
        public string BusProtocol { get; set; }
        public string Cachecade { get; set; }
        public string DeviceDescription { get; set; }
        public string DiskCachePolicy { get; set; }
        public string FQDD { get; set; }
        public string InstanceID { get; set; }
        public string LastSystemInventoryTime { get; set; }
        public string LastUpdateTime { get; set; }
        public string LockStatus { get; set; }
        public string MediaType { get; set; }
        public string Name { get; set; }
        public string ObjectStatus { get; set; }
        public string OperationName { get; set; }
        public string OperationPercentComplete { get; set; }
        public string PrimaryStatus { get; set; }
        public string RAIDStatus { get; set; }
        public string RAIDTypes { get; set; }
        public string ReadCachePolicy { get; set; }
        public string RemainingRedundancy { get; set; }
        public string RollupStatus { get; set; }
        public string SizeInBytes { get; set; }
        public string SpanDepth { get; set; }
        public string SpanLength { get; set; }
        public string StartingLBAinBlocks { get; set; }
        public string StripeSize { get; set; }
        public string T10PIStatus { get; set; }
        public string VirtualDiskTargetID { get; set; }
        public string WriteCachePolicy { get; set; }


    }


    public class DellPhysicalDisk : HasPropertyChanged
    {

        public DellPhysicalDisk()
        {

        }

        public DellPhysicalDisk(string deviceType, string blockSizeInBytes, string busProtocol, string configLockdownCapable, string configLockdownState, string connector, string deviceDescription,
            string driveFormFactor, string fqdd, string freeSizeInBytes, string hotSpareStatus, string instanceID, string lastSystemInventoryTime, string lastUpdateTime, string manufacturer, string manufacturingDay,
            string manufacturingWeek, string manufacturingYear, string maxCapableSpeed, string mediaType, string model, string operationName, string operationPercentComplete, string ppid, string predictiveFailureState,
            string primaryStatus, string raidType, string raidStatus, string remainingRatedWriteEndurance, string revision, string rollupStatus, string securityState, string serialNumber, string sizeInBytes,
            string slot,string systemEraseCapability, string t10PICapability, string updateLockdownCapable, string updateLockdownState, string usedSizeInBytes)
        {
            DeviceType = deviceType;
            BlockSizeInBytes = blockSizeInBytes;
            BusProtocol = busProtocol;
            ConfigLockdownCapable = configLockdownCapable;
            ConfigLockdownState = configLockdownState;
            Connector = connector;
            DeviceDescription = deviceDescription;
            DriveFormFactor = driveFormFactor;
            FQDD = fqdd;
            FreeSizeInBytes = freeSizeInBytes;
            HotSpareStatus = hotSpareStatus;
            InstanceID = instanceID;
            LastSystemInventoryTime = lastSystemInventoryTime;
            LastUpdateTime = lastUpdateTime;
            Manufacturer = manufacturer;
            ManufacturingDay = manufacturingDay;
            ManufacturingWeek = manufacturingWeek;
            ManufacturingYear = manufacturingYear;
            MaxCapableSpeed = maxCapableSpeed;
            MediaType = mediaType;
            Model = model;
            OperationName = operationName;
            OperationPercentComplete = operationPercentComplete;
            PPID = ppid;
            PredictiveFailureState = predictiveFailureState;
            PrimaryStatus = primaryStatus;
            RAIDType = raidType;
            RaidStatus = raidStatus;
            RemainingRatedWriteEndurance = remainingRatedWriteEndurance;
            Revision = revision;
            RollupStatus = rollupStatus;
            SASAddress = rollupStatus;
            SecurityState = securityState;
            SerialNumber = serialNumber;
            SizeInBytes = sizeInBytes;
            Slot = slot;
            SystemEraseCapability = systemEraseCapability;
            T10PICapability = t10PICapability;
            UpdateLockdownCapable = updateLockdownCapable;
            UpdateLockdownState = updateLockdownState;
            UsedSizeInBytes = usedSizeInBytes;

        }



        public string DeviceType { get; set; }
        public string BlockSizeInBytes { get; set; }
        public string BusProtocol { get; set; }
        public string ConfigLockdownCapable { get; set; }
        public string ConfigLockdownState { get; set; }
        public string Connector { get; set; }
        public string DeviceDescription { get; set; }
        public string DriveFormFactor { get; set; }
        public string FQDD { get; set; }
        public string FreeSizeInBytes { get; set; }
        public string HotSpareStatus { get; set; }
        public string InstanceID { get; set; }
        public string LastSystemInventoryTime { get; set; }
        public string LastUpdateTime { get; set; }
        public string Manufacturer { get; set; }
        public string ManufacturingDay { get; set; }
        public string ManufacturingWeek { get; set; }
        public string ManufacturingYear { get; set; }
        public string MaxCapableSpeed { get; set; }
        public string MediaType { get; set; }
        public string Model { get; set; }
        public string OperationName { get; set; }
        public string OperationPercentComplete { get; set; }
        public string PPID { get; set; }
        public string PredictiveFailureState { get; set; }
        public string PrimaryStatus { get; set; }
        public string RAIDType { get; set; }
        public string RaidStatus { get; set; }
        public string RemainingRatedWriteEndurance { get; set; }
        public string Revision { get; set; }
        public string RollupStatus { get; set; }
        public string SASAddress { get; set; }
        public string SecurityState { get; set; }
        public string SerialNumber { get; set; }
        public string SizeInBytes { get; set; }
        public string Slot { get; set; }
        public string SystemEraseCapability { get; set; }
        public string T10PICapability { get; set; }
        public string UpdateLockdownCapable { get; set; }
        public string UpdateLockdownState { get; set; }
        public string UsedSizeInBytes { get; set; }
        public string EncryptionStatus { get; set; }
        public string PartNumber { get; set; }


    }




    public class DellFans : HasPropertyChanged
    {

        public DellFans()
        {

        }


        public DellFans(string redundancyStatus, string activeCooling, string variableSpeed, string pwm, string currentReading, string rateUnits, string unitModifier,
            string baseUnits, string primaryStatus, string deviceDescription, string instanceID)
        {
            RedundancyStatus = redundancyStatus;
            ActiveCooling = activeCooling;
            VariableSpeed = variableSpeed;
            PWM = pwm;
            CurrentReading = currentReading;
            RateUnits = rateUnits;
            UnitModifier = unitModifier;
            BaseUnits = baseUnits;
            PrimaryStatus = primaryStatus;
            DeviceDescription = deviceDescription;
            InstanceID = instanceID;
          

        }



        public string RedundancyStatus { get; set; }
        public string ActiveCooling { get; set; }
        public string VariableSpeed { get; set; }
        public string PWM { get; set; }
        public string CurrentReading { get; set; }
        public string RateUnits { get; set; }
        public string UnitModifier { get; set; }
        public string BaseUnits { get; set; }
        public string PrimaryStatus { get; set; }
        public string DeviceDescription { get; set; }
        public string InstanceID { get; set; }


    }

    


    public class DellPower : HasPropertyChanged
    {
        public DellPower()
        {

        }
        public DellPower(string pmbbusonitoring, string range1MaxInputPower, string redMinNumberNeeded, string redundancyStatus,string type,string primaryStatus, string totalOutputPower,
            string inputVoltage, string firmwareVersion, string manufacturer, string partNumber, string serialNumber, string model, string deviceDescription, string instanceID )
        {

            PMBusMonitoring = pmbbusonitoring;
            Range1MaxInputPower = range1MaxInputPower;
            RedMinNumberNeeded = redMinNumberNeeded;
            RedundancyStatus = redundancyStatus;
            Type = type;
            PrimaryStatus = primaryStatus;
            TotalOutputPower = totalOutputPower;
            InputVoltage = inputVoltage;
            FirmwareVersion = firmwareVersion;
            Manufacturer = manufacturer;
            PartNumber = partNumber;
            SerialNumber = serialNumber;
            Model = model;
            DeviceDescription = deviceDescription;
            InstanceID = instanceID;
        }



     
        public string PMBusMonitoring { get; set; }
        public string Range1MaxInputPower { get; set; }
        public string RedMinNumberNeeded { get; set; }
        public string RedundancyStatus { get; set; }
        public string Type { get; set; }
        public string PrimaryStatus { get; set; }
        public string TotalOutputPower { get; set; }
        public string InputVoltage { get; set; }
        public string FirmwareVersion { get; set; }
        public string Manufacturer { get; set; }
        public string PartNumber { get; set; }
        public string SerialNumber { get; set; }
        public string Model { get; set; }
        public string DeviceDescription { get; set; }
        public string InstanceID { get; set; }

       
    }

    public class DellThermal : HasPropertyChanged
    {

        public DellThermal()
        {

        }

    
        public string Name { get; set; }
        public string MemberId { get; set; }
        public string PhysicalContext { get; set; }
        public string SensorNumber { get; set; }
        public string Health { get; set; }
        public string ReadingCelsius { get; set; }
        public string MinReadingRangeTemp { get; set; }
        public string MaxReadingRangeTemp { get; set; }
       


    }











}
