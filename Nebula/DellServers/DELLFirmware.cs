﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.DellServers
{
    public class DELLFirmware
    {

        public string odatacontext { get; set; }
        public string odataid { get; set; }
        public string odatatype { get; set; }
        public string Description { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public FirmwareStatus Status { get; set; }
        public bool Updateable { get; set; }
        public string Version { get; set; }

    }

       
    public class FirmwareStatus
    {
        public string Health { get; set; }
        public string State { get; set; }
    }


}
