﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.DellServers
{
    public class DELLComponentType
    {


     
            private ComponentPROPERTY[] PropertyField;

            private string ClassNameField;

            private string KeyField;




        public string ClassName
        {
            get
            {
                return this.ClassNameField;
            }
            set
            {
                this.ClassNameField = value;
            }
        }


        public string Key
        {
            get
            {
                return this.KeyField;
            }
            set
            {
                this.KeyField = value;
            }
        }

        public ComponentPROPERTY[] PROPERTY
            {
                get
                {
                    return this.PropertyField;
                }
                set
                {
                    this.PropertyField = value;
                }
            }

     
        }

    
        public partial class ComponentPROPERTY
        {

            private string VALUEField;

            private string DisplayValueField;

            private string NAMEField;

            private string TYPEField;

            /// <remarks/>
            public string VALUE
            {
                get
                {
                    return this.VALUEField;
                }
                set
                {
                    this.VALUEField = value;
                }
            }

            /// <remarks/>
            public string DisplayValue
            {
                get
                {
                    return this.DisplayValueField;
                }
                set
                {
                    this.DisplayValueField = value;
                }
            }

          
            public string NAME
            {
                get
                {
                    return this.NAMEField;
                }
                set
                {
                    this.NAMEField = value;
                }
            }

            public string TYPE
            {
                get
                {
                    return this.TYPEField;
                }
                set
                {
                    this.TYPEField = value;
                }
            }
        }



    }





