﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.DellServers
{


    public class DELLMemory
    {
        public string odatacontext { get; set; }
        public string odataid { get; set; }
        public string odatatype { get; set; }
        public int[] AllowedSpeedsMHz { get; set; }
        public int AllowedSpeedsMHzodatacount { get; set; }
        public object BaseModuleType { get; set; }
        public int BusWidthBits { get; set; }
        public int CapacityMiB { get; set; }
        public int DataWidthBits { get; set; }
        public string Description { get; set; }
        public string DeviceLocator { get; set; }
        public string ErrorCorrection { get; set; }
        public string Id { get; set; }
        public Links Links { get; set; }
        public string Manufacturer { get; set; }
        public object[] MaxTDPMilliWatts { get; set; }
        public int MaxTDPMilliWattsodatacount { get; set; }
        public string MemoryDeviceType { get; set; }
        public object[] MemoryMedia { get; set; }
        public int MemoryMediaodatacount { get; set; }
        public object MemoryType { get; set; }
        public Metrics Metrics { get; set; }
        public string Name { get; set; }
        public object[] OperatingMemoryModes { get; set; }
        public int OperatingMemoryModesodatacount { get; set; }
        public int OperatingSpeedMhz { get; set; }
        public string PartNumber { get; set; }
        public int RankCount { get; set; }
        public object[] Regions { get; set; }
        public int Regionsodatacount { get; set; }
        public string SerialNumber { get; set; }
        public Status Status { get; set; }
    }

    public class Links
    {
        public Chassis Chassis { get; set; }
    }

    public class Chassis
    {
        public string odataid { get; set; }
    }

    public class Metrics
    {
        public string odataid { get; set; }
    }

    public class Status
    {
        public string Health { get; set; }
        public string State { get; set; }
    }





   

}
