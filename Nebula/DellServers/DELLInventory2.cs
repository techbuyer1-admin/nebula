﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.DellServers
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class CIM
    {

        private CIMMESSAGE mESSAGEField;

        private decimal cIMVERSIONField;

        private decimal dTDVERSIONField;

        /// <remarks/>
        public CIMMESSAGE MESSAGE
        {
            get
            {
                return this.mESSAGEField;
            }
            set
            {
                this.mESSAGEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal CIMVERSION
        {
            get
            {
                return this.cIMVERSIONField;
            }
            set
            {
                this.cIMVERSIONField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal DTDVERSION
        {
            get
            {
                return this.dTDVERSIONField;
            }
            set
            {
                this.dTDVERSIONField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CIMMESSAGE
    {

        private CIMMESSAGEVALUENAMEDINSTANCE[] sIMPLEREQField;

        private ushort idField;

        private decimal pROTOCOLVERSIONField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("VALUE.NAMEDINSTANCE", IsNullable = false)]
        public CIMMESSAGEVALUENAMEDINSTANCE[] SIMPLEREQ
        {
            get
            {
                return this.sIMPLEREQField;
            }
            set
            {
                this.sIMPLEREQField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal PROTOCOLVERSION
        {
            get
            {
                return this.pROTOCOLVERSIONField;
            }
            set
            {
                this.pROTOCOLVERSIONField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CIMMESSAGEVALUENAMEDINSTANCE
    {

        private CIMMESSAGEVALUENAMEDINSTANCEINSTANCENAME iNSTANCENAMEField;

        private CIMMESSAGEVALUENAMEDINSTANCEINSTANCE iNSTANCEField;

        /// <remarks/>
        public CIMMESSAGEVALUENAMEDINSTANCEINSTANCENAME INSTANCENAME
        {
            get
            {
                return this.iNSTANCENAMEField;
            }
            set
            {
                this.iNSTANCENAMEField = value;
            }
        }

        /// <remarks/>
        public CIMMESSAGEVALUENAMEDINSTANCEINSTANCE INSTANCE
        {
            get
            {
                return this.iNSTANCEField;
            }
            set
            {
                this.iNSTANCEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CIMMESSAGEVALUENAMEDINSTANCEINSTANCENAME
    {

        private CIMMESSAGEVALUENAMEDINSTANCEINSTANCENAMEKEYBINDING kEYBINDINGField;

        private string cLASSNAMEField;

        /// <remarks/>
        public CIMMESSAGEVALUENAMEDINSTANCEINSTANCENAMEKEYBINDING KEYBINDING
        {
            get
            {
                return this.kEYBINDINGField;
            }
            set
            {
                this.kEYBINDINGField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CLASSNAME
        {
            get
            {
                return this.cLASSNAMEField;
            }
            set
            {
                this.cLASSNAMEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CIMMESSAGEVALUENAMEDINSTANCEINSTANCENAMEKEYBINDING
    {

        private CIMMESSAGEVALUENAMEDINSTANCEINSTANCENAMEKEYBINDINGKEYVALUE kEYVALUEField;

        private string nAMEField;

        /// <remarks/>
        public CIMMESSAGEVALUENAMEDINSTANCEINSTANCENAMEKEYBINDINGKEYVALUE KEYVALUE
        {
            get
            {
                return this.kEYVALUEField;
            }
            set
            {
                this.kEYVALUEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CIMMESSAGEVALUENAMEDINSTANCEINSTANCENAMEKEYBINDINGKEYVALUE
    {

        private string vALUETYPEField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string VALUETYPE
        {
            get
            {
                return this.vALUETYPEField;
            }
            set
            {
                this.vALUETYPEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CIMMESSAGEVALUENAMEDINSTANCEINSTANCE
    {

        private object[] itemsField;

        private string cLASSNAMEField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PROPERTY", typeof(CIMMESSAGEVALUENAMEDINSTANCEINSTANCEPROPERTY))]
        [System.Xml.Serialization.XmlElementAttribute("PROPERTY.ARRAY", typeof(CIMMESSAGEVALUENAMEDINSTANCEINSTANCEPROPERTYARRAY))]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CLASSNAME
        {
            get
            {
                return this.cLASSNAMEField;
            }
            set
            {
                this.cLASSNAMEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CIMMESSAGEVALUENAMEDINSTANCEINSTANCEPROPERTY
    {

        private string vALUEField;

        private string displayValueField;

        private string nAMEField;

        private string tYPEField;

        /// <remarks/>
        public string VALUE
        {
            get
            {
                return this.vALUEField;
            }
            set
            {
                this.vALUEField = value;
            }
        }

        /// <remarks/>
        public string DisplayValue
        {
            get
            {
                return this.displayValueField;
            }
            set
            {
                this.displayValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CIMMESSAGEVALUENAMEDINSTANCEINSTANCEPROPERTYARRAY
    {

        private CIMMESSAGEVALUENAMEDINSTANCEINSTANCEPROPERTYARRAYVALUEARRAY vALUEARRAYField;

        private string nAMEField;

        private string tYPEField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("VALUE.ARRAY")]
        public CIMMESSAGEVALUENAMEDINSTANCEINSTANCEPROPERTYARRAYVALUEARRAY VALUEARRAY
        {
            get
            {
                return this.vALUEARRAYField;
            }
            set
            {
                this.vALUEARRAYField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NAME
        {
            get
            {
                return this.nAMEField;
            }
            set
            {
                this.nAMEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CIMMESSAGEVALUENAMEDINSTANCEINSTANCEPROPERTYARRAYVALUEARRAY
    {

        private byte[] vALUEField;

        private string[] displayValueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("VALUE")]
        public byte[] VALUE
        {
            get
            {
                return this.vALUEField;
            }
            set
            {
                this.vALUEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DisplayValue")]
        public string[] DisplayValue
        {
            get
            {
                return this.displayValueField;
            }
            set
            {
                this.displayValueField = value;
            }
        }
    }


}
