﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nebula.Helpers;

namespace Nebula.DellServers
{
    public class DELLDataClasses : HasPropertyChanged
    {

        public DELLDataClasses(string component)
        {
            ComponentName = component;
            TheProperties = new ObservableCollection<PropertyValues>();
        }

        public string ComponentName { get; set; }

        public ObservableCollection<PropertyValues> TheProperties { get; set; }

    }

   
    public class PropertyValues
    {
        public PropertyValues(string name,string displayvalue)
        {
            Name = name;
            DisplayValue = displayvalue;
        }

        public string Name { get; set; }
        public string DisplayValue { get; set; }
    }


    








}
