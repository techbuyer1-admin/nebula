﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nebula.Helpers;

namespace Nebula.Models
{

    [Serializable]
    public class ServerBladeChecklist : HasPropertyChanged
    {


        //public string  { get; set; }
        //public string ModelNo { get; set; }
        //public string Category { get; set; }
        //public List<StepItem> Steps { get; set; }

        public string ChassisSerialNo  { get; set; }
        //GOODS IN & TEST


        public bool GoodsINQ1 { get; set; }
        public bool GoodsINQ2 { get; set; }
        public bool GoodsINQ3 { get; set; }
        public bool GoodsINQ4 { get; set; }
        public bool GoodsINQ5 { get; set; }
        public bool GoodsINQ6 { get; set; }
        public bool GoodsINQ7 { get; set; }
        public bool GoodsINQ8 { get; set; }
        public bool GoodsINQ9 { get; set; }
        public bool GoodsINQ10 { get; set; }
        public bool GoodsINQ11 { get; set; }
        public bool GoodsINQ12 { get; set; }
        public bool GoodsINQ13 { get; set; }
        public bool GoodsINQ14 { get; set; }
        public bool GoodsINQ15 { get; set; }
        public string GoodsINQ16 { get; set; } // textbox
        public bool GoodsINQ17 { get; set; }
        public bool GoodsINQ18 { get; set; }
        public bool GoodsINQ19 { get; set; }
        public bool GoodsINQ20 { get; set; }
        public bool GoodsINQ21 { get; set; }
        public bool GoodsINQ22 { get; set; }
        public bool GoodsINQ23 { get; set; }
        public bool GoodsINQ24 { get; set; }
        public bool GoodsINQ25 { get; set; }

        public string PODocumentPath { get; set; }

        public string GoodsINRep { get; set; }
        public string PurchaseOrderNo { get; set; }

        public string DateCheck { get; set; }

        //Goods Out

        public bool GoodsOUTQ1 { get; set; }
        public bool GoodsOUTQ2 { get; set; }
        public bool GoodsOUTQ3 { get; set; }
        public bool GoodsOUTQ4 { get; set; }
        public bool GoodsOUTQ5 { get; set; }
        public bool GoodsOUTQ6 { get; set; }
        public bool GoodsOUTQ7 { get; set; }
        public bool GoodsOUTQ8 { get; set; }
        public bool GoodsOUTQ9 { get; set; }
        public bool GoodsOUTQ10 { get; set; }
        public bool GoodsOUTQ11 { get; set; }
        public bool GoodsOUTQ12 { get; set; }
        public bool GoodsOUTQ13 { get; set; }
        public bool GoodsOUTQ14 { get; set; }
        public bool GoodsOUTQ15 { get; set; }
        public bool GoodsOUTQ16 { get; set; }
        public bool GoodsOUTQ17 { get; set; }
        public bool GoodsOUTQ18 { get; set; }
        public bool GoodsOUTQ19 { get; set; }
        public bool GoodsOUTQ20 { get; set; }
        public bool GoodsOUTQ21 { get; set; }
        public bool GoodsOUTQ22 { get; set; }
        public bool GoodsOUTQ23 { get; set; }
        public bool GoodsOUTQ24 { get; set; }
        public bool GoodsOUTQ25 { get; set; }
        public string GoodsOUTRep { get; set; }
        public string SalesOrderNo { get; set; }

        public string SODocumentPath { get; set; }

    }
}
