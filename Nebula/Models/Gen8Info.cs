﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Nebula.Helpers;

namespace Nebula.Models
{
    public class Gen8Info : HasPropertyChanged
    {

        public Gen8Info()
        {
            //Create new instance
            PhysicalDriveInfo = new ObservableCollection<HPPhysicalDrivesModel>();
        }


        public string TestedBy { get; set; }
        //SD CARD Presence
        public string SDCardInserted { get; set; }
        //Firmware
        public string BiosCurrentVersion { get; set; }
        public string BiosBackupVersion { get; set; }
        public string iLOVersion { get; set; }
        public string SPSVersion { get; set; }
        public string IntelligentProvisioningLocation { get; set; }
        public string IntelligentProvisioningVersion { get; set; }
        //ILO Network Port Status
        public string ILONetworkPort { get; set; }
        //Server Info
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string ChassisSerial { get; set; }
        public string AssetTag { get; set; }
        public string HostName { get; set; }
        public string HealthStatus { get; set; }
        public string SKU { get; set; }

        //public string OutOfRegionMessage { get; set; }


        //Out Of Region Visibility On HP Report
        public Visibility _OutOfRegionViz;
        public Visibility OutOfRegionViz
        {
            get { return _OutOfRegionViz; }
            set
            {
                _OutOfRegionViz = value;
                OnPropertyChanged("OutOfRegionViz");
            }
        }


        private string _OutOfRegionMessage;

        public string OutOfRegionMessage
        {
            get { return _OutOfRegionMessage; }
            set
            {
                _OutOfRegionMessage = value;
                OnPropertyChanged("OutOfRegionMessage");
            }
        }

        //memory
        public string TotalMemory { get; set; }
        public string MemoryHealth { get; set; }
        public string PopulatedSlots { get; set; }

        //cpu
        public string CPUFamily { get; set; }
        public string CPUCount { get; set; }
        public string CPUHealth { get; set; }

        public string PostState { get; set; }

        //power
        public string PowerState { get; set; }
        public string PowerAutoOn { get; set; }
        public string PowerOnDelay { get; set; }
        public string PowerRegulatorMode { get; set; }
        public string PowerAllocationLimit { get; set; }


        //iLO Self Test Results
        public string NVRAMDataNotes { get; set; }
        public string NVRAMDataStatus { get; set; }
        public string NVRAMSpaceNotes { get; set; }
        public string NVRAMSpaceStatus { get; set; }
        public string EmbeddedFlashSDCardNotes{ get; set; }
        public string EmbeddedFlashSDCardStatus { get; set; }
        public string EEPROMNotes { get; set; }
        public string EEPROMStatus { get; set; }

        public string HostRomNotes { get; set; }
        public string HostRomStatus { get; set; }
        public string SupportedHostNotes { get; set; }
        public string SupportedHostStatus { get; set; }

        public string PowerManagementControllerNotes { get; set; }
        public string PowerManagementControllerStatus { get; set; }
        public string CPLDPAL0Notes { get; set; }
        public string CPLDPAL0Status { get; set; }
        public string CPLDPAL1Notes { get; set; }
        public string CPLDPAL1Status { get; set; }

        public string ILOLicense { get; set; }

        //Embedded Health Summary
        public string BiosHardwareStatus { get; set; }
        public string FansStatus { get; set; }
        public string FansRedundancy { get; set; }
        public string TemperatureStatus { get; set; }
        public string PowerSuppliesStatus { get; set; }

        public string PowerSupplyRedundancy { get; set; }
        public string ProcessorStatus { get; set; }

        public string MemoryStatus { get; set; }
        public string NetworkStatus { get; set; }
        public string StorageStatus { get; set; }





        //REPORT Logical disk info
        public string LogicalDiskNumber { get; set; }
        public string RaidType { get; set; }
        public string LogicalDiskStatus { get; set; }

        public string PhysicalDiskNumber { get; set; }
        public string Capacity { get; set; }
        public string PhysicalDiskStatus { get; set; }
        public string InterFace { get; set; }
        public string DiskModel { get; set; }
        public string DiskLocation { get; set; }
        public string Health { get; set; }

        public StackPanel DriveReport { get; set; }


        public HealthStorageGen8 StorageInfo { get; set; }
        public ObservableCollection<HealthFansGen8> FanInfo { get; set; }
        public ObservableCollection<HealthCPUGen8> CPUInfo { get; set; }
        public ObservableCollection<HealthMemoryGen8> MemoryInfo { get; set; }

        public ObservableCollection<HealthTemperatureGen8> TemperatureInfo { get; set; }
        public ObservableCollection<HealthPowerGen8> PowerInfo { get; set; }
        public ObservableCollection<HealthNetworkGen8> NICInfo { get; set; }
        //TO HOLD PHYSICAL DRIVES SEPARATE TO LOGICAL\PHYSICAL
        public ObservableCollection<HPPhysicalDrivesModel> PhysicalDriveInfo { get; set; }

        

    }


    public class HealthTemperatureGen8
    {
        public HealthTemperatureGen8(string label, string location, string status, string current, string caution, string critical)
        {
            Label = label;
            Location = location;
            Status = status;
            CurrentReading = current;
            Caution = caution;
            Critical = critical;
        }
        public string Label { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }
        public string CurrentReading { get; set; }
        public string Caution { get; set; }
        public string Critical { get; set; }
    }

    public class HealthFansGen8
    {
        public HealthFansGen8(string label, string zone, string status, string speed)
        {
            Label = label;
            Zone = zone;
            Status = status;
            Speed = speed;
        
        }
        public string Label { get; set; }
        public string Zone { get; set; }
        public string Status { get; set; }
        public string Speed { get; set; }
      
    }

    public class HealthNetworkGen8
    {
        public HealthNetworkGen8(string netport, string prtdescription,string location, string ipaddress, string macadd, string status)
        {
            NetworkPort = netport;
            PortDescription = prtdescription;
            Location = location;
            IPAddress = ipaddress;
            MacAddress = macadd;
            Status = status;
        }
        public string NetworkPort { get; set; }
        public string PortDescription { get; set; }
        public string Location { get; set; }
        
        public string IPAddress { get; set; }
        public string MacAddress{ get; set; }
        public string Status { get; set; }


    }

    public class HealthMemoryGen8
    {
       
        public HealthMemoryGen8(string label,string socket, string status, string hpsmartmemory, string partno, string memtype, string size, string frequency, string minvoltage, string ranks, string technology)
        {
            Label = label;
            Socket = socket;
            Status = status;
            HPSmartMemory = hpsmartmemory;
            PartNo = PartNo;
            MemType = memtype;
            Size = size;
            Frequency = frequency;
            MinVoltage = minvoltage;
            Ranks = ranks;
            Technology = technology;
        }

        public string Label { get; set; }
        public string Socket { get; set; }
        public string Status { get; set; }
        public string HPSmartMemory { get; set; }
        public string PartNo { get; set; }
        public string MemType { get; set; }
        public string Size { get; set; }

        public string Frequency { get; set; }
        public string MinVoltage { get; set; }

        public string Ranks { get; set; }
        public string Technology { get; set; }
       
    }

    public class HealthPowerGen8
    {
        public HealthPowerGen8(string label, string present, string status, string pds, string hotplugcap, string model, string spare, string serial,string capacity, string firmware)
        {
            Label = label;
            Present = present;
            Status = status;
            PDS = pds;
            HotPluggable = hotplugcap;
            Model = model;
            Spare = spare;
            Serial = serial;
            Capacity = capacity;
            Firmware = firmware;

        }

        public string Label { get; set; }
        public string Present { get; set; }
        public string Status { get; set; }
        public string PDS { get; set; }
        public string HotPluggable { get; set; }
        public string Model { get; set; }
        public string Spare { get; set; }
        public string Serial { get; set; }
        public string Capacity { get; set; }
        public string Firmware { get; set; }

    }

    public class HealthCPUGen8
    {
        public HealthCPUGen8(string label, string name, string status, string speed,string exetech, string memtech,string l1cache,string l2cache, string l3cache)
        {
            Label = label;
            Name = name;
            Status = status;
            Speed = speed;
            ExecutionTech = exetech;
            MemoryTech = memtech;
            L1Cache = l1cache;
            L2Cache = l2cache;
            L3Cache = l3cache;

        }
        public string Label { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Speed { get; set; }
        public string ExecutionTech { get; set; }
        public string MemoryTech { get; set; }
        public string L1Cache { get; set; }
        public string L2Cache { get; set; }
        public string L3Cache { get; set; }

    }

    public class HealthStorageGen8
    {

        public HealthStorageGen8()
        {
            DriveEnclosure = new ObservableCollection<DriveEnclosureInfoGen8>();
            LogicalDrives = new ObservableCollection<HealthLogicalDriveGen8>();
        }
        public HealthStorageGen8(string label, string status, string controllerstatus,string serial,string model,string firmware, string discoverystatus,ObservableCollection<DriveEnclosureInfoGen8> encolsureinfo)
        {
            Label = label;
            Status = status;
            ControllerStatus = controllerstatus;
            Serial = serial;
            Model = model;
            Firmware = firmware;
            DiscoveryStatus = discoverystatus;
            LogicalDrives = new ObservableCollection<HealthLogicalDriveGen8>();
            DriveEnclosure = new ObservableCollection<DriveEnclosureInfoGen8>();
            DriveEnclosure = encolsureinfo;
        }
        public string Label { get; set; }
        public string Status { get; set; }
        public string ControllerStatus { get; set; }

        public string Serial { get; set; }
        public string Model { get; set; }
        public string Firmware { get; set; }

        public string DiscoveryStatus { get; set; }

        //public HealthLogicalDriveGen8 LogicalDrive { get; set; }
        public ObservableCollection<HealthLogicalDriveGen8> LogicalDrives { get; set; }

        public ObservableCollection<DriveEnclosureInfoGen8> DriveEnclosure { get; set; }

        
    }

    public class DriveEnclosureInfoGen8
    {
        public DriveEnclosureInfoGen8()
        {

        }
        public DriveEnclosureInfoGen8(string label, string status, string drivebay)
        {
            Label = label;
            Status = status;
            DriveBay = drivebay;
           
        }

        public string Label { get; set; }
        public string Status { get; set; }
        public string DriveBay { get; set; }

    }



    //LOGICAL DRIVE
    public class HealthLogicalDriveGen8
    {

        public HealthLogicalDriveGen8()
        {
            PhysicalDrives = new ObservableCollection<PhysicalDrivesGen8>();
        }

        public HealthLogicalDriveGen8(string label, string status, string capacity, string faulttolerance, string logicaldrivetype, string encryption, GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERLOGICAL_DRIVEPHYSICAL_DRIVE physdrives) // , ObservableCollection<PhysicalDrivesGen8> physicaldrivesinfo
        {
            //FOR USE WITH PHYSICAL ONLY DRIVES
            Label = label;
            Status = status;
            Capacity = capacity;
            FaultTolerance = faulttolerance;
            LogicalDriveType = logicaldrivetype;
            Encryption = encryption;


            PhysicalDrives.Add(new PhysicalDrivesGen8(physdrives.LABEL.VALUE.ToString(), physdrives.STATUS.VALUE.ToString(), physdrives.SERIAL_NUMBER.VALUE.ToString(), physdrives.MODEL.VALUE.ToString(), physdrives.CAPACITY.VALUE.ToString(), physdrives.MARKETING_CAPACITY.VALUE.ToString(), physdrives.LOCATION.VALUE.ToString(), physdrives.FW_VERSION.VALUE.ToString(), physdrives.DRIVE_CONFIGURATION.VALUE.ToString(), physdrives.ENCRYPTION_STATUS.VALUE.ToString(), physdrives.MEDIA_TYPE.VALUE.ToString()));

        }


        public HealthLogicalDriveGen8(string label, string status, string capacity, string faulttolerance, string logicaldrivetype, string encryption, GET_EMBEDDED_HEALTH_DATA_GEN8.GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERLOGICAL_DRIVEPHYSICAL_DRIVE[] physdrives) // , ObservableCollection<PhysicalDrivesGen8> physicaldrivesinfo
        {
            Label = label;
            Status = status;
            Capacity = capacity;
            FaultTolerance = faulttolerance;
            LogicalDriveType = logicaldrivetype;
            Encryption = encryption;

              
           
            PhysicalDrives = new ObservableCollection<PhysicalDrivesGen8>();

            PhysDrives = physdrives;

            foreach(var itm in PhysDrives)
            {

                //MessageBox.Show(itm.LABEL.VALUE);

                PhysicalDrives.Add(new PhysicalDrivesGen8(itm.LABEL.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.SERIAL_NUMBER.VALUE.ToString(), itm.MODEL.VALUE.ToString(), itm.CAPACITY.VALUE.ToString(), itm.MARKETING_CAPACITY.VALUE.ToString(), itm.LOCATION.VALUE.ToString(), itm.FW_VERSION.VALUE.ToString(), itm.DRIVE_CONFIGURATION.VALUE.ToString(), itm.ENCRYPTION_STATUS.VALUE.ToString(), itm.MEDIA_TYPE.VALUE.ToString()));
            }

           // PhysicalDrives = physicaldrivesinfo;
        }
        public string Label { get; set; }
        public string Status { get; set; }
        public string Capacity { get; set; }
        public string FaultTolerance { get; set; }
        public string LogicalDriveType { get; set; }
        public string Encryption { get; set; }

        public GET_EMBEDDED_HEALTH_DATA_GEN8.GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERLOGICAL_DRIVEPHYSICAL_DRIVE[] PhysDrives;
        public ObservableCollection<PhysicalDrivesGen8> PhysicalDrives { get; set; }


    }


    //PHYSICAL DRIVES
    public class PhysicalDrivesGen8
    {
        public PhysicalDrivesGen8()
        {

        }
        public PhysicalDrivesGen8(string label, string status, string serialnumber,string model,string capacity, string marketingcapacity, string location, string firmwareversion, string driveconfiguration, string encryptionstatus,string mediatype)
        {
            Label = label;
            Status = status;
            SerialNumber = serialnumber;
            Model = model;
            Capacity = capacity;
            MarketingCapacity = marketingcapacity;
            Location = location;
            FirmwareVersion = firmwareversion;
            DriveConfiguration = driveconfiguration;
            EncryptionStatus = encryptionstatus;
            MediaType = mediatype;
        }

        public string Label { get; set; }
        public string Status { get; set; }
        public string SerialNumber { get; set; }
        public string Model { get; set; }
        public string Capacity { get; set; }
        public string MarketingCapacity { get; set; }
        public string Location { get; set; }
        public string FirmwareVersion { get; set; }
        public string DriveConfiguration { get; set; }

        public string EncryptionStatus { get; set; }
        public string MediaType { get; set; }

    }











}
