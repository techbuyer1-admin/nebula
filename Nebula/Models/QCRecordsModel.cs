﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nebula.Helpers;

namespace Nebula.Models
{

    [Serializable]
    public class QCRecordsModel : HasPropertyChanged
    {
        public QCRecordsModel()
        {
           

        }



        private int _Id;

        public int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
                OnPropertyChanged("Id");
            }
        }

        private DateTime _Date;

        public DateTime Date
        {
            get { return _Date; }
            set
            {
                _Date = value;
                OnPropertyChanged("Date");
            }
        }



        private string _PONumber;

        public string PONumber
        {
            get { return _PONumber; }
            set
            {
                _PONumber = value;
                OnPropertyChanged("PONumber");
            }
        }

        private string _PartNumber;

        public string PartNumber
        {
            get { return _PartNumber; }
            set
            {
                _PartNumber = value;
                OnPropertyChanged("PartNumber");
            }
        }


        private string _Quantity;


        public string Quantity
        {
            get { return _Quantity; }
            set
            {
                _Quantity = value;
                OnPropertyChanged("Quantity");
            }
        }


        private string _SerialNo;


        public string SerialNo
        {
            get { return _SerialNo; }
            set
            {
                _SerialNo = value;
                OnPropertyChanged("SerialNo");
            }
        }



        private string _QCCheckedBy;

        public string QCCheckedBy
        {
            get { return _QCCheckedBy; }
            set
            {
                _QCCheckedBy = value;
                OnPropertyChanged("QCCheckedBy");
            }
        }
        private string _BookedInBy;

        public string BookedInBy
        {
            get { return _BookedInBy; }
            set
            {
                _BookedInBy = value;
                OnPropertyChanged("BookedInBy");
            }
        }

        private string _TestedBy;

        public string TestedBy
        {
            get { return _TestedBy; }
            set
            {
                _TestedBy = value;
                OnPropertyChanged("TestedBy");
            }
        }


        private string _Issue;

        public string Issue
        {
            get { return _Issue; }
            set
            {
                _Issue = value;
                OnPropertyChanged("Issue");
            }
        }


        private string _Specify;


        public string Specify
        {
            get { return _Specify; }
            set
            {
                _Specify = value;
                OnPropertyChanged("Specify");
            }
        }


        private string _Status;

        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                OnPropertyChanged("Status");
            }
        }


        private string _Comments;

        public string Comments
        {
            get { return _Comments; }
            set
            {
                _Comments = value;
                OnPropertyChanged("Comments");
            }
        }

    }


    public class Operatives : HasPropertyChanged
    {

        public Operatives()
        {



        }


        private int _Id;
        public int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
                OnPropertyChanged("Id");
            }
        }


        private string _OperativeName;
        public string OperativeName
        {
            get { return _OperativeName; }
            set
            {
                _OperativeName = value;
                OnPropertyChanged("OperativeName");
            }
        }


        private bool _isQC;

        public bool isQC
        {
            get { return _isQC; }
            set
            {
                _isQC = value;
                OnPropertyChanged("isQC");
            }
        }

    }

}
