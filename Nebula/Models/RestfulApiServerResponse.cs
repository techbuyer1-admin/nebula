﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Nebula.Models
{

    //public class RestfulApiServerResponse
    //{
    //}


    //public partial class Temperatures
    //    {
    //        [JsonProperty("Comments", NullValueHandling = NullValueHandling.Ignore)]
    //        public Comments Comments { get; set; }

    //        [JsonProperty("HpBios.1.0.0", NullValueHandling = NullValueHandling.Ignore)]
    //        public HpBios100 HpBios100 { get; set; }

    //        [JsonProperty("Chassis.1.0.0", NullValueHandling = NullValueHandling.Ignore)]
    //        public Chassis100 Chassis100 { get; set; }

    //        [JsonProperty("FwSwVersionInventory.1.2.0", NullValueHandling = NullValueHandling.Ignore)]
    //        public FwSwVersionInventory120 FwSwVersionInventory120 { get; set; }

    //        [JsonProperty("ComputerSystem.1.0.1", NullValueHandling = NullValueHandling.Ignore)]
    //        public ComputerSystem101 ComputerSystem101 { get; set; }
    //    }

    //    public partial class Chassis100
    //    {
    //        [JsonProperty("/rest/v1/Chassis/1")]
    //        public RestV1Chassis1 RestV1Chassis1 { get; set; }
    //    }

    //    public partial class RestV1Chassis1
    //    {
    //        [JsonProperty("@odata.context")]
    //        public string OdataContext { get; set; }

    //        [JsonProperty("@odata.id")]
    //        public string OdataId { get; set; }

    //        [JsonProperty("@odata.type")]
    //        public string OdataType { get; set; }

    //        [JsonProperty("ChassisType")]
    //        public string ChassisType { get; set; }

    //        [JsonProperty("Id")]
    //        [JsonConverter(typeof(StringEnumConverter))]
    //        public long Id { get; set; }

    //        [JsonProperty("Manufacturer")]
    //        public string Manufacturer { get; set; }

    //        [JsonProperty("Model")]
    //        public string Model { get; set; }

    //        [JsonProperty("Name")]
    //        public string Name { get; set; }

    //        [JsonProperty("Oem")]
    //        public RestV1Chassis1_Oem Oem { get; set; }

    //        [JsonProperty("Power")]
    //        public Power Power { get; set; }

    //        [JsonProperty("SKU")]
    //        public string Sku { get; set; }

    //        [JsonProperty("SerialNumber")]
    //        public string SerialNumber { get; set; }

    //        [JsonProperty("Status")]
    //        public RestV1Chassis1_Status Status { get; set; }

    //        [JsonProperty("Thermal")]
    //        public Power Thermal { get; set; }

    //        [JsonProperty("links")]
    //        public RestV1Chassis1_Links Links { get; set; }
    //    }

    //    public partial class RestV1Chassis1_Links
    //    {
    //        [JsonProperty("ComputerSystems")]
    //        public PowerMetrics[] ComputerSystems { get; set; }

    //        [JsonProperty("ManagedBy")]
    //        public PowerMetrics[] ManagedBy { get; set; }

    //        [JsonProperty("PowerMetrics")]
    //        public PowerMetrics PowerMetrics { get; set; }

    //        [JsonProperty("ThermalMetrics")]
    //        public PowerMetrics ThermalMetrics { get; set; }

    //        [JsonProperty("self")]
    //        public PowerMetrics Self { get; set; }
    //    }

    //    public partial class PowerMetrics
    //    {
    //        [JsonProperty("href")]
    //        public string Href { get; set; }
    //    }

    //    public partial class RestV1Chassis1_Oem
    //    {
    //        [JsonProperty("Hp")]
    //        public PurpleHp Hp { get; set; }
    //    }

    //    public partial class PurpleHp
    //    {
    //        [JsonProperty("@odata.type")]
    //        public string OdataType { get; set; }

    //        [JsonProperty("Firmware")]
    //        public Firmware Firmware { get; set; }

    //        [JsonProperty("Type")]
    //        public string Type { get; set; }
    //    }

    //    public partial class Firmware
    //    {
    //        [JsonProperty("PlatformDefinitionTable")]
    //        public Bios PlatformDefinitionTable { get; set; }

    //        [JsonProperty("PowerManagementController")]
    //        public Bios PowerManagementController { get; set; }

    //        [JsonProperty("PowerManagementControllerBootloader")]
    //        public PowerManagementControllerBootloader PowerManagementControllerBootloader { get; set; }

    //        [JsonProperty("SASProgrammableLogicDevice")]
    //        public Bios SasProgrammableLogicDevice { get; set; }

    //        [JsonProperty("SPSFirmwareVersionData")]
    //        public Bios SpsFirmwareVersionData { get; set; }

    //        [JsonProperty("SystemProgrammableLogicDevice")]
    //        public Bios SystemProgrammableLogicDevice { get; set; }
    //    }

    //    public partial class Bios
    //    {
    //        [JsonProperty("Current")]
    //        public BiosCurrent Current { get; set; }
    //    }

    //    public partial class BiosCurrent
    //    {
    //        [JsonProperty("VersionString")]
    //        public string VersionString { get; set; }
    //    }

    //    public partial class PowerManagementControllerBootloader
    //    {
    //        [JsonProperty("Current")]
    //        public PowerManagementControllerBootloaderCurrent Current { get; set; }
    //    }

    //    public partial class PowerManagementControllerBootloaderCurrent
    //    {
    //        [JsonProperty("Family")]
    //        [JsonConverter(typeof(StringEnumConverter))]
    //        public long Family { get; set; }

    //        [JsonProperty("VersionString")]
    //        public string VersionString { get; set; }
    //    }

    //    public partial class Power
    //    {
    //        [JsonProperty("@odata.id")]
    //        public string OdataId { get; set; }
    //    }

    //    public partial class RestV1Chassis1_Status
    //    {
    //        [JsonProperty("Health")]
    //        public string Health { get; set; }

    //        [JsonProperty("State")]
    //        public string State { get; set; }
    //    }

    //    public partial class Comments
    //    {
    //        [JsonProperty("BIOSDate")]
    //        public string BiosDate { get; set; }

    //        [JsonProperty("BIOSFamily")]
    //        public string BiosFamily { get; set; }

    //        [JsonProperty("Manufacturer")]
    //        public string Manufacturer { get; set; }

    //        [JsonProperty("Model")]
    //        public string Model { get; set; }
    //    }

    //    public partial class ComputerSystem101
    //    {
    //        [JsonProperty("/rest/v1/Systems/1")]
    //        public RestV1Systems1 RestV1Systems1 { get; set; }
    //    }

    //    public partial class RestV1Systems1
    //    {
    //        [JsonProperty("@odata.context")]
    //        public string OdataContext { get; set; }

    //        [JsonProperty("@odata.id")]
    //        public string OdataId { get; set; }

    //        [JsonProperty("@odata.type")]
    //        public string OdataType { get; set; }

    //        [JsonProperty("Actions")]
    //        public RestV1Systems1_Actions Actions { get; set; }

    //        [JsonProperty("AssetTag")]
    //        public string AssetTag { get; set; }

    //        [JsonProperty("AvailableActions")]
    //        public AvailableAction[] AvailableActions { get; set; }

    //        [JsonProperty("Bios")]
    //        public Bios Bios { get; set; }

    //        [JsonProperty("BiosVersion")]
    //        public string BiosVersion { get; set; }

    //        [JsonProperty("Boot")]
    //        public Boot Boot { get; set; }

    //        [JsonProperty("Description")]
    //        public string Description { get; set; }

    //        [JsonProperty("EthernetInterfaces")]
    //        public Power EthernetInterfaces { get; set; }

    //        [JsonProperty("HostCorrelation")]
    //        public HostCorrelation HostCorrelation { get; set; }

    //        [JsonProperty("HostName")]
    //        public string HostName { get; set; }

    //        [JsonProperty("Id")]
    //        [JsonConverter(typeof(StringEnumConverter))]
    //        public long Id { get; set; }

    //        [JsonProperty("IndicatorLED")]
    //        public string IndicatorLed { get; set; }

    //        [JsonProperty("LogServices")]
    //        public Power LogServices { get; set; }

    //        [JsonProperty("Manufacturer")]
    //        public string Manufacturer { get; set; }

    //        [JsonProperty("Memory")]
    //        public Memory Memory { get; set; }

    //        [JsonProperty("MemorySummary")]
    //        public MemorySummary MemorySummary { get; set; }

    //        [JsonProperty("Model")]
    //        public string Model { get; set; }

    //        [JsonProperty("Name")]
    //        public string Name { get; set; }

    //        [JsonProperty("Oem")]
    //        public RestV1Systems1_Oem Oem { get; set; }

    //        [JsonProperty("Power")]
    //        public string Power { get; set; }

    //        [JsonProperty("PowerState")]
    //        public string PowerState { get; set; }

    //        [JsonProperty("ProcessorSummary")]
    //        public ProcessorSummary ProcessorSummary { get; set; }

    //        [JsonProperty("Processors")]
    //        public Processors Processors { get; set; }

    //        [JsonProperty("SKU")]
    //        public string Sku { get; set; }

    //        [JsonProperty("SerialNumber")]
    //        public string SerialNumber { get; set; }

    //        [JsonProperty("Status")]
    //        public RestV1Chassis1_Status Status { get; set; }

    //        [JsonProperty("SystemType")]
    //        public string SystemType { get; set; }

    //        [JsonProperty("UUID")]
    //        public string Uuid { get; set; }

    //        [JsonProperty("links")]
    //        public RestV1Systems1_Links Links { get; set; }
    //    }

    //    public partial class RestV1Systems1_Actions
    //    {
    //        [JsonProperty("#ComputerSystem.Reset")]
    //        public SystemReset ComputerSystemReset { get; set; }
    //    }

    //    public partial class SystemReset
    //    {
    //        [JsonProperty("ResetType@Redfish.AllowableValues")]
    //        public string[] ResetTypeRedfishAllowableValues { get; set; }

    //        [JsonProperty("target")]
    //        public string Target { get; set; }
    //    }

    //    public partial class AvailableAction
    //    {
    //        [JsonProperty("Action")]
    //        public string Action { get; set; }

    //        [JsonProperty("Capabilities")]
    //        public Capability[] Capabilities { get; set; }
    //    }

    //    public partial class Capability
    //    {
    //        [JsonProperty("AllowableValues")]
    //        public string[] AllowableValues { get; set; }

    //        [JsonProperty("PropertyName")]
    //        public string PropertyName { get; set; }
    //    }

    //    public partial class Boot
    //    {
    //        [JsonProperty("BootSourceOverrideEnabled")]
    //        public string BootSourceOverrideEnabled { get; set; }

    //        [JsonProperty("BootSourceOverrideSupported")]
    //        public string[] BootSourceOverrideSupported { get; set; }

    //        [JsonProperty("BootSourceOverrideTarget")]
    //        public string BootSourceOverrideTarget { get; set; }
    //    }

    //    public partial class HostCorrelation
    //    {
    //        [JsonProperty("HostMACAddress")]
    //        public string[] HostMacAddress { get; set; }

    //        [JsonProperty("HostName")]
    //        public string HostName { get; set; }

    //        [JsonProperty("IPAddress")]
    //        public string[] IpAddress { get; set; }
    //    }

    //    public partial class RestV1Systems1_Links
    //    {
    //        [JsonProperty("Chassis")]
    //        public PowerMetrics[] Chassis { get; set; }

    //        [JsonProperty("EthernetInterfaces")]
    //        public PowerMetrics EthernetInterfaces { get; set; }

    //        [JsonProperty("Logs")]
    //        public PowerMetrics Logs { get; set; }

    //        [JsonProperty("ManagedBy")]
    //        public PowerMetrics[] ManagedBy { get; set; }

    //        [JsonProperty("Processors")]
    //        public PowerMetrics Processors { get; set; }

    //        [JsonProperty("self")]
    //        public PowerMetrics Self { get; set; }
    //    }

    //    public partial class Memory
    //    {
    //        [JsonProperty("Status")]
    //        public MemoryStatus Status { get; set; }

    //        [JsonProperty("TotalSystemMemoryGB")]
    //        public long TotalSystemMemoryGb { get; set; }
    //    }

    //    public partial class MemoryStatus
    //    {
    //        [JsonProperty("HealthRollUp")]
    //        public string HealthRollUp { get; set; }
    //    }

    //    public partial class MemorySummary
    //    {
    //        [JsonProperty("Status")]
    //        public MemoryStatus Status { get; set; }

    //        [JsonProperty("TotalSystemMemoryGiB")]
    //        public long TotalSystemMemoryGiB { get; set; }
    //    }

    //    public partial class RestV1Systems1_Oem
    //    {
    //        [JsonProperty("Hp")]
    //        public FluffyHp Hp { get; set; }
    //    }

    //    public partial class FluffyHp
    //    {
    //        [JsonProperty("@odata.type")]
    //        public string OdataType { get; set; }

    //        [JsonProperty("Actions")]
    //        public HpActions Actions { get; set; }

    //        [JsonProperty("AvailableActions")]
    //        public AvailableAction[] AvailableActions { get; set; }

    //        [JsonProperty("Bios")]
    //        public HpBios Bios { get; set; }

    //        [JsonProperty("DeviceDiscoveryComplete")]
    //        public DeviceDiscoveryComplete DeviceDiscoveryComplete { get; set; }

    //        [JsonProperty("IntelligentProvisioningIndex")]
    //        public long IntelligentProvisioningIndex { get; set; }

    //        [JsonProperty("IntelligentProvisioningLocation")]
    //        public string IntelligentProvisioningLocation { get; set; }

    //        [JsonProperty("IntelligentProvisioningVersion")]
    //        public string IntelligentProvisioningVersion { get; set; }

    //        [JsonProperty("PostState")]
    //        public string PostState { get; set; }

    //        [JsonProperty("PowerAllocationLimit")]
    //        public long PowerAllocationLimit { get; set; }

    //        [JsonProperty("PowerAutoOn")]
    //        public string PowerAutoOn { get; set; }

    //        [JsonProperty("PowerOnDelay")]
    //        public string PowerOnDelay { get; set; }

    //        [JsonProperty("PowerRegulatorMode")]
    //        public string PowerRegulatorMode { get; set; }

    //        [JsonProperty("PowerRegulatorModesSupported")]
    //        public string[] PowerRegulatorModesSupported { get; set; }

    //        [JsonProperty("TrustedModules")]
    //        public TrustedModule[] TrustedModules { get; set; }

    //        [JsonProperty("Type")]
    //        public string Type { get; set; }

    //        [JsonProperty("VirtualProfile")]
    //        public string VirtualProfile { get; set; }

    //        [JsonProperty("links")]
    //        public HpLinks Links { get; set; }
    //    }

    //    public partial class HpActions
    //    {
    //        [JsonProperty("#HpComputerSystemExt.PowerButton")]
    //        public HpComputerSystemExtPowerButton HpComputerSystemExtPowerButton { get; set; }

    //        [JsonProperty("#HpComputerSystemExt.SystemReset")]
    //        public SystemReset HpComputerSystemExtSystemReset { get; set; }
    //    }

    //    public partial class HpComputerSystemExtPowerButton
    //    {
    //        [JsonProperty("PushType@Redfish.AllowableValues")]
    //        public string[] PushTypeRedfishAllowableValues { get; set; }

    //        [JsonProperty("target")]
    //        public string Target { get; set; }
    //    }

    //    public partial class HpBios
    //    {
    //        [JsonProperty("Backup")]
    //        public Backup Backup { get; set; }

    //        [JsonProperty("Bootblock")]
    //        public Backup Bootblock { get; set; }

    //        [JsonProperty("Current")]
    //        public Backup Current { get; set; }

    //        [JsonProperty("UefiClass")]
    //        public long UefiClass { get; set; }
    //    }

    //    public partial class Backup
    //    {
    //        [JsonProperty("Date")]
    //        public string Date { get; set; }

    //        [JsonProperty("Family")]
    //        public string Family { get; set; }

    //        [JsonProperty("VersionString")]
    //        public string VersionString { get; set; }
    //    }

    //    public partial class DeviceDiscoveryComplete
    //    {
    //        [JsonProperty("AMSDeviceDiscovery")]
    //        public string AmsDeviceDiscovery { get; set; }

    //        [JsonProperty("DeviceDiscovery")]
    //        public string DeviceDiscovery { get; set; }

    //        [JsonProperty("SmartArrayDiscovery")]
    //        public string SmartArrayDiscovery { get; set; }
    //    }

    //    public partial class HpLinks
    //    {
    //        [JsonProperty("BIOS")]
    //        public PowerMetrics Bios { get; set; }

    //        [JsonProperty("EthernetInterfaces")]
    //        public PowerMetrics EthernetInterfaces { get; set; }

    //        [JsonProperty("FirmwareInventory")]
    //        public PowerMetrics FirmwareInventory { get; set; }

    //        [JsonProperty("Memory")]
    //        public PowerMetrics Memory { get; set; }

    //        [JsonProperty("NetworkAdapters")]
    //        public PowerMetrics NetworkAdapters { get; set; }

    //        [JsonProperty("PCIDevices")]
    //        public PowerMetrics PciDevices { get; set; }

    //        [JsonProperty("PCISlots")]
    //        public PowerMetrics PciSlots { get; set; }

    //        [JsonProperty("SmartStorage")]
    //        public PowerMetrics SmartStorage { get; set; }

    //        [JsonProperty("SoftwareInventory")]
    //        public PowerMetrics SoftwareInventory { get; set; }
    //    }

    //    public partial class TrustedModule
    //    {
    //        [JsonProperty("Status")]
    //        public string Status { get; set; }
    //    }

    //    public partial class ProcessorSummary
    //    {
    //        [JsonProperty("Count")]
    //        public long Count { get; set; }

    //        [JsonProperty("Model")]
    //        public string Model { get; set; }

    //        [JsonProperty("Status")]
    //        public MemoryStatus Status { get; set; }
    //    }

    //    public partial class Processors
    //    {
    //        [JsonProperty("Count")]
    //        public long Count { get; set; }

    //        [JsonProperty("ProcessorFamily")]
    //        public string ProcessorFamily { get; set; }

    //        [JsonProperty("Status")]
    //        public MemoryStatus Status { get; set; }
    //    }

    //    public partial class FwSwVersionInventory120
    //    {
    //        [JsonProperty("/rest/v1/Systems/1/FirmwareInventory")]
    //        public RestV1Systems1FirmwareInventory RestV1Systems1FirmwareInventory { get; set; }
    //    }

    //    public partial class RestV1Systems1FirmwareInventory
    //    {
    //        [JsonProperty("@odata.context")]
    //        public string OdataContext { get; set; }

    //        [JsonProperty("@odata.id")]
    //        public string OdataId { get; set; }

    //        [JsonProperty("@odata.type")]
    //        public string OdataType { get; set; }

    //        [JsonProperty("Current")]
    //        public Dictionary<string, CurrentElement[]> Current { get; set; }

    //        [JsonProperty("Id")]
    //        public string Id { get; set; }

    //        [JsonProperty("Name")]
    //        public string Name { get; set; }

    //        [JsonProperty("links")]
    //        public RestV1Systems1FirmwareInventoryLinks Links { get; set; }
    //    }

    //    public partial class CurrentElement
    //    {
    //        [JsonProperty("Location")]
    //        public string Location { get; set; }

    //        [JsonProperty("Name")]
    //        public string Name { get; set; }

    //        [JsonProperty("VersionString")]
    //        public string VersionString { get; set; }

    //        [JsonProperty("Key", NullValueHandling = NullValueHandling.Ignore)]
    //        public string Key { get; set; }
    //    }

    //    public partial class RestV1Systems1FirmwareInventoryLinks
    //    {
    //        [JsonProperty("self")]
    //        public PowerMetrics Self { get; set; }
    //    }

    //    public partial class HpBios100
    //    {
    //        [JsonProperty("/rest/v1/Systems/1/Bios")]
    //        public RestV1Systems1Bios RestV1Systems1Bios { get; set; }
    //    }

    //    public partial class RestV1Systems1Bios
    //    {
    //        [JsonProperty("@odata.context")]
    //        public string OdataContext { get; set; }

    //        [JsonProperty("@odata.id")]
    //        public string OdataId { get; set; }

    //        [JsonProperty("@odata.type")]
    //        public string OdataType { get; set; }

    //        [JsonProperty("AutoPowerOn")]
    //        public string AutoPowerOn { get; set; }

    //        [JsonProperty("Id")]
    //        public string Id { get; set; }

    //        [JsonProperty("Name")]
    //        public string Name { get; set; }

    //        [JsonProperty("PowerOnDelay")]
    //        public string PowerOnDelay { get; set; }

    //        [JsonProperty("PowerRegulator")]
    //        public string PowerRegulator { get; set; }

    //        [JsonProperty("SwapROM")]
    //        public bool SwapRom { get; set; }

    //        [JsonProperty("TpmOpromMeasuring")]
    //        public string TpmOpromMeasuring { get; set; }

    //        [JsonProperty("TpmState")]
    //        public string TpmState { get; set; }

    //        [JsonProperty("links")]
    //        public RestV1Systems1FirmwareInventoryLinks Links { get; set; }
    //    }
  
}

