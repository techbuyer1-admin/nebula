﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Models
{
    public class PrivateKeyInfo
    {

        //https://stackoverflow.com/questions/55378001/renci-sshnet-common-sshexception-invalid-private-key-file-when-loading-ssh-pr


        public PrivateKeyInfo()
        {



            //            var keyStr = @"-----BEGIN RSA PRIVATE KEY-----
            //MIIEogIBAAKCAQEA0FEQwE1POzxYgjf8/fFiKda01TRkRfOj/BPA++U2+itBRILw
            //uJjqf9/f6bvIDO10surcatC3LLSP4N0ijBD8edAPtqdZNgC/l7vUoXmhecp2LNy2
            //zQWvy55PaHOJEfxnSS8Dcq0BfXE8xTX2DK9sMH0E5MuZ+klfMq8Mloc7N8HqC9hS
            //u1DeLc4owmQ/e/ahfilV8vtAkp8/QEOc2bs0uvOQF60y/fuEX2Tm2CuNYS64Cwwz
            //8Fx0pUrVN/olGwGkZ4k2A2NaVV3wt13zA/X+MT3SQEoKfMqm41umtFYBVo1vQXdo
            //kOGnSxLEgiXMLEjxjG0e+s4tBHqySFVtow5fXQIDAQABAoIBABHhM1feCtPhvRMI
            //LS26nxtKM2EtSwH6BHQpWeIE7XARcFuqBXFCprXtCDHujSAW7XJB2ENOwLoUkVjS
            //ghmVh6YI5qBdwrlw3QDv/TMS1no2l2gIVaRJPapzXwtvBy/FAzRz1Y7tLHiQ1I90
            //rzatg9KxmE7u7rtdus8F2UW+O6XwVjoXPnhQjxsdiyt7zd/7HbGtp3L0PPMSPn/S
            //QxbpUzMsb6w+C8x4KTYL7AmPwjlI4ifQ9djr9tP9VCcyGnz7jWLHSgev/XKVnyk5
            //58G15MbMAlF4kpNYc0RmdrlUUiNswOLG0lUbBCXGxay/uCYkAggcaip+nTvHXuVL
            //nIru7z0CgYEA6fWkReim4kGJc6VumQtTNFuWh7vVQunsdtG99qIuXgtdWew0Y2Jt
            //DpbZPYbnC2puNyRZx2GGUF6+d4rKPoHdWBUPzuBCrkD532NgP0h8qRoGtjzYi+Zc
            //ZVDJCiiHnpOQE4Xd6U6lPO68NvfS4atZfEWIXeKwd5Ih1fVwWURBaAsCgYEA4/D/
            //0sVT8u0xzrBqsDwSzonG3UwkrOh/AsYkThl+c7jXkpJuS8i0kUM8yW0hM2jjkpK5
            //RBqcFLZ/nccd5IvZx5jCF8RTusDrnIILlXB80IfpGR/Y5MOH8SlFsSwMfLbvJQzC
            //XHIwV/ApbkvGEFRrGp0GmhhmLMJIWtwiHgoVLzcCgYB+xhRVrVOAlnKcr6/WTkSv
            //wlE3C5jrwUXYvJ5q3kaxvwa+izd4AwWFOkhNBqR7kxhV2OStHTNiqXQb4qDkbgo6
            //iG8gcnSvZ5X+biLhJ64lE20/oojUhebbH4NQddXHH76aVZeBrJPtCJAEWRV0Ix1C
            //MYtLFGL657xd67kNJecrMQKBgFJe7eO54WthIeWiRPuoydcPXOjtsjc10WgsOERs
            //3BBvlJ9oASmrvuzbkmTr5dqRJeP6165vu0WU6asXoIrNqwaBAmraTLOpMbueFxeK
            //zeABrU+h/Zw1uLOCiafL1jtfVC5cXQRRU30D0cUE5u9Rwl6crh0TgiDcmI8VC9M1
            //K5CjAoGAHntrr8CGvy/cUlsAiGTGppPmJMcgd1A/b8CUkv7U62Kd1AWM+2ecI8h/
            //rjbRS3IOfgsMjiL2sxagedcli+RAYGG52hL08QUUTdA4vzZYrmwbIXKtCBcHfC4E
            //01zg1+BBh4H/htvm/TxYULA43UxgIEgSWG6GDamZdhOJVTEPDaw=
            //-----END RSA PRIVATE KEY-----
            //";
            string str = File.ReadAllText(@"C:\Users\NJM\AppData\Local\Packages\CanonicalGroupLimited.UbuntuonWindows_79rhkp1fndgsc\LocalState\rootfs\home\nick\.ssh\id_rsa");

            //Load key above into a memory stream to use tin the connection
            using (var keystrm = new MemoryStream(Encoding.ASCII.GetBytes(str)))
            {
                //set the PrivateKeyInfo property ;
                ThePrivateKey = new PrivateKeyFile(keystrm);
            }



            // string str = Convert.ToBase64String(File.ReadAllBytes(@"C:\Users\NJM\AppData\Local\Packages\CanonicalGroupLimited.UbuntuonWindows_79rhkp1fndgsc\LocalState\rootfs\home\nick\.ssh\id_rsa"));


            //Console.WriteLine(File.ReadAllText(@"C:\Users\NJM\AppData\Local\Packages\CanonicalGroupLimited.UbuntuonWindows_79rhkp1fndgsc\LocalState\rootfs\home\nick\.ssh\id_rsa"));
        }


        private PrivateKeyFile _ThePrivateKey;

        public PrivateKeyFile ThePrivateKey
        {
            get { return _ThePrivateKey; }
            set
            {
                _ThePrivateKey = value;
            }
        }


        //public void sftpConnect()
        //{
        //    var fileLength = data.Length;

        //    var keyStr = ConfigurationManager.ConnectionStrings["SftpKey"].ConnectionString;
        //    using (var keystrm = new MemoryStream(Convert.FromBase64String(keyStr)))
        //    {
        //        var privateKey = new PrivateKeyFile(keystrm);
        //        using (var ftp = new SftpClient(_ftpServer, _ftpUser, new[] { privateKey }))
        //        {
        //            ftp.ErrorOccurred += ErrorOccurred;
        //            ftp.Connect();
        //            ftp.ChangeDirectory(_ftpPath);
        //            using (var dataStream = new MemoryStream(Encoding.UTF8.GetBytes(data)))
        //            {
        //                ftp.UploadFile(dataStream, Path.GetFileName(message.MessageId), true,
        //                    (length) => result = fileLength == (int)length);
        //            }
        //            ftp.Disconnect();
        //        }
        //    }
        //}
    }
}
