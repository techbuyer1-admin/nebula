﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nebula.Helpers;

namespace Nebula.Models
{
    public class ITADReport : HasPropertyChanged, IEnumerable
    {

        public ITADReport()
        {
            ReportCollection = new ObservableCollection<ITADReport>();
        }

        public ITADReport(string intid, string productcode, string productdescription, string quantity, string printquantity, string serialnumber)
        {
            //Quantity = quantity;
            //SerialNumber = serialnumber;
        }


        public int _InternalID;
        public int InternalID
        {
            get { return _InternalID; }
            set
            {
                _InternalID = value;
                OnPropertyChanged("InternalID");
            }
        }

        private DateTime _DateCreated;
        public DateTime DateCreated
        {
            get { return _DateCreated; }
            set
            {
                _DateCreated = value;
                OnPropertyChanged("DateCreated");
            }
        }

        //private string _DateCreated;
        //public string DateCreated
        //{
        //    get { return _DateCreated; }
        //    set
        //    {
        //        _DateCreated = value;
        //        OnPropertyChanged("DateCreated");
        //    }
        //}

        public String _POSONumber;
        public String POSONumber
        {
            get { return _POSONumber; }
            set
            {
                _POSONumber = value;
                OnPropertyChanged("POSONumber");
            }
        }

        public String _AssetNo;
        public String AssetNo
        {
            get { return _AssetNo; }
            set
            {
                _AssetNo = value;
                OnPropertyChanged("AssetNo");
            }
        }


        public String _SerialNo;
        public String SerialNo
        {
            get { return _SerialNo; }
            set
            {
                _SerialNo = value;
                OnPropertyChanged("SerialNo");
            }
        }

    

        public String _Manufacturer;
        public String Manufacturer
        {
            get { return _Manufacturer; }
            set
            {
                _Manufacturer = value;
                OnPropertyChanged("Manufacturer");
            }
        }

        public String _ModelNo;
        public String ModelNo
        {
            get { return _ModelNo; }
            set
            {
                _ModelNo = value;
                OnPropertyChanged("ModelNo");
            }
        }

        public String _DeviceCategory;
        public String DeviceCategory
        {
            get { return _DeviceCategory; }
            set
            {
                _DeviceCategory = value;
                OnPropertyChanged("DeviceCategory");
            }
        }

        public String _ProcessPerformed;
        public String ProcessPerformed
        {
            get { return _ProcessPerformed; }
            set
            {
                _ProcessPerformed = value;
                OnPropertyChanged("ProcessPerformed");
            }
        }


        public String _TestingPerformed;
        public String TestingPerformed
        {
            get { return _TestingPerformed; }
            set
            {
                _TestingPerformed = value;
                OnPropertyChanged("TestingPerformed");
            }
        }


        public String _Notes;
        public String Notes
        {
            get { return _Notes; }
            set
            {
                _Notes = value;
                OnPropertyChanged("Notes");
            }
        }

        public String _ReportBody;
        public String ReportBody
        {
            get { return _ReportBody; }
            set
            {
                _ReportBody = value;
                OnPropertyChanged("ReportBody");
            }
        }

        public String _SavePath;
        public String SavePath
        {
            get { return _SavePath; }
            set
            {
                _SavePath = value;
                OnPropertyChanged("SavePath");
            }
        }
        public String _TestedBy;
        public String TestedBy
        {
            get { return _TestedBy; }
            set
            {
                _TestedBy = value;
                OnPropertyChanged("TestedBy");
            }
        }

        public String _VerifiedBy;
        public String VerifiedBy
        {
            get { return _VerifiedBy; }
            set
            {
                _VerifiedBy = value;
                OnPropertyChanged("VerifiedBy");
            }
        }

        public String _VerificationPerformed;
        public String VerificationPerformed
        {
            get { return _VerificationPerformed; }
            set
            {
                _VerificationPerformed = value;
                OnPropertyChanged("VerificationPerformed");
            }
        }




        public ObservableCollection<ITADReport> _ReportCollection;
        public ObservableCollection<ITADReport> ReportCollection
        {
            get { return _ReportCollection; }
            set
            {
                _ReportCollection = value;
                OnPropertyChanged("ReportCollection");
            }
        }


      

        public IEnumerator GetEnumerator()
        {

            foreach (object o in ReportCollection)
            {
                if (o == null)
                {
                    break;
                }
                yield return o;
            }


            //throw new NotImplementedException();
        }
    }

    public class ITADProcessesPerformed : HasPropertyChanged
    {
        public ITADProcessesPerformed()
        {

        }
        
        public String _Process;
        public String Process
        {
            get { return _Process; }
            set
            {
                _Process = value;
                OnPropertyChanged("Process");
            }
        }
    }

    public class ITADTestingPerformed : HasPropertyChanged
    {
        public ITADTestingPerformed()
        {

        }

        public String _Process;
        public String Process
        {
            get { return _Process; }
            set
            {
                _Process = value;
                OnPropertyChanged("Process");
            }
        }
    }


    public class ITADVerificationPerformed : HasPropertyChanged
    {
        public ITADVerificationPerformed()
        {

        }

        public String _Process;
        public String Process
        {
            get { return _Process; }
            set
            {
                _Process = value;
                OnPropertyChanged("Process");
            }
        }
    }



    public class ITADDeviceCategories : HasPropertyChanged
    {
        public ITADDeviceCategories()
        {

        }

        public String _Manufacturer;
        public String Manufacturer
        {
            get { return _Manufacturer; }
            set
            {
                _Manufacturer = value;
                OnPropertyChanged("Manufacturer");
            }
        }

        public String _Model;
        public String Model
        {
            get { return _Model; }
            set
            {
                _Model = value;
                OnPropertyChanged("Model");
            }
        }

        public String _Category;
        public String Category
        {
            get { return _Category; }
            set
            {
                _Category = value;
                OnPropertyChanged("Category");
            }
        }


       


    }
}
