﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nebula.Helpers;

namespace Nebula.Models
{

    [Serializable]
    public class NetsuiteInventory : HasPropertyChanged, IEnumerable
    {
      
        public NetsuiteInventory()
        {
         
            //Collection to hold serials
            SerialNumbers = new ObservableCollection<string>();
            //Collection to hold Multiple Various Items
            VariousCollection = new ObservableCollection<NetsuiteInventory>();
        }

        public NetsuiteInventory(string intid,string productcode,string productdescription,string quantity,string printquantity,string serialnumber)
        {
            ProductCode = productcode;
            ProductDescription = productdescription;
            PrintQuantity = printquantity;
            Quantity = quantity;
            SerialNumber = serialnumber;
        }


        public String _InternalID;
        public String InternalID
        {
            get { return _InternalID; }
            set
            {
                _InternalID = value;
                OnPropertyChanged("InternalID");
            }
        }


        public String _ProductCode;
        public String ProductCode
        {
            get { return _ProductCode; }
            set
            {
                _ProductCode = value;
                OnPropertyChanged("ProductCode");
            }
        }


        public String _ProductDescription;
        public String ProductDescription
        {
            get { return _ProductDescription; }
            set
            {
                _ProductDescription = value;
                OnPropertyChanged("ProductDescription");
            }
        }

        public String _PrintQuantity;
        public String PrintQuantity
        {
            get { return _PrintQuantity; }
            set
            {
                _PrintQuantity = value;
                OnPropertyChanged("PrintQuantity");
            }
        }


        //Additional Properties for Drive Test

        //private string _ProcessedDate;
        //public string ProcessedDate
        //{
        //    get { return _ProcessedDate; }
        //    set
        //    {
        //        _ProcessedDate = value;
        //        OnPropertyChanged("ProcessedDate");
        //    }
        //}

        private DateTime _ProcessedDate;
        public DateTime ProcessedDate
        {
            get { return _ProcessedDate; }
            set
            {
                _ProcessedDate = value;
                OnPropertyChanged("ProcessedDate");
            }
        }

        private string _LineType;
        public string LineType
        {
            get { return _LineType; }
            set
            {
                _LineType = value;
                OnPropertyChanged("LineType");
            }
        }

        private string _POLineUnitPrice;
        public string POLineUnitPrice
        {
            get { return _POLineUnitPrice; }
            set
            {
                _POLineUnitPrice = value;
                OnPropertyChanged("POLineUnitPrice");
            }
        }

        private string _POUnitAmount;
        public string POUnitAmount
        {
            get { return _POUnitAmount; }
            set
            {
                _POUnitAmount = value;
                OnPropertyChanged("POUnitAmount");
            }
        }
        public String _PONumber;
        public String PONumber
        {
            get { return _PONumber; }
            set
            {
                _PONumber = value;
                OnPropertyChanged("PONumber");
            }
        }


        public String _ReceivedByOperative;
        public String ReceivedByOperative
        {
            get { return _ReceivedByOperative; }
            set
            {
                _ReceivedByOperative = value;
                OnPropertyChanged("ReceivedByOperative");
            }
        }


        public String _TestedByOperative;
        public String TestedByOperative
        {
            get { return _TestedByOperative; }
            set
            {
                _TestedByOperative = value;
                OnPropertyChanged("TestedByOperative");
            }
        }

        public String _Quantity;
        public String Quantity
        {
            get { return _Quantity; }
            set
            {
                _Quantity = value;
                OnPropertyChanged("Quantity");
            }
        }

        public String _POQuantity;
        public String POQuantity
        {
            get { return _POQuantity; }
            set
            {
                _POQuantity = value;
                OnPropertyChanged("POQuantity");
            }
        }

        public int _ImportTotal;
        public int ImportTotal
        {
            get { return _ImportTotal; }
            set
            {
                _ImportTotal = value;
                OnPropertyChanged("ImportTotal");
            }
        }

        public String _RecievedQuantity;
        public String RecievedQuantity
        {
            get { return _RecievedQuantity; }
            set
            {
                _RecievedQuantity = value;
                OnPropertyChanged("RecievedQuantity");
            }
        }

        public String _DiscrepancyQuantity;
        public String DiscrepancyQuantity
        {
            get { return _DiscrepancyQuantity; }
            set
            {
                _DiscrepancyQuantity = value;
                OnPropertyChanged("DiscrepancyQuantity");
            }
        }


        public string _PartCodeRequired;
        public string PartCodeRequired
        {
            get { return _PartCodeRequired; }
            set
            {
                _PartCodeRequired = value;
                OnPropertyChanged("PartCodeRequired");
            }
        }

        //New Part Code boolean field
        public bool _NewPartCodeRequired;
        public bool NewPartCodeRequired
        {
            get { return _NewPartCodeRequired; }
            set
            {
                _NewPartCodeRequired = value;
                OnPropertyChanged("NewPartCodeRequired");
            }
        }

        //EraseIT
        public bool _EraseITUsed;
        public bool EraseITUsed
        {
            get { return _EraseITUsed; }
            set
            {
                _EraseITUsed = value;
                OnPropertyChanged("EraseITUsed");
            }
        }


        public String _SerialNumber;
        public String SerialNumber
        {
            get { return _SerialNumber; }
            set
            {
                _SerialNumber = value;
                OnPropertyChanged("SerialNumber");
            }
        }

        public String _CurrentFirmware;
        public String CurrentFirmware
        {
            get { return _CurrentFirmware; }
            set
            {
                _CurrentFirmware = value;
                OnPropertyChanged("CurrentFirmware");
            }
        }


        public String _HPSerialNumber;
        public String HPSerialNumber
        {
            get { return _HPSerialNumber; }
            set
            {
                _HPSerialNumber = value;
                OnPropertyChanged("HPSerialNumber");
            }
        }

        public String _ATF;
        public String ATF
        {
            get { return _ATF; }
            set
            {
                _ATF = value;
                OnPropertyChanged("ATF");
            }
        }


        public String _SevenDigit;
        public String SevenDigit
        {
            get { return _SevenDigit; }
            set
            {
                _SevenDigit = value;
                OnPropertyChanged("SevenDigit");
            }
        }



        public String _DriveResult;
        public String DriveResult
        {
            get { return _DriveResult; }
            set
            {
                _DriveResult = value;
                OnPropertyChanged("DriveResult");
            }
        }

        public String _DriveFailedReason;
        public String DriveFailedReason
        {
            get { return _DriveFailedReason; }
            set
            {
                _DriveFailedReason = value;
                OnPropertyChanged("DriveFailedReason");
            }
        }

        public String _DriveFailedReasonSpecify;
        public String DriveFailedReasonSpecify
        {
            get { return _DriveFailedReasonSpecify; }
            set
            {
                _DriveFailedReasonSpecify = value;
                OnPropertyChanged("DriveFailedReasonSpecify");
            }
        }

        public String _WipeSoftware;
        public String WipeSoftware
        {
            get { return _WipeSoftware; }
            set
            {
                _WipeSoftware = value;
                OnPropertyChanged("WipeSoftware");
            }
        }


        public String _ReportPath;
        public String ReportPath
        {
            get { return _ReportPath; }
            set
            {
                _ReportPath = value;
                OnPropertyChanged("ReportPath");
            }
        }

        public String _ITADRef;
        public String ITADRef
        {
            get { return _ITADRef; }
            set
            {
                _ITADRef = value;
                OnPropertyChanged("ITADRef");
            }
        }


        public int _SerialsRemaining;
        public int SerialsRemaining
        {
            get { return _SerialsRemaining; }
            set
            {
                _SerialsRemaining = value;
                OnPropertyChanged("SerialsRemaining");
            }
        }


        public String _Complete;
        public String Complete
        {
            get { return _Complete; }
            set
            {
                _Complete = value;
                OnPropertyChanged("Complete");
            }
        }

        //public int _VariousCount;
        //public int VariousCount
        //{
        //    get { return _VariousCount; }
        //    set
        //    {
        //        _VariousCount = value;
        //        OnPropertyChanged("VariousCount");
        //    }
        //}

        public int _VariousIndex;
        public int VariousIndex
        {
            get { return _VariousIndex; }
            set
            {
                _VariousIndex = value;
                OnPropertyChanged("VariousIndex");
            }
        }


        public ObservableCollection<NetsuiteInventory> _VariousCollection;
        public ObservableCollection<NetsuiteInventory> VariousCollection
        {
            get { return _VariousCollection; }
            set
            {
                _VariousCollection = value;
                OnPropertyChanged("VariousCollection");
            }
        }


        public ObservableCollection<string> _SerialNumbers;
        public ObservableCollection<string> SerialNumbers
        {
            get { return _SerialNumbers; }
            set
            {
                _SerialNumbers = value;
                OnPropertyChanged("SerialNumbers");
            }
        }

        public IEnumerator GetEnumerator()
        {

            foreach(object o in VariousCollection)
            {
                if(o == null)
                {
                    break;
                }
                yield return o;
            }


            //throw new NotImplementedException();
        }
    }




}
