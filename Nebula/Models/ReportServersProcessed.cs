﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Models
{
    [Serializable]
    public class ReportServersProcessed
    {
       
        public string Date { get; set; }
        public string ProcessedBy { get; set; }
        public string POSONo { get; set; }
        public string ChassisSerialNo { get; set; }

        public string TimeCompleted { get; set; }

        public string Department { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }

        public ReportServersProcessed(string processedby,string posono, string chassisserial, string department, string status, string description)
        {
            ProcessedBy = processedby;
            POSONo = posono;
            ChassisSerialNo = chassisserial;
            Department = department;
            Status = status;
            Description = description;
            
        }


    }
}
