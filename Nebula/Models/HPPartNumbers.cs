﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nebula.Helpers;

namespace Nebula.Models
{
    public class HPPartNumbers : HasPropertyChanged
    {




        public string _SparePartNumber;
        public string SparePartNumber
        {
            get { return _SparePartNumber; }
            set
            {
                _SparePartNumber = value;
                OnPropertyChanged("SparePartNumber");
            }
        }

        public string _OptionPartNumber;
        public string OptionPartNumber
        {
            get { return _OptionPartNumber; }
            set
            {
                _OptionPartNumber = value;
                OnPropertyChanged("OptionPartNumber");
            }
        }

        public int _Obsolete;
        public int Obsolete
        {
            get { return _Obsolete; }
            set
            {
                _Obsolete = value;
                OnPropertyChanged("Obsolete");
            }
        }


        private HPPartNumbers _HPPartSelectedItem;
        public HPPartNumbers HPPartSelectedItem
        {
            get { return _HPPartSelectedItem; }
            set
            {
                _HPPartSelectedItem = value;
                OnPropertyChanged("HPPartSelectedItem");
            }
        }



     }
    }