﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Models
{
    public class TestOptionsSelected
    {

        //Class to hold boolean values of checkboxes for reports loaded through the serial cluster view
        public TestOptionsSelected()
        {
        }

        public TestOptionsSelected(bool hardreset, bool greenlight, bool faulty)
        {
            HardReset = hardreset;
            GreenLight = greenlight;
            Faulty = faulty;
        }

        public bool HardReset { get; set; }
        public bool GreenLight { get; set; }
        public bool Faulty { get; set; }

               
    }
}
