﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using HtmlAgilityPack;
using Nebula.Commands;
using Nebula.DellServers;
using Nebula.Helpers;
using Nebula.LenovoServers;
using Nebula.Models;
using Nebula.ViewModels;
using Newtonsoft.Json.Linq;
using PuppeteerSharp;

namespace Nebula.Models
{
    public class RedfishRestful
    {
        //REDFISH 
        public RedfishServerCollection RedfishServer = new RedfishServerCollection();
        public RedfishServerCollection EntireServer = new RedfishServerCollection();
        public RedfishArea RedfishZone = null;

        //Root collections
        List<string> RootLinks = new List<string>();
        List<string> ResourceLinks = new List<string>();
        List<string> CollectionLinks = new List<string>();

        //Hold flattened json
        public List<RedfishKeyValuePair> RFoutput = new List<RedfishKeyValuePair>();
        //public List<RedfishKeyValue> RFoutput2 = new List<RedfishKeyValue>();


        public List<RedfishKeyValuePair> TotalJsonoutput = new List<RedfishKeyValuePair>();
        public string jsonReturned = "";
        public int URILayerDepth = 10;

        public string SystemArea = "";


        //Create new list to hold non repeating odata links
        List<string> LiveLinksList = new List<string>();
        int odataCount = 0;
        int uriCount = 0;




        public RedfishRestful()
        {
            //Redfish which is now available on Dell servers in the form of the 2.30.30.30 firmware update for
            //Create main collection container
            RedfishServer.RedfishSection = new System.Collections.ObjectModel.ObservableCollection<RedfishArea>();
            //Create main collection container
            EntireServer.RedfishSection = new System.Collections.ObjectModel.ObservableCollection<RedfishArea>();
        }

        public async Task<string> RedfishRestCallSession(string uri, HttpMethod httpType, string xauthtoken, MainViewModel passedvm)
        {
            try
            {
                //Discovered ODATA Links
                //add to unique links list
                LiveLinksList.Add(uri);

                //check link is not already present
                if (!LiveLinksList.Contains(uri))
                {


                }
                else
                {

                    //Add to count
                    //odataCount += 1;
                    //OdataCount.Text = RootLinkLB.Items.Count.ToString();



                    //Unique URI
                    //Add fllowed links
                    //URILinks.Items.Add(uri);
                    //Add to count
                    uriCount += 1;
                    //URICount.Text = uriCount.ToString();


                    jsonReturned = "";


                    //To ignore SSL Server errors on api call
                    HttpClientHandler clientHandler = new HttpClientHandler();
                    clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                    // Pass the handler to httpclient(from you are calling api)
                    var client = new HttpClient(clientHandler);
                    //Header type required
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Add Basic Authorization
                    //var byteArray = Encoding.ASCII.GetBytes("admin:password");
                    //var byteArray = Encoding.ASCII.GetBytes(login);
                    //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                    //To use X-Auth-Token just add as default request header
                    //client.DefaultRequestHeaders.Add("X-Auth-Token", "8d8014382859b2121690488f571bc14b");
                    client.DefaultRequestHeaders.Add("X-Auth-Token", xauthtoken);



                    //Add Request items
                    var request = new HttpRequestMessage();
                    request.Method = httpType;
                    request.RequestUri = new Uri(uri);


                    //if(httpType == httpType.)
                    //request.Content = new StringContent("", Encoding.UTF8, "application/json");//CONTENT-TYPE header
                    //request.Content = new StringContent("", Encoding.UTF8, "text/json");//CONTENT-TYPE header

                    //Get request
                    using (var response = await client.SendAsync(request))
                    {
                        //Check response is valid
                        if (response.EnsureSuccessStatusCode().IsSuccessStatusCode)
                        {
                            //Get string json response
                            var sResponse = await response.Content.ReadAsStringAsync();
                            // LOCATION 
                            //response.Headers.Location
                            //XAUTH
                            //response.Headers.WwwAuthenticate

                            //Get raw json and add as an array
                            CollectionLinks.Add(sResponse + ",");
                            //MessageBox.Show(sResponse);
                            //jsonReturned += sResponse;
                            jsonReturned = sResponse;


                            //await ReadRedfishRoot(sResponse, "");
                            return sResponse;
                        }
                    }



                }

            }
            catch (Exception ex)
            {
                //Write to script window
                passedvm.ILORestOutput1 += "\n" + ex.Message + " " + uri + "\n\n";
                //MessageBox.Show(ex.Message + " " + uri);

                return "";
            }

            return "";
        }


        //Use to post an action
        public async Task<string> PostAction(string xauthtoken)
        {
            //client.DefaultRequestHeaders.Add("X-Auth-Token", xauthtoken);

            //var json = new FormUrlEncodedContent(new[]
            //{
            //    new KeyValuePair<string, string>("ResetType", "ForceOff")
            //});
            return "";
        }


        public async Task<DellXAuthKey> DellGetSessionID(string BaseAddress, string Targeturi, HttpMethod httpType, string login, MainViewModel passedvm)
        {
            //To ignore SSL Server errors on api call
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            using (var client = new HttpClient(clientHandler))
            {
                //var byteArray = Encoding.ASCII.GetBytes("admin:password");
                var byteArray = Encoding.ASCII.GetBytes("root:calvin");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                //Header type required
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(BaseAddress);
                //client.BaseAddress = new Uri("https://192.168.101.198");
                //client.DefaultRequestHeaders.Add("X-Auth-Token", xauthtoken);
                //client.DefaultRequestHeaders.Add("X-Auth-Token", xauthtoken);

                //var json = new FormUrlEncodedContent(new[]
                //{
                //    new KeyValuePair<string, string>("ResetType", "ForceOff")
                //});


                // WORKS POWER ACTION
                //var content = new StringContent("{\"ResetType\": \"ForceOff\"}", Encoding.UTF8, "application/json");

                var content = new StringContent("{\"UserName\": \"root\", \"Password\": \"calvin\"}", Encoding.UTF8, "application/json");

                // var content = new System.Net.Http.StringContent(json.ToString(), Encoding.UTF8, "application/json");

                var result = await client.PostAsync(Targeturi, content);
                //var result = await client.PostAsync("/redfish/v1/SessionService/Sessions", content);
                //var result = await client.PostAsync("/redfish/v1/Chassis/System.Embedded.1/Actions/Chassis.Reset", content);
                string resultContent = await result.Content.ReadAsStringAsync();

                var headers = result.Headers.Concat(result.Content.Headers);


                //Create new XAuth object
                DellXAuthKey XauthDetails = new DellXAuthKey();

                //Cycle through the headers and return a DellXAuth object
                foreach (var header in headers)
                {
                    switch (header.Key)
                    {
                        case "X-Auth-Token":
                            //Assign xauthcode
                            XauthDetails.XAuthKey = header.Value.FirstOrDefault();
                            break;
                        case "Location":
                            //Assign location uri, used to DELETE session after all calls made
                            XauthDetails.Location = header.Value.FirstOrDefault();
                            break;
                    }
                    Console.WriteLine(header.Key + " : " + header.Value.FirstOrDefault());
                }

                //Console.WriteLine(resultContent);

                return XauthDetails;
            }
        }


        //Get Dell x-auth-token
        //public
        //var content = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
        //var result = client.PostAsync(url, content).Result;
        public async Task<string> DellRedfishRestCall(string uri, HttpMethod httpType, DellXAuthKey xauthtoken, MainViewModel passedvm)
        {
            try
            {
                //Discovered ODATA Links
                //add to unique links list
                LiveLinksList.Add(uri);

                //check link is not already present
                if (!LiveLinksList.Contains(uri))
                {


                }
                else
                {

                    //Add to count
                    uriCount += 1;
                    //Set 
                    jsonReturned = "";



                    //To ignore SSL Server errors on api call
                    HttpClientHandler clientHandler = new HttpClientHandler();
                    clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                    // Pass the handler to httpclient(from you are calling api)
                    var client = new HttpClient(clientHandler);
                    //Header type required
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Add Basic Authorization
                    //var byteArray = Encoding.ASCII.GetBytes("admin:password");
                    //var byteArray = Encoding.ASCII.GetBytes(login);
                    //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    client.DefaultRequestHeaders.Add("X-Auth-Token", xauthtoken.XAuthKey);



                    Uri uri1 = new Uri(uri, UriKind.RelativeOrAbsolute);


                    //Add Request items
                    var request = new HttpRequestMessage();
                    request.Method = httpType;
                    request.RequestUri = uri1;





                    //Choose HTTP type
                    switch (httpType.Method)
                    {
                        case "GET":



                            //MessageBox.Show("GET");
                            //Get request
                            using (var response = await client.SendAsync(request))
                            {
                                //Check response is valid
                                if (response.EnsureSuccessStatusCode().IsSuccessStatusCode)
                                {
                                    //var content = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
                                    //var result = client.PostAsync(url, content).Result;

                                    //Get string json response
                                    var sResponse = await response.Content.ReadAsStringAsync();
                                    // LOCATION 
                                    //if (response.Headers.Location != null)
                                    //    MessageBox.Show(response.Headers.Location.AbsoluteUri);
                                    ////XAUTH
                                    //if (response.Headers.Location != null)
                                    //    MessageBox.Show(response.Headers.WwwAuthenticate.ToString());


                                    //Get raw json and add as an array
                                    CollectionLinks.Add(sResponse + ",");
                                    //MessageBox.Show(sResponse);
                                    //jsonReturned += sResponse;
                                    jsonReturned = sResponse;


                                    //await ReadRedfishRoot(sResponse, "");
                                    return sResponse;
                                }
                            }
                            break;
                        case "POST":
                            //MessageBox.Show("POST");
                            //Create content as Json string Content  "ImageURI": "//pinnacle.local/tech_resources/Server_Updates/DELL/PER (Rack)/PER 20 (12th Gen)/PER 720/BIOS/VIA APP/PER720-BIOS-2.9.0.EXE"
                            var content = new StringContent("{\"UserName\": \"root\", \"Password\": \"calvin\"}", Encoding.UTF8, "application/json");
                            //var content = new StringContent("{\"ImageURI\": \"//pinnacle.local/tech_resources/Server_Updates/DELL/PER (Rack)/PER 20 (12th Gen)/PER 720/BIOS/VIA APP/PER720-BIOS-2.9.0.EXE\",\"TransferProtocol\": \"HTTP\"}", Encoding.UTF8, "application/json");

                            //client.BaseAddress = new Uri("https://192.168.101.198");
                            //MessageBox.Show(content.Headers.ToString());
                            //Get request client.SendAsync(request)

                            using (var response = await client.PostAsync(uri1, content))
                            {


                                //Check response is valid
                                if (response.EnsureSuccessStatusCode().IsSuccessStatusCode)
                                {
                                    //var content = new StringContent("{\"UserName\": \"root\",\"Password\": \"calvin\"}", Encoding.UTF8, "application/json");
                                    //var result = client.PostAsync(uri, content).Result;

                                    //Get string json response
                                    var sResponse = await response.Content.ReadAsStringAsync();
                                    //// LOCATION 
                                    //if (response.Headers.Location != null)
                                    //    MessageBox.Show(response.Headers.Location.AbsoluteUri);
                                    ////XAUTH
                                    //if (response.Headers.Location != null)
                                    //    MessageBox.Show(response.Headers.WwwAuthenticate.ToString());


                                    //Get raw json and add as an array
                                    CollectionLinks.Add(sResponse + ",");
                                    //MessageBox.Show(sResponse);
                                    //jsonReturned += sResponse;
                                    jsonReturned = sResponse;


                                    //await ReadRedfishRoot(sResponse, "");
                                    return sResponse;
                                }
                            }
                            break;
                        case "PATCH":
                            break;
                        case "DELETE":
                            using (var response = await client.DeleteAsync(uri1))
                            {


                                //Check response is valid
                                if (response.EnsureSuccessStatusCode().IsSuccessStatusCode)
                                {
                                    var sResponse = await response.Content.ReadAsStringAsync();
                                    jsonReturned = sResponse;

                                    return sResponse;
                                }

                            }
                            break;
                    }

                    Console.WriteLine(httpType.Method);

                    //if(httpType.Method == httpType.Method)
                    //request.Content = new StringContent("", Encoding.UTF8, "application/json");//CONTENT-TYPE header
                    //request.Content = new StringContent("", Encoding.UTF8, "text/json");//CONTENT-TYPE header





                }

            }
            catch (Exception ex)
            {


                //Write to script window
                passedvm.ILORestOutput1 += "\n" + ex.Message + " " + uri + "\n\n";
                //MessageBox.Show(ex.Message + " " + uri);

                return "";
            }

            return "";
        }

        //Create HTTP Request for Actions or Patching as not included natively in httpclient
        public async Task RedfishPatchCall(string uri, HttpMethod httpType, string login, MainViewModel passedvm, string jsonPayload)
        {
            try
            {
                //Add patch as HttpMethod as not included natively in .net 4.72
                var method = new HttpMethod("PATCH");

                //To ignore SSL Server errors on api call
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
                //clientHandler.AutomaticDecompression = DecompressionMethods.Deflate;

                // Pass the handler to httpclient(from you are calling api)
                var client = new HttpClient(clientHandler);
                //Header type required
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Add Basic Authorization
                //var byteArray = Encoding.ASCII.GetBytes("admin:password");
                var byteArray = Encoding.ASCII.GetBytes(login);
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                //client.DefaultRequestHeaders.AcceptEncoding.Add(new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));
                //Send URI link
                Uri uri1 = new Uri(uri, UriKind.RelativeOrAbsolute);

                //Add Request items
                var request = new HttpRequestMessage();
                //HTTP Method to use Patch
                request.Method = method;
                //Uri to send to
                request.RequestUri = uri1;
                //Json to include as content
                request.Content = new StringContent(jsonPayload, Encoding.UTF8, "application/json");

                //Send and recieve the response
                var response = await client.SendAsync(request);

                //Output to script window
                passedvm.ILORestOutput1 += "\n" + response + "\n";


            }
            catch (Exception ex)
            {

                //Write to script window
                passedvm.ILORestOutput1 += "\n" + ex.Message + " " + uri + "\n\n";
                //MessageBox.Show(ex.Message + " " + uri);

            }
        }

        public async Task<string> RedfishActionCall(string uri, HttpMethod httpType, string login, MainViewModel passedvm, string jsonPayload)
        {
            try
            {

                //CALL EXAMPLE
                //RedfishRestful redFish = new RedfishRestful();
                //await redFish.RedfishActionCall("https://" + vm.Server1IPv4 + "/redfish/v1/UpdateService/Actions/UpdateService.SimpleUpdate", HttpMethod.Post, "root:calvin", vm, "{\"ImageURI\": \"http://10.3.5.10:9555/api/firmware\",\"TransferProtocol\": \"HTTP\"}");

                //Add to count
                uriCount += 1;
                //Set 
                jsonReturned = "";



                //To ignore SSL Server errors on api call
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
                //clientHandler.AutomaticDecompression = DecompressionMethods.Deflate;

                // Pass the handler to httpclient(from you are calling api)
                var client = new HttpClient(clientHandler);
                //Header type required
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Add Basic Authorization
                //var byteArray = Encoding.ASCII.GetBytes("admin:password");
                var byteArray = Encoding.ASCII.GetBytes(login);
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                //client.DefaultRequestHeaders.AcceptEncoding.Add(new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));
                //Send URI link
                Uri uri1 = new Uri(uri, UriKind.RelativeOrAbsolute);


                //Add Request items
                var request = new HttpRequestMessage();
                request.Method = httpType;
                request.RequestUri = uri1;



                //Choose HTTP type
                switch (httpType.Method)
                {
                    case "GET":

                        //Get request
                        using (var response = await client.SendAsync(request))
                        {
                            //Check response is valid
                            if (response.EnsureSuccessStatusCode().IsSuccessStatusCode)
                            {
                                //Get string json response
                                var sResponse = await response.Content.ReadAsStringAsync();

                                //Console.WriteLine(sResponse);

                                //Get raw json and add as an array
                                CollectionLinks.Add(sResponse + ",");
                                //MessageBox.Show(sResponse);
                                //jsonReturned += sResponse;
                                jsonReturned = sResponse;


                                //await ReadRedfishRoot(sResponse, "");
                                return sResponse;
                            }
                        }
                        break;
                    case "POST":

                        var postcontent = new StringContent(jsonPayload, Encoding.UTF8, "application/json");

                        using (var response = await client.PostAsync(uri1, postcontent))
                        {


                            //Check response is valid
                            if (response.EnsureSuccessStatusCode().IsSuccessStatusCode)
                            {

                                //Get string json response
                                var sResponse = await response.Content.ReadAsStringAsync();


                                //Get raw json and add as an array
                                CollectionLinks.Add(sResponse + ",");
                                //MessageBox.Show(sResponse);
                                //jsonReturned += sResponse;
                                jsonReturned = sResponse;

                                passedvm.ILORestOutput1 += "\n" + sResponse + "";

                                //await ReadRedfishRoot(sResponse, "");
                                return sResponse;
                            }
                        }
                        break;
                    case "PATCH":

                        break;
                    case "DELETE":
                        break;
                }

                Console.WriteLine(httpType.Method);



            }
            catch (Exception ex)
            {


                //Write to script window
                passedvm.ILORestOutput1 += "\n" + ex.Message + " " + uri + "\n\n";
                //MessageBox.Show(ex.Message + " " + uri);

                return "";
            }

            return "";
        }








        //Create HTTP Request , string jsonPayload
        public async Task<string> RedfishRestCall(string uri, HttpMethod httpType, string login, MainViewModel passedvm)
        {
            try
            {
                //Discovered ODATA Links
                //add to unique links list
                LiveLinksList.Add(uri);

                //check link is not already present
                if (!LiveLinksList.Contains(uri))
                {


                }
                else
                {

                    //Add to count
                    uriCount += 1;
                    //Set 
                    jsonReturned = "";



                    //To ignore SSL Server errors on api call
                    HttpClientHandler clientHandler = new HttpClientHandler();
                    clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
                    //clientHandler.AutomaticDecompression = DecompressionMethods.Deflate;

                    // Pass the handler to httpclient(from you are calling api)
                    var client = new HttpClient(clientHandler);
                    //Header type required
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Add Basic Authorization
                    //var byteArray = Encoding.ASCII.GetBytes("admin:password");
                    var byteArray = Encoding.ASCII.GetBytes(login);
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    //client.DefaultRequestHeaders.AcceptEncoding.Add(new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));
                    //Send URI link
                    Uri uri1 = new Uri(uri, UriKind.RelativeOrAbsolute);


                    //Add Request items
                    var request = new HttpRequestMessage();
                    request.Method = httpType;
                    request.RequestUri = uri1;



                    //Choose HTTP type
                    switch (httpType.Method)
                    {
                        case "GET":

                            //Get request
                            using (var response = await client.SendAsync(request))
                            {
                                //Check response is valid
                                if (response.EnsureSuccessStatusCode().IsSuccessStatusCode)
                                {
                                    //Get string json response
                                    var sResponse = await response.Content.ReadAsStringAsync();

                                    //Console.WriteLine(sResponse);

                                    //Get raw json and add as an array
                                    CollectionLinks.Add(sResponse + ",");
                                    //MessageBox.Show(sResponse);
                                    //jsonReturned += sResponse;
                                    jsonReturned = sResponse;


                                    //await ReadRedfishRoot(sResponse, "");
                                    return sResponse;
                                }
                            }
                            break;
                        case "POST":
                            //MessageBox.Show("POST");
                            //Create content as Json string Content  "ImageURI": "//pinnacle.local/tech_resources/Server_Updates/DELL/PER (Rack)/PER 20 (12th Gen)/PER 720/BIOS/VIA APP/PER720-BIOS-2.9.0.EXE"
                            // var content = new StringContent("{\"UserName\": \"root\", \"Password\": \"calvin\"}", Encoding.UTF8, "application/json");
                            //var content = new StringContent("{\"ImageURI\": \"//pinnacle.local/tech_resources/Server_Updates/DELL/PER (Rack)/PER 20 (12th Gen)/PER 720/BIOS/VIA APP/PER720-BIOS-2.9.0.EXE\",\"TransferProtocol\": \"HTTP\"}", Encoding.UTF8, "application/json");
                            var content = new StringContent("{\"ImageURI\": \"//pinnacle.local/tech_resources/Server_Updates/DELL/PER (Rack)/PER 20 (12th Gen)/PER 720/BIOS/VIA APP/PER720-BIOS-2.9.0.EXE\",\"TransferProtocol\": \"HTTP\"}", Encoding.UTF8, "application/json");


                            //client.BaseAddress = new Uri("https://192.168.101.198");
                            //MessageBox.Show(content.Headers.ToString());
                            //Get request client.SendAsync(request)

                            using (var response = await client.PostAsync(uri1, content))
                            {


                                //Check response is valid
                                if (response.EnsureSuccessStatusCode().IsSuccessStatusCode)
                                {

                                    //Get string json response
                                    var sResponse = await response.Content.ReadAsStringAsync();


                                    //Get raw json and add as an array
                                    CollectionLinks.Add(sResponse + ",");
                                    //MessageBox.Show(sResponse);
                                    //jsonReturned += sResponse;
                                    jsonReturned = sResponse;


                                    //await ReadRedfishRoot(sResponse, "");
                                    return sResponse;
                                }
                            }
                            break;
                        case "PATCH":
                            break;
                        case "DELETE":
                            break;
                    }

                    Console.WriteLine(httpType.Method);


                }

            }
            catch (Exception ex)
            {


                //Write to script window
                passedvm.ILORestOutput1 += "\n" + ex.Message + " " + uri + "\n\n";
                //MessageBox.Show(ex.Message + " " + uri);

                return "";
            }

            return "";
        }



        public async Task ReadRedfishRoot(string myJsonString, string areaRef, string vendor)
        {

            try
            {


                // Clear
                RFoutput.Clear();

                //Set Lenovo flag to false before each call
                lenovoRunOnce = false;


                //Create JSON Object to pass full string
                JObject data = JObject.Parse(myJsonString);
                //Flatten the JSON, adds to RFoutput collection
                flattenJSON(data, areaRef, vendor);




                //Use Crawler to follow linksRead JSON Payload
                foreach (var itm in RFoutput)
                {
                    //Get Root Links itm.Key.Contains("Managers")
                    switch (itm.Key)
                    {

                        case var s when itm.Key.Contains(".@odata.id"):

                            if (itm.Key.Contains("Assembly") || itm.Key.Contains("BootOptions") || itm.Key.Contains("smartstorageconfig") || itm.Key.Contains("NetworkDeviceFunctions") || itm.Key.Contains("NetworkInterfaces") || itm.Key.Contains("TelemetryService") || itm.Key.Contains("RedundancySet") || itm.Key.Contains("FastPowerMeter") || itm.Key.Contains("PowerMeter") || itm.Key.Contains("EventService") || itm.Key.Contains("JSONSchemas") || itm.Key.Contains("TaskService") || itm.Key.Contains("LogServices") || itm.Key.Contains("PCIe") || itm.Key.Contains("Assembly") || itm.Key.Contains("AccountService") || itm.Key.Contains("EventService") || itm.Key.Contains("Schemas") || itm.Key.Contains("Registries") || itm.Key.Contains("Sessions") || itm.Key.Contains("SessionService") || itm.Key.Contains("Fabrics") || itm.Key.Contains("Task") || itm.Key.Contains("UpdateService") || itm.Key.Contains("self"))
                            {
                                //itm.Key.Contains("Links") || || itm.Key.Contains("Link") 
                            }
                            else
                            {
                                if (itm.Key.Count() == 10)
                                {
                                    //repeating header just skip
                                }
                                else
                                {
                                    //MessageBox.Show(itm.Value);
                                    //TotalJsonoutput.Add(itm);


                                    if (RootLinks.Contains(itm.Value))
                                    {

                                    }
                                    else
                                    {

                                        RootLinks.Add(itm.Value);


                                    }

                                }

                            }

                            break;
                        case var s when itm.Key.Contains(".href"): ///redfish/v1/Chassis/1/Power/

                            if (itm.Key.Contains("RedundancySet") || itm.Key.Contains("FastPowerMeter") || itm.Key.Contains("TelemetryService") || itm.Key.Contains("PowerMeter") || itm.Key.Contains("LogServices") || itm.Key.Contains("PCIe") || itm.Key.Contains("Assembly") || itm.Key.Contains("AccountService") || itm.Key.Contains("EventService") || itm.Key.Contains("Schemas") || itm.Key.Contains("Registries") || itm.Key.Contains("Sessions") || itm.Key.Contains("SessionService") || itm.Key.Contains("Fabrics") || itm.Key.Contains("Task") || itm.Key.Contains("UpdateService") || itm.Key.Contains("self"))
                            {

                            }
                            else
                            {

                                if (itm.Key.Contains("Link") || itm.Key.Contains("Links") || itm.Key.Contains("TelemetryService") || itm.Key.Contains("Assembly") || itm.Key.Contains("EventService") || itm.Key.Contains("JSONSchemas") || itm.Key.Contains("TaskService") || itm.Key.Contains("Oem") && itm.Key.Contains("Memory") || itm.Key.Contains("SmartStorage") || itm.Key.Contains("Drives") || itm.Key.Contains("ArrayControllers") || itm.Key.Contains("PhysicalDrives") || itm.Key.Contains("StorageEnclosures") || itm.Key.Contains("UnconfiguredDrives"))
                                {

                                    //TotalJsonoutput.Add(itm);


                                    if (RootLinks.Contains(itm.Value))
                                    {

                                    }
                                    else
                                    {

                                        RootLinks.Add(itm.Value);

                                    }

                                }

                            }
                            //RedundancySet

                            break;
                        default:

                            if (itm.Key.Contains("Links") || itm.Key.Contains("Assembly") || itm.Key.Contains("TelemetryService") || itm.Key.Contains("RedundancySet") || itm.Key.Contains("FastPowerMeter") || itm.Key.Contains("PowerMeter") || itm.Key.Contains("Link") || itm.Key.Contains("EventService") || itm.Key.Contains("JSONSchemas") || itm.Key.Contains("TaskService") || itm.Key.Contains("LogServices") || itm.Key.Contains("PCIe") || itm.Key.Contains("Assembly") || itm.Key.Contains("AccountService") || itm.Key.Contains("EventService") || itm.Key.Contains("Schemas") || itm.Key.Contains("Registries") || itm.Key.Contains("Sessions") || itm.Key.Contains("SessionService") || itm.Key.Contains("Fabrics") || itm.Key.Contains("Task") || itm.Key.Contains("UpdateService") || itm.Key.Contains("self"))
                            {

                            }
                            else
                            {
                                //TotalJsonoutput.Add(itm);

                            }

                            break;
                    }


                }

                RFoutput.Clear();


                //Important clear before next call
                RedfishServer.RedfishSection.Clear();
                //EntireServer.RedfishSection.Clear();

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }




        //  Break JSON down to Key


        private IList<string> flattenJSON(JToken token, string areaRef, string vendor)
        {
            return _flattenJSON(token, new List<string>(), areaRef, vendor);
        }



        //Break JSON down to Key\Value pairs
        private IList<string> _flattenJSON(JToken token, List<string> path, string areaRef, string vendor)
        {
            var output = new List<string>();



            if (token.Type == JTokenType.Object)
            {

                // Output the object's child properties
                output.AddRange(token.Children().SelectMany(x => _flattenJSON(x, path, areaRef, vendor)));

            }
            else if (token.Type == JTokenType.Property)
            {
                var prop = token as JProperty;
                // Insert the property name into the path
                output.AddRange(_flattenJSON(prop.Value, new List<string>(path) { prop.Name }, areaRef, vendor));
            }
            else if (token.Type == JTokenType.Array)
            {
                // Output each array element
                var arrayIndex = 0;

                foreach (var child in token.Children())
                {
                    // Append the array index to the end of the last path segment - e.g. someProperty[n]
                    var newPath = new List<string>(path);
                    newPath[newPath.Count - 1] += "[" + arrayIndex++ + "]";

                    //CurrentMessage.Text = child.ToString();
                    output.AddRange(_flattenJSON(child, newPath, areaRef, vendor));
                }
            }
            else
            {



                //********* DELL SERVERS *********
                if (vendor == "Dell")
                {
                    // Join the path segments delimited with periods, followed by the literal value .Substring(token.ToString().LastIndexOf(".")) + "#".Replace(".","")
                    //Check for next lot of headers string.Join(".", path).Contains("@odata.context") || string.Join(".", path).Contains("@odata.id") || 



                    //WORKS WITH CONTEXT
                    if (string.Join(".", path) == "@odata.context")
                    {
                        ////Strip out values
                        //if (string.Join(".", path).Contains("Links") || string.Join(".", path).Contains("Assembly") || string.Join(".", path).Contains("RedundancySet") || string.Join(".", path).Contains("FastPowerMeter") || string.Join(".", path).Contains("PowerMeter") || string.Join(".", path).Contains("Link") || string.Join(".", path).Contains("EventService") || string.Join(".", path).Contains("JSONSchemas") || string.Join(".", path).Contains("TaskService") || string.Join(".", path).Contains("LogServices") || string.Join(".", path).Contains("PCIe") || string.Join(".", path).Contains("AccountService") || string.Join(".", path).Contains("EventService") || string.Join(".", path).Contains("Schemas") || string.Join(".", path).Contains("Registries") || string.Join(".", path).Contains("Sessions") || string.Join(".", path).Contains("SessionService") || string.Join(".", path).Contains("Fabrics") || string.Join(".", path).Contains("Task") || string.Join(".", path).Contains("UpdateService") || string.Join(".", path).Contains("self"))
                        //{

                        //}
                        //else
                        //{
                        //Doesn't require instance if Expand command issupported
                        //Add to main collection
                        RedfishServer.RedfishSection = new System.Collections.ObjectModel.ObservableCollection<RedfishArea>();

                        //Create a new Area Property
                        //RedfishZone = new RedfishArea(token.ToString());
                        RedfishZone = new RedfishArea(token.ToString());

                        //Create new Key Value Collection
                        RedfishZone.RedfishPairs = new System.Collections.ObjectModel.ObservableCollection<RedfishKeyValuePair>();

                        //MessageBox.Show(token.ToString());

                        //Add to zone
                        RedfishServer.RedfishSection.Add(RedfishZone);
                        //Add to entire server collection
                        EntireServer.RedfishSection.Add(RedfishZone);

                        //}
                    }



                    //Check if redfish area is created before adding a new key value pair
                    if (RedfishZone != null)
                    {
                        //Strip out values
                        if (string.Join(".", path).Contains("Links") || string.Join(".", path).Contains("Assembly") || string.Join(".", path).Contains("href") || string.Join(".", path).Contains("RedundancySet") || string.Join(".", path).Contains("FastPowerMeter") || string.Join(".", path).Contains("PowerMeter") || string.Join(".", path).Contains("Link") || string.Join(".", path).Contains("EventService") || string.Join(".", path).Contains("JSONSchemas") || string.Join(".", path).Contains("TaskService") || string.Join(".", path).Contains("LogServices") || string.Join(".", path).Contains("PCIe") || string.Join(".", path).Contains("AccountService") || string.Join(".", path).Contains("EventService") || string.Join(".", path).Contains("Schemas") || string.Join(".", path).Contains("Registries") || string.Join(".", path).Contains("Sessions") || string.Join(".", path).Contains("SessionService") || string.Join(".", path).Contains("Fabrics") || string.Join(".", path).Contains("Task") || string.Join(".", path).Contains("UpdateService") || string.Join(".", path).Contains("self"))
                        {

                        }
                        else
                        {
                            //if (RedfishZone.RedfishPairs.Any(p => p.Value == token.ToString()))
                            //{
                            //    MessageBox.Show("Yes");
                            //}
                            //else
                            //{
                            //MessageBox.Show(string.Join(".", path) + " = " + token.ToString());
                            RedfishZone.RedfishPairs.Add(new RedfishKeyValuePair(string.Join(".", path), token.ToString()));

                            // Add to normal keyvalue
                            RFoutput.Add(new RedfishKeyValuePair(string.Join(".", path), token.ToString()));
                            //}

                        }




                    }


                }



                //********* HP SERVERS *********
                if (vendor == "HP")
                {




                    // Join the path segments delimited with periods, followed by the literal value .Substring(token.ToString().LastIndexOf(".")) + "#".Replace(".","")
                    //Check for next lot of headers string.Join(".", path).Contains("@odata.context") || string.Join(".", path).Contains("@odata.id") || 
                    //WORKS WITH CONTEXT

                    if (string.Join(".", path) == "@odata.context")
                    {
                        //Add to main collection
                        RedfishServer.RedfishSection = new System.Collections.ObjectModel.ObservableCollection<RedfishArea>();

                        //Create a new Area Property
                        RedfishZone = new RedfishArea(token.ToString());

                        //Create new Key Value Collection
                        RedfishZone.RedfishPairs = new System.Collections.ObjectModel.ObservableCollection<RedfishKeyValuePair>();

                        ////Check observable collection exists
                        //if (RedfishServer.RedfishSection.Any(r => r.oDataContext == RedfishZone.oDataContext))
                        //{
                        //    MessageBox.Show("Already in Redfish Section");
                        //  // don't add if already there
                        //}
                        //else
                        //{
                        //Add to zone
                        RedfishServer.RedfishSection.Add(RedfishZone);
                        //Add zone to entire server collection
                        EntireServer.RedfishSection.Add(RedfishZone);
                        //}


                    }




                    //Check if redfish area is created before adding a new key value pair
                    if (RedfishZone != null)
                    {
                        //if (RedfishZone.RedfishPairs.Any(r => r.Key == token.ToString()))
                        //{


                        //}
                        //else
                        //{
                        //MessageBox.Show(string.Join(".", path) + " = " + token.ToString());
                        RedfishZone.RedfishPairs.Add(new RedfishKeyValuePair(string.Join(".", path), token.ToString()));
                        //}

                    }

                    //if (RFoutput.Any(r => r.Key == token.ToString()))
                    //{


                    //}
                    //else
                    //{
                    // Add to normal keyvalue
                    RFoutput.Add(new RedfishKeyValuePair(string.Join(".", path), token.ToString()));
                    //}




                }


                if (vendor == "Lenovo")
                {
                    //Lenovo headers are not consistent


                    if (lenovoRunOnce == false)
                    {
                        //Add to main collection
                        RedfishServer.RedfishSection = new System.Collections.ObjectModel.ObservableCollection<RedfishArea>();

                        //Create a new Area Property Use a fixed area ref as Lenovo data comes through mixed up.
                        RedfishZone = new RedfishArea(areaRef);

                        //Create new Key Value Collection
                        RedfishZone.RedfishPairs = new System.Collections.ObjectModel.ObservableCollection<RedfishKeyValuePair>();

                        //Add to zone
                        RedfishServer.RedfishSection.Add(RedfishZone);
                        //Add to entire server collection
                        EntireServer.RedfishSection.Add(RedfishZone);

                        lenovoRunOnce = true;
                    }



                    //Global 
                    //Check if redfish area is created before adding a new key value pair
                    if (RedfishZone != null)
                    {
                        //Strip out values
                        if (string.Join(".", path).Contains("Links") || string.Join(".", path).Contains("Assembly") || string.Join(".", path).Contains("href") || string.Join(".", path).Contains("RedundancySet") || string.Join(".", path).Contains("FastPowerMeter") || string.Join(".", path).Contains("PowerMeter") || string.Join(".", path).Contains("Link") || string.Join(".", path).Contains("EventService") || string.Join(".", path).Contains("JSONSchemas") || string.Join(".", path).Contains("TaskService") || string.Join(".", path).Contains("LogServices") || string.Join(".", path).Contains("PCIe") || string.Join(".", path).Contains("AccountService") || string.Join(".", path).Contains("EventService") || string.Join(".", path).Contains("Schemas") || string.Join(".", path).Contains("Registries") || string.Join(".", path).Contains("Sessions") || string.Join(".", path).Contains("SessionService") || string.Join(".", path).Contains("Fabrics") || string.Join(".", path).Contains("Task") || string.Join(".", path).Contains("UpdateService") || string.Join(".", path).Contains("self"))
                        {

                        }
                        else
                        {
                            //if (RedfishZone.RedfishPairs.Any(p => p.Value == token.ToString()))
                            //{
                            //    MessageBox.Show("Yes");
                            //}
                            //else
                            //{
                            //ReturnedRespTXT.Text += "\n\n### " + string.Join(".", path) + " = " + token.ToString() + " ###\n";
                            //MessageBox.Show(string.Join(".", path) + " = " + token.ToString());
                            RedfishZone.RedfishPairs.Add(new RedfishKeyValuePair(string.Join(".", path), token.ToString()));

                            //// Add to normal keyvalue
                            RFoutput.Add(new RedfishKeyValuePair(string.Join(".", path), token.ToString()));
                        }

                    }
                }



            }





            return output;
        }


        //GET DATA FROM SERVERS
        //public async Task<HPServerData> GetHPDataLINQ(string ServerIP, string PasswordOverride, MainViewModel passedvm, string ServerGen)
        //{
        //    //Write link to console window
        //    passedvm.ILORestOutput1 += "****** Starting Server Inventory, please wait... ******\n\n";

        //    passedvm.ProgressPercentage1 = 10;

        //    //Counter for looping through odata links
        //    int Counter = 0;



        //    switch (ServerGen)
        //    {
        //        case "Gen8":

        //            //INITIAL ROOT CALLS
        //            passedvm.ILORestOutput1 += "*** Checking Root Node ***\n\n";
        //            //ROOT NODE ONE VIEW SYSTEM CHECK (Write directly to the script window as the check looks for data there)
        //            passedvm.ILORestOutput1 += await RedfishRestCall("https://" + ServerIP + "/redfish/v1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);

        //            //await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //test one view warning
        //            //passedvm.ILORestOutput1 += "\n" + "One View" + "\n";

        //            ////SD CARD CHECK
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/managers/1/embeddedmedia/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");


        //            //SYSTEMS IF RUNNING THIS YOU DON'T REQUIRE MEMORY
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //Chassis
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //Managers Contains ILO Self Test info DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //SYSTEMS 
        //            //Bios DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Bios/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");


        //            //Firmware Inventory DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/FirmwareInventory/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");



        //            //CPU DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Processors/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            ////Memory DIRECT
        //            //await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Memory/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(),passedvm);


        //            //await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //Ethernet Interfaces DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/EthernetInterfaces/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            ////Network Adapters DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/NetworkAdapters/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            ////Set Layer Depth to 4 to drill down further (Mainly required for Storage) use 6 to get logical drive members
        //            URILayerDepth = 5;





        //            //Write link to console window
        //            passedvm.ILORestOutput1 += "\n\n****** Running Redfish Crawler Scripts ******\n\n";

        //            //Drill down 10 layers and follow any returned links URILayerDepth 
        //            for (int i = 0; i < URILayerDepth; i++)
        //            {


        //                foreach (var itm in RootLinks)
        //                {
        //                    //Write link to console window
        //                    passedvm.ILORestOutput1 += itm + "\n";
        //                    //Console.WriteLine(itm);
        //                    ResourceLinks.Add(itm);
        //                }

        //                RootLinks.Clear();

        //                //Set Percent done
        //                passedvm.ProgressPercentage1 = passedvm.ProgressPercentage1 + 10;

        //                ////Loop through each resource link and drill down multiple layers 
        //                foreach (var item in ResourceLinks)
        //                {
        //                    //Call odata and href links
        //                    //await RedfishRestCall(@"https://" + ServerIP + item.ToString() + "?$expand=*($levels=1)", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
        //                    await RedfishRestCall(@"https://" + ServerIP + item.ToString() + "", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
        //                    //Flatten Json objects to Key Value Pairs
        //                    await ReadRedfishRoot(jsonReturned, "Layer" + i.ToString(), "HP");
        //                }



        //                // Update Layer Count
        //                Counter += 1;
        //                //Set Message


        //                ResourceLinks.Clear();
        //            }



        //            RootLinks.Clear();

        //            break;
        //        case "Gen9":


        //            //INITIAL ROOT CALLS
        //            passedvm.ILORestOutput1 += "*** Checking Root Node ***\n\n";
        //            //ROOT NODE ONE VIEW SYSTEM CHECK (Write directly to the script window as the check looks for data there)
        //            passedvm.ILORestOutput1 += await RedfishRestCall("https://" + ServerIP + "/redfish/v1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);

        //            //await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //test one view warning
        //            //passedvm.ILORestOutput1 += "\n" + "One View" + "\n";

        //            ////SD CARD CHECK
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/managers/1/embeddedmedia/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");


        //            //SYSTEMS IF RUNNING THIS YOU DON'T REQUIRE MEMORY
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //Chassis
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //Managers Contains ILO Self Test info DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //SYSTEMS 
        //            //Bios DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Bios/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");


        //            //Firmware Inventory DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/FirmwareInventory/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");



        //            //CPU DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Processors/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            ////Memory DIRECT
        //            //await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Memory/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(),passedvm);


        //            //await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //Ethernet Interfaces DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/1/EthernetInterfaces/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            ////Network Adapters DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/NetworkAdapters/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            ////Set Layer Depth to 4 to drill down further (Mainly required for Storage) use 6 to get logical drive members
        //            URILayerDepth = 5;





        //            //Write link to console window
        //            passedvm.ILORestOutput1 += "\n\n****** Running Redfish Crawler Scripts ******\n\n";

        //            //Drill down 10 layers and follow any returned links URILayerDepth 
        //            for (int i = 0; i < URILayerDepth; i++)
        //            {


        //                foreach (var itm in RootLinks)
        //                {
        //                    //Write link to console window
        //                    passedvm.ILORestOutput1 += itm + "\n";
        //                    //Console.WriteLine(itm);
        //                    ResourceLinks.Add(itm);
        //                }

        //                RootLinks.Clear();

        //                //Set Percent done
        //                passedvm.ProgressPercentage1 = passedvm.ProgressPercentage1 + 10;

        //                ////Loop through each resource link and drill down multiple layers 
        //                foreach (var item in ResourceLinks)
        //                {
        //                    //Call odata and href links
        //                    //await RedfishRestCall(@"https://" + ServerIP + item.ToString() + "?$expand=*($levels=1)", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
        //                    await RedfishRestCall(@"https://" + ServerIP + item.ToString() + "", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
        //                    //Flatten Json objects to Key Value Pairs
        //                    await ReadRedfishRoot(jsonReturned, "Layer" + i.ToString(), "HP");
        //                }



        //                // Update Layer Count
        //                Counter += 1;
        //                //Set Message


        //                ResourceLinks.Clear();
        //            }

        //            RootLinks.Clear();

        //            break;
        //        case "Gen10":
        //            //INITIAL ROOT CALLS
        //            passedvm.ILORestOutput1 += "*** Checking Root Node ***\n\n";
        //            //ROOT NODE ONE VIEW SYSTEM CHECK (Write directly to the script window as the check looks for data there)
        //            //passedvm.ILORestOutput1 += await RedfishRestCall("https://" + ServerIP + "/redfish/v1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);

        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
        //            await ReadRedfishRoot(jsonReturned, "", "HP");
        //            passedvm.ILORestOutput1 += jsonReturned;
        //            //Clear Links as don't want it to run in loop below
        //            RootLinks.Clear();

        //            //////SD CARD CHECK
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/managers/1/embeddedmedia/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");



        //            //////Storage
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/SmartStorage/ArrayControllers/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");





        //            ////Set Layer Depth to 4 to drill down further (Mainly required for Storage) use 6 to get logical drive members
        //            URILayerDepth = 4;

        //            //Write link to console window
        //            passedvm.ILORestOutput1 += "\n\n****** Running Redfish Crawler Scripts ******\n\n";

        //            //Drill down 10 layers and follow any returned links URILayerDepth 
        //            for (int i = 0; i < URILayerDepth; i++)
        //            {


        //                foreach (var itm in RootLinks)
        //                {
        //                    //Write link to console window
        //                    passedvm.ILORestOutput1 += itm + "\n";
        //                    //Console.WriteLine(itm);
        //                    ResourceLinks.Add(itm);
        //                }

        //                RootLinks.Clear();

        //                //Set Percent done
        //                passedvm.ProgressPercentage1 = passedvm.ProgressPercentage1 + 10;

        //                ////Loop through each resource link and drill down multiple layers 
        //                foreach (var item in ResourceLinks)
        //                {
        //                    //Call odata and href links
        //                    // use ?$expand=. to expand any Members, will require a different sorting method
        //                    await RedfishRestCall(@"https://" + ServerIP + item.ToString() + "", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
        //                    //Flatten Json objects to Key Value Pairs
        //                    await ReadRedfishRoot(jsonReturned, "Layer" + i.ToString(), "HP");
        //                }



        //                // Update Layer Count
        //                Counter += 1;
        //                //Set Message


        //                ResourceLinks.Clear();
        //            }

        //            RootLinks.Clear();



        //            //SYSTEMS IF RUNNING THIS YOU DON'T REQUIRE MEMORY
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //////Firmware
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/UpdateService/FirmwareInventory/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");



        //            //////Chassis
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            ////Managers Contains ILO Self Test info DIRECT ?$expand=.
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");


        //            //////Thermal DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/Thermal/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //////Power DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/Power/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");


        //            //CPU DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Processors/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            ////Memory DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Memory/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //////Ethernet Interfaces DIRECT
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/1/EthernetInterfaces/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");

        //            //Network Adapters DIRECT 
        //            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/NetworkAdapters/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


        //            await ReadRedfishRoot(jsonReturned, "", "HP");




        //            break;
        //    }







        //    // EXTRACT JSON DATA TO INTERNAL CLASS
        //    // SEPARATION INTO A HP SERVER DATA OBJECT NEEDS TO BE HERE, WHEN ALL LINK DATA HAVE BEEN ADDED

        //    //HP DATA
        //    //Create base server class
        //    HPServerData HPServer = new HPServerData();

        //    //Declarations of new HP Object Collections
        //    HealthCPUHP cpuHealth = new HealthCPUHP();
        //    HealthMemoryHP MemoryHealth = new HealthMemoryHP();
        //    HealthFansHP FanHealth = new HealthFansHP();
        //    HealthTemperatureHP TempHealth = new HealthTemperatureHP();
        //    HealthPowerHP PowerHealth = new HealthPowerHP();
        //    HealthNetworkHP NetworkHealth = new HealthNetworkHP();
        //    //New Storage info instances
        //    HPServer.StorageInfo = new HealthStorageHP();
        //    HPServer.StorageInfo.LogicalDrive = new HealthLogicalDriveHP();
        //    //HPServer.StorageInfo.LogicalDrive.PhysicalDrives = new ObservableCollection<PhysicalDrivesHP>();
        //    DriveEnclosureInfoHP DriveEnclosureInfo = new DriveEnclosureInfoHP();
        //    HealthLogicalDriveHP LogicalDriveInfo = new HealthLogicalDriveHP();
        //    PhysicalDrivesHP PhysicalDriveInfo = new PhysicalDrivesHP();
        //    PhysicalDrivesHP PhysicalDrive = new PhysicalDrivesHP();


        //    //To hold Network Card Model
        //    string networkCard = "";
        //    int networkCount = 0;

        //    //Memory Counts
        //    int dimmCount = 0;
        //    double totalMem = 0;






        //    //OUT PUT SERVER DATA
        //    foreach (var itm in EntireServer.RedfishSection)
        //    {
        //        Console.WriteLine("\n" + itm.oDataContext + "");

        //        //USE LINQ TO QUERY COLLECTION
        //        //System 
        //        if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/$entity") || itm.oDataContext.Contains("/redfish/v1/$metadata#ComputerSystem.ComputerSystem"))
        //        {
        //            //ADD LINQ TO COLLECTION QUERY TO EXTRACT PROPERTY VALUES 
        //            //Console.WriteLine(keyvalue.Key + " = " + keyvalue.Value);
        //            HPServer.AssetTag = itm.RedfishPairs.Where(x => x.Key == "AssetTag").FirstOrDefault()?.Value;
        //            HPServer.HostName = itm.RedfishPairs.Where(x => x.Key == "HostName").FirstOrDefault()?.Value;
        //            HPServer.Manufacturer = itm.RedfishPairs.Where(x => x.Key == "Manufacturer").FirstOrDefault()?.Value;
        //            HPServer.Model = itm.RedfishPairs.Where(x => x.Key == "Model").FirstOrDefault()?.Value;
        //            HPServer.ChassisSerial = itm.RedfishPairs.Where(x => x.Key == "SerialNumber").FirstOrDefault()?.Value;
        //            HPServer.SKU = itm.RedfishPairs.Where(x => x.Key == "SKU").FirstOrDefault()?.Value;
        //            HPServer.CPUCount = itm.RedfishPairs.Where(x => x.Key == "ProcessorSummary.Count").FirstOrDefault()?.Value;
        //            HPServer.CPUFamily = itm.RedfishPairs.Where(x => x.Key == "ProcessorSummary.Model").FirstOrDefault()?.Value.Trim();
        //            HPServer.CPUHealth = itm.RedfishPairs.Where(x => x.Key == "ProcessorSummary.Status.HealthRollUp").FirstOrDefault()?.Value;
        //            HPServer.MemoryHealth = itm.RedfishPairs.Where(x => x.Key == "MemorySummary.Status.HealthRollup").FirstOrDefault()?.Value;
        //            HPServer.TotalMemory = itm.RedfishPairs.Where(x => x.Key == "MemorySummary.TotalSystemMemoryGiB").FirstOrDefault()?.Value + "GB";
        //            //Gen8, 9 poststate
        //            HPServer.PostState = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.PostState").FirstOrDefault()?.Value;
        //            //Gen10 poststate
        //            HPServer.PostState = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.PostState").FirstOrDefault()?.Value;
        //            //HEALTH AGGREGATE ROUND UP GEN 10
        //            HPServer.BiosHardwareStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.AggregateHealthStatus.BiosOrHardwareHealth.Status.Health").FirstOrDefault()?.Value;
        //            HPServer.FansRedundancy = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.AggregateHealthStatus.FanRedundancy").FirstOrDefault()?.Value;
        //            HPServer.FansStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.AggregateHealthStatus.Fans.Status.Health").FirstOrDefault()?.Value;
        //            HPServer.MemoryStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.AggregateHealthStatus.Memory.Status.Health").FirstOrDefault()?.Value;
        //            HPServer.MemoryHealth = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.AggregateHealthStatus.Memory.Status.Health").FirstOrDefault()?.Value;
        //            HPServer.ProcessorStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.AggregateHealthStatus.Processors.Status.Health").FirstOrDefault()?.Value;
        //            HPServer.CPUHealth = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.AggregateHealthStatus.Processors.Status.Health").FirstOrDefault()?.Value;
        //            HPServer.PowerSuppliesStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.AggregateHealthStatus.PowerSupplies.Status.Health").FirstOrDefault()?.Value;
        //            HPServer.PowerSupplyRedundancy = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.AggregateHealthStatus.PowerSupplyRedundancy").FirstOrDefault()?.Value;
        //            HPServer.StorageStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.AggregateHealthStatus.Storage.Status.Health").FirstOrDefault()?.Value;
        //            HPServer.TemperatureStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.AggregateHealthStatus.Temperatures.Status.Health").FirstOrDefault()?.Value;
        //            //HPServer.NetworkStatus = "OK";
        //            HPServer.BiosCurrentVersion = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.Bios.Current.VersionString").FirstOrDefault()?.Value;
        //            HPServer.BiosBackupVersion = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.Bios.Backup.VersionString").FirstOrDefault()?.Value;
        //            HPServer.IntelligentProvisioningVersion = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.IntelligentProvisioningVersion").FirstOrDefault()?.Value;
        //            //TPM Gen10 Properties
        //            HPServer.TpmVisibility = itm.RedfishPairs.Where(x => x.Key == "Bios.Attributes.TpmVisibility").FirstOrDefault()?.Value;
        //            HPServer.TpmType = itm.RedfishPairs.Where(x => x.Key == "Bios.Attributes.TpmType").FirstOrDefault()?.Value;
        //            HPServer.TpmState = itm.RedfishPairs.Where(x => x.Key == "Bios.Attributes.TpmState").FirstOrDefault()?.Value;
        //            //Power State
        //            HPServer.PowerState = itm.RedfishPairs.Where(x => x.Key == "PowerState").FirstOrDefault()?.Value;

        //        }


        //        //SD CARD Property extraction
        //        if (itm.oDataContext.Contains("/redfish/v1/$metadata#Managers/Members/1/EmbeddedMedia$entity"))
        //        {
        //            //Get SD Card
        //            HPServer.SDCardInserted = itm.RedfishPairs.Where(x => x.Key == "SDCard.Status.State").FirstOrDefault()?.Value;
        //        }

        //        //Bios Properties
        //        if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/Bios$entity"))
        //        {

        //            //Get SD Card
        //            HPServer.HostName = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.HPSystemManagementHomepageAddress").FirstOrDefault()?.Value;

        //        }


        //        //**** FIRMWARE ****
        //        //Gen8 Gen9 Firmware Oem.Hpe.Bios.Backup.VersionString
        //        if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/FirmwareInventory$entity"))
        //        {
        //            HPServer.BiosCurrentVersion = itm.RedfishPairs.Where(x => x.Key == "Current.SystemRomActive[0].VersionString").FirstOrDefault()?.Value;
        //            HPServer.BiosBackupVersion = itm.RedfishPairs.Where(x => x.Key == "Current.SystemRomBackup[0].VersionString").FirstOrDefault()?.Value;
        //            HPServer.iLOVersion = itm.RedfishPairs.Where(x => x.Key == "Current.SystemBMC[0].VersionString").FirstOrDefault()?.Value;
        //            HPServer.SPSVersion = itm.RedfishPairs.Where(x => x.Key == "Current.SPSFirmwareVersionData[0].VersionString").FirstOrDefault()?.Value;
        //            HPServer.IntelligentProvisioningVersion = itm.RedfishPairs.Where(x => x.Key == "Current.IntelligentProvisioning[0].VersionString").FirstOrDefault()?.Value;

        //        }

        //        //Firmware Gen 10
        //        if (itm.oDataContext == "/redfish/v1/$metadata#SoftwareInventoryCollection.SoftwareInventoryCollection")
        //        {
        //            //ADD LINQ TO COLLECTION QUERY TO EXTRACT PROPERTY VALUES 
        //            //Capture members count to use in for loop
        //            int FirmwareCount = Convert.ToInt32(itm.RedfishPairs.Where(x => x.Key == "Members@odata.count").FirstOrDefault()?.Value);


        //            for (int i = 0; i < FirmwareCount; i++)
        //            {

        //                // Get Description
        //                string firmwareType = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Description").FirstOrDefault()?.Value;

        //                switch (firmwareType)
        //                {
        //                    case "SystemBMC":
        //                        HPServer.iLOVersion = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Version").FirstOrDefault()?.Value;
        //                        break;
        //                    case "SystemRomActive":
        //                        HPServer.BiosCurrentVersion = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Version").FirstOrDefault()?.Value;
        //                        break;
        //                    case "SystemRomBackup":
        //                        HPServer.BiosBackupVersion = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Version").FirstOrDefault()?.Value;
        //                        break;
        //                    case "InnovationEngineFirmware":
        //                        HPServer.InnovationEngine = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Version").FirstOrDefault()?.Value;
        //                        break;
        //                    case "SPSFirmwareVersionData":
        //                        HPServer.SPSVersion = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Version").FirstOrDefault()?.Value;
        //                        break;
        //                    case "Intelligent Provisioning":
        //                        HPServer.IntelligentProvisioningVersion = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Version").FirstOrDefault()?.Value;
        //                        break;
        //                }
        //            }
        //        }




        //        //ILO Self Test 
        //        //Gen8,9
        //        if (itm.oDataContext.Contains("/redfish/v1/$metadata#Managers/Members/$entity"))
        //        {
        //            //ILO SELF TEST
        //            //Gen8,9
        //            //License
        //            HPServer.ILOLicense = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.License.LicenseString").FirstOrDefault()?.Value;
        //            //NVRAM DATA
        //            HPServer.NVRAMDataNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[0].Notes").FirstOrDefault()?.Value;
        //            //NVRAM DATA
        //            HPServer.NVRAMDataStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[0].Status").FirstOrDefault()?.Value;
        //            //NVRAM SPACE
        //            HPServer.NVRAMSpaceNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[1].Notes").FirstOrDefault()?.Value;
        //            //NVRAM SPACE
        //            HPServer.NVRAMSpaceStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[1].Status").FirstOrDefault()?.Value;
        //            //SD CARD
        //            HPServer.EmbeddedFlashSDCardNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[2].Notes").FirstOrDefault()?.Value;
        //            //SD CARD
        //            HPServer.EmbeddedFlashSDCardStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[2].Status").FirstOrDefault()?.Value;
        //            //EPROM
        //            HPServer.EEPROMNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[3].Notes").FirstOrDefault()?.Value;
        //            //EPROM
        //            HPServer.EEPROMStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[3].Status").FirstOrDefault()?.Value;
        //            //HOST ROM
        //            HPServer.HostRomNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[4].Notes").FirstOrDefault()?.Value;
        //            //HOST ROM
        //            HPServer.HostRomStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[4].Status").FirstOrDefault()?.Value;
        //            //SUPPORTED HOST
        //            HPServer.SupportedHostNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[5].Notes").FirstOrDefault()?.Value;
        //            //SUPPORTED HOST
        //            HPServer.SupportedHostStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[5].Status").FirstOrDefault()?.Value;
        //            //POWER MANAGEMENT CONTROLLER
        //            HPServer.PowerManagementControllerNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[6].Notes").FirstOrDefault()?.Value;
        //            //POWER MANAGEMENT CONTROLLER
        //            HPServer.PowerManagementControllerStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[6].Status").FirstOrDefault()?.Value;
        //            //CPLDPAL0
        //            HPServer.CPLDPAL0Notes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[7].Notes").FirstOrDefault()?.Value;
        //            //CPLDPAL0
        //            HPServer.CPLDPAL0Status = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[7].Status").FirstOrDefault()?.Value;
        //            //CPLDPAL1
        //            HPServer.CPLDPAL1Notes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[8].Notes").FirstOrDefault()?.Value;
        //            //CPLDPAL1
        //            HPServer.CPLDPAL1Status = itm.RedfishPairs.Where(x => x.Key == "Oem.Hp.iLOSelfTestResults[8].Status").FirstOrDefault()?.Value;

        //        }

        //        //Gen 10
        //        if (itm.oDataContext.Contains("/redfish/v1/$metadata#Manager.Manager"))
        //        {

        //            //ILO SELF TEST
        //            //Gen10
        //            //License
        //            HPServer.ILOLicense = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.License.LicenseString").FirstOrDefault()?.Value;
        //            //NVRAM DATA
        //            HPServer.NVRAMDataNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[0].Notes").FirstOrDefault()?.Value;
        //            //NVRAM DATA
        //            HPServer.NVRAMDataStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[0].Status").FirstOrDefault()?.Value;
        //            //NVRAM SPACE
        //            HPServer.NVRAMSpaceNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[1].Notes").FirstOrDefault()?.Value;
        //            //NVRAM SPACE
        //            HPServer.NVRAMSpaceStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[1].Status").FirstOrDefault()?.Value;
        //            //SD CARD
        //            HPServer.EmbeddedFlashSDCardNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[2].Notes").FirstOrDefault()?.Value;
        //            //SD CARD
        //            HPServer.EmbeddedFlashSDCardStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[2].Status").FirstOrDefault()?.Value;
        //            //EPROM
        //            HPServer.EEPROMNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[3].Notes").FirstOrDefault()?.Value;
        //            //EPROM
        //            HPServer.EEPROMStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[3].Status").FirstOrDefault()?.Value;
        //            //HOST ROM
        //            HPServer.HostRomNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[4].Notes").FirstOrDefault()?.Value;
        //            //HOST ROM
        //            HPServer.HostRomStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[4].Status").FirstOrDefault()?.Value;
        //            //SUPPORTED HOST
        //            HPServer.SupportedHostNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[5].Notes").FirstOrDefault()?.Value;
        //            //SUPPORTED HOST
        //            HPServer.SupportedHostStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[5].Status").FirstOrDefault()?.Value;
        //            //POWER MANAGEMENT CONTROLLER
        //            HPServer.PowerManagementControllerNotes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[6].Notes").FirstOrDefault()?.Value;
        //            //POWER MANAGEMENT CONTROLLER
        //            HPServer.PowerManagementControllerStatus = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[6].Status").FirstOrDefault()?.Value;
        //            //CPLDPAL0
        //            HPServer.CPLDPAL0Notes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[7].Notes").FirstOrDefault()?.Value;
        //            //CPLDPAL0
        //            HPServer.CPLDPAL0Status = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[7].Status").FirstOrDefault()?.Value;
        //            //CPLDPAL1
        //            HPServer.CPLDPAL1Notes = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[8].Notes").FirstOrDefault()?.Value;
        //            //CPLDPAL1
        //            HPServer.CPLDPAL1Status = itm.RedfishPairs.Where(x => x.Key == "Oem.Hpe.iLOSelfTestResults[8].Status").FirstOrDefault()?.Value;


        //        }

        //        //COLLECTIONS





        //        //Loop through all key value pairs in Entire Server Object
        //        foreach (var keyvalue in itm.RedfishPairs)
        //        {
        //            //Uncomment to see Data returned in Output window
        //            Console.WriteLine(keyvalue.Key + " = " + keyvalue.Value);






        //            //CPU  
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/Processors/Members/$entity") || itm.oDataContext.Contains("/redfish/v1/$metadata#ProcessorCollection.ProcessorCollection"))
        //            {

        //                switch (keyvalue.Key)
        //                {
        //                    //GEN8 GEN9 OLD
        //                    case "Model":
        //                        //Create CPU Object
        //                        cpuHealth = new HealthCPUHP();
        //                        cpuHealth.Name = keyvalue.Value.Trim();
        //                        break;
        //                    case "Socket":
        //                        cpuHealth.Label = keyvalue.Value;
        //                        break;
        //                    case "Status.Health":
        //                        cpuHealth.Status = keyvalue.Value;

        //                        break;
        //                    case "Oem.Hp.RatedSpeedMHz":
        //                        cpuHealth.Speed = keyvalue.Value + "MHz";
        //                        break;
        //                    case "Oem.Hpe.RatedSpeedMHz":
        //                        cpuHealth.Speed = keyvalue.Value + "MHz";
        //                        break;

        //                    case "Oem.Hp.Characteristics[0]":
        //                        cpuHealth.MemoryTech = keyvalue.Value;
        //                        break;
        //                    case "Oem.Hpe.Characteristics[0]":
        //                        cpuHealth.MemoryTech = keyvalue.Value;
        //                        break;

        //                    case "Oem.Hp.Cache[0].InstalledSizeKB":
        //                        cpuHealth.L1Cache = keyvalue.Value + " KB";
        //                        //MessageBox.Show(cpuHealth.L1Cache);
        //                        break;
        //                    case "Oem.Hpe.Cache[0].InstalledSizeKB":
        //                        cpuHealth.L1Cache = keyvalue.Value + " KB";
        //                        //MessageBox.Show(cpuHealth.L1Cache);
        //                        break;
        //                    case "Oem.Hp.Cache[1].InstalledSizeKB":
        //                        cpuHealth.L2Cache = keyvalue.Value + " KB";
        //                        //MessageBox.Show(cpuHealth.L2Cache);
        //                        break;
        //                    case "Oem.Hpe.Cache[1].InstalledSizeKB":
        //                        cpuHealth.L2Cache = keyvalue.Value + " KB";
        //                        //MessageBox.Show(cpuHealth.L2Cache);
        //                        break;
        //                    case "Oem.Hp.Cache[2].InstalledSizeKB":
        //                        cpuHealth.L3Cache = keyvalue.Value + " KB";
        //                        //MessageBox.Show(cpuHealth.L3Cache);
        //                        //Add CPU Objects

        //                        break;
        //                    case "Oem.Hpe.Cache[2].InstalledSizeKB":
        //                        cpuHealth.L3Cache = keyvalue.Value + " KB";
        //                        //MessageBox.Show(cpuHealth.L3Cache);
        //                        //Add CPU Objects

        //                        break;
        //                    case "TotalCores":
        //                        cpuHealth.ExecutionTech = keyvalue.Value + " Cores; ";

        //                        break;
        //                    case "TotalThreads":
        //                        cpuHealth.ExecutionTech += keyvalue.Value + " Threads";
        //                        HPServer.CPUInfo.Add(cpuHealth);
        //                        break;

        //                }



        //                //GEN10 CPU Processor Collection
        //                if (keyvalue.Key.Contains("Members["))
        //                {

        //                    switch (keyvalue.Key)
        //                    {


        //                        case var s when keyvalue.Key.Contains("].Id"):
        //                            //Create CPU Object
        //                            cpuHealth = new HealthCPUHP();
        //                            //MemoryHealth.Socket = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Socket = " + MemoryHealth.Socket + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Model"):

        //                            cpuHealth.Name = keyvalue.Value.Trim();
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Socket"):
        //                            cpuHealth.Label = keyvalue.Value;
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Status.Health"):
        //                            cpuHealth.Status = keyvalue.Value;

        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hp.RatedSpeedMHz"):
        //                            cpuHealth.Speed = keyvalue.Value + "MHz";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.RatedSpeedMHz"):
        //                            cpuHealth.Speed = keyvalue.Value + "MHz";
        //                            break;

        //                        case var s when keyvalue.Key.Contains("].Oem.Hp.Characteristics[0]"):
        //                            cpuHealth.MemoryTech = keyvalue.Value;
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.Characteristics[0]"):
        //                            cpuHealth.MemoryTech = keyvalue.Value;
        //                            break;

        //                        case var s when keyvalue.Key.Contains("].Oem.Hp.Cache[0].InstalledSizeKB"):
        //                            cpuHealth.L1Cache = keyvalue.Value + " KB";
        //                            //MessageBox.Show(cpuHealth.L1Cache);
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.Cache[0].InstalledSizeKB"):
        //                            cpuHealth.L1Cache = keyvalue.Value + " KB";
        //                            //MessageBox.Show(cpuHealth.L1Cache);
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hp.Cache[1].InstalledSizeKB"):
        //                            cpuHealth.L2Cache = keyvalue.Value + " KB";
        //                            //MessageBox.Show(cpuHealth.L2Cache);
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.Cache[1].InstalledSizeKB"):
        //                            cpuHealth.L2Cache = keyvalue.Value + " KB";
        //                            //MessageBox.Show(cpuHealth.L2Cache);
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hp.Cache[2].InstalledSizeKB"):
        //                            cpuHealth.L3Cache = keyvalue.Value + " KB";
        //                            //MessageBox.Show(cpuHealth.L3Cache);
        //                            //Add CPU Objects

        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.Cache[2].InstalledSizeKB"):
        //                            cpuHealth.L3Cache = keyvalue.Value + " KB";
        //                            //MessageBox.Show(cpuHealth.L3Cache);
        //                            //Add CPU Objects

        //                            break;
        //                        case var s when keyvalue.Key.Contains("].TotalCores"):
        //                            cpuHealth.ExecutionTech = keyvalue.Value + " Cores; ";

        //                            break;
        //                        case var s when keyvalue.Key.Contains("].TotalThreads"):
        //                            cpuHealth.ExecutionTech += keyvalue.Value + " Threads";
        //                            HPServer.CPUInfo.Add(cpuHealth);
        //                            break;

        //                    }

        //                }
        //            }

        //            //****MEMORY*****
        //            //GEN 10 MEMORY
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#MemoryCollection.MemoryCollection"))
        //            {
        //                if (keyvalue.Key.Contains("Members["))
        //                {

        //                    switch (keyvalue.Key)
        //                    {


        //                        case var s when keyvalue.Key.Contains("].Id"):
        //                            MemoryHealth = new HealthMemoryHP();
        //                            MemoryHealth.Socket = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Socket = " + MemoryHealth.Socket + "\n";
        //                            break;

        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.Attributes[0]"):
        //                            MemoryHealth.HPSmartMemory = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "HPSmartMemory = " + MemoryHealth.HPSmartMemory + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].PartNumber"):
        //                            MemoryHealth.PartNo = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Part No = " + MemoryHealth.PartNo + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].MemoryDeviceType"):
        //                            MemoryHealth.MemType = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "MemType = " + MemoryHealth.MemType + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].CapacityMiB"):
        //                            MemoryHealth.Size = keyvalue.Value;
        //                            //Add total dimm count and total memory size here
        //                            dimmCount += 1;
        //                            totalMem += Convert.ToDouble(keyvalue.Value);
        //                            //ReturnedRespTXT.Text += "Size = " + MemoryHealth.Size + "\n";

        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.MaxOperatingSpeedMTs"):

        //                            MemoryHealth.Frequency = keyvalue.Value + " MHz";
        //                            //ReturnedRespTXT.Text += "Frequency = " + MemoryHealth.Frequency+ "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.MinimumVoltageVoltsX10"):

        //                            //Divide total by 10 to get true value
        //                            double minVoltage = Convert.ToDouble(keyvalue.Value) / 10.00;
        //                            MemoryHealth.MinVoltage = minVoltage.ToString() + " v";
        //                            //ReturnedRespTXT.Text += "MinVoltage = " + MemoryHealth.MinVoltage + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].RankCount"):

        //                            MemoryHealth.Ranks = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Ranks = " + MemoryHealth.Ranks + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.BaseModuleType"):

        //                            MemoryHealth.Technology = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Technology = " + MemoryHealth.Technology + "\n";

        //                            break;
        //                        case var s when keyvalue.Key.Contains("].MemoryLocation.Slot"):
        //                            MemoryHealth.Label = keyvalue.Value;

        //                            //ReturnedRespTXT.Text += "Label = " + MemoryHealth.Label + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Status.Health"):
        //                            //New memory object
        //                            MemoryHealth.Status = keyvalue.Value;
        //                            //Assign to Frontpage
        //                            HPServer.MemoryHealth = MemoryHealth.Status;

        //                            //Add Memory Object
        //                            HPServer.MemoryInfo.Add(MemoryHealth);

        //                            break;
        //                    }



        //                }

        //            }

        //            //GEN8 9 MEMORY 
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/Memory/Members/$entity"))
        //            {


        //                switch (keyvalue.Key)
        //                {
        //                    case "DIMMStatus":
        //                        //New memory object
        //                        MemoryHealth = new HealthMemoryHP();
        //                        MemoryHealth.Status = keyvalue.Value;
        //                        //ReturnedRespTXT.Text += "Status = " + MemoryHealth.Status + "\n";
        //                        break;

        //                    case "Id":
        //                        MemoryHealth.Socket = keyvalue.Value;
        //                        //ReturnedRespTXT.Text += "Socket = " + MemoryHealth.Socket + "\n";
        //                        break;

        //                    case "HPMemoryType":
        //                        MemoryHealth.HPSmartMemory = keyvalue.Value;
        //                        //ReturnedRespTXT.Text += "HPSmartMemory = " + MemoryHealth.HPSmartMemory + "\n";
        //                        break;
        //                    case "PartNumber":
        //                        MemoryHealth.PartNo = keyvalue.Value;
        //                        //ReturnedRespTXT.Text += "Part No = " + MemoryHealth.PartNo + "\n";
        //                        break;
        //                    case "DIMMType":
        //                        MemoryHealth.MemType = keyvalue.Value;
        //                        //ReturnedRespTXT.Text += "MemType = " + MemoryHealth.MemType + "\n";
        //                        break;
        //                    case "SizeMB":
        //                        MemoryHealth.Size = keyvalue.Value;
        //                        //Add total dimm count and total memory size here
        //                        dimmCount += 1;
        //                        totalMem += Convert.ToDouble(keyvalue.Value);
        //                        //ReturnedRespTXT.Text += "Size = " + MemoryHealth.Size + "\n";

        //                        break;
        //                    case "MaximumFrequencyMHz":
        //                        MemoryHealth.Frequency = keyvalue.Value + " MHz";
        //                        //ReturnedRespTXT.Text += "Frequency = " + MemoryHealth.Frequency+ "\n";
        //                        break;
        //                    case "MinimumVoltageVoltsX10":
        //                        //Divide total by 10 to get true value
        //                        double minVoltage = Convert.ToDouble(keyvalue.Value) / 10.00;
        //                        MemoryHealth.MinVoltage = minVoltage.ToString() + " v";
        //                        //ReturnedRespTXT.Text += "MinVoltage = " + MemoryHealth.MinVoltage + "\n";
        //                        break;
        //                    case "Rank":
        //                        MemoryHealth.Ranks = keyvalue.Value;
        //                        //ReturnedRespTXT.Text += "Ranks = " + MemoryHealth.Ranks + "\n";
        //                        break;
        //                    case "DIMMTechnology":
        //                        MemoryHealth.Technology = keyvalue.Value;
        //                        //ReturnedRespTXT.Text += "Technology = " + MemoryHealth.Technology + "\n";

        //                        break;
        //                    case "SocketLocator":

        //                        MemoryHealth.Label = keyvalue.Value;

        //                        //Add Memory Object
        //                        HPServer.MemoryInfo.Add(MemoryHealth);
        //                        //ReturnedRespTXT.Text += "Label = " + MemoryHealth.Label + "\n";
        //                        break;
        //                }


        //            }




        //            //******** POWER ***********
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Chassis/Members/1/Power$entity") || itm.oDataContext.Contains("/redfish/v1/$metadata#Power.Power"))
        //            {
        //                //******** Power Supplies ***********
        //                //Collection is embedded instead of separate uri links, need to handle differently
        //                if (keyvalue.Key.Contains("PowerSupplies["))
        //                {
        //                    switch (keyvalue.Key)
        //                    {
        //                        case var s when keyvalue.Key.Contains("].FirmwareVersion"):
        //                            //New Power object
        //                            PowerHealth = new HealthPowerHP();
        //                            PowerHealth.Firmware = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + PowerHealth.Speed + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Model"):
        //                            PowerHealth.Model = keyvalue.Value;
        //                            PowerHealth.PDS = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + PowerHealth.Label + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hp.BayNumber"):
        //                            PowerHealth.Label = "Power Supply " + keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.BayNumber"):
        //                            PowerHealth.Label = "Power Supply " + keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Status.Health"):
        //                            PowerHealth.Status = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hp.HotplugCapable"):
        //                            PowerHealth.HotPluggable = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.HotplugCapable"):
        //                            PowerHealth.HotPluggable = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].SerialNumber"):
        //                            PowerHealth.Serial = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].SparePartNumber"):
        //                            PowerHealth.Spare = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].PowerCapacityWatts"):
        //                            PowerHealth.Capacity = keyvalue.Value + " Watts";
        //                            //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Status.State"):
        //                            PowerHealth.Present = keyvalue.Value;
        //                            //Add Power Objects
        //                            HPServer.PowerInfo.Add(PowerHealth);
        //                            //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
        //                            break;
        //                    }

        //                }


        //            }

        //            //******** GEN10 THERMAL  FANS & TEMPS ***********
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Thermal.Thermal"))
        //            {

        //                //******** FANS ***********
        //                //Collection is embedded instead of separate uri links, need to handle differently
        //                if (keyvalue.Key.Contains("Fans["))
        //                {

        //                    switch (keyvalue.Key)
        //                    {
        //                        case var s when keyvalue.Key.Contains("].Name"):
        //                            //New fan object
        //                            FanHealth = new HealthFansHP();
        //                            FanHealth.Label = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + FanHealth.Label + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hp.Location"):
        //                            FanHealth.Zone = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + FanHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.Location"):
        //                            FanHealth.Zone = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + FanHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].ReadingUnits"):

        //                            //FanHealth.Speed = keyvalue.Value + "%";
        //                            //ReturnedRespTXT.Text += "Label = " + FanHealth.Speed + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Reading"):

        //                            FanHealth.Speed = keyvalue.Value + "%";
        //                            //ReturnedRespTXT.Text += "Label = " + FanHealth.Speed + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Status.Health"):
        //                            FanHealth.Status = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Status = " + FanHealth.Status + "\n\n";
        //                            //Add Fan Objects
        //                            HPServer.FanInfo.Add(FanHealth);
        //                            break;
        //                    }


        //                }

        //                //******** TEMPERATURE ***********
        //                //Collection is embedded instead of separate uri links, need to handle differently
        //                if (keyvalue.Key.Contains("Temperatures["))
        //                {

        //                    switch (keyvalue.Key)
        //                    {
        //                        case var s when keyvalue.Key.Contains("].Name"):
        //                            //New Temp object
        //                            TempHealth = new HealthTemperatureHP();
        //                            TempHealth.Label = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + TempHealth.Label + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].ReadingCelsius"):

        //                            TempHealth.CurrentReading = keyvalue.Value + " C";
        //                            //ReturnedRespTXT.Text += "Label = " + TempHealth.Speed + "\n";
        //                            break;

        //                        case var s when keyvalue.Key.Contains("].PhysicalContext"):
        //                            TempHealth.Location = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + TempHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Status.Health"):
        //                            TempHealth.Status = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Status = " + TempHealth.Status + "\n\n";

        //                            break;
        //                        case var s when keyvalue.Key.Contains("].UpperThresholdCritical"):
        //                            TempHealth.Caution = keyvalue.Value + " C";
        //                            //ReturnedRespTXT.Text += "Status = " + TempHealth.Status + "\n\n";

        //                            break;
        //                        case var s when keyvalue.Key.Contains("].UpperThresholdFatal"):
        //                            TempHealth.Critical = keyvalue.Value + " C";
        //                            //ReturnedRespTXT.Text += "Status = " + TempHealth.Status + "\n\n";
        //                            //Add Temp Objects
        //                            HPServer.TemperatureInfo.Add(TempHealth);
        //                            break;
        //                    }


        //                }


        //            }


        //            //******** GEN8,9 THERMAL  FANS & TEMPS ***********
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Chassis/Members/1/Thermal$entity"))
        //            {

        //                //******** FANS ***********
        //                //Collection is embedded instead of separate uri links, need to handle differently
        //                if (keyvalue.Key.Contains("Fans["))
        //                {

        //                    switch (keyvalue.Key)
        //                    {

        //                        case var s when keyvalue.Key.Contains("].CurrentReading"):
        //                            //New fan object
        //                            FanHealth = new HealthFansHP();
        //                            FanHealth.Speed = keyvalue.Value + "%";
        //                            //ReturnedRespTXT.Text += "Label = " + FanHealth.Speed + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].FanName"):

        //                            FanHealth.Label = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + FanHealth.Label + "\n";
        //                            break;

        //                        case var s when keyvalue.Key.Contains("].Oem.Hp.Location"):
        //                            FanHealth.Zone = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + FanHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.Location"):
        //                            FanHealth.Zone = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + FanHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Status.Health"):
        //                            FanHealth.Status = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Status = " + FanHealth.Status + "\n\n";
        //                            //Add Fan Objects
        //                            HPServer.FanInfo.Add(FanHealth);
        //                            break;
        //                    }


        //                }

        //                //******** TEMPERATURE ***********
        //                //Collection is embedded instead of separate uri links, need to handle differently
        //                if (keyvalue.Key.Contains("Temperatures["))
        //                {

        //                    switch (keyvalue.Key)
        //                    {
        //                        case var s when keyvalue.Key.Contains("].Name"):
        //                            //New Temp object
        //                            TempHealth = new HealthTemperatureHP();
        //                            TempHealth.Label = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + TempHealth.Label + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].CurrentReading"):

        //                            TempHealth.CurrentReading = keyvalue.Value + " C";
        //                            //ReturnedRespTXT.Text += "Label = " + TempHealth.Speed + "\n";
        //                            break;

        //                        case var s when keyvalue.Key.Contains("].PhysicalContext"):
        //                            TempHealth.Location = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + TempHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Status.Health"):
        //                            TempHealth.Status = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Status = " + TempHealth.Status + "\n\n";

        //                            break;
        //                        case var s when keyvalue.Key.Contains("].UpperThresholdCritical"):
        //                            TempHealth.Caution = keyvalue.Value + " C";
        //                            //ReturnedRespTXT.Text += "Status = " + TempHealth.Status + "\n\n";

        //                            break;
        //                        case var s when keyvalue.Key.Contains("].UpperThresholdFatal"):
        //                            TempHealth.Critical = keyvalue.Value + " C";
        //                            //ReturnedRespTXT.Text += "Status = " + TempHealth.Status + "\n\n";
        //                            //Add Temp Objects
        //                            HPServer.TemperatureInfo.Add(TempHealth);
        //                            break;
        //                    }


        //                }


        //            }







        //            //******** GEN8 GEN9 INTERNAL NETWORK CARDS *********** 
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Managers/Members/1/EthernetInterfaces") || itm.oDataContext.Contains("/redfish/v1/$metadata#Managers/Members/1/EthernetInterfaces/Members/$entity"))
        //            {
        //                //******** Network Supplies ***********
        //                //Collection is embedded instead of separate uri links, need to handle differently
        //                if (keyvalue.Key.Contains("Items["))
        //                {
        //                    switch (keyvalue.Key)
        //                    {


        //                        case var s when keyvalue.Key.Contains("].FactoryMacAddress"):
        //                            //MessageBox.Show("New Instance");
        //                            //New Network object
        //                            NetworkHealth = new HealthNetworkHP();
        //                            NetworkHealth.MacAddress = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].IPv4Addresses"):

        //                            if (keyvalue.Key.Contains("].AddressOrigin") || keyvalue.Key.Contains("].Gateway") || keyvalue.Key.Contains("].SubnetMask"))
        //                            {
        //                                //skip as wrong value
        //                                //MessageBox.Show("Skipped");
        //                            }
        //                            else
        //                            {

        //                                //Add ip address
        //                                NetworkHealth.IPAddress = keyvalue.Value;
        //                            }

        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
        //                            break;
        //                        //case "Id":
        //                        case var s when keyvalue.Key.Contains("].Id"):
        //                            //MessageBox.Show("Port" + keyvalue.Value);
        //                            NetworkHealth.NetworkPort = "Port " + keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Speed + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Type "):
        //                            NetworkHealth.PortDescription = keyvalue.Value;
        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Label + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].Name"):
        //                            if (keyvalue.Key.Contains("].NameServers"))
        //                            {
        //                                //skip as wrong value
        //                                //MessageBox.Show("Skipped");
        //                            }
        //                            else
        //                            {

        //                                //Add ip address
        //                                NetworkHealth.Location = "Internal " + keyvalue.Value;
        //                            }

        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
        //                            break;

        //                        case var s when keyvalue.Key.Contains("].Status.Health"):

        //                            if (string.IsNullOrEmpty(keyvalue.Value))
        //                            {
        //                                NetworkHealth.Status = "Unknown";
        //                            }
        //                            else
        //                            {
        //                                NetworkHealth.Status = keyvalue.Value;
        //                            }
        //                            //Add Network Objects
        //                            //MessageBox.Show("Added");
        //                            HPServer.NICInfo.Add(NetworkHealth);
        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n"; 
        //                            break;



        //                    }

        //                }


        //            }

        //            //******** GEN8 GEN9  ADDITIONAL NETWORK CARDS ***********
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/NetworkAdapters/Members/$entity"))
        //            {

        //                if (keyvalue.Key == "Name")
        //                {
        //                    //Set string as model only appears once
        //                    networkCard = keyvalue.Value;
        //                }


        //                //******** Network Supplies ***********
        //                //Collection is embedded instead of separate uri links, need to handle differently
        //                if (keyvalue.Key.Contains("PhysicalPorts["))
        //                {
        //                    //MessageBox.Show("Yep Here");
        //                    switch (keyvalue.Key)
        //                    {

        //                        case var s when keyvalue.Key.Contains("].FullDuplex"):
        //                            //New Network object
        //                            NetworkHealth = new HealthNetworkHP();
        //                            //Increment the network card count
        //                            networkCount += 1;

        //                            NetworkHealth.NetworkPort = "Port " + networkCount.ToString();
        //                            NetworkHealth.Location = "Internal Card";
        //                            //***** PORT DESCRIPTION FIELD ********
        //                            //Add Network Card Model to Port Description Field
        //                            NetworkHealth.PortDescription = networkCard;
        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].IPv4Addresses"):


        //                            //Add ip address
        //                            NetworkHealth.IPAddress = keyvalue.Value;

        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
        //                            break;

        //                        case var s when keyvalue.Key.Contains("].MacAddress"):
        //                            NetworkHealth.MacAddress = keyvalue.Value;

        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].UEFIDevicePath"):
        //                            NetworkHealth.Status = "Unknown";
        //                            //Add Network Objects
        //                            HPServer.NICInfo.Add(NetworkHealth);
        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
        //                            break;


        //                    }

        //                }
        //            }



        //            //Collection is embedded instead of separate uri links, need to handle differently

        //            //GEN 10 Additional Card Network Adapters   
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#NetworkAdapterCollection.NetworkAdapterCollection"))
        //            {


        //                if (keyvalue.Key.Contains("Members["))
        //                {
        //                    switch (keyvalue.Key)
        //                    {

        //                        //OTHER PROPERTIES
        //                        case var s when keyvalue.Key.Contains("].Id"):
        //                            MessageBox.Show("New");
        //                            //New Network object
        //                            NetworkHealth = new HealthNetworkHP();
        //                            //Increment the network card count
        //                            networkCount += 1;

        //                            NetworkHealth.NetworkPort = "Port " + networkCount.ToString();
        //                            NetworkHealth.Location = "Internal Card";

        //                            //Add ip address
        //                            NetworkHealth.IPAddress = keyvalue.Value;

        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].PortMaximumMTU"):

        //                            //***** PORT DESCRIPTION FIELD ********
        //                            //Add Network Card Model to Port Description Field
        //                            NetworkHealth.PortDescription = networkCard;
        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
        //                            break;


        //                        case var s when keyvalue.Key.Contains("].Oem.Hpe.LldpData.Receiving.ManagementAddress"):

        //                            NetworkHealth.MacAddress = keyvalue.Value;

        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].PhysicalPortNumber"):

        //                            //MessageBox.Show("ADD");
        //                            //Add Network Objects
        //                            HPServer.NICInfo.Add(NetworkHealth);
        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
        //                            break;


        //                    }

        //                }

        //            }







        //            //GEN 10 Internal Server Ports Ethernet Interfaces
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#EthernetInterfaceCollection.EthernetInterfaceCollection"))
        //            {

        //                //******** Network Supplies ***********
        //                //Collection is embedded instead of separate uri links, need to handle differently

        //                if (keyvalue.Key.Contains("Members["))
        //                {
        //                    switch (keyvalue.Key)
        //                    {

        //                        //OTHER PROPERTIES
        //                        case var s when keyvalue.Key.Contains("].Id"):
        //                            //New Network object
        //                            NetworkHealth = new HealthNetworkHP();

        //                            //Add port number
        //                            NetworkHealth.NetworkPort = "Port " + keyvalue.Value;
        //                            NetworkHealth.Location = "Embedded Card";

        //                            break;
        //                        case var s when keyvalue.Key.Contains("].IPv4Addresses[0].Address"):
        //                            //Workaround any duplicate contains and skip
        //                            if (!keyvalue.Key.Contains("].IPv4Addresses[0].AddressOrigin"))
        //                            {
        //                                //Set IP
        //                                NetworkHealth.IPAddress = keyvalue.Value;
        //                            }

        //                            break;
        //                        case var s when keyvalue.Key.Contains("].PermanentMACAddress"):

        //                            NetworkHealth.MacAddress = keyvalue.Value;

        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n"; 
        //                            break;

        //                        case var s when keyvalue.Key.Contains("].Name"):
        //                            if (!keyvalue.Key.Contains("].NameServers"))
        //                            {
        //                                //Add port description
        //                                NetworkHealth.PortDescription = keyvalue.Value;
        //                            }

        //                            break;
        //                        case var s when keyvalue.Key.Contains("].SpeedMbps"):

        //                            // NO BACKING PROP
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].FullDuplex"):

        //                            // NO BACKING PROP
        //                            break;
        //                        case var s when keyvalue.Key.Contains("].LinkStatus"):

        //                            // NO BACKING PROP
        //                            break;

        //                        case var s when keyvalue.Key.Contains("].Status.Health"):

        //                            NetworkHealth.Status = keyvalue.Value;
        //                            //Add Network Objects
        //                            HPServer.NICInfo.Add(NetworkHealth);
        //                            //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
        //                            break;

        //                    }

        //                }

        //            }








        //            //Storage Properties
        //            //Controller 
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/SmartStorage/ArrayControllers/Members/$entity") || itm.oDataContext.Contains("/redfish/v1/$metadata#HpeSmartStorageArrayController.HpeSmartStorageArrayController"))
        //            {

        //                switch (keyvalue.Key)
        //                {
        //                    case "AdapterType":
        //                        //New Enclosure object
        //                        //StorageHealth = new DriveEnclosureInfoHP();
        //                        //Adapter
        //                        HPServer.StorageInfo.Label = keyvalue.Value;
        //                        break;
        //                    case "Status.Health":
        //                        //Status
        //                        HPServer.StorageInfo.Status = keyvalue.Value;
        //                        break;
        //                    case "SerialNumber":
        //                        //Serial No
        //                        HPServer.StorageInfo.Serial = keyvalue.Value;
        //                        break;
        //                    case "Model":
        //                        //Model
        //                        HPServer.StorageInfo.Model = keyvalue.Value;
        //                        break;
        //                    case "FirmwareVersion.Current.VersionString":
        //                        //Firmware Version
        //                        HPServer.StorageInfo.Firmware = keyvalue.Value.Trim();
        //                        break;
        //                }
        //            }

        //            //Enclosure  
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/SmartStorage/ArrayControllers/Members/0/StorageEnclosures/Members/$entity") || itm.oDataContext.Contains("/redfish/v1/$metadata#HpeSmartStorageStorageEnclosure.HpeSmartStorageStorageEnclosure"))
        //            {

        //                //Storage Enclosure
        //                switch (keyvalue.Key)
        //                {
        //                    case "DriveBayCount":
        //                        //New Enclosure object
        //                        DriveEnclosureInfo = new DriveEnclosureInfoHP();
        //                        //Drive Bay Count
        //                        DriveEnclosureInfo.DriveBay = keyvalue.Value;
        //                        break;
        //                    case "Location":
        //                        DriveEnclosureInfo.Label = keyvalue.Value;
        //                        break;

        //                    case "Status.Health":
        //                        //Status
        //                        DriveEnclosureInfo.Status = keyvalue.Value;
        //                        //Add enclosure object
        //                        HPServer.StorageInfo.DriveEnclosure.Add(DriveEnclosureInfo);
        //                        break;
        //                }
        //            }



        //            //Logical Drives
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/SmartStorage/ArrayControllers/Members/0/LogicalDrives/Members/$entity") || itm.oDataContext.Contains("/redfish/v1/$metadata#HpeSmartStorageLogicalDrive.HpeSmartStorageLogicalDrive"))
        //            {

        //                switch (keyvalue.Key)
        //                {
        //                    case "CapacityMiB":
        //                        //New Enclosure object
        //                        LogicalDriveInfo = new HealthLogicalDriveHP();
        //                        //Drive Bay Count
        //                        LogicalDriveInfo.Capacity = (Convert.ToInt32(keyvalue.Value) / 1024).ToString() + "GB";
        //                        break;
        //                    case "LogicalDriveNumber":
        //                        LogicalDriveInfo.Label = keyvalue.Value;
        //                        break;
        //                    case "LogicalDriveEncryption":
        //                        LogicalDriveInfo.Encryption = keyvalue.Value;
        //                        break;
        //                    case "LogicalDriveType":
        //                        LogicalDriveInfo.LogicalDriveType = keyvalue.Value;
        //                        break;
        //                    case "Raid":
        //                        LogicalDriveInfo.FaultTolerance = "Raid " + keyvalue.Value;
        //                        break;
        //                    case "Status.Health":
        //                        //Status
        //                        LogicalDriveInfo.Status = keyvalue.Value;
        //                        //Add enclosure object
        //                        HPServer.StorageInfo.LogicalDrives.Add(LogicalDriveInfo);
        //                        break;
        //                }
        //            }




        //            //Physical Drives
        //            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/SmartStorage/ArrayControllers/Members/0/DiskDrives/Members/$entity") || itm.oDataContext.Contains("/redfish/v1/$metadata#HpeSmartStorageDiskDrive.HpeSmartStorageDiskDrive"))
        //            {

        //                switch (keyvalue.Key)
        //                {
        //                    case "@odata.id":
        //                        //Important! Capture odata id to identify which disk is in which logical drive
        //                        PhysicalDrive = new PhysicalDrivesHP();
        //                        PhysicalDrive.odataId = keyvalue.Value;
        //                        break;
        //                    case "CapacityGB":
        //                        //Drive Bay Count
        //                        PhysicalDrive.MarketingCapacity = keyvalue.Value + "GB"; //(Convert.ToInt32(keyvalue.Value) / 1024).ToString() + "GB";
        //                        break;
        //                    case "CapacityMiB":
        //                        //Drive Bay Count
        //                        PhysicalDrive.Capacity = (Convert.ToInt32(keyvalue.Value) / 1024).ToString() + "GB";
        //                        break;
        //                    case "EncryptedDrive":
        //                        //Encryption
        //                        PhysicalDrive.EncryptionStatus = keyvalue.Value;
        //                        PhysicalDrive.Encryption = PhysicalDrive.EncryptionStatus;
        //                        break;
        //                    case "FirmwareVersion.Current.VersionString":
        //                        //Firmware
        //                        PhysicalDrive.FirmwareVersion = keyvalue.Value;

        //                        break;
        //                    case "MediaType":
        //                        //Media Type
        //                        PhysicalDrive.MediaType = keyvalue.Value;
        //                        break;
        //                    case "BlockSize":
        //                        //Interface
        //                        PhysicalDrive.BlockSize = keyvalue.Value;
        //                        break;
        //                    case "CurrentTemperatureCelsius":
        //                        //Interface
        //                        PhysicalDrive.CurrentTemp = keyvalue.Value;
        //                        break;
        //                    case "MaximumTemperatureCelsius":
        //                        //Interface
        //                        PhysicalDrive.MaxTemp = keyvalue.Value;
        //                        break;
        //                    case "RotationalSpeedRpm":
        //                        //Interface
        //                        PhysicalDrive.RotationalSpeed = keyvalue.Value;
        //                        break;
        //                    case "InterfaceType":
        //                        //Interface
        //                        PhysicalDrive.Interface = keyvalue.Value;
        //                        break;
        //                    case "InterfaceSpeed":
        //                        //Interface Speed
        //                        PhysicalDrive.InterfaceSpeed = keyvalue.Value;
        //                        break;
        //                    case "Location":
        //                        //Location
        //                        PhysicalDrive.Label = keyvalue.Value;
        //                        PhysicalDrive.Location = keyvalue.Value;
        //                        break;
        //                    case "Model":
        //                        //Model
        //                        PhysicalDrive.Model = keyvalue.Value;
        //                        //PhysicalDriveInfo.Model = keyvalue.Value;
        //                        break;
        //                    case "SerialNumber":
        //                        //Model
        //                        PhysicalDrive.SerialNumber = keyvalue.Value;
        //                        PhysicalDrive.SerialNo = keyvalue.Value;
        //                        break;
        //                    case "Status.Health":
        //                        //Model
        //                        PhysicalDrive.Status = keyvalue.Value;
        //                        PhysicalDrive.Health = keyvalue.Value;
        //                        break;

        //                    case "Status.State":
        //                        //Assign State Value
        //                        PhysicalDrive.DriveConfiguration = keyvalue.Value;
        //                        //Add To PhysicalDrive Collection
        //                        HPServer.PhysicalDriveInfo.Add(PhysicalDrive);

        //                        break;
        //                }
        //            }


        //            //**** IMPORTANT ****
        //            // Logical Drive Members
        //            // Because a flat data file the logical drives get added first then the physicals, loop through the logical drives   || itm.oDataContext.Contains("/redfish/v1/Systems/1/SmartStorage/ArrayControllers/0/LogicalDrives/1/DataDrives/"
        //            //Loop starting at 1 /redfish/v1/$metadata#HpeSmartStorageDiskDriveCollection.HpeSmartStorageDiskDriveCollection
        //            for (int i = 1; i < HPServer.StorageInfo.LogicalDrives.Count() + 1; i++)
        //            {
        //                if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/SmartStorage/ArrayControllers/Members/0/LogicalDrives/Members/" + i + "/DataDrives") || itm.oDataContext.Contains("/redfish/v1/$metadata#HpeSmartStorageDiskDriveCollection.HpeSmartStorageDiskDriveCollection"))
        //                {
        //                    switch (keyvalue.Key)
        //                    {
        //                        case var s when keyvalue.Key.Contains("Members["):

        //                            //MessageBox.Show(keyvalue.Value + " " + i.ToString());

        //                            foreach (var drive in HPServer.PhysicalDriveInfo)
        //                            {

        //                                if (drive.odataId == keyvalue.Value)
        //                                {
        //                                    // Add to separate physical drives only collection
        //                                    HPServer.StorageInfo.LogicalDrives.ElementAt(i - 1).PhysicalDrives.Add(drive);
        //                                }
        //                            }

        //                            break;
        //                    }


        //                }
        //            }


        //            // ******************** END HP DATA SPLIT ******************* 



        //        } // redfish key value loop



        //        //Set 100 % Percent done
        //        // passedvm.ProgressPercentage1 = 100;


        //    }





        //    //*********    HEALTH AGREGGATE       *************
        //    int populated = 0;
        //    //Run Custom Health AggregateHealthStatus
        //    if (ServerGen == "Gen8" || ServerGen == "Gen9")
        //    {
        //        //HEALTH ROLL UP 
        //        //Bios
        //        HPServer.BiosHardwareStatus = HPServer.HostRomStatus;
        //        //Cpu
        //        HPServer.ProcessorStatus = HPServer.CPUHealth;
        //        //Memory Health Roll Up
        //        HPServer.MemoryStatus = HPServer.MemoryHealth;
        //        //Storage Health Roll Up
        //        HPServer.StorageStatus = HPServer.StorageInfo.Status;
        //        //Network Health at a glance
        //        var NetworkHealthRollUp = HPServer.NICInfo.Where(e => e.Status != "OK" || e.Status != "Unknown").ToList();

        //        //Check if different value found, if so display it
        //        if (NetworkHealthRollUp.Count > 0)
        //        {
        //            foreach (var item in NetworkHealthRollUp)
        //            {
        //                if (item.Status == "Unknown")
        //                {
        //                    //if blank, set as ok
        //                    HPServer.NetworkStatus = "OK";
        //                }
        //                else
        //                {
        //                    HPServer.NetworkStatus = item.Status;
        //                }

        //            }
        //        }
        //        else
        //        {
        //            HPServer.NetworkStatus = "OK";

        //        }


        //        //Power Health at a glance
        //        var PowerHealthRollUp = HPServer.PowerInfo.Where(e => e.Status != "OK").ToList();
        //        //Check if different value found, if so display it
        //        if (PowerHealthRollUp.Count > 0)
        //        {
        //            foreach (var item in PowerHealthRollUp)
        //            {
        //                HPServer.PowerState = item.Status;
        //                HPServer.PowerSupplyRedundancy = "Not Redundant";
        //                HPServer.PowerSuppliesStatus = item.Status;
        //            }
        //        }
        //        else
        //        {
        //            HPServer.PowerState = "OK";
        //            HPServer.PowerSupplyRedundancy = "Redundant";
        //            HPServer.PowerSuppliesStatus = "OK";
        //        }


        //        //Fans Health at a glance
        //        var FansHealthRollUp = HPServer.FanInfo.Where(e => e.Status != "OK").ToList();
        //        //Check if different value found, if so display it
        //        if (FansHealthRollUp.Count > 0)
        //        {
        //            foreach (var item in FansHealthRollUp)
        //            {
        //                HPServer.FansRedundancy = "Not Redundant";
        //                HPServer.FansStatus = item.Status;
        //            }
        //        }
        //        else
        //        {
        //            HPServer.FansStatus = "OK";
        //            HPServer.FansRedundancy = "Redundant";
        //        }

        //        //Temperature Health at a glance
        //        var TemperatureHealthRollUp = HPServer.TemperatureInfo.Where(e => e.Status != "OK" || e.Status != "").ToList();

        //        //MessageBox.Show(TemperatureHealthRollUp.Count.ToString());
        //        //Check if different value found, if so display it
        //        if (TemperatureHealthRollUp.Count > 0)
        //        {
        //            foreach (var item in TemperatureHealthRollUp)
        //            {
        //                if (item.Status == "")
        //                {
        //                    //if blank, set as ok
        //                    HPServer.TemperatureStatus = "OK";
        //                }
        //                else
        //                {
        //                    HPServer.TemperatureStatus = item.Status;
        //                }

        //            }
        //        }
        //        else
        //        {
        //            HPServer.TemperatureStatus = "OK";

        //        }




        //        //For non collection properties
        //        HPServer.PopulatedSlots = dimmCount.ToString();
        //        HPServer.TotalMemory = (totalMem / 1024).ToString();
        //    }
        //    else
        //    {


        //        //For non collection properties
        //        foreach (var dimm in HPServer.MemoryInfo)
        //        {

        //            if (string.IsNullOrEmpty(dimm.MemType))
        //            {

        //            }
        //            else
        //            {
        //                //Add to count
        //                populated += 1;
        //            }


        //        }
        //        //For non collection properties
        //        HPServer.PopulatedSlots = populated.ToString(); //dimmCount.ToString();
        //        HPServer.TotalMemory = (totalMem / 1024).ToString();
        //    }





        //    //Clear EntireServer collection after Server Data Was Captured
        //    EntireServer.RedfishSection.Clear();


        //    // MessageBox.Show(HPServer.TpmVisibility + "/" + HPServer.TpmType + "/" + HPServer.TpmState);


        //    return HPServer;
        //}


        //GET DATA FROM SERVERS
        public async Task<HPServerData> GetHPData(string ServerIP, string PasswordOverride, MainViewModel passedvm, string ServerGen)
        {
            //Write link to console window
            passedvm.ILORestOutput1 += "****** Starting Server Inventory, please wait... ******\n\n";

            passedvm.ProgressPercentage1 = 10;

            //Counter for looping through odata links
            int Counter = 0;



            switch (ServerGen)
            {
                case "Gen8":

                    //INITIAL ROOT CALLS
                    passedvm.ILORestOutput1 += "*** Checking Root Node ***\n\n";
                    //ROOT NODE ONE VIEW SYSTEM CHECK (Write directly to the script window as the check looks for data there)
                    passedvm.ILORestOutput1 += await RedfishRestCall("https://" + ServerIP + "/redfish/v1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);

                    //await ReadRedfishRoot(jsonReturned, "", "HP");

                    //test one view warning
                    //passedvm.ILORestOutput1 += "\n" + "One View" + "\n";

                    ////SD CARD CHECK
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/managers/1/embeddedmedia/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");


                    //SYSTEMS IF RUNNING THIS YOU DON'T REQUIRE MEMORY
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    //Chassis
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    //Managers Contains ILO Self Test info DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    //SYSTEMS 
                    //Bios DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Bios/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");


                    //Firmware Inventory DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/FirmwareInventory/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");



                    //CPU DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Processors/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    ////Memory DIRECT
                    //await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Memory/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(),passedvm);


                    //await ReadRedfishRoot(jsonReturned, "", "HP");

                    //Ethernet Interfaces DIRECT
                    //await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/EthernetInterfaces/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(),passedvm);


                    //await ReadRedfishRoot(jsonReturned, "", "HP");

                    ////Network Adapters DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/NetworkAdapters/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    ////Set Layer Depth to 4 to drill down further (Mainly required for Storage) use 6 to get logical drive members
                    URILayerDepth = 6;





                    //Write link to console window
                    passedvm.ILORestOutput1 += "\n\n****** Running Redfish Crawler Scripts ******\n\n";

                    //Drill down 10 layers and follow any returned links URILayerDepth 
                    for (int i = 0; i < URILayerDepth; i++)
                    {


                        foreach (var itm in RootLinks)
                        {
                            //Write link to console window
                            passedvm.ILORestOutput1 += itm + "\n";
                            //Console.WriteLine(itm);
                            ResourceLinks.Add(itm);
                        }

                        RootLinks.Clear();

                        //Set Percent done
                        passedvm.ProgressPercentage1 = passedvm.ProgressPercentage1 + 10;

                        ////Loop through each resource link and drill down multiple layers 
                        foreach (var item in ResourceLinks)
                        {
                            //Call odata and href links
                            //await RedfishRestCall(@"https://" + ServerIP + item.ToString() + "?$expand=*($levels=1)", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
                            await RedfishRestCall(@"https://" + ServerIP + item.ToString() + "", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
                            //Flatten Json objects to Key Value Pairs
                            await ReadRedfishRoot(jsonReturned, "Layer" + i.ToString(), "HP");
                        }



                        // Update Layer Count
                        Counter += 1;
                        //Set Message


                        ResourceLinks.Clear();
                    }

                    RootLinks.Clear();

                    break;
                case "Gen9":


                    //INITIAL ROOT CALLS
                    passedvm.ILORestOutput1 += "*** Checking Root Node ***\n\n";
                    //ROOT NODE ONE VIEW SYSTEM CHECK (Write directly to the script window as the check looks for data there)
                    passedvm.ILORestOutput1 += await RedfishRestCall("https://" + ServerIP + "/redfish/v1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);

                    //await ReadRedfishRoot(jsonReturned, "", "HP");

                    //test one view warning
                    //passedvm.ILORestOutput1 += "\n" + "One View" + "\n";

                    ////SD CARD CHECK
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/managers/1/embeddedmedia/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");


                    //SYSTEMS IF RUNNING THIS YOU DON'T REQUIRE MEMORY
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    //Chassis
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    //Managers Contains ILO Self Test info DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    //SYSTEMS 
                    //Bios DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Bios/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");


                    //Firmware Inventory DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/FirmwareInventory/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");



                    //CPU DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Processors/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    ////Memory DIRECT
                    //await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Memory/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(),passedvm);


                    //await ReadRedfishRoot(jsonReturned, "", "HP");

                    //Ethernet Interfaces DIRECT
                    //await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/1/EthernetInterfaces/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    //await ReadRedfishRoot(jsonReturned, "", "HP");

                    ////Network Adapters DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/NetworkAdapters/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    ////Set Layer Depth to 4 to drill down further (Mainly required for Storage) use 6 to get logical drive members
                    URILayerDepth = 6;





                    //Write link to console window
                    passedvm.ILORestOutput1 += "\n\n****** Running Redfish Crawler Scripts ******\n\n";

                    //Drill down 10 layers and follow any returned links URILayerDepth 
                    for (int i = 0; i < URILayerDepth; i++)
                    {


                        foreach (var itm in RootLinks)
                        {
                            //Write link to console window
                            passedvm.ILORestOutput1 += itm + "\n";
                            //Console.WriteLine(itm);
                            ResourceLinks.Add(itm);
                        }

                        RootLinks.Clear();

                        //Set Percent done
                        passedvm.ProgressPercentage1 = passedvm.ProgressPercentage1 + 10;

                        ////Loop through each resource link and drill down multiple layers 
                        foreach (var item in ResourceLinks)
                        {
                            //Call odata and href links
                            //await RedfishRestCall(@"https://" + ServerIP + item.ToString() + "?$expand=*($levels=1)", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
                            await RedfishRestCall(@"https://" + ServerIP + item.ToString() + "", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
                            //Flatten Json objects to Key Value Pairs
                            await ReadRedfishRoot(jsonReturned, "Layer" + i.ToString(), "HP");
                        }



                        // Update Layer Count
                        Counter += 1;
                        //Set Message


                        ResourceLinks.Clear();
                    }

                    RootLinks.Clear();

                    break;
                //case "Gen10":
                //    //INITIAL ROOT CALLS
                //    passedvm.ILORestOutput1 += "*** Checking Root Node ***\n\n";
                //    //ROOT NODE ONE VIEW SYSTEM CHECK (Write directly to the script window as the check looks for data there)
                //    //passedvm.ILORestOutput1 += await RedfishRestCall("https://" + ServerIP + "/redfish/v1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);

                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
                //    await ReadRedfishRoot(jsonReturned, "", "HP");
                //    passedvm.ILORestOutput1 += jsonReturned;
                //    //Clear Links as don't want it to run in loop below
                //    RootLinks.Clear();

                //    //////SD CARD CHECK
                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/managers/1/embeddedmedia/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    await ReadRedfishRoot(jsonReturned, "", "HP");






                //    //SYSTEMS IF RUNNING THIS YOU DON'T REQUIRE MEMORY
                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    await ReadRedfishRoot(jsonReturned, "", "HP");

                //    //////Firmware
                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/UpdateService/FirmwareInventory/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    await ReadRedfishRoot(jsonReturned, "", "HP");



                //    //////Chassis
                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    await ReadRedfishRoot(jsonReturned, "", "HP");

                //    ////Managers Contains ILO Self Test info DIRECT ?$expand=.
                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    await ReadRedfishRoot(jsonReturned, "", "HP");


                //    //////Thermal DIRECT
                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/Thermal/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    await ReadRedfishRoot(jsonReturned, "", "HP");

                //    //////Power DIRECT
                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/Power/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    await ReadRedfishRoot(jsonReturned, "", "HP");


                //    //CPU DIRECT
                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Processors/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    await ReadRedfishRoot(jsonReturned, "", "HP");

                //    ////Memory DIRECT
                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Memory/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    await ReadRedfishRoot(jsonReturned, "", "HP");

                //    //////Ethernet Interfaces Manager DIRECT
                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/1/EthernetInterfaces/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    await ReadRedfishRoot(jsonReturned, "", "HP");

                //    //////Base Network Adapters System DIRECT
                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/BaseNetworkAdapters/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    await ReadRedfishRoot(jsonReturned, "", "HP");

                //    //Network Adapters DIRECT 
                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/NetworkAdapters/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    await ReadRedfishRoot(jsonReturned, "", "HP");



                //    RootLinks.Clear();
                //    ////////Storage
                //    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/SmartStorage/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    await ReadRedfishRoot(jsonReturned, "", "HP");

                //    //////Storage
                //    //await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/SmartStorage/HostBusAdapters/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                //    //await ReadRedfishRoot(jsonReturned, "", "HP");


                //    ////Set Layer Depth to 4 to drill down further (Mainly required for Storage) use 6 to get logical drive members
                //    URILayerDepth = 4;

                //    //Write link to console window
                //    passedvm.ILORestOutput1 += "\n\n****** Running Redfish Crawler Scripts ******\n\n";

                //    //Drill down 10 layers and follow any returned links URILayerDepth 
                //    for (int i = 0; i < URILayerDepth; i++)
                //    {


                //        foreach (var itm in RootLinks)
                //        {
                //            //Write link to console window
                //            passedvm.ILORestOutput1 += itm + "\n";
                //            //Console.WriteLine(itm);
                //            ResourceLinks.Add(itm);
                //        }

                //        //RootLinks.Clear();

                //        //Set Percent done
                //        passedvm.ProgressPercentage1 = passedvm.ProgressPercentage1 + 10;

                //        ////Loop through each resource link and drill down multiple layers 
                //        foreach (var item in ResourceLinks)
                //        {
                //            //Call odata and href links
                //            // use ?$expand=. to expand any Members, will require a different sorting method
                //            await RedfishRestCall(@"https://" + ServerIP + item.ToString() + "", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
                //            //Flatten Json objects to Key Value Pairs
                //            await ReadRedfishRoot(jsonReturned, "Layer" + i.ToString(), "HP");
                //        }



                //        // Update Layer Count
                //        Counter += 1;
                //        //Set Message


                //        ResourceLinks.Clear();
                //    }

                //    RootLinks.Clear();

                //    break;
                case "Gen10":
                    //INITIAL ROOT CALLS
                    passedvm.ILORestOutput1 += "*** Checking Root Node ***\n\n";
                    //ROOT NODE ONE VIEW SYSTEM CHECK (Write directly to the script window as the check looks for data there)
                    //passedvm.ILORestOutput1 += await RedfishRestCall("https://" + ServerIP + "/redfish/v1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);

                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
                    await ReadRedfishRoot(jsonReturned, "", "HP");
                    passedvm.ILORestOutput1 += jsonReturned;
                    //Clear Links as don't want it to run in loop below
                    RootLinks.Clear();

                    //////SD CARD CHECK
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/managers/1/embeddedmedia/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");



                    //////Storage
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/SmartStorage/ArrayControllers/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    ////Storage
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/SmartStorage/HostBusAdapters/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");



                    ////Set Layer Depth to 4 to drill down further (Mainly required for Storage) use 6 to get logical drive members
                    URILayerDepth = 4;

                    //Write link to console window
                    passedvm.ILORestOutput1 += "\n\n****** Running Redfish Crawler Scripts ******\n\n";

                    //Drill down 10 layers and follow any returned links URILayerDepth 
                    for (int i = 0; i < URILayerDepth; i++)
                    {


                        foreach (var itm in RootLinks)
                        {
                            //Write link to console window
                            passedvm.ILORestOutput1 += itm + "\n";
                            //Console.WriteLine(itm);
                            ResourceLinks.Add(itm);
                        }

                        RootLinks.Clear();

                        //Set Percent done
                        passedvm.ProgressPercentage1 = passedvm.ProgressPercentage1 + 10;

                        ////Loop through each resource link and drill down multiple layers 
                        foreach (var item in ResourceLinks)
                        {
                            //Call odata and href links
                            // use ?$expand=. to expand any Members, will require a different sorting method
                            await RedfishRestCall(@"https://" + ServerIP + item.ToString() + "", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);
                            //Flatten Json objects to Key Value Pairs
                            await ReadRedfishRoot(jsonReturned, "Layer" + i.ToString(), "HP");
                        }



                        // Update Layer Count
                        Counter += 1;
                        //Set Message


                        ResourceLinks.Clear();
                    }

                    RootLinks.Clear();



                    //SYSTEMS IF RUNNING THIS YOU DON'T REQUIRE MEMORY
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    //////Firmware
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/UpdateService/FirmwareInventory/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");



                    //////Chassis
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    ////Managers Contains ILO Self Test info DIRECT ?$expand=.
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/1/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");


                    //////Thermal DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/Thermal/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    //////Power DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/Power/", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");


                    //CPU DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Processors/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    ////Memory DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/Memory/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    //////Ethernet Interfaces Manager DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/1/EthernetInterfaces/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    //////Base Network Adapters System DIRECT
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/1/BaseNetworkAdapters/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");

                    //Network Adapters DIRECT 
                    await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/1/NetworkAdapters/?$expand=.", HttpMethod.Get, "Administrator:" + PasswordOverride.Trim(), passedvm);


                    await ReadRedfishRoot(jsonReturned, "", "HP");




                    break;
            }







            // EXTRACT JSON DATA TO INTERNAL CLASS
            // SEPARATION INTO A HP SERVER DATA OBJECT NEEDS TO BE HERE, WHEN ALL LINK DATA HAVE BEEN ADDED

            //HP DATA
            //Create base server class
            HPServerData HPServer = new HPServerData();

            //Declarations of new HP Object Collections
            HealthCPUHP cpuHealth = new HealthCPUHP();
            HealthMemoryHP MemoryHealth = new HealthMemoryHP();
            HealthFansHP FanHealth = new HealthFansHP();
            HealthTemperatureHP TempHealth = new HealthTemperatureHP();
            HealthPowerHP PowerHealth = new HealthPowerHP();
            HealthNetworkHP NetworkHealth = new HealthNetworkHP();
            //New Storage info instances
            HPServer.StorageInfo = new HealthStorageHP();
            HPServer.StorageInfo.LogicalDrive = new HealthLogicalDriveHP();
            //HPServer.StorageInfo.LogicalDrive.PhysicalDrives = new ObservableCollection<PhysicalDrivesHP>();
            DriveEnclosureInfoHP DriveEnclosureInfo = new DriveEnclosureInfoHP();
            HealthLogicalDriveHP LogicalDriveInfo = new HealthLogicalDriveHP();
            PhysicalDrivesHP PhysicalDriveInfo = new PhysicalDrivesHP();
            PhysicalDrivesHP PhysicalDrive = new PhysicalDrivesHP();


            //To hold Network Card Model
            string networkCard = "";
            int networkCount = 0;

            //Memory Counts
            int dimmCount = 0;
            double totalMem = 0;


            //OUT PUT SERVER DATA
            foreach (var itm in EntireServer.RedfishSection)
            {
                //Uncomment to see Data returned in Output window
                //Console.WriteLine(itm.oDataContext + "\n");

                //Loop through all key value pairs in Entire Server Object
                foreach (var keyvalue in itm.RedfishPairs)
                {
                    //Uncomment to see Data returned in Output window
                    //Console.WriteLine(keyvalue.Key + " = " + keyvalue.Value);

                    //SD CARD Property extraction
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Managers/Members/1/EmbeddedMedia$entity"))
                    {

                        switch (keyvalue.Key)
                        {
                            case "SDCard.Status.State":
                                //SD Card inserted property
                                HPServer.SDCardInserted = keyvalue.Value.Trim();

                                //SPOOF TO TRIGGER WARNING uncomment
                                //HPServer.SDCardInserted = "Inserted";
                                break;
                        }
                    }


                    //Bios Properties
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/Bios$entity"))
                    {

                        switch (keyvalue.Key)
                        {
                            case "Oem.Hp.HPSystemManagementHomepageAddress":
                                //Manufacturer
                                HPServer.HostName = keyvalue.Value.Trim();
                                break;
                        }
                    }





                    //GEN 10 Firmware
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#SoftwareInventoryCollection.SoftwareInventoryCollection"))
                    {
                        //Collection is embedded instead of separate uri links, need to handle differently


                        if (keyvalue.Key.Contains("Members["))
                        {
                            switch (keyvalue.Key)
                            {
                                case var s when keyvalue.Key.Contains("].Description"):

                                    if (keyvalue.Value == "InnovationEngineFirmware")
                                    {
                                        HPServer.InnovationEngine = keyvalue.Value.Trim();
                                    }

                                    if (keyvalue.Value == "SPSFirmwareVersionData")
                                    {
                                        HPServer.SPSVersion = keyvalue.Value.Trim();
                                    }

                                    break;
                                case var s when keyvalue.Key.Contains("].Version"):

                                    if (HPServer.InnovationEngine == "InnovationEngineFirmware")
                                    {
                                        HPServer.InnovationEngine = keyvalue.Value.Trim();
                                    }

                                    if (HPServer.SPSVersion == "SPSFirmwareVersionData")
                                    {
                                        HPServer.SPSVersion = keyvalue.Value.Trim();
                                    }
                                    break;

                            }

                        }

                    }



                    //System info 
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/$entity") || itm.oDataContext.Contains("/redfish/v1/$metadata#ComputerSystem.ComputerSystem"))
                    {

                        switch (keyvalue.Key)
                        {
                            case "AssetTag":
                                //Asset Tag
                                HPServer.AssetTag = keyvalue.Value.Trim();
                                break;
                            case "HostName":
                                //Server Name
                                HPServer.HostName = keyvalue.Value.Trim();
                                break;

                            case "Manufacturer":
                                //Manufacturer
                                HPServer.Manufacturer = keyvalue.Value.Trim();
                                break;
                            case "Model":
                                //Model
                                HPServer.Model = keyvalue.Value.Trim();
                                break;
                            case "SerialNumber":
                                //Chassis Serial
                                HPServer.ChassisSerial = keyvalue.Value.Trim();
                                break;
                            case "SKU":
                                //SKU
                                HPServer.SKU = keyvalue.Value.Trim();
                                break;

                            case "ProcessorSummary.Count":
                                //Processor 
                                HPServer.CPUCount = keyvalue.Value.Trim();
                                break;
                            case "ProcessorSummary.Status.HealthRollUp":
                                //CPU Health Rollup
                                HPServer.CPUHealth = keyvalue.Value.Trim();
                                break;
                            case "ProcessorSummary.Model":
                                //Processor 
                                HPServer.CPUFamily = keyvalue.Value.Trim();
                                break;
                            case "Memory.Status.HealthRollUp":
                                //Memory Roll Up Health
                                HPServer.MemoryHealth = keyvalue.Value.Trim();
                                break;
                            case "Oem.Hp.PostState":
                                //Post State
                                HPServer.PostState = keyvalue.Value.Trim();
                                break;
                            //Gen10
                            case "Oem.Hpe.PostState":
                                //Post State
                                HPServer.PostState = keyvalue.Value.Trim();
                                break;
                            //HEALTH ROUND UP GEN 10
                            case "Oem.Hpe.AggregateHealthStatus.BiosOrHardwareHealth.Status.Health":
                                //Post State
                                HPServer.BiosHardwareStatus = keyvalue.Value.Trim();
                                break;
                            case "Oem.Hpe.AggregateHealthStatus.FanRedundancy":
                                //Post State
                                HPServer.FansRedundancy = keyvalue.Value.Trim();
                                break;
                            case "Oem.Hpe.AggregateHealthStatus.Fans.Status.Health":
                                //Post State
                                HPServer.FansStatus = keyvalue.Value.Trim();
                                break;
                            case "Oem.Hpe.AggregateHealthStatus.Memory.Status.Health":
                                //Post State
                                HPServer.MemoryStatus = keyvalue.Value.Trim();
                                //Assign to Frontpage
                                HPServer.MemoryHealth = HPServer.MemoryStatus;
                                break;
                            case "Oem.Hpe.AggregateHealthStatus.Processors.Status.Health":
                                //Post State
                                HPServer.ProcessorStatus = keyvalue.Value.Trim();
                                //Assign to Frontpage
                                HPServer.CPUHealth = HPServer.ProcessorStatus;
                                break;
                            case "Oem.Hpe.AggregateHealthStatus.PowerSupplies.Status.Health":
                                //Post State
                                HPServer.PowerSuppliesStatus = keyvalue.Value.Trim();
                                break;
                            case "Oem.Hpe.AggregateHealthStatus.PowerSupplyRedundancy":
                                //Post State
                                HPServer.PowerSupplyRedundancy = keyvalue.Value.Trim();
                                break;
                            case "Oem.Hpe.AggregateHealthStatus.SmartStorageBattery.Status.Health":
                                //Post State
                                //HPServer.smar = keyvalue.Value.Trim();
                                break;
                            case "Oem.Hpe.AggregateHealthStatus.Storage.Status.Health":
                                //Post State
                                HPServer.StorageStatus = keyvalue.Value.Trim();
                                break;
                            case "Oem.Hpe.AggregateHealthStatus.Temperatures.Status.Health":
                                //Post State
                                HPServer.TemperatureStatus = keyvalue.Value.Trim();
                                HPServer.NetworkStatus = "OK";
                                break;
                            case "Oem.Hpe.Bios.Current.VersionString":
                                //BIOS
                                HPServer.BiosCurrentVersion = keyvalue.Value.Trim();
                                break;

                            case "Oem.Hpe.Bios.Backup.VersionString":
                                //BACKUP BIOS
                                HPServer.BiosBackupVersion = keyvalue.Value.Trim();
                                break;


                            case "Oem.Hpe.IntelligentProvisioningVersion":
                                //Intelligent Provisioning
                                HPServer.IntelligentProvisioningVersion = keyvalue.Value.Trim();
                                break;
                            //TPM Properties
                            case "Bios.Attributes.TpmVisibility":
                                //TPM STATE, TO DISABLE use redfish patch to set to Hidden. Reset to Visible after updates are done
                                HPServer.TpmVisibility = keyvalue.Value.Trim();
                                break;
                            case "Bios.Attributes.TpmType":
                                //TPM STATE, TO DISABLE use redfish patch to set to Hidden. Reset to Visible after updates are done
                                HPServer.TpmType = keyvalue.Value.Trim();
                                break;
                            case "Bios.Attributes.TpmState":
                                //TPM STATE, TO DISABLE use redfish patch to set to Hidden. Reset to Visible after updates are done
                                HPServer.TpmState = keyvalue.Value.Trim();
                                break;


                        }


                    }

                    //Gen8 Gen9 Firmware Oem.Hpe.Bios.Backup.VersionString
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/FirmwareInventory$entity"))
                    {

                        switch (keyvalue.Key)
                        {

                            case "Current.SystemRomActive[0].VersionString":
                                //BIOS
                                HPServer.BiosCurrentVersion = keyvalue.Value.Trim();
                                break;

                            case "Current.SystemRomBackup[0].VersionString":
                                //BACKUP BIOS
                                HPServer.BiosBackupVersion = keyvalue.Value.Trim();
                                break;

                            case "Current.SystemBMC[0].VersionString":
                                //ILO
                                HPServer.iLOVersion = keyvalue.Value.Trim();
                                break;
                            case "Current.SPSFirmwareVersionData[0].VersionString":
                                //SPS
                                HPServer.SPSVersion = keyvalue.Value.Trim();
                                break;

                            case "Current.IntelligentProvisioning[0].VersionString":
                                //Intelligent Provisioning
                                HPServer.IntelligentProvisioningVersion = keyvalue.Value.Trim();
                                break;

                        }

                    }



                    //ILO Self Test /redfish/v1/$metadata#Manager.Manager
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Managers/Members/$entity") || itm.oDataContext.Contains("/redfish/v1/$metadata#Manager.Manager"))
                    {

                        switch (keyvalue.Key)
                        {

                            case "Oem.Hp.License.LicenseString":
                                //Ilo License Shows under Firmware Levels
                                HPServer.ILOLicense = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[0].Notes"):
                                //NVRAM DATA
                                HPServer.NVRAMDataNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[0].Status"):
                                //NVRAM DATA
                                HPServer.NVRAMDataStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[1].Notes"):
                                //NVRAM SPACE
                                HPServer.NVRAMSpaceNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[1].Status"):
                                //NVRAM SPACE
                                HPServer.NVRAMSpaceStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[2].Notes"):
                                //SD CARD
                                HPServer.EmbeddedFlashSDCardNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[2].Status"):
                                //SD CARD
                                HPServer.EmbeddedFlashSDCardStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[3].Notes"):
                                //EPROM
                                HPServer.EEPROMNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[3].Status"):
                                //EPROM
                                HPServer.EEPROMStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[4].Notes"):
                                //HOST ROM
                                HPServer.HostRomNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[4].Status"):
                                //HOST ROM
                                HPServer.HostRomStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[5].Notes"):
                                //SUPPORTED HOST
                                HPServer.SupportedHostNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[5].Status"):
                                //SUPPORTED HOST
                                HPServer.SupportedHostStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[6].Notes"):
                                //POWER MANAGEMENT CONTROLLER
                                HPServer.PowerManagementControllerNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[6].Status"):
                                //POWER MANAGEMENT CONTROLLER
                                HPServer.PowerManagementControllerStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[7].Notes"):
                                //CPLDPAL0
                                HPServer.CPLDPAL0Notes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[7].Status"):
                                //CPLDPAL0
                                HPServer.CPLDPAL0Status = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[8].Notes"):
                                //CPLDPAL1
                                HPServer.CPLDPAL1Notes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hp.iLOSelfTestResults[8].Status"):
                                //CPLDPAL1
                                HPServer.CPLDPAL1Status = keyvalue.Value.Trim();
                                break;
                            //Gen 10
                            case "Oem.Hpe.License.LicenseString":
                                //Ilo License Shows under Firmware Levels
                                HPServer.ILOLicense = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[0].Notes"):
                                //NVRAM DATA
                                HPServer.NVRAMDataNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[0].Status"):
                                //NVRAM DATA
                                HPServer.NVRAMDataStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[1].Notes"):
                                //NVRAM SPACE
                                HPServer.NVRAMSpaceNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[1].Status"):
                                //NVRAM SPACE
                                HPServer.NVRAMSpaceStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[2].Notes"):
                                //SD CARD
                                HPServer.EmbeddedFlashSDCardNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[2].Status"):
                                //SD CARD
                                HPServer.EmbeddedFlashSDCardStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[3].Notes"):
                                //EPROM
                                HPServer.EEPROMNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[3].Status"):
                                //EPROM
                                HPServer.EEPROMStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[4].Notes"):
                                //HOST ROM
                                HPServer.HostRomNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[4].Status"):
                                //HOST ROM
                                HPServer.HostRomStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[5].Notes"):
                                //SUPPORTED HOST
                                HPServer.SupportedHostNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[5].Status"):
                                //SUPPORTED HOST
                                HPServer.SupportedHostStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[6].Notes"):
                                //POWER MANAGEMENT CONTROLLER
                                HPServer.PowerManagementControllerNotes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[6].Status"):
                                //POWER MANAGEMENT CONTROLLER
                                HPServer.PowerManagementControllerStatus = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[7].Notes"):
                                //CPLDPAL0
                                HPServer.CPLDPAL0Notes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[7].Status"):
                                //CPLDPAL0
                                HPServer.CPLDPAL0Status = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[8].Notes"):
                                //CPLDPAL1
                                HPServer.CPLDPAL1Notes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[8].Status"):
                                //CPLDPAL1
                                HPServer.CPLDPAL1Status = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[9].Notes"):
                                //CPLDPAL1
                                HPServer.CPLDPAL1Notes = keyvalue.Value.Trim();
                                break;
                            case var s when keyvalue.Key.Contains("Oem.Hpe.iLOSelfTestResults[9].Status"):
                                //CPLDPAL1
                                HPServer.CPLDPAL1Status = keyvalue.Value.Trim();
                                break;
                            case "FirmwareVersion":
                                //ILO
                                HPServer.iLOVersion = keyvalue.Value.Trim();
                                break;

                        }


                    }



                    //CPU  
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/Processors/Members/$entity") || itm.oDataContext.Contains("/redfish/v1/$metadata#ProcessorCollection.ProcessorCollection"))
                    {

                        switch (keyvalue.Key)
                        {
                            //GEN8 GEN9 OLD
                            case "Model":
                                //Create CPU Object
                                cpuHealth = new HealthCPUHP();
                                cpuHealth.Name = keyvalue.Value.Trim();
                                break;
                            case "Socket":
                                cpuHealth.Label = keyvalue.Value;
                                break;
                            case "Status.Health":
                                cpuHealth.Status = keyvalue.Value;

                                break;
                            case "Oem.Hp.RatedSpeedMHz":
                                cpuHealth.Speed = keyvalue.Value + "MHz";
                                break;
                            case "Oem.Hpe.RatedSpeedMHz":
                                cpuHealth.Speed = keyvalue.Value + "MHz";
                                break;

                            case "Oem.Hp.Characteristics[0]":
                                cpuHealth.MemoryTech = keyvalue.Value;
                                break;
                            case "Oem.Hpe.Characteristics[0]":
                                cpuHealth.MemoryTech = keyvalue.Value;
                                break;

                            case "Oem.Hp.Cache[0].InstalledSizeKB":
                                cpuHealth.L1Cache = keyvalue.Value + " KB";
                                //MessageBox.Show(cpuHealth.L1Cache);
                                break;
                            case "Oem.Hpe.Cache[0].InstalledSizeKB":
                                cpuHealth.L1Cache = keyvalue.Value + " KB";
                                //MessageBox.Show(cpuHealth.L1Cache);
                                break;
                            case "Oem.Hp.Cache[1].InstalledSizeKB":
                                cpuHealth.L2Cache = keyvalue.Value + " KB";
                                //MessageBox.Show(cpuHealth.L2Cache);
                                break;
                            case "Oem.Hpe.Cache[1].InstalledSizeKB":
                                cpuHealth.L2Cache = keyvalue.Value + " KB";
                                //MessageBox.Show(cpuHealth.L2Cache);
                                break;
                            case "Oem.Hp.Cache[2].InstalledSizeKB":
                                cpuHealth.L3Cache = keyvalue.Value + " KB";
                                //MessageBox.Show(cpuHealth.L3Cache);
                                //Add CPU Objects

                                break;
                            case "Oem.Hpe.Cache[2].InstalledSizeKB":
                                cpuHealth.L3Cache = keyvalue.Value + " KB";
                                //MessageBox.Show(cpuHealth.L3Cache);
                                //Add CPU Objects

                                break;
                            case "TotalCores":
                                cpuHealth.ExecutionTech = keyvalue.Value + " Cores; ";

                                break;
                            case "TotalThreads":
                                cpuHealth.ExecutionTech += keyvalue.Value + " Threads";
                                HPServer.CPUInfo.Add(cpuHealth);
                                break;

                        }



                        //GEN10 CPU Processor Collection
                        if (keyvalue.Key.Contains("Members["))
                        {

                            switch (keyvalue.Key)
                            {


                                case var s when keyvalue.Key.Contains("].Id"):
                                    //Create CPU Object
                                    cpuHealth = new HealthCPUHP();
                                    //MemoryHealth.Socket = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Socket = " + MemoryHealth.Socket + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Model"):

                                    cpuHealth.Name = keyvalue.Value.Trim();
                                    break;
                                case var s when keyvalue.Key.Contains("].Socket"):
                                    cpuHealth.Label = keyvalue.Value;
                                    break;
                                case var s when keyvalue.Key.Contains("].Status.Health"):
                                    cpuHealth.Status = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hp.RatedSpeedMHz"):
                                    cpuHealth.Speed = keyvalue.Value + "MHz";
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.RatedSpeedMHz"):
                                    cpuHealth.Speed = keyvalue.Value + "MHz";
                                    break;

                                case var s when keyvalue.Key.Contains("].Oem.Hp.Characteristics[0]"):
                                    cpuHealth.MemoryTech = keyvalue.Value;
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.Characteristics[0]"):
                                    cpuHealth.MemoryTech = keyvalue.Value;
                                    break;

                                case var s when keyvalue.Key.Contains("].Oem.Hp.Cache[0].InstalledSizeKB"):
                                    cpuHealth.L1Cache = keyvalue.Value + " KB";
                                    //MessageBox.Show(cpuHealth.L1Cache);
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.Cache[0].InstalledSizeKB"):
                                    cpuHealth.L1Cache = keyvalue.Value + " KB";
                                    //MessageBox.Show(cpuHealth.L1Cache);
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hp.Cache[1].InstalledSizeKB"):
                                    cpuHealth.L2Cache = keyvalue.Value + " KB";
                                    //MessageBox.Show(cpuHealth.L2Cache);
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.Cache[1].InstalledSizeKB"):
                                    cpuHealth.L2Cache = keyvalue.Value + " KB";
                                    //MessageBox.Show(cpuHealth.L2Cache);
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hp.Cache[2].InstalledSizeKB"):
                                    cpuHealth.L3Cache = keyvalue.Value + " KB";
                                    //MessageBox.Show(cpuHealth.L3Cache);
                                    //Add CPU Objects

                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.Cache[2].InstalledSizeKB"):
                                    cpuHealth.L3Cache = keyvalue.Value + " KB";
                                    //MessageBox.Show(cpuHealth.L3Cache);
                                    //Add CPU Objects

                                    break;
                                case var s when keyvalue.Key.Contains("].TotalCores"):
                                    cpuHealth.ExecutionTech = keyvalue.Value + " Cores; ";

                                    break;
                                case var s when keyvalue.Key.Contains("].TotalThreads"):
                                    cpuHealth.ExecutionTech += keyvalue.Value + " Threads";
                                    HPServer.CPUInfo.Add(cpuHealth);
                                    break;

                            }

                        }
                    }

                    //****MEMORY*****
                    //GEN 10 MEMORY
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#MemoryCollection.MemoryCollection"))
                    {
                        if (keyvalue.Key.Contains("Members["))
                        {

                            switch (keyvalue.Key)
                            {


                                case var s when keyvalue.Key.Contains("].Id"):
                                    MemoryHealth = new HealthMemoryHP();
                                    MemoryHealth.Socket = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Socket = " + MemoryHealth.Socket + "\n";
                                    break;

                                case var s when keyvalue.Key.Contains("].Oem.Hpe.Attributes[0]"):
                                    MemoryHealth.HPSmartMemory = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "HPSmartMemory = " + MemoryHealth.HPSmartMemory + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].PartNumber"):
                                    MemoryHealth.PartNo = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Part No = " + MemoryHealth.PartNo + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].MemoryDeviceType"):
                                    MemoryHealth.MemType = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "MemType = " + MemoryHealth.MemType + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].CapacityMiB"):
                                    MemoryHealth.Size = keyvalue.Value;
                                    //Add total dimm count and total memory size here
                                    dimmCount += 1;
                                    totalMem += Convert.ToDouble(keyvalue.Value);
                                    //ReturnedRespTXT.Text += "Size = " + MemoryHealth.Size + "\n";

                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.MaxOperatingSpeedMTs"):

                                    MemoryHealth.Frequency = keyvalue.Value + " MHz";
                                    //ReturnedRespTXT.Text += "Frequency = " + MemoryHealth.Frequency+ "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.MinimumVoltageVoltsX10"):

                                    //Divide total by 10 to get true value
                                    double minVoltage = Convert.ToDouble(keyvalue.Value) / 10.00;
                                    MemoryHealth.MinVoltage = minVoltage.ToString() + " v";
                                    //ReturnedRespTXT.Text += "MinVoltage = " + MemoryHealth.MinVoltage + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].RankCount"):

                                    MemoryHealth.Ranks = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Ranks = " + MemoryHealth.Ranks + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.BaseModuleType"):

                                    MemoryHealth.Technology = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Technology = " + MemoryHealth.Technology + "\n";

                                    break;
                                case var s when keyvalue.Key.Contains("].MemoryLocation.Slot"):
                                    MemoryHealth.Label = keyvalue.Value;

                                    //ReturnedRespTXT.Text += "Label = " + MemoryHealth.Label + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Status.Health"):
                                    //New memory object
                                    MemoryHealth.Status = keyvalue.Value;
                                    //Assign to Frontpage
                                    HPServer.MemoryHealth = MemoryHealth.Status;

                                    //Add Memory Object
                                    HPServer.MemoryInfo.Add(MemoryHealth);

                                    break;
                            }



                        }

                    }

                    //GEN8 9 MEMORY 
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/Memory/Members/$entity"))
                    {


                        switch (keyvalue.Key)
                        {
                            case "DIMMStatus":
                                //New memory object
                                MemoryHealth = new HealthMemoryHP();
                                MemoryHealth.Status = keyvalue.Value;
                                //ReturnedRespTXT.Text += "Status = " + MemoryHealth.Status + "\n";
                                break;

                            case "Id":
                                MemoryHealth.Socket = keyvalue.Value;
                                //ReturnedRespTXT.Text += "Socket = " + MemoryHealth.Socket + "\n";
                                break;

                            case "HPMemoryType":
                                MemoryHealth.HPSmartMemory = keyvalue.Value;
                                //ReturnedRespTXT.Text += "HPSmartMemory = " + MemoryHealth.HPSmartMemory + "\n";
                                break;
                            case "PartNumber":
                                MemoryHealth.PartNo = keyvalue.Value;
                                //ReturnedRespTXT.Text += "Part No = " + MemoryHealth.PartNo + "\n";
                                break;
                            case "DIMMType":
                                MemoryHealth.MemType = keyvalue.Value;
                                //ReturnedRespTXT.Text += "MemType = " + MemoryHealth.MemType + "\n";
                                break;
                            case "SizeMB":
                                MemoryHealth.Size = keyvalue.Value;
                                //Add total dimm count and total memory size here
                                dimmCount += 1;
                                totalMem += Convert.ToDouble(keyvalue.Value);
                                //ReturnedRespTXT.Text += "Size = " + MemoryHealth.Size + "\n";

                                break;
                            case "MaximumFrequencyMHz":
                                MemoryHealth.Frequency = keyvalue.Value + " MHz";
                                //ReturnedRespTXT.Text += "Frequency = " + MemoryHealth.Frequency+ "\n";
                                break;
                            case "MinimumVoltageVoltsX10":
                                //Divide total by 10 to get true value
                                double minVoltage = Convert.ToDouble(keyvalue.Value) / 10.00;
                                MemoryHealth.MinVoltage = minVoltage.ToString() + " v";
                                //ReturnedRespTXT.Text += "MinVoltage = " + MemoryHealth.MinVoltage + "\n";
                                break;
                            case "Rank":
                                MemoryHealth.Ranks = keyvalue.Value;
                                //ReturnedRespTXT.Text += "Ranks = " + MemoryHealth.Ranks + "\n";
                                break;
                            case "DIMMTechnology":
                                MemoryHealth.Technology = keyvalue.Value;
                                //ReturnedRespTXT.Text += "Technology = " + MemoryHealth.Technology + "\n";

                                break;
                            case "SocketLocator":

                                MemoryHealth.Label = keyvalue.Value;

                                //Add Memory Object
                                HPServer.MemoryInfo.Add(MemoryHealth);
                                //ReturnedRespTXT.Text += "Label = " + MemoryHealth.Label + "\n";
                                break;
                        }


                    }




                    //******** POWER ***********
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Chassis/Members/1/Power$entity") || itm.oDataContext.Contains("/redfish/v1/$metadata#Power.Power"))
                    {
                        //******** Power Supplies ***********
                        //Collection is embedded instead of separate uri links, need to handle differently
                        if (keyvalue.Key.Contains("PowerSupplies["))
                        {
                            switch (keyvalue.Key)
                            {
                                case var s when keyvalue.Key.Contains("].FirmwareVersion"):
                                    //New Power object
                                    PowerHealth = new HealthPowerHP();
                                    PowerHealth.Firmware = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + PowerHealth.Speed + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Model"):
                                    PowerHealth.Model = keyvalue.Value;
                                    PowerHealth.PDS = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + PowerHealth.Label + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hp.BayNumber"):
                                    PowerHealth.Label = "Power Supply " + keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.BayNumber"):
                                    PowerHealth.Label = "Power Supply " + keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Status.Health"):
                                    PowerHealth.Status = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hp.HotplugCapable"):
                                    PowerHealth.HotPluggable = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.HotplugCapable"):
                                    PowerHealth.HotPluggable = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].SerialNumber"):
                                    PowerHealth.Serial = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].SparePartNumber"):
                                    PowerHealth.Spare = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].PowerCapacityWatts"):
                                    PowerHealth.Capacity = keyvalue.Value + " Watts";
                                    //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Status.State"):
                                    PowerHealth.Present = keyvalue.Value;
                                    //Add Power Objects
                                    HPServer.PowerInfo.Add(PowerHealth);
                                    //ReturnedRespTXT.Text += "Label = " + PowerHealth.Zone + "\n";
                                    break;
                            }

                        }


                    }

                    //******** GEN10 THERMAL  FANS & TEMPS ***********
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Thermal.Thermal"))
                    {

                        //******** FANS ***********
                        //Collection is embedded instead of separate uri links, need to handle differently
                        if (keyvalue.Key.Contains("Fans["))
                        {

                            switch (keyvalue.Key)
                            {
                                case var s when keyvalue.Key.Contains("].Name"):
                                    //New fan object
                                    FanHealth = new HealthFansHP();
                                    FanHealth.Label = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + FanHealth.Label + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hp.Location"):
                                    FanHealth.Zone = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + FanHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.Location"):
                                    FanHealth.Zone = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + FanHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].ReadingUnits"):

                                    //FanHealth.Speed = keyvalue.Value + "%";
                                    //ReturnedRespTXT.Text += "Label = " + FanHealth.Speed + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Reading"):

                                    FanHealth.Speed = keyvalue.Value + "%";
                                    //ReturnedRespTXT.Text += "Label = " + FanHealth.Speed + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Status.Health"):
                                    FanHealth.Status = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Status = " + FanHealth.Status + "\n\n";
                                    //Add Fan Objects
                                    HPServer.FanInfo.Add(FanHealth);
                                    break;
                            }


                        }

                        //******** TEMPERATURE ***********
                        //Collection is embedded instead of separate uri links, need to handle differently
                        if (keyvalue.Key.Contains("Temperatures["))
                        {

                            switch (keyvalue.Key)
                            {
                                case var s when keyvalue.Key.Contains("].Name"):
                                    //New Temp object
                                    TempHealth = new HealthTemperatureHP();
                                    TempHealth.Label = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + TempHealth.Label + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].ReadingCelsius"):

                                    TempHealth.CurrentReading = keyvalue.Value + " C";
                                    //ReturnedRespTXT.Text += "Label = " + TempHealth.Speed + "\n";
                                    break;

                                case var s when keyvalue.Key.Contains("].PhysicalContext"):
                                    TempHealth.Location = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + TempHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Status.Health"):
                                    TempHealth.Status = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Status = " + TempHealth.Status + "\n\n";

                                    break;
                                case var s when keyvalue.Key.Contains("].UpperThresholdCritical"):
                                    TempHealth.Caution = keyvalue.Value + " C";
                                    //ReturnedRespTXT.Text += "Status = " + TempHealth.Status + "\n\n";

                                    break;
                                case var s when keyvalue.Key.Contains("].UpperThresholdFatal"):
                                    TempHealth.Critical = keyvalue.Value + " C";
                                    //ReturnedRespTXT.Text += "Status = " + TempHealth.Status + "\n\n";
                                    //Add Temp Objects
                                    HPServer.TemperatureInfo.Add(TempHealth);
                                    break;
                            }


                        }


                    }


                    //******** GEN8,9 THERMAL  FANS & TEMPS ***********
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Chassis/Members/1/Thermal$entity"))
                    {

                        //******** FANS ***********
                        //Collection is embedded instead of separate uri links, need to handle differently
                        if (keyvalue.Key.Contains("Fans["))
                        {

                            switch (keyvalue.Key)
                            {

                                case var s when keyvalue.Key.Contains("].CurrentReading"):
                                    //New fan object
                                    FanHealth = new HealthFansHP();
                                    FanHealth.Speed = keyvalue.Value + "%";
                                    //ReturnedRespTXT.Text += "Label = " + FanHealth.Speed + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].FanName"):

                                    FanHealth.Label = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + FanHealth.Label + "\n";
                                    break;

                                case var s when keyvalue.Key.Contains("].Oem.Hp.Location"):
                                    FanHealth.Zone = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + FanHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.Location"):
                                    FanHealth.Zone = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + FanHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Status.Health"):
                                    FanHealth.Status = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Status = " + FanHealth.Status + "\n\n";
                                    //Add Fan Objects
                                    HPServer.FanInfo.Add(FanHealth);
                                    break;
                            }


                        }

                        //******** TEMPERATURE ***********
                        //Collection is embedded instead of separate uri links, need to handle differently
                        if (keyvalue.Key.Contains("Temperatures["))
                        {

                            switch (keyvalue.Key)
                            {
                                case var s when keyvalue.Key.Contains("].Name"):
                                    //New Temp object
                                    TempHealth = new HealthTemperatureHP();
                                    TempHealth.Label = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + TempHealth.Label + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].CurrentReading"):

                                    TempHealth.CurrentReading = keyvalue.Value + " C";
                                    //ReturnedRespTXT.Text += "Label = " + TempHealth.Speed + "\n";
                                    break;

                                case var s when keyvalue.Key.Contains("].PhysicalContext"):
                                    TempHealth.Location = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + TempHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Status.Health"):
                                    TempHealth.Status = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Status = " + TempHealth.Status + "\n\n";

                                    break;
                                case var s when keyvalue.Key.Contains("].UpperThresholdCritical"):
                                    TempHealth.Caution = keyvalue.Value + " C";
                                    //ReturnedRespTXT.Text += "Status = " + TempHealth.Status + "\n\n";

                                    break;
                                case var s when keyvalue.Key.Contains("].UpperThresholdFatal"):
                                    TempHealth.Critical = keyvalue.Value + " C";
                                    //ReturnedRespTXT.Text += "Status = " + TempHealth.Status + "\n\n";
                                    //Add Temp Objects
                                    HPServer.TemperatureInfo.Add(TempHealth);
                                    break;
                            }


                        }


                    }







                    //******** GEN8 GEN9 INTERNAL NETWORK CARDS *********** 
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Managers/Members/1/EthernetInterfaces") || itm.oDataContext.Contains("/redfish/v1/$metadata#Managers/Members/1/EthernetInterfaces/Members/$entity"))
                    {
                        //******** Network Supplies ***********
                        //Collection is embedded instead of separate uri links, need to handle differently
                        if (keyvalue.Key.Contains("Items["))
                        {
                            switch (keyvalue.Key)
                            {


                                case var s when keyvalue.Key.Contains("].FactoryMacAddress"):
                                    //MessageBox.Show("New Instance");
                                    //New Network object
                                    NetworkHealth = new HealthNetworkHP();
                                    NetworkHealth.MacAddress = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].IPv4Addresses"):

                                    if (keyvalue.Key.Contains("].AddressOrigin") || keyvalue.Key.Contains("].Gateway") || keyvalue.Key.Contains("].SubnetMask"))
                                    {
                                        //skip as wrong value
                                        //MessageBox.Show("Skipped");
                                    }
                                    else
                                    {

                                        //Add ip address
                                        NetworkHealth.IPAddress = keyvalue.Value;
                                    }

                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;
                                //case "Id":
                                case var s when keyvalue.Key.Contains("].Id"):
                                    //MessageBox.Show("Port" + keyvalue.Value);
                                    NetworkHealth.NetworkPort = "Port " + keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Speed + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Type "):
                                    NetworkHealth.PortDescription = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Label + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Name"):
                                    if (keyvalue.Key.Contains("].NameServers"))
                                    {
                                        //skip as wrong value
                                        //MessageBox.Show("Skipped");
                                    }
                                    else
                                    {

                                        //Add ip address
                                        NetworkHealth.Location = "Internal " + keyvalue.Value;
                                    }

                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;

                                case var s when keyvalue.Key.Contains("].Status.Health"):

                                    if (string.IsNullOrEmpty(keyvalue.Value))
                                    {
                                        NetworkHealth.Status = "Unknown";
                                    }
                                    else
                                    {
                                        NetworkHealth.Status = keyvalue.Value;
                                    }
                                    //Add Network Objects
                                    //MessageBox.Show("Added");
                                    HPServer.NICInfo.Add(NetworkHealth);
                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n"; 
                                    break;



                            }

                        }


                    }

                    //******** GEN8 GEN9  ADDITIONAL NETWORK CARDS ***********
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/1/NetworkAdapters/Members/$entity"))
                    {

                        if (keyvalue.Key == "Name")
                        {
                            //Set string as model only appears once
                            networkCard = keyvalue.Value;
                        }


                        //******** Network Supplies ***********
                        //Collection is embedded instead of separate uri links, need to handle differently
                        if (keyvalue.Key.Contains("PhysicalPorts["))
                        {
                            //MessageBox.Show("Yep Here");
                            switch (keyvalue.Key)
                            {

                                case var s when keyvalue.Key.Contains("].FullDuplex"):
                                    //New Network object
                                    NetworkHealth = new HealthNetworkHP();
                                    //Increment the network card count
                                    networkCount += 1;

                                    NetworkHealth.NetworkPort = "Port " + networkCount.ToString();
                                    NetworkHealth.Location = "Internal Card";
                                    //***** PORT DESCRIPTION FIELD ********
                                    //Add Network Card Model to Port Description Field
                                    NetworkHealth.PortDescription = networkCard;
                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].IPv4Addresses"):


                                    //Add ip address
                                    NetworkHealth.IPAddress = keyvalue.Value;

                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;

                                case var s when keyvalue.Key.Contains("].MacAddress"):
                                    NetworkHealth.MacAddress = keyvalue.Value;

                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].UEFIDevicePath"):
                                    NetworkHealth.Status = "Unknown";
                                    //Add Network Objects
                                    HPServer.NICInfo.Add(NetworkHealth);
                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;


                            }

                        }
                    }


                    //GEN 10 Internal Server Ports Ethernet Interfaces 
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#EthernetInterfaceCollection.EthernetInterfaceCollection"))
                    {

                        //******** Network Supplies ***********
                        //Collection is embedded instead of separate uri links, need to handle differently

                        if (keyvalue.Key.Contains("Members["))
                        {
                            switch (keyvalue.Key)
                            {

                                //OTHER PROPERTIES
                                case var s when keyvalue.Key.Contains("].Id"):
                                    //New Network object
                                    NetworkHealth = new HealthNetworkHP();

                                    //Add port number
                                    NetworkHealth.NetworkPort = "Port " + keyvalue.Value;
                                    NetworkHealth.Location = "Embedded Card";

                                    break;
                                case var s when keyvalue.Key.Contains("].IPv4Addresses[0].Address"):
                                    //Workaround any duplicate contains and skip
                                    if (!keyvalue.Key.Contains("].IPv4Addresses[0].AddressOrigin"))
                                    {
                                        //Set IP
                                        NetworkHealth.IPAddress = keyvalue.Value;
                                    }

                                    break;
                                case var s when keyvalue.Key.Contains("].PermanentMACAddress"):

                                    NetworkHealth.MacAddress = keyvalue.Value;

                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n"; 
                                    break;

                                case var s when keyvalue.Key.Contains("].Name"):
                                    if (!keyvalue.Key.Contains("].NameServers"))
                                    {
                                        //Add port description
                                        NetworkHealth.PortDescription = keyvalue.Value;
                                    }

                                    break;
                                case var s when keyvalue.Key.Contains("].SpeedMbps"):

                                    // NO BACKING PROP
                                    break;
                                case var s when keyvalue.Key.Contains("].FullDuplex"):

                                    // NO BACKING PROP
                                    break;
                                case var s when keyvalue.Key.Contains("].LinkStatus"):

                                    // NO BACKING PROP
                                    break;

                                case var s when keyvalue.Key.Contains("].Status.Health"):

                                    NetworkHealth.Status = keyvalue.Value;
                                    //Add Network Objects
                                    HPServer.NICInfo.Add(NetworkHealth);
                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;

                            }

                        }

                    }

                    //GEN 10 Additional Card Network Adapters   
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#NetworkAdapterCollection.NetworkAdapterCollection"))
                    {
                        //Collection is embedded instead of separate uri links, need to handle differently


                        if (keyvalue.Key.Contains("Members["))
                        {
                            switch (keyvalue.Key)
                            {

                                //OTHER PROPERTIES ].Oem.Hpe.LldpData.Transmitting.ManagementAddresses[0].ManagementIPAddress
                                case var s when keyvalue.Key.Contains("].Id"):
                                    //MessageBox.Show("New");
                                    //New Network object
                                    NetworkHealth = new HealthNetworkHP();
                                    //Increment the network card count
                                    networkCount += 1;

                                    NetworkHealth.NetworkPort = "Port " + networkCount.ToString();
                                    NetworkHealth.Location = keyvalue.Value;
                                    break;
                                case var s when keyvalue.Key.Contains("].Model"):

                                    //***** PORT DESCRIPTION FIELD ********
                                    //Add Network Card Model to Port Description Field
                                    NetworkHealth.PortDescription = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Location"):

                                    //***** PORT DESCRIPTION FIELD ********
                                    //Add Network Card Model to Port Description Field
                                    NetworkHealth.PortDescription = keyvalue.Value;
                                    NetworkHealth.Location = keyvalue.Value;
                                    break;
                                case var s when keyvalue.Key.Contains("].Name"):
                                    //MessageBox.Show(keyvalue.Value);
                                    //***** PORT DESCRIPTION FIELD ********
                                    //Add Network Card Model to Port Description Field
                                    NetworkHealth.PortDescription = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.LldpData.Receiving.ManagementAddress"):

                                    NetworkHealth.MacAddress = keyvalue.Value;

                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].SerialNumber"):

                                    //MessageBox.Show("ADD");
                                    //Add Network Objects
                                    HPServer.NICInfo.Add(NetworkHealth);
                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;


                            }

                        }

                    }

                    //GEN 10 Base Additional Card Network Adapters   
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#HpeBaseNetworkAdapterCollection.HpeBaseNetworkAdapterCollection"))
                    {
                        //Collection is embedded instead of separate uri links, need to handle differently


                        if (keyvalue.Key.Contains("Members["))
                        {
                            switch (keyvalue.Key)
                            {

                                //OTHER PROPERTIES ].Oem.Hpe.LldpData.Transmitting.ManagementAddresses[0].ManagementIPAddress
                                case var s when keyvalue.Key.Contains("].Id"):
                                    //MessageBox.Show("New");
                                    //New Network object
                                    NetworkHealth = new HealthNetworkHP();
                                    //Increment the network card count
                                    networkCount += 1;

                                    NetworkHealth.NetworkPort = "Card " + networkCount.ToString();
                                    NetworkHealth.Location = keyvalue.Value;
                                    break;
                                case var s when keyvalue.Key.Contains("].Model"):

                                    //***** PORT DESCRIPTION FIELD ********
                                    //Add Network Card Model to Port Description Field
                                    NetworkHealth.PortDescription = keyvalue.Value;
                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Location"):

                                    //***** PORT DESCRIPTION FIELD ********
                                    //Add Network Card Model to Port Description Field
                                    NetworkHealth.PortDescription = keyvalue.Value;
                                    NetworkHealth.Location = keyvalue.Value;
                                    break;
                                case var s when keyvalue.Key.Contains("].Name"):
                                    //MessageBox.Show(keyvalue.Value);
                                    //***** PORT DESCRIPTION FIELD ********
                                    //Add Network Card Model to Port Description Field
                                    if (keyvalue.Key.Contains("PhysicalPorts"))
                                    {
                                        //Skip
                                    }
                                    else
                                    {
                                        NetworkHealth.PortDescription = keyvalue.Value;
                                    }


                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].Oem.Hpe.LldpData.Receiving.ManagementAddress"):

                                    NetworkHealth.MacAddress = keyvalue.Value;

                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;
                                case var s when keyvalue.Key.Contains("].SerialNumber"):

                                    //MessageBox.Show("ADD");
                                    //Add Network Objects
                                    HPServer.NICInfo.Add(NetworkHealth);
                                    //ReturnedRespTXT.Text += "Label = " + NetworkHealth.Zone + "\n";
                                    break;


                            }

                        }

                    }










                    //HostBusAdapter
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#HpeSmartStorageHostBusAdapter.HpeSmartStorageHostBusAdapter"))
                    {
                        switch (keyvalue.Key)
                        {
                            case "Description":
                                //New Enclosure object
                                //StorageHealth = new DriveEnclosureInfoHP();
                                //Adapter
                                HPServer.StorageInfo.Label = keyvalue.Value;
                                break;
                            case "Status.Health":
                                //Status
                                HPServer.StorageInfo.Status = keyvalue.Value;
                                break;
                            case "SerialNumber":
                                //Serial No
                                HPServer.StorageInfo.Serial = keyvalue.Value;
                                break;
                            case "Location":
                                //Model
                                HPServer.StorageInfo.Model = keyvalue.Value;
                                break;
                            case "FirmwareVersion.Current.VersionString":
                                //Firmware Version
                                HPServer.StorageInfo.Firmware = keyvalue.Value.Trim();
                                break;
                        }
                    }

                    //Gen10 Storage
                    //Controller
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#HpeSmartStorageArrayController.HpeSmartStorageArrayController"))
                    {

                        switch (keyvalue.Key)
                        {
                            case "AdapterType":
                                //New Enclosure object
                                //StorageHealth = new DriveEnclosureInfoHP();
                                //Adapter
                                HPServer.StorageInfo.Label = keyvalue.Value;
                                break;
                            case "Status.Health":
                                //Status
                                HPServer.StorageInfo.Status = keyvalue.Value;
                                break;
                            case "SerialNumber":
                                //Serial No
                                HPServer.StorageInfo.Serial = keyvalue.Value;
                                break;
                            case "Model":
                                //Model
                                HPServer.StorageInfo.Model = keyvalue.Value;
                                break;
                            case "FirmwareVersion.Current.VersionString":
                                //Firmware Version
                                HPServer.StorageInfo.Firmware = keyvalue.Value.Trim();
                                break;
                        }
                    }


                    //Gen10 Enclosure
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#HpeSmartStorageStorageEnclosure.HpeSmartStorageStorageEnclosure"))
                    {

                        //Storage Enclosure
                        switch (keyvalue.Key)
                        {
                            case "DriveBayCount":
                                //New Enclosure object
                                DriveEnclosureInfo = new DriveEnclosureInfoHP();
                                //Drive Bay Count
                                DriveEnclosureInfo.DriveBay = keyvalue.Value;
                                break;
                            case "Location":
                                DriveEnclosureInfo.Label = keyvalue.Value;
                                break;

                            case "Status.Health":
                                //Status
                                DriveEnclosureInfo.Status = keyvalue.Value;
                                //Add enclosure object
                                HPServer.StorageInfo.DriveEnclosure.Add(DriveEnclosureInfo);
                                break;
                        }
                    }

                    //Gen10 Logical Drives
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#HpeSmartStorageLogicalDrive.HpeSmartStorageLogicalDrive"))
                    {

                        switch (keyvalue.Key)
                        {
                            case "CapacityMiB":
                                //New Enclosure object
                                LogicalDriveInfo = new HealthLogicalDriveHP();
                                //Drive Bay Count
                                LogicalDriveInfo.Capacity = (Convert.ToInt32(keyvalue.Value) / 1024).ToString() + "GB";
                                break;
                            case "LogicalDriveNumber":
                                LogicalDriveInfo.Label = keyvalue.Value;
                                break;
                            case "LogicalDriveEncryption":
                                LogicalDriveInfo.Encryption = keyvalue.Value;
                                break;
                            case "LogicalDriveType":
                                LogicalDriveInfo.LogicalDriveType = keyvalue.Value;
                                break;
                            case "Raid":
                                LogicalDriveInfo.FaultTolerance = "Raid " + keyvalue.Value;
                                break;
                            case "Status.Health":
                                //Status
                                LogicalDriveInfo.Status = keyvalue.Value;
                                //Add enclosure object
                                HPServer.StorageInfo.LogicalDrives.Add(LogicalDriveInfo);
                              
                                break;
                        }
                    }

                    //Gen10 Physical
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#HpeSmartStorageDiskDrive.HpeSmartStorageDiskDrive"))
                    {

                        switch (keyvalue.Key)
                        {
                            case "@odata.id":
                                //Important! Capture odata id to identify which disk is in which logical drive
                                PhysicalDrive = new PhysicalDrivesHP();
                                PhysicalDrive.odataId = keyvalue.Value;
                                break;
                            case "CapacityGB":
                                //Drive Bay Count
                                PhysicalDrive.MarketingCapacity = keyvalue.Value + "GB"; //(Convert.ToInt32(keyvalue.Value) / 1024).ToString() + "GB";
                                break;
                            case "CapacityMiB":
                                //Drive Bay Count
                                PhysicalDrive.Capacity = (Convert.ToInt32(keyvalue.Value) / 1024).ToString() + "GB";
                                break;
                            case "EncryptedDrive":
                                //Encryption
                                PhysicalDrive.EncryptionStatus = keyvalue.Value;
                                PhysicalDrive.Encryption = PhysicalDrive.EncryptionStatus;
                                break;
                            case "FirmwareVersion.Current.VersionString":
                                //Firmware
                                PhysicalDrive.FirmwareVersion = keyvalue.Value;

                                break;
                            case "MediaType":
                                //Media Type
                                PhysicalDrive.MediaType = keyvalue.Value;
                                break;
                            case "BlockSize":
                                //Interface
                                PhysicalDrive.BlockSize = keyvalue.Value;
                                break;
                            case "CurrentTemperatureCelsius":
                                //Interface
                                PhysicalDrive.CurrentTemp = keyvalue.Value;
                                break;
                            case "MaximumTemperatureCelsius":
                                //Interface
                                PhysicalDrive.MaxTemp = keyvalue.Value;
                                break;
                            case "RotationalSpeedRpm":
                                //Interface
                                PhysicalDrive.RotationalSpeed = keyvalue.Value;
                                break;
                            case "InterfaceType":
                                //Interface
                                PhysicalDrive.Interface = keyvalue.Value;
                                break;
                            case "InterfaceSpeed":
                                //Interface Speed
                                PhysicalDrive.InterfaceSpeed = keyvalue.Value;
                                break;
                            case "Location":
                                //Location
                                PhysicalDrive.Label = keyvalue.Value;
                                PhysicalDrive.Location = keyvalue.Value;
                                break;
                            case "Model":
                                //Model
                                PhysicalDrive.Model = keyvalue.Value;
                                //PhysicalDriveInfo.Model = keyvalue.Value;
                                break;
                            case "SerialNumber":
                                //Model
                                PhysicalDrive.SerialNumber = keyvalue.Value;
                                PhysicalDrive.SerialNo = keyvalue.Value;
                                break;
                            case "Status.Health":
                                //Model
                                PhysicalDrive.Status = keyvalue.Value;
                                PhysicalDrive.Health = keyvalue.Value;
                                break;

                            case "Status.State":
                                //Assign State Value
                                PhysicalDrive.DriveConfiguration = keyvalue.Value;
                                //Add To PhysicalDrive Collection
                                HPServer.PhysicalDriveInfo.Add(PhysicalDrive);

                                break;
                        }
                    }

                    //Gen10 Logical drive members
                    //for (int i = 1; i < HPServer.StorageInfo.LogicalDrives.Count() + 1; i++)
                    //{
                    //    if (itm.oDataContext.Contains("/redfish/v1/$metadata#HpeSmartStorageDiskDriveCollection.HpeSmartStorageDiskDriveCollection"))
                    //    {
                    //        switch (keyvalue.Key)
                    //        {
                    //            case var s when keyvalue.Key.Contains("Members["):

                    //                //MessageBox.Show(keyvalue.Value + " " + i.ToString());

                    //                foreach (var drive in HPServer.PhysicalDriveInfo)
                    //                {

                    //                    if (drive.odataId == keyvalue.Value)
                    //                    {
                    //                        // Add to separate physical drives only collection
                    //                       HPServer.StorageInfo.LogicalDrives.ElementAt(i - 1).PhysicalDrives.Add(drive);
                    //                    }
                    //                }

                    //                break;
                    //        }


                    //    }
                    //}



                    



                    //GEN8 9 Storage Properties
                    //Controller 
                    for (int j = 0; j < Convert.ToInt32(passedvm.StorageDepth) + 1; j++)
                    {
                        if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/" + j + "/SmartStorage/ArrayControllers/Members/$entity"))
                        {

                            switch (keyvalue.Key)
                            {
                                case "AdapterType":
                                    //New Enclosure object
                                    //StorageHealth = new DriveEnclosureInfoHP();
                                    //Adapter
                                    HPServer.StorageInfo.Label = keyvalue.Value;
                                    break;
                                case "Status.Health":
                                    //Status
                                    HPServer.StorageInfo.Status = keyvalue.Value;
                                    break;
                                case "SerialNumber":
                                    //Serial No
                                    HPServer.StorageInfo.Serial = keyvalue.Value;
                                    break;
                                case "Model":
                                    //Model
                                    HPServer.StorageInfo.Model = keyvalue.Value;
                                    break;
                                case "FirmwareVersion.Current.VersionString":
                                    //Firmware Version
                                    HPServer.StorageInfo.Firmware = keyvalue.Value.Trim();
                                    break;
                            }
                        }
                    }

                    //Enclosure
                    for (int j = 0; j < Convert.ToInt32(passedvm.StorageDepth) + 1; j++)
                    {
                        for (int i = 1; i < Convert.ToInt32(passedvm.StorageDepth) + 1; i++)
                        {
                            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/" + i + "/SmartStorage/ArrayControllers/Members/" + j + "/StorageEnclosures/Members/$entity"))
                            {

                                //Storage Enclosure
                                switch (keyvalue.Key)
                                {
                                    case "DriveBayCount":
                                        //New Enclosure object
                                        DriveEnclosureInfo = new DriveEnclosureInfoHP();
                                        //Drive Bay Count
                                        DriveEnclosureInfo.DriveBay = keyvalue.Value;
                                        break;
                                    case "Location":
                                        DriveEnclosureInfo.Label = keyvalue.Value;
                                        break;

                                    case "Status.Health":
                                        //Status
                                        DriveEnclosureInfo.Status = keyvalue.Value;
                                        //Add enclosure object
                                        HPServer.StorageInfo.DriveEnclosure.Add(DriveEnclosureInfo);
                                        break;
                                }
                            }
                        }
                    }

                    //Logical Drives
                    for (int j = 0; j < Convert.ToInt32(passedvm.StorageDepth) + 1; j++)
                    {
                        for (int i = 1; i < 20 + 1; i++)
                        {
                            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/" + i + "/SmartStorage/ArrayControllers/Members/" + j + "/LogicalDrives/Members/$entity"))
                            {

                                switch (keyvalue.Key)
                                {
                                    case "CapacityMiB":
                                        //New Enclosure object
                                        LogicalDriveInfo = new HealthLogicalDriveHP();
                                        //Drive Bay Count
                                        LogicalDriveInfo.Capacity = (Convert.ToInt32(keyvalue.Value) / 1024).ToString() + "GB";
                                        break;
                                    case "LogicalDriveNumber":
                                        LogicalDriveInfo.Label = keyvalue.Value;
                                        break;
                                    case "LogicalDriveEncryption":
                                        LogicalDriveInfo.Encryption = keyvalue.Value;
                                        break;
                                    case "LogicalDriveType":
                                        LogicalDriveInfo.LogicalDriveType = keyvalue.Value;
                                        break;
                                    case "Raid":
                                        LogicalDriveInfo.FaultTolerance = "Raid " + keyvalue.Value;
                                        break;
                                    case "Status.Health":
                                        //Status
                                        LogicalDriveInfo.Status = keyvalue.Value;
                                        //Add enclosure object
                                        HPServer.StorageInfo.LogicalDrives.Add(LogicalDriveInfo);
                                        break;
                                }
                            }
                        }
                    }


                    //Console.WriteLine("STORAGE DEPTH = " + passedvm.StorageDepth);


                    //Physical Drives
                    for (int j = 0; j < Convert.ToInt32(passedvm.StorageDepth) + 1; j++)
                    {
                        for (int i = 0; i < Convert.ToInt32(passedvm.StorageDepth) + 1; i++)
                        {
                            if (itm.oDataContext.Contains("/redfish/v1/$metadata#Systems/Members/" + j + "/SmartStorage/ArrayControllers/Members/" + i + "/DiskDrives/Members/$entity"))
                            {

                                switch (keyvalue.Key)
                                {
                                    case "@odata.id":
                                        //Important! Capture odata id to identify which disk is in which logical drive
                                        PhysicalDrive = new PhysicalDrivesHP();
                                        PhysicalDrive.odataId = keyvalue.Value;
                                        break;
                                    case "CapacityGB":
                                        //Drive Bay Count
                                        PhysicalDrive.MarketingCapacity = keyvalue.Value + "GB"; //(Convert.ToInt32(keyvalue.Value) / 1024).ToString() + "GB";
                                        break;
                                    case "CapacityMiB":
                                        //Drive Bay Count
                                        PhysicalDrive.Capacity = (Convert.ToInt32(keyvalue.Value) / 1024).ToString() + "GB";
                                        break;
                                    case "EncryptedDrive":
                                        //Encryption
                                        PhysicalDrive.EncryptionStatus = keyvalue.Value;
                                        PhysicalDrive.Encryption = PhysicalDrive.EncryptionStatus;
                                        break;
                                    case "FirmwareVersion.Current.VersionString":
                                        //Firmware
                                        PhysicalDrive.FirmwareVersion = keyvalue.Value;

                                        break;
                                    case "MediaType":
                                        //Media Type
                                        PhysicalDrive.MediaType = keyvalue.Value;
                                        break;
                                    case "BlockSize":
                                        //Interface
                                        PhysicalDrive.BlockSize = keyvalue.Value;
                                        break;
                                    case "CurrentTemperatureCelsius":
                                        //Interface
                                        PhysicalDrive.CurrentTemp = keyvalue.Value;
                                        break;
                                    case "MaximumTemperatureCelsius":
                                        //Interface
                                        PhysicalDrive.MaxTemp = keyvalue.Value;
                                        break;
                                    case "RotationalSpeedRpm":
                                        //Interface
                                        PhysicalDrive.RotationalSpeed = keyvalue.Value;
                                        break;
                                    case "InterfaceType":
                                        //Interface
                                        PhysicalDrive.Interface = keyvalue.Value;
                                        break;
                                    case "InterfaceSpeed":
                                        //Interface Speed
                                        PhysicalDrive.InterfaceSpeed = keyvalue.Value;
                                        break;
                                    case "Location":
                                        //Location
                                        PhysicalDrive.Label = keyvalue.Value;
                                        PhysicalDrive.Location = keyvalue.Value;
                                        break;
                                    case "Model":
                                        //Model
                                        PhysicalDrive.Model = keyvalue.Value;
                                        //PhysicalDriveInfo.Model = keyvalue.Value;
                                        break;
                                    case "SerialNumber":
                                        //Model
                                        PhysicalDrive.SerialNumber = keyvalue.Value;
                                        PhysicalDrive.SerialNo = keyvalue.Value;
                                        break;
                                    case "Status.Health":
                                        //Model
                                        PhysicalDrive.Status = keyvalue.Value;
                                        PhysicalDrive.Health = keyvalue.Value;
                                        break;

                                    case "Status.State":
                                        //Assign State Value
                                        PhysicalDrive.DriveConfiguration = keyvalue.Value;
                                        //Add To PhysicalDrive Collection
                                        HPServer.PhysicalDriveInfo.Add(PhysicalDrive);

                                        break;
                                }
                            }
                        }
                    }


                    //**** IMPORTANT ****
                    // Logical Drive Members
                    // Because a flat data file the logical drives get added first then the physicals, loop through the logical drives   || itm.oDataContext.Contains("/redfish/v1/Systems/1/SmartStorage/ArrayControllers/0/LogicalDrives/1/DataDrives/"
                    //Loop starting at 1 /redfish/v1/$metadata#HpeSmartStorageDiskDriveCollection.HpeSmartStorageDiskDriveCollection

                    //for (int j = 0; j < 30 + 1; j++)
                    //{
                    //for (int i = 1; i < HPServer.StorageInfo.LogicalDrives.Count() + 1; i++)
                    //{
                    //    if (itm.oDataContext.Contains("/LogicalDrives/Members/" + i + "/DataDrives"))
                    //    {
                    //        switch (keyvalue.Key)
                    //        {
                    //            case var s when keyvalue.Key.Contains("Members["):

                    //                //MessageBox.Show(keyvalue.Value + " " + i.ToString());

                    //                foreach (var drive in HPServer.PhysicalDriveInfo)
                    //                {

                    //                    if (drive.odataId == keyvalue.Value)
                    //                    {
                    //                        // Add to separate physical drives only collection
                    //                        HPServer.StorageInfo.LogicalDrives.ElementAt(i - 1).PhysicalDrives.Add(drive);
                    //                    }
                    //                }

                    //                break;
                    //        }


                    //    }
                    //}
                    //}

                 


                } // redfish key value loop



                //Set 100 % Percent done
                // passedvm.ProgressPercentage1 = 100;


            }

            // ******************** END HP DATA SPLIT ******************* 
            ////TEST DATA, Comment out for production

            //for (int i = 0; i < 3; i++)
            //{
            //    HPServer.StorageInfo.LogicalDrives.Add(new HealthLogicalDriveHP("Logical" + i.ToString(), "OK", "243GB", "Raid0", "Data", "False", null)); //"Logical" + i.ToString(),"","","","","", null)
            //}

            //for (int i = 0; i < 10; i++)
            //{
            //    HPServer.PhysicalDriveInfo.Add(new PhysicalDrivesHP("HDD", "OK", "1111", "WD", "243GB", "249GB", "1", "4.2", "Raid0", "none", "SSD"));
            //}

            //// END TEST DATA

            //*********    HEALTH AGREGGATE       *************
            int populated = 0;
            //Run Custom Health AggregateHealthStatus
            if (ServerGen == "Gen8" || ServerGen == "Gen9")
            {
                //HEALTH ROLL UP 
                //Bios
                HPServer.BiosHardwareStatus = HPServer.HostRomStatus;
                //Cpu
                HPServer.ProcessorStatus = HPServer.CPUHealth;
                //Memory Health Roll Up
                HPServer.MemoryStatus = HPServer.MemoryHealth;
                //Storage Health Roll Up
                HPServer.StorageStatus = HPServer.StorageInfo.Status;
                //Network Health at a glance
                var NetworkHealthRollUp = HPServer.NICInfo.Where(e => e.Status != "OK" || e.Status != "Unknown").ToList();

                //Check if different value found, if so display it
                if (NetworkHealthRollUp.Count > 0)
                {
                    foreach (var item in NetworkHealthRollUp)
                    {
                        if (item.Status == "Unknown")
                        {
                            //if blank, set as ok
                            HPServer.NetworkStatus = "OK";
                        }
                        else
                        {
                            HPServer.NetworkStatus = item.Status;
                        }

                    }
                }
                else
                {
                    HPServer.NetworkStatus = "OK";

                }


                //Power Health at a glance
                var PowerHealthRollUp = HPServer.PowerInfo.Where(e => e.Status != "OK").ToList();
                //Check if different value found, if so display it
                if (PowerHealthRollUp.Count > 0)
                {
                    foreach (var item in PowerHealthRollUp)
                    {
                        HPServer.PowerState = item.Status;
                        HPServer.PowerSupplyRedundancy = "Not Redundant";
                        HPServer.PowerSuppliesStatus = item.Status;
                    }
                }
                else
                {
                    HPServer.PowerState = "OK";
                    HPServer.PowerSupplyRedundancy = "Redundant";
                    HPServer.PowerSuppliesStatus = "OK";
                }


                //Fans Health at a glance
                var FansHealthRollUp = HPServer.FanInfo.Where(e => e.Status != "OK").ToList();
                //Check if different value found, if so display it
                if (FansHealthRollUp.Count > 0)
                {
                    foreach (var item in FansHealthRollUp)
                    {
                        HPServer.FansRedundancy = "Not Redundant";
                        HPServer.FansStatus = item.Status;
                    }
                }
                else
                {
                    HPServer.FansStatus = "OK";
                    HPServer.FansRedundancy = "Redundant";
                }

                //Temperature Health at a glance
                var TemperatureHealthRollUp = HPServer.TemperatureInfo.Where(e => e.Status != "OK" || e.Status != "").ToList();

                //MessageBox.Show(TemperatureHealthRollUp.Count.ToString());
                //Check if different value found, if so display it
                if (TemperatureHealthRollUp.Count > 0)
                {
                    foreach (var item in TemperatureHealthRollUp)
                    {
                        if (item.Status == "")
                        {
                            //if blank, set as ok
                            HPServer.TemperatureStatus = "OK";
                        }
                        else
                        {
                            HPServer.TemperatureStatus = item.Status;
                        }

                    }
                }
                else
                {
                    HPServer.TemperatureStatus = "OK";

                }




                //For non collection properties
                HPServer.PopulatedSlots = dimmCount.ToString();
                HPServer.TotalMemory = (totalMem / 1024).ToString();
            }
            else
            {


                //For non collection properties
                foreach (var dimm in HPServer.MemoryInfo)
                {

                    if (string.IsNullOrEmpty(dimm.MemType))
                    {

                    }
                    else
                    {
                        //Add to count
                        populated += 1;
                    }


                }
                //For non collection properties
                HPServer.PopulatedSlots = populated.ToString(); //dimmCount.ToString();
                HPServer.TotalMemory = (totalMem / 1024).ToString();
            }





            //Clear EntireServer collection after Server Data Was Captured
            EntireServer.RedfishSection.Clear();


            // MessageBox.Show(HPServer.TpmVisibility + "/" + HPServer.TpmType + "/" + HPServer.TpmState);


            return HPServer;
        }


        public async Task WaitForText(string texttocheck, int millSecs, string sourcetext)
        {
            //call by following
            // await WaitForText("main memory", 1000, "json text");

            do
            {

                if (sourcetext.Contains(texttocheck))
                {


                    break;
                }
                else
                {

                    // Console.WriteLine("Waiting For " + texttocheck);

                    await Task.Delay(millSecs);



                }

            } while (true);

        }


        public async Task WaitForTasksToFinish(int millSecs, string property2check, string ServerIP, string PasswordOverride, MainViewModel passedvm, string ServerGen)
        {
            //call by following
            // await WaitForText("main memory", 1000, "json text");

            //string taskCount = "0"; ?$select=Members@odata.count
            string jsonvaluetocheck = "";

            do
            {
               // Console.WriteLine("root:" + PasswordOverride.Trim());

                await RedfishRestCall("https://" + ServerIP + "/redfish/v1/TaskService/Tasks/?$select=Members@odata.count", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);


                await ReadRedfishRoot(jsonReturned, "", "Dell");


                //OUT PUT SERVER DATA
                foreach (var itm in EntireServer.RedfishSection)
                {

                    //passedvm.ILORestOutput1 += itm.oDataContext + "\n";
                    //Console.WriteLine("\n" + itm.oDataContext + "");
                    //Loop through all key value pairs in Entire Server Object
                    foreach (var keyvalue in itm.RedfishPairs)
                    {

                        if (keyvalue.Key == "Members@odata.count")
                        {
                            jsonvaluetocheck = keyvalue.Value;
                        }
                        //Console.WriteLine("\n" + keyvalue.Key + " = " + keyvalue.Value);
                    }

                }

                //Check if count there
                if (property2check == jsonvaluetocheck)
                {
                    //return jsonvaluetocheck;

                    //Console.WriteLine("BROKEN OUT OF JSON CHECK " + property2check + " = " + jsonvaluetocheck);

                    break;
                }
                else
                {
                    //Check if Script is Cancelled
                    passedvm.CancelServerScript("S1");
                    //Break out of queue loop if one of these conditions are true
                    if (passedvm.CancelScriptS1 == true || passedvm.ILORestOutput1.Contains("Response status code does not indicate success: 404 (Not Found).") || passedvm.ILORestOutput1.Contains("Response status code does not indicate success: 406 (Not Acceptable).") || passedvm.ILORestOutput1.Contains("Response status code does not indicate success: 500 (Internal Server Error)."))
                    {
                        //Exit out of loop
                        break;
                    }


                    //Wait 10 seconds and run again
                    await Task.Delay(millSecs);
                }

            } while (true);

        }


        // To capture Tasks issued to server. This will allow Time not to be wasted whilst guessing when tasks are finished.
        public async Task<string> ServerGetTasks(string ServerIP, string PasswordOverride, MainViewModel passedvm, string ServerGen)
        {
            await RedfishRestCall("https://" + ServerIP + "/redfish/v1/TaskService/Tasks?$select=Members@odata.count", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);


            await ReadRedfishRoot(jsonReturned, "", "Dell");


            //OUT PUT SERVER DATA
            foreach (var itm in EntireServer.RedfishSection)
            {



                //passedvm.ILORestOutput1 += itm.oDataContext + "\n";
                Console.WriteLine("\n" + itm.oDataContext + "");
                //Loop through all key value pairs in Entire Server Object
                foreach (var keyvalue in itm.RedfishPairs)
                {

                    if (keyvalue.Key == "Members@odata.count")
                    {
                        return keyvalue.Value;
                    }
                    Console.WriteLine("\n" + keyvalue.Key + " = " + keyvalue.Value);
                }

            }




            //POST ACTION
            // await RedfishRestCall("https://" + ServerIP + "/redfish/v1/UpdateService/Actions/UpdateService.SimpleUpdate", HttpMethod.Post, "root:calvin", passedvm);


            return "Nothing Found";

        }



        //******* DELL SERVERS ********
        public async Task<DELLServerInfo> GetDellData(string ServerIP, string PasswordOverride, MainViewModel passedvm, string ServerGen)
        {


            //Write link to console window
            passedvm.ILORestOutput1 += "****** Starting Server Inventory, please wait... ******\n\n";

            passedvm.ProgressPercentage1 = 10;

            //Counter for looping through odata links
            int Counter = 0;
            //INITIAL ROOT CALLS

            //Write link to console window
            passedvm.ILORestOutput1 += "****** Running Redfish Crawler Scripts, Checking Root Node... ******\n\n";



            if (passedvm.ILORestOutput1.Contains("iDRAC9"))
            {
                //********* OLD CODE ******* ?$expand=*($levels=1)
                //await RedfishRestCall("https://" + ServerIP + "/redfish/v1/", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);


                //await ReadRedfishRoot(jsonReturned, "", "Dell");

                ////SYSTEMS
                await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/System.Embedded.1/", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);


                await ReadRedfishRoot(jsonReturned, "", "Dell");



                ////////CHASSIS
                await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/System.Embedded.1/", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);


                await ReadRedfishRoot(jsonReturned, "", "Dell");




                ////////IDRAC
                await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/iDRAC.Embedded.1/", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);


                await ReadRedfishRoot(jsonReturned, "", "Dell");


                //FIRMWARE
                await RedfishRestCall("https://" + ServerIP + "/redfish/v1/UpdateService/FirmwareInventory)", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);


                await ReadRedfishRoot(jsonReturned, "", "Dell");





                ////CPU
                await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/System.Embedded.1/Processors/", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);


                await ReadRedfishRoot(jsonReturned, "", "Dell");

                ////MEMORY
                await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/System.Embedded.1/Memory/", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);


                await ReadRedfishRoot(jsonReturned, "", "Dell");

                ////NETWORK
                await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/System.Embedded.1/EthernetInterfaces/", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);


                await ReadRedfishRoot(jsonReturned, "", "Dell");

                ////FANS THERMAL
                //await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/System.Embedded.1/Thermal/", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);


                //await ReadRedfishRoot(jsonReturned, "", "Dell");

                ////POWER
                //await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/System.Embedded.1/Power/", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);


                //await ReadRedfishRoot(jsonReturned, "", "Dell");

                //Clear Links as don't want it to run in loop below /redfish/v1/TaskService/Tasks?$select=Members@odata.count
                RootLinks.Clear();

                ////STORAGE
                await RedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/System.Embedded.1/Storage/", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);


                await ReadRedfishRoot(jsonReturned, "", "Dell");



                //Set Layer Depth to 4 to drill down further(Mainly required for Storage) use 6 to get logical drive members
                URILayerDepth = 3;



                //Drill down 10 layers and follow any returned links URILayerDepth 
                for (int i = 0; i < URILayerDepth; i++)
                {


                    foreach (var itm in RootLinks)
                    {
                        //Write link to console window
                        passedvm.ILORestOutput1 += itm + "\n";
                        //Console.WriteLine(itm);
                        ResourceLinks.Add(itm);
                    }

                    RootLinks.Clear();

                    //Set Percent done
                    passedvm.ProgressPercentage1 = passedvm.ProgressPercentage1 + 10;

                    ////Loop through each resource link and drill down multiple layers 
                    foreach (var item in ResourceLinks)
                    {

                        //Call odata and href links
                        // use ?$expand=. to expand any Members, will require a different sorting method
                        //New call using session x auth key
                        //await DellRedfishRestCall("https://" + ServerIP + item.ToString() + "?$expand=*($levels=1)", HttpMethod.Get, xAuthobject, passedvm); ?$expand=*($levels=1)
                        await RedfishRestCall(@"https://" + ServerIP + item.ToString() + "", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);
                        //Flatten Json objects to Key Value Pairs
                        await ReadRedfishRoot(jsonReturned, "Layer" + i.ToString(), "Dell");
                    }



                    // Update Layer Count
                    Counter += 1;
                    //Set Message


                    ResourceLinks.Clear();
                }

                RootLinks.Clear();



            }
            else
            {
                //GET X-AUTH-TOKEN & Location to delete session at end
                var xAuthobject = await DellGetSessionID("https://" + ServerIP, "/redfish/v1/SessionService/Sessions", HttpMethod.Post, "root:" + PasswordOverride.Trim(), passedvm);

                passedvm.ILORestOutput1 += "*** Aquiring Session Id ***\n\n";
                // MessageBox.Show(xAuthobject.XAuthKey + "  " + xAuthobject.Location);

                //ROOT OBJECT

                //await DellRedfishRestCall("https://" + ServerIP + "/redfish/v1/", HttpMethod.Get, xAuthobject, passedvm);

                //await ReadRedfishRoot(jsonReturned, "", "Dell");

                ////Systems
                await DellRedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/System.Embedded.1/", HttpMethod.Get, xAuthobject, passedvm);

                await ReadRedfishRoot(jsonReturned, "", "Dell");

                ////Idrac

                await DellRedfishRestCall("https://" + ServerIP + "/redfish/v1/Managers/iDRAC.Embedded.1/", HttpMethod.Get, xAuthobject, passedvm);

                await ReadRedfishRoot(jsonReturned, "", "Dell");

                ////Chassis

                await DellRedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/System.Embedded.1/?$expand=*($levels=1)", HttpMethod.Get, xAuthobject, passedvm);

                await ReadRedfishRoot(jsonReturned, "", "Dell");

                ////Firmware

                await DellRedfishRestCall("https://" + ServerIP + "/redfish/v1/UpdateService/FirmwareInventory/?$expand=*($levels=1)", HttpMethod.Get, xAuthobject, passedvm);

                await ReadRedfishRoot(jsonReturned, "", "Dell");


                //CPU
                await DellRedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/System.Embedded.1/Processors/?$expand=*($levels=1)", HttpMethod.Get, xAuthobject, passedvm);

                await ReadRedfishRoot(jsonReturned, "", "Dell");

                ////MEMORY 
                await DellRedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/System.Embedded.1/Memory/?$expand=*($levels=1)", HttpMethod.Get, xAuthobject, passedvm);

                await ReadRedfishRoot(jsonReturned, "", "Dell");

                ////NETWORK
                await DellRedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/System.Embedded.1/EthernetInterfaces/?$expand=*($levels=1)", HttpMethod.Get, xAuthobject, passedvm);

                await ReadRedfishRoot(jsonReturned, "", "Dell");

                ////FANS
                //await DellRedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/System.Embedded.1/Thermal/?$expand=*($levels=1)", HttpMethod.Get, xAuthobject, passedvm);

                //await ReadRedfishRoot(jsonReturned, "", "Dell");

                ////POWER
                //await DellRedfishRestCall("https://" + ServerIP + "/redfish/v1/Chassis/System.Embedded.1/Power/?$expand=*($levels=1)", HttpMethod.Get, xAuthobject, passedvm);

                //await ReadRedfishRoot(jsonReturned, "", "Dell");


                //Clear Links as don't want it to run in loop below
                RootLinks.Clear();


                //STORAGE
                await DellRedfishRestCall("https://" + ServerIP + "/redfish/v1/Systems/System.Embedded.1/Storage/?$expand=*($levels=1)", HttpMethod.Get, xAuthobject, passedvm);

                await ReadRedfishRoot(jsonReturned, "", "Dell");


                //Set Layer Depth to 4 to drill down further(Mainly required for Storage) use 6 to get logical drive members
                URILayerDepth = 1;



                //Drill down 10 layers and follow any returned links URILayerDepth 
                for (int i = 0; i < URILayerDepth; i++)
                {


                    foreach (var itm in RootLinks)
                    {
                        //Write link to console window
                        passedvm.ILORestOutput1 += itm + "\n";
                        //Console.WriteLine(itm);
                        ResourceLinks.Add(itm);
                    }

                    RootLinks.Clear();

                    //Set Percent done
                    passedvm.ProgressPercentage1 = passedvm.ProgressPercentage1 + 10;

                    ////Loop through each resource link and drill down multiple layers 
                    foreach (var item in ResourceLinks)
                    {

                        //Call odata and href links
                        // use ?$expand=. to expand any Members, will require a different sorting method
                        //New call using session x auth key
                        await DellRedfishRestCall("https://" + ServerIP + item.ToString() + "?$expand=*($levels=1)", HttpMethod.Get, xAuthobject, passedvm);
                        //await RedfishRestCall(@"https://" + ServerIP + item.ToString() + "?$expand=*($levels=1)", HttpMethod.Get, "root:" + PasswordOverride.Trim(), passedvm);
                        //Flatten Json objects to Key Value Pairs
                        await ReadRedfishRoot(jsonReturned, "Layer" + i.ToString(), "Dell");
                    }



                    // Update Layer Count
                    Counter += 1;
                    //Set Message


                    ResourceLinks.Clear();
                }

                RootLinks.Clear();// 





                //DELETE SESSION
                await DellRedfishRestCall("https://" + ServerIP + xAuthobject.Location, HttpMethod.Delete, xAuthobject, passedvm);

            }





            //POST ACTION
            // await RedfishRestCall("https://" + ServerIP + "/redfish/v1/UpdateService/Actions/UpdateService.SimpleUpdate", HttpMethod.Post, "root:calvin", passedvm);





            //MAP DATA TO INTERNAL CLASS

            DELLServerInfo DellServer = new DELLServerInfo();
            //Create default collections
            DellCpu cpu = new DellCpu();
            DellMemory memory = new DellMemory();
            DellFans fan = new DellFans();
            DellNetwork network = new DellNetwork();
            DellPower power = new DellPower();
            DellStorage storage = new DellStorage();
            DelliDrac idrac = new DelliDrac();
            DellPhysicalDisk physicalDisk = new DellPhysicalDisk();
            DellVirtualDisk vDisk = new DellVirtualDisk();
            DellThermal thermal = new DellThermal();
            //to hold firmware reference
            string firmwareName = "";


            //OUT PUT SERVER DATA
            foreach (var itm in EntireServer.RedfishSection)
            {



                //passedvm.ILORestOutput1 += itm.oDataContext + "\n";
                Console.WriteLine("\n" + itm.oDataContext + "");
                //Loop through all key value pairs in Entire Server Object
                foreach (var keyvalue in itm.RedfishPairs)
                {
                    Console.WriteLine(keyvalue.Key + " = " + keyvalue.Value);

                    //System 
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#ComputerSystem.ComputerSystem"))
                    {

                        switch (keyvalue.Key)
                        {
                            case "Manufacturer":
                                //Manufaturer
                                DellServer.Manufacturer = keyvalue.Value.Trim();

                                break;
                            case "Model":
                                //Model
                                DellServer.Model = keyvalue.Value.Trim();

                                break;
                            case "AssetTag":
                                //Asset Tag
                                DellServer.AssetTag = keyvalue.Value.Trim();

                                break;
                            case "SKU":
                                //SKU
                                DellServer.ServiceTag = keyvalue.Value.Trim();

                                break;
                            case "SerialNumber":
                                //Serial No
                                DellServer.BoardSerialNumber = keyvalue.Value.Trim();

                                break;
                            case "PartNumber":
                                //Part No
                                DellServer.BoardPartNumber = keyvalue.Value.Trim();

                                break;

                            case "HostName":
                                //Server Name
                                DellServer.HostName = keyvalue.Value;

                                break;
                            case "PowerState":
                                //Server Name
                                DellServer.PowerState = keyvalue.Value;

                                break;
                            case "ProcessorSummary.Count":
                                //Server Name
                                DellServer.CPUCount = keyvalue.Value;

                                break;
                            case "ProcessorSummary.Model":
                                //Server Name
                                DellServer.CPUFamily = keyvalue.Value;

                                break;
                            case "ProcessorSummary.Status":
                                //Server Name
                                DellServer.CPURollupStatus = keyvalue.Value;

                                break;
                            case "MemorySummary.TotalSystemMemoryGiB":
                                //Server Name
                                DellServer.TotalMemory = keyvalue.Value + "GB";

                                break;
                            case "BiosVersion":
                                //Server Name
                                DellServer.BiosCurrentVersion = keyvalue.Value;

                                break;
                            case "MemorySummary.Status.HealthRollup":
                                //Server Name
                                DellServer.MemoryRollupStatus = keyvalue.Value;

                                break;
                                //case "ProcessorSummary.Count":
                                //    //Server Name
                                //    DellServer.CPUCount = keyvalue.Value;

                                //    break;
                                //case "ProcessorSummary.Model":
                                //    //Server Name
                                //    DellServer.CPUFamily = keyvalue.Value;

                                //    break;
                        }
                    }




                    //Manager 
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Manager.Manager"))
                    {

                        switch (keyvalue.Key)
                        {

                            case "Model":
                                //Server Model
                                DellServer.SystemGeneration = keyvalue.Value;

                                break;

                            case "FirmwareVersion":
                                //IDRAC Firmware
                                DellServer.iDRACVersion = keyvalue.Value;

                                break;

                        }
                    }



                    //Chassis Power & Fans
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Chassis.Chassis"))
                    {
                        //PSU
                        if (keyvalue.Key.Contains("Power.PowerSupplies["))
                        {
                            switch (keyvalue.Key)
                            {

                                case var s when keyvalue.Key.Contains("].FirmwareVersion"):
                                    //Create new psu instance
                                    power = new DellPower();

                                    power.FirmwareVersion = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].InputRanges[0].InputType"):

                                    power.Type = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].InputRanges[0].OutputWattage"):

                                    power.TotalOutputPower = keyvalue.Value + " W";

                                    break;
                                case var s when keyvalue.Key.Contains("].LineInputVoltage"):

                                    power.InputVoltage = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].Manufacturer"):

                                    power.Manufacturer = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].MemberId"):

                                    //Check for property that contains the same value and skip past it
                                    if (keyvalue.Key.Contains(".Redundancy["))
                                    {

                                    }
                                    else
                                    {
                                        //Console.WriteLine(keyvalue.Value);
                                        power.InstanceID = keyvalue.Value;
                                    }


                                    break;
                                case var s when keyvalue.Key.Contains("].Model"):
                                    //Console.WriteLine(keyvalue.Value);
                                    power.Model = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].Name"):

                                    power.DeviceDescription = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].PartNumber"):

                                    power.PartNumber = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].PowerInputWatts"):

                                    power.Range1MaxInputPower = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].Redundancy[0].Status.Health"):

                                    power.RedundancyStatus = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].SerialNumber"):

                                    power.SerialNumber = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].Status.Health"):

                                    power.PrimaryStatus = keyvalue.Value;
                                    //Add psu to collection
                                    DellServer.DELLPowerCollection.Add(power);

                                    break;
                            }
                        }


                        //FANS
                        if (keyvalue.Key.Contains("Thermal.Fans["))
                        {
                            switch (keyvalue.Key)
                            {

                                case var s when keyvalue.Key.Contains("].FanName"):
                                    //Create new psu instance
                                    fan = new DellFans();

                                    fan.InstanceID = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].MemberId"):

                                    fan.DeviceDescription = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].Reading"):

                                    //Check for property that contains the same value
                                    if (keyvalue.Key.Contains("].ReadingUnits"))
                                    {
                                        fan.BaseUnits = keyvalue.Value;
                                    }
                                    else
                                    {
                                        fan.CurrentReading = keyvalue.Value + " RPM";
                                    }


                                    break;
                                //case var s when keyvalue.Key.Contains("].ReadingUnits"):

                                //    fan.BaseUnits = keyvalue.Value;

                                //    break;
                                case var s when keyvalue.Key.Contains("].Name"):

                                    fan.RateUnits = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].Status.Health"):

                                    fan.PrimaryStatus = keyvalue.Value;
                                    //Add psu to collection
                                    DellServer.DELLFanCollection.Add(fan);

                                    break;
                            }

                        }


                        if (keyvalue.Key.Contains("Thermal.Temperatures["))
                        {
                            switch (keyvalue.Key)
                            {
                                case var s when keyvalue.Key.Contains("].MaxReadingRangeTemp"):
                                    //Create new psu instance
                                    thermal = new DellThermal();
                                    thermal.MaxReadingRangeTemp = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].MemberId"):


                                    thermal.MemberId = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].MinReadingRangeTemp"):
                                    thermal.MinReadingRangeTemp = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].Name "):

                                    thermal.Name = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].PhysicalContext"):

                                    thermal.PhysicalContext = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].ReadingCelsius"):

                                    thermal.ReadingCelsius = keyvalue.Value;

                                    //Check if member id is the exhaust port 
                                    if (thermal.MemberId.Contains("#SystemBoardExhaustTemp"))
                                    {
                                        DellServer.EstimatedExhaustTemperature = thermal.ReadingCelsius;
                                    }

                                    break;
                                case var s when keyvalue.Key.Contains("].SensorNumber"):

                                    thermal.SensorNumber = keyvalue.Value;

                                    break;



                                case var s when keyvalue.Key.Contains("].Status.Health"):

                                    thermal.Health = keyvalue.Value;
                                    //Add psu to collection
                                    DellServer.DELLThermalCollection.Add(thermal);

                                    break;
                            }
                        }


                    }


                    //Firmware capture
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#SoftwareInventoryCollection.SoftwareInventoryCollection"))
                    {
                        if (keyvalue.Key.Contains("Members["))
                        {


                            switch (keyvalue.Key)
                            {

                                case var s when keyvalue.Key.Contains("].Name"):
                                    firmwareName = keyvalue.Value;
                                    break;

                                case var s when keyvalue.Key.Contains("].Version"):

                                    //Check if name property is CPLD
                                    if (firmwareName.Contains("CPLD"))
                                    {
                                        DellServer.CPLDVersion = keyvalue.Value;
                                    }

                                    break;

                            }
                        }

                    }

                    //COLLECTIONS

                    //CPU
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#ProcessorCollection.ProcessorCollection"))
                    {


                        if (keyvalue.Key.Contains("Members["))
                        {
                            switch (keyvalue.Key)
                            {
                                case var s when keyvalue.Key.Contains("].Id"):
                                    //Create ne CPU Instance
                                    cpu = new DellCpu();
                                    //Server Model
                                    cpu.InstanceID = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].Manufacturer"):

                                    cpu.Manufacturer = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].MaxSpeedMHz"):

                                    cpu.MaxClockSpeed = keyvalue.Value + " Mhz";
                                    cpu.CurrentClockSpeed = keyvalue.Value + " Mhz";

                                    break;
                                case var s when keyvalue.Key.Contains("].Model"):

                                    cpu.Model = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].Name"):

                                    cpu.DeviceDescription = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].ProcessorId.VendorId"):

                                    cpu.CPUFamily = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].Status.Health"):

                                    cpu.PrimaryStatus = keyvalue.Value;
                                    //cpu.CPUStatus = keyvalue.Value;
                                    break;

                                case var s when keyvalue.Key.Contains("].TotalCores"):

                                    cpu.NumberOfProcessorCores = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].TotalThreads"):

                                    cpu.NumberOfEnabledThreads = keyvalue.Value;
                                    //Add cpu instance
                                    DellServer.DELLCpuCollection.Add(cpu);

                                    break;

                            }

                        }
                    }


                    //Memory
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#MemoryCollection.MemoryCollection"))
                    {

                        if (keyvalue.Key.Contains("Members["))
                        {
                            switch (keyvalue.Key)
                            {
                                case var s when keyvalue.Key.Contains("].AllowedSpeedsMHz[0]"):

                                    //Create new Memory Instance
                                    memory = new DellMemory();

                                    memory.Speed = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].CapacityMiB"):

                                    memory.Size = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].Description"):


                                    memory.InstanceID = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].Id"):

                                    memory.DeviceDescription = keyvalue.Value;


                                    break;
                                case var s when keyvalue.Key.Contains("].ErrorCorrection"):

                                    memory.Model = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].Manufacturer"):

                                    memory.Manufacturer = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].MemoryDeviceType"):

                                    memory.MemoryType = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].PartNumber"):

                                    memory.PartNumber = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].RankCount"):

                                    memory.Rank = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].SerialNumber"):

                                    memory.SerialNumber = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].Status.Health"):

                                    memory.PrimaryStatus = keyvalue.Value;
                                    //Add memory to collection
                                    DellServer.DELLMemoryCollection.Add(memory);

                                    break;

                            }

                        }
                    }


                    //Network
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#EthernetInterfaceCollection.EthernetInterfaceCollection"))
                    {

                        if (keyvalue.Key.Contains("Members["))
                        {
                            switch (keyvalue.Key)
                            {
                                case var s when keyvalue.Key.Contains("].AutoNeg"):
                                    //Create new Netowrk Instance
                                    network = new DellNetwork();
                                    network.AutoNegotiation = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].Description"):

                                    network.DeviceDescription = keyvalue.Value;
                                    break;
                                case var s when keyvalue.Key.Contains("].FullDuplex"):

                                    network.LinkDuplex = keyvalue.Value;
                                    break;
                                case var s when keyvalue.Key.Contains("].Id"):

                                    network.InstanceID = keyvalue.Value;
                                    break;
                                case var s when keyvalue.Key.Contains("].MACAddress"):

                                    network.PermanentMACAddress = keyvalue.Value;
                                    break;

                                case var s when keyvalue.Key.Contains("].SpeedMbps"):

                                    network.TransmitFlowControl = keyvalue.Value;
                                    break;
                                case var s when keyvalue.Key.Contains("].Name"):

                                    network.MediaType = keyvalue.Value;
                                    break;


                                case var s when keyvalue.Key.Contains("].Status.Health"):

                                    network.Health = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].Status.State"):

                                    network.State = keyvalue.Value;
                                    //Add memory to collection
                                    DellServer.DELLNetworkCollection.Add(network);

                                    break;

                            }

                        }
                    }



                    //Controllers
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#StorageCollection.StorageCollection"))
                    {
                        if (keyvalue.Key.Contains("Members["))
                        {
                            switch (keyvalue.Key)
                            {

                                case var s when keyvalue.Key.Contains("].Description"):
                                    //create new storage instance ].StorageControllers[0].MemberId
                                    storage = new DellStorage();

                                    storage.InstanceID = keyvalue.Value;
                                    //Check which controller member is and add to embedded or secondary controller property
                                    if (keyvalue.Key.Contains("Members[0]"))
                                    {
                                        DellServer.ControllerType = storage.InstanceID;
                                    }
                                    else if (keyvalue.Key.Contains("Members[1]"))
                                    {
                                        DellServer.ControllerType2 = storage.InstanceID;
                                    }

                                    break;
                                case var s when keyvalue.Key.Contains("].StorageControllers[0].MemberId"):
                                    storage.DeviceDescription = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].StorageControllers[0].FirmwareVersion"):
                                    storage.ControllerFirmwareVersion = keyvalue.Value;

                                    //Check which controller member is and add to embedded or secondary controller property
                                    if (keyvalue.Key.Contains("Members[0]"))
                                    {
                                        DellServer.ControllerVersion = storage.ControllerFirmwareVersion;
                                    }
                                    else if (keyvalue.Key.Contains("Members[1]"))
                                    {
                                        DellServer.ControllerVersion = storage.ControllerFirmwareVersion;
                                    }


                                    break;
                                case var s when keyvalue.Key.Contains("].StorageControllers[0].Manufacturer"):
                                    storage.Manufacturer = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].StorageControllers[0].SpeedGbps"):
                                    storage.MaxPossiblePCILinkSpeed = keyvalue.Value + " Gbps";

                                    break;
                                case var s when keyvalue.Key.Contains("].StorageControllers[0].Status.Health"):

                                    if (keyvalue.Key.Contains("].Status.HealthRollup"))
                                    {
                                        storage.RollupStatus = keyvalue.Value;
                                        DellServer.StorageRollupStatus = keyvalue.Value;
                                    }
                                    else
                                    {
                                        storage.PrimaryStatus = keyvalue.Value;
                                    }


                                    break;

                                //case var s when keyvalue.Key.Contains("].StorageControllers[0].Status.HealthRollup"):
                                //    storage.RollupStatus = keyvalue.Value;

                                //    break;

                                case var s when keyvalue.Key.Contains("].StorageControllers[0].Status.State"):
                                    storage.State = keyvalue.Value;
                                    //Add memory to collection
                                    DellServer.DELLStorageCollection.Add(storage);


                                    break;

                            }
                        }

                    }



                    //Storage Physical & Virtual Disks
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#Storage.Storage"))
                    {
                        if (keyvalue.Key.Contains("Drives["))
                        {
                            switch (keyvalue.Key)
                            {

                                case var s when keyvalue.Key.Contains("].BlockSizeBytes"):
                                    //create new pDisk instance ].pDiskControllers[0].MemberId
                                    physicalDisk = new DellPhysicalDisk();

                                    physicalDisk.BlockSizeInBytes = keyvalue.Value + "Bytes";

                                    break;
                                case var s when keyvalue.Key.Contains("].CapableSpeedGbs"):

                                    physicalDisk.MaxCapableSpeed = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].CapacityBytes"):

                                    physicalDisk.SizeInBytes = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].Description"):


                                    physicalDisk.DeviceType = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].EncryptionStatus"):
                                    physicalDisk.EncryptionStatus = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].Id"):

                                    if (keyvalue.Key.Contains("Drives[") && !keyvalue.Key.Contains("Identifiers["))
                                    {
                                        physicalDisk.DeviceDescription = keyvalue.Value;
                                    }

                                    break;

                                case var s when keyvalue.Key.Contains("].Manufacturer"):
                                    physicalDisk.Manufacturer = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].MediaType"):
                                    physicalDisk.MediaType = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].Model"):
                                    physicalDisk.Model = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].PartNumber"):
                                    physicalDisk.PartNumber = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].Protocol"):
                                    physicalDisk.BusProtocol = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].SerialNumber"):
                                    physicalDisk.SerialNumber = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].Status.Health"):
                                    if (keyvalue.Key.Contains("Drives[") && !keyvalue.Key.Contains("HealthRollup"))
                                    {
                                        physicalDisk.PrimaryStatus = keyvalue.Value;
                                    }

                                    break;

                                case var s when keyvalue.Key.Contains("].Status.HealthRollup"):
                                    physicalDisk.RollupStatus = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].Status.State"):
                                    //pDisk.State = keyvalue.Value;
                                    //Add memory to collection
                                    DellServer.DELLPhysicalDiskCollection.Add(physicalDisk);


                                    break;

                            }
                        }

                    }



                    //Virtual Disk
                    if (itm.oDataContext.Contains("/redfish/v1/$metadata#VolumeCollection.VolumeCollection"))
                    {
                        if (keyvalue.Key.Contains("Members["))
                        {
                            switch (keyvalue.Key)
                            {

                                case var s when keyvalue.Key.Contains("].Description"):
                                    //create new vDisk instance ].vDiskControllers[0].MemberId
                                    vDisk = new DellVirtualDisk();

                                    vDisk.InstanceID = keyvalue.Value;

                                    break;
                                case var s when keyvalue.Key.Contains("].Id "):
                                    vDisk.DeviceDescription = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].vDiskControllers[0].Status.Health"):
                                    vDisk.PrimaryStatus = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].vDiskControllers[0].Status.HealthRollup"):
                                    vDisk.RollupStatus = keyvalue.Value;

                                    break;

                                case var s when keyvalue.Key.Contains("].vDiskControllers[0].Status.State"):
                                    //vDisk.State = keyvalue.Value;
                                    //Add memory to collection
                                    DellServer.DELLVirtualDiskCollection.Add(vDisk);


                                    break;

                            }
                        }

                    }


                }

            }


            //ROLLUP HEALTH STATUS

            if (DellServer.DELLThermalCollection.Count > 0)
            {
                foreach (var item in DellServer.DELLThermalCollection)
                {
                    if (item.Health == "")
                    {
                        //if blank, set as ok
                        DellServer.TempRollupStatus = "OK";
                    }
                    else
                    {
                        DellServer.TempRollupStatus = item.Health;
                    }

                }
            }
            else
            {
                DellServer.TempRollupStatus = "OK";

            }
            //FANS
            if (DellServer.DELLFanCollection.Count > 0)
            {
                foreach (var item in DellServer.DELLFanCollection)
                {
                    if (item.PrimaryStatus == "")
                    {
                        //if blank, set as ok
                        DellServer.FanRollupStatus = "OK";
                    }
                    else
                    {
                        DellServer.FanRollupStatus = item.PrimaryStatus;
                    }

                }
            }
            else
            {
                DellServer.FanRollupStatus = "OK";

            }

            //Power Health at a glance
            var PowerHealthRollUp = DellServer.DELLPowerCollection.Where(e => e.PrimaryStatus != "OK").ToList();
            //Check if different value found, if so display it
            if (PowerHealthRollUp.Count > 0)
            {
                foreach (var item in PowerHealthRollUp)
                {
                    DellServer.PSRollupStatus = item.PrimaryStatus;

                }
            }
            else
            {
                DellServer.PSRollupStatus = "OK";

            }




            //Count populated memory slots
            DellServer.PopulatedDIMMSlots = DellServer.DELLMemoryCollection.Count.ToString();





            //Return server object
            return DellServer;
        }

        //
        bool lenovoRunOnce = false;

        //******* Lenovo XClarity Servers for System X use One Cli Tool
        public async Task<LenovoServerInfo> GetLenovoData(string ServerIP, string PasswordOverride, MainViewModel passedvm, string ServerGen)
        {
            //SYSTEM REGIONS
            ////SYSTEMS

            ////Systems ?$expand=*($levels=1)
            await RedfishRestCall(@"https://" + ServerIP + "/redfish/v1/Systems/1" + "", HttpMethod.Get, "USERID:" + PasswordOverride.Trim(), passedvm);
            //Initial Read
            await ReadRedfishRoot(jsonReturned, "System", "Lenovo");


            //Chassis
            //await RedfishRestCall(@"https://" + ServerIP + "/redfish/v1/Chassis/System.Embedded.1?$select=AssetTag", HttpMethod.Get, "USERID:" + PasswordOverride.Trim());
            await RedfishRestCall(@"https://" + ServerIP + "/redfish/v1/Chassis/1" + "", HttpMethod.Get, "USERID:" + PasswordOverride.Trim(), passedvm);
            //Initial Read 
            await ReadRedfishRoot(jsonReturned, "Chassis", "Lenovo");



            ////Managers
            //await RedfishRestCall(@"https://" + ServerIP + "/redfish/v1/Managers/1" + "", HttpMethod.Get, "USERID:" + PasswordOverride.Trim(), passedvm);
            ////Initial Read
            //await ReadRedfishRoot(jsonReturned, "Managers", "Lenovo");




            //TARGETED URI'S



            ////Firmware
            await RedfishRestCall(@"https://" + ServerIP + "/redfish/v1/UpdateService/FirmwareInventory?$expand=*($levels=1)", HttpMethod.Get, "USERID:" + PasswordOverride.Trim(), passedvm);
            //Initial Read
            await ReadRedfishRoot(jsonReturned, "FirmwareInventory", "Lenovo");



            //CPU check
            await RedfishRestCall(@"https://" + ServerIP + "/redfish/v1/Systems/1/Processors?$expand=*($levels=1)", HttpMethod.Get, "USERID:" + PasswordOverride.Trim(), passedvm);
            //Initial Read
            await ReadRedfishRoot(jsonReturned, "Processors", "Lenovo");


            //Memory check
            await RedfishRestCall(@"https://" + ServerIP + "/redfish/v1/Systems/1/Memory?$expand=*($levels=1)", HttpMethod.Get, "USERID:" + PasswordOverride.Trim(), passedvm);
            //Initial Read
            await ReadRedfishRoot(jsonReturned, "Memory", "Lenovo");



            //Ethernet Interfaces
            await RedfishRestCall(@"https://" + ServerIP + "/redfish/v1/Managers/1/EthernetInterfaces?$expand=*($levels=1)", HttpMethod.Get, "USERID:" + PasswordOverride.Trim(), passedvm);
            //Initial Read
            await ReadRedfishRoot(jsonReturned, "EthernetInterfaces", "Lenovo");



            ////Network check
            //await RedfishRestCall(@"https://" + ServerIP + "/redfish/v1/Chassis/1/NetworkAdapters?$expand=*($levels=1)", HttpMethod.Get, "USERID:" + PasswordOverride.Trim(), passedvm);
            ////Initial Read
            //await ReadRedfishRoot(jsonReturned, "Description" , "Lenovo");


            //Power  
            await RedfishRestCall(@"https://" + ServerIP + "/redfish/v1/Chassis/1/Power?$expand=*($levels=1)", HttpMethod.Get, "USERID:" + PasswordOverride.Trim(), passedvm);
            //Initial Read
            await ReadRedfishRoot(jsonReturned, "Power", "Lenovo");


            //Fans Thermal
            await RedfishRestCall(@"https://" + ServerIP + "/redfish/v1/Chassis/1/Thermal?$expand=*($levels=1)", HttpMethod.Get, "USERID:" + PasswordOverride.Trim(), passedvm);
            //Initial Read
            await ReadRedfishRoot(jsonReturned, "Thermal", "Lenovo");



            //Storage Controllers check  redfish/v1/Systems/System.Embedded.1/Storage/RAID.Integrated.1-1
            await RedfishRestCall(@"https://" + ServerIP + "/redfish/v1/Systems/1/Storage?$expand=*($levels=2)", HttpMethod.Get, "USERID:" + PasswordOverride.Trim(), passedvm);
            //Initial Read
            await ReadRedfishRoot(jsonReturned, "Storage", "Lenovo");




            //*************** MAPPING OF DATA *******************

            //Declare new Instance of Lenovo Server
            LenovoServerInfo LenovoServer = new LenovoServerInfo();
            //Declare new instances of the coillection
            LenovoServer.LenovoBMCCollection = new ObservableCollection<LenovoBMC>();
            LenovoServer.LenovoCPUCollection = new ObservableCollection<LenovoCPU>();
            LenovoServer.LenovoFanCollection = new ObservableCollection<LenovoFans>();
            LenovoServer.LenovoFirmwareCollection = new ObservableCollection<LenovoFirmware>();
            LenovoServer.LenovoMemoryCollection = new ObservableCollection<LenovoMemory>();
            LenovoServer.LenovoNetworkCollection = new ObservableCollection<LenovoNetwork>();
            LenovoServer.LenovoPhysicalDiskCollection = new ObservableCollection<LenovoPhysicalDisk>();
            LenovoServer.LenovoPowerCollection = new ObservableCollection<LenovoPower>();
            LenovoServer.LenovoStorageCollection = new ObservableCollection<LenovoStorage>();
            LenovoServer.LenovoStorageSoftwareCollection = new ObservableCollection<LenovoStorageSoftware>();
            LenovoServer.LenovoSystemBoardCollection = new ObservableCollection<LenovoSystemBoard>();
            LenovoServer.LenovoVirtualDiskCollection = new ObservableCollection<LenovoVirtualDisk>();







            //OUT PUT SERVER DATA
            foreach (var itm in EntireServer.RedfishSection)
            {
                //Console.WriteLine("\n" + itm.oDataContext + "");


                //System 
                if (itm.oDataContext == "System")
                {
                    //ADD LINQ TO COLLECTION QUERY TO EXTRACT PROPERTY VALUES 
                    //Console.WriteLine(keyvalue.Key + " = " + keyvalue.Value);
                    LenovoServer.Manufacturer = itm.RedfishPairs.Where(x => x.Key == "Manufacturer").FirstOrDefault()?.Value;

                    LenovoServer.SystemSerialNumber = itm.RedfishPairs.Where(x => x.Key == "SerialNumber").FirstOrDefault()?.Value;
                    LenovoServer.SystemUUID = itm.RedfishPairs.Where(x => x.Key == "UUID").FirstOrDefault()?.Value;
                    LenovoServer.SKU = itm.RedfishPairs.Where(x => x.Key == "SKU").FirstOrDefault()?.Value;
                    //LenovoServer.BiosCurrentVersion = itm.RedfishPairs.Where(x => x.Key == "BiosVersion").FirstOrDefault()?.Value;
                    LenovoServer.AssetTag = itm.RedfishPairs.Where(x => x.Key == "AssetTag").FirstOrDefault()?.Value;
                    LenovoServer.Model = itm.RedfishPairs.Where(x => x.Key == "Model").FirstOrDefault()?.Value;
                    LenovoServer.CPUCount = itm.RedfishPairs.Where(x => x.Key == "ProcessorSummary.Count").FirstOrDefault()?.Value;
                    LenovoServer.CPUFamily = itm.RedfishPairs.Where(x => x.Key == "ProcessorSummary.Model").FirstOrDefault()?.Value;
                    LenovoServer.CPURollupStatus = itm.RedfishPairs.Where(x => x.Key == "ProcessorSummary.Status.HealthRollup").FirstOrDefault()?.Value;
                    LenovoServer.ThreadPerCore = itm.RedfishPairs.Where(x => x.Key == "ProcessorSummary.LogicalProcessorCount").FirstOrDefault()?.Value;
                    LenovoServer.MemoryRollupStatus = itm.RedfishPairs.Where(x => x.Key == "MemorySummary.Status.HealthRollup").FirstOrDefault()?.Value;
                    LenovoServer.TotalMemory = itm.RedfishPairs.Where(x => x.Key == "MemorySummary.TotalSystemMemoryGiB").FirstOrDefault()?.Value + "GB";
                    LenovoServer.HostName = itm.RedfishPairs.Where(x => x.Key == "HostName").FirstOrDefault()?.Value;
                    LenovoServer.PowerState = itm.RedfishPairs.Where(x => x.Key == "PowerState").FirstOrDefault()?.Value;
                    LenovoServer.TrustedModulesState = itm.RedfishPairs.Where(x => x.Key == "TrustedModules[0].Status.State").FirstOrDefault()?.Value;
                    LenovoServer.TrustedModulesInterfaceType = itm.RedfishPairs.Where(x => x.Key == "TrustedModules[0].InterfaceType").FirstOrDefault()?.Value;
                    //Add TPM HostName = XCC-7X02-S4ACS557

                }

                //Chassis
                if (itm.oDataContext == "Chassis")
                {
                    //ADD LINQ TO COLLECTION QUERY TO EXTRACT PROPERTY VALUES 
                    LenovoServer.BoardSerialNumber = itm.RedfishPairs.Where(x => x.Key == "Oem.Lenovo.SystemBoardSerialNumber").FirstOrDefault()?.Value;
                }

                //Manager
                if (itm.oDataContext == "Managers")
                {
                    //ADD LINQ TO COLLECTION QUERY TO EXTRACT PROPERTY VALUES 
                    //LenovoServer.BMCCurrentVersion = itm.RedfishPairs.Where(x => x.Key == "FirmwareVersion").FirstOrDefault()?.Value;
                }


                //Firmware
                if (itm.oDataContext == "FirmwareInventory")
                {
                    //ADD LINQ TO COLLECTION QUERY TO EXTRACT PROPERTY VALUES 
                    //Capture members count to use in for loop
                    int FirmwareCount = Convert.ToInt32(itm.RedfishPairs.Where(x => x.Key == "Members@odata.count").FirstOrDefault()?.Value);


                    for (int i = 0; i < FirmwareCount; i++)
                    {
                        try
                        {
                            //Create a new instance of Firmware
                            LenovoFirmware FirmwareInstance = new LenovoFirmware();
                            //Use LINQ to populate properties use ? to check for null before value
                            FirmwareInstance.Name = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Name").FirstOrDefault()?.Value;
                            FirmwareInstance.Manufacturer = itm.RedfishPairs.Where(x => x?.Key == "Members[" + i.ToString() + "].Manufacturer").FirstOrDefault()?.Value;
                            FirmwareInstance.Id = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Id").FirstOrDefault()?.Value;
                            FirmwareInstance.Description = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Description").FirstOrDefault()?.Value;
                            FirmwareInstance.SoftwareId = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].SoftwareId").FirstOrDefault()?.Value;
                            FirmwareInstance.Version = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Version").FirstOrDefault()?.Value;
                            FirmwareInstance.ReleaseDate = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].ReleaseDate").FirstOrDefault()?.Value;
                            FirmwareInstance.Updateable = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Updateable").FirstOrDefault()?.Value;

                            //Add to summary page
                            if (FirmwareInstance.Id == "BMC-Primary")
                            {
                                LenovoServer.BMCCurrentVersion = FirmwareInstance.Version;
                            }
                            if (FirmwareInstance.Id == "BMC-Backup")
                            {
                                LenovoServer.BMCBackupVersion = FirmwareInstance.Version;
                            }
                            if (FirmwareInstance.Id == "UEFI")
                            {
                                LenovoServer.BiosCurrentVersion = FirmwareInstance.Version;
                            }
                            if (FirmwareInstance.Id == "LXPM")
                            {
                                LenovoServer.LXPMCurrentVersion = FirmwareInstance.Version;
                            }

                            //Add instance to collection
                            LenovoServer.LenovoFirmwareCollection.Add(FirmwareInstance);




                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex.Message);
                        }

                    }
                }

                //CPU
                if (itm.oDataContext == "Processors")
                {
                    //ADD LINQ TO COLLECTION QUERY TO EXTRACT PROPERTY VALUES 
                    //Capture members count to use in for loop
                    int CpuCount = Convert.ToInt32(itm.RedfishPairs.Where(x => x.Key == "Members@odata.count").FirstOrDefault()?.Value);

                    for (int i = 0; i < CpuCount; i++)
                    {

                        try
                        {
                            //Create a new instance of CPU
                            LenovoCPU CpuInstance = new LenovoCPU();
                            //Use LINQ to populate properties
                            CpuInstance.Name = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Socket").FirstOrDefault()?.Value;
                            CpuInstance.CPUCount = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Name").FirstOrDefault()?.Value;
                            CpuInstance.CPUFamily = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Version").FirstOrDefault()?.Value;
                            CpuInstance.CorePerCPU = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].TotalCores").FirstOrDefault()?.Value;
                            CpuInstance.CurrentFrequency = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Oem.Lenovo.CurrentClockSpeedMHz").FirstOrDefault()?.Value + " Mhz";
                            CpuInstance.MaxTurboFrequency = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].MaxSpeedMHz").FirstOrDefault()?.Value + " Mhz";
                            CpuInstance.ThreadPerCore = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].TotalThreads").FirstOrDefault()?.Value;
                            CpuInstance.L1Cache = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Oem.Lenovo.CacheInfo[0].InstalledSizeKByte").FirstOrDefault()?.Value;
                            CpuInstance.L2Cache = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Oem.Lenovo.CacheInfo[1].InstalledSizeKByte").FirstOrDefault()?.Value;
                            CpuInstance.L3Cache = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Oem.Lenovo.CacheInfo[2].InstalledSizeKByte").FirstOrDefault()?.Value;
                            CpuInstance.HealthState = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Status.Health").FirstOrDefault()?.Value;

                            //Add instance to collection
                            LenovoServer.LenovoCPUCollection.Add(CpuInstance);

                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex.Message);
                        }
                    }


                }


                //Memory
                if (itm.oDataContext == "Memory")
                {
                    //ADD LINQ TO COLLECTION QUERY TO EXTRACT PROPERTY VALUES 
                    //Capture members count to use in for loop
                    int MemoryCount = Convert.ToInt32(itm.RedfishPairs.Where(x => x.Key == "Members@odata.count").FirstOrDefault().Value);

                    for (int i = 0; i < MemoryCount; i++)
                    {
                        try
                        {
                            //Create a new instance of Memory
                            LenovoMemory MemoryInstance = new LenovoMemory();
                            //Use LINQ to populate properties use ? to check for null before value
                            MemoryInstance.ElementName = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Name").FirstOrDefault()?.Value;
                            MemoryInstance.Manufacturer = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Manufacturer").FirstOrDefault()?.Value;
                            MemoryInstance.Model = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Model").FirstOrDefault()?.Value;
                            MemoryInstance.MaxMemorySpeed = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].AllowedSpeedsMHz[0]").FirstOrDefault()?.Value;
                            MemoryInstance.Capacity = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].CapacityMiB").FirstOrDefault()?.Value;
                            MemoryInstance.PartNumber = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].PartNumber").FirstOrDefault()?.Value;
                            MemoryInstance.SerialNumber = itm.RedfishPairs.Where(x => x?.Key == "Members[" + i.ToString() + "].SerialNumber").FirstOrDefault()?.Value;
                            MemoryInstance.Rank = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].RankCount").FirstOrDefault()?.Value;
                            MemoryInstance.MemType = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].MemoryDeviceType").FirstOrDefault()?.Value;
                            MemoryInstance.Health = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Status.Health").FirstOrDefault()?.Value;

                            //Add instance to collection
                            LenovoServer.LenovoMemoryCollection.Add(MemoryInstance);
                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex.Message);
                        }

                        //Count max memory slots
                        LenovoServer.MaxDIMMSlots = LenovoServer.LenovoMemoryCollection.Count().ToString();

                        //Count populated memory slots
                        LenovoServer.PopulatedDIMMSlots = LenovoServer.LenovoMemoryCollection.Where(x => x.Capacity != string.Empty).Count().ToString();


                    }
                }

                //EthernetInterfaces
                if (itm.oDataContext == "EthernetInterfaces")
                {
                    //ADD LINQ TO COLLECTION QUERY TO EXTRACT PROPERTY VALUES 
                    //Capture members count to use in for loop
                    int EthernetInterfacesCount = Convert.ToInt32(itm.RedfishPairs.Where(x => x.Key == "Members@odata.count").FirstOrDefault().Value);

                    for (int i = 0; i < EthernetInterfacesCount; i++)
                    {
                        try
                        {
                            //Create a new instance of EthernetInterfaces
                            LenovoNetwork EthernetInterfacesInstance = new LenovoNetwork();
                            //Use LINQ to populate properties use ? to check for null before value
                            EthernetInterfacesInstance.Name = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Name").FirstOrDefault().Value;
                            EthernetInterfacesInstance.PortAvailability = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].InterfaceEnabled").FirstOrDefault().Value;
                            EthernetInterfacesInstance.MaxSpeed = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].SpeedMbps").FirstOrDefault()?.Value;
                            EthernetInterfacesInstance.EnabledState = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Status.Health").FirstOrDefault()?.Value;
                            EthernetInterfacesInstance.LinkTechnology = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Status.Health").FirstOrDefault()?.Value;
                            EthernetInterfacesInstance.RequestedState = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Status.Health").FirstOrDefault()?.Value;
                            EthernetInterfacesInstance.IPV4 = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].IPv4Addresses[0].Address").FirstOrDefault()?.Value;
                            EthernetInterfacesInstance.MacAddress = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].PermanentMACAddress").FirstOrDefault()?.Value;
                            EthernetInterfacesInstance.Health = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Status.Health").FirstOrDefault()?.Value;

                            //Add instance to collection
                            LenovoServer.LenovoNetworkCollection.Add(EthernetInterfacesInstance);
                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex.Message);
                        }

                    }


                }

                //Fans
                if (itm.oDataContext == "Thermal")
                {
                    //ADD LINQ TO COLLECTION QUERY TO EXTRACT PROPERTY VALUES 
                    //Capture members count to use in for loop
                    int FansCount = Convert.ToInt32(itm.RedfishPairs.Where(x => x.Key == "Fans@odata.count").FirstOrDefault()?.Value);

                    //Get fan rollup health
                    LenovoServer.FanRollupStatus = itm.RedfishPairs.Where(x => x.Key == "Status.HealthRollup").FirstOrDefault()?.Value;

                    for (int i = 0; i < FansCount; i++)
                    {
                        try
                        {
                            //Create a new instance of Fans
                            LenovoFans FansInstance = new LenovoFans();
                            //Use LINQ to populate properties use ? to check for null before value
                            FansInstance.ElementName = itm.RedfishPairs.Where(x => x.Key == "Fans[" + i.ToString() + "].Name").FirstOrDefault()?.Value;
                            FansInstance.Location = itm.RedfishPairs.Where(x => x.Key == "Fans[" + i.ToString() + "].Location.PartLocation.LocationType").FirstOrDefault()?.Value;
                            FansInstance.CurrentSpeed = itm.RedfishPairs.Where(x => x.Key == "Fans[" + i.ToString() + "].Reading").FirstOrDefault()?.Value;
                            FansInstance.ReadingUnits = itm.RedfishPairs.Where(x => x.Key == "Fans[" + i.ToString() + "].ReadingUnits").FirstOrDefault()?.Value;
                            FansInstance.State = itm.RedfishPairs.Where(x => x.Key == "Fans[" + i.ToString() + "].Status.State").FirstOrDefault()?.Value;
                            FansInstance.HotPluggable = itm.RedfishPairs.Where(x => x.Key == "Fans[" + i.ToString() + "].HotPluggable").FirstOrDefault()?.Value;
                            FansInstance.HealthState = itm.RedfishPairs.Where(x => x.Key == "Fans[" + i.ToString() + "].Status.Health").FirstOrDefault()?.Value;


                            //Add instance to collection
                            LenovoServer.LenovoFanCollection.Add(FansInstance);

                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex.Message);
                        }

                    }

                    // Temperatures

                    //Capture members count to use in for loop
                    int TemperatureCount = Convert.ToInt32(itm.RedfishPairs.Where(x => x.Key == "Temperatures@odata.count").FirstOrDefault()?.Value);


                    //Get fan rollup health
                    LenovoServer.TempRollupStatus = itm.RedfishPairs.Where(x => x.Key == "Status.HealthRollup").FirstOrDefault()?.Value;

                    for (int i = 0; i < TemperatureCount; i++)
                    {
                        try
                        {
                            //Create a new instance of Temp
                            LenovoBMC TempInstance = new LenovoBMC();
                            //Use LINQ to populate properties use ? to check for null before value
                            TempInstance.ElementName = itm.RedfishPairs.Where(x => x.Key == "Temperatures[" + i.ToString() + "].Name").FirstOrDefault()?.Value;
                            TempInstance.PhysicalContext = itm.RedfishPairs.Where(x => x.Key == "Temperatures[" + i.ToString() + "].PhysicalContext").FirstOrDefault()?.Value;
                            TempInstance.Sensor = itm.RedfishPairs.Where(x => x.Key == "Temperatures[" + i.ToString() + "].SensorNumber").FirstOrDefault()?.Value;
                            TempInstance.BaseUnit = "Celsius";
                            TempInstance.CurrentReading = itm.RedfishPairs.Where(x => x.Key == "Temperatures[" + i.ToString() + "].ReadingCelsius").FirstOrDefault()?.Value;
                            TempInstance.Critical = itm.RedfishPairs.Where(x => x.Key == "Temperatures[" + i.ToString() + "].UpperThresholdCritical").FirstOrDefault()?.Value;


                            //Add instance to collection
                            LenovoServer.LenovoBMCCollection.Add(TempInstance);

                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex.Message);
                        }

                    }



                }


                //Power
                if (itm.oDataContext == "Power")
                {
                    //ADD LINQ TO COLLECTION QUERY TO EXTRACT PROPERTY VALUES 
                    //Capture members count to use in for loop
                    int PowerCount = Convert.ToInt32(itm.RedfishPairs.Where(x => x.Key == "PowerSupplies@odata.count").FirstOrDefault()?.Value);


                    ////Get Power rollup health
                    //LenovoServer.PSRollupStatus = itm.RedfishPairs.Where(x => x.Key == "Redundancy[0].Status.Health").FirstOrDefault()?.Value;

                    for (int i = 0; i < PowerCount; i++)
                    {
                        try
                        {
                            //Create a new instance of Power
                            LenovoPower PowerInstance = new LenovoPower();
                            //Use LINQ to populate properties use ? to check for null before value
                            PowerInstance.ElementName = itm.RedfishPairs.Where(x => x.Key == "PowerSupplies[" + i.ToString() + "].Name").FirstOrDefault()?.Value;
                            PowerInstance.TotalOutputPower = itm.RedfishPairs.Where(x => x.Key == "PowerSupplies[" + i.ToString() + "].PowerCapacityWatts").FirstOrDefault()?.Value;
                            PowerInstance.HotPluggable = itm.RedfishPairs.Where(x => x.Key == "PowerSupplies[" + i.ToString() + "].HotPluggable").FirstOrDefault()?.Value;
                            PowerInstance.Model = itm.RedfishPairs.Where(x => x.Key == "PowerSupplies[" + i.ToString() + "].Model").FirstOrDefault()?.Value;
                            PowerInstance.Manufacturer = itm.RedfishPairs.Where(x => x?.Key == "PowerSupplies[" + i.ToString() + "].Manufacturer").FirstOrDefault()?.Value;
                            PowerInstance.PowerType = itm.RedfishPairs.Where(x => x.Key == "PowerSupplies[" + i.ToString() + "].PowerSupplyType").FirstOrDefault()?.Value;
                            PowerInstance.HealthState = itm.RedfishPairs.Where(x => x.Key == "PowerSupplies[" + i.ToString() + "].Status.Health").FirstOrDefault()?.Value;

                            //Add instance to collection
                            LenovoServer.LenovoPowerCollection.Add(PowerInstance);




                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex.Message);
                        }



                        //Power Health at a glance
                        var PowerHealthRollUp = LenovoServer.LenovoPowerCollection.Where(e => e.HealthState != "OK").ToList();
                        //Check if different value found, if so display it
                        if (PowerHealthRollUp.Count > 0)
                        {
                            foreach (var item in PowerHealthRollUp)
                            {
                                LenovoServer.PSRollupStatus = item.HealthState;

                            }
                        }
                        else
                        {
                            LenovoServer.PSRollupStatus = "OK";

                        }

                    }
                }


                //Storage
                if (itm.oDataContext == "Storage")
                {
                    //ADD LINQ TO COLLECTION QUERY TO EXTRACT PROPERTY VALUES 
                    //Capture members count to use in for loop
                    int StorageCount = Convert.ToInt32(itm.RedfishPairs.Where(x => x.Key == "Members@odata.count").FirstOrDefault()?.Value);


                    for (int i = 0; i < StorageCount; i++)
                    {
                        try
                        {
                            //Create a new instance of Storage
                            LenovoStorage StorageInstance = new LenovoStorage();
                            //Use LINQ to populate properties use ? to check for null before value
                            StorageInstance.Name = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Name").FirstOrDefault()?.Value;
                            StorageInstance.SerialNumber = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].StorageControllers[0].SerialNumber").FirstOrDefault()?.Value;
                            StorageInstance.Model = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].StorageControllers[0].Model").FirstOrDefault()?.Value;
                            StorageInstance.Manufacturer = itm.RedfishPairs.Where(x => x?.Key == "Members[" + i.ToString() + "].StorageControllers[0].Manufacturer").FirstOrDefault()?.Value;
                            StorageInstance.PartNumber = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].StorageControllers[0].PartNumber").FirstOrDefault()?.Value;
                            StorageInstance.UUID = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].StorageControllers[0].Identifiers[0].DurableName").FirstOrDefault()?.Value;
                            StorageInstance.Speed = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].StorageControllers[0].SpeedGbps").FirstOrDefault()?.Value;
                            StorageInstance.Mode = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].StorageControllers[0].Oem.Lenovo.Mode").FirstOrDefault()?.Value;
                            StorageInstance.Firmware = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].StorageControllers[0].FirmwareVersion").FirstOrDefault()?.Value;
                            StorageInstance.SKU = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].StorageControllers[0].SKU").FirstOrDefault()?.Value;
                            StorageInstance.Location = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].StorageControllers[0].Location.Info").FirstOrDefault()?.Value;
                            StorageInstance.Health = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Status.Health").FirstOrDefault()?.Value;
                            //Get Storage rollup health
                            LenovoServer.StorageRollupStatus = itm.RedfishPairs.Where(x => x.Key == "Members[" + i.ToString() + "].Status.HealthRollup").FirstOrDefault()?.Value;

                            //Add instance to collection
                            LenovoServer.LenovoStorageCollection.Add(StorageInstance);
                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine(ex.Message);
                        }

                    }
                }

                //Drives

                //Firmware

                //Temperatures




                //Loop through key value and add to console window
                foreach (var keyvalue in itm.RedfishPairs)
                {


                    Console.WriteLine(keyvalue.Key + " = " + keyvalue.Value);

                }



            }


            ////ROLLUP HEALTH STATUS

            //if (DellServer.DELLThermalCollection.Count > 0)
            //{
            //    foreach (var item in DellServer.DELLThermalCollection)
            //    {
            //        if (item.Health == "")
            //        {
            //            //if blank, set as ok
            //            DellServer.TempRollupStatus = "OK";
            //        }
            //        else
            //        {
            //            DellServer.TempRollupStatus = item.Health;
            //        }

            //    }
            //}
            //else
            //{
            //    DellServer.TempRollupStatus = "OK";

            //}
            ////FANS
            //if (DellServer.DELLFanCollection.Count > 0)
            //{
            //    foreach (var item in DellServer.DELLFanCollection)
            //    {
            //        if (item.PrimaryStatus == "")
            //        {
            //            //if blank, set as ok
            //            DellServer.FanRollupStatus = "OK";
            //        }
            //        else
            //        {
            //            DellServer.FanRollupStatus = item.PrimaryStatus;
            //        }

            //    }
            //}
            //else
            //{
            //    DellServer.FanRollupStatus = "OK";

            //}

            ////Power Health at a glance
            //var PowerHealthRollUp = DellServer.DELLPowerCollection.Where(e => e.PrimaryStatus != "OK").ToList();
            ////Check if different value found, if so display it
            //if (PowerHealthRollUp.Count > 0)
            //{
            //    foreach (var item in PowerHealthRollUp)
            //    {
            //        DellServer.PSRollupStatus = item.PrimaryStatus;

            //    }
            //}
            //else
            //{
            //    DellServer.PSRollupStatus = "OK";

            //}




            ////Count populated memory slots
            //DellServer.PopulatedDIMMSlots = DellServer.DELLMemoryCollection.Count.ToString();





            //Return server object
            return LenovoServer;
        }

        public void GetIBMData()
        {

        }

        public async Task<DELLServerInfo> GetCiscoData(string ServerIP, string PasswordOverride, MainViewModel passedvm, string ServerGen)
        {



            return null;
        }



        //Async delay
        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }


    }

    public class DellXAuthKey
    {
        //Hold Extracted values
        public string XAuthKey { get; set; }
        public string Location { get; set; }
    }

}


////PUPPETEER HEADLESS BROWSER
////embedded license for gen 12 & 13 capture using puppeteer headless browser
//string fullUrl = "https://" + ServerIP + "/login.html";

//List<string> programmerLinks = new List<string>();

//var options = new LaunchOptions()
//{
//    Headless = true, // set to true to hide browser ?console
//    IgnoreHTTPSErrors = true,
//    Args = new[] { "--disable-extensions", "--disable-popups", "--disable-web-security", "--disable-features=IsolateOrigins,site-per-process", },
//    ExecutablePath = @"C:\Program Files\Google\Chrome\Application\chrome.exe"
//};


//var browser = await Puppeteer.LaunchAsync(options);
//var page = await browser.NewPageAsync();
//await page.GoToAsync(fullUrl);

//await PutTaskDelay(2000);

////var pageHeaderHandle = await page.QuerySelectorAsync("div");
////var innerTextHandle = await pageHeaderHandle.GetPropertyAsync("prodClassName");
////var innerText = await innerTextHandle.JsonValueAsync();

//string content = await page.GetContentAsync();

////Select from content
//switch (content)
//{
//    case var s when content.Contains("prodClassName\">Basic"):
//        passedvm.ILORestOutput1 += "Embedded License Model = Basic\n\n";
//        break;
//    case var s when content.Contains("prodClassName\">Express"):
//        passedvm.ILORestOutput1 += "Embedded License Model = Express\n\n";
//        break;
//    case var s when content.Contains("prodClassName\">Enterprise"):
//        passedvm.ILORestOutput1 += "Embedded License Model = Enterprise\n\n";
//        break;
//}

////Grab a screenshot of the page
////await page.ScreenshotAsync(@"C:\Users\n.myers\Documents\DellScreenshot.png");

////End of embedded license capture
///


