﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nebula.Helpers;

namespace Nebula.Models
{

    [Serializable]

    public class WeekCommencing
    {
        public WeekCommencing()
        {
            WeeklyItems = new ObservableCollection<WeeklyServerRecordsModel>();
        }

        public DateTime Week { get; set; }

        public string FileName { get; set; }

        public int BuildTotals { get; set; }

        public int MotherboardTotals { get; set; }

        public int CTOTotals { get; set; }

        public ObservableCollection<WeeklyServerRecordsModel> WeeklyItems { get; set; }

    }


    public class WeeklyServerRecordsModel : HasPropertyChanged
    {
       
        
        private int _id;

        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged("id");
            }
        }

        // public DateTime Day { get; set; }
        //Property for current windows user
        private DateTime _Day;

        public DateTime Day
        {
            get { return _Day; }
            set
            {
                _Day = value;
                OnPropertyChanged("Day");
            }
        }

       
        private string _SONumber;

        public string SONumber
        {
            get { return _SONumber; }
            set
            {
                _SONumber = value;
                OnPropertyChanged("SONumber");
            }
        }

        private string _ServerModel;

        public string ServerModel
        {
            get { return _ServerModel; }
            set
            {
                _ServerModel = value;
                OnPropertyChanged("ServerModel");
            }
        }


        private int _Quantity;

        public int Quantity
        {
            get { return _Quantity; }
            set
            {
                _Quantity = value;
                OnPropertyChanged("Quantity");
            }
        }


        private int _BuildQuantity;

        public int BuildQuantity
        {
            get { return _BuildQuantity; }
            set
            {
                _BuildQuantity = value;
                OnPropertyChanged("BuildQuantity");
            }
        }

        private int _BuildFWQuantity;

        public int BuildFWQuantity
        {
            get { return _BuildFWQuantity; }
            set
            {
                _BuildFWQuantity = value;
                OnPropertyChanged("BuildFWQuantity");
            }
        }

        private int _BuildTestQuantity;

        public int BuildTestQuantity
        {
            get { return _BuildTestQuantity; }
            set
            {
                _BuildTestQuantity = value;
                OnPropertyChanged("BuildTestQuantity");
            }
        }


        private int _CPUTestQuantity;

        public int CPUTestQuantity
        {
            get { return _CPUTestQuantity; }
            set
            {
                _CPUTestQuantity = value;
                OnPropertyChanged("CPUTestQuantity");
            }
        }


        private int _CPUZQuantity;

        public int CPUZQuantity
        {
            get { return _CPUZQuantity; }
            set
            {
                _CPUZQuantity = value;
                OnPropertyChanged("CPUZQuantity");
            }
        }


        private int _CTOQuantity;

        public int CTOQuantity
        {
            get { return _CTOQuantity; }
            set
            {
                _CTOQuantity = value;
                OnPropertyChanged("CTOQuantity");
            }
        }

        private int _DIMMTestQuantity;

        public int DIMMTestQuantity
        {
            get { return _DIMMTestQuantity; }
            set
            {
                _DIMMTestQuantity = value;
                OnPropertyChanged("DIMMTestQuantity");
            }
        }


        private int _IOBoardQuantity;

        public int IOBoardQuantity
        {
            get { return _IOBoardQuantity; }
            set
            {
                _IOBoardQuantity = value;
                OnPropertyChanged("IOBoardQuantity");
            }
        }


        private int _MotherboardQuantity;

        public int MotherboardQuantity
        {
            get { return _MotherboardQuantity; }
            set
            {
                _MotherboardQuantity = value;
                OnPropertyChanged("MotherboardQuantity");
            }
        }


        private int _RAMReplaceQuantity;

        public int RAMReplaceQuantity
        {
            get { return _RAMReplaceQuantity; }
            set
            {
                _RAMReplaceQuantity = value;
                OnPropertyChanged("RAMReplaceQuantity");
            }
        }


        private int _RMAReplaceBuildQuantity;

        public int RMAReplaceBuildQuantity
        {
            get { return _RMAReplaceBuildQuantity; }
            set
            {
                _RMAReplaceBuildQuantity = value;
                OnPropertyChanged("RMAReplaceBuildQuantity");
            }
        }

        private int _RMATotals;

        public int RMATotals
        {
            get { return _RMATotals; }
            set
            {
                _RMATotals = value;
                OnPropertyChanged("RMATotals");
            }
        }

        private int _PlugAndPlayQuantity;

        public int PlugAndPlayQuantity
        {
            get { return _PlugAndPlayQuantity; }
            set
            {
                _PlugAndPlayQuantity = value;
                OnPropertyChanged("PlugAndPlayQuantity");
            }
        }


        private int _POTestingQuantity;

        public int POTestingQuantity
        {
            get { return _POTestingQuantity; }
            set
            {
                _POTestingQuantity = value;
                OnPropertyChanged("POTestingQuantity");
            }
        }

        private int _PartTestingQuantity;

        public int PartTestingQuantity
        {
            get { return _PartTestingQuantity; }
            set
            {
                _PartTestingQuantity = value;
                OnPropertyChanged("PartTestingQuantity");
            }
        }


        private int _ClientGearQuantity;

        public int ClientGearQuantity
        {
            get { return _ClientGearQuantity; }
            set
            {
                _ClientGearQuantity = value;
                OnPropertyChanged("ClientGearQuantity");
            }
        }


        private string _Technician;

        public string Technician
        {
            get { return _Technician; }
            set
            {
                _Technician = value;
                OnPropertyChanged("Technician");
            }
        }
        private string _SalesRep;

        public string SalesRep
        {
            get { return _SalesRep; }
            set
            {
                _SalesRep = value;
                OnPropertyChanged("SalesRep");
            }
        }
        private string _JobType;

        public string JobType
        {
            get { return _JobType; }
            set
            {
                _JobType = value;
                OnPropertyChanged("JobType");
            }
        }


        private string _Issues;

        public string Issues
        {
            get { return _Issues; }
            set
            {
                _Issues = value;
                OnPropertyChanged("Issues");
            }
        }
        private string _ActionTaken;

        public string ActionTaken
        {
            get { return _ActionTaken; }
            set
            {
                _ActionTaken = value;
                OnPropertyChanged("ActionTaken");
            }
        }
        private string _Status;

        public string Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
                OnPropertyChanged("Status");
            }
        }



        //Returns string value of count
        public string ReturnJobTypes()
        {

            string jobType = "";

            if (BuildQuantity > 0)
            {
                jobType += "Build ";
            }

            if (BuildFWQuantity > 0)
            {
                jobType += "Build FW ";
            }

            if (BuildTestQuantity > 0)
            {
                jobType += "Build Test ";
            }

            if (CPUTestQuantity > 0)
            {
                jobType += "CPU Test ";
            }

            if (CPUZQuantity > 0)
            {
                jobType += "CPUZ ";
            }

            if (CTOQuantity > 0)
            {
                jobType += "CTO ";
            }

            if (DIMMTestQuantity > 0)
            {
                jobType += "DIMM Test ";
            }

            if (IOBoardQuantity > 0)
            {
                jobType += "IO Board ";
            }

            if (MotherboardQuantity > 0)
            {
                jobType += "Motherboard ";
            }

            if (RAMReplaceQuantity > 0)
            {
                jobType += "RAM Replace ";
            }

            if (RMAReplaceBuildQuantity > 0)
            {
                jobType += "RMA Build ";
            }

            if (PlugAndPlayQuantity > 0)
            {
                jobType += "Plug & Play ";
            }

            if (POTestingQuantity > 0)
            {
                jobType += "PO Testing ";
            }

            if (PartTestingQuantity > 0)
            {
                jobType += "Part Testing ";
            }

            if (ClientGearQuantity > 0)
            {
                jobType += "Client Gear ";
               
            }

            return jobType;

        }



      

    }

   

  

}
