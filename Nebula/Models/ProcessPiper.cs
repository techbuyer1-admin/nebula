﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Nebula.Commands;
using Nebula.DellServers;
using Nebula.Helpers;
using Nebula.ViewModels;

namespace Nebula.Models
{
    public class ProcessPiper
    {
        public string StdOut { get; private set; }
        public string StdErr { get; private set; }
        public string ExMessage { get; set; }
        //declare viewmodel
        MainViewModel vm;
        public TextBox txtBX = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt") as TextBox;
        public TextBox txtBXF = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxtFinal") as TextBox;
        public TextBox txtBX1 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt1") as TextBox;
        public TextBox txtBX1F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt1Final") as TextBox;
        public TextBox txtBX2 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt2") as TextBox;
        public TextBox txtBX2F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt2Final") as TextBox;
        public TextBox txtBX3 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt3") as TextBox;
        public TextBox txtBX3F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt3Final") as TextBox;
        public TextBox txtBX4 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt4") as TextBox;
        public TextBox txtBX4F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt4Final") as TextBox;
        public TextBox txtBX5 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt5") as TextBox;
        public TextBox txtBX5F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt5Final") as TextBox;
        public TextBox txtBX6 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt6") as TextBox;
        public TextBox txtBX6F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt6Final") as TextBox;
        public TextBox txtBX7 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt7") as TextBox;
        public TextBox txtBX7F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt7Final") as TextBox;
        public TextBox txtBX8 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt8") as TextBox;
        public TextBox txtBX8F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt8Final") as TextBox;
        public TextBox txtBX9 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt9") as TextBox;
        public TextBox txtBX9F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt9Final") as TextBox;
        public TextBox txtBX10 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt10") as TextBox;
        public TextBox txtBX10F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt10Final") as TextBox;
        public TextBox txtBX11 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt11") as TextBox;
        public TextBox txtBX11F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt11Final") as TextBox;
        public TextBox txtBX12 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt12") as TextBox;
        public TextBox txtBX12F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt12Final") as TextBox;
        public TextBox txtBX13 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt13") as TextBox;
        public TextBox txtBX13F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt13Final") as TextBox;
        public TextBox txtBX14 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt14") as TextBox;
        public TextBox txtBX14F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt14Final") as TextBox;
        public TextBox txtBX15 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt15") as TextBox;
        public TextBox txtBX15F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt15Final") as TextBox;
        public TextBox txtBX16 = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt16") as TextBox;
        public TextBox txtBX16F = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt16Final") as TextBox;


        //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData), ConsoleIncoming, para, FlowDoc, ConsoleRTB);
        public ProcessPiper()
        {

        }

        public ProcessPiper(MainViewModel PassedViewModel)
        {
            //get the passed in viewmodel and assign to the local vm variable
            vm = PassedViewModel;

        }
        public void ScrollToEnd()
        {
            if (txtBX != null)
                txtBX.ScrollToEnd();
            if (txtBXF != null)
                txtBXF.ScrollToEnd();
            if (txtBX1 != null)
                txtBX1.ScrollToEnd();
            if (txtBX1F != null)
                txtBX1F.ScrollToEnd();
            if (txtBX2 != null)
                txtBX2.ScrollToEnd();
            if (txtBX2F != null)
                txtBX2F.ScrollToEnd();
            if (txtBX3 != null)
                txtBX3.ScrollToEnd();
            if (txtBX3F != null)
                txtBX3F.ScrollToEnd();
            if (txtBX4 != null)
                txtBX4.ScrollToEnd();
            if (txtBX4F != null)
                txtBX4F.ScrollToEnd();
            if (txtBX5 != null)
                txtBX5.ScrollToEnd();
            if (txtBX5F != null)
                txtBX5F.ScrollToEnd();
            if (txtBX6 != null)
                txtBX6.ScrollToEnd();
            if (txtBX6F != null)
                txtBX6F.ScrollToEnd();
            if (txtBX7 != null)
                txtBX7.ScrollToEnd();
            if (txtBX7F != null)
                txtBX7F.ScrollToEnd();
            if (txtBX8 != null)
                txtBX8.ScrollToEnd();
            if (txtBX8F != null)
                txtBX8F.ScrollToEnd();
            if (txtBX9 != null)
                txtBX9.ScrollToEnd();
            if (txtBX9F != null)
                txtBX9F.ScrollToEnd();
            if (txtBX10 != null)
                txtBX10.ScrollToEnd();
            if (txtBX10F != null)
                txtBX10F.ScrollToEnd();
            if (txtBX11 != null)
                txtBX11.ScrollToEnd();
            if (txtBX11F != null)
                txtBX11F.ScrollToEnd();
            if (txtBX12 != null)
                txtBX12.ScrollToEnd();
            if (txtBX12F != null)
                txtBX12F.ScrollToEnd();
            if (txtBX13 != null)
                txtBX13.ScrollToEnd();
            if (txtBX13F != null)
                txtBX13F.ScrollToEnd();
            if (txtBX14 != null)
                txtBX14.ScrollToEnd();
            if (txtBX14F != null)
                txtBX14F.ScrollToEnd();
            if (txtBX15 != null)
                txtBX15.ScrollToEnd();
            if (txtBX15F != null)
                txtBX15F.ScrollToEnd();
            if (txtBX16 != null)
                txtBX16.ScrollToEnd();
            if (txtBX16F != null)
                txtBX16F.ScrollToEnd();
        }


        //delegate
        public delegate void UpdateUiTextDelegate();


        public void Start(FileInfo exe, string args, string command, string workingDir)
        {
          
            //vm.CMDOutput.Clear();

            using (Process process = new Process())
            {
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                // Redirects the standard input so that commands can be sent to the shell.
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
                process.EnableRaisingEvents = true;

                // Runs the specified command and exits the shell immediately.
                process.StartInfo.Arguments = args;

                //Event Wiring
                process.OutputDataReceived += ProcessOutputDataHandler;
                process.ErrorDataReceived += ProcessErrorDataHandler;
                //process.OutputDataReceived += SortOutputHandler;
                //process.ErrorDataReceived += SortOutputHandler;
                //process.Exited += process_Exited;
                process.Exited += new System.EventHandler(process_Exited);
                //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
                process.Start();
                // Send a directory command and an exit command to the shell
                process.StandardInput.WriteLine(command);
               
                //Asyncronus
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();


                process.StandardInput.WriteLine("exit");
                process.WaitForExit();

              
              
                process.Close();
                //process.ExitCode();
            
            }

        }





        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

     

        public void ProcessOutputDataHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
           
            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);
            
            vm.CMDOutput += outLine.Data + "\r";
            //Console.WriteLine(txtBX.Text + "\n\r");
            //vm.ServerState += outLine.Data + "\r";
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            // txtBX.ScrollToEnd();
            //txtOutput.Clear();
            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void ProcessErrorDataHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //var txtBX = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt") as TextBox;
            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);
            
            vm.CMDOutput += outLine.Data + "\r";
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //Console.WriteLine(txtBX.Text + "\n\r");


            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void process_Exited(object sender, EventArgs e)
        {
            if(vm.CMDOutput.Contains("state:"))
            {
                int i = vm.CMDOutput.IndexOf("state:");
                vm.ServerState = vm.CMDOutput.Substring(i);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }
            else
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }

            //MessageBox.Show("Process Complete"); 
        }


        //GENERIC

        //1
        public async void StartILORestGeneric(FileInfo exe, string command, string args, string workingDir)
        {

            vm.CMDOutput = "";
            vm.CommandInstruction = "";
            //vm.CMDOutput.Clear();

            using (Process process = new Process())
            {
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                // Redirects the standard input so that commands can be sent to the shell.
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
                process.EnableRaisingEvents = true;

                // Runs the specified command and exits the shell immediately.
                process.StartInfo.Arguments = args;

                //Event Wiring
                process.OutputDataReceived += ILORestProcessOutputDataHandlerGeneric;
                process.ErrorDataReceived += ILORestProcessErrorDataHandlerGeneric;
                //process.OutputDataReceived += SortOutputHandler;
                //process.ErrorDataReceived += SortOutputHandler;
                //process.Exited += process_Exited;
                process.Exited += new System.EventHandler(ILORestprocess_ExitedGeneric);
                //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
                process.Start();

                // kill remote console workaround for gen 8 hang
                if (args.Contains("biosdefaults"))
                {
                    await PutTaskDelay(2000);
                    //StaticFunctions.KillProcess("HPLOCONS");
                }
                {
                    // Send a directory command and an exit command to the shell
                    process.StandardInput.WriteLine(command);

                }


                // Send a directory command and an exit command to the shell
                process.StandardInput.WriteLine("exit");
                // Runs the specified command and exits the shell immediately.
                //process.StartInfo.Arguments = args;




                //process.StandardInput.WriteLine(login);
                //await PutTaskDelay(6000);



                //Asyncronus
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();



                //process.StandardInput.WriteLine("get");

                //process.StandardInput.WriteLine("exit");
                process.WaitForExit();



                process.Close();
                //process.ExitCode();

            }


        }






        //multi server support

        public void ILORestProcessOutputDataHandlerGeneric(object sendingProcess, DataReceivedEventArgs outLine)
        {

            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            //vm.ILORestOutput += outLine.Data + "\r";
            vm.ILORestOutput1 += outLine.Data + "\r";
            //vm.ILORestOutput2 += outLine.Data + "\r";
            //Console.WriteLine(txtBX.Text + "\n\r");
            //vm.ServerState += outLine.Data + "\r";
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //txtOutput.Clear();
            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void ILORestProcessErrorDataHandlerGeneric(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //var txtBX = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt") as TextBox;
            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            //vm.ILORestOutput += outLine.Data + "\r";
            vm.ILORestOutput1 += outLine.Data + "\r";
            // vm.ILORestOutput2 += outLine.Data + "\r";

            //Console.WriteLine(txtBX.Text + "\n\r");
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));

            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void ILORestprocess_ExitedGeneric(object sender, EventArgs e)
        {
            if (vm.ILORestOutput1.Contains("state:"))
            {
                int i = vm.ILORestOutput1.IndexOf("state:");
                vm.ServerState1 = vm.ILORestOutput1.Substring(i);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }
            else
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }

            //MessageBox.Show("Process Complete"); 
        }


        //






        public async void StartILORest1(FileInfo exe, string command, string args, string workingDir)
        {

            vm.CMDOutput = "";
            vm.CommandInstruction = "";
            //vm.CMDOutput.Clear();

            using (Process process = new Process())
            {
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                // Redirects the standard input so that commands can be sent to the shell.
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
                process.EnableRaisingEvents = true;

                // Runs the specified command and exits the shell immediately.
                process.StartInfo.Arguments = args;

                //Event Wiring
                process.OutputDataReceived += ILORestProcessOutputDataHandler1;
                process.ErrorDataReceived += ILORestProcessErrorDataHandler1;
                //process.OutputDataReceived += SortOutputHandler;
                //process.ErrorDataReceived += SortOutputHandler;
                //process.Exited += process_Exited;
                process.Exited += new System.EventHandler(ILORestprocess_Exited1);
                //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
                process.Start();

                // kill remote console workaround for gen 8 hang
                if (args.Contains("biosdefaults"))
                {
                    await PutTaskDelay(2000);
                    //StaticFunctions.KillProcess("HPLOCONS");
                    CloseAppByPid(vm.HPRemoteConsolePIDS1);
                }
                else
                {
                    // Send a directory command and an exit command to the shell
                    process.StandardInput.WriteLine(command);

                }


                // Send a directory command and an exit command to the shell
                process.StandardInput.WriteLine("exit");
                // Runs the specified command and exits the shell immediately.
                //process.StartInfo.Arguments = args;




                //process.StandardInput.WriteLine(login);
                //await PutTaskDelay(6000);



                //Asyncronus
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();



                //process.StandardInput.WriteLine("get");

                //process.StandardInput.WriteLine("exit");
                process.WaitForExit();



                process.Close();
                //process.ExitCode();

            }


        }








        public void ILORestProcessOutputDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {


            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            //vm.ILORestOutput += outLine.Data + "\r";
            vm.ILORestOutput1 += outLine.Data + "\r";
            //vm.ILORestOutput2 += outLine.Data + "\r";
            //Console.WriteLine(txtBX.Text + "\n\r");
            //vm.ServerState += outLine.Data + "\r";
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //txtOutput.Clear();
            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void ILORestProcessErrorDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //var txtBX = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt") as TextBox;
            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            //vm.ILORestOutput += outLine.Data + "\r";
            vm.ILORestOutput1 += outLine.Data + "\r";
            // vm.ILORestOutput2 += outLine.Data + "\r";

            //Console.WriteLine(txtBX.Text + "\n\r");
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));

            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void ILORestprocess_Exited1(object sender, EventArgs e)
        {
            if (vm.ILORestOutput1.Contains("state:"))
            {
                int i = vm.ILORestOutput1.IndexOf("state:");
                vm.ServerState1 = vm.ILORestOutput1.Substring(i);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }
            else
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }

            //MessageBox.Show("Process Complete"); 
        }

        //  Close RC by ID
        private void CloseAppByPid(int processID)
        {
            try
            {
                Process proc = Process.GetProcessById(processID);
                if (proc.ProcessName.Contains("HPLOCONS"))
                {
                    proc.Kill();
                }

                if (proc.ProcessName.Contains("ilorest"))
                {
                    proc.Kill();
                }

            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                // Process already exited.
            }
        }



        //JUST FOR LAUNCHING A DOCUMENT
        public void StartExeProcess(string filepath, string args, string command)
        {
            
            ProcessStartInfo startInfo = new ProcessStartInfo();
            //startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            //startInfo.RedirectStandardOutput = true;
            //startInfo.RedirectStandardError = true;
            //startInfo.RedirectStandardInput = true;
            //startInfo.WorkingDirectory = @"C:\Windows\System32\"; W:\Warehouse General\TB Tools App\Documentation\TechBuyer Tools Application Guide V1.0.pdf
            //startInfo.FileName = @"" + StaticFunctions.UncPathToUse + @"nick\ghostscript\gswin64c.exe";
            startInfo.FileName = filepath;

            //startInfo.WindowStyle = ProcessWindowStyle.Hidden;


            //startInfo.Arguments = " -n 6";




            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                Process.Start(filepath);
                //using (Process exeProcess = Process.Start(startInfo))
                //{
                //    //exeProcess.StandardInput.WriteLine(command);
                //    //StdOut = exeProcess.StandardOutput.ReadToEnd();
                //    //StdErr = exeProcess.StandardError.ReadToEnd();

                //    //exeProcess.StandardInput.WriteLine("exit");

                  
                //    //exeProcess.WaitForExit();
                //}
            }
            catch
            {
                // Log error.
            }
        }



        //DELL RACDM COMMANDS
        //1
        public async void StartRACDM1(FileInfo exe, string command, string args, string workingDir)
        {

            vm.CMDOutput = "";
            vm.CommandInstruction = "";
            //vm.CMDOutput.Clear();

           

            using (Process process = new Process())
            {
                
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
               
                // Redirects the standard input so that commands can be sent to the shell.
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
                process.EnableRaisingEvents = true;

                // Runs the specified command and exits the shell immediately.
                process.StartInfo.Arguments = args;
                //RUN AS ADMINISTRATOR
                if (System.Environment.OSVersion.Version.Major >= 6)
                {
                    process.StartInfo.Verb = "runas";
                }
                //Event Wiring
                process.OutputDataReceived += RACDMProcessOutputDataHandler1;
                process.ErrorDataReceived += RACDMProcessErrorDataHandler1;
                //process.OutputDataReceived += SortOutputHandler;
                //process.ErrorDataReceived += SortOutputHandler;
                //process.Exited += process_Exited;
                process.Exited += new System.EventHandler(RACDMProcess_Exited1);
                //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
                process.Start();

                // kill remote console workaround for gen 8 hang
                if (args.Contains("biosdefaults"))
                {
                    await PutTaskDelay(2000);
                    //StaticFunctions.KillProcess("HPLOCONS");
                }
                {
                    // Send a directory command and an exit command to the shell
                    process.StandardInput.WriteLine(command);

                }


                // Send a directory command and an exit command to the shell
                process.StandardInput.WriteLine("exit");
                // Runs the specified command and exits the shell immediately.
                //process.StartInfo.Arguments = args;




                //process.StandardInput.WriteLine(login);
                //await PutTaskDelay(6000);



                //Asyncronus
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();



                //process.StandardInput.WriteLine("get");

                //process.StandardInput.WriteLine("exit");
                process.WaitForExit();



                process.Close();
                //process.ExitCode();

            }


        }

        //multi server support

        public void RACDMProcessOutputDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {

            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            //vm.ILORestOutput += outLine.Data + "\r";
            vm.ILORestOutput1 += outLine.Data + "\r";
            //vm.ILORestOutput2 += outLine.Data + "\r";
            //Console.WriteLine(txtBX.Text + "\n\r");
            //vm.ServerState += outLine.Data + "\r";
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //txtOutput.Clear();
            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void RACDMProcessErrorDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //var txtBX = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt") as TextBox;
            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            //vm.ILORestOutput += outLine.Data + "\r";
            vm.ILORestOutput1 += outLine.Data + "\r";
            // vm.ILORestOutput2 += outLine.Data + "\r";

            //Console.WriteLine(txtBX.Text + "\n\r");
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));

            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void RACDMProcess_Exited1(object sender, EventArgs e)
        {
            if (vm.ILORestOutput1.Contains("state:"))
            {
                int i = vm.ILORestOutput1.IndexOf("state:");
                vm.ServerState1 = vm.ILORestOutput1.Substring(i);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }
            else
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }

            //MessageBox.Show("Process Complete"); 
        }





        //ENCLOSURE
        public async void StartRACDMEnclosure(FileInfo exe, string command, string args, string workingDir)
        {

            //vm.CMDOutput = "";
            //vm.CommandInstruction = "";
            //vm.CMDOutput.Clear();



            using (Process process = new Process())
            {
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                // Redirects the standard input so that commands can be sent to the shell.
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
                process.EnableRaisingEvents = true;

                // Runs the specified command and exits the shell immediately.
                process.StartInfo.Arguments = args;

                //Event Wiring
                process.OutputDataReceived += RACDMProcessOutputDataHandlerEnclosure;
                process.ErrorDataReceived += RACDMProcessErrorDataHandlerEnclosure;
                //process.OutputDataReceived += SortOutputHandler;
                //process.ErrorDataReceived += SortOutputHandler;
                //process.Exited += process_Exited;
                process.Exited += new System.EventHandler(RACDMProcess_ExitedEnclosure);
                //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
                process.Start();

                // kill remote console workaround for gen 8 hang
                if (args.Contains("biosdefaults"))
                {
                    await PutTaskDelay(2000);
                    //StaticFunctions.KillProcess("HPLOCONS");
                }
                {
                    // Send a directory command and an exit command to the shell
                    process.StandardInput.WriteLine(command);

                }


                // Send a directory command and an exit command to the shell
                process.StandardInput.WriteLine("exit");
                // Runs the specified command and exits the shell immediately.
                //process.StartInfo.Arguments = args;




                //process.StandardInput.WriteLine(login);
                //await PutTaskDelay(6000);



                //Asyncronus
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();



                //process.StandardInput.WriteLine("get");

                //process.StandardInput.WriteLine("exit");
                process.WaitForExit();



                process.Close();
                //process.ExitCode();

            }


        }

        //multi server support

        public void RACDMProcessOutputDataHandlerEnclosure(object sendingProcess, DataReceivedEventArgs outLine)
        {

            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            //vm.ILORestOutput += outLine.Data + "\r";
            // vm.ILORestOutputEnclosure += outLine.Data + "\r";
            //vm.ILORestOutput2 += outLine.Data + "\r";
            //Console.WriteLine(txtBX.Text + "\n\r");
            //vm.ServerState += outLine.Data + "\r";
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //txtOutput.Clear();
            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void RACDMProcessErrorDataHandlerEnclosure(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //var txtBX = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt") as TextBox;
            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            //vm.ILORestOutput += outLine.Data + "\r";
            //  vm.ILORestOutputEnclosure += outLine.Data + "\r";
            // vm.ILORestOutput2 += outLine.Data + "\r";

            //Console.WriteLine(txtBX.Text + "\n\r");
            // Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));

            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void RACDMProcess_ExitedEnclosure(object sender, EventArgs e)
        {
            //if (vm.ILORestOutputEnclosure.Contains("state:"))
            //{
            //    int i = vm.ILORestOutputEnclosure.IndexOf("state:");
            //    vm.ServerState16 = vm.ILORestOutputEnclosure.Substring(i);
            //    Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //}
            //else
            //{
            //    Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //}

            //MessageBox.Show("Process Complete"); 
        }







        //DELL Hardware inventory
        //DELL RACDM COMMANDS
        //1
        //DELL RACDM COMMANDS
        //1
        public async void StartHardwareInventoryRACDM1(FileInfo exe, string command, string args, string workingDir)
        {

            vm.CMDOutput = "";
            vm.CommandInstruction = "";
            //vm.CMDOutput.Clear();

            using (Process process = new Process())
            {

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;

                // Redirects the standard input so that commands can be sent to the shell.
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
                process.EnableRaisingEvents = true;

                // Runs the specified command and exits the shell immediately.
                process.StartInfo.Arguments = args;
                //RUN AS ADMINISTRATOR
                if (System.Environment.OSVersion.Version.Major >= 6)
                {
                    process.StartInfo.Verb = "runas";
                }
                //Event Wiring
                process.OutputDataReceived += RACDMHardwareInventoryProcessOutputDataHandler1;
                process.ErrorDataReceived += RACDMHardwareInventoryProcessErrorDataHandler1;
                //process.OutputDataReceived += SortOutputHandler;
                //process.ErrorDataReceived += SortOutputHandler;
                //process.Exited += process_Exited;
                process.Exited += new System.EventHandler(RACDMHardwareInventoryProcess_Exited1);
                //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
                process.Start();

                // kill remote console workaround for gen 8 hang
                if (args.Contains("biosdefaults"))
                {
                    await PutTaskDelay(2000);
                    //StaticFunctions.KillProcess("HPLOCONS");
                }
                {
                    // Send a directory command and an exit command to the shell
                    process.StandardInput.WriteLine(command);

                }


                // Send a directory command and an exit command to the shell
                process.StandardInput.WriteLine("exit");
                // Runs the specified command and exits the shell immediately.
                //process.StartInfo.Arguments = args;




                //process.StandardInput.WriteLine(login);
                //await PutTaskDelay(6000);



                //Asyncronus
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();



                //process.StandardInput.WriteLine("get");

                //process.StandardInput.WriteLine("exit");
                process.WaitForExit();



                process.Close();
                //process.ExitCode();

            }


        }




        //HARDWARE INVENTORY PROCESSES

        //multi server support

        public void RACDMHardwareInventoryProcessOutputDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {

            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            vm.ILORestOutput1 += outLine.Data + "\r";
            vm.DELLHardwareInventory1 += outLine.Data + "\r";
            //vm.ILORestOutput2 += outLine.Data + "\r";
            //Console.WriteLine(txtBX.Text + "\n\r");
            //vm.ServerState += outLine.Data + "\r";
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //txtOutput.Clear();
            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void RACDMHardwareInventoryProcessErrorDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //var txtBX = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt") as TextBox;
            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            //vm.ILORestOutput += outLine.Data + "\r";
            vm.ILORestOutput1 += outLine.Data + "\r";
            vm.DELLHardwareInventory1 += outLine.Data + "\r";
            // vm.ILORestOutput2 += outLine.Data + "\r";

            //Console.WriteLine(txtBX.Text + "\n\r");
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));

            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void RACDMHardwareInventoryProcess_Exited1(object sender, EventArgs e)
        {
            if (vm.DELLHardwareInventory1.Contains("state:"))
            {
                int i = vm.DELLHardwareInventory1.IndexOf("state:");
                vm.ServerState1 = vm.DELLHardwareInventory1.Substring(i);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }
            else
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }

            //MessageBox.Show("Process Complete"); 
        }



       
        //SENSOR INVENTORY PROCESSES


        //1
        public async void StartSensorInventoryRACDM1(FileInfo exe, string command, string args, string workingDir)
        {
           
            vm.CMDOutput = "";
            vm.CommandInstruction = "";
            //vm.CMDOutput.Clear();

            using (Process process = new Process())
            {

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;

                // Redirects the standard input so that commands can be sent to the shell.
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
                process.EnableRaisingEvents = true;

                // Runs the specified command and exits the shell immediately.
                process.StartInfo.Arguments = args;
                //RUN AS ADMINISTRATOR
                if (System.Environment.OSVersion.Version.Major >= 6)
                {
                    process.StartInfo.Verb = "runas";
                }
                //Event Wiring
                process.OutputDataReceived += RACDMSensorInventoryProcessOutputDataHandler1;
                process.ErrorDataReceived += RACDMSensorInventoryProcessErrorDataHandler1;
                //process.OutputDataReceived += SortOutputHandler;
                //process.ErrorDataReceived += SortOutputHandler;
                //process.Exited += process_Exited;
                process.Exited += new System.EventHandler(RACDMSensorInventoryProcess_Exited1);
                //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
                process.Start();

                // kill remote console workaround for gen 8 hang
                if (args.Contains("biosdefaults"))
                {
                    await PutTaskDelay(2000);
                    //StaticFunctions.KillProcess("HPLOCONS");
                }
                {
                    // Send a directory command and an exit command to the shell
                    process.StandardInput.WriteLine(command);

                }


                // Send a directory command and an exit command to the shell
                process.StandardInput.WriteLine("exit");
                // Runs the specified command and exits the shell immediately.
                //process.StartInfo.Arguments = args;




                //process.StandardInput.WriteLine(login);
                //await PutTaskDelay(6000);



                //Asyncronus
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();



                //process.StandardInput.WriteLine("get");

                //process.StandardInput.WriteLine("exit");
                process.WaitForExit();



                process.Close();
                //process.ExitCode();

            }


        }
        //multi server support

        public void RACDMSensorInventoryProcessOutputDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {

            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            //vm.ILORestOutput += outLine.Data + "\r";
            //vm.ILORestOutput1 += outLine.Data + "\r";
            if (outLine.Data != null)
            {
              
                if (outLine.Data.Contains("#CurrentDisplay="))
                {
                    //Update LCD property
                    vm.DELLFrontLCDS1 = outLine.Data.Replace("#CurrentDisplay=", "");
                }
                else
                {
                    //Update powerstate with getremoteservicestatus command
                   vm.DELLSystemStatusS1 = outLine.Data;
                }
                   
            }
         

            //vm.ILORestOutput2 += outLine.Data + "\r";
            //Console.WriteLine(txtBX.Text + "\n\r");
            //vm.ServerState += outLine.Data + "\r";
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //txtOutput.Clear();
            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void RACDMSensorInventoryProcessErrorDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //var txtBX = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt") as TextBox;
            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            //vm.ILORestOutput += outLine.Data + "\r";
            // vm.ILORestOutput1 += outLine.Data + "\r";
            if (outLine.Data != null)
            {

                if (outLine.Data.Contains("#CurrentDisplay="))
                {
                    //Update LCD property
                    vm.DELLFrontLCDS1 = outLine.Data.Replace("#CurrentDisplay=", "");
                }
                else
                {
                    //Update powerstate with getremoteservicestatus command
                    vm.DELLSystemStatusS1 = outLine.Data;
                }

            }


            // vm.ILORestOutput2 += outLine.Data + "\r";

            //Console.WriteLine(txtBX.Text + "\n\r");
           // Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));

            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void RACDMSensorInventoryProcess_Exited1(object sender, EventArgs e)
        {
            //if (vm.DELLSensorInventory1.Contains("state:"))
            //{
            //    int i = vm.DELLSensorInventory1.IndexOf("state:");
            //    vm.ServerState1 = vm.DELLSensorInventory1.Substring(i);
            //    Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //}
            //else
            //{
            //    Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //}

           // Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));

            //MessageBox.Show("Process Complete"); 
        }





        //////1
        //public void StartRACDMGeneric1(FileInfo exe, string command, string args, string workingDir)
        //{

        //    vm.CMDOutput = "";
        //    vm.CommandInstruction = "";
        //    //vm.CMDOutput.Clear();

        //    using (Process process = new Process())
        //    {

        //        process.StartInfo.UseShellExecute = false;
        //        process.StartInfo.CreateNoWindow = true;
        //        process.StartInfo.RedirectStandardOutput = true;
        //        process.StartInfo.RedirectStandardError = true;

        //        // Redirects the standard input so that commands can be sent to the shell.
        //        process.StartInfo.RedirectStandardInput = true;
        //        process.StartInfo.WorkingDirectory = workingDir;
        //        process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
        //        process.EnableRaisingEvents = true;

        //        // Runs the specified command and exits the shell immediately.
        //        process.StartInfo.Arguments = args;
        //        //RUN AS ADMINISTRATOR
        //        if (System.Environment.OSVersion.Version.Major >= 6)
        //        {
        //            process.StartInfo.Verb = "runas";
        //        }

        //        //process.OutputDataReceived += GenericProcessOutputDataHandler1;
        //        //process.ErrorDataReceived += GenericProcessErrorDataHandler1;
        //        ////process.OutputDataReceived += SortOutputHandler;
        //        ////process.ErrorDataReceived += SortOutputHandler;
        //        ////process.Exited += process_Exited;
        //        //process.Exited += new System.EventHandler(GenericProcess_Exited1);


        //        //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
        //        process.Start();

        //        //Asyncronus
        //        process.BeginOutputReadLine();
        //        process.BeginErrorReadLine();

        //        process.WaitForExit();
        //        process.Close();
        //        //process.ExitCode();

        //    }


        //}



        //Obtain return data for Blade IP Query




        //Cisco CURL COMMANDS
        //1
        public async void StartCURL1(FileInfo exe, string command, string args, string workingDir)
        {

            vm.CMDOutput = "";
            vm.CommandInstruction = "";
            //vm.CMDOutput.Clear();



            using (Process process = new Process())
            {

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;

                // Redirects the standard input so that commands can be sent to the shell.
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
                process.EnableRaisingEvents = true;

                // Runs the specified command and exits the shell immediately.
                process.StartInfo.Arguments = args;
                //RUN AS ADMINISTRATOR
                if (System.Environment.OSVersion.Version.Major >= 6)
                {
                    process.StartInfo.Verb = "runas";
                }
                //Event Wiring
                process.OutputDataReceived += CURLProcessOutputDataHandler1;
                process.ErrorDataReceived += CURLProcessErrorDataHandler1;
                //process.OutputDataReceived += SortOutputHandler;
                //process.ErrorDataReceived += SortOutputHandler;
                //process.Exited += process_Exited;
                process.Exited += new System.EventHandler(CURLProcess_Exited1);
                //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
                process.Start();

                // kill remote console workaround for gen 8 hang
                if (args.Contains("biosdefaults"))
                {
                    await PutTaskDelay(2000);
                    //StaticFunctions.KillProcess("HPLOCONS");
                }
                {
                    // Send a directory command and an exit command to the shell
                    process.StandardInput.WriteLine(command);

                }


                // Send a directory command and an exit command to the shell
                process.StandardInput.WriteLine("exit");
                // Runs the specified command and exits the shell immediately.
                //process.StartInfo.Arguments = args;




                //process.StandardInput.WriteLine(login);
                //await PutTaskDelay(6000);



                //Asyncronus
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();



                //process.StandardInput.WriteLine("get");

                //process.StandardInput.WriteLine("exit");
                process.WaitForExit();



                process.Close();
                //process.ExitCode();

            }


        }

        //multi server support

        public void CURLProcessOutputDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {
            vm.ILORestOutput1 += outLine.Data + "\r";

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));

        }

        public void CURLProcessErrorDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {

            vm.ILORestOutput1 += outLine.Data + "\r";

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));

        }

        public void CURLProcess_Exited1(object sender, EventArgs e)
        {
            if (vm.ILORestOutput1.Contains("state:"))
            {
                int i = vm.ILORestOutput1.IndexOf("state:");
                vm.ServerState1 = vm.ILORestOutput1.Substring(i);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }
            else
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }

            //MessageBox.Show("Process Complete"); 
        }


        //END CISCO CURL



        public async void GetBladeIP1(FileInfo exe, string command, string args, string workingDir)
        {
            try
            {

          
            vm.CMDOutput = "";
            vm.CommandInstruction = "";
            //vm.CMDOutput.Clear();

            using (Process process = new Process())
            {

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;

                // Redirects the standard input so that commands can be sent to the shell.
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
                process.EnableRaisingEvents = true;

                // Runs the specified command and exits the shell immediately.
                process.StartInfo.Arguments = args;
                //RUN AS ADMINISTRATOR
                if (System.Environment.OSVersion.Version.Major >= 6)
                {
                    process.StartInfo.Verb = "runas";
                }
                //Event Wiring
                process.OutputDataReceived += GetBladeIPProcessOutputDataHandler1;
                process.ErrorDataReceived += GetBladeIPProcessErrorDataHandler1;
                //process.OutputDataReceived += SortOutputHandler;
                //process.ErrorDataReceived += SortOutputHandler;
                //process.Exited += process_Exited;
                process.Exited += new System.EventHandler(GetBladeIPProcess_Exited1);
                //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
                process.Start();

                // kill remote console workaround for gen 8 hang
                if (args.Contains("biosdefaults"))
                {
                    await PutTaskDelay(2000);
                    //StaticFunctions.KillProcess("HPLOCONS");
                }
                {
                    // Send a directory command and an exit command to the shell
                    process.StandardInput.WriteLine(command);

                }


                // Send a directory command and an exit command to the shell
                process.StandardInput.WriteLine("exit");
                // Runs the specified command and exits the shell immediately.
                //process.StartInfo.Arguments = args;




                //process.StandardInput.WriteLine(login);
                //await PutTaskDelay(6000);



                //Asyncronus
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();



                //process.StandardInput.WriteLine("get");

                //process.StandardInput.WriteLine("exit");
                process.WaitForExit();



                process.Close();
                //process.ExitCode();

            }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void GetBladeIPProcessOutputDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {

          vm.DELLBladeIPInfo += outLine.Data + "\r";
           
        }

        public void GetBladeIPProcessErrorDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {
          
            vm.DELLBladeIPInfo += outLine.Data + "\r";
         
        }

        public void GetBladeIPProcess_Exited1(object sender, EventArgs e)
        {
            
        }


        //Obtain return data for Blade IP Query
        public async void GetBladeStatus1(FileInfo exe, string command, string args, string workingDir)
        {
            try
            {

          
            vm.CMDOutput = "";
            vm.CommandInstruction = "";
            //vm.CMDOutput.Clear();

            using (Process process = new Process())
            {

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;

                // Redirects the standard input so that commands can be sent to the shell.
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
                process.EnableRaisingEvents = true;

                // Runs the specified command and exits the shell immediately.
                process.StartInfo.Arguments = args;
                //RUN AS ADMINISTRATOR
                if (System.Environment.OSVersion.Version.Major >= 6)
                {
                    process.StartInfo.Verb = "runas";
                }
                //Event Wiring
                process.OutputDataReceived += GetBladeStatusProcessOutputDataHandler1;
                process.ErrorDataReceived += GetBladeStatusProcessErrorDataHandler1;
                //process.OutputDataReceived += SortOutputHandler;
                //process.ErrorDataReceived += SortOutputHandler;
                //process.Exited += process_Exited;
                process.Exited += new System.EventHandler(GetBladeIPProcess_Exited1);
                //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
                process.Start();

                // kill remote console workaround for gen 8 hang
                if (args.Contains("biosdefaults"))
                {
                    await PutTaskDelay(2000);
                    //StaticFunctions.KillProcess("HPLOCONS");
                }
                {
                    // Send a directory command and an exit command to the shell
                    process.StandardInput.WriteLine(command);

                }


                // Send a directory command and an exit command to the shell
                process.StandardInput.WriteLine("exit");
                // Runs the specified command and exits the shell immediately.
                //process.StartInfo.Arguments = args;




                //process.StandardInput.WriteLine(login);
                //await PutTaskDelay(6000);



                //Asyncronus
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();



                //process.StandardInput.WriteLine("get");

                //process.StandardInput.WriteLine("exit");
                process.WaitForExit();



                process.Close();
                //process.ExitCode();

            }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void GetBladeStatusProcessOutputDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {

            vm.DELLBladeStatus += outLine.Data + "\r";

        }

        public void GetBladeStatusProcessErrorDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {

            vm.DELLBladeStatus += outLine.Data + "\r";

        }

        public void GetBladeStatusProcess_Exited1(object sender, EventArgs e)
        {

        }






        //COLLECT DATA FROM PROCESS SILENT

        public async void CollectDellHardwareInventoryRACDM1(FileInfo exe, string command, string args, string workingDir)
        {

            vm.CMDOutput = "";
            vm.CommandInstruction = "";
            //vm.CMDOutput.Clear();

            using (Process process = new Process())
            {

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;

                // Redirects the standard input so that commands can be sent to the shell.
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
                process.EnableRaisingEvents = true;

                // Runs the specified command and exits the shell immediately.
                process.StartInfo.Arguments = args;
                //RUN AS ADMINISTRATOR
                if (System.Environment.OSVersion.Version.Major >= 6)
                {
                    process.StartInfo.Verb = "runas";
                }
                //Event Wiring
                process.OutputDataReceived += CollectDellHardwareInventoryProcessOutputDataHandler1;
                process.ErrorDataReceived += CollectDellHardwareInventoryProcessErrorDataHandler1;
                //process.OutputDataReceived += SortOutputHandler;
                //process.ErrorDataReceived += SortOutputHandler;
                //process.Exited += process_Exited;
                process.Exited += new System.EventHandler(CollectDellHardwareInventoryProcess_Exited1);
                //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
                process.Start();

                // kill remote console workaround for gen 8 hang
                if (args.Contains("biosdefaults"))
                {
                    await PutTaskDelay(2000);
                    //StaticFunctions.KillProcess("HPLOCONS");
                }
                {
                    // Send a directory command and an exit command to the shell
                    process.StandardInput.WriteLine(command);

                }


                // Send a directory command and an exit command to the shell
                process.StandardInput.WriteLine("exit");
                // Runs the specified command and exits the shell immediately.
                //process.StartInfo.Arguments = args;




                //process.StandardInput.WriteLine(login);
                //await PutTaskDelay(6000);



                //Asyncronus
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();



                //process.StandardInput.WriteLine("get");

                //process.StandardInput.WriteLine("exit");
                process.WaitForExit();



                process.Close();
                //process.ExitCode();

            }


        }




        //HARDWARE INVENTORY PROCESSES

        //multi server support

        public void CollectDellHardwareInventoryProcessOutputDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {

            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);
            if (outLine.Data != null)
            {
                vm.DELLInfoS1.Add(outLine.Data);
               // vm.ILORestOutput1 += outLine.Data + "\r";

                // vm.StringBuilderS1.Append(outLine.Data);

                // vm.DELLHardwareInventory1 += outLine.Data + "\r";
                //vm.ILORestOutput2 += outLine.Data + "\r";
                //Console.WriteLine(txtBX.Text + "\n\r");
                //vm.ServerState += outLine.Data + "\r";
                //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }

            //txtOutput.Clear();
            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void CollectDellHardwareInventoryProcessErrorDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //var txtBX = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt") as TextBox;
            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);


            //if (outLine.Data.Contains("["))
            //{
            //    MessageBox.Show(outLine.Data);
            //}



            vm.DELLInfoS1.Add(outLine.Data);
            // vm.ILORestOutput1 += outLine.Data + "\r";
            //  vm.DELLHardwareInventory1 += outLine.Data + "\r";
            // vm.ILORestOutput2 += outLine.Data + "\r";

            //Console.WriteLine(txtBX.Text + "\n\r");
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));

            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void CollectDellHardwareInventoryProcess_Exited1(object sender, EventArgs e)
        {
            //if (vm.DELLHardwareInventory1.Contains("state:"))
            //{
            //    int i = vm.DELLHardwareInventory1.IndexOf("state:");
            //    vm.ServerState1 = vm.DELLHardwareInventory1.Substring(i);
            //    Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //}
            //else
            //{
            //    Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //}
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //MessageBox.Show("Process Complete"); 
        }



        //COLLECT DATA FROM CALL TO RACDM HWINVENTORY

        public async void CollectHardwareInventoryRACDM1(FileInfo exe, string command, string args, string workingDir)
        {

            vm.CMDOutput = "";
            vm.CommandInstruction = "";
            //vm.CMDOutput.Clear();

            using (Process process = new Process())
            {

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;

                // Redirects the standard input so that commands can be sent to the shell.
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
                process.EnableRaisingEvents = true;

                // Runs the specified command and exits the shell immediately.
                process.StartInfo.Arguments = args;
                //RUN AS ADMINISTRATOR
                if (System.Environment.OSVersion.Version.Major >= 6)
                {
                    process.StartInfo.Verb = "runas";
                }
                //Event Wiring
                process.OutputDataReceived += CollectHardwareInventoryProcessOutputDataHandler1;
                process.ErrorDataReceived += CollectHardwareInventoryProcessErrorDataHandler1;
                //process.OutputDataReceived += SortOutputHandler;
                //process.ErrorDataReceived += SortOutputHandler;
                //process.Exited += process_Exited;
                process.Exited += new System.EventHandler(CollectHardwareInventoryProcess_Exited1);
                //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
                process.Start();

                // kill remote console workaround for gen 8 hang
                if (args.Contains("biosdefaults"))
                {
                    await PutTaskDelay(2000);
                    //StaticFunctions.KillProcess("HPLOCONS");
                }
                {
                    // Send a directory command and an exit command to the shell
                    process.StandardInput.WriteLine(command);

                }


                // Send a directory command and an exit command to the shell
                process.StandardInput.WriteLine("exit");
                // Runs the specified command and exits the shell immediately.
                //process.StartInfo.Arguments = args;




                //process.StandardInput.WriteLine(login);
                //await PutTaskDelay(6000);



                //Asyncronus
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();



                //process.StandardInput.WriteLine("get");

                //process.StandardInput.WriteLine("exit");
                process.WaitForExit();



                process.Close();
                //process.ExitCode();

            }


        }




        //HARDWARE INVENTORY PROCESSES

        //multi server support

        public void CollectHardwareInventoryProcessOutputDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {

            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);
            if(outLine.Data != null)
            {
                vm.DELLInfoS1.Add(outLine.Data);
                vm.ILORestOutput1 += outLine.Data + "\r";

                // vm.StringBuilderS1.Append(outLine.Data);

                // vm.DELLHardwareInventory1 += outLine.Data + "\r";
                //vm.ILORestOutput2 += outLine.Data + "\r";
                //Console.WriteLine(txtBX.Text + "\n\r");
                //vm.ServerState += outLine.Data + "\r";
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }
          
            //txtOutput.Clear();
            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void CollectHardwareInventoryProcessErrorDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //var txtBX = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt") as TextBox;
            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);


            //if (outLine.Data.Contains("["))
            //{
            //    MessageBox.Show(outLine.Data);
            //}



            vm.DELLInfoS1.Add(outLine.Data);
            // vm.ILORestOutput1 += outLine.Data + "\r";
            //  vm.DELLHardwareInventory1 += outLine.Data + "\r";
            // vm.ILORestOutput2 += outLine.Data + "\r";

            //Console.WriteLine(txtBX.Text + "\n\r");
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));

            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void CollectHardwareInventoryProcess_Exited1(object sender, EventArgs e)
        {
            //if (vm.DELLHardwareInventory1.Contains("state:"))
            //{
            //    int i = vm.DELLHardwareInventory1.IndexOf("state:");
            //    vm.ServerState1 = vm.DELLHardwareInventory1.Substring(i);
            //    Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //}
            //else
            //{
            //    Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //}
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //MessageBox.Show("Process Complete"); 
        }


    

        //LENOVO SERVERS

        //DELL Lenovo COMMANDS
        //1
        public async void StartLenovo1(FileInfo exe, string command, string args, string workingDir)
        {

            vm.CMDOutput = "";
            vm.CommandInstruction = "";
            //vm.CMDOutput.Clear();



            using (Process process = new Process())
            {

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;

                // Redirects the standard input so that commands can be sent to the shell.
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.WorkingDirectory = workingDir;
                process.StartInfo.FileName = exe.FullName; // Path.Combine(Environment.SystemDirectory, "cmd.exe");
                process.EnableRaisingEvents = true;

                // Runs the specified command and exits the shell immediately.
                process.StartInfo.Arguments = args;
                //RUN AS ADMINISTRATOR
                if (System.Environment.OSVersion.Version.Major >= 6)
                {
                    process.StartInfo.Verb = "runas";
                }
                //Event Wiring
                process.OutputDataReceived += LenovoProcessOutputDataHandler1;
                process.ErrorDataReceived += LenovoProcessErrorDataHandler1;
                //process.OutputDataReceived += SortOutputHandler;
                //process.ErrorDataReceived += SortOutputHandler;
                //process.Exited += process_Exited;
                process.Exited += new System.EventHandler(LenovoProcess_Exited1);
                //MessageBox.Show(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe" + command);
                process.Start();

                // kill remote console workaround for gen 8 hang
                if (args.Contains("biosdefaults"))
                {
                    await PutTaskDelay(2000);
                    //StaticFunctions.KillProcess("HPLOCONS");
                }
                {
                    // Send a directory command and an exit command to the shell
                    process.StandardInput.WriteLine(command);

                }


                // Send a directory command and an exit command to the shell
                process.StandardInput.WriteLine("exit");
                // Runs the specified command and exits the shell immediately.
                //process.StartInfo.Arguments = args;




                //process.StandardInput.WriteLine(login);
                //await PutTaskDelay(6000);



                //Asyncronus
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();



                //process.StandardInput.WriteLine("get");

                //process.StandardInput.WriteLine("exit");
                process.WaitForExit();



                process.Close();
                //process.ExitCode();

            }


        }






        //multi server support

        public void LenovoProcessOutputDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {

            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            //vm.ILORestOutput += outLine.Data + "\r";
            vm.ILORestOutput1 += outLine.Data + "\r";
            //vm.ILORestOutput2 += outLine.Data + "\r";
            //Console.WriteLine(txtBX.Text + "\n\r");
            //vm.ServerState += outLine.Data + "\r";
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            //txtOutput.Clear();
            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void LenovoProcessErrorDataHandler1(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //var txtBX = Helper.GetDescendantFromName(Application.Current.MainWindow, "outputtxt") as TextBox;
            //txtBX.Text = outLine.Data + "\r";
            //WriteData1(outLine.Data);

            //vm.ILORestOutput += outLine.Data + "\r";
            vm.ILORestOutput1 += outLine.Data + "\r";
            // vm.ILORestOutput2 += outLine.Data + "\r";

            //Console.WriteLine(txtBX.Text + "\n\r");
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));

            //vm.CMDOutput.Add(outLine.Data);
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(WriteData1), outLine.Data); //.Replace("--More--", ""));
            //vm.CMDOutput.Add(outLine.Data);
            //MessageBox.Show(outLine.Data);
        }

        public void LenovoProcess_Exited1(object sender, EventArgs e)
        {
            if (vm.ILORestOutput1.Contains("state:"))
            {
                int i = vm.ILORestOutput1.IndexOf("state:");
                vm.ServerState1 = vm.ILORestOutput1.Substring(i);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }
            else
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new UpdateUiTextDelegate(ScrollToEnd));
            }

            //MessageBox.Show("Process Complete"); 
        }


      






    }
}
