﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Models
{
    public class IPInfo
    {
        public IPInfo(string ip4, string host,string macadd,string whichvendor)
        {
            IPv4 = ip4;
            //IPv6 = ip6;
            MacAddress = macadd;
            Vendor = whichvendor;
        }
        public string IPv4 { get; set; }
        //public string IPv6 { get; set; }
        public string Hostname { get; set; }
        public string MacAddress { get; set; }
        public string Vendor { get; set; }

    }
}
