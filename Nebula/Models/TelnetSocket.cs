﻿using System;
using System.Text;
using System.Net.Sockets;
using System.Threading;

public partial class TelnetConnection
{
    public class TelnetSocket
    {
        private TcpClient client;
        private Thread readWriteThread;
        private NetworkStream networkStream;
        //private string password;

        //http://manpages.ubuntu.com/manpages/xenial/man1/telnet-ssl.1.html

        public TelnetSocket(string ip, int port)
        {
            try
            {
                client = new TcpClient(ip, port);
                Console.WriteLine("Connected to server.");
            }
            catch (SocketException)
            {
                Console.WriteLine("Failed to connect to server");
                return;
            }

            //Assign networkstream
            networkStream = client.GetStream();

            //start socket read/write thread
            readWriteThread = new Thread(readWrite);
            readWriteThread.Start();
        }

        private void readWrite()
        {
            string command, recieved;

            //Read first thing givent o us
            recieved = read();
            Console.WriteLine(recieved);

            //Set up connection loop
            while (true)
            {
                Console.Write("Response: ");
                command = Console.ReadLine();

                if (command == "exit")
                    break;

                write(command);
                recieved = read();

                Console.WriteLine(recieved);
            }

            Console.WriteLine("Disconnected from server");
            networkStream.Close();
            client.Close();
        }

        public void write(string message)
        {
            message += Environment.NewLine;
            byte[] messageBytes = Encoding.ASCII.GetBytes(message);
            networkStream.Write(messageBytes, 0, messageBytes.Length);
        }

        public string read()
        {
            byte[] data = new byte[1024];
            string recieved = "";

            int size = networkStream.Read(data, 0, data.Length);
            recieved = Encoding.ASCII.GetString(data, 0, size);

            return recieved;
        }
    }
}



