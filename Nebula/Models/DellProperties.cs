﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Models
{
    public class DellProperties
    {
        public string Name { get; set; }

        ObservableCollection<DellPropertiesCollection> Items { get; set; }

        public DellProperties()
        {
            Items = new ObservableCollection<DellPropertiesCollection>();
        }


}



    public class DellPropertiesCollection
    {
        public string PropName { get; set; }
        public string PropValue { get; set; }
    }
}
