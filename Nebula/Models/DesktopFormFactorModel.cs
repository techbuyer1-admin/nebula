﻿using Nebula.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Models
{
    public class DesktopFormFactorModel : HasPropertyChanged
    {




        public string _Manufacturer;
        public string Manufacturer
        {
            get { return _Manufacturer; }
            set
            {
                _Manufacturer = value;
                OnPropertyChanged("Manufacturer");
            }
        }

        public string _Description;
        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                OnPropertyChanged("Description");
            }
        }

        public string _Abbreviations;
        public string Abbreviations
        {
            get { return _Abbreviations; }
            set
            {
                _Abbreviations = value;
                OnPropertyChanged("Abbreviations");
            }
        }


        private HPPartNumbers _LaptopDesktopSelectedItem;
        public HPPartNumbers LaptopDesktopSelectedItem
        {
            get { return _LaptopDesktopSelectedItem; }
            set
            {
                _LaptopDesktopSelectedItem = value;
                OnPropertyChanged("LaptopDesktopSelectedItem");
            }
        }



    }
 }

