﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Models
{
  
    public class Gen10FirmwareJson
    {
        public string odatacontext { get; set; }
        public string odataetag { get; set; }
        public string odataid { get; set; }
        public string odatatype { get; set; }
        public string Description { get; set; }
        public Gen10FWMember[] Members { get; set; }
        public int Membersodatacount { get; set; }
        public string Name { get; set; }
    }

    public class Gen10FWMember
    {
        public string odatacontext { get; set; }
        public string odataid { get; set; }
        public string odatatype { get; set; }
        public string Description { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public Gen10FWOem Oem { get; set; }
        public Gen10FWStatus Status { get; set; }
        public string Version { get; set; }
    }

    public class Gen10FWOem
    {
        public Gen10FWHpe Hpe { get; set; }
    }

    public class Gen10FWHpe
    {
        public string odatacontext { get; set; }
        public string odatatype { get; set; }
        public string DeviceClass { get; set; }
        public string DeviceContext { get; set; }
        public string[] Targets { get; set; }
        public string DeviceInstance { get; set; }
    }

    public class Gen10FWStatus
    {
        public string Health { get; set; }
        public string State { get; set; }
    }




}
