﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Nebula.Models
{

    //Key Value Pair for flattened Json
    public class RedfishKeyValuePair
    {
        public RedfishKeyValuePair()
        {

        }

        public RedfishKeyValuePair(string key, string value)
        {
            Key = key;
            Value = value;
        }

        public string Key { get; set; }
        public string Value { get; set; }
    }


    //Collection to hold all items
    public class RedfishServerCollection
    {

        public RedfishServerCollection()
        {
            //Create new collection
            ObservableCollection<RedfishArea> RedfishSection = new ObservableCollection<RedfishArea>();
        }


        //Key Value Pairs
        public ObservableCollection<RedfishArea> RedfishSection { get; set; }

    }



    // Contains Header and KeyValue Pairs
    public class RedfishArea
    {


        public RedfishArea()
        {

            //Create new collection
            ObservableCollection<RedfishKeyValuePair> RedfishPairs = new ObservableCollection<RedfishKeyValuePair>();
        }

        public RedfishArea(string dataContext)
        {
            //Add a reference
            oDataContext = dataContext;
            //Create new collection
            ObservableCollection<RedfishKeyValuePair> RedfishPairs = new ObservableCollection<RedfishKeyValuePair>();
        }

        // Data Header
        public string oDataContext { get; set; }

        //Key Value Pairs
        public ObservableCollection<RedfishKeyValuePair> RedfishPairs { get; set; }



    }

   


}
