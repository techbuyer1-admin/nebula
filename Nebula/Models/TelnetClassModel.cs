﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nebula.Models
{
    public class TelnetClassModel
    {


        public TelnetClassModel()
        {
            //https://www.codeproject.com/Articles/19071/Quick-tool-A-minimalistic-Telnet-library
            //create a new telnet connection to hostname "gobelijn" on port "23"
            TelnetConnection tc = new TelnetConnection("192.168.0.38", 23);

            //login with user "root",password "rootpassword", using a timeout of 100ms, and show server output



            string s = tc.Login("nick", "password", 150);
            Console.Write(s);
            // Console.ReadLine();
            // server output should end with "$" or ">", otherwise the connection failed
            string prompt = s.TrimEnd();
            prompt = s.Substring(prompt.Length - 1, 1);
            if (prompt != "$" && prompt != ">")
                throw new Exception("Connection failed");

            prompt = "";

            //Add command List
            tc.WriteLine("mkdir Oliver");
            tc.WriteLine("cd ~/Desktop");
            tc.WriteLine("mkdir Jacob");
            // while connected
            while (tc.IsConnected && prompt.Trim() != "exit")
            {
                // display server output
                Console.Write(tc.Read());

                // send client input to server
                prompt = Console.ReadLine();
                tc.WriteLine(prompt);

                // display server output
                Console.Write(tc.Read());
            }
            Console.WriteLine("***DISCONNECTED");
            Console.ReadLine();
        }
    }

    }






