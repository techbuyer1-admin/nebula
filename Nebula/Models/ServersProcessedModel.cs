﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Models
{
    [Serializable]
    public class ServersProcessedModel
    {
      
        public ServersProcessedModel()
        {
            //empty constructor
        }

        public ServersProcessedModel(string posono, string manufacturer, string modelno,string chassisserialno,string operative,string operaativelocation,string department,string timecompleted,string serverstate,string description, string factoryreset, string notes, string faultdescription,string gdprriskdetected)
        {
            //empty constructor
            POSONo = posono;
            Manufacturer = manufacturer;
            ModelNo = modelno;
            ChassisSerialNo = chassisserialno;
            Operative = operative;
            OperativeLocation = operaativelocation;
            Department = department;
            TimeCompleted = timecompleted;
            ServerState = serverstate;
            Description = description;
            FactoryReset = factoryreset;
            Notes = notes;
            FaultDescription = faultdescription;
            GDPRRiskDetected = gdprriskdetected;



        }

        public int EntryId { get; set; }
        public DateTime DateProcessed { get; set; }
        public string POSONo { get; set; }
        public string Manufacturer { get; set; }
        public string ModelNo { get; set; }
        public string ChassisSerialNo { get; set; }
        public string Operative { get; set; }
        public string OperativeLocation { get; set; }
        public string Department { get; set; }
        public string TimeCompleted { get; set; }
        public string ServerState { get; set; }
        public string Description { get; set; }
        public string FactoryReset { get; set; }
        public string Notes { get; set; }
        public string FaultDescription { get; set; }
        public string GDPRRiskDetected { get; set; }
    }
}
