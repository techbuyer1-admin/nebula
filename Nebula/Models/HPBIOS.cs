﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Models
{
    //class HPBIOS
    //{
    //}


    public class HPBIOS
    {
        public ServerInfo[] ServerInfo { get; set; }
    }

    public class ServerInfo
    {
        public Comments Comments { get; set; }
        public Hpbios100 HpBios100 { get; set; }
        public Chassis100 Chassis100 { get; set; }
        public Fwswversioninventory120 FwSwVersionInventory120 { get; set; }
        public Computersystem101 ComputerSystem101 { get; set; }
        public Manager100 Manager100 { get; set; }
    }

    public class Comments
    {
        public string BIOSDate { get; set; }
        public string BIOSFamily { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
    }

    public class Hpbios100
    {
        public RestV1Systems1Bios restv1Systems1Bios { get; set; }
    }

    public class RestV1Systems1Bios
    {
        public string odatacontext { get; set; }
        public string odataid { get; set; }
        public string odatatype { get; set; }
        public string AutoPowerOn { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string PowerOnDelay { get; set; }
        public string PowerRegulator { get; set; }
        public bool SwapROM { get; set; }
        public string TpmOpromMeasuring { get; set; }
        public string TpmState { get; set; }
        public Links links { get; set; }
    }

    public class Links
    {
        public Self self { get; set; }
    }

    public class Self
    {
        public string href { get; set; }
    }

    public class Chassis100
    {
        public RestV1Chassis1 restv1Chassis1 { get; set; }
    }

    public class RestV1Chassis1
    {
        public string odatacontext { get; set; }
        public string odataid { get; set; }
        public string odatatype { get; set; }
        public string ChassisType { get; set; }
        public string Id { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Name { get; set; }
        public Oem Oem { get; set; }
        public Power Power { get; set; }
        public string SKU { get; set; }
        public string SerialNumber { get; set; }
        public Status Status { get; set; }
        public Thermal Thermal { get; set; }
        public Links1 links { get; set; }
    }

    public class Oem
    {
        public Hp Hp { get; set; }
    }

    public class Hp
    {
        public string odatatype { get; set; }
        public Firmware Firmware { get; set; }
        public string Type { get; set; }
    }

    public class Firmware
    {
        public Platformdefinitiontable PlatformDefinitionTable { get; set; }
        public Powermanagementcontroller PowerManagementController { get; set; }
        public Powermanagementcontrollerbootloader PowerManagementControllerBootloader { get; set; }
        public Sasprogrammablelogicdevice SASProgrammableLogicDevice { get; set; }
        public Spsfirmwareversiondata SPSFirmwareVersionData { get; set; }
        public Systemprogrammablelogicdevice SystemProgrammableLogicDevice { get; set; }
    }

    public class Platformdefinitiontable
    {
        public Current Current { get; set; }
    }

    public class Current
    {
        public string VersionString { get; set; }
    }

    public class Powermanagementcontroller
    {
        public Current1 Current { get; set; }
    }

    public class Current1
    {
        public string VersionString { get; set; }
    }

    public class Powermanagementcontrollerbootloader
    {
        public Current2 Current { get; set; }
    }

    public class Current2
    {
        public string Family { get; set; }
        public string VersionString { get; set; }
    }

    public class Sasprogrammablelogicdevice
    {
        public Current3 Current { get; set; }
    }

    public class Current3
    {
        public string VersionString { get; set; }
    }

    public class Spsfirmwareversiondata
    {
        public Current4 Current { get; set; }
    }

    public class Current4
    {
        public string VersionString { get; set; }
    }

    public class Systemprogrammablelogicdevice
    {
        public Current5 Current { get; set; }
    }

    public class Current5
    {
        public string VersionString { get; set; }
    }

    public class Power
    {
        public string odataid { get; set; }
    }

    public class Status
    {
        public string Health { get; set; }
        public string State { get; set; }
    }

    public class Thermal
    {
        public string odataid { get; set; }
    }

    public class Links1
    {
        public Computersystem[] ComputerSystems { get; set; }
        public Managedby[] ManagedBy { get; set; }
        public Powermetrics PowerMetrics { get; set; }
        public Thermalmetrics ThermalMetrics { get; set; }
        public Self1 self { get; set; }
    }

    public class Powermetrics
    {
        public string href { get; set; }
    }

    public class Thermalmetrics
    {
        public string href { get; set; }
    }

    public class Self1
    {
        public string href { get; set; }
    }

    public class Computersystem
    {
        public string href { get; set; }
    }

    public class Managedby
    {
        public string href { get; set; }
    }

    public class Fwswversioninventory120
    {
        public RestV1Systems1Firmwareinventory restv1Systems1FirmwareInventory { get; set; }
    }

    public class RestV1Systems1Firmwareinventory
    {
        public string odatacontext { get; set; }
        public string odataid { get; set; }
        public string odatatype { get; set; }
        public Current6 Current { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public Links2 links { get; set; }
    }

    public class Current6
    {
        public Intelligentprovisioning[] IntelligentProvisioning { get; set; }
        public Other[] Other { get; set; }
        public Platformdefinitiontable1[] PlatformDefinitionTable { get; set; }
        public Powermanagementcontroller1[] PowerManagementController { get; set; }
        public Powermanagementcontrollerbootloader1[] PowerManagementControllerBootloader { get; set; }
        public Powersupply[] PowerSupplies { get; set; }
        public Sasprogrammablelogicdevice1[] SASProgrammableLogicDevice { get; set; }
        public Spsfirmwareversiondata1[] SPSFirmwareVersionData { get; set; }
        public Systembmc[] SystemBMC { get; set; }
        public Systemprogrammablelogicdevice1[] SystemProgrammableLogicDevice { get; set; }
        public Systemromactive[] SystemRomActive { get; set; }
        public Systemrombackup[] SystemRomBackup { get; set; }
        public Systemrombootblock[] SystemRomBootblock { get; set; }
    }

    public class Intelligentprovisioning
    {
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
    }

    public class Other
    {
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
    }

    public class Platformdefinitiontable1
    {
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
    }

    public class Powermanagementcontroller1
    {
        public string Key { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
    }

    public class Powermanagementcontrollerbootloader1
    {
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
    }

    public class Powersupply
    {
        public string Key { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
    }

    public class Sasprogrammablelogicdevice1
    {
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
    }

    public class Spsfirmwareversiondata1
    {
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
    }

    public class Systembmc
    {
        public string Key { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
    }

    public class Systemprogrammablelogicdevice1
    {
        public string Key { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
    }

    public class Systemromactive
    {
        public string Key { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
    }

    public class Systemrombackup
    {
        public string Key { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
    }

    public class Systemrombootblock
    {
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
    }

    public class Links2
    {
        public Self2 self { get; set; }
    }

    public class Self2
    {
        public string href { get; set; }
    }

    public class Computersystem101
    {
        public RestV1Systems1 restv1Systems1 { get; set; }
    }

    public class RestV1Systems1
    {
        public string odatacontext { get; set; }
        public string odataid { get; set; }
        public string odatatype { get; set; }
        public Actions Actions { get; set; }
        public string AssetTag { get; set; }
        public Availableaction1[] AvailableActions { get; set; }
        public Bios Bios { get; set; }
        public string BiosVersion { get; set; }
        public Boot Boot { get; set; }
        public string Description { get; set; }
        public Ethernetinterfaces EthernetInterfaces { get; set; }
        public Hostcorrelation HostCorrelation { get; set; }
        public string HostName { get; set; }
        public string Id { get; set; }
        public string IndicatorLED { get; set; }
        public Logservices LogServices { get; set; }
        public string Manufacturer { get; set; }
        public Memory Memory { get; set; }
        public Memorysummary MemorySummary { get; set; }
        public string Model { get; set; }
        public string Name { get; set; }
        public Oem1 Oem { get; set; }
        public string Power { get; set; }
        public string PowerState { get; set; }
        public Processorsummary ProcessorSummary { get; set; }
        public Processors Processors { get; set; }
        public string SKU { get; set; }
        public string SerialNumber { get; set; }
        public Status5 Status { get; set; }
        public string SystemType { get; set; }
        public string UUID { get; set; }
        public Links4 links { get; set; }
    }

    public class Actions
    {
        public ComputersystemReset ComputerSystemReset { get; set; }
    }

    public class ComputersystemReset
    {
        public string[] ResetTypeRedfishAllowableValues { get; set; }
        public string target { get; set; }
    }

    public class Bios
    {
        public Current7 Current { get; set; }
    }

    public class Current7
    {
        public string VersionString { get; set; }
    }

    public class Boot
    {
        public string BootSourceOverrideEnabled { get; set; }
        public string[] BootSourceOverrideSupported { get; set; }
        public string BootSourceOverrideTarget { get; set; }
    }

    public class Ethernetinterfaces
    {
        public string odataid { get; set; }
    }

    public class Hostcorrelation
    {
        public string[] HostMACAddress { get; set; }
        public string HostName { get; set; }
        public string[] IPAddress { get; set; }
    }

    public class Logservices
    {
        public string odataid { get; set; }
    }

    public class Memory
    {
        public Status1 Status { get; set; }
        public int TotalSystemMemoryGB { get; set; }
    }

    public class Status1
    {
        public string HealthRollUp { get; set; }
    }

    public class Memorysummary
    {
        public Status2 Status { get; set; }
        public int TotalSystemMemoryGiB { get; set; }
    }

    public class Status2
    {
        public string HealthRollUp { get; set; }
    }

    public class Oem1
    {
        public Hp1 Hp { get; set; }
    }

    public class Hp1
    {
        public string odatatype { get; set; }
        public Actions1 Actions { get; set; }
        public Availableaction[] AvailableActions { get; set; }
        public Bios1 Bios { get; set; }
        public Devicediscoverycomplete DeviceDiscoveryComplete { get; set; }
        public int IntelligentProvisioningIndex { get; set; }
        public string IntelligentProvisioningLocation { get; set; }
        public string IntelligentProvisioningVersion { get; set; }
        public string PostState { get; set; }
        public int PowerAllocationLimit { get; set; }
        public string PowerAutoOn { get; set; }
        public string PowerOnDelay { get; set; }
        public string PowerRegulatorMode { get; set; }
        public string[] PowerRegulatorModesSupported { get; set; }
        public Trustedmodule[] TrustedModules { get; set; }
        public string Type { get; set; }
        public string VirtualProfile { get; set; }
        public Links3 links { get; set; }
    }

    public class Actions1
    {
        public HpcomputersystemextPowerbutton HpComputerSystemExtPowerButton { get; set; }
        public HpcomputersystemextSystemreset HpComputerSystemExtSystemReset { get; set; }
    }

    public class HpcomputersystemextPowerbutton
    {
        public string[] PushTypeRedfishAllowableValues { get; set; }
        public string target { get; set; }
    }

    public class HpcomputersystemextSystemreset
    {
        public string[] ResetTypeRedfishAllowableValues { get; set; }
        public string target { get; set; }
    }

    public class Bios1
    {
        public Backup Backup { get; set; }
        public Bootblock Bootblock { get; set; }
        public Current8 Current { get; set; }
        public int UefiClass { get; set; }
    }

    public class Backup
    {
        public string Date { get; set; }
        public string Family { get; set; }
        public string VersionString { get; set; }
    }

    public class Bootblock
    {
        public string Date { get; set; }
        public string Family { get; set; }
        public string VersionString { get; set; }
    }

    public class Current8
    {
        public string Date { get; set; }
        public string Family { get; set; }
        public string VersionString { get; set; }
    }

    public class Devicediscoverycomplete
    {
        public string AMSDeviceDiscovery { get; set; }
        public string DeviceDiscovery { get; set; }
        public string SmartArrayDiscovery { get; set; }
    }

    public class Links3
    {
        public BIOS2 BIOS { get; set; }
        public Ethernetinterfaces1 EthernetInterfaces { get; set; }
        public Firmwareinventory FirmwareInventory { get; set; }
        public Memory1 Memory { get; set; }
        public Networkadapters NetworkAdapters { get; set; }
        public Pcidevices PCIDevices { get; set; }
        public Pcislots PCISlots { get; set; }
        public Smartstorage SmartStorage { get; set; }
        public Softwareinventory SoftwareInventory { get; set; }
    }

    public class BIOS2
    {
        public string href { get; set; }
    }

    public class Ethernetinterfaces1
    {
        public string href { get; set; }
    }

    public class Firmwareinventory
    {
        public string href { get; set; }
    }

    public class Memory1
    {
        public string href { get; set; }
    }

    public class Networkadapters
    {
        public string href { get; set; }
    }

    public class Pcidevices
    {
        public string href { get; set; }
    }

    public class Pcislots
    {
        public string href { get; set; }
    }

    public class Smartstorage
    {
        public string href { get; set; }
    }

    public class Softwareinventory
    {
        public string href { get; set; }
    }

    public class Availableaction
    {
        public string Action { get; set; }
        public Capability[] Capabilities { get; set; }
    }

    public class Capability
    {
        public string[] AllowableValues { get; set; }
        public string PropertyName { get; set; }
    }

    public class Trustedmodule
    {
        public string Status { get; set; }
    }

    public class Processorsummary
    {
        public int Count { get; set; }
        public string Model { get; set; }
        public Status3 Status { get; set; }
    }

    public class Status3
    {
        public string HealthRollUp { get; set; }
    }

    public class Processors
    {
        public int Count { get; set; }
        public string ProcessorFamily { get; set; }
        public Status4 Status { get; set; }
    }

    public class Status4
    {
        public string HealthRollUp { get; set; }
    }

    public class Status5
    {
        public string Health { get; set; }
        public string State { get; set; }
    }

    public class Links4
    {
        public Chassis[] Chassis { get; set; }
        public Ethernetinterfaces2 EthernetInterfaces { get; set; }
        public Logs Logs { get; set; }
        public Managedby1[] ManagedBy { get; set; }
        public Processors1 Processors { get; set; }
        public Self3 self { get; set; }
    }

    public class Ethernetinterfaces2
    {
        public string href { get; set; }
    }

    public class Logs
    {
        public string href { get; set; }
    }

    public class Processors1
    {
        public string href { get; set; }
    }

    public class Self3
    {
        public string href { get; set; }
    }

    public class Chassis
    {
        public string href { get; set; }
    }

    public class Managedby1
    {
        public string href { get; set; }
    }

    public class Availableaction1
    {
        public string Action { get; set; }
        public Capability1[] Capabilities { get; set; }
    }

    public class Capability1
    {
        public string[] AllowableValues { get; set; }
        public string PropertyName { get; set; }
    }

    public class Manager100
    {
        public RestV1Managers1 restv1Managers1 { get; set; }
    }

    public class RestV1Managers1
    {
        public string odatacontext { get; set; }
        public string odataid { get; set; }
        public string odatatype { get; set; }
        public Actions2 Actions { get; set; }
        public Availableaction3[] AvailableActions { get; set; }
        public Commandshell CommandShell { get; set; }
        public string Description { get; set; }
        public Ethernetinterfaces3 EthernetInterfaces { get; set; }
        public Firmware1 Firmware { get; set; }
        public string FirmwareVersion { get; set; }
        public Graphicalconsole GraphicalConsole { get; set; }
        public string Id { get; set; }
        public Logservices1 LogServices { get; set; }
        public string ManagerType { get; set; }
        public string Name { get; set; }
        public Networkprotocol NetworkProtocol { get; set; }
        public Oem2 Oem { get; set; }
        public Serialconsole SerialConsole { get; set; }
        public Status6 Status { get; set; }
        public string UUID { get; set; }
        public Virtualmedia VirtualMedia { get; set; }
        public Links6 links { get; set; }
    }

    public class Actions2
    {
        public ManagerReset ManagerReset { get; set; }
    }

    public class ManagerReset
    {
        public string target { get; set; }
    }

    public class Commandshell
    {
        public string[] ConnectTypesSupported { get; set; }
        public bool Enabled { get; set; }
        public int MaxConcurrentSessions { get; set; }
        public bool ServiceEnabled { get; set; }
    }

    public class Ethernetinterfaces3
    {
        public string odataid { get; set; }
    }

    public class Firmware1
    {
        public Current9 Current { get; set; }
    }

    public class Current9
    {
        public string VersionString { get; set; }
    }

    public class Graphicalconsole
    {
        public string[] ConnectTypesSupported { get; set; }
        public bool Enabled { get; set; }
        public int MaxConcurrentSessions { get; set; }
        public bool ServiceEnabled { get; set; }
    }

    public class Logservices1
    {
        public string odataid { get; set; }
    }

    public class Networkprotocol
    {
        public string odataid { get; set; }
    }

    public class Oem2
    {
        public Hp2 Hp { get; set; }
    }

    public class Hp2
    {
        public string odatatype { get; set; }
        public Actions3 Actions { get; set; }
        public Availableaction2[] AvailableActions { get; set; }
        public string ClearRestApiStatus { get; set; }
        public Federationconfig FederationConfig { get; set; }
        public Firmware2 Firmware { get; set; }
        public License License { get; set; }
        public bool RequiredLoginForiLORBSU { get; set; }
        public int SerialCLISpeed { get; set; }
        public string SerialCLIStatus { get; set; }
        public string Type { get; set; }
        public bool VSPLogDownloadEnabled { get; set; }
        public Iloselftestresult[] iLOSelfTestResults { get; set; }
        public Links5 links { get; set; }
    }

    public class Actions3
    {
        public HpiloClearrestapistate HpiLOClearRestApiState { get; set; }
        public HpiloResettofactorydefaults HpiLOResetToFactoryDefaults { get; set; }
        public HpiloIlofunctionality HpiLOiLOFunctionality { get; set; }
    }

    public class HpiloClearrestapistate
    {
        public string target { get; set; }
    }

    public class HpiloResettofactorydefaults
    {
        public string[] ResetTypeRedfishAllowableValues { get; set; }
        public string target { get; set; }
    }

    public class HpiloIlofunctionality
    {
        public string target { get; set; }
    }

    public class Federationconfig
    {
        public string IPv6MulticastScope { get; set; }
        public int MulticastAnnouncementInterval { get; set; }
        public string MulticastDiscovery { get; set; }
        public int MulticastTimeToLive { get; set; }
        public string iLOFederationManagement { get; set; }
    }

    public class Firmware2
    {
        public Current10 Current { get; set; }
    }

    public class Current10
    {
        public string Date { get; set; }
        public bool DebugBuild { get; set; }
        public int MajorVersion { get; set; }
        public int MinorVersion { get; set; }
        public string Time { get; set; }
        public string VersionString { get; set; }
    }

    public class License
    {
        public string LicenseKey { get; set; }
        public string LicenseString { get; set; }
        public string LicenseType { get; set; }
    }

    public class Links5
    {
        public Activehealthsystem ActiveHealthSystem { get; set; }
        public Datetimeservice DateTimeService { get; set; }
        public Embeddedmediaservice EmbeddedMediaService { get; set; }
        public Federationdispatch FederationDispatch { get; set; }
        public Federationgroups FederationGroups { get; set; }
        public Federationpeers FederationPeers { get; set; }
        public Licenseservice LicenseService { get; set; }
        public Securityservice SecurityService { get; set; }
        public Updateservice UpdateService { get; set; }
        public Vsploglocation VSPLogLocation { get; set; }
    }

    public class Activehealthsystem
    {
        public string href { get; set; }
    }

    public class Datetimeservice
    {
        public string href { get; set; }
    }

    public class Embeddedmediaservice
    {
        public string href { get; set; }
    }

    public class Federationdispatch
    {
        public string extref { get; set; }
    }

    public class Federationgroups
    {
        public string href { get; set; }
    }

    public class Federationpeers
    {
        public string href { get; set; }
    }

    public class Licenseservice
    {
        public string href { get; set; }
    }

    public class Securityservice
    {
        public string href { get; set; }
    }

    public class Updateservice
    {
        public string href { get; set; }
    }

    public class Vsploglocation
    {
        public string extref { get; set; }
    }

    public class Availableaction2
    {
        public string Action { get; set; }
        public Capability2[] Capabilities { get; set; }
    }

    public class Capability2
    {
        public string[] AllowableValues { get; set; }
        public string PropertyName { get; set; }
    }

    public class Iloselftestresult
    {
        public string Notes { get; set; }
        public string SelfTestName { get; set; }
        public string Status { get; set; }
    }

    public class Serialconsole
    {
        public string[] ConnectTypesSupported { get; set; }
        public bool Enabled { get; set; }
        public int MaxConcurrentSessions { get; set; }
        public bool ServiceEnabled { get; set; }
    }

    public class Status6
    {
        public string State { get; set; }
    }

    public class Virtualmedia
    {
        public string odataid { get; set; }
    }

    public class Links6
    {
        public Ethernetnics EthernetNICs { get; set; }
        public Logs1 Logs { get; set; }
        public Managerforchassi[] ManagerForChassis { get; set; }
        public Managerforserver[] ManagerForServers { get; set; }
        public Networkservice NetworkService { get; set; }
        public Virtualmedia1 VirtualMedia { get; set; }
        public Self4 self { get; set; }
    }

    public class Ethernetnics
    {
        public string href { get; set; }
    }

    public class Logs1
    {
        public string href { get; set; }
    }

    public class Networkservice
    {
        public string href { get; set; }
    }

    public class Virtualmedia1
    {
        public string href { get; set; }
    }

    public class Self4
    {
        public string href { get; set; }
    }

    public class Managerforchassi
    {
        public string href { get; set; }
    }

    public class Managerforserver
    {
        public string href { get; set; }
    }

    public class Availableaction3
    {
        public string Action { get; set; }
    }



}
