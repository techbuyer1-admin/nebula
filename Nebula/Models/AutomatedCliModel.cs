﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Models
{
    [Serializable]
    public class AutomatedCliModel
    {


        public string Brand { get; set; }
        public string ModelNo { get; set; }
        public string Category { get; set; }

        public string SequenceType { get; set; }
        public List<StepItem> Steps { get; set; }


    public AutomatedCliModel()
        {

        }



    }


    //To use in a collection
    public class StepItem
    {
        //Default Constructor
        public StepItem()
        {

        }

        //Overide Constructor
        public StepItem(int stepno, string stepdescription, string clicommandtosend)
        {
            StepNo = stepno;
            StepDescription = stepdescription;
            CliCommandToSend = clicommandtosend;
        }
        public int StepNo { get; set; }
        public string StepDescription { get; set; }
        public string CliCommandToSend { get; set; }

    }

}
