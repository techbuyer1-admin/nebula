﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nebula.Helpers;

namespace Nebula.Models
{
    public class HPPhysicalDrivesModel : HasPropertyChanged
    {

        public string _odataID;
        public string odataID
        {
            get { return _odataID; }
            set
            {
                _odataID = value;
                OnPropertyChanged("odataID");
            }
        }

        public string _Model;
        public string Model
        {
            get { return _Model; }
            set
            {
                _Model = value;
                OnPropertyChanged("Model");
            }
        }

        public string _Capacity;
        public string Capacity
        {
            get { return _Capacity; }
            set
            {
                _Capacity = value;
                OnPropertyChanged("Capacity");
            }
        }

        public string _Interface;
        public string Interface
        {
            get { return _Interface; }
            set
            {
                _Interface = value;
                OnPropertyChanged("Interface");
            }
        }

        public string _Location;
        public string Location
        {
            get { return _Location; }
            set
            {
                _Location = value;
                OnPropertyChanged("Location");
            }
        }

        public string _SerialNumber;
        public string SerialNumber
        {
            get { return _SerialNumber; }
            set
            {
                _SerialNumber = value;
                OnPropertyChanged("SerialNumber");
            }
        }

        public string _InterfaceSpeed;
        public string InterfaceSpeed
        {
            get { return _InterfaceSpeed; }
            set
            {
                _InterfaceSpeed = value;
                OnPropertyChanged("InterfaceSpeed");
            }
        }


        public string _Encryption;
        public string Encryption
        {
            get { return _Encryption; }
            set
            {
                _Encryption = value;
                OnPropertyChanged("Encryption");
            }
        }


        public string _Temp;
        public string Temp
        {
            get { return _Temp; }
            set
            {
                _Temp = value;
                OnPropertyChanged("Temp");
            }
        }

        public string _BlockSize;
        public string BlockSize
        {
            get { return _BlockSize; }
            set
            {
                _BlockSize = value;
                OnPropertyChanged("BlockSize");
            }
        }

        public string _MediaType;
        public string MediaType
        {
            get { return _MediaType; }
            set
            {
                _MediaType = value;
                OnPropertyChanged("MediaType");
            }
        }

        public string _Health;
        public string Health
        {
            get { return _Health; }
            set
            {
                _Health = value;
                OnPropertyChanged("Health");
            }
        }


    }
}
