﻿using System.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.IO;
using Nebula.Properties;
using System.Windows;

namespace Nebula.Models
{
    public class SerialClassModel
    {
        // The main control for communicating through the RS-232 port
        private SerialPort _serialPort = new SerialPort();
        private SerialPort _serialPort2 = new SerialPort();

        public enum DataMode { Text, Hex }
        public enum LogMsgType { Incoming, Outgoing, Normal, Warning, Error };

        public static string Output { get; set; }
        public string Instruction { get; set; }

        private DataMode CurrentDataMode
        {
            get
            {
                //if (rbHex.Checked) return DataMode.Hex;
                /* else */
                return DataMode.Text;
            }
            set
            {
                //if (value == DataMode.Text) rbText.Checked = true;
                //else rbHex.Checked = true;
            }
        }

        // Various colors for logging info
        //private System.Windows.Media.Color[] LogMsgTypeColor = { Color.Blue, Color.Green, Color.Black, Color.Orange, Color.Red };

        // Temp holder for whether a key was pressed
        // private bool KeyHandled = false;

        private Settings settings = Settings.Default;


        public SerialClassModel()
        {
            // Load user settings
            settings.Reload();


            foreach (string s in SerialPort.GetPortNames())
            {
                //comboBox1.Items.Add(s);
            }


            //Serial Init
            _serialPort.PortName = "COM5";
            _serialPort2.PortName = "COM6";

            // When data is recieved through the port, call this method
            _serialPort.DataReceived += new SerialDataReceivedEventHandler(_serialPort_DataReceived);
            _serialPort.PinChanged += new SerialPinChangedEventHandler(_serialPort_PinChanged);
            _serialPort2.DataReceived += new SerialDataReceivedEventHandler(_serialPort2_DataReceived);
            _serialPort2.PinChanged += new SerialPinChangedEventHandler(_serialPort2_PinChanged);
        }




        private void LoadSettings()
        {
            //comboBox3.Items.Clear(); comboBox3.Items.AddRange(Enum.GetNames(typeof(Parity)));
            //comboBox5.Items.Clear(); comboBox5.Items.AddRange(Enum.GetNames(typeof(StopBits)));

            //comboBox3.Text = settings.Parity.ToString();
            //comboBox5.Text = settings.StopBits.ToString();
            //comboBox4.Text = settings.DataBits.ToString();
            //comboBox3.Text = settings.Parity.ToString();
            //comboBox3.Text = settings.BaudRate.ToString();

            ////RefreshComPortList();

            ////chkClearOnOpen.Checked = settings.ClearOnOpen;
            ////chkClearWithDTR.Checked = settings.ClearWithDTR;

            //// If it is still avalible, select the last com port used
            //if (comboBox1.Items.Contains(settings.PortName)) comboBox1.Text = settings.PortName;
            //else if (comboBox1.Items.Count > 0) comboBox1.SelectedIndex = comboBox1.Items.Count - 1;
            //else
            //{
            //    MessageBox.Show(this, "There are no COM Ports detected on this computer.\nPlease install a COM Port and restart this app.", "No COM Ports Installed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    //this.Close();
            //}

        }

        private string RefreshComPortList(IEnumerable<string> PreviousPortNames, string CurrentSelection, bool PortOpen)
        {
            // Create a new return report to populate
            string selected = null;

            // Retrieve the list of ports currently mounted by the operating system (sorted by name)
            string[] ports = SerialPort.GetPortNames();

            // First determain if there was a change (any additions or removals)
            bool updated = PreviousPortNames.Except(ports).Count() > 0 || ports.Except(PreviousPortNames).Count() > 0;

            // If there was a change, then select an appropriate default port
            if (updated)
            {
                // Use the correctly ordered set of port names
                ports = OrderedPortNames();

                // Find newest port if one or more were added
                string newest = SerialPort.GetPortNames().Except(PreviousPortNames).OrderBy(a => a).LastOrDefault();

                // If the port was already open... (see logic notes and reasoning in Notes.txt)
                if (PortOpen)
                {
                    if (ports.Contains(CurrentSelection)) selected = CurrentSelection;
                    else if (!String.IsNullOrEmpty(newest)) selected = newest;
                    else selected = ports.LastOrDefault();
                }
                else
                {
                    if (!String.IsNullOrEmpty(newest)) selected = newest;
                    else if (ports.Contains(CurrentSelection)) selected = CurrentSelection;
                    else selected = ports.LastOrDefault();
                }
            }

            // If there was a change to the port list, return the recommended default selection
            return selected;
        }

        private string[] OrderedPortNames()
        {
            // Just a placeholder for a successful parsing of a string to an integer
            int num;

            // Order the serial port names in numberic order (if possible)
            return SerialPort.GetPortNames().OrderBy(a => a.Length > 3 && int.TryParse(a.Substring(3), out num) ? num : 0).ToArray();
        }


        private void SaveSettings()
        {
            //settings.PortName = comboBox1.Text;
            //settings.BaudRate = int.Parse(comboBox2.Text);
            //settings.Parity = (Parity)Enum.Parse(typeof(Parity), comboBox3.Text);
            //settings.DataBits = int.Parse(comboBox4.Text);
            ////settings.DataMode = CurrentDataMode;
            //settings.StopBits = (StopBits)Enum.Parse(typeof(StopBits), comboBox5.Text);

            ////settings.ClearOnOpen = chkClearOnOpen.Checked;
            ////settings.ClearWithDTR = chkClearWithDTR.Checked;

            //settings.Save();
        }



        private void Log(LogMsgType msgtype, string msg)
        {
            //richTextBox1.Invoke(new EventHandler(delegate
            //{
            //    richTextBox1.SelectedText = string.Empty;
            //    richTextBox1.SelectionFont = new Font(richTextBox1.SelectionFont, FontStyle.Bold);
            //    richTextBox1.SelectionColor = LogMsgTypeColor[(int)msgtype];
            //    richTextBox1.AppendText(msg);
            //    richTextBox1.ScrollToCaret();
            //}));


        }


        private void Log2(LogMsgType msgtype, string msg)
        {
            //richTextBox2.Invoke(new EventHandler(delegate
            //{
            //    richTextBox2.SelectedText = string.Empty;
            //    richTextBox2.SelectionFont = new Font(richTextBox2.SelectionFont, FontStyle.Bold);
            //    richTextBox2.SelectionColor = LogMsgTypeColor[(int)msgtype];
            //    richTextBox2.AppendText(msg);
            //    richTextBox2.ScrollToCaret();
            //}));
        }




        void _serialPort_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort2_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        private void UpdatePinState()
        {
            //this.Invoke(new ThreadStart(() => {
            //    // Show the state of the pins
            //    //chkCD.Checked = comport.CDHolding;
            //    //chkCTS.Checked = comport.CtsHolding;
            //    //chkDSR.Checked = comport.DsrHolding;
            //}));
        }


        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // If the com port has been closed, do nothing
            if (!_serialPort.IsOpen) return;

            // This method will be called when there is data waiting in the port's buffer

            // Determain which mode (string or binary) the user is in
            if (CurrentDataMode == DataMode.Text)
            {
                // Read all the data waiting in the buffer
                string data = _serialPort.ReadExisting();

                // Display the text to the user in the terminal
                if (data.Contains("--More--"))
                {
                    _serialPort.Write("\r");
                    Log(LogMsgType.Incoming, data);

                }
                else
                {
                    Log(LogMsgType.Incoming, data);
                }

            }
            else
            {
                // Obtain the number of bytes waiting in the port's buffer
                int bytes = _serialPort.BytesToRead;

                // Create a byte array buffer to hold the incoming data
                byte[] buffer = new byte[bytes];

                // Read the data from the port and store it in our buffer
                _serialPort.Read(buffer, 0, bytes);

                // Show the user the incoming data in hex format
                //Log(LogMsgType.Incoming, ByteArrayToHexString(buffer));
            }
        }

        private void _serialPort2_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // If the com port has been closed, do nothing
            if (!_serialPort2.IsOpen) return;

            // This method will be called when there is data waiting in the port's buffer

            // Determain which mode (string or binary) the user is in
            if (CurrentDataMode == DataMode.Text)
            {
                // Read all the data waiting in the buffer
                string data = _serialPort2.ReadExisting();

                // Display the text to the user in the terminal
                if (data.Contains("--More--"))
                {
                    _serialPort2.Write("\r");
                    Log2(LogMsgType.Incoming, data);

                }
                else
                {
                    Log2(LogMsgType.Incoming, data);
                }

            }
            else
            {
                // Obtain the number of bytes waiting in the port's buffer
                int bytes = _serialPort2.BytesToRead;

                // Create a byte array buffer to hold the incoming data
                byte[] buffer = new byte[bytes];

                // Read the data from the port and store it in our buffer
                _serialPort2.Read(buffer, 0, bytes);

                // Show the user the incoming data in hex format
                //Log(LogMsgType.Incoming, ByteArrayToHexString(buffer));
            }
        }




        private void SendCarriageReturn()
        {
            if (CurrentDataMode == DataMode.Text)
            {
                // Send the user's text straight out the port
                _serialPort.Write("\r");
                _serialPort2.Write("\r");
                // Show in the terminal window the user's text
                // Log(LogMsgType.Outgoing, "\n");
            }
            else
            {
                //try
                //{
                //    // Convert the user's string of hex digits (ex: B4 CA E2) to a byte array
                //    byte[] data = HexStringToByteArray(txtSendData.Text);

                //    // Send the binary data out the port
                //    comport.Write(data, 0, data.Length);

                //    // Show the hex digits on in the terminal window
                //    Log(LogMsgType.Outgoing, ByteArrayToHexString(data) + "\n");
                //}
                //catch (FormatException)
                //{
                //    // Inform the user if the hex string was not properly formatted
                //    Log(LogMsgType.Error, "Not properly formatted hex string: " + txtSendData.Text + "\n");
                //}
            }
            //textBox1.SelectAll();
        }



        private void SendData()
        {
            if (CurrentDataMode == DataMode.Text)
            {
                //// Send the user's text straight out the port
                //_serialPort.Write(textBox1.Text + "\r");
                //_serialPort2.Write(textBox1.Text + "\r");


                //// Show in the terminal window the user's text
                //Log(LogMsgType.Outgoing, textBox1.Text + "\n");
                //Log2(LogMsgType.Outgoing, textBox1.Text + "\n");
            }
            else
            {
                //try
                //{
                //    // Convert the user's string of hex digits (ex: B4 CA E2) to a byte array
                //    byte[] data = HexStringToByteArray(txtSendData.Text);

                //    // Send the binary data out the port
                //    _serialPort.Write(data, 0, data.Length);

                //    // Show the hex digits on in the terminal window
                //    Log(LogMsgType.Outgoing, ByteArrayToHexString(data) + "\n");
                //}
                //catch (FormatException)
                //{
                //    // Inform the user if the hex string was not properly formatted
                //    Log(LogMsgType.Error, "Not properly formatted hex string: " + textBox1.Text + "\n");
                //}
            }
            //textBox1.SelectAll();

           

        }

        List<SerialPort> ComCollection = new List<SerialPort>();


        public void OpenMultiPorts(List<SerialPort> comlst,string portname, string baud,string parity, string databit,string stopbit, string handshake)
            {

            bool error = false;

            foreach (var cp in comlst)
                {
                    // If the port is open, close it.
                    if (cp.IsOpen)
                        cp.Close();
                    else
                    {
                        // Set the port's settings
                        //cp.PortName = comboBox1.Text;
                        cp.BaudRate = int.Parse(baud);
                        cp.Parity = (Parity)Enum.Parse(typeof(Parity), parity);
                        cp.DataBits = int.Parse(databit);
                        cp.StopBits = (StopBits)Enum.Parse(typeof(StopBits), stopbit);



                        try
                        {
                      
                        // Open the port
                        cp.Open();
                        }
                        catch (UnauthorizedAccessException) { error = true; string err = error.ToString(); }
                        catch (IOException) { error = true; string err = error.ToString(); }
                        catch (ArgumentException) { error = true; string err = error.ToString(); }

                        //if (error) MessageBox.Show(this, "Could not open the COM port.  Most likely it is already in use, has been removed, or is unavailable.", "COM Port Unavalible", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        //else
                        //{
                        //    // Show the initial pin states
                        //    UpdatePinState();
                        //    //chkDTR.Checked = comport.DtrEnable;
                        //    //chkRTS.Checked = comport.RtsEnable;
                        //}
                    }






                    // Change the state of the form's controls
                    //EnableControls();

                    // If the port is open, send focus to the send data box
                    if (cp.IsOpen)
                    {
                       // textBox1.Focus();
                        //if (chkClearOnOpen.Checked) ClearTerminal();
                        ClearTerminal();
                    }
                }



            }



            private void button3_Click(object sender, EventArgs e)
            {
                List<SerialPort> prts = new List<SerialPort>();
                prts.Add(_serialPort);
                prts.Add(_serialPort2);

                OpenMultiPorts(prts, "", "","","","","");

            }

            private void ClearTerminal()
            {
                //richTextBox1.Clear();
                //richTextBox2.Clear();
            }

            private void button2_Click(object sender, EventArgs e)
            {
                SendData();
                //textBox1.Clear();
                //textBox1.Focus();
                //if (nick =)
                //{

                //}
                //else
                //{

                //}

            }

            private void button4_Click(object sender, EventArgs e)
            {
                if (_serialPort.IsOpen)
                {
                    _serialPort.Close();
                    //if (chkClearOnOpen.Checked) ClearTerminal();
                    ClearTerminal();
                }

                if (_serialPort.IsOpen)
                {

                    _serialPort.Close();
                    //if (chkClearOnOpen.Checked) ClearTerminal();
                    ClearTerminal();
                }
                else if (_serialPort2.IsOpen)
                {
                    _serialPort2.Close();

                    //if (chkClearOnOpen.Checked) ClearTerminal();
                    ClearTerminal();
                }
                else if (_serialPort.IsOpen && _serialPort2.IsOpen)
                {//if (chkClearOnOpen.Checked) ClearTerminal();
                    ClearTerminal();
                    _serialPort.Close();
                    _serialPort2.Close();
                }

                //selected = ports.LastOrDefault();
                //elseif(_serialPort2.IsOpen)
                //{
                //    _serialPort.Close();
                //}

            }

            private void richTextBox1_TextChanged(object sender, EventArgs e)
            {

                //int totallines = richTextBox1.Lines.Length;
                //string lastline = richTextBox1.Lines[totallines - 1];

                //MessageBox.Show(lastline);

                //if (lastline.Contains("--More--"))
                //{
                //    //_serialPort.Write("\r");
                //    //Log(LogMsgType.Incoming, data);

                //}
            }

            private void richTextBox2_TextChanged(object sender, EventArgs e)
            {
                //int totallines = richTextBox2.Lines.Length;
                //string lastline = richTextBox2.Lines[totallines - 1];

                //MessageBox.Show(lastline);

                //if (lastline.Contains("--More--"))
                //{
                //    //_serialPort.Write("\r");
                //    //Log(LogMsgType.Incoming, data);

                //}

            }


      

    }
}
