﻿using Nebula.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;


namespace Nebula.Models
{
    public class HPServerData : HasPropertyChanged
    {

        public HPServerData()
        {


            //Server System
            CPUInfo = new ObservableCollection<HealthCPUHP>();
            MemoryInfo = new ObservableCollection<HealthMemoryHP>();
            FanInfo = new ObservableCollection<HealthFansHP>();
            TemperatureInfo = new ObservableCollection<HealthTemperatureHP>();
            PowerInfo = new ObservableCollection<HealthPowerHP>();
            NICInfo = new ObservableCollection<HealthNetworkHP>();

            //Create new instance for drives
            PhysicalDriveInfo = new ObservableCollection<PhysicalDrivesHP>();
        }


        public string TestedBy { get; set; }
        //SD CARD Presence
        public string SDCardInserted { get; set; }
        //Firmware
        public string BiosCurrentVersion { get; set; }
        public string BiosBackupVersion { get; set; }
        public string iLOVersion { get; set; }
        public string SPSVersion { get; set; }
        public string IntelligentProvisioningLocation { get; set; }
        public string IntelligentProvisioningVersion { get; set; }
        public string InnovationEngine { get; set; }
        //ILO Network Port Status
        public string ILONetworkPort { get; set; }
        //Server Info
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string ChassisSerial { get; set; }
        public string AssetTag { get; set; }
        public string HostName { get; set; }
        public string HealthStatus { get; set; }
        public string SKU { get; set; }

        // public string OutOfRegionMessage { get; set; }


        //Out Of Region Visibility On HP Report
        public Visibility _OutOfRegionViz;
        public Visibility OutOfRegionViz
        {
            get { return _OutOfRegionViz; }
            set
            {
                _OutOfRegionViz = value;
                OnPropertyChanged("OutOfRegionViz");
            }
        }


        private string _OutOfRegionMessage;

        public string OutOfRegionMessage
        {
            get { return _OutOfRegionMessage; }
            set
            {
                _OutOfRegionMessage = value;
                OnPropertyChanged("OutOfRegionMessage");
            }
        }

        //memory
        public string TotalMemory { get; set; }
        public string MemoryHealth { get; set; }
        public string PopulatedSlots { get; set; }

        //cpu
        public string CPUFamily { get; set; }
        public string CPUCount { get; set; }
        public string CPUHealth { get; set; }

        public string PostState { get; set; }

        //power
        public string PowerState { get; set; }
        public string PowerAutoOn { get; set; }
        public string PowerOnDelay { get; set; }
        public string PowerRegulatorMode { get; set; }
        public string PowerAllocationLimit { get; set; }


        //iLO Self Test Results
        public string NVRAMDataNotes { get; set; }
        public string NVRAMDataStatus { get; set; }
        public string NVRAMSpaceNotes { get; set; }
        public string NVRAMSpaceStatus { get; set; }
        public string EmbeddedFlashSDCardNotes { get; set; }
        public string EmbeddedFlashSDCardStatus { get; set; }
        public string EEPROMNotes { get; set; }
        public string EEPROMStatus { get; set; }

        public string HostRomNotes { get; set; }
        public string HostRomStatus { get; set; }
        public string SupportedHostNotes { get; set; }
        public string SupportedHostStatus { get; set; }

        public string PowerManagementControllerNotes { get; set; }
        public string PowerManagementControllerStatus { get; set; }
        public string CPLDPAL0Notes { get; set; }
        public string CPLDPAL0Status { get; set; }
        public string CPLDPAL1Notes { get; set; }
        public string CPLDPAL1Status { get; set; }

        public string ILOLicense { get; set; }

        //Embedded Health Summary
        public string BiosHardwareStatus { get; set; }
        public string FansStatus { get; set; }
        public string FansRedundancy { get; set; }
        public string TemperatureStatus { get; set; }
        public string PowerSuppliesStatus { get; set; }

        public string PowerSupplyRedundancy { get; set; }
        public string ProcessorStatus { get; set; }

        public string MemoryStatus { get; set; }
        public string NetworkStatus { get; set; }
        public string StorageStatus { get; set; }
        //To hold if GEN 10 TPM is installed and visible. If so this need to be set to hidden prior to installing any updates. Requires reboot after property is changed.
        public string TpmVisibility { get; set; }
        public string TpmType { get; set; }
        public string TpmState { get; set; }



        //REPORT Logical disk info
        public string LogicalDiskNumber { get; set; }
        public string RaidType { get; set; }
        public string LogicalDiskStatus { get; set; }

        public string PhysicalDiskNumber { get; set; }
        public string Capacity { get; set; }
        public string PhysicalDiskStatus { get; set; }
        public string InterFace { get; set; }
        public string DiskModel { get; set; }
        public string DiskLocation { get; set; }
        public string Health { get; set; }

        public StackPanel DriveReport { get; set; }


        public HealthStorageHP StorageInfo { get; set; }
        public ObservableCollection<HealthFansHP> FanInfo { get; set; }
        public ObservableCollection<HealthCPUHP> CPUInfo { get; set; }
        public ObservableCollection<HealthMemoryHP> MemoryInfo { get; set; }

        public ObservableCollection<HealthTemperatureHP> TemperatureInfo { get; set; }
        public ObservableCollection<HealthPowerHP> PowerInfo { get; set; }
        public ObservableCollection<HealthNetworkHP> NICInfo { get; set; }
        //TO HOLD PHYSICAL DRIVES SEPARATE TO LOGICAL\PHYSICAL
        public ObservableCollection<PhysicalDrivesHP> PhysicalDriveInfo { get; set; }



    }


    public class HealthTemperatureHP
    {

        public HealthTemperatureHP()
        {

        }
        public HealthTemperatureHP(string label, string location, string status, string current, string caution, string critical)
        {
            Label = label;
            Location = location;
            Status = status;
            CurrentReading = current;
            Caution = caution;
            Critical = critical;
        }
        public string Label { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }
        public string CurrentReading { get; set; }
        public string Caution { get; set; }
        public string Critical { get; set; }
    }

    public class HealthFansHP
    {
        public HealthFansHP()
        {

        }

        public HealthFansHP(string label, string zone, string status, string speed)
        {
            Label = label;
            Zone = zone;
            Status = status;
            Speed = speed;

        }
        public string Label { get; set; }
        public string Zone { get; set; }
        public string Status { get; set; }
        public string Speed { get; set; }

    }

    public class HealthNetworkHP
    {
        public HealthNetworkHP()
        {

        }
        public HealthNetworkHP(string netport, string prtdescription, string location, string ipaddress, string macadd, string status)
        {
            NetworkPort = netport;
            PortDescription = prtdescription;
            Location = location;
            IPAddress = ipaddress;
            MacAddress = macadd;
            Status = status;
        }
        public string NetworkPort { get; set; }
        public string PortDescription { get; set; }
        public string Location { get; set; }

        public string IPAddress { get; set; }
        public string MacAddress { get; set; }
        public string Status { get; set; }


    }

    public class HealthMemoryHP
    {

        public HealthMemoryHP()
        {

        }

        public HealthMemoryHP(string label, string socket, string status, string hpsmartmemory, string partno, string memtype, string size, string frequency, string minvoltage, string ranks, string technology)
        {
            Label = label;
            Socket = socket;
            Status = status;
            HPSmartMemory = hpsmartmemory;
            PartNo = PartNo;
            MemType = memtype;
            Size = size;
            Frequency = frequency;
            MinVoltage = minvoltage;
            Ranks = ranks;
            Technology = technology;
        }

        public string Label { get; set; }
        public string Socket { get; set; }
        public string Status { get; set; }
        public string HPSmartMemory { get; set; }
        public string PartNo { get; set; }
        public string MemType { get; set; }
        public string Size { get; set; }

        public string Frequency { get; set; }
        public string MinVoltage { get; set; }

        public string Ranks { get; set; }
        public string Technology { get; set; }

    }

    public class HealthPowerHP
    {
        public HealthPowerHP()
        {

        }

        public HealthPowerHP(string label, string present, string status, string pds, string hotplugcap, string model, string spare, string serial, string capacity, string firmware)
        {
            Label = label;
            Present = present;
            Status = status;
            PDS = pds;
            HotPluggable = hotplugcap;
            Model = model;
            Spare = spare;
            Serial = serial;
            Capacity = capacity;
            Firmware = firmware;

        }

        public string Label { get; set; }
        public string Present { get; set; }
        public string Status { get; set; }
        public string PDS { get; set; }
        public string HotPluggable { get; set; }
        public string Model { get; set; }
        public string Spare { get; set; }
        public string Serial { get; set; }
        public string Capacity { get; set; }
        public string Firmware { get; set; }

    }

    public class HealthCPUHP
    {
        public HealthCPUHP()
        {

        }

        public HealthCPUHP(string label, string name, string status, string speed, string exetech, string memtech, string l1cache, string l2cache, string l3cache)
        {
            Label = label;
            Name = name;
            Status = status;
            Speed = speed;
            ExecutionTech = exetech;
            MemoryTech = memtech;
            L1Cache = l1cache;
            L2Cache = l2cache;
            L3Cache = l3cache;

        }
        public string Label { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Speed { get; set; }
        public string ExecutionTech { get; set; }
        public string MemoryTech { get; set; }
        public string L1Cache { get; set; }
        public string L2Cache { get; set; }
        public string L3Cache { get; set; }

    }

    public class HealthStorageHP
    {

        public HealthStorageHP()
        {
            DriveEnclosure = new ObservableCollection<DriveEnclosureInfoHP>();
            LogicalDrives = new ObservableCollection<HealthLogicalDriveHP>();
        }
        public HealthStorageHP(string label, string status, string controllerstatus, string serial, string model, string firmware, string discoverystatus, ObservableCollection<DriveEnclosureInfoHP> encolsureinfo)
        {
            Label = label;
            Status = status;
            ControllerStatus = controllerstatus;
            Serial = serial;
            Model = model;
            Firmware = firmware;
            DiscoveryStatus = discoverystatus;
            LogicalDrives = new ObservableCollection<HealthLogicalDriveHP>();
            DriveEnclosure = new ObservableCollection<DriveEnclosureInfoHP>();
            DriveEnclosure = encolsureinfo;
        }
        public string Label { get; set; }
        public string Status { get; set; }
        public string ControllerStatus { get; set; }

        public string Serial { get; set; }
        public string Model { get; set; }
        public string Firmware { get; set; }

        public string DiscoveryStatus { get; set; }

        public HealthLogicalDriveHP LogicalDrive { get; set; }
        public ObservableCollection<HealthLogicalDriveHP> LogicalDrives { get; set; }

        public ObservableCollection<DriveEnclosureInfoHP> DriveEnclosure { get; set; }


    }

    public class DriveEnclosureInfoHP
    {
        public DriveEnclosureInfoHP()
        {

        }
        public DriveEnclosureInfoHP(string label, string status, string drivebay)
        {
            Label = label;
            Status = status;
            DriveBay = drivebay;

        }

        public string Label { get; set; }
        public string Status { get; set; }
        public string DriveBay { get; set; }

    }



    //LOGICAL DRIVE
    public class HealthLogicalDriveHP
    {

        public HealthLogicalDriveHP()
        {
            //PhysicalDrives = new ObservableCollection<PhysicalDrivesHP>();
            //PhysicalDrives.Add(new PhysicalDrivesHP("100","","","","","","","","","",""));
        }

        public HealthLogicalDriveHP(string label, string status, string capacity, string faulttolerance, string logicaldrivetype, string encryption, PhysicalDrivesHP physdrives) // , ObservableCollection<PhysicalDrivesHP> physicaldrivesinfo
        {
            

            //FOR USE WITH PHYSICAL ONLY DRIVES
            Label = label;
            Status = status;
            Capacity = capacity;
            FaultTolerance = faulttolerance;
            LogicalDriveType = logicaldrivetype;
            Encryption = encryption;


           // PhysicalDrives.Add(new PhysicalDrivesHP(physdrives.Label.ToString(), physdrives.Status.ToString(), physdrives.SerialNumber.ToString(), physdrives.Model.ToString(), physdrives.Capacity.ToString(), physdrives.MarketingCapacity.ToString(), physdrives.Location.ToString(), physdrives.FirmwareVersion.ToString(), physdrives.DriveConfiguration.ToString(), physdrives.EncryptionStatus.ToString(), physdrives.MediaType.ToString()));

        }


        public string Label { get; set; }
        public string Status { get; set; }
        public string Capacity { get; set; }
        public string FaultTolerance { get; set; }
        public string LogicalDriveType { get; set; }
        public string Encryption { get; set; }

        //Public GET_EMBEDDED_HEALTH_DATA_HP.GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERLOGICAL_DRIVEPHYSICAL_DRIVE[] PhysDrives;
        //public ObservableCollection<PhysicalDrivesHP> PhysicalDrives { get; set; }


    }


    //PHYSICAL DRIVES
    public class PhysicalDrivesHP
    {
        public PhysicalDrivesHP()
        {

        }
        public PhysicalDrivesHP(string label, string status, string serialnumber, string model, string capacity, string marketingcapacity, string location, string firmwareversion, string driveconfiguration, string encryptionstatus, string mediatype)
        {
            Label = label;
            Status = status;
            SerialNumber = serialnumber;
            Model = model;
            Capacity = capacity;
            MarketingCapacity = marketingcapacity;
            Location = location;
            FirmwareVersion = firmwareversion;
            DriveConfiguration = driveconfiguration;
            EncryptionStatus = encryptionstatus;
            MediaType = mediatype;
        }

        public string odataId { get; set; }
        public string BlockSize { get; set; }
        public string CurrentTemp { get; set; }
        public string MaxTemp { get; set; }
        public string RotationalSpeed { get; set; }
        public string SSDEndurance { get; set; }
        public string Interface { get; set; }
        public string InterfaceSpeed { get; set; }
        public string Label { get; set; }
        public string Status { get; set; }
        public string Health { get; set; }
        public string SerialNumber { get; set; }
        public string SerialNo { get; set; }
        public string Model { get; set; }
        public string Capacity { get; set; }
        public string MarketingCapacity { get; set; }
        public string Location { get; set; }
        public string FirmwareVersion { get; set; }
        public string DriveConfiguration { get; set; }

        public string EncryptionStatus { get; set; }
        public string Encryption { get; set; }
        public string MediaType { get; set; }

    }


    //public class HPPhysicalDrivesModel : HasPropertyChanged
    //{




    //    public string _Model;
    //    public string Model
    //    {
    //        get { return _Model; }
    //        set
    //        {
    //            _Model = value;
    //            OnPropertyChanged("Model");
    //        }
    //    }

    //    public string _Capacity;
    //    public string Capacity
    //    {
    //        get { return _Capacity; }
    //        set
    //        {
    //            _Capacity = value;
    //            OnPropertyChanged("Capacity");
    //        }
    //    }

    //    public string _Interface;
    //    public string Interface
    //    {
    //        get { return _Interface; }
    //        set
    //        {
    //            _Interface = value;
    //            OnPropertyChanged("Interface");
    //        }
    //    }

    //    public string _Location;
    //    public string Location
    //    {
    //        get { return _Location; }
    //        set
    //        {
    //            _Location = value;
    //            OnPropertyChanged("Location");
    //        }
    //    }

    //    public string _SerialNumber;
    //    public string SerialNumber
    //    {
    //        get { return _SerialNumber; }
    //        set
    //        {
    //            _SerialNumber = value;
    //            OnPropertyChanged("SerialNumber");
    //        }
    //    }

    //    public string _InterfaceSpeed;
    //    public string InterfaceSpeed
    //    {
    //        get { return _InterfaceSpeed; }
    //        set
    //        {
    //            _InterfaceSpeed = value;
    //            OnPropertyChanged("InterfaceSpeed");
    //        }
    //    }


    //    public string _Encryption;
    //    public string Encryption
    //    {
    //        get { return _Encryption; }
    //        set
    //        {
    //            _Encryption = value;
    //            OnPropertyChanged("Encryption");
    //        }
    //    }


    //    public string _Temp;
    //    public string Temp
    //    {
    //        get { return _Temp; }
    //        set
    //        {
    //            _Temp = value;
    //            OnPropertyChanged("Temp");
    //        }
    //    }

    //    public string _BlockSize;
    //    public string BlockSize
    //    {
    //        get { return _BlockSize; }
    //        set
    //        {
    //            _BlockSize = value;
    //            OnPropertyChanged("BlockSize");
    //        }
    //    }

    //    public string _MediaType;
    //    public string MediaType
    //    {
    //        get { return _MediaType; }
    //        set
    //        {
    //            _MediaType = value;
    //            OnPropertyChanged("MediaType");
    //        }
    //    }

    //    public string _Health;
    //    public string Health
    //    {
    //        get { return _Health; }
    //        set
    //        {
    //            _Health = value;
    //            OnPropertyChanged("Health");
    //        }
    //    }


    //}


}
