﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nebula.Models.WipeDrive
{
    public class WipeDriveModel
    {
        public string CloudCode { get; set; }
        public Report Report { get; set; }
    }

    public class Report
    {
        public string GroupUUID { get; set; }
        public string ProductName { get; set; }
        public string ProductVersion { get; set; }
        public string KernelVersion { get; set; }
        public Job[] Jobs { get; set; }
        public Hardware Hardware { get; set; }
        public Hardwaretests HardwareTests { get; set; }
    }

    public class Hardware
    {
        public Computer Computer { get; set; }
        public Motherboard Motherboard { get; set; }
        public Processor[] Processors { get; set; }
        public BIOS BIOS { get; set; }
        public Ramstick[] RamSticks { get; set; }
        public NIC[] NICS { get; set; }
        public Displayadapter[] DisplayAdapters { get; set; }
        public Multimediacard[] MultimediaCards { get; set; }
        public Storagecontroller[] StorageControllers { get; set; }
        public object[] Batteries { get; set; }
        public object[] OpticalDrives { get; set; }
        public Device[] Devices { get; set; }
        public Mouse[] Mice { get; set; }
        public Keyboard[] Keyboards { get; set; }
    }

    public class Computer
    {
        public string AssetTag { get; set; }
        public string ChassisType { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Serial { get; set; }
        public int USBPorts { get; set; }
        public int USB2Ports { get; set; }
        public int USB3Ports { get; set; }
        public int FirewirePorts { get; set; }
        public int ThunderboltPorts { get; set; }
        public int Thunderbolt2Ports { get; set; }
        public int Thunderbolt3Ports { get; set; }
        public int PCIExpressSlots { get; set; }
        public int TPM { get; set; }
    }

    public class Motherboard
    {
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string AssetTag { get; set; }
        public string Serial { get; set; }
        public string Version { get; set; }
        public int TotalRamSlots { get; set; }
    }

    public class BIOS
    {
        public string Manufacturer { get; set; }
        public string Version { get; set; }
        public string Date { get; set; }
        public string ROMSize { get; set; }
        public string Revision { get; set; }
        public string FirmwareRevision { get; set; }
        public string Characteristics { get; set; }
    }

    public class Processor
    {
        public string ID { get; set; }
        public string Model { get; set; }
        public string Manufacturer { get; set; }
        public string Family { get; set; }
        public string Serial { get; set; }
        public string AssetTag { get; set; }
        public int Cores { get; set; }
        public int Threads { get; set; }
        public string Speed { get; set; }
        public string MaxSpeed { get; set; }
        public string L1Cache { get; set; }
        public string L2Cache { get; set; }
        public string L3Cache { get; set; }
        public string Voltage { get; set; }
        public string Signature { get; set; }
        public string Characteristics { get; set; }
        public string Flags { get; set; }
    }

    public class Ramstick
    {
        public string AssetTag { get; set; }
        public string Capacity { get; set; }
        public string FormFactor { get; set; }
        public string Manufacturer { get; set; }
        public string PartNumber { get; set; }
        public string Serial { get; set; }
        public string Speed { get; set; }
        public string Type { get; set; }
        public string TypeDetail { get; set; }
    }

    public class NIC
    {
        public string MACAddress { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string PciId { get; set; }
        public string Speed { get; set; }
        public string Interface { get; set; }
        public string Capabilities { get; set; }
        public string Configuration { get; set; }
    }

    public class Displayadapter
    {
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string PciId { get; set; }
    }

    public class Multimediacard
    {
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string PciId { get; set; }
    }

    public class Storagecontroller
    {
        public string Description { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string PciId { get; set; }
    }

    public class Device
    {
        public string UUID { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Serial { get; set; }
        public int DriveMediaType { get; set; }
        public string Firmware { get; set; }
        public long Size { get; set; }
        public int LogicalSectorSize { get; set; }
        public int PhysicalSectorSize { get; set; }
        public string PSID { get; set; }
        public string WWID { get; set; }
        public Prewipesmartdata PreWipeSmartData { get; set; }
        public Postwipesmartdata PostWipeSmartData { get; set; }
        public int SSDPercentageLifeUsed { get; set; }
        public int TrimType { get; set; }
        public int Transport { get; set; }
        public string EUI64 { get; set; }
        public string EstimatedLifeRemaining { get; set; }
        public string EstimatedLifeRemainingReason { get; set; }
    }

    public class Prewipesmartdata
    {
        public Smartattribute[] SmartAttributes { get; set; }
        public object[] SelfTestResults { get; set; }
        public string OverallHealth { get; set; }
    }

    public class Smartattribute
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Flag { get; set; }
        public string Value { get; set; }
        public string Worst { get; set; }
        public string Threshold { get; set; }
        public string Type { get; set; }
        public string Updated { get; set; }
        public string WhenFailed { get; set; }
        public string RawValue { get; set; }
    }

    public class Postwipesmartdata
    {
        public object[] SmartAttributes { get; set; }
        public object[] SelfTestResults { get; set; }
        public string OverallHealth { get; set; }
    }

    public class Mouse
    {
        public string Product { get; set; }
    }

    public class Keyboard
    {
        public string Product { get; set; }
    }

    public class Hardwaretests
    {
        public int TimeStarted { get; set; }
        public int TimeCompleted { get; set; }
        public object[] Display { get; set; }
        public object[] Mouse { get; set; }
        public object[] Keyboard { get; set; }
        public object[] Processor { get; set; }
        public object[] Network { get; set; }
        public object[] Storage { get; set; }
        public object[] Ram { get; set; }
        public object[] Webcam { get; set; }
        public object[] Sound { get; set; }
        public object[] Battery { get; set; }
        public object[] Ports { get; set; }
    }

    public class Job
    {
        public string Name { get; set; }
        public string UUID { get; set; }
        public int Number { get; set; }
        public int Action { get; set; }
        public string PatternName { get; set; }
        public string PatternShortName { get; set; }
        public string PatternIndex { get; set; }
        public string UserName { get; set; }
        public string ComputerID { get; set; }
        public string CustomField { get; set; }
        public object[] CustomJobFields { get; set; }
        public Operation[] Operations { get; set; }
        public string Options { get; set; }
        public int TpmAction { get; set; }
    }

    public class Operation
    {
        public string SecurityCode { get; set; }
        public long TotalSectors { get; set; }
        public int TotalSectorsOverwritten { get; set; }
        public int TotalSectorsVerified { get; set; }
        public int DirtySectors { get; set; }
        public int DriveErrors { get; set; }
        public int TimeStarted { get; set; }
        public int TimeEnded { get; set; }
        public int Passes { get; set; }
        public int ActionResult { get; set; }
        public Pretest PreTest { get; set; }
        public bool HPAFound { get; set; }
        public bool HPARemoved { get; set; }
        public bool DCOFound { get; set; }
        public bool DCORemoved { get; set; }
        public bool DCOLockFound { get; set; }
        public bool DCOLockRemoved { get; set; }
        public bool AMAXFound { get; set; }
        public bool AMAXRemoved { get; set; }
        public bool SecurityLocked { get; set; }
        public bool SecurityFrozenFound { get; set; }
        public bool SecurityFrozenRemoved { get; set; }
        public int EnhancedSecureErasePasses { get; set; }
        public int SanitizeCryptoErasePasses { get; set; }
        public int SanitizeBlockErasePasses { get; set; }
        public int SanitizeOverwritePasses { get; set; }
        public int OpalCryptoErasePasses { get; set; }
        public int TrimPasses { get; set; }
        public string UUID { get; set; }
        public string FailureReason { get; set; }
        public int NistWipeLevel { get; set; }
        public int RequiredNistWipeLevel { get; set; }
        public string NistLevelReason { get; set; }
        public string DcoLockWarning { get; set; }
        public string StorageDeviceUUID { get; set; }
        public int ZeroPassesAdded { get; set; }
    }

    public class Pretest
    {
        public int PreTestResult { get; set; }
        public int PreTestSectorsRead { get; set; }
        public int PreTestSeconds { get; set; }
    }




}
