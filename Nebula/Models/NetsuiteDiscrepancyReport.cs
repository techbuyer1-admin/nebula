﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nebula.Helpers;

namespace Nebula.Models
{
    public class NetsuiteDiscrepancyReport : HasPropertyChanged
    {
        public NetsuiteDiscrepancyReport()
        {
            //Collection to hold serials
            SerialNumbers = new ObservableCollection<string>();
        }

        public String _PONumber;
        public String PONumber
        {
            get { return _PONumber; }
            set
            {
                _PONumber = value;
                OnPropertyChanged("PONumber");
            }
        }

        private string _LineType;
        public string LineType
        {
            get { return _LineType; }
            set
            {
                _LineType = value;
                OnPropertyChanged("LineType");
            }
        }

        public String _ProductCode;
        public String ProductCode
        {
            get { return _ProductCode; }
            set
            {
                _ProductCode = value;
                OnPropertyChanged("ProductCode");
            }
        }


        public String _ProductDescription;
        public String ProductDescription
        {
            get { return _ProductDescription; }
            set
            {
                _ProductDescription = value;
                OnPropertyChanged("ProductDescription");
            }
        }

        private string _POLineUnitPrice;
        public string POLineUnitPrice
        {
            get { return _POLineUnitPrice; }
            set
            {
                _POLineUnitPrice = value;
                OnPropertyChanged("POLineUnitPrice");
            }
        }

        private string _POUnitAmount;
        public string POUnitAmount
        {
            get { return _POUnitAmount; }
            set
            {
                _POUnitAmount = value;
                OnPropertyChanged("POUnitAmount");
            }
        }

        public String _ExpectedQuantity;
        public String ExpectedQuantity
        {
            get { return _ExpectedQuantity; }
            set
            {
                _ExpectedQuantity = value;
                OnPropertyChanged("ExpectedQuantity");
            }
        }

        public String _ReceivedQuantity;
        public String ReceivedQuantity
        {
            get { return _ReceivedQuantity; }
            set
            {
                _ReceivedQuantity = value;
                OnPropertyChanged("ReceivedQuantity");
            }
        }

        //New Part Code
        public bool _NewPartCode;
        public bool NewPartCode
        {
            get { return _NewPartCode; }
            set
            {
                _NewPartCode = value;
                OnPropertyChanged("NewPartCode");
            }
        }

        public String _Discrepancy;
        public String Discrepancy
        {
            get { return _Discrepancy; }
            set
            {
                _Discrepancy = value;
                OnPropertyChanged("Discrepancy");
            }
        }



        public String _Comments;
        public String Comments
        {
            get { return _Comments; }
            set
            {
                _Comments = value;
                OnPropertyChanged("Comments");
            }
        }


        public ObservableCollection<string> _SerialNumbers;
        public ObservableCollection<string> SerialNumbers
        {
            get { return _SerialNumbers; }
            set
            {
                _SerialNumbers = value;
                OnPropertyChanged("SerialNumbers");
            }
        }

    }
}
