﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Models
{
    public class CedarInteropModel
    {

        public CedarInteropModel()
        {
                
        }

        public async Task<string> GetCedarRequest()
        {




            var client = new HttpClient();
            //var request = new HttpRequestMessage
            //{
            //    Method = HttpMethod.Get,
            //    RequestUri = new Uri("https://cedar-azure.techbuyer.com/api"),

            //};

            var userName = "35edba61-ce4d-409f-8d47-de2bc86dc3UK";
            var userPassword = "D1dUkn0wTh4tW1p1ngInTheUKIsS3cur3";

            var authenticationString = $"{userName}:{userPassword}";
            var base64String = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(authenticationString));

            var requestMessage = new HttpRequestMessage(HttpMethod.Get, "https://cedar-azure.techbuyer.com/api");
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64String);

            // add default header type
            //KEY USED BEFORE
           //client.DefaultRequestHeaders.Add("key", "0b93040a-e447-4c06-9a71-ae6aa38e18de");
           client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


            using (var response = await client.SendAsync(requestMessage))
            {
                response.EnsureSuccessStatusCode();


                //var body = await response.Content.ReadAsStringAsync();

                //DisksModel sResponse = await response.Content.ReadAsAsync<DisksModel>();
                string Cedarresponse = await response.Content.ReadAsStringAsync();

                return Cedarresponse;
                //MessageBox.Show(Cedarresponse);
            }

        }

        //POST PO NUMBER TO OBTAIN NEW KEY
        public async Task<string> PostPOCedarRequest()
        {



            using (HttpClient client = new HttpClient())
            {
                //client.DefaultRequestHeaders.Authorization =  new AuthenticationHeaderValue("Key", "0b93040a-e447-4c06-9a71-ae6aa38e18de");

                client.DefaultRequestHeaders.Add("key", "0b93040a-e447-4c06-9a71-ae6aa38e18de");

                // MessageBox.Show(client.DefaultRequestHeaders.ToString());

                using (StringContent jsonContent = new StringContent("{\"reference\": \"PO10102\",\"operative\": \"Haylie Campbell\",\"status\": \"Backlog\"}"))
                {
                    jsonContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    using (HttpResponseMessage response = await client.PostAsync("https://cedar-azure.techbuyer.com:9555/api/postOrder", jsonContent))
                    {
                        string responseString = await response.Content.ReadAsStringAsync();

                        return responseString;
                    }
                }

            }
        }

        //POST CEDAR DRIVE COLLECTION
        public async Task<string> PostDrivesCedarRequest(string keyID)
        {



            using (HttpClient client = new HttpClient())
            {

                using (StringContent jsonContent = new StringContent("{    \"data\": [        {        \"reference\": \"PO10102\",        \"model\": \"W347KB - REF\",        \"serial\": \"ZYT98XW4R\",        \"description\": \"DELL 600GB 15K 6G 3.5INCH SAS HDD\",        \"status\": \"Awaiting Test\"        },        {        \"reference\": \"PO10102\",        \"model\": \"W347KB - REF\",        \"serial\": \"ZYT98XW4R\",        \"description\": \"DELL 600GB 15K 6G 3.5INCH SAS HDD\",        \"status\": \"Awaiting Test\"        }    ]}"))
                {
                    jsonContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    using (HttpResponseMessage response = await client.PostAsync("https://cedar-azure.techbuyer.com:9555/api/postOrderDrives/" + keyID + "", jsonContent))
                    {
                        string responseString = await response.Content.ReadAsStringAsync();

                        return responseString;
                    }
                }
            }
        }

    }
}
