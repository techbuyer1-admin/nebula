﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Models
{
    public class Gen10DiagnosticReport
    {
       // public ObservableCollection<diagOutput> diagCollection { get; set; }
        public Gen10DiagnosticReport()
        {
            //diagCollection = new ObservableCollection<diagOutput>(); 
           // diagOutput diag = new diagOutput();
            //diag.
        }
        
        // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class diagOutput
        {

            private diagOutputTestLogRecord[] testLogRecordField;

            private string typeField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("testLogRecord")]
            public diagOutputTestLogRecord[] testLogRecord
            {
                get
                {
                    return this.testLogRecordField;
                }
                set
                {
                    this.testLogRecordField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class diagOutputTestLogRecord
        {

            private diagOutputTestLogRecordTestCompletion testCompletionField;

            private string componentField;

            private string deviceField;

            private string testField;

            private string testCaptionField;

            private string testTimeField;

            private byte passedCountField;

            private byte failedCountField;

            /// <remarks/>
            public diagOutputTestLogRecordTestCompletion testCompletion
            {
                get
                {
                    return this.testCompletionField;
                }
                set
                {
                    this.testCompletionField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string component
            {
                get
                {
                    return this.componentField;
                }
                set
                {
                    this.componentField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string device
            {
                get
                {
                    return this.deviceField;
                }
                set
                {
                    this.deviceField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string test
            {
                get
                {
                    return this.testField;
                }
                set
                {
                    this.testField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string testCaption
            {
                get
                {
                    return this.testCaptionField;
                }
                set
                {
                    this.testCaptionField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string testTime
            {
                get
                {
                    return this.testTimeField;
                }
                set
                {
                    this.testTimeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte passedCount
            {
                get
                {
                    return this.passedCountField;
                }
                set
                {
                    this.passedCountField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte failedCount
            {
                get
                {
                    return this.failedCountField;
                }
                set
                {
                    this.failedCountField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class diagOutputTestLogRecordTestCompletion
        {

            private string completionTimeField;

            private string resultField;

            private string testTimeField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string completionTime
            {
                get
                {
                    return this.completionTimeField;
                }
                set
                {
                    this.completionTimeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string result
            {
                get
                {
                    return this.resultField;
                }
                set
                {
                    this.resultField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string testTime
            {
                get
                {
                    return this.testTimeField;
                }
                set
                {
                    this.testTimeField = value;
                }
            }
        }
    }
     
}
