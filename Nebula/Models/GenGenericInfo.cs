﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nebula.Helpers;

namespace Nebula.Models
{
    public class GenGenericInfo :  HasPropertyChanged //, IEnumerator, IEnumerable
    {
        //SD CARD Presence
        public string SDCardInserted { get; set; }

        //Firmware
        public string BiosCurrentVersion { get; set; }
        public string BiosBackupVersion { get; set; }
        public string iLOVersion { get; set; }
        public string SPSVersion { get; set; }
        public string IntelligentProvisioningLocation { get; set; }
        public string IntelligentProvisioningVersion { get; set; }


        //Server Info
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string ChassisSerial { get; set; }
        public string AssetTag { get; set; }
        public string HostName { get; set; }
        public string HealthStatus { get; set; }
        public string SKU { get; set; }


        private string _OutOfRegionMessage;

        public string OutOfRegionMessage
        {
            get { return _OutOfRegionMessage; }
            set
            {
                _OutOfRegionMessage = value;
                OnPropertyChanged("OutOfRegionMessage");
            }
        }



        //memory
        public string TotalMemory { get; set; }
        public string MemoryHealth { get; set; }

        //cpu
        public string CPUFamily { get; set; }
        public string CPUCount { get; set; }
        public string CPUHealth { get; set; }

        public string PostState { get; set; }

        //power
        public string PowerState { get; set; }
        public string PowerAutoOn { get; set; }
        public string PowerOnDelay { get; set; }
        public string PowerRegulatorMode { get; set; }
        public string PowerAllocationLimit { get; set; }


        //iLO Self Test Results
        public string NVRAMDataNotes { get; set; }
        public string NVRAMDataStatus { get; set; }
        public string NVRAMSpaceNotes { get; set; }
        public string NVRAMSpaceStatus { get; set; }
        public string EmbeddedFlashSDCardNotes { get; set; }
        public string EmbeddedFlashSDCardStatus { get; set; }
        public string EEPROMNotes { get; set; }
        public string EEPROMStatus { get; set; }

        public string HostRomNotes { get; set; }
        public string HostRomStatus { get; set; }
        public string SupportedHostNotes { get; set; }
        public string SupportedHostStatus { get; set; }

        public string PowerManagementControllerNotes { get; set; }
        public string PowerManagementControllerStatus { get; set; }
        public string CPLDPAL0Notes { get; set; }
        public string CPLDPAL0Status { get; set; }
        public string CPLDPAL1Notes { get; set; }
        public string CPLDPAL1Status { get; set; }

        //Embedded Health Summary
        public string BiosHardwareStatus { get; set; }
        public string FansStatus { get; set; }
        public string FansRedundancy { get; set; }
        public string TemperatureStatus { get; set; }
        public string PowerSuppliesStatus { get; set; }

        public string PowerSupplyRedundancy { get; set; }
        public string ProcessorStatus { get; set; }

        public string MemoryStatus { get; set; }
        public string NetworkStatus { get; set; }
        public string StorageStatus { get; set; }

        public HealthStorageGenGeneric StorageInfo { get; set; }
        public ObservableCollection<HealthFansGenGeneric> FanInfo { get; set; }
        public ObservableCollection<HealthCPUGenGeneric> CPUInfo { get; set; }
        public ObservableCollection<HealthMemoryGenGeneric> MemoryInfo { get; set; }

        public ObservableCollection<HealthTemperatureGenGeneric> TemperatureInfo { get; set; }
        public ObservableCollection<HealthPowerGenGeneric> PowerInfo { get; set; }
        public ObservableCollection<HealthNetworkGenGeneric> NICInfo { get; set; }

    }


    public class HealthTemperatureGenGeneric
    {
        public HealthTemperatureGenGeneric(string label, string location, string status, string current, string caution, string critical)
        {
            Label = label;
            Location = location;
            Status = status;
            CurrentReading = current;
            Caution = caution;
            Critical = critical;
        }
        public string Label { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }
        public string CurrentReading { get; set; }
        public string Caution { get; set; }
        public string Critical { get; set; }
    }

    public class HealthFansGenGeneric
    {
        public HealthFansGenGeneric(string label, string zone, string status, string speed)
        {
            Label = label;
            Zone = zone;
            Status = status;
            Speed = speed;

        }
        public string Label { get; set; }
        public string Zone { get; set; }
        public string Status { get; set; }
        public string Speed { get; set; }

    }

    public class HealthNetworkGenGeneric
    {
        public HealthNetworkGenGeneric(string netport, string prtdescription, string location, string ipaddress, string macadd, string status)
        {
            NetworkPort = netport;
            PortDescription = prtdescription;
            Location = location;
            IPAddress = ipaddress;
            MacAddress = macadd;
            Status = status;
        }
        public string NetworkPort { get; set; }
        public string PortDescription { get; set; }
        public string Location { get; set; }

        public string IPAddress { get; set; }
        public string MacAddress { get; set; }
        public string Status { get; set; }


    }

    public class HealthMemoryGenGeneric
    {
        public HealthMemoryGenGeneric(string label, string socket, string status, string hpsmartmemory, string partno, string memtype, string size, string frequency, string minvoltage, string ranks, string technology)
        {
            Label = label;
            Socket = socket;
            Status = status;
            HPSmartMemory = hpsmartmemory;
            PartNo = PartNo;
            MemType = memtype;
            Size = size;
            Frequency = frequency;
            MinVoltage = minvoltage;
            Ranks = ranks;
            Technology = technology;
        }

        public string Label { get; set; }
        public string Socket { get; set; }
        public string Status { get; set; }
        public string HPSmartMemory { get; set; }
        public string PartNo { get; set; }
        public string MemType { get; set; }
        public string Size { get; set; }

        public string Frequency { get; set; }
        public string MinVoltage { get; set; }

        public string Ranks { get; set; }
        public string Technology { get; set; }

    }

    public class HealthPowerGenGeneric
    {
        public HealthPowerGenGeneric(string label, string present, string status, string pds, string hotplugcap, string model, string spare, string serial, string capacity, string firmware)
        {
            Label = label;
            Present = present;
            Status = status;
            PDS = pds;
            HotPluggable = hotplugcap;
            Model = model;
            Spare = spare;
            Serial = serial;
            Capacity = capacity;
            Firmware = firmware;

        }

        public string Label { get; set; }
        public string Present { get; set; }
        public string Status { get; set; }
        public string PDS { get; set; }
        public string HotPluggable { get; set; }
        public string Model { get; set; }
        public string Spare { get; set; }
        public string Serial { get; set; }
        public string Capacity { get; set; }
        public string Firmware { get; set; }

    }

    public class HealthCPUGenGeneric
    {
        public HealthCPUGenGeneric(string label, string name, string status, string speed, string exetech, string memtech, string l1cache, string l2cache, string l3cache)
        {
            Label = label;
            Name = name;
            Status = status;
            Speed = speed;
            ExecutionTech = exetech;
            MemoryTech = memtech;
            L1Cache = l1cache;
            L2Cache = l2cache;
            L3Cache = l3cache;

        }
        public string Label { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Speed { get; set; }
        public string ExecutionTech { get; set; }
        public string MemoryTech { get; set; }
        public string L1Cache { get; set; }
        public string L2Cache { get; set; }
        public string L3Cache { get; set; }

    }

    public class HealthStorageGenGeneric
    {

        public HealthStorageGenGeneric()
        {
            DriveEnclosure = new ObservableCollection<DriveEnclosureInfoGenGeneric>();
        }
        public HealthStorageGenGeneric(string label, string status, string controllerstatus, string serial, string model, string firmware, string discoverystatus, ObservableCollection<DriveEnclosureInfoGenGeneric> encolsureinfo)
        {
            Label = label;
            Status = status;
            ControllerStatus = controllerstatus;
            Serial = serial;
            Model = model;
            Firmware = firmware;
            DiscoveryStatus = discoverystatus;
            DriveEnclosure = new ObservableCollection<DriveEnclosureInfoGenGeneric>();
            DriveEnclosure = encolsureinfo;
        }
        public string Label { get; set; }
        public string Status { get; set; }
        public string ControllerStatus { get; set; }

        public string Serial { get; set; }
        public string Model { get; set; }
        public string Firmware { get; set; }

        public string DiscoveryStatus { get; set; }

        public ObservableCollection<DriveEnclosureInfoGenGeneric> DriveEnclosure { get; set; }


    }

    public class DriveEnclosureInfoGenGeneric
    {
        public DriveEnclosureInfoGenGeneric()
        {

        }
        public DriveEnclosureInfoGenGeneric(string label, string status, string drivebay)
        {
            Label = label;
            Status = status;
            DriveBay = drivebay;

        }

        public string Label { get; set; }
        public string Status { get; set; }
        public string DriveBay { get; set; }

    }


}

