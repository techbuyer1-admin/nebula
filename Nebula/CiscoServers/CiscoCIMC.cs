﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.CiscoServers
{
   

    //public class CiscoCIMC
    //{
    //    public string DateTimeLocalOffset { get; set; }
    //    public string Id { get; set; }
    //    public Links Links { get; set; }
    //    public string ManagerType { get; set; }
    //    public string odatacontext { get; set; }
    //    public string odataid { get; set; }
    //    public Virtualmedia VirtualMedia { get; set; }
    //    public string Description { get; set; }
    //    public Logservices LogServices { get; set; }
    //    public Serialinterfaces SerialInterfaces { get; set; }
    //    public string FirmwareVersion { get; set; }
    //    public string UUID { get; set; }
    //    public Status Status { get; set; }
    //    public Managernetworkprotocol ManagerNetworkProtocol { get; set; }
    //    public string Name { get; set; }
    //    public Actions Actions { get; set; }
    //    public string ServiceEntryPointUUID { get; set; }
    //    public string DateTime { get; set; }
    //    public string odatatype { get; set; }
    //    public Graphicalconsole GraphicalConsole { get; set; }
    //    public Commandshell CommandShell { get; set; }
    //    public Serialconsole SerialConsole { get; set; }
    //    public string Model { get; set; }
    //    public Ethernetinterfaces EthernetInterfaces { get; set; }
    //}

    //public class Links
    //{
    //    public string[] ManagerForChassis { get; set; }
    //    public string[] ManagerForServers { get; set; }
    //}

    //public class Virtualmedia
    //{
    //    [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
    //    public string odataid { get; set; }
    //}

    //public class Logservices
    //{
    //    [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
    //    public string odataid { get; set; }
    //}

    //public class Serialinterfaces
    //{
    //    [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
    //    public string odataid { get; set; }
    //}

    //public class Status
    //{
    //    public string State { get; set; }
    //    public string Health { get; set; }
    //}

    //public class Managernetworkprotocol
    //{
    //    [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
    //    public string odataid { get; set; }
    //}

    //public class Actions
    //{
    //    public OemImportbmcconfig OemImportBmcConfig { get; set; }
    //    public OemBmcfwupdate OemBmcFwUpdate { get; set; }
    //    public ManagerReset ManagerReset { get; set; }
    //    public OemBiosfwupdate OemBiosFwUpdate { get; set; }
    //    public OemExportbmcconfig OemExportBmcConfig { get; set; }
    //    public OemBmctechsupportexport OemBmcTechSupportExport { get; set; }
    //}

    //public class OemImportbmcconfig
    //{
    //    public string[] ProtocolRedfishAllowableValues { get; set; }
    //    public string[] PassphraseRedfishAllowableValues { get; set; }
    //    public string[] RemoteUsernameRedfishAllowableValues { get; set; }
    //    public string[] RemotePathRedfishAllowableValues { get; set; }
    //    public string[] RemoteHostnameRedfishAllowableValues { get; set; }
    //    public string Target { get; set; }
    //    public string[] RemotePasswordRedfishAllowableValues { get; set; }
    //}

    //public class OemBmcfwupdate
    //{
    //    public string[] ProtocolRedfishAllowableValues { get; set; }
    //    public string Target { get; set; }
    //    public string[] RemoteUsernameRedfishAllowableValues { get; set; }
    //    public string[] RemotePathRedfishAllowableValues { get; set; }
    //    public string[] RemoteHostnameRedfishAllowableValues { get; set; }
    //    public string[] RemotePasswordRedfishAllowableValues { get; set; }
    //}

    //public class ManagerReset
    //{
    //    public string Target { get; set; }
    //    public string[] ResetTypeRedfishAllowableValues { get; set; }
    //}

    //public class OemBiosfwupdate
    //{
    //    public string[] ProtocolRedfishAllowableValues { get; set; }
    //    public string Target { get; set; }
    //    public string[] RemoteUsernameRedfishAllowableValues { get; set; }
    //    public string[] RemotePathRedfishAllowableValues { get; set; }
    //    public string[] RemoteHostnameRedfishAllowableValues { get; set; }
    //    public string[] RemotePasswordRedfishAllowableValues { get; set; }
    //}

    //public class OemExportbmcconfig
    //{
    //    public string[] ProtocolRedfishAllowableValues { get; set; }
    //    public string[] PassphraseRedfishAllowableValues { get; set; }
    //    public string[] RemoteUsernameRedfishAllowableValues { get; set; }
    //    public string[] RemotePathRedfishAllowableValues { get; set; }
    //    public string[] RemoteHostnameRedfishAllowableValues { get; set; }
    //    public string Target { get; set; }
    //    public string[] RemotePasswordRedfishAllowableValues { get; set; }
    //}

    //public class OemBmctechsupportexport
    //{
    //    public string[] ProtocolRedfishAllowableValues { get; set; }
    //    public string Target { get; set; }
    //    public string[] RemoteUsernameRedfishAllowableValues { get; set; }
    //    public string[] RemotePathRedfishAllowableValues { get; set; }
    //    public string[] RemoteHostnameRedfishAllowableValues { get; set; }
    //    public string[] RemotePasswordRedfishAllowableValues { get; set; }
    //}

    //public class Graphicalconsole
    //{
    //    public int MaxConcurrentSessions { get; set; }
    //    public string[] ConnectTypesSupported { get; set; }
    //    public bool ServiceEnabled { get; set; }
    //}

    //public class Commandshell
    //{
    //    public int MaxConcurrentSessions { get; set; }
    //    public string[] ConnectTypesSupported { get; set; }
    //    public bool ServiceEnabled { get; set; }
    //}

    //public class Serialconsole
    //{
    //    public int MaxConcurrentSessions { get; set; }
    //    public string[] ConnectTypesSupported { get; set; }
    //    public bool ServiceEnabled { get; set; }
    //}

    //public class Ethernetinterfaces
    //{
    //    public string odataid { get; set; }
    //}


}
