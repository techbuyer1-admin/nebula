﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.CiscoServers
{


    public class CiscoServerRESTSchema
    {
        public Chassis Chassis { get; set; }
        public string odataid { get; set; }
        public Jsonschemas JSONSchemas { get; set; }
        public string RedfishVersion { get; set; }
        public Eventservice EventService { get; set; }
        public Systems Systems { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public Links Links { get; set; }
        public Taskservice TaskService { get; set; }
        public Managers Managers { get; set; }
        public string odatatype { get; set; }
        public Sessionservice SessionService { get; set; }
        public string odatacontext { get; set; }
        public string Id { get; set; }
        public Accountservice AccountService { get; set; }
        public Messageregistry MessageRegistry { get; set; }
    }

    public class Chassis
    {
        [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
        public string odataid { get; set; }
    }

    public class Jsonschemas
    {
        [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
        public string odataid { get; set; }
    }

    public class Eventservice
    {
        [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
        public string odataid { get; set; }
    }

    public class Systems
    {
        [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
        public string odataid { get; set; }
    }

    public class Links
    {
        [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
        public Sessions Sessions { get; set; }
    }

    public class Sessions
    {
        [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
        public string odataid { get; set; }
    }

    public class Taskservice
    {
        [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
        public string odataid { get; set; }
    }

    public class Managers
    {
        [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
        public string odataid { get; set; }
    }

    public class Sessionservice
    {
        [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
        public string odataid { get; set; }
    }

    public class Accountservice
    {
        [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
        public string odataid { get; set; }
    }

    public class Messageregistry
    {
        [Newtonsoft.Json.JsonProperty(PropertyName = "@odata.id")]
        public string odataid { get; set; }
    }





}
