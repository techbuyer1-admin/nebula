﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nebula.Helpers;

namespace Nebula.CiscoServers
{
    public class CiscoServerInfo : HasPropertyChanged
    {


        public CiscoServerInfo()
        {

            //single collection properties
            //iDracInfo = new CiscoiDrac();

            //multi collection properties
            CiscoCPUCollection = new ObservableCollection<CiscoCPU>();
            CiscoFanCollection = new ObservableCollection<CiscoFans>();
            CiscoPowerCollection = new ObservableCollection<CiscoPower>();
            CiscoMemoryCollection = new ObservableCollection<CiscoMemory>();
            CiscoNetworkCollection = new ObservableCollection<CiscoNetwork>();
            CiscoStorageCollection = new ObservableCollection<CiscoStorage>();
            CiscoStorageSoftwareCollection = new ObservableCollection<CiscoStorageSoftware>();
            CiscoVirtualDiskCollection = new ObservableCollection<CiscoVirtualDisk>();
            CiscoPhysicalDiskCollection = new ObservableCollection<CiscoPhysicalDisk>();
            CiscoSystemBoardCollection = new ObservableCollection<CiscoSystemBoard>();
            CiscoBMCCollection = new ObservableCollection<CiscoBMC>();
            CiscoFirmwareCollection = new ObservableCollection<CiscoFirmware>();
        }

        private string _TestedBy;

        public string TestedBy
        {
            get { return _TestedBy; }
            set
            {
                _TestedBy = value;
                OnPropertyChanged("TestedBy");
            }
        }

        private string _SDCardInserted;

        public string SDCardInserted
        {
            get { return _SDCardInserted; }
            set
            {
                _SDCardInserted = value;
                OnPropertyChanged("SDCardInserted");
            }
        }




        //SD CARD Presence

        //FIRMWARE

        private string _BiosCurrentVersion;

        public string BiosCurrentVersion
        {
            get { return _BiosCurrentVersion; }
            set
            {
                _BiosCurrentVersion = value;
                OnPropertyChanged("BiosCurrentVersion");
            }
        }

        private string _BiosReleaseDate;

        public string BiosReleaseDate
        {
            get { return _BiosReleaseDate; }
            set
            {
                _BiosReleaseDate = value;
                OnPropertyChanged("BiosReleaseDate");
            }
        }

        private string _IMMCurrentVersion;

        public string IMMCurrentVersion
        {
            get { return _IMMCurrentVersion; }
            set
            {
                _IMMCurrentVersion = value;
                OnPropertyChanged("IMMCurrentVersion");
            }
        }


        private string _IMMReleaseDate;

        public string IMMReleaseDate
        {
            get { return _IMMReleaseDate; }
            set
            {
                _IMMReleaseDate = value;
                OnPropertyChanged("IMMReleaseDate");
            }
        }

        private string _DSACurrentVersion;

        public string DSACurrentVersion
        {
            get { return _DSACurrentVersion; }
            set
            {
                _DSACurrentVersion = value;
                OnPropertyChanged("DSACurrentVersion");
            }
        }

        private string _DSAReleaseDate;

        public string DSAReleaseDate
        {
            get { return _DSAReleaseDate; }
            set
            {
                _DSAReleaseDate = value;
                OnPropertyChanged("DSAReleaseDate");
            }
        }



        //SERVER GENERAL INFO

        private string _Manufacturer;

        public string Manufacturer
        {
            get { return _Manufacturer; }
            set
            {
                _Manufacturer = value;
                OnPropertyChanged("Manufacturer");
            }
        }

        private string _Model;

        public string Model
        {
            get { return _Model; }
            set
            {
                _Model = value;
                OnPropertyChanged("Model");
            }
        }

        private string _SystemSerialNumber;

        public string SystemSerialNumber
        {
            get { return _SystemSerialNumber; }
            set
            {
                _SystemSerialNumber = value;
                OnPropertyChanged("SystemSerialNumber");
            }
        }

        private string _BoardSerialNumber;

        public string BoardSerialNumber
        {
            get { return _BoardSerialNumber; }
            set
            {
                _BoardSerialNumber = value;
                OnPropertyChanged("BoardSerialNumber");
            }
        }

        private string _SystemUUID;

        public string SystemUUID
        {
            get { return _SystemUUID; }
            set
            {
                _SystemUUID = value;
                OnPropertyChanged("SystemUUID");
            }
        }


        private string _Version;

        public string Version
        {
            get { return _Version; }
            set
            {
                _Version = value;
                OnPropertyChanged("Version");
            }
        }



        private string _AmbientTemperature;

        public string AmbientTemperature
        {
            get { return _AmbientTemperature; }
            set
            {
                _AmbientTemperature = value;
                OnPropertyChanged("AmbientTemperature");
            }
        }

        // END SERVER GENERAL INFO


        //MEMORY PROPERTIES

        private string _TotalMemory;

        public string TotalMemory
        {
            get { return _TotalMemory; }
            set
            {
                _TotalMemory = value;
                OnPropertyChanged("TotalMemory");
            }
        }

        private string _MaxDIMMSlots;

        public string MaxDIMMSlots
        {
            get { return _MaxDIMMSlots; }
            set
            {
                _MaxDIMMSlots = value;
                OnPropertyChanged("MaxDIMMSlots");
            }
        }

        private string _PopulatedDIMMSlots;

        public string PopulatedDIMMSlots
        {
            get { return _PopulatedDIMMSlots; }
            set
            {
                _PopulatedDIMMSlots = value;
                OnPropertyChanged("PopulatedDIMMSlots");
            }
        }

        private string _SysMemErrorMethodology;

        public string SysMemErrorMethodology
        {
            get { return _SysMemErrorMethodology; }
            set
            {
                _SysMemErrorMethodology = value;
                OnPropertyChanged("SysMemErrorMethodology");
            }
        }


        private string _MemoryBlockSize;

        public string MemoryBlockSize
        {
            get { return _MemoryBlockSize; }
            set
            {
                _MemoryBlockSize = value;
                OnPropertyChanged("MemoryBlockSize");
            }
        }


        private string _MemoryOperationMode;

        public string MemoryOperationMode
        {
            get { return _MemoryOperationMode; }
            set
            {
                _MemoryOperationMode = value;
                OnPropertyChanged("MemorySlotsUsed");
            }
        }

        private string _SysMemMaxCapacitySize;

        public string SysMemMaxCapacitySize
        {
            get { return _SysMemMaxCapacitySize; }
            set
            {
                _SysMemMaxCapacitySize = value;
                OnPropertyChanged("SysMemMaxCapacitySize");
            }
        }



        //HEALTH AT A GLANCE
        private string _RollupStatus;

        public string RollupStatus
        {
            get { return _RollupStatus; }
            set
            {
                _RollupStatus = value;
                OnPropertyChanged("RollupStatus");
            }
        }

        private string _CurrentRollupStatus;

        public string CurrentRollupStatus
        {
            get { return _CurrentRollupStatus; }
            set
            {
                _CurrentRollupStatus = value;
                OnPropertyChanged("CurrentRollupStatus");
            }
        }


        private string _BatteryRollupStatus;

        public string BatteryRollupStatus
        {
            get { return _BatteryRollupStatus; }
            set
            {
                _BatteryRollupStatus = value;
                OnPropertyChanged("BatteryRollupStatus");
            }
        }


        private string _IntrusionRollupStatus;

        public string IntrusionRollupStatus
        {
            get { return _IntrusionRollupStatus; }
            set
            {
                _IntrusionRollupStatus = value;
                OnPropertyChanged("IntrusionRollupStatus");
            }
        }

        private string _FanRollupStatus;

        public string FanRollupStatus
        {
            get { return _FanRollupStatus; }
            set
            {
                _FanRollupStatus = value;
                OnPropertyChanged("FanRollupStatus");
            }
        }


        private string _PSRollupStatus;

        public string PSRollupStatus
        {
            get { return _PSRollupStatus; }
            set
            {
                _PSRollupStatus = value;
                OnPropertyChanged("PSRollupStatus");
            }
        }



        private string _MemoryRollupStatus;

        public string MemoryRollupStatus
        {
            get { return _MemoryRollupStatus; }
            set
            {
                _MemoryRollupStatus = value;
                OnPropertyChanged("MemoryRollupStatus");
            }
        }



        private string _CPURollupStatus;

        public string CPURollupStatus
        {
            get { return _CPURollupStatus; }
            set
            {
                _CPURollupStatus = value;
                OnPropertyChanged("CPURollupStatus");
            }
        }


        private string _TempRollupStatus;

        public string TempRollupStatus
        {
            get { return _TempRollupStatus; }
            set
            {
                _TempRollupStatus = value;
                OnPropertyChanged("TempRollupStatus");
            }
        }



        private string _VoltRollupStatus;

        public string VoltRollupStatus
        {
            get { return _VoltRollupStatus; }
            set
            {
                _VoltRollupStatus = value;
                OnPropertyChanged("VoltRollupStatus");
            }
        }



        private string _StorageRollupStatus;

        public string StorageRollupStatus
        {
            get { return _StorageRollupStatus; }
            set
            {
                _StorageRollupStatus = value;
                OnPropertyChanged("StorageRollupStatus");
            }
        }


        private string _SDCardRollupStatus;

        public string SDCardRollupStatus
        {
            get { return _SDCardRollupStatus; }
            set
            {
                _SDCardRollupStatus = value;
                OnPropertyChanged("SDCardRollupStatus");
            }
        }

        private string _LicensingRollupStatus;

        public string LicensingRollupStatus
        {
            get { return _LicensingRollupStatus; }
            set
            {
                _LicensingRollupStatus = value;
                OnPropertyChanged("LicensingRollupStatus");
            }
        }







        //CPU

        private string _CPUFamily;

        public string CPUFamily
        {
            get { return _CPUFamily; }
            set
            {
                _CPUFamily = value;
                OnPropertyChanged("CPUFamily");
            }
        }



        private string _CPUCount;

        public string CPUCount
        {
            get { return _CPUCount; }
            set
            {
                _CPUCount = value;
                OnPropertyChanged("CPUCount");
            }
        }


        private string _CurrentClockSpeed;

        public string CurrentClockSpeed
        {
            get { return _CurrentClockSpeed; }
            set
            {
                _CurrentClockSpeed = value;
                OnPropertyChanged("CurrentClockSpeed");
            }
        }

        private string _MaxClockSpeed;

        public string MaxClockSpeed
        {
            get { return _MaxClockSpeed; }
            set
            {
                _MaxClockSpeed = value;
                OnPropertyChanged("MaxClockSpeed");
            }
        }

        private string _CorePerCPU;

        public string CorePerCPU
        {
            get { return _CorePerCPU; }
            set
            {
                _CorePerCPU = value;
                OnPropertyChanged("CorePerCPU");
            }
        }


        private string _ThreadPerCore;

        public string ThreadPerCore
        {
            get { return _ThreadPerCore; }
            set
            {
                _ThreadPerCore = value;
                OnPropertyChanged("ThreadPerCore");
            }
        }


        private string _Voltage;

        public string Voltage
        {
            get { return _Voltage; }
            set
            {
                _Voltage = value;
                OnPropertyChanged("Voltage");
            }
        }


        //POWER

        private string _PowerState;

        public string PowerState
        {
            get { return _PowerState; }
            set
            {
                _PowerState = value;
                OnPropertyChanged("PowerState");
            }
        }





        //TO Hold data as string Values


        //FANS
        private string _FansInventory;

        public string FansInventory
        {
            get { return _FansInventory; }
            set
            {
                _FansInventory = value;
                OnPropertyChanged("FansInventory");
            }
        }
        //CPU
        private string _CPUInventory;

        public string CPUInventory
        {
            get { return _CPUInventory; }
            set
            {
                _CPUInventory = value;
                OnPropertyChanged("CPUInventory");
            }
        }
        //MEMORY
        private string _MemoryInventory;

        public string MemoryInventory
        {
            get { return _MemoryInventory; }
            set
            {
                _MemoryInventory = value;
                OnPropertyChanged("MemoryInventory");
            }
        }

        //NETWORK
        private string _NetworkInventory;

        public string NetworkInventory
        {
            get { return _NetworkInventory; }
            set
            {
                _NetworkInventory = value;
                OnPropertyChanged("NetworkInventory");
            }
        }

        //STORAGE
        private string _StorageInventory;

        public string StorageInventory
        {
            get { return _StorageInventory; }
            set
            {
                _StorageInventory = value;
                OnPropertyChanged("StorageInventory");
            }
        }

        //VIRTUAL DISK
        private string _VirtualDiskInventory;

        public string VirtualDiskInventory
        {
            get { return _VirtualDiskInventory; }
            set
            {
                _VirtualDiskInventory = value;
                OnPropertyChanged("VirtualDiskInventory");
            }
        }

        //PHYSICAL DISK
        private string _PhysicalDiskInventory;

        public string PhysicalDiskInventory
        {
            get { return _PhysicalDiskInventory; }
            set
            {
                _PhysicalDiskInventory = value;
                OnPropertyChanged("PhysicalDiskInventory");
            }
        }

        //POWER
        private string _PowerInventory;

        public string PowerInventory
        {
            get { return _PowerInventory; }
            set
            {
                _PowerInventory = value;
                OnPropertyChanged("PowerInventory");
            }
        }






        //public ObservableCollection<CiscoSystem> CiscoSystemCollection { get; set; }
        //public ObservableCollection<CiscoiDrac> CiscoiDracCollection { get; set; }
        public ObservableCollection<CiscoCPU> CiscoCPUCollection { get; set; }
        public ObservableCollection<CiscoFans> CiscoFanCollection { get; set; }
        public ObservableCollection<CiscoPower> CiscoPowerCollection { get; set; }
        public ObservableCollection<CiscoMemory> CiscoMemoryCollection { get; set; }
        public ObservableCollection<CiscoNetwork> CiscoNetworkCollection { get; set; }
        public ObservableCollection<CiscoStorage> CiscoStorageCollection { get; set; }
        public ObservableCollection<CiscoStorageSoftware> CiscoStorageSoftwareCollection { get; set; }
        public ObservableCollection<CiscoVirtualDisk> CiscoVirtualDiskCollection { get; set; }
        public ObservableCollection<CiscoPhysicalDisk> CiscoPhysicalDiskCollection { get; set; }
        public ObservableCollection<CiscoBMC> CiscoBMCCollection { get; set; }
        public ObservableCollection<CiscoSystemBoard> CiscoSystemBoardCollection { get; set; }
        public ObservableCollection<CiscoFirmware> CiscoFirmwareCollection { get; set; }


    }



    public class CiscoCPU : HasPropertyChanged
    {

        public CiscoCPU()
        {

        }

        public CiscoCPU(string model, string manufacturer, string cpuFamily, string deviceDescription, string instanceID, string cache1Location, string cache2Location, string cache3Location, string cache3Associativity, string cache2Associativity, string cache1Associativity,
            string cache3Type, string cache2Type, string cache1Type, string cache3ErrorMethodology, string cache2ErrorMethodology, string cache1ErrorMethodology, string cache3SRAMType, string cache2SRAMType, string cache1SRAMType, string cache3WritePolicy, string cache2WritePolicy, string cache1WritePolicy,
            string cache3Level, string cache2Level, string cache1Level, string cache3PrimaryStatus, string cache2PrimaryStatus, string cache1PrimaryStatus, string cache3InstalledSize, string cache2InstalledSize, string cache1InstalledSize, string cache3Size, string cache2Size, string cache1Size,
            string turboModeCapable, string turboModeEnabled, string executeDisabledCapable, string executeDisabledEnabled, string virtualizationTechnologyCapable, string virtualizationTechnologyEnabled, string hyperThreadingCapable, string hyperThreadingEnabled, string characteristics, string cpuStatus,
            string voltage, string numberOfProcessorCores, string numberOfEnabledThreads, string numberOfEnabledCores, string primaryStatus, string externalBusClockSpeed, string maxClockSpeed, string currentClockSpeed)
        {




        }


        public string CPUCount { get; set; }
        public string Name { get; set; }
        public string CPUFamily { get; set; }
        public string Model { get; set; }
        public string HealthState { get; set; }
        public string CorePerCPU { get; set; }
        public string ThreadPerCore { get; set; }
        public string CurrentFrequency { get; set; }
        public string MaxTurboFrequency { get; set; }
        public string L1Cache { get; set; }
        public string L2Cache { get; set; }
        public string L3Cache { get; set; }

    }

    public class CiscoMemory : HasPropertyChanged
    {

        public CiscoMemory()
        {

        }
        public CiscoMemory(string rank, string primaryStatus, string manufactureDate, string model, string partNumber, string serialNumber, string manufacturer,
            string bankLabel, string size, string currentOperatingSpeed, string speed, string memoryType, string deviceDescription, string instanceID)
        {
            //Rank = rank;
            //PrimaryStatus = primaryStatus;
            //ManufactureDate = manufactureDate;
            //Model = model;
            //PartNumber = partNumber;
            //SerialNumber = serialNumber;
            //Manufacturer = manufacturer;
            //BankLabel = bankLabel;
            //Size = size;
            //CurrentOperatingSpeed = currentOperatingSpeed;
            //Speed = speed;
            //MemoryType = memoryType;
            //DeviceDescription = deviceDescription;
            //InstanceID = instanceID;


        }


        public string BankLabel { get; set; }
        public string ElementName { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string PartNumber { get; set; }
        public string SerialNumber { get; set; }
        public string MaxMemorySpeed { get; set; }
        public string Capacity { get; set; }
        public string FRUPartNumber { get; set; }



    }



    public class CiscoSystemBoard : HasPropertyChanged
    {

        public CiscoSystemBoard()
        {

        }
        public CiscoSystemBoard(string elementname, string manufacturer, string model, string partnumber, string frunumber, string serialnumber)
        {

            ElementName = elementname;
            Manufacturer = manufacturer;
            Model = model;
            PartNumber = partnumber;
            FRUNumber = frunumber;
            SerialNumber = serialnumber;
        }

        //Element Name
        private string _ElementName;

        public string ElementName
        {
            get { return _ElementName; }
            set
            {
                _ElementName = value;
                OnPropertyChanged("ElementName");
            }
        }

        //Manufacturer
        private string _Manufacturer;

        public string Manufacturer
        {
            get { return _Manufacturer; }
            set
            {
                _Manufacturer = value;
                OnPropertyChanged("Manufacturer");
            }
        }


        //Model
        private string _Model;

        public string Model
        {
            get { return _Model; }
            set
            {
                _Model = value;
                OnPropertyChanged("Model");
            }
        }

        //PartNumber
        private string _PartNumber;

        public string PartNumber
        {
            get { return _PartNumber; }
            set
            {
                _PartNumber = value;
                OnPropertyChanged("PartNumber");
            }
        }



        //FRUNumber
        private string _FRUNumber;

        public string FRUNumber
        {
            get { return _FRUNumber; }
            set
            {
                _FRUNumber = value;
                OnPropertyChanged("FRUNumber");
            }
        }



        //Manufacturer
        private string _SerialNumber;

        public string SerialNumber
        {
            get { return _SerialNumber; }
            set
            {
                _SerialNumber = value;
                OnPropertyChanged("SerialNumber");
            }
        }
    }




    public class CiscoBMC : HasPropertyChanged
    {
        public CiscoBMC()
        {

        }


        //ElementName
        private string _ElementName;

        public string ElementName
        {
            get { return _ElementName; }
            set
            {
                _ElementName = value;
                OnPropertyChanged("ElementName");
            }
        }

        //CurrentReading
        private string _CurrentReading;

        public string CurrentReading
        {
            get { return _CurrentReading; }
            set
            {
                _CurrentReading = value;
                OnPropertyChanged("CurrentReading");
            }
        }

        //BaseUnit
        private string _BaseUnit;

        public string BaseUnit
        {
            get { return _BaseUnit; }
            set
            {
                _BaseUnit = value;
                OnPropertyChanged("BaseUnit");
            }
        }


    }





    public class CiscoControllerBattery : HasPropertyChanged
    {
        public CiscoControllerBattery(string raidState, string primaryStatus, string deviceDescription, string instanceID)
        {
            RAIDState = raidState;
            PrimaryStatus = primaryStatus;
            DeviceDescription = deviceDescription;
            InstanceID = instanceID;


        }



        public string RAIDState { get; set; }
        public string PrimaryStatus { get; set; }
        public string DeviceDescription { get; set; }
        public string InstanceID { get; set; }


    }




    public class CiscoPCI : HasPropertyChanged
    {
        public CiscoPCI()
        {

        }
        public CiscoPCI(string slotType, string slotLength, string dataBusWidth, string description, string manufacturer, string pciSubDeviceID, string pciSubVendorID, string pciDeviceID, string pciVendorID, string functionNumber,
            string deviceNumber, string busNumber, string deviceDescription, string instanceID)
        {
            //SlotType = slotType;
            //SlotLength = slotLength;
            //DataBusWidth = dataBusWidth;
            //Description = description;
            //Manufacturer = manufacturer;
            //PCISubDeviceID = pciSubDeviceID;
            //PCISubVendorID = pciSubVendorID;
            //PCIDeviceID = pciDeviceID;
            //PCIVendorID = pciVendorID;
            //FunctionNumber = functionNumber;
            //DeviceNumber = deviceNumber;
            //BusNumber = busNumber;
            //DeviceDescription = deviceDescription;
            //InstanceID = instanceID;


        }




        public string Name { get; set; }
        public string Tag { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public string PartNumber { get; set; }
        public string FRUNumber { get; set; }
        public string OperationalStatus { get; set; }


    }


    public class CiscoNetwork : HasPropertyChanged
    {
        public CiscoNetwork()
        {

        }
        public CiscoNetwork(string slotType, string slotLength, string dataBusWidth, string description, string manufacturer, string pciSubDeviceID, string pciSubVendorID, string pciDeviceID, string pciVendorID, string functionNumber,
            string deviceNumber, string busNumber, string deviceDescription, string instanceID)
        {
            //SlotType = slotType;
            //SlotLength = slotLength;
            //DataBusWidth = dataBusWidth;
            //Description = description;
            //Manufacturer = manufacturer;
            //PCISubDeviceID = pciSubDeviceID;
            //PCISubVendorID = pciSubVendorID;
            //PCIDeviceID = pciDeviceID;
            //PCIVendorID = pciVendorID;
            //FunctionNumber = functionNumber;
            //DeviceNumber = deviceNumber;
            //BusNumber = busNumber;
            //DeviceDescription = deviceDescription;
            //InstanceID = instanceID;


        }




        public string Name { get; set; }
        public string LinkTechnology { get; set; }
        public string MaxSpeed { get; set; }
        public string EnabledState { get; set; }
        public string PortAvailability { get; set; }
        public string RequestedState { get; set; }



    }


    public class CiscoStorage : HasPropertyChanged
    {

        public CiscoStorage()
        {

        }

        public CiscoStorage(string realtimeCapability, string supportControllerBootMode, string supportEnhancedAutoForeignImport, string maxAvailablePCILinkSpeed, string maxPossiblePCILinkSpeed, string patrolReadState, string driverVersion,
            string cacheSizeInMB, string supportRAID10UnevenSpans, string t10PICapability, string slicedVDCapability, string cachecadeCapability, string encryptionCapability, string encryptionMode, string securityStatus, string sasAddress,
            string productName, string deviceCardSlotType, string deviceCardSlotLength, string deviceCardDataBusWidth, string deviceCardManufacturer, string pciSubDeviceID, string pciDeviceID, string pciSubVendorID, string pciVendorID,
            string function, string device, string bus, string controllerFirmwareVersion, string persistentHotspare, string pciSlot, string rollupStatus, string primaryStatus, string deviceDescription, string instanceID)
        {



        }



        public string Name { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public string Manufacturer { get; set; }
        public string PartNumber { get; set; }
        public string FRUNumber { get; set; }
        public string UUID { get; set; }
        public string CacheStatus { get; set; }
        public string CacheSerialNumber { get; set; }
        public string CacheMemorySize { get; set; }



    }

    public class CiscoStorageSoftware : HasPropertyChanged
    {

        public CiscoStorageSoftware()
        {

        }

        public CiscoStorageSoftware(string realtimeCapability, string supportControllerBootMode, string supportEnhancedAutoForeignImport, string maxAvailablePCILinkSpeed, string maxPossiblePCILinkSpeed, string patrolReadState, string driverVersion,
            string cacheSizeInMB, string supportRAID10UnevenSpans, string t10PICapability, string slicedVDCapability, string cachecadeCapability, string encryptionCapability, string encryptionMode, string securityStatus, string sasAddress,
            string productName, string deviceCardSlotType, string deviceCardSlotLength, string deviceCardDataBusWidth, string deviceCardManufacturer, string pciSubDeviceID, string pciDeviceID, string pciSubVendorID, string pciVendorID,
            string function, string device, string bus, string controllerFirmwareVersion, string persistentHotspare, string pciSlot, string rollupStatus, string primaryStatus, string deviceDescription, string instanceID)
        {



        }



        public string ElementName { get; set; }
        public string ProductName { get; set; }
        public string Manufacturer { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
        public string Classifications { get; set; }
        public string ClassificationsDescriptions { get; set; }
        public string IdentityInfoValue { get; set; }
        public string IdentityInfoType { get; set; }
        public string SoftwareID { get; set; }
        public string SubDeviceID { get; set; }
        public string ReleaseDate { get; set; }
        public string SoftwareStatus { get; set; }
        public string SoftwareRole { get; set; }



    }

    public class CiscoFirmware : HasPropertyChanged
    {
        public CiscoFirmware()
        {

        }
        public CiscoFirmware(string slotType, string slotLength, string dataBusWidth, string description, string manufacturer, string pciSubDeviceID, string pciSubVendorID, string pciDeviceID, string pciVendorID, string functionNumber,
            string deviceNumber, string busNumber, string deviceDescription, string instanceID)
        {
            //SlotType = slotType;
            //SlotLength = slotLength;
            //DataBusWidth = dataBusWidth;
            //Description = description;
            //Manufacturer = manufacturer;
            //PCISubDeviceID = pciSubDeviceID;
            //PCISubVendorID = pciSubVendorID;
            //PCIDeviceID = pciDeviceID;
            //PCIVendorID = pciVendorID;
            //FunctionNumber = functionNumber;
            //DeviceNumber = deviceNumber;
            //BusNumber = busNumber;
            //DeviceDescription = deviceDescription;
            //InstanceID = instanceID;


        }




        public string Name { get; set; }
        public string BuildID { get; set; }
        public string BuildIDBackup { get; set; }
        public string BuildDate { get; set; }
        public string BuildDateBackup { get; set; }


    }

    public class CiscoVirtualDisk : HasPropertyChanged
    {

        public CiscoVirtualDisk()
        {

        }

        public CiscoVirtualDisk(string deviceType, string blockSizeInBytes, string busProtocol, string cachecade, string diskCachePolicy, string deviceDescription, string fqdd, string instanceID,
            string lastSystemInventoryTime, string lastUpdateTime, string lockStatus, string mediaType, string name, string objectStatus, string operationName, string operationPercentComplete,
            string primaryStatus, string raidStatus, string raidTypes, string readCachePolicy, string remainingRedundancy, string rollupStatus, string sizeInBytes, string spanDepth,
            string spanLength, string startingLBAinBlocks, string stripeSize, string t10PIStatus, string virtualDiskTargetID, string writeCachePolicy)
        {


        }



        public string DeviceType { get; set; }
        public string BlockSizeInBytes { get; set; }
        public string BusProtocol { get; set; }
        public string Cachecade { get; set; }
        public string DeviceDescription { get; set; }
        public string DiskCachePolicy { get; set; }
        public string FQDD { get; set; }
        public string InstanceID { get; set; }
        public string LastSystemInventoryTime { get; set; }
        public string LastUpdateTime { get; set; }
        public string LockStatus { get; set; }
        public string MediaType { get; set; }
        public string Name { get; set; }
        public string ObjectStatus { get; set; }
        public string OperationName { get; set; }
        public string OperationPercentComplete { get; set; }
        public string PrimaryStatus { get; set; }
        public string RAIDStatus { get; set; }
        public string RAIDTypes { get; set; }
        public string ReadCachePolicy { get; set; }
        public string RemainingRedundancy { get; set; }
        public string RollupStatus { get; set; }
        public string SizeInBytes { get; set; }
        public string SpanDepth { get; set; }
        public string SpanLength { get; set; }
        public string StartingLBAinBlocks { get; set; }
        public string StripeSize { get; set; }
        public string T10PIStatus { get; set; }
        public string VirtualDiskTargetID { get; set; }
        public string WriteCachePolicy { get; set; }


    }


    public class CiscoPhysicalDisk : HasPropertyChanged
    {

        public CiscoPhysicalDisk()
        {

        }

        public CiscoPhysicalDisk(string deviceType, string blockSizeInBytes, string busProtocol, string configLockdownCapable, string configLockdownState, string connector, string deviceDescription,
            string driveFormFactor, string fqdd, string freeSizeInBytes, string hotSpareStatus, string instanceID, string lastSystemInventoryTime, string lastUpdateTime, string manufacturer, string manufacturingDay,
            string manufacturingWeek, string manufacturingYear, string maxCapableSpeed, string mediaType, string model, string operationName, string operationPercentComplete, string ppid, string predictiveFailureState,
            string primaryStatus, string raidType, string raidStatus, string remainingRatedWriteEndurance, string revision, string rollupStatus, string securityState, string serialNumber, string sizeInBytes,
            string slot, string systemEraseCapability, string t10PICapability, string updateLockdownCapable, string updateLockdownState, string usedSizeInBytes)
        {


        }



        public string DeviceType { get; set; }
        public string BlockSizeInBytes { get; set; }
        public string BusProtocol { get; set; }
        public string ConfigLockdownCapable { get; set; }
        public string ConfigLockdownState { get; set; }
        public string Connector { get; set; }
        public string DeviceDescription { get; set; }
        public string DriveFormFactor { get; set; }
        public string FQDD { get; set; }
        public string FreeSizeInBytes { get; set; }
        public string HotSpareStatus { get; set; }
        public string InstanceID { get; set; }
        public string LastSystemInventoryTime { get; set; }
        public string LastUpdateTime { get; set; }
        public string Manufacturer { get; set; }
        public string ManufacturingDay { get; set; }
        public string ManufacturingWeek { get; set; }
        public string ManufacturingYear { get; set; }
        public string MaxCapableSpeed { get; set; }
        public string MediaType { get; set; }
        public string Model { get; set; }
        public string OperationName { get; set; }
        public string OperationPercentComplete { get; set; }
        public string PPID { get; set; }
        public string PredictiveFailureState { get; set; }
        public string PrimaryStatus { get; set; }
        public string RAIDType { get; set; }
        public string RaidStatus { get; set; }
        public string RemainingRatedWriteEndurance { get; set; }
        public string Revision { get; set; }
        public string RollupStatus { get; set; }
        public string SASAddress { get; set; }
        public string SecurityState { get; set; }
        public string SerialNumber { get; set; }
        public string SizeInBytes { get; set; }
        public string Slot { get; set; }
        public string SystemEraseCapability { get; set; }
        public string T10PICapability { get; set; }
        public string UpdateLockdownCapable { get; set; }
        public string UpdateLockdownState { get; set; }
        public string UsedSizeInBytes { get; set; }


    }




    public class CiscoFans : HasPropertyChanged
    {

        public CiscoFans()
        {

        }


        public CiscoFans(string redundancyStatus, string activeCooling, string variableSpeed, string pwm, string currentReading, string rateUnits, string unitModifier,
            string baseUnits, string primaryStatus, string deviceDescription, string instanceID)
        {



        }



        public string ElementName { get; set; }
        public string HealthState { get; set; }
        public string CurrentSpeed { get; set; }

    }




    public class CiscoPower : HasPropertyChanged
    {
        public CiscoPower()
        {

        }
        public CiscoPower(string pmbbusonitoring, string range1MaxInputPower, string redMinNumberNeeded, string redundancyStatus, string type, string primaryStatus, string totalOutputPower,
            string inputVoltage, string firmwareVersion, string manufacturer, string partNumber, string serialNumber, string model, string deviceDescription, string instanceID)
        {

        }




        public string ElementName { get; set; }
        public string HealthState { get; set; }
        public string TotalOutputPower { get; set; }



    }



}
