﻿using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Nebula.Commands;
using Nebula.Commands.XTerminal;
using Nebula.Models;
using Nebula.Helpers;
using Nebula.Views;
using MahApps.Metro.Controls;
using System.Windows.Documents;
using System.Windows;
using System.Collections.ObjectModel;
using System.Threading;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Sockets;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Windows.Media;
using System.Runtime.InteropServices;
using System.Windows.Input;
using Microsoft.VisualBasic.FileIO;
using Microsoft.Office.Interop.Excel;

namespace Nebula.ViewModels
{
    [Serializable]
    public class DriveTestViewModel : HasPropertyChanged
    {
        public string dsktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);   //@"" + dsktop + @"
        public string recent = Environment.GetFolderPath(Environment.SpecialFolder.Recent);   //@"" + recent + @"
        public string downld = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Downloads";   //@"" + recent + @"
        public string myDocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);   //@"" + myDocs + @"

        // Variable for MAHAPPS DIALOGS
        private IDialogCoordinator dialogCoordinator;



        //Constructors
        public DriveTestViewModel()
        {

            //USER VALUES CAPTURE
            //Get Current Windows User and remove domain prefix  Strip out the surname leaving only initials and capitalise
            //Check if returned username has 4 characters, if not set to 3
            int usernameCount = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Count();
            int subLength = 4;

            if (usernameCount < 4)
            {
                subLength = 3;
            }
            else
            {
                subLength = 4;
            }

            CurrentUser = "Technician Name: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, subLength).Replace(".", "").ToUpper() + " " + DateTime.Now.ToShortDateString();
            //Report USer
            ReportUser = "Technician Name: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, subLength).ToUpper() + " " + DateTime.Now.ToShortDateString();
            //Get Current Windows User and remove domain prefix  Strip out the surname leaving only initials and capitalise
            FullUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").ToUpperUserFullName();

            //Set Fulluser to Current
            CurrentUserInitials = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, subLength).ToUpper();

            //Get Region
            CurrentUserLocation = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            //Assign to Static Var
            StaticFunctions.UserLocation = CurrentUserLocation;


            switch (StaticFunctions.UserLocation)
            {
                case "Australia":
                    if (Directory.Exists(@"W:\AU Warehouse"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\AU Warehouse\";
                    }
                    else if(Directory.Exists(@"W:\"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\";
                    }

                    break;
                case "United States":
                    if (Directory.Exists(@"T:\NJ Warehouse\Technical Resources"))
                    {
                        StaticFunctions.UncPathToUse = @"T:\NJ Warehouse\Technical Resources\";
                    }
                    else if (Directory.Exists(@"T:\Technical Resources"))
                    {
                        StaticFunctions.UncPathToUse = @"T:\Technical Resources\";
                    }

                    break;
                case "United Kingdom":
                    // File Server DFS Swap Over
                    if (Directory.Exists(@"W:\UK Warehouse\Warehouse General"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\UK Warehouse\Warehouse General\";
                    }
                    else if (Directory.Exists(@"W:\Warehouse General"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\Warehouse General\";
                    }

                    break;
                case "France":
                    StaticFunctions.UncPathToUse = @"W:\FR Warehouse\Warehouse General\";
                    break;
                case "Germany":
                    StaticFunctions.UncPathToUse = @"W:\Warehouse General\";
                    break;

            }


            //Create the base collection NetsuiteOriginalImportCollection
            NetsuiteOriginalImportCollection = new ObservableCollection<NetsuiteInventory>();
            NetsuiteImportCollection = new ObservableCollection<NetsuiteInventory>();
            NetsuiteComparisonCollection = new ObservableCollection<NetsuiteInventory>();
            NetsuiteScannedCollection = new ObservableCollection<NetsuiteInventory>();
            SearchCollection = new ObservableCollection<NetsuiteInventory>();
            TestedDriveCollection = new ObservableCollection<NetsuiteInventory>();
            SerialsTotalsCollection = new ObservableCollection<NetsuiteInventory>();
            ReportCollection = new ObservableCollection<NetsuiteInventory>();
            ImportSerialsCollection = new ObservableCollection<ImportSerials>();

            //Declare collection for Discrepancy Report
            NetsuiteDiscrepancyCollection = new ObservableCollection<NetsuiteDiscrepancyReport>();

            //Add Selected items properties
            ImportLVSelectedItem = new NetsuiteInventory();
            ProcessedLVSelectedItem = new NetsuiteInventory();

            //Declare Commands
            _DPCrudCommand = new DPCrudCommand(this);
            _DPDBCrudCommand = new DPDBCrudCommand(this);
            _DPVariousCompleteCommand = new DPVariousCompleteCommand(this);
            _DPCheckPartCodeCommand = new DPCheckPartCodeCommand(this);
            _DPImportCSVCommand = new DPImportCSVCommand(this);
            _DPRemoveLVItemsCommand = new DPRemoveLVItemsCommand(this);
            _DPDeleteSelectedItemCommand = new DPDeleteSelectedItemCommand(this);
            _DPScanSerialsCommand = new DPScanSerialsCommand(this);
            _DPQuantityCommand = new DPQuantityCommand(this);
            _DPExtraItemsCommand = new DPExtraItemsCommand(this);
            _DPExportCSVCommand = new DPExportCSVCommand(this);
            _DPCapsLockONCommand = new DPCapsLockONCommand(this);
            _DPSelectFilePathCommand = new DPSelectFilePathCommand(this);
            _DPLaunchFileCommand = new DPLaunchFileCommand(this);
            _DPEmailDocumentsCommand = new DPEmailDocumentsCommand(this);

            _PrintSaveFlowDocumentDTCommand = new PrintSaveFlowDocumentDTCommand(this);
            _PrintSaveFlowDocumentREPCommand = new PrintSaveFlowDocumentREPCommand(this);

            //Text Suggestion List to load into Auto Textbox, used with server views
            TextSuggestion = new ObservableCollection<string>();


            //Set Date Range
            DateFrom = DateTime.Today;
            DateTo = DateTime.Today;

            SerialsCount = 0;
            TestCount = "0";
            SearchCount = "0";
            SerialTBEnabled = false;
            //Enable Import List
            ImportListEnabled = true;

            //FocusOnSerialTXT = false;
            //FocusOnProductCodeTXT = false;
            //FocusOnPONumberTXT = false;
            //Set Caps lock on for Text Box
            DPCapsLockONCommand.Execute("");

            //Refresh the netsuite data
            RefreshNSData();

            PONumber = "";
            POQuantity = "";
            PartCode = "";
            PartDescription = "";

            //Progress ring on test
            ProgressIsActive = false;

            FocusOnPONumberTXT = true;
            //to hold product codes
            ProductCodes = new List<string>();

            //Set Span Days

            SpanDays = "30";


            // Load the Product Codes only if inventory file exists
            if (File.Exists(@"" + myDocs + @"\Inventory.csv"))
            {
                //Will Create a new file for each year
                string newFileName = @"" + myDocs + @"\Inventory.csv";
                ProductCodes.Clear();
                //Uses a VB class to process the file instead of streamreader
                using (TextFieldParser parser = new TextFieldParser(newFileName))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        string currentLine = parser.ReadLine();

                        //Add 
                        //PartCodeSearch.AutoSuggestionList.Add(FromCsv(currentLine));
                        ProductCodes.Add(FromCsv(currentLine));

                    }
                }
            }

        }

        public DriveTestViewModel(IDialogCoordinator instance)
        {

            //Dialog
            dialogCoordinator = instance;

            //USER VALUES CAPTURE
            //Get Current Windows User and remove domain prefix  Strip out the surname leaving only initials and capitalise
            //Check if returned username has 4 characters, if not set to 3
            int usernameCount = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Count();
            int subLength = 4;

            if (usernameCount < 4)
            {
                subLength = 3;
            }
            else
            {
                subLength = 4;
            }

            CurrentUser = "Technician Name: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, subLength).Replace(".", "").ToUpper() + " " + DateTime.Now.ToShortDateString();
            //Report USer
            ReportUser = "Technician Name: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, subLength).ToUpper() + " " + DateTime.Now.ToShortDateString();
            //Get Current Windows User and remove domain prefix  Strip out the surname leaving only initials and capitalise
            FullUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").ToUpperUserFullName();

            //Set Fulluser to Current
            CurrentUserInitials = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, subLength).ToUpper();

            //Get Region
            CurrentUserLocation = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            //Assign to Static Var
            StaticFunctions.UserLocation = CurrentUserLocation;


            switch (StaticFunctions.UserLocation)
            {
                case "Australia":
                    if (Directory.Exists(@"W:\AU Warehouse"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\AU Warehouse\";
                    }
                    else if(Directory.Exists(@"W:\"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\";
                    }

                    break;
                case "United States":
                    if (Directory.Exists(@"T:\NJ Warehouse\Technical Resources"))
                    {
                        StaticFunctions.UncPathToUse = @"T:\NJ Warehouse\Technical Resources\";
                    }
                    else if (Directory.Exists(@"T:\Technical Resources"))
                    {
                        StaticFunctions.UncPathToUse = @"T:\Technical Resources\";
                    }

                    break;
                case "United Kingdom":
                    // File Server DFS Swap Over
                    if (Directory.Exists(@"W:\UK Warehouse\Warehouse General"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\UK Warehouse\Warehouse General\";
                    }
                    else if (Directory.Exists(@"W:\Warehouse General"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\Warehouse General\";
                    }
                    break;
                case "France":
                    StaticFunctions.UncPathToUse = @"W:\FR Warehouse\Warehouse General\";
                    break;
                case "Germany":
                    StaticFunctions.UncPathToUse = @"W:\Warehouse General\";
                    break;

            }


            //Create the base collection NetsuiteOriginalImportCollection
            NetsuiteOriginalImportCollection = new ObservableCollection<NetsuiteInventory>();
            NetsuiteImportCollection = new ObservableCollection<NetsuiteInventory>();
            NetsuiteComparisonCollection = new ObservableCollection<NetsuiteInventory>();
            NetsuiteScannedCollection = new ObservableCollection<NetsuiteInventory>();
            SearchCollection = new ObservableCollection<NetsuiteInventory>();
            TestedDriveCollection = new ObservableCollection<NetsuiteInventory>();
            SerialsTotalsCollection = new ObservableCollection<NetsuiteInventory>();
            ReportCollection = new ObservableCollection<NetsuiteInventory>();
            ImportSerialsCollection = new ObservableCollection<ImportSerials>();

            //Declare collection for Discrepancy Report
            NetsuiteDiscrepancyCollection = new ObservableCollection<NetsuiteDiscrepancyReport>();

            //Add Selected items properties
            ImportLVSelectedItem = new NetsuiteInventory();
            ProcessedLVSelectedItem = new NetsuiteInventory();

            //Declare Commands
            _DPCrudCommand = new DPCrudCommand(this);
            _DPDBCrudCommand = new DPDBCrudCommand(this);
            _DPVariousCompleteCommand = new DPVariousCompleteCommand(this);
            _DPCheckPartCodeCommand = new DPCheckPartCodeCommand(this);
            _DPImportCSVCommand = new DPImportCSVCommand(this);
            _DPRemoveLVItemsCommand = new DPRemoveLVItemsCommand(this);
            _DPDeleteSelectedItemCommand = new DPDeleteSelectedItemCommand(this);
            _DPScanSerialsCommand = new DPScanSerialsCommand(this);
            _DPQuantityCommand = new DPQuantityCommand(this);
            _DPExtraItemsCommand = new DPExtraItemsCommand(this);
            _DPExportCSVCommand = new DPExportCSVCommand(this);
            _DPCapsLockONCommand = new DPCapsLockONCommand(this);
            _DPSelectFilePathCommand = new DPSelectFilePathCommand(this);
            _DPLaunchFileCommand = new DPLaunchFileCommand(this);
            _DPEmailDocumentsCommand = new DPEmailDocumentsCommand(this);

            _PrintSaveFlowDocumentDTCommand = new PrintSaveFlowDocumentDTCommand(this);
            _PrintSaveFlowDocumentREPCommand = new PrintSaveFlowDocumentREPCommand(this);

            //Text Suggestion List to load into Auto Textbox, used with server views
            TextSuggestion = new ObservableCollection<string>();


            //Set Date Range
            DateFrom = DateTime.Today;
            DateTo = DateTime.Today;

            SerialsCount = 0;
            TestCount = "0";
            SearchCount = "0";
            SerialTBEnabled = false;
            //Enable Import List
            ImportListEnabled = true;

            //FocusOnSerialTXT = false;
            //FocusOnProductCodeTXT = false;
            //FocusOnPONumberTXT = false;
            //Set Caps lock on for Text Box
            DPCapsLockONCommand.Execute("");

            //Refresh the netsuite data
            RefreshNSData();

            PONumber = "";
            POQuantity = "";
            PartCode = "";
            PartDescription = "";

            //Progress ring on test
            ProgressIsActive = false;

            FocusOnPONumberTXT = true;
            //to hold product codes
            ProductCodes = new List<string>();

            //Set Span Days

            SpanDays = "30";


            // Load the Product Codes only if inventory file exists
            if (File.Exists(@"" + myDocs + @"\Inventory.csv"))
            {
                //Will Create a new file for each year
                string newFileName = @"" + myDocs + @"\Inventory.csv";
                ProductCodes.Clear();
                //Uses a VB class to process the file instead of streamreader
                using (TextFieldParser parser = new TextFieldParser(newFileName))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        string currentLine = parser.ReadLine();

                        //Add 
                        //PartCodeSearch.AutoSuggestionList.Add(FromCsv(currentLine));
                        ProductCodes.Add(FromCsv(currentLine));

                    }
                }
            }
        }

        private void StartUp()
        {
           
        }


        //MAHAPPS DIALOG

        public async void CDROMWarning(string message)
        {
            try
            {
                await dialogCoordinator.ShowMessageAsync(this, "Server Notification", message);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public async void SystemMessageDialog(string systemref, string message)
        {
            try
            {

                var window = App.Current.Windows.OfType<MetroWindow>().FirstOrDefault(x => x.IsActive);
                if (window != null)
                {
                    await window.ShowMessageAsync(systemref, message, MessageDialogStyle.Affirmative);
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public async Task<MessageDialogResult> SystemMessageDialogYesNoCancel(string systemref, string message, string dialogStyle)
        {
            MessageDialogResult result = new MessageDialogResult();

            try
            {
                //Usage Example
                //vm.SystemMessageDialog("Nebula Notification", "Please enter a full search entry", "Standard");
                //vm.SystemMessageDialog("Nebula Notification", "Please enter a full search entry", YesNo");
                //Enum values
                //Canceled = -1,
                //Negative = 0,
                //Affirmative = 1,
                //FirstAuxiliary = 2,
                //SecondAuxiliary = 3

                var window = App.Current.Windows.OfType<MetroWindow>().FirstOrDefault(x => x.IsActive);
                if (window != null)
                {

                    switch (dialogStyle)
                    {
                        case "Standard":
                            result = await window.ShowMessageAsync(systemref, message, MessageDialogStyle.Affirmative);
                            break;
                        case "YesNo":
                            result = await window.ShowMessageAsync(systemref, message, MessageDialogStyle.AffirmativeAndNegative);
                            break;
                        case "YesNoCancel":
                            result = await window.ShowMessageAsync(systemref, message, MessageDialogStyle.AffirmativeAndNegativeAndDoubleAuxiliary);
                            break;
                    }


                    return result;
                }
                else
                {
                    return result;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

                return result;
            }
        }


        //Date Range From
        private DateTime _DateFrom;

        public DateTime DateFrom
        {
            get { return _DateFrom; }
            set
            {
                _DateFrom = value;
                OnPropertyChanged("DateFrom");
            }
        }

        //Date Range To
        private DateTime _DateTo;

        public DateTime DateTo
        {
            get { return _DateTo; }
            set
            {
                _DateTo = value;
                OnPropertyChanged("DateTo");
            }
        }


        //To override po start date in DB
        private string _StartDateOverride;

        public string StartDateOverride
        {
            get { return _StartDateOverride; }
            set
            {
                _StartDateOverride = value;
                OnPropertyChanged("StartDateOverride");
            }
        }






        //Number of days to span for Report Linking 30, 60, 90 etc
        private string _SpanDays;

        public string SpanDays
        {
            get { return _SpanDays; }
            set
            {
                _SpanDays = value;
                OnPropertyChanged("SpanDays");
            }
        }


        //Observable Collection To Hold Text Suggestions for AutoComplete Textbox
        private ObservableCollection<string> _TextSuggestion = new ObservableCollection<string>();
        public ObservableCollection<string> TextSuggestion
        {
            get { return _TextSuggestion; }
            set
            {
                _TextSuggestion = value;
                OnPropertyChanged("TextSuggestion");
            }
        }


        //Extract the Product Codes from the file
        public string FromCsv(string csvLine)
        {
            string code = "";
            if (csvLine != null)
            {
                string[] values = csvLine.Split(',');


                if (values[0] != string.Empty)
                    code = Convert.ToString(values[0]);
            }

            return code;
        }


        //Product Code List
        private List<string> _ProductCodes;

        public List<string> ProductCodes
        {
            get { return _ProductCodes; }
            set
            {
                _ProductCodes = value;
                OnPropertyChanged("ProductCodes");
            }
        }



        //PROPERTIES  
        private UserControl _UCView;

        public UserControl UCView
        {
            get { return _UCView; }
            set
            {
                _UCView = value;
                OnPropertyChanged("UCView");
            }
        }

        //boolean property to show Discrepancy Run
        private bool _HasDiscrepancyRun;

        public bool HasDiscrepancyRun
        {
            get { return _HasDiscrepancyRun; }
            set
            {
                _HasDiscrepancyRun = value;
                OnPropertyChanged("HasDiscrepancyRun");
            }
        }


        //boolean property for exact search
        private bool _MatchExactly;

        public bool MatchExactly
        {
            get { return _MatchExactly; }
            set
            {
                _MatchExactly = value;
                OnPropertyChanged("MatchExactly");
            }
        }


        //boolean property to show or hide the progress ring
        private bool _ProgressIsActive;

        public bool ProgressIsActive
        {
            get { return _ProgressIsActive; }
            set
            {
                _ProgressIsActive = value;
                OnPropertyChanged("ProgressIsActive");
            }
        }

        //to hold user value
        private string _PreviousSerialNumber;
        public string PreviousSerialNumber
        {
            get { return _PreviousSerialNumber; }
            set
            {
                _PreviousSerialNumber = value;
                OnPropertyChanged("PreviousSerialNumber");
            }
        }

        //to hold user value
        private string _CurrentUserLocation;
        public string CurrentUserLocation
        {
            get { return _CurrentUserLocation; }
            set
            {
                _CurrentUserLocation = value;
                OnPropertyChanged("CurrentUserLocation");
            }
        }


        //to hold user value
        private string _CurrentUser;
        public string CurrentUser
        {
            get { return _CurrentUser; }
            set
            {
                _CurrentUser = value;
                OnPropertyChanged("CurrentUser");
            }
        }

        //Property for current windows user initials
        private string _CurrentUserInitials;

        public string CurrentUserInitials
        {
            get { return _CurrentUserInitials; }
            set
            {
                _CurrentUserInitials = value;
                OnPropertyChanged("CurrentUserInitials");
            }
        }

        //Property for current windows user
        private string _ReportUser;

        public string ReportUser
        {
            get { return _ReportUser; }
            set
            {
                _ReportUser = value;
                OnPropertyChanged("ReportUser");
            }
        }

        //Property for full windows user
        private string _FullUser;

        public string FullUser
        {
            get { return _FullUser; }
            set
            {
                _FullUser = value;
                OnPropertyChanged("FullUser");
            }
        }

        //Import Message properties
        //Message Text
        private string _ImportMessage;
        public string ImportMessage
        {
            get { return _ImportMessage; }
            set
            {
                _ImportMessage = value;
                OnPropertyChanged("ImportMessage");
            }
        }

        //Colour of message
        private Brush _ImportMessageColour;
        public Brush ImportMessageColour
        {
            get { return _ImportMessageColour; }
            set
            {
                _ImportMessageColour = value;
                OnPropertyChanged("ImportMessageColour");
            }
        }

        private string _ProcessedMessage;
        public string ProcessedMessage
        {
            get { return _ProcessedMessage; }
            set
            {
                _ProcessedMessage = value;
                OnPropertyChanged("ProcessedMessage");
            }
        }

        //Message to display after discrepancy
        private string _DiscrepancyMessage;
        public string DiscrepancyMessage
        {
            get { return _DiscrepancyMessage; }
            set
            {
                _DiscrepancyMessage = value;
                OnPropertyChanged("DiscrepancyMessage");
            }
        }

        //Colour of message
        private Brush _ProcessedMessageColour;
        public Brush ProcessedMessageColour
        {
            get { return _ProcessedMessageColour; }
            set
            {
                _ProcessedMessageColour = value;
                OnPropertyChanged("ProcessedMessageColour");
            }
        }


        //to hold user value
        private string _DBSearch;
        public string DBSearch
        {
            get { return _DBSearch; }
            set
            {
                _DBSearch = value.ToUpper();
                OnPropertyChanged("DBSearch");
            }
        }


        ////to hold user value
        //private int _ScanDelay;
        //public int ScanDelay
        //{
        //    get { return _ScanDelay; }
        //    set
        //    {
        //       OnPropertyChanged("ScanDelay");
        //    }
        //}


        //public static readonly DependencyProperty ScanDelay =
        // DependencyProperty.Register("ScanDelay", typeof(string), typeof(TextBox), new FrameworkPropertyMetadata
        // {
        //     DefaultValue = "",
        //     DefaultUpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged,
        // });



        //to hold user value
        private string _SerialSearch;
        public string SerialSearch
        {
            get { return _SerialSearch; }
            set
            {
                _SerialSearch = value.ToUpper();
                OnPropertyChanged("SerialSearch");
            }
        }

        //to hold user value
        private string _ProductSearch;
        public string ProductSearch
        {
            get { return _ProductSearch; }
            set
            {
                _ProductSearch = value.ToUpper();
                OnPropertyChanged("ProductSearch");
            }
        }

        //to hold user value
        private string _PONumber;
        public string PONumber
        {
            get { return _PONumber; }
            set
            {
                _PONumber = value.ToUpper();
                OnPropertyChanged("PONumber");
            }
        }

        //PO Import Total
        private int _POImportTotal;
        public int POImportTotal
        {
            get { return _POImportTotal; }
            set
            {
                _POImportTotal = value;
                OnPropertyChanged("POImportTotal");
            }
        }


        //TotalOnImport
        private int _TotalOnImport;
        public int TotalOnImport
        {
            get { return _TotalOnImport; }
            set
            {
                _TotalOnImport = value;
                OnPropertyChanged("TotalOnImport");
            }
        }


        //PO Required Part Codes Total
        private int _PartCodesTotal;
        public int PartCodesTotal
        {
            get { return _PartCodesTotal; }
            set
            {
                _PartCodesTotal = value;
                OnPropertyChanged("PartCodesTotal");
            }
        }


        //PO Total
        private int _POTotal;
        public int POTotal
        {
            get { return _POTotal; }
            set
            {
                _POTotal = value;
                OnPropertyChanged("POTotal");
            }
        }

        //Processed Total
        private int _ProcessedTotal;
        public int ProcessedTotal
        {
            get { return _ProcessedTotal; }
            set
            {
                _ProcessedTotal = value;
                OnPropertyChanged("ProcessedTotal");
            }
        }

        //to hold Process Section SerialNo value
        private string _ProcessSerialNo;
        public string ProcessSerialNo
        {
            get { return _ProcessSerialNo; }
            set
            {
                _ProcessSerialNo = value.ToUpper();
                OnPropertyChanged("ProcessSerialNo");
            }
        }


        //Current Line Serial Count
        private int _CurrentLineSerialCount;
        public int CurrentLineSerialCount
        {
            get { return _CurrentLineSerialCount; }
            set
            {
                _CurrentLineSerialCount = value;
                OnPropertyChanged("CurrentLineSerialCount");
            }
        }


        //to hold PartCode value
        private string _PartCode;
        public string PartCode
        {
            get { return _PartCode; }
            set
            {
                _PartCode = value.ToUpper();
                OnPropertyChanged("PartCode");
            }
        }

        //to hold user value
        private string _PartDescription;
        public string PartDescription
        {
            get { return _PartDescription; }
            set
            {
                _PartDescription = value;
                OnPropertyChanged("PartDescription");
            }
        }

        //to hold user value
        private string _POQuantity;
        public string POQuantity
        {
            get { return _POQuantity; }
            set
            {
                _POQuantity = value;
                OnPropertyChanged("POQuantity");
            }
        }

        //to hold message to user value
        private string _POMessage;
        public string POMessage
        {
            get { return _POMessage; }
            set
            {
                _POMessage = value;
                OnPropertyChanged("POMessage");
            }
        }


        private string _POLineUnitPrice;
        public string POLineUnitPrice
        {
            get { return _POLineUnitPrice; }
            set
            {
                _POLineUnitPrice = value;
                OnPropertyChanged("POLineUnitPrice");
            }
        }

        private string _POUnitAmount;
        public string POUnitAmount
        {
            get { return _POUnitAmount; }
            set
            {
                _POUnitAmount = value;
                OnPropertyChanged("POUnitAmount");
            }
        }

        private string _POTotalAmount;
        public string POTotalAmount
        {
            get { return _POTotalAmount; }
            set
            {
                _POTotalAmount = value;
                OnPropertyChanged("POTotalAmount");
            }
        }


        private string _PartCodeRequired;
        public string PartCodeRequired
        {
            get { return _PartCodeRequired; }
            set
            {
                _PartCodeRequired = value;
                OnPropertyChanged("PartCodeRequired");
            }
        }

        private bool _FocusOnSerialTXT;
        public bool FocusOnSerialTXT
        {
            get { return _FocusOnSerialTXT; }
            set
            {
                _FocusOnSerialTXT = FocusOnSerialTXT;
                OnPropertyChanged("FocusOnSerialTXT");
            }
        }


        private string _FailiureReason;
        public string FailiureReason
        {
            get { return _FailiureReason; }
            set
            {
                _FailiureReason = value;
                OnPropertyChanged("FailiureReason");
            }
        }


        private string _FailiureReasonSpecify;
        public string FailiureReasonSpecify
        {
            get { return _FailiureReasonSpecify; }
            set
            {
                _FailiureReasonSpecify = value;
                OnPropertyChanged("FailiureReasonSpecify");
            }
        }



        private bool _FocusOnProductCodeTXT;
        public bool FocusOnProductCodeTXT
        {
            get { return _FocusOnProductCodeTXT; }
            set
            {
                _FocusOnProductCodeTXT = FocusOnProductCodeTXT;
                OnPropertyChanged("FocusOnProductCodeTXT");
            }
        }

        private bool _FocusOnPONumberTXT;
        public bool FocusOnPONumberTXT
        {
            get { return _FocusOnPONumberTXT; }
            set
            {
                _FocusOnPONumberTXT = FocusOnPONumberTXT;
                OnPropertyChanged("FocusOnPONumberTXT");
            }
        }


       



        //to hold Scanned Serial No value
        private string _ScannedSerial;
        public string ScannedSerial
        {
            get { return _ScannedSerial; }
            set
            {
                _ScannedSerial = value.ToUpper();

                if(ImportLVSelectedItem.SerialsRemaining == 0 || ImportLVSelectedItem.SerialsRemaining < 0)
                {
                    _ScannedSerial = "";
                    SerialMessage2 = "No serials remaining";
                }
                else
                {
                    //Add Serial to DB
                    Object[] values = new Object[4];
                    values[0] = (Object)"AddSerial";
                    values[1] = null;
                    values[2] = null;
                    values[3] = null;

                 DPDBCrudCommand.Execute(values);

                 CommandManager.InvalidateRequerySuggested();

                }

               // MessageBox.Show(SerialMessage2);

                OnPropertyChanged("ScannedSerial");
            }
        }

        //Serial Message
        private string _SerialMessage2;
        public string SerialMessage2
        {
            get { return _SerialMessage2; }
            set
            {
                _SerialMessage2 = value;

              
                OnPropertyChanged("SerialMessage2");
            }
        }

        //Test Count Items
        private string _TestCount;
        public string TestCount
        {
            get { return _TestCount; }
            set
            {
                _TestCount = "Total " + value;


                OnPropertyChanged("TestCount");
            }
        }

        private string _SearchCount;
        public string SearchCount
        {
            get { return _SearchCount; }
            set
            {
                _SearchCount = "Total " + value;


                OnPropertyChanged("SearchCount");
            }
        }

        //Pass Count
        private string _PassCount;
        public string PassCount
        {
            get { return _PassCount; }
            set
            {
                _PassCount = "Pass " + value;
                OnPropertyChanged("PassCount");
            }
        }

        //Fail Count
        private string _FailCount;
        public string FailCount
        {
            get { return _FailCount; }
            set
            {
                _FailCount = "Fails " + value;
                OnPropertyChanged("FailCount");
            }
        }


        //Pending Count
        private string _PendingCount;
        public string PendingCount
        {
            get { return _PendingCount; }
            set
            {
                _PendingCount = @"Pending\No Match " + value;
                OnPropertyChanged("PendingCount");
            }
        }

        //Give focus to Serial Textbox
        private int _SerialsCount;
        public int SerialsCount
        {
            get { return _SerialsCount; }
            set
            {
                _SerialsCount = value;
              

                OnPropertyChanged("SerialsCount");
            }
        }

        //Enable\Disable Serial Textbox
        private bool _SerialTBEnabled;
        public bool SerialTBEnabled
        {
            get { return _SerialTBEnabled; }
            set
            {
                _SerialTBEnabled = value;
                OnPropertyChanged("SerialTBEnabled");
            }
        }


        //MultiSelect
        private bool _MultiSelect;
        public bool MultiSelect
        {
            get { return _MultiSelect; }
            set
            {
                _MultiSelect = value;
                OnPropertyChanged("MultiSelect");
            }
        }

        //SelectionMode
        private SelectionMode _SelectMode;
        public SelectionMode SelectMode
        {
            get { return _SelectMode; }
            set
            {
                _SelectMode = value;
                OnPropertyChanged("SelectMode");
            }
        }



        //display Various Section if detected
        private Visibility _ShowVariousSection;
        public Visibility ShowVariousSection
        {
            get { return _ShowVariousSection; }
            set
            {
                _ShowVariousSection = value;
                OnPropertyChanged("ShowVariousSection");
            }
        }

        //display Scan TextBox Section if detected
        private Visibility _ShowScanTxt;
        public Visibility ShowScanTxt
        {
            get { return _ShowScanTxt; }
            set
            {
                _ShowScanTxt = value;
                OnPropertyChanged("ShowScanTxt");
            }
        }

        private bool _ShowPopup;
        public bool ShowPopup
        {
            get { return _ShowPopup; }
            set
            {
                _ShowPopup = value;
                OnPropertyChanged("ShowPopup");
            }
        }

        //Import LV Enabled
        private bool _ImportListEnabled;
        public bool ImportListEnabled
        {
            get { return _ImportListEnabled; }
            set
            {
                _ImportListEnabled = value;
                OnPropertyChanged("ImportListEnabled");
            }
        }


        //Index of a listview item, needs binding on the control
        private int _ImportListViewIndex;
        public int ImportListViewIndex
        {
            get { return _ImportListViewIndex; }
            set
            {
                _ImportListViewIndex = value;


                OnPropertyChanged("ImportListViewIndex");
            }
        }


        //Last Index of a listview item, needs binding on the control
        private int _ImportListLastIndex;
        public int ImportListLastIndex
        {
            get { return _ImportListLastIndex; }
            set
            {
                _ImportListLastIndex = value;


                OnPropertyChanged("ImportListLastIndex");
            }
        }

        //Index of a listview item, needs binding on the control
        private int _ProcessedListViewIndex;
        public int ProcessedListViewIndex
        {
            get { return _ProcessedListViewIndex; }
            set
            {
                _ProcessedListViewIndex = value;


                OnPropertyChanged("ProcessedListViewIndex");
            }
        }

      


        //to hold import lv selected value
        private NetsuiteInventory _ImportLVSelectedItem;
        public NetsuiteInventory ImportLVSelectedItem
        {
            get { return _ImportLVSelectedItem; }
            set
            {
                _ImportLVSelectedItem = value;

               

                OnPropertyChanged("ImportLVSelectedItem");
            }
        }

        //to hold processed lv selected value
        private NetsuiteInventory _ProcessedLVSelectedItem;
        public NetsuiteInventory ProcessedLVSelectedItem
        {
            get { return _ProcessedLVSelectedItem; }
            set
            {

                _ProcessedLVSelectedItem = value;

        


                OnPropertyChanged("ProcessedLVSelectedItem");
            }
        }

        //to hold test results lv selected value
        private NetsuiteInventory _TestResultsLVSelectedItem;
        public NetsuiteInventory TestResultsLVSelectedItem
        {
            get { return _TestResultsLVSelectedItem; }
            set
            {
                //Update Previous serial number property as this will be used to change
                if(value != null)
                {
                    PreviousSerialNumber = value.SerialNumber;


                }


                _TestResultsLVSelectedItem = value;
              
                           
               

                OnPropertyChanged("TestResultsLVSelectedItem");
            }
        }

        //to hold Search lv selected value
        private NetsuiteInventory _SearchResultsLVSelectedItem;
        public NetsuiteInventory SearchResultsLVSelectedItem
        {
            get { return _SearchResultsLVSelectedItem; }
            set
            {
                _SearchResultsLVSelectedItem = value;


                OnPropertyChanged("SearchResultsLVSelectedItem");
            }
        }

        //To Hold list of pre-recieved drives imprted from a csv
        private ObservableCollection<ImportSerials> _ImportSerialsCollection = new ObservableCollection<ImportSerials>();
        public ObservableCollection<ImportSerials> ImportSerialsCollection
        {
            get { return _ImportSerialsCollection; }
            set
            {
                _ImportSerialsCollection = value;

                OnPropertyChanged("ImportSerialsCollection");
            }
        }


        //to hold imported results from CSV
        private ObservableCollection<NetsuiteInventory> _NetsuiteImportCollection = new ObservableCollection<NetsuiteInventory>();
        public ObservableCollection<NetsuiteInventory> NetsuiteImportCollection
        {
            get { return _NetsuiteImportCollection; }
            set
            {
                _NetsuiteImportCollection = value;

                //MessageBox.Show("Inside Prop1");
             
              

                OnPropertyChanged("NetsuiteImportCollection");
            }
        }

        //Events
        public void ImportLVHandler(object sender, NotifyCollectionChangedEventArgs e)
        {
            var items = (ItemCollection)sender;
            POTotal = 0;
            //Perform a totalling of all quantities within the collection
            foreach (NetsuiteInventory ns in items)
            {
                POTotal += Convert.ToInt32(ns.Quantity);
            }
        }

        public void ProcessedLVHandler(object sender, NotifyCollectionChangedEventArgs e)
        {
            var items = (ItemCollection)sender;
            ProcessedTotal = 0;
            //Perform a totalling of all quantities within the collection
            foreach (NetsuiteInventory ns in items)
            {
                ProcessedTotal += Convert.ToInt32(ns.Quantity);
            }
        }

        //to hold netsuite inventory results from CSV
        private ObservableCollection<NetsuiteInventory> _NetsuiteOriginalImportCollection = new ObservableCollection<NetsuiteInventory>();
        public ObservableCollection<NetsuiteInventory> NetsuiteOriginalImportCollection
        {
            get { return _NetsuiteOriginalImportCollection; }
            set
            {
                _NetsuiteOriginalImportCollection = value;
                OnPropertyChanged("NetsuiteOriginalImportCollection");
            }
        }

        //to hold netsuite inventory results from CSV
        private ObservableCollection<NetsuiteInventory> _NetsuiteComparisonCollection = new ObservableCollection<NetsuiteInventory>();
        public ObservableCollection<NetsuiteInventory> NetsuiteComparisonCollection
        {
            get { return _NetsuiteComparisonCollection; }
            set
            {
                _NetsuiteComparisonCollection = value;
                OnPropertyChanged("NetsuiteComparisonCollection");
            }
        }

        //to hold Processed Lines
        private ObservableCollection<NetsuiteInventory> _ScannedSerialCollection = new ObservableCollection<NetsuiteInventory>();
        public ObservableCollection<NetsuiteInventory> ScannedSerialCollection
        {
            get { return _ScannedSerialCollection; }
            set
            {
                _ScannedSerialCollection = value;

               OnPropertyChanged("ScannedSerialCollection");
            }
        }


        //to hold Scanned items
        private ObservableCollection<NetsuiteInventory> _NetsuiteScannedCollection = new ObservableCollection<NetsuiteInventory>();
        public ObservableCollection<NetsuiteInventory> NetsuiteScannedCollection
        {
            get { return _NetsuiteScannedCollection; }
            set
            {
                _NetsuiteScannedCollection = value;

                OnPropertyChanged("NetsuiteScannedCollection");
            }
        }

        //to hold Processed Lines
        private ObservableCollection<NetsuiteInventory> _SerialsTotalsCollection = new ObservableCollection<NetsuiteInventory>();
        public ObservableCollection<NetsuiteInventory> SerialsTotalsCollection
        {
            get { return _SerialsTotalsCollection; }
            set
            {
                _SerialsTotalsCollection = value;

                OnPropertyChanged("SerialsTotalsCollection");
            }
        }

        //to hold Drive Test Lines
        private ObservableCollection<NetsuiteInventory> _TestedDriveCollection = new ObservableCollection<NetsuiteInventory>();
        public ObservableCollection<NetsuiteInventory> TestedDriveCollection
        {
            get { return _TestedDriveCollection; }
            set
            {
                _TestedDriveCollection = value;

                OnPropertyChanged("TestedDriveCollection");
            }
        }

        //to hold General Search Results
        private ObservableCollection<NetsuiteInventory> _SearchCollection = new ObservableCollection<NetsuiteInventory>();
        public ObservableCollection<NetsuiteInventory> SearchCollection
        {
            get { return _SearchCollection; }
            set
            {
                _SearchCollection = value;

                OnPropertyChanged("SearchCollection");
            }
        }

        //Report Collection
        private ObservableCollection<NetsuiteInventory> _ReportCollection = new ObservableCollection<NetsuiteInventory>();
        public ObservableCollection<NetsuiteInventory> ReportCollection
        {
            get { return _ReportCollection; }
            set
            {
                _ReportCollection = value;

                OnPropertyChanged("ReportCollection");
            }
        }

        //to hold netsuite inventory results from CSV USES NetsuiteDiscrepancyReport
        private ObservableCollection<NetsuiteDiscrepancyReport> _NetsuiteDiscrepancyCollection = new ObservableCollection<NetsuiteDiscrepancyReport>();
        public ObservableCollection<NetsuiteDiscrepancyReport> NetsuiteDiscrepancyCollection
        {
            get { return _NetsuiteDiscrepancyCollection; }
            set
            {
                _NetsuiteDiscrepancyCollection = value;
                OnPropertyChanged("NetsuiteDiscrepancyCollection");
            }
        }




        //Command backing properties 
        //Print SaveFlow Document Command
        private PrintSaveFlowDocumentDTCommand _PrintSaveFlowDocumentDTCommand = null;
        public PrintSaveFlowDocumentDTCommand PrintSaveFlowDocumentDTCommand
        {
            get { return _PrintSaveFlowDocumentDTCommand; }
        }

        //Print SaveFlow Document Report Command
        private PrintSaveFlowDocumentREPCommand _PrintSaveFlowDocumentREPCommand = null;
        public PrintSaveFlowDocumentREPCommand PrintSaveFlowDocumentREPCommand
        {
            get { return _PrintSaveFlowDocumentREPCommand; }
        }

        //CRUD Operations
        private DPCrudCommand _DPCrudCommand = null;
        public DPCrudCommand DPCrudCommand
        {
            get { return _DPCrudCommand; }
        }

        //SQL LITE DBCRUD Operations
        private DPDBCrudCommand _DPDBCrudCommand = null;
        public DPDBCrudCommand DPDBCrudCommand
        {
            get { return _DPDBCrudCommand; }
        }

        //Various Completed Command
        private DPVariousCompleteCommand _DPVariousCompleteCommand = null;
        public DPVariousCompleteCommand DPVariousCompleteCommand
        {
            get { return _DPVariousCompleteCommand; }
        }

        //CheckPartCode against CSV
        private DPCheckPartCodeCommand _DPCheckPartCodeCommand = null;
        public DPCheckPartCodeCommand DPCheckPartCodeCommand
        {
            get { return _DPCheckPartCodeCommand; }
        }

        //Import Netsuite CSV Command
        private DPImportCSVCommand _DPImportCSVCommand = null;
        public DPImportCSVCommand DPImportCSVCommand
        {
            get { return _DPImportCSVCommand; }
        }

        //Remove Netsuite item 
        private DPRemoveLVItemsCommand _DPRemoveLVItemsCommand = null;
        public DPRemoveLVItemsCommand DPRemoveLVItemsCommand
        {
            get { return _DPRemoveLVItemsCommand; }
        }

        //Remove Individual item 
        private DPDeleteSelectedItemCommand _DPDeleteSelectedItemCommand = null;
        public DPDeleteSelectedItemCommand DPDeleteSelectedItemCommand
        {
            get { return _DPDeleteSelectedItemCommand; }
        }

        //Scan Serials 
        private DPScanSerialsCommand _DPScanSerialsCommand = null;
        public DPScanSerialsCommand DPScanSerialsCommand
        {
            get { return _DPScanSerialsCommand; }
        }

        //Quantity Totals
        private DPQuantityCommand _DPQuantityCommand = null;
        public  DPQuantityCommand DPQuantityCommand
        {
            get { return _DPQuantityCommand; }
        }


        //Increase\Decrease Quantity
        private DPExtraItemsCommand _DPExtraItemsCommand = null;
        public DPExtraItemsCommand DPExtraItemsCommand
        {
            get { return _DPExtraItemsCommand; }
        }

        //Increase\Decrease Quantity
        private DPExportCSVCommand _DPExportCSVCommand = null;
        public DPExportCSVCommand DPExportCSVCommand
        {
            get { return _DPExportCSVCommand; }
        }

        //caps lock on
        private DPCapsLockONCommand _DPCapsLockONCommand = null;
        public DPCapsLockONCommand DPCapsLockONCommand
        {
            get { return _DPCapsLockONCommand; }
        }

        //file path command
        private DPSelectFilePathCommand _DPSelectFilePathCommand = null;
        public DPSelectFilePathCommand DPSelectFilePathCommand
        {
            get { return _DPSelectFilePathCommand; }
        }

        //Launch File command
        private DPLaunchFileCommand _DPLaunchFileCommand = null;
        public DPLaunchFileCommand DPLaunchFileCommand
        {
            get { return _DPLaunchFileCommand; }
        }
        //Email Documents command
        private DPEmailDocumentsCommand _DPEmailDocumentsCommand = null;
        public DPEmailDocumentsCommand DPEmailDocumentsCommand
        {
            get { return _DPEmailDocumentsCommand; }
        }

        //Methods 


        //SHOW VARIOUS\ADD EXTRA SCREENS IN POPUP OR JUST THE SERIAL SCAN
        public void GetSelectedImportViewItem()
        {
            if (ImportLVSelectedItem != null)
            if (ImportLVSelectedItem.ProductCode != null)
            {
                    // Console.WriteLine("Serial Count: "+ SerialsCount); 


                    ////Set the Serials Remaing Property
                    //vm.ImportLVSelectedItem.SerialsRemaining -= 1;

                    //ZERO COUNT
                    int itemCount = 0;
                    //// LOOP THROUGH CURRENT ITEMS AND COUNT INDIVIDUAL LINES
                    foreach (NetsuiteInventory itm in ScannedSerialCollection.Where(x => x.ProductCode == ImportLVSelectedItem.ProductCode))
                    {
                        itemCount += 1;
                        //Console.WriteLine(itm.ProductCode + " " + itemCount);

                    }


                    if (ImportLVSelectedItem.SerialsRemaining == 0)
                    {

                     

                        ////Set the Serials Remaing Property
                        //vm.ImportLVSelectedItem.SerialsRemaining -= 1;
                        if(itemCount == 0)
                        {
                            //MessageBox.Show("Here");
                            //Update the quantity property
                            SerialsCount = ushort.Parse(ImportLVSelectedItem.Quantity);
                            //Set the Serials Remaining Property on the object

                            ImportLVSelectedItem.SerialsRemaining = SerialsCount;
                            //get how many are already scanned
                           // ImportLVSelectedItem.SerialsRemaining = Convert.ToInt32(ImportLVSelectedItem.Quantity) - itemCount;
                        }
                        else
                        {
                            //get how many are already scanned
                            ImportLVSelectedItem.SerialsRemaining = Convert.ToInt32(ImportLVSelectedItem.Quantity) - itemCount;
                        }
                     


                        ////Update the quantity property
                        //SerialsCount = ushort.Parse(ImportLVSelectedItem.Quantity);
                        ////Set the Serials Remaining Property on the object
                      
                        //ImportLVSelectedItem.SerialsRemaining = SerialsCount;
                        //////Close the Pop Up
                        //DPVariousCompleteCommand.Execute("Close");
                    }
                    else
                    {

                        //
                        //get how many are already scanned
                        ImportLVSelectedItem.SerialsRemaining = Convert.ToInt32(ImportLVSelectedItem.Quantity) - itemCount;

                        //if(ImportLVSelectedItem.SerialsRemaining < 0)
                        //{
                         //ImportLVSelectedItem.SerialsRemaining = 0;
                        //}
                        //do nothing;
                    }

                    //Update the quantity property
                   // SerialsCount = ushort.Parse(ImportLVSelectedItem.Quantity);


                    //Check if various and show inputs
               if (ImportLVSelectedItem.ProductCode.Contains("VARIOUS") || ImportLVSelectedItem.ProductCode.Contains("various") || ImportLVSelectedItem.ProductCode.Contains("Various"))
                {
                    ShowVariousSection = Visibility.Visible;
                    ShowScanTxt = Visibility.Collapsed;
                    FocusOnProductCodeTXT = true;

                    //Set Focus on Serial TextBox
                    //passedTB.Focus();
                    //FocusOnProductCodeTXT = true;
                }
                else
                {
                    ShowVariousSection = Visibility.Collapsed;
                    ShowScanTxt = Visibility.Visible;
                    //Enable TextBox Input
                    SerialTBEnabled = true;
                    //Set Focus on Serial TextBox
                    FocusOnSerialTXT = true;

                    //Set Focus on Serial TextBox
                    // FocusOnSerialTXT = true;
                }
                //show pop up menu to add custom items
                ShowPopup = true;
            }
              

          
           
        }


        public void GetSelectedImportViewItem(string anyExtras)
        {

          

            //Check if various and show inputs
            if (anyExtras == "Add Extras")
            {
                ShowVariousSection = Visibility.Visible;
                ShowScanTxt = Visibility.Collapsed;
                FocusOnProductCodeTXT = true;
               // FocusOnProductCodeTXT = true;
            }
            else
            {
                ShowVariousSection = Visibility.Collapsed;
                ShowScanTxt = Visibility.Visible;
                //Enable TextBox Input
                SerialTBEnabled = true;
                FocusOnSerialTXT = false;
                //set focus on textbox
                //FocusOnSerialTXT = true;
            }
            //show pop up menu to add custom items
            ShowPopup = true;



        }







        private void RefreshNSData()
        {
            if (File.Exists(@"" + myDocs + @"\Inventory.csv"))
            {

            }
            else
            {
                File.Copy(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\Inventory.csv", myDocs + @"\Inventory.csv");
            }

            //Copy the excel with query to netsuite local to user
            if (File.Exists(@"" + myDocs + @"\NS Item WQ.xlsx"))
            {





                //call async function
                GetExcelData();

            }
            else
            {


                if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\NS Item WQ.xlsx"))
                {
                    File.Copy(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\NS Item WQ.xlsx", @"" + myDocs + @"\NS Item WQ.xlsx");
                    //call async function
                    GetExcelData();



                }
                else
                {
                    File.Copy(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\NS Item WQ.xlsx", @"" + myDocs + @"\NS Item WQ.xlsx");



                    //call async function
                    GetExcelData();

                }




            }
        }


        private async void GetExcelData()
        {

            try
            {


                if (File.Exists(@"" + myDocs + @"\Inventory.csv"))
                {
                    // Get local copy of file
                    string inv = @"" + myDocs + @"\Inventory.csv";

                    // Check if file was is older than a day since last updated, if so then rerun inventory check
                    FileInfo fi = new FileInfo(inv);
                    if (fi.LastWriteTime < DateTime.Now.AddDays(-1))
                    {
                        //Run inventory check
                        await System.Threading.Tasks.Task.Run(() => ExcelToCSV(@"" + myDocs + @"\NS Item WQ.xlsx", @"" + myDocs + @"\Inventory.csv"));
                    }
                    else
                    {
                        //skip
                    }

                    //Skip reloading part
                    //await PutTaskDelay(20000);
                }
                else
                {
                    //Run Inventory check again
                    //await PutTaskDelay(20000);
                    await System.Threading.Tasks.Task.Run(() => ExcelToCSV(@"" + myDocs + @"\NS Item WQ.xlsx", @"" + myDocs + @"\Inventory.csv"));
                }



            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }


        //exporting excel into csv file
        public async void ExcelToCSV(string excelPath, string exportPath)
        {

            //this Thread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Microsoft.Office.Interop.Excel.Application app = null;
            //InteropExcel.Workbooks wkbks = null;
            Microsoft.Office.Interop.Excel.Workbook wb = null;


            try
            {


                String fromFile = excelPath;
                String toFile = exportPath;

                app = new Microsoft.Office.Interop.Excel.Application();
                wb = app.Workbooks.Open(fromFile, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                app.DisplayAlerts = false;

                // Loop through and end query on each workbook then run refresh all after
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    foreach (QueryTable table in worksheet.QueryTables)
                        table.BackgroundQuery = false;
                }


                // refresh the excel data
                wb.RefreshAll();

                //Listed way but never finishes
                //app.CalculateUntilAsyncQueriesDone();

                //** old delay **
                // await PutTaskDelay(16000);

                //wb.Save();

                //await PutTaskDelay(5000);
                // this does not throw exception if file doesnt exist
                //File.Delete(toFile);
                //await PutTaskDelay(12000);
                wb.SaveAs(toFile, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSVWindows, Type.Missing, Type.Missing, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, false, Type.Missing, Type.Missing, Type.Missing);
                //await PutTaskDelay(8000);
                //wb.Close(true, Type.Missing, Type.Missing);

                //app.Quit();
                if (wb != null)
                {
                    //app.DisplayAlerts = false;
                    // wb.Close(false);
                    wb.Close(true, Type.Missing, Type.Missing);
                    // Marshal.FinalReleaseComObject(wb);
                    wb = null;
                }

                //if (wkbks != null)
                //{
                //    wkbks.Close();
                //    Marshal.FinalReleaseComObject(wkbks);
                //    wkbks = null;
                //}

                if (app != null)
                {
                    // Close Excel.
                    app.Quit();
                    // Marshal.ReleaseComObject(app);
                    // Marshal.FinalReleaseComObject(app);
                    await PutTaskDelay(1000);
                    //Terminate PID Instance
                    TerminateExcelProcess(app);

                    app = null;


                }




            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        //CAPTURE PID FOR EXCEL PROCESS

        [DllImport("user32.dll")]
        static extern int GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);

        Process GetExcelProcess(Microsoft.Office.Interop.Excel.Application excelApp)
        {
            int id;
            GetWindowThreadProcessId(excelApp.Hwnd, out id);
            return Process.GetProcessById(id);
        }

        void TerminateExcelProcess(Microsoft.Office.Interop.Excel.Application excelApp)
        {
            var process = GetExcelProcess(excelApp);
            if (process != null)
            {
                process.Kill();
            }
        }








        //TASK DELAY
        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }
    }
}