﻿using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Nebula.Commands;
using Nebula.Commands.XTerminal;
using Nebula.Models;
using Nebula.Helpers;
using Nebula.Views;
using MahApps.Metro.Controls;
using System.Windows.Documents;
using System.Windows;
using System.Collections.ObjectModel;
using System.Threading;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Sockets;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace Nebula.ViewModels
{
    public class GoodsInOutViewModel : HasPropertyChanged
    {
        public string dsktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);   //@"" + dsktop + @"
        public string myDocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);   //@"" + myDocs + @"



        // Variable for MAHAPPS DIALOGS
        private IDialogCoordinator dialogCoordinator;



        public GoodsInOutViewModel()
        {
            //USER VALUES CAPTURE
            //Get Current Windows User and remove domain prefix  Strip out the surname leaving only initials and capitalise
            //Check if returned username has 4 characters, if not set to 3
            int usernameCount = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Count();
            int subLength = 4;

            if (usernameCount < 4)
            {
                subLength = 3;
            }
            else
            {
                subLength = 4;
            }

            CurrentUser = "Technician Name: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, subLength).Replace(".", "").ToUpper() + " " + DateTime.Now.ToShortDateString();
            //Report USer
            ReportUser = "Technician Name: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, subLength).ToUpper() + " " + DateTime.Now.ToShortDateString();
            //Get Current Windows User and remove domain prefix  Strip out the surname leaving only initials and capitalise
            FullUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").ToUpperUserFullName();

            //Set Fulluser to Current
            CurrentUserInitials = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, subLength).ToUpper();

            //Get Region
            CurrentUserLocation = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            //Assign to Static Var
            StaticFunctions.UserLocation = CurrentUserLocation;


            switch (StaticFunctions.UserLocation)
            {
                case "Australia":
                    if (Directory.Exists(@"W:\AU Warehouse"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\AU Warehouse\";
                    }
                    else if (Directory.Exists(@"W:\"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\";
                    }

                    break;
                case "United States":
                    if (Directory.Exists(@"T:\NJ Warehouse\Technical Resources"))
                    {
                        StaticFunctions.UncPathToUse = @"T:\NJ Warehouse\Technical Resources\";
                    }
                    else if (Directory.Exists(@"T:\Technical Resources"))
                    {
                        StaticFunctions.UncPathToUse = @"T:\Technical Resources\";
                    }

                    break;
                case "United Kingdom":
                    // File Server DFS Swap Over
                    if (Directory.Exists(@"W:\UK Warehouse\Warehouse General"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\UK Warehouse\Warehouse General\";
                    }
                    else if (Directory.Exists(@"W:\Warehouse General"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\Warehouse General\";
                    }
                    break;
                case "France":
                    StaticFunctions.UncPathToUse = @"W:\FR Warehouse\Warehouse General\";
                    break;
                case "Germany":
                    StaticFunctions.UncPathToUse = @"W:\Warehouse General\";
                    break;

            }

            //Register the commands
            //_LoadContentCommand = new LoadContentCommand(this);
            //Register the commands
            _GenerateChecklistCommand = new GenerateChecklistCommand(this);
            _PreviewChecklistCommand = new PreviewChecklistCommand(this);
            _ReadChecklistCommand = new ReadChecklistCommand(this);
            //_PrintSaveDocCommand = new PrintSaveDocCommand(this);
            _PrintSaveFlowDocumentCommand = new PrintSaveFlowDocumentCommand(this);
            _SigCommand = new SigCommand(this);
            _AddFilesCommand = new AddFilesCommand(this);
            _AddFoldersCommand = new AddFoldersCommand(this);
            _AddFilesDellCommand = new AddFilesDellCommand(this);
            _AddFilesLenovoCommand = new AddFilesLenovoCommand(this);
            _SelectFolderCommand = new SelectFolderCommand(this);
            //Read Servers Processed command
            _ReadServersProcessedCommand = new ReadServersProcessedCommand(this);

            //IP Scanner command
            _IPScannerSearchCommand = new IPScannerSearchCommand(this);
            //Create a new IPResults collection
            IPResults = new ObservableCollection<IPInfo>();
            //LoadContent = new LandingPageView();

            //Set the date for the checklist date
            TodaysDate = DateTime.Now.ToShortDateString();
            DateCheck = TodaysDate;
            ChassisSerialNo = string.Empty;
            PurchaseOrderNumber = string.Empty;
            //Assign empty strings
            GoodsINRep = String.Empty;
            GoodsOUTRep = String.Empty;
            ChecklistMessage = String.Empty;
            //ServersProcessedCollection = new ServersProcessedCollection<ServersProcessedModel>

            //for ip scanner
            StartingIP = "192.168.101.1";
            EndingIP = "192.168.101.254";

            //MessageBox.Show(StaticFunctions.ChassisNoPassthrough);

            //passing in a chassis serial from the mainviewmodel
            ChassisSerialNo = StaticFunctions.ChassisNoPassthrough;
            //Weekly Server Records
            WeeklyServerRecordCollection = new ObservableCollection<WeeklyServerRecordsModel>();
            //Weekly Report Collection
            WeeklyServerReportCollection = new ObservableCollection<WeeklyServerRecordsModel>();


            //Quality Control Collections
            //List of users
            OperativeCollection = new ObservableCollection<Operatives>();
            //List of qc users
            QCOperativeCollection = new ObservableCollection<Operatives>();
            //Selected items
            //OperativeSelected = new Operatives();
            QCOperativeSelected = new Operatives();

            //List of QC Items
            QCItems = new ObservableCollection<QCRecordsModel>();

            //Add OBJECT
            AddQCRecord = new QCRecordsModel();

            //Update OBJECT
            UpdateQCRecord = new QCRecordsModel();



            Gen10reportFiles = new List<string>();

            AddWeeklyRecord = new WeeklyServerRecordsModel();
            UpdateWeeklyRecord = new WeeklyServerRecordsModel();
            //WeekComm = new WeekCommencing();

            //ITAD Properties
            ITADAssetsProcessed = new ObservableCollection<string>();
            ITADAttachedFiles = new ObservableCollection<string>();
            ITADReportCollection = new ObservableCollection<ITADReport>();
            ITADCategoryCollection = new ObservableCollection<ITADDeviceCategories>();
            ITADProcessesCollection = new ObservableCollection<ITADProcessesPerformed>();
            ITADTestingCollection = new ObservableCollection<ITADTestingPerformed>();
            ITADVerificationCollection = new ObservableCollection<ITADVerificationPerformed>();
            //on report document year at the bottom of the page
            WhichYear = "TechBuyer " + DateTime.Now.Year.ToString();

            //Text Suggestion List to load into Auto Textbox, used with server views
            TextSuggestion = new ObservableCollection<string>();

            //Create new list for product codes
            ProductCodes = new List<string>();

            //Set Initial Dates for Weekly Records
            DateFrom = DateTimeExtensions.StartOfWeek(DateTime.Now, DayOfWeek.Monday);
            DateTo = DateTime.Now;

            //ChassisSerialNo = "Hello"; //StaticFunctions.ChassisNoPassthrough;
        }



        public GoodsInOutViewModel(IDialogCoordinator instance)
        {
            //Dialog
            dialogCoordinator = instance;

            //USER VALUES CAPTURE
            //Get Current Windows User and remove domain prefix  Strip out the surname leaving only initials and capitalise
            //Check if returned username has 4 characters, if not set to 3
            int usernameCount = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Count();
            int subLength = 4;

            if (usernameCount < 4)
            {
                subLength = 3;
            }
            else
            {
                subLength = 4;
            }

            CurrentUser = "Technician Name: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, subLength).Replace(".", "").ToUpper() + " " + DateTime.Now.ToShortDateString();
            //Report USer
            ReportUser = "Technician Name: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, subLength).ToUpper() + " " + DateTime.Now.ToShortDateString();
            //Get Current Windows User and remove domain prefix  Strip out the surname leaving only initials and capitalise
            FullUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").ToUpperUserFullName();

            //Set Fulluser to Current
            CurrentUserInitials = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, subLength).ToUpper();

            //Get Region
            CurrentUserLocation = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            //Assign to Static Var
            StaticFunctions.UserLocation = CurrentUserLocation;


            switch (StaticFunctions.UserLocation)
            {
                case "Australia":
                    if (Directory.Exists(@"W:\AU Warehouse"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\AU Warehouse\";
                    }
                    else if (Directory.Exists(@"W:\"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\";
                    }

                    break;
                case "United States":
                    if (Directory.Exists(@"T:\NJ Warehouse\Technical Resources"))
                    {
                        StaticFunctions.UncPathToUse = @"T:\NJ Warehouse\Technical Resources\";
                    }
                    else if (Directory.Exists(@"T:\Technical Resources"))
                    {
                        StaticFunctions.UncPathToUse = @"T:\Technical Resources\";
                    }

                    break;
                case "United Kingdom":
                    // File Server DFS Swap Over
                    if (Directory.Exists(@"W:\UK Warehouse\Warehouse General"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\UK Warehouse\Warehouse General\";
                    }
                    else if (Directory.Exists(@"W:\Warehouse General"))
                    {
                        StaticFunctions.UncPathToUse = @"W:\Warehouse General\";
                    }

                    break;
                case "France":
                    StaticFunctions.UncPathToUse = @"W:\FR Warehouse\Warehouse General\";
                    break;
                case "Germany":
                    StaticFunctions.UncPathToUse = @"W:\Warehouse General\";
                    break;

            }

            //Register the commands
            //_LoadContentCommand = new LoadContentCommand(this);
            //Register the commands
            _GenerateChecklistCommand = new GenerateChecklistCommand(this);
            _PreviewChecklistCommand = new PreviewChecklistCommand(this);
            _ReadChecklistCommand = new ReadChecklistCommand(this);
            //_PrintSaveDocCommand = new PrintSaveDocCommand(this);
            _PrintSaveFlowDocumentCommand = new PrintSaveFlowDocumentCommand(this);
            _SigCommand = new SigCommand(this);
            _AddFilesCommand = new AddFilesCommand(this);
            _AddFoldersCommand = new AddFoldersCommand(this);
            _AddFilesDellCommand = new AddFilesDellCommand(this);
            _AddFilesLenovoCommand = new AddFilesLenovoCommand(this);
            _SelectFolderCommand = new SelectFolderCommand(this);
            //Read Servers Processed command
            _ReadServersProcessedCommand = new ReadServersProcessedCommand(this);

            //IP Scanner command
            _IPScannerSearchCommand = new IPScannerSearchCommand(this);
            //Create a new IPResults collection
            IPResults = new ObservableCollection<IPInfo>();
            //LoadContent = new LandingPageView();

            //Set the date for the checklist date
            TodaysDate = DateTime.Now.ToShortDateString();
            DateCheck = TodaysDate;
            ChassisSerialNo = string.Empty;
            PurchaseOrderNumber = string.Empty;
            //Assign empty strings
            GoodsINRep = String.Empty;
            GoodsOUTRep = String.Empty;
            ChecklistMessage = String.Empty;
            //ServersProcessedCollection = new ServersProcessedCollection<ServersProcessedModel>

            //for ip scanner
            StartingIP = "192.168.101.1";
            EndingIP = "192.168.101.254";

            //MessageBox.Show(StaticFunctions.ChassisNoPassthrough);

            //passing in a chassis serial from the mainviewmodel
            ChassisSerialNo = StaticFunctions.ChassisNoPassthrough;
            //Weekly Server Records
            WeeklyServerRecordCollection = new ObservableCollection<WeeklyServerRecordsModel>();
            //Weekly Report Collection
            WeeklyServerReportCollection = new ObservableCollection<WeeklyServerRecordsModel>();


            //Quality Control Collections
            //List of users
            OperativeCollection = new ObservableCollection<Operatives>();
            //List of qc users
            QCOperativeCollection = new ObservableCollection<Operatives>();
            //Selected items
            //OperativeSelected = new Operatives();
            QCOperativeSelected = new Operatives();

            //List of QC Items
            QCItems = new ObservableCollection<QCRecordsModel>();

            //Add OBJECT
            AddQCRecord = new QCRecordsModel();

            //Update OBJECT
            UpdateQCRecord = new QCRecordsModel();



            Gen10reportFiles = new List<string>();

            AddWeeklyRecord = new WeeklyServerRecordsModel();
            UpdateWeeklyRecord = new WeeklyServerRecordsModel();
            //WeekComm = new WeekCommencing();

            //ITAD Properties
            ITADAssetsProcessed = new ObservableCollection<string>();
            ITADAttachedFiles = new ObservableCollection<string>();
            ITADReportCollection = new ObservableCollection<ITADReport>();
            ITADCategoryCollection = new ObservableCollection<ITADDeviceCategories>();
            ITADProcessesCollection = new ObservableCollection<ITADProcessesPerformed>();
            ITADTestingCollection = new ObservableCollection<ITADTestingPerformed>();
            ITADVerificationCollection = new ObservableCollection<ITADVerificationPerformed>();
            //on report document year at the bottom of the page
            WhichYear = "TechBuyer " + DateTime.Now.Year.ToString();

            //Create new list for product codes
            ProductCodes = new List<string>();

            //Set Initial Dates for Weekly Records
            DateFrom = DateTimeExtensions.StartOfWeek(DateTime.Now, DayOfWeek.Monday);
            DateTo = DateTime.Now;

            //ChassisSerialNo = "Hello"; //StaticFunctions.ChassisNoPassthrough;

        }


        private void StartUp(IDialogCoordinator instance)
        {


        }

        //MAHAPPS DIALOG

        public async void CDROMWarning(string message)
        {
            try
            {
                await dialogCoordinator.ShowMessageAsync(this, "Server Notification", message);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public async void SystemMessageDialog(string systemref, string message)
        {
            try
            {

                var window = App.Current.Windows.OfType<MetroWindow>().FirstOrDefault(x => x.IsActive);
                if (window != null)
                {
                    await window.ShowMessageAsync(systemref, message, MessageDialogStyle.Affirmative);
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public async Task<MessageDialogResult> SystemMessageDialogYesNoCancel(string systemref, string message, string dialogStyle)
        {
            MessageDialogResult result = new MessageDialogResult();

            try
            {
                //Usage Example
                //vm.SystemMessageDialog("Nebula Notification", "Please enter a full search entry", "Standard");
                //vm.SystemMessageDialog("Nebula Notification", "Please enter a full search entry", YesNo");
                //Enum values
                //Canceled = -1,
                //Negative = 0,
                //Affirmative = 1,
                //FirstAuxiliary = 2,
                //SecondAuxiliary = 3

                var window = App.Current.Windows.OfType<MetroWindow>().FirstOrDefault(x => x.IsActive);
                if (window != null)
                {

                    switch (dialogStyle)
                    {
                        case "Standard":
                            result = await window.ShowMessageAsync(systemref, message, MessageDialogStyle.Affirmative);
                            break;
                        case "YesNo":
                            result = await window.ShowMessageAsync(systemref, message, MessageDialogStyle.AffirmativeAndNegative);
                            break;
                        case "YesNoCancel":
                            result = await window.ShowMessageAsync(systemref, message, MessageDialogStyle.AffirmativeAndNegativeAndDoubleAuxiliary);
                            break;
                    }


                    return result;
                }
                else
                {
                    return result;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

                return result;
            }
        }


        //PROPERTIES
        //List property to hold Gen10 Reports
        private List<string> _Gen10reportFiles;

        public List<string> Gen10reportFiles
        {
            get { return _Gen10reportFiles; }
            set
            {
                _Gen10reportFiles = value;
                OnPropertyChanged("Gen10reportFiles");
            }
        }

        //Product Code List
        private List<string> _ProductCodes;

        public List<string> ProductCodes
        {
            get { return _ProductCodes; }
            set
            {
                _ProductCodes = value;
                OnPropertyChanged("ProductCodes");
            }
        }


        //private static string _ServerChassisSerial;

        //public static string ServerChassisSerial
        //{
        //    get { return _ServerChassisSerial; }
        //    set
        //    {
        //        _ServerChassisSerial = value;
        //        OnPropertyChanged("ServerChassisSerial");
        //    }
        //}
        //to hold the report files required



        //year for the report window
        public string TodaysDate { get; set; }

        //year for the report window
        public string WhichYear { get; set; }


        //Weekly Server Report Properties

        private WeekCommencing _WeekComm;

        public WeekCommencing WeekComm
        {
            get { return _WeekComm; }
            set
            {
                _WeekComm = value;
                OnPropertyChanged("WeekComm");
            }
        }


        //Observable Collection To Hold Text Suggestions for AutoComplete Textbox
        private ObservableCollection<string> _TextSuggestion = new ObservableCollection<string>();
        public ObservableCollection<string> TextSuggestion
        {
            get { return _TextSuggestion; }
            set
            {
                _TextSuggestion = value;
                OnPropertyChanged("TextSuggestion");
            }
        }


        private string _CurrentFilter;

        public string CurrentFilter
        {
            get { return _CurrentFilter; }
            set
            {
                _CurrentFilter = value;
                OnPropertyChanged("CurrentFilter");
            }
        }

        //For Test Department Reports
        private string _RecordsTotal;

        public string RecordsTotal
        {
            get { return _RecordsTotal; }
            set
            {
                _RecordsTotal = value;
                OnPropertyChanged("RecordsTotal");
            }
        }



        //For Test Department Reports
        private string _ReportingSearchText;

        public string ReportingSearchText
        {
            get { return _ReportingSearchText; }
            set
            {
                _ReportingSearchText = value;
                OnPropertyChanged("ReportingSearchText");
            }
        }


        //SERVERS PROCESSED VIEW For search in Servers Processed
        private string _ServersProcessedSearch;

        public string ServersProcessedSearch
        {
            get { return _ServersProcessedSearch; }
            set
            {
                _ServersProcessedSearch = value;
                OnPropertyChanged("ServersProcessedSearch");
            }
        }

        private string _ServersProcessedCount;

        public string ServersProcessedCount
        {
            get { return _ServersProcessedCount; }
            set
            {
                _ServersProcessedCount = value;
                OnPropertyChanged("ServersProcessedCount");
            }
        }

        private bool _ServersProcessedDateSearch;

        public bool ServersProcessedDateSearch
        {
            get { return _ServersProcessedDateSearch; }
            set
            {
                _ServersProcessedDateSearch = value;
                OnPropertyChanged("ServersProcessedDateSearch");
            }
        }


        private bool _ProgressIsActive1;

        public bool ProgressIsActive1
        {
            get { return _ProgressIsActive1; }
            set
            {
                _ProgressIsActive1 = value;
                OnPropertyChanged("ProgressIsActive1");
            }
        }

        private Visibility _ProgressVisibility1;

        public Visibility ProgressVisibility1
        {
            get { return _ProgressVisibility1; }
            set
            {
                _ProgressVisibility1 = value;
                OnPropertyChanged("ProgressVisibility1");
            }
        }


        //TOTALS FOR WEEKLY SERVER RECORDS  

        private int _BuildTotals;

        public int BuildTotals
        {
            get { return _BuildTotals; }
            set
            {
                _BuildTotals = value;
                OnPropertyChanged("BuildTotals");
            }
        }

        private int _CTOTotals;

        public int CTOTotals
        {
            get { return _CTOTotals; }
            set
            {
                _CTOTotals = value;
                OnPropertyChanged("CTOTotals");
            }
        }

        private int _MotherboardTotals;

        public int MotherboardTotals
        {
            get { return _MotherboardTotals; }
            set
            {
                _MotherboardTotals = value;
                OnPropertyChanged("MotherboardTotals");
            }
        }

        private int _POTestingTotals;

        public int POTestingTotals
        {
            get { return _POTestingTotals; }
            set
            {
                _POTestingTotals = value;
                OnPropertyChanged("POTestingTotals");
            }
        }

        private int _PartTestingTotals;

        public int PartTestingTotals
        {
            get { return _PartTestingTotals; }
            set
            {
                _PartTestingTotals = value;
                OnPropertyChanged("PartTestingTotals");
            }
        }

        private int _RMATotals;

        public int RMATotals
        {
            get { return _RMATotals; }
            set
            {
                _RMATotals = value;
                OnPropertyChanged("RMATotals");
            }
        }

        private int _ClientGearTotals;

        public int ClientGearTotals
        {
            get { return _ClientGearTotals; }
            set
            {
                _ClientGearTotals = value;
                OnPropertyChanged("ClientGearTotals");
            }
        }

        //General Count on Front Screen
        private int _WeeklyServerTotal;

        public int WeeklyServerTotal
        {
            get { return _WeeklyServerTotal; }
            set
            {
                _WeeklyServerTotal = value;
                OnPropertyChanged("WeeklyServerTotal");
            }
        }

        //END TOTALS WeeklyServerTotal

        private bool _PurchaseOrdersOnly;

        public bool PurchaseOrdersOnly
        {
            get { return _PurchaseOrdersOnly; }
            set
            {
                _PurchaseOrdersOnly = value;
                OnPropertyChanged("PurchaseOrdersOnly");
            }
        }

        private string _Day;

        public string Day
        {
            get { return _Day; }
            set
            {
                _Day = value;
                OnPropertyChanged("Day");
            }
        }

        private string _SONumber;

        public string SONumber
        {
            get { return _SONumber; }
            set
            {
                _SONumber = value;
                OnPropertyChanged("SONumber");
            }
        }

        private string _ServerType;

        public string ServerType
        {
            get { return _ServerType; }
            set
            {
                _ServerType = value;
                OnPropertyChanged("ServerType");
            }
        }

        private int _BuildQuantity;

        public int BuildQuantity
        {
            get { return _BuildQuantity; }
            set
            {
                _BuildQuantity = value;
                OnPropertyChanged("BuildQuantity");
            }
        }

        private int _BuildFWQuantity;

        public int BuildFWQuantity
        {
            get { return _BuildFWQuantity; }
            set
            {
                _BuildFWQuantity = value;
                OnPropertyChanged("BuildFWQuantity");
            }
        }

        private int _BuildTestQuantity;

        public int BuildTestQuantity
        {
            get { return _BuildTestQuantity; }
            set
            {
                _BuildTestQuantity = value;
                OnPropertyChanged("BuildTestQuantity");
            }
        }


        private int _CPUTestQuantity;

        public int CPUTestQuantity
        {
            get { return _CPUTestQuantity; }
            set
            {
                _CPUTestQuantity = value;
                OnPropertyChanged("CPUTestQuantity");
            }
        }


        private int _CPUZQuantity;

        public int CPUZQuantity
        {
            get { return _CPUZQuantity; }
            set
            {
                _CPUZQuantity = value;
                OnPropertyChanged("CPUZQuantity");
            }
        }


        private int _CTOQuantity;

        public int CTOQuantity
        {
            get { return _CTOQuantity; }
            set
            {
                _CTOQuantity = value;
                OnPropertyChanged("CTOQuantity");
            }
        }

        private int _DIMMTestQuantity;

        public int DIMMTestQuantity
        {
            get { return _DIMMTestQuantity; }
            set
            {
                _DIMMTestQuantity = value;
                OnPropertyChanged("DIMMTestQuantity");
            }
        }


        private int _IOBoardQuantity;

        public int IOBoardQuantity
        {
            get { return _IOBoardQuantity; }
            set
            {
                _IOBoardQuantity = value;
                OnPropertyChanged("IOBoardQuantity");
            }
        }


        private int _MotherboardQuantity;

        public int MotherboardQuantity
        {
            get { return _MotherboardQuantity; }
            set
            {
                _MotherboardQuantity = value;
                OnPropertyChanged("MotherboardQuantity");
            }
        }


        private int _RAMReplaceQuantity;

        public int RAMReplaceQuantity
        {
            get { return _RAMReplaceQuantity; }
            set
            {
                _RAMReplaceQuantity = value;
                OnPropertyChanged("RAMReplaceQuantity");
            }
        }


        private int _RMAReplaceBuildQuantity;

        public int RMAReplaceBuildQuantity
        {
            get { return _RMAReplaceBuildQuantity; }
            set
            {
                _RMAReplaceBuildQuantity = value;
                OnPropertyChanged("RMAReplaceBuildQuantity");
            }
        }

        private int _PlugAndPlayQuantity;

        public int PlugAndPlayQuantity
        {
            get { return _PlugAndPlayQuantity; }
            set
            {
                _PlugAndPlayQuantity = value;
                OnPropertyChanged("PlugAndPlayQuantity");
            }
        }


        private int _POTestingQuantity;

        public int POTestingQuantity
        {
            get { return _POTestingQuantity; }
            set
            {
                _POTestingQuantity = value;
                OnPropertyChanged("POTestingQuantity");
            }
        }

        private string _SalesRep;

        public string SalesRep
        {
            get { return _SalesRep; }
            set
            {
                _SalesRep = value;
                OnPropertyChanged("SalesRep");
            }
        }

        private string _JobType;

        public string JobType
        {
            get { return _JobType; }
            set
            {
                _JobType = value;
                OnPropertyChanged("JobType");
            }
        }

        private string _Issues;

        public string Issues
        {
            get { return _Issues; }
            set
            {
                _Issues = value;
                OnPropertyChanged("Issues");
            }
        }

        private string _ActionTaken;

        public string ActionTaken
        {
            get { return _ActionTaken; }
            set
            {
                _ActionTaken = value;
                OnPropertyChanged("ActionTaken");
            }
        }

        //SERVERS PROCESSED PROPERTIES


        //Property for current windows user
        private string _CurrentUser;

        public string CurrentUser
        {
            get { return _CurrentUser; }
            set
            {
                _CurrentUser = value;
                OnPropertyChanged("CurrentUser");
            }
        }

        //Property for current windows user initials
        private string _CurrentUserInitials;

        public string CurrentUserInitials
        {
            get { return _CurrentUserInitials; }
            set
            {
                _CurrentUserInitials = value;
                OnPropertyChanged("CurrentUserInitials");
            }
        }

        //Property for current windows user
        private string _ReportUser;

        public string ReportUser
        {
            get { return _ReportUser; }
            set
            {
                _ReportUser = value;
                OnPropertyChanged("ReportUser");
            }
        }

        //Property for full windows user
        private string _FullUser;

        public string FullUser
        {
            get { return _FullUser; }
            set
            {
                _FullUser = value;
                OnPropertyChanged("FullUser");
            }
        }

        //Property for current windows user
        private string _CurrentUserLocation;

        public string CurrentUserLocation
        {
            get { return _CurrentUserLocation; }
            set
            {
                _CurrentUserLocation = value;
                OnPropertyChanged("CurrentUser");
            }
        }

        //Command Output Property
        //to hold recieved data from a process call
        private string _CMDOutput;

        public string CMDOutput
        {
            get { return _CMDOutput; }
            set
            {
                _CMDOutput = value;
                OnPropertyChanged("CMDOutput");
            }
        }



        //properties to hold checklist
        private string _ChecklistMessage;

        public string ChecklistMessage
        {
            get { return _ChecklistMessage; }
            set
            {
                _ChecklistMessage = value;
                OnPropertyChanged("ChecklistMessage");
            }
        }

        private string _ChecklistMessage1;

        public string ChecklistMessage1
        {
            get { return _ChecklistMessage1; }
            set
            {
                _ChecklistMessage1 = value;
                OnPropertyChanged("ChecklistMessage1");
            }
        }

        private string _ChecklistMessage2;

        public string ChecklistMessage2
        {
            get { return _ChecklistMessage2; }
            set
            {
                _ChecklistMessage2 = value;
                OnPropertyChanged("ChecklistMessage2");
            }
        }

        private string _ChecklistMessage3;

        public string ChecklistMessage3
        {
            get { return _ChecklistMessage3; }
            set
            {
                _ChecklistMessage3 = value;
                OnPropertyChanged("ChecklistMessage3");
            }
        }

        private string _ChecklistMessage4;

        public string ChecklistMessage4
        {
            get { return _ChecklistMessage4; }
            set
            {
                _ChecklistMessage4 = value;
                OnPropertyChanged("ChecklistMessage4");
            }
        }

        private string _ChecklistMessage5;

        public string ChecklistMessage5
        {
            get { return _ChecklistMessage5; }
            set
            {
                _ChecklistMessage5 = value;
                OnPropertyChanged("ChecklistMessage5");
            }
        }


        private string _ChecklistMessage6;

        public string ChecklistMessage6
        {
            get { return _ChecklistMessage6; }
            set
            {
                _ChecklistMessage6 = value;
                OnPropertyChanged("ChecklistMessage6");
            }
        }


        private string _ChecklistMessage7;

        public string ChecklistMessage7
        {
            get { return _ChecklistMessage7; }
            set
            {
                _ChecklistMessage7 = value;
                OnPropertyChanged("ChecklistMessage7");
            }
        }


        private string _ChecklistMessage8;

        public string ChecklistMessage8
        {
            get { return _ChecklistMessage8; }
            set
            {
                _ChecklistMessage8 = value;
                OnPropertyChanged("ChecklistMessage8");
            }
        }


        private string _ChecklistMessage9;

        public string ChecklistMessage9
        {
            get { return _ChecklistMessage9; }
            set
            {
                _ChecklistMessage9 = value;
                OnPropertyChanged("ChecklistMessage9");
            }
        }


        private string _ChecklistMessage10;

        public string ChecklistMessage10
        {
            get { return _ChecklistMessage10; }
            set
            {
                _ChecklistMessage10 = value;
                OnPropertyChanged("ChecklistMessage10");
            }
        }


        private string _ChecklistMessage11;

        public string ChecklistMessage11
        {
            get { return _ChecklistMessage11; }
            set
            {
                _ChecklistMessage11 = value;
                OnPropertyChanged("ChecklistMessage11");
            }
        }


        private string _ChecklistMessage12;

        public string ChecklistMessage12
        {
            get { return _ChecklistMessage12; }
            set
            {
                _ChecklistMessage12 = value;
                OnPropertyChanged("ChecklistMessage12");
            }
        }


        private string _ChecklistMessage13;

        public string ChecklistMessage13
        {
            get { return _ChecklistMessage13; }
            set
            {
                _ChecklistMessage13 = value;
                OnPropertyChanged("ChecklistMessage13");
            }
        }


        private string _ChecklistMessage14;

        public string ChecklistMessage14
        {
            get { return _ChecklistMessage14; }
            set
            {
                _ChecklistMessage14 = value;
                OnPropertyChanged("ChecklistMessage14");
            }
        }


        private string _ChecklistMessage15;

        public string ChecklistMessage15
        {
            get { return _ChecklistMessage15; }
            set
            {
                _ChecklistMessage15 = value;
                OnPropertyChanged("ChecklistMessage15");
            }
        }


        private string _ChecklistMessage16;

        public string ChecklistMessage16
        {
            get { return _ChecklistMessage16; }
            set
            {
                _ChecklistMessage16 = value;
                OnPropertyChanged("ChecklistMessage16");
            }


        }

        //Property for flow document in Server/Blade Checklist view 
        private FlowDocument _ChecklistFlowDoc;

        public FlowDocument ChecklistFlowDoc
        {
            get { return _ChecklistFlowDoc; }
            set
            {
                _ChecklistFlowDoc = value;
                OnPropertyChanged("ChecklistFlowDoc");
            }
        }



        //Property for current loaded Server/Blade Checklist
        private ServerBladeChecklist _LoadedChecklist;

        public ServerBladeChecklist LoadedChecklist
        {
            get { return _LoadedChecklist; }
            set
            {
                _LoadedChecklist = value;
                OnPropertyChanged("LoadedChecklist");
            }
        }



        //SERVER PROCESSING

        //Command Output Property
        //to hold recieved data from a process call
        private string _ServerState;

        public string ServerState
        {
            get { return _ServerState; }
            set
            {
                _ServerState = value;
                OnPropertyChanged("ServerState");
            }
        }


        //TO HOLD INSTANCE OF SELECTED ITEM FROM LIST VIEW

        //Quality Control Properties


        private string _Operative;

        public string Operative
        {
            get { return _Operative; }
            set
            {
                _Operative = value;
                OnPropertyChanged("Operative");
            }
        }

        private bool _IsQC;

        public bool IsQC
        {
            get { return _IsQC; }
            set
            {
                _IsQC = value;
                OnPropertyChanged("IsQC");
            }
        }


        //TOTALS FOR WEEKLY SERVER RECORDS

        private int _BentPinsTotals;

        public int BentPinsTotals
        {
            get { return _BentPinsTotals; }
            set
            {
                _BentPinsTotals = value;
                OnPropertyChanged("BentPinsTotals");
            }
        }

        private int _FirmwareTotals;

        public int FirmwareTotals
        {
            get { return _FirmwareTotals; }
            set
            {
                _FirmwareTotals = value;
                OnPropertyChanged("FirmwareTotals");
            }
        }

        private int _WrongPNTotals;

        public int WrongPNTotals
        {
            get { return _WrongPNTotals; }
            set
            {
                _WrongPNTotals = value;
                OnPropertyChanged("WrongPNTotals");
            }
        }

        private int _DirtyTotals;

        public int DirtyTotals
        {
            get { return _DirtyTotals; }
            set
            {
                _DirtyTotals = value;
                OnPropertyChanged("DirtyTotals");
            }
        }

        private int _MissingPaperWorkTotals;

        public int MissingPaperWorkTotals
        {
            get { return _MissingPaperWorkTotals; }
            set
            {
                _MissingPaperWorkTotals = value;
                OnPropertyChanged("MissingPaperWorkTotals");
            }
        }

        private int _MissingPartsTotals;

        public int MissingPartsTotals
        {
            get { return _MissingPartsTotals; }
            set
            {
                _MissingPartsTotals = value;
                OnPropertyChanged("MissingPartsTotals");
            }
        }

        private int _ExtraPartsTotals;

        public int ExtraPartsTotals
        {
            get { return _ExtraPartsTotals; }
            set
            {
                _ExtraPartsTotals = value;
                OnPropertyChanged("ExtraPartsTotals");
            }
        }

        private int _ILOSwitchTotals;

        public int ILOSwitchTotals
        {
            get { return _ILOSwitchTotals; }
            set
            {
                _ILOSwitchTotals = value;
                OnPropertyChanged("ILOSwitchTotals");
            }
        }

        private int _DataHolderTotals;

        public int DataHolderTotals
        {
            get { return _DataHolderTotals; }
            set
            {
                _DataHolderTotals = value;
                OnPropertyChanged("DataHolderTotals");
            }
        }


        private int _DamagedTotals;

        public int DamagedTotals
        {
            get { return _DamagedTotals; }
            set
            {
                _DamagedTotals = value;
                OnPropertyChanged("DamagedTotals");
            }
        }

        private int _WrongCablingTotals;

        public int WrongCablingTotals
        {
            get { return _WrongCablingTotals; }
            set
            {
                _WrongCablingTotals = value;
                OnPropertyChanged("WrongCablingTotals");
            }
        }
        private int _StickersTotals;

        public int StickersTotals
        {
            get { return _StickersTotals; }
            set
            {
                _StickersTotals = value;
                OnPropertyChanged("StickersTotals");
            }
        }

        private int _NotBookedInTotals;

        public int NotBookedInTotals
        {
            get { return _NotBookedInTotals; }
            set
            {
                _NotBookedInTotals = value;
                OnPropertyChanged("NotBookedInTotals");
            }
        }

        private int _OtherTotals;

        public int OtherTotals
        {
            get { return _OtherTotals; }
            set
            {
                _OtherTotals = value;
                OnPropertyChanged("OtherTotals");
            }
        }



        //END TOTALS




        private Operatives _OperativeSelected;

        public Operatives OperativeSelected
        {
            get { return _OperativeSelected; }
            set
            {
                _OperativeSelected = value;
                OnPropertyChanged("OperativeSelected");
            }
        }

        private Operatives _QCOperativeSelected;

        public Operatives QCOperativeSelected
        {
            get { return _QCOperativeSelected; }
            set
            {
                _QCOperativeSelected = value;
                OnPropertyChanged("QCOperativeSelected");
            }
        }


        //Hold Serial QR
        private BitmapImage _TESTQRCodeGI;
        public BitmapImage TESTQRCodeGI
        {
            get { return _TESTQRCodeGI; }
            set
            {
                _TESTQRCodeGI = value;

                OnPropertyChanged("TESTQRCodeGI");
            }
        }

        //Hold Serial QR
        private BitmapImage _TESTQRCodeGO;
        public BitmapImage TESTQRCodeGO
        {
            get { return _TESTQRCodeGO; }
            set
            {
                _TESTQRCodeGO = value;

                OnPropertyChanged("TESTQRCodeGO");
            }
        }



        //Hold Image to attach if CLI is empty
        private BitmapImage _ImageAttach1;
        public BitmapImage ImageAttach1
        {
            get { return _ImageAttach1; }
            set
            {
                _ImageAttach1 = value;

                OnPropertyChanged("ImageAttach1");
            }
        }


        //Search value 
        private string _QCSearch;

        public string QCSearch
        {
            get { return _QCSearch; }
            set
            {
                _QCSearch = value;
                OnPropertyChanged("QCSearch");
            }
        }

        //Notification Message colour for Weekly Server Reports
        private Brush _NotificationMessageColour;

        public Brush NotificationMessageColour
        {
            get { return _NotificationMessageColour; }
            set
            {
                _NotificationMessageColour = value;
                OnPropertyChanged("NotificationMessageColour");
            }
        }

        //Notification Message for Weekly Server Reports
        private string _NotificationMessage;

        public string NotificationMessage
        {
            get { return _NotificationMessage; }
            set
            {
                _NotificationMessage = value;
                OnPropertyChanged("NotificationMessage");
            }
        }

        private QCRecordsModel _AddQCRecord;

        public QCRecordsModel AddQCRecord
        {
            get { return _AddQCRecord; }
            set
            {
                _AddQCRecord = value;
                OnPropertyChanged("AddQCRecord");
            }
        }

        private QCRecordsModel _UpdateQCRecord;
        public QCRecordsModel UpdateQCRecord
        {
            get { return _UpdateQCRecord; }
            set
            {
                _UpdateQCRecord = value;
                OnPropertyChanged("UpdateQCRecord");
            }
        }




        //TO HOLD INSTANCE OF SELECTED ITEM FROM LIST VIEW
        private WeeklyServerRecordsModel _AddWeeklyRecord;

        public WeeklyServerRecordsModel AddWeeklyRecord
        {
            get { return _AddWeeklyRecord; }
            set
            {
                _AddWeeklyRecord = value;
                OnPropertyChanged("AddWeeklyRecord");
            }
        }




        //TO HOLD INSTANCE OF SELECTED ITEM FROM LIST VIEW
        private WeeklyServerRecordsModel _UpdateWeeklyRecord;

        public WeeklyServerRecordsModel UpdateWeeklyRecord
        {
            get { return _UpdateWeeklyRecord; }
            set
            {
                _UpdateWeeklyRecord = value;
                OnPropertyChanged("UpdateWeeklyRecord");
            }
        }


        //DATE FROM
        private DateTime _DateFrom;

        public DateTime DateFrom
        {
            get { return _DateFrom; }
            set
            {
                _DateFrom = value;
                OnPropertyChanged("DateFrom");
            }
        }

        //DATE TO
        private DateTime _DateTo;

        public DateTime DateTo
        {
            get { return _DateTo; }
            set
            {
                _DateTo = value;
                OnPropertyChanged("DateTo");
            }
        }




        public void CheckILOCurrentState(string ServerIP)
        {
            ProcessPiper pp = new ProcessPiper();

            //FOR ILO RESTFUL
            // ilo rest full path  C:\Program Files\Hewlett Packard Enterprise\RESTful Interface Tool\ilorest.exe  bootorder biosdefaults
            //pp.StartILORest(new System.IO.FileInfo(@"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "login 192.168.101.34 -u username -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
            pp.StartILORest1(new System.IO.FileInfo(@"" + myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "serverstatus --url " + ServerIP + " -u username -p " + OverridePasswordS1.Trim() + "", @"" + myDocs + @"\HPTOOLS\RESTful Interface Tool");
        }


        //LOAD CONTENT PROP

        //private UserControl _loadContent;

        //public UserControl LoadContent
        //{
        //    get { return _loadContent; }
        //    set
        //    {
        //        _loadContent = value;
        //        OnPropertyChanged("LoadContent");
        //    }
        //}

        public void ClearChecklistControls()
        {
            ChassisSerialNo = String.Empty;
            GoodsINQ1 = false;
            GoodsINQ2 = false;
            GoodsINQ3 = false;
            GoodsINQ4 = false;
            GoodsINQ5 = false;
            GoodsINQ6 = false;
            GoodsINQ7 = false;
            GoodsINQ8 = false;
            GoodsINQ9 = false;
            GoodsINQ10 = false;
            GoodsINQ11 = false;
            GoodsINQ12 = false;
            GoodsINQ13 = false;
            GoodsINQ14 = false;
            GoodsINQ15 = false;
            GoodsINQ16TXT = String.Empty;
            GoodsINQ17 = false;
            GoodsINQ18 = false;
            GoodsINQ19 = false;
            GoodsINQ20 = false;
            GoodsINQ21 = false;
            GoodsINQ22 = false;
            GoodsINQ23 = false;
            GoodsINQ24 = false;
            GoodsINQ25 = false;
            PurchaseOrderNumber = String.Empty;
            GoodsINRep = String.Empty;
            GoodsOUTQ1 = false;
            GoodsOUTQ2 = false;
            GoodsOUTQ3 = false;
            GoodsOUTQ4 = false;
            GoodsOUTQ5 = false;
            GoodsOUTQ6 = false;
            GoodsOUTQ7 = false;
            GoodsOUTQ8 = false;
            GoodsOUTQ9 = false;
            GoodsOUTQ10 = false;
            GoodsOUTQ11 = false;
            GoodsOUTQ12 = false;
            GoodsOUTQ13 = false;
            GoodsOUTQ14 = false;
            GoodsOUTQ15 = false;
            GoodsOUTQ16 = false;
            GoodsOUTQ17 = false;
            GoodsOUTQ18 = false;
            GoodsOUTQ19 = false;
            GoodsOUTQ20 = false;
            GoodsOUTQ21 = false;
            GoodsOUTQ22 = false;
            GoodsOUTQ23 = false;
            GoodsOUTQ24 = false;
            GoodsOUTQ25 = false;
            SalesOrderNumber = String.Empty;
            GoodsINRep = String.Empty;
            GoodsOUTRep = String.Empty;
            LoadedChecklist = null;

        }


        //LOAD WINDOW PROPERTY SET WHICH WINDOW TO LOAD NEXT. ACCESS VIA VM.
        private MetroWindow _loadWindow;

        public MetroWindow LoadWindow
        {
            get { return _loadWindow; }
            set
            {
                _loadWindow = value;
                OnPropertyChanged("LoadWindow");
            }
        }

        private UserControl _loadReport;

        public UserControl LoadReport
        {
            get { return _loadReport; }
            set
            {
                _loadReport = value;
                OnPropertyChanged("LoadReport");
            }
        }

        //Checklist Properties

        private string _DocumentsSavePath;

        public string DocumentsSavePath
        {
            get { return _DocumentsSavePath; }
            set
            {
                _DocumentsSavePath = value;

                OnPropertyChanged("DocumentsSavePath");
            }
        }

        private string _ChassisSerialNo;

        public string ChassisSerialNo
        {
            get { return _ChassisSerialNo; }
            set
            {
                _ChassisSerialNo = value;

                OnPropertyChanged("ChassisSerialNo");
            }
        }

        //public static string _ChassisSerialNoPassthrough;


        private string _OverridePasswordS1;

        public string OverridePasswordS1
        {
            get { return _OverridePasswordS1; }
            set
            {
                _OverridePasswordS1 = value;
                OnPropertyChanged("OverridePasswordS1");
            }
        }

        //GOODS IN AND TEST
        private bool _GoodsINQ1;
        public bool GoodsINQ1
        {
            get { return _GoodsINQ1; }
            set
            {
                _GoodsINQ1 = value;
                OnPropertyChanged("GoodsINQ1");
            }
        }

        private bool _GoodsINQ2;
        public bool GoodsINQ2
        {
            get { return _GoodsINQ2; }
            set
            {
                _GoodsINQ2 = value;
                OnPropertyChanged("GoodsINQ2");
            }
        }

        private bool _GoodsINQ3;
        public bool GoodsINQ3
        {
            get { return _GoodsINQ3; }
            set
            {
                _GoodsINQ3 = value;
                OnPropertyChanged("GoodsINQ3");
            }
        }

        private bool _GoodsINQ4;
        public bool GoodsINQ4
        {
            get { return _GoodsINQ4; }
            set
            {
                _GoodsINQ4 = value;
                OnPropertyChanged("GoodsINQ4");
            }
        }

        private bool _GoodsINQ5;
        public bool GoodsINQ5
        {
            get { return _GoodsINQ5; }
            set
            {
                _GoodsINQ5 = value;
                OnPropertyChanged("GoodsINQ5");
            }
        }

        private bool _GoodsINQ6;
        public bool GoodsINQ6
        {
            get { return _GoodsINQ6; }
            set
            {
                _GoodsINQ6 = value;
                OnPropertyChanged("GoodsINQ6");
            }
        }

        private bool _GoodsINQ7;
        public bool GoodsINQ7
        {
            get { return _GoodsINQ7; }
            set
            {
                _GoodsINQ7 = value;
                OnPropertyChanged("GoodsINQ7");
            }
        }

        private bool _GoodsINQ8;
        public bool GoodsINQ8
        {
            get { return _GoodsINQ8; }
            set
            {
                _GoodsINQ8 = value;
                OnPropertyChanged("GoodsINQ8");
            }
        }

        private bool _GoodsINQ9;
        public bool GoodsINQ9
        {
            get { return _GoodsINQ9; }
            set
            {
                _GoodsINQ9 = value;
                OnPropertyChanged("GoodsINQ9");
            }
        }
        private bool _GoodsINQ10;
        public bool GoodsINQ10
        {
            get { return _GoodsINQ10; }
            set
            {
                _GoodsINQ10 = value;
                OnPropertyChanged("GoodsINQ10");
            }
        }

        private bool _GoodsINQ11;
        public bool GoodsINQ11
        {
            get { return _GoodsINQ11; }
            set
            {
                _GoodsINQ11 = value;
                OnPropertyChanged("GoodsINQ11");
            }
        }

        private bool _GoodsINQ12;
        public bool GoodsINQ12
        {
            get { return _GoodsINQ12; }
            set
            {
                _GoodsINQ12 = value;
                OnPropertyChanged("GoodsINQ12");
            }
        }

        private bool _GoodsINQ13;
        public bool GoodsINQ13
        {
            get { return _GoodsINQ13; }
            set
            {
                _GoodsINQ13 = value;
                OnPropertyChanged("GoodsINQ13");
            }
        }

        private bool _GoodsINQ14;
        public bool GoodsINQ14
        {
            get { return _GoodsINQ14; }
            set
            {
                _GoodsINQ14 = value;
                OnPropertyChanged("GoodsINQ14");
            }
        }

        private bool _GoodsINQ15;
        public bool GoodsINQ15
        {
            get { return _GoodsINQ15; }
            set
            {
                _GoodsINQ15 = value;
                OnPropertyChanged("GoodsINQ15");
            }
        }
        //TEXTBOX
        private string _GoodsINQ16TXT;
        public string GoodsINQ16TXT
        {
            get { return _GoodsINQ16TXT; }
            set
            {
                _GoodsINQ16TXT = value;
                OnPropertyChanged("GoodsINQ16TXT");
            }
        }

        private bool _GoodsINQ17;
        public bool GoodsINQ17
        {
            get { return _GoodsINQ17; }
            set
            {
                _GoodsINQ17 = value;
                OnPropertyChanged("GoodsINQ17");
            }
        }

        private bool _GoodsINQ18;
        public bool GoodsINQ18
        {
            get { return _GoodsINQ18; }
            set
            {
                _GoodsINQ18 = value;
                OnPropertyChanged("GoodsINQ18");
            }
        }

        private bool _GoodsINQ19;
        public bool GoodsINQ19
        {
            get { return _GoodsINQ19; }
            set
            {
                _GoodsINQ19 = value;
                OnPropertyChanged("GoodsINQ19");
            }
        }

        private bool _GoodsINQ20;
        public bool GoodsINQ20
        {
            get { return _GoodsINQ20; }
            set
            {
                _GoodsINQ20 = value;
                OnPropertyChanged("GoodsINQ20");
            }
        }

        private bool _GoodsINQ21;
        public bool GoodsINQ21
        {
            get { return _GoodsINQ21; }
            set
            {
                _GoodsINQ21 = value;
                OnPropertyChanged("GoodsINQ21");
            }
        }

        private bool _GoodsINQ22;
        public bool GoodsINQ22
        {
            get { return _GoodsINQ22; }
            set
            {
                _GoodsINQ22 = value;
                OnPropertyChanged("GoodsINQ22");
            }
        }

        private bool _GoodsINQ23;
        public bool GoodsINQ23
        {
            get { return _GoodsINQ23; }
            set
            {
                _GoodsINQ23 = value;
                OnPropertyChanged("GoodsINQ23");
            }
        }

        private bool _GoodsINQ24;
        public bool GoodsINQ24
        {
            get { return _GoodsINQ24; }
            set
            {
                _GoodsINQ24 = value;
                OnPropertyChanged("GoodsINQ24");
            }
        }

        private bool _GoodsINQ25;
        public bool GoodsINQ25
        {
            get { return _GoodsINQ25; }
            set
            {
                _GoodsINQ25 = value;
                OnPropertyChanged("GoodsINQ25");
            }
        }




        private string _GoodsINRep;

        public string GoodsINRep
        {
            get { return _GoodsINRep; }
            set
            {
                _GoodsINRep = value;
                OnPropertyChanged("GoodsINRep");
            }
        }


        private string _PurchaseOrderNumber;

        public string PurchaseOrderNumber
        {
            get { return _PurchaseOrderNumber; }
            set
            {
                _PurchaseOrderNumber = value;
                OnPropertyChanged("PurchaseOrderNumber");
            }
        }


        public string DateCheck { get; set; }

        //Goods Out

        private bool _GoodsOUTQ1;
        public bool GoodsOUTQ1
        {
            get { return _GoodsOUTQ1; }
            set
            {
                _GoodsOUTQ1 = value;
                OnPropertyChanged("GoodsOUTQ1");
            }
        }
        private bool _GoodsOUTQ2;
        public bool GoodsOUTQ2
        {
            get { return _GoodsOUTQ2; }
            set
            {
                _GoodsOUTQ2 = value;
                OnPropertyChanged("GoodsOUTQ2");
            }
        }
        private bool _GoodsOUTQ3;
        public bool GoodsOUTQ3
        {
            get { return _GoodsOUTQ3; }
            set
            {
                _GoodsOUTQ3 = value;
                OnPropertyChanged("GoodsOUTQ3");
            }
        }


        private bool _GoodsOUTQ4;
        public bool GoodsOUTQ4
        {
            get { return _GoodsOUTQ4; }
            set
            {
                _GoodsOUTQ4 = value;
                OnPropertyChanged("GoodsOUTQ4");
            }
        }

        private bool _GoodsOUTQ5;
        public bool GoodsOUTQ5
        {
            get { return _GoodsOUTQ5; }
            set
            {
                _GoodsOUTQ5 = value;
                OnPropertyChanged("GoodsOUTQ5");
            }
        }

        private bool _GoodsOUTQ6;
        public bool GoodsOUTQ6
        {
            get { return _GoodsOUTQ6; }
            set
            {
                _GoodsOUTQ6 = value;
                OnPropertyChanged("GoodsOUTQ6");
            }
        }

        private bool _GoodsOUTQ7;
        public bool GoodsOUTQ7
        {
            get { return _GoodsOUTQ7; }
            set
            {
                _GoodsOUTQ7 = value;
                OnPropertyChanged("GoodsOUTQ7");
            }
        }

        private bool _GoodsOUTQ8;
        public bool GoodsOUTQ8
        {
            get { return _GoodsOUTQ8; }
            set
            {
                _GoodsOUTQ8 = value;
                OnPropertyChanged("GoodsOUTQ8");
            }
        }

        private bool _GoodsOUTQ9;
        public bool GoodsOUTQ9
        {
            get { return _GoodsOUTQ9; }
            set
            {
                _GoodsOUTQ9 = value;
                OnPropertyChanged("GoodsOUTQ9");
            }
        }

        private bool _GoodsOUTQ10;
        public bool GoodsOUTQ10
        {
            get { return _GoodsOUTQ10; }
            set
            {
                _GoodsOUTQ10 = value;
                OnPropertyChanged("GoodsOUTQ10");
            }
        }

        private bool _GoodsOUTQ11;
        public bool GoodsOUTQ11
        {
            get { return _GoodsOUTQ11; }
            set
            {
                _GoodsOUTQ11 = value;
                OnPropertyChanged("GoodsOUTQ11");
            }
        }

        private bool _GoodsOUTQ12;
        public bool GoodsOUTQ12
        {
            get { return _GoodsOUTQ12; }
            set
            {
                _GoodsOUTQ12 = value;
                OnPropertyChanged("GoodsOUTQ12");
            }
        }

        private bool _GoodsOUTQ13;
        public bool GoodsOUTQ13
        {
            get { return _GoodsOUTQ13; }
            set
            {
                _GoodsOUTQ13 = value;
                OnPropertyChanged("GoodsOUTQ13");
            }
        }

        private bool _GoodsOUTQ14;
        public bool GoodsOUTQ14
        {
            get { return _GoodsOUTQ14; }
            set
            {
                _GoodsOUTQ14 = value;
                OnPropertyChanged("GoodsOUTQ14");
            }
        }

        private bool _GoodsOUTQ15;
        public bool GoodsOUTQ15
        {
            get { return _GoodsOUTQ15; }
            set
            {
                _GoodsOUTQ15 = value;
                OnPropertyChanged("GoodsOUTQ15");
            }
        }

        private bool _GoodsOUTQ16;
        public bool GoodsOUTQ16
        {
            get { return _GoodsOUTQ16; }
            set
            {
                _GoodsOUTQ16 = value;
                OnPropertyChanged("GoodsOUTQ16");
            }
        }

        private bool _GoodsOUTQ17;
        public bool GoodsOUTQ17
        {
            get { return _GoodsOUTQ17; }
            set
            {
                _GoodsOUTQ17 = value;
                OnPropertyChanged("GoodsOUTQ17");
            }
        }

        private bool _GoodsOUTQ18;
        public bool GoodsOUTQ18
        {
            get { return _GoodsOUTQ18; }
            set
            {
                _GoodsOUTQ18 = value;
                OnPropertyChanged("GoodsOUTQ18");
            }
        }

        private bool _GoodsOUTQ19;
        public bool GoodsOUTQ19
        {
            get { return _GoodsOUTQ19; }
            set
            {
                _GoodsOUTQ19 = value;
                OnPropertyChanged("GoodsOUTQ19");
            }
        }

        private bool _GoodsOUTQ20;
        public bool GoodsOUTQ20
        {
            get { return _GoodsOUTQ20; }
            set
            {
                _GoodsOUTQ20 = value;
                OnPropertyChanged("GoodsOUTQ20");
            }
        }

        private bool _GoodsOUTQ21;
        public bool GoodsOUTQ21
        {
            get { return _GoodsOUTQ21; }
            set
            {
                _GoodsOUTQ21 = value;
                OnPropertyChanged("GoodsOUTQ21");
            }
        }

        private bool _GoodsOUTQ22;
        public bool GoodsOUTQ22
        {
            get { return _GoodsOUTQ22; }
            set
            {
                _GoodsOUTQ22 = value;
                OnPropertyChanged("GoodsOUTQ22");
            }
        }

        private bool _GoodsOUTQ23;
        public bool GoodsOUTQ23
        {
            get { return _GoodsOUTQ23; }
            set
            {
                _GoodsOUTQ23 = value;
                OnPropertyChanged("GoodsOUTQ23");
            }
        }

        private bool _GoodsOUTQ24;
        public bool GoodsOUTQ24
        {
            get { return _GoodsOUTQ24; }
            set
            {
                _GoodsOUTQ24 = value;
                OnPropertyChanged("GoodsOUTQ24");
            }
        }

        private bool _GoodsOUTQ25;
        public bool GoodsOUTQ25
        {
            get { return _GoodsOUTQ25; }
            set
            {
                _GoodsOUTQ25 = value;
                OnPropertyChanged("GoodsOUTQ25");
            }
        }



        //Property for current windows user
        private string _GoodsOUTRep;

        public string GoodsOUTRep
        {
            get { return _GoodsOUTRep; }
            set
            {
                _GoodsOUTRep = value;
                OnPropertyChanged("GoodsOUTRep");
            }
        }


        private string _SalesOrderNumber;

        public string SalesOrderNumber
        {
            get { return _SalesOrderNumber; }
            set
            {
                _SalesOrderNumber = value;
                OnPropertyChanged("SalesOrderNumber");
            }
        }


        private string _SalesOrderNumberPath;

        public string SalesOrderNumberPath
        {
            get { return _SalesOrderNumberPath; }
            set
            {
                _SalesOrderNumberPath = value;
                OnPropertyChanged("SalesOrderNumberPath");
            }
        }




        //Contains all items read from the annual csv file
        private ObservableCollection<ServersProcessedModel> _ServersProcessedCollection;

        public ObservableCollection<ServersProcessedModel> ServersProcessedCollection
        {
            get { return _ServersProcessedCollection; }
            set
            {
                _ServersProcessedCollection = value;
                OnPropertyChanged("ServersProcessedCollection");
            }
        }


        //Contains all items read from the annual csv file
        private ObservableCollection<ServersProcessedModel> _ServersInternalCollection;

        public ObservableCollection<ServersProcessedModel> ServersInternalCollection
        {
            get { return _ServersInternalCollection; }
            set
            {
                _ServersInternalCollection = value;
                OnPropertyChanged("ServersInternalCollection");
            }
        }

        // Weekly Server Records
        //Contains all items read from the annual csv file
        private ObservableCollection<WeeklyServerRecordsModel> _WeeklyServerRecordCollection;
        public ObservableCollection<WeeklyServerRecordsModel> WeeklyServerRecordCollection
        {
            get { return _WeeklyServerRecordCollection; }
            set
            {
                _WeeklyServerRecordCollection = value;
                OnPropertyChanged("WeeklyServerRecordCollection");
            }
        }

        // To hold internal collection
        private ObservableCollection<WeeklyServerRecordsModel> _WeeklyServerReportCollection;
        public ObservableCollection<WeeklyServerRecordsModel> WeeklyServerReportCollection
        {
            get { return _WeeklyServerReportCollection; }
            set
            {
                _WeeklyServerReportCollection = value;
                OnPropertyChanged("WeeklyServerReportCollection");
            }
        }


        //Quality Control Observable Collections
        private ObservableCollection<Operatives> _OperativeCollection;
        public ObservableCollection<Operatives> OperativeCollection
        {
            get { return _OperativeCollection; }
            set
            {
                _OperativeCollection = value;
                OnPropertyChanged("OperativeCollection");
            }
        }


        private ObservableCollection<Operatives> _QCOperativeCollection;
        public ObservableCollection<Operatives> QCOperativeCollection
        {
            get { return _QCOperativeCollection; }
            set
            {
                _QCOperativeCollection = value;
                OnPropertyChanged("QCOperativeCollection");
            }
        }

        private ObservableCollection<QCRecordsModel> _QCItems;
        public ObservableCollection<QCRecordsModel> QCItems
        {
            get { return _QCItems; }
            set
            {
                _QCItems = value;
                OnPropertyChanged("QCItems");

            }
        }





        //IP SCANNER PROPERTIES


        // Dns.GetHostName();

        private ObservableCollection<IPInfo> _IPResults;

        public ObservableCollection<IPInfo> IPResults
        {
            get { return _IPResults; }
            set
            {
                _IPResults = value;
                OnPropertyChanged("IPResults");
            }
        }

        //Start IP OF Search
        private string _StartingIP;
        public string StartingIP
        {
            get { return _StartingIP; }
            set
            {
                _StartingIP = value;
                OnPropertyChanged("StartingIP");
            }
        }
        //End IP OF Search
        private string _EndingIP;
        public string EndingIP
        {
            get { return _EndingIP; }
            set
            {
                _EndingIP = value;
                OnPropertyChanged("EndingIP");
            }
        }

        private string _ReturnedIP;
        public string ReturnedIP
        {
            get { return _ReturnedIP; }
            set
            {
                _ReturnedIP = value;
                OnPropertyChanged("ReturnedIP");
            }
        }

        //ITAD REPORT GENERATOR PROPERTRIES

        private string _ITADDate;
        public string ITADDate
        {
            get { return _ITADDate; }
            set
            {
                _ITADDate = value;

                OnPropertyChanged("ITADDate");
            }
        }

        private string _ITADSerialNo;
        public string ITADSerialNo
        {
            get { return _ITADSerialNo; }
            set
            {
                _ITADSerialNo = value;
                //_ITADSerialNo = "Serial No: " + value;
                ITADAssetSerialNo = _ITADPOSONo + "-" + _ITADAssetNo + "-" + _ITADSerialNo;
                OnPropertyChanged("ITADSerialNo");
            }
        }


        private string _TestSerialNo;
        public string TestSerialNo
        {
            get { return _TestSerialNo; }
            set
            {


                _TestSerialNo = value.ToUpper();


                OnPropertyChanged("TestSerialNo");
            }
        }

        private string _ITADAssetNo;
        public string ITADAssetNo
        {
            get { return _ITADAssetNo; }
            set
            {
                _ITADAssetNo = value;
                //_ITADAssetNo = "Asset No: " + value;
                ITADAssetSerialNo = _ITADPOSONo + "-" + _ITADAssetNo + "-" + _ITADSerialNo;
                OnPropertyChanged("ITADAssetNo");
            }
        }

        //ITAD REPORT GENERATOR PROPERTRIES

        private string _ITADAssetSerialNo;
        public string ITADAssetSerialNo
        {
            get { return _ITADAssetSerialNo; }
            set
            {
                _ITADAssetSerialNo = value;
                //_ITADAssetSerialNo = "Asset-Serial No: " + value;

                OnPropertyChanged("ITADAssetSerialNo");
            }
        }


        private string _ITADPOSONo;
        public string ITADPOSONo
        {
            get { return _ITADPOSONo; }
            set
            {
                _ITADPOSONo = value;
                ITADAssetSerialNo = _ITADPOSONo + "-" + _ITADAssetNo + "-" + _ITADSerialNo;
                OnPropertyChanged("ITADPOSONo");
            }
        }

        private string _TestPOSONo;
        public string TestPOSONo
        {
            get { return _TestPOSONo; }
            set
            {
                _TestPOSONo = value.ToUpper();

                OnPropertyChanged("TestPOSONo");
            }
        }

        private string _TestReportPOSONo;
        public string TestReportPOSONo
        {
            get { return _TestReportPOSONo; }
            set
            {
                _TestReportPOSONo = value.ToUpper();

                OnPropertyChanged("TestReportPOSONo");
            }
        }

        private bool _SaveToMyDocs;
        public bool SaveToMyDocs
        {
            get { return _SaveToMyDocs; }
            set
            {
                _SaveToMyDocs = value;

                OnPropertyChanged("SaveToMyDocs");
            }
        }

        private string _ITADManufacturer;
        public string ITADManufacturer
        {
            get { return _ITADManufacturer; }
            set
            {
                _ITADManufacturer = value;

                OnPropertyChanged("ITADManufacturer");
            }
        }

        private string _ITADModel;
        public string ITADModel
        {
            get { return _ITADModel; }
            set
            {
                _ITADModel = value;

                OnPropertyChanged("ITADModel");
            }
        }

        private string _ITADDeviceCategory;
        public string ITADDeviceCategory
        {
            get { return _ITADDeviceCategory; }
            set
            {
                _ITADDeviceCategory = value;

                OnPropertyChanged("ITADDeviceCategory");
            }
        }


        //Hold Serial QR
        private BitmapImage _ITADQRCode;
        public BitmapImage ITADQRCode
        {
            get { return _ITADQRCode; }
            set
            {
                _ITADQRCode = value;

                OnPropertyChanged("ITADQRCode");
            }
        }



        //FORM SELECTED ITEMS
        private ITADDeviceCategories _ITADDeviceCategorySelItem;
        public ITADDeviceCategories ITADDeviceCategorySelItem
        {
            get { return _ITADDeviceCategorySelItem; }
            set
            {
                _ITADDeviceCategorySelItem = value;

                OnPropertyChanged("ITADDeviceCategorySelItem");
            }
        }

        private ITADProcessesPerformed _ITADProcessPerformedSelItem;
        public ITADProcessesPerformed ITADProcessPerformedSelItem
        {
            get { return _ITADProcessPerformedSelItem; }
            set
            {
                _ITADProcessPerformedSelItem = value;

                OnPropertyChanged("ITADProcessPerformedSelItem");
            }
        }

        private ITADTestingPerformed _ITADTestingPerformedSelItem;
        public ITADTestingPerformed ITADTestingPerformedSelItem
        {
            get { return _ITADTestingPerformedSelItem; }
            set
            {
                _ITADTestingPerformedSelItem = value;

                OnPropertyChanged("ITADTestingPerformedSelItem");
            }
        }


        private ITADVerificationPerformed _ITADVerificationPerformedSelItem;
        public ITADVerificationPerformed ITADVerificationPerformedSelItem
        {
            get { return _ITADVerificationPerformedSelItem; }
            set
            {
                _ITADVerificationPerformedSelItem = value;

                OnPropertyChanged("ITADVerificationPerformedSelItem");
            }
        }

        //DELETE SELECTED ITEMS


        private ITADDeviceCategories _ITADDeviceCategorySelItemDel;
        public ITADDeviceCategories ITADDeviceCategorySelItemDel
        {
            get { return _ITADDeviceCategorySelItemDel; }
            set
            {
                _ITADDeviceCategorySelItemDel = value;

                OnPropertyChanged("ITADDeviceCategorySelItemDel");
            }
        }

        private ITADProcessesPerformed _ITADProcessPerformedSelItemDel;
        public ITADProcessesPerformed ITADProcessPerformedSelItemDel
        {
            get { return _ITADProcessPerformedSelItemDel; }
            set
            {
                _ITADProcessPerformedSelItemDel = value;

                OnPropertyChanged("ITADProcessPerformedSelItemDel");
            }
        }

        private ITADTestingPerformed _ITADTestingPerformedSelItemDel;
        public ITADTestingPerformed ITADTestingPerformedSelItemDel
        {
            get { return _ITADTestingPerformedSelItemDel; }
            set
            {
                _ITADTestingPerformedSelItemDel = value;

                OnPropertyChanged("ITADTestingPerformedSelItemDel");
            }
        }


        private ITADVerificationPerformed _ITADVerificationPerformedSelItemDel;
        public ITADVerificationPerformed ITADVerificationPerformedSelItemDel
        {
            get { return _ITADVerificationPerformedSelItemDel; }
            set
            {
                _ITADVerificationPerformedSelItemDel = value;

                OnPropertyChanged("ITADVerificationPerformedSelItemDel");
            }
        }


        private string _ITADProcessPerformed;
        public string ITADProcessPerformed
        {
            get { return _ITADProcessPerformed; }
            set
            {
                _ITADProcessPerformed = value;

                OnPropertyChanged("ITADProcessPerformed");
            }
        }

        private string _ITADTestingPerformed;
        public string ITADTestingPerformed
        {
            get { return _ITADTestingPerformed; }
            set
            {
                _ITADTestingPerformed = value;

                OnPropertyChanged("ITADTestingPerformed");
            }
        }

        private string _ITADVerificationPer;
        public string ITADVerificationPer
        {
            get { return _ITADVerificationPer; }
            set
            {
                _ITADVerificationPer = value;
                OnPropertyChanged("ITADVerificationPer");
            }
        }



        private string _ITADAddCategory;
        public string ITADAddCategory
        {
            get { return _ITADAddCategory; }
            set
            {
                _ITADAddCategory = value;

                OnPropertyChanged("ITADAddCategory");
            }
        }

        private string _ITADAddProcess;
        public string ITADAddProcess
        {
            get { return _ITADAddProcess; }
            set
            {
                _ITADAddProcess = value;

                OnPropertyChanged("ITADAddProcess");
            }
        }

        private string _ITADAddTesting;
        public string ITADAddTesting
        {
            get { return _ITADAddTesting; }
            set
            {
                _ITADAddTesting = value;

                OnPropertyChanged("ITADAddTesting");
            }
        }


        private string _ITADAddVerification;
        public string ITADAddVerification
        {
            get { return _ITADAddVerification; }
            set
            {
                _ITADAddVerification = value;

                OnPropertyChanged("ITADAddVerification");
            }
        }

        private string _ITADNotes;
        public string ITADNotes
        {
            get { return _ITADNotes; }
            set
            {
                _ITADNotes = value;
                OnPropertyChanged("ITADNotes");
            }
        }


        private string _ITADVerifiedBy;
        public string ITADVerifiedBy
        {
            get { return _ITADVerifiedBy; }
            set
            {
                _ITADVerifiedBy = value;
                OnPropertyChanged("ITADVerifiedBy");
            }
        }


        private string _ITADReportUserDate;
        public string ITADReportUserDate
        {
            get { return _ITADReportUserDate; }
            set
            {
                _ITADReportUserDate = value;
                OnPropertyChanged("ITADReportUserDate");
            }
        }


        private string _ITADVerification;
        public string ITADVerification
        {
            get { return _ITADVerification; }
            set
            {
                _ITADVerification = value;
                OnPropertyChanged("ITADVerification");
            }
        }

        private string _ITADDeclaration;
        public string ITADDeclaration
        {
            get { return _ITADDeclaration; }
            set
            {
                _ITADDeclaration = value;
                OnPropertyChanged("ITADDeclaration");
            }
        }

        private string _ITADDeclaration2;
        public string ITADDeclaration2
        {
            get { return _ITADDeclaration2; }
            set
            {
                _ITADDeclaration2 = value;
                OnPropertyChanged("ITADDeclaration2");
            }
        }

        private string _ITADTechConfirm;
        public string ITADTechConfirm
        {
            get { return _ITADTechConfirm; }
            set
            {
                _ITADTechConfirm = value;
                OnPropertyChanged("ITADTechConfirm");
            }
        }

        private string _ITADVerificationConfirm;
        public string ITADVerificationConfirm
        {
            get { return _ITADVerificationConfirm; }
            set
            {
                _ITADVerificationConfirm = value;
                OnPropertyChanged("ITADVerificationConfirm");
            }
        }






        private string _ITADCLIOutput;
        public string ITADCLIOutput
        {
            get { return _ITADCLIOutput; }
            set
            {
                _ITADCLIOutput = value;


                OnPropertyChanged("ITADCLIOutput");
            }
        }

        private string _TestCLIOutput;
        public string TestCLIOutput
        {
            get { return _TestCLIOutput; }
            set
            {


                _TestCLIOutput = value;



                OnPropertyChanged("TestCLIOutput");
            }
        }

        private string _ITADSavePath;
        public string ITADSavePath
        {
            get { return _ITADSavePath; }
            set
            {
                _ITADSavePath = value;

                OnPropertyChanged("ITADSavePath");
            }
        }

        private bool _ITADIsReprint;
        public bool ITADIsReprint
        {
            get { return _ITADIsReprint; }
            set
            {
                _ITADIsReprint = value;

                OnPropertyChanged("ITADIsReprint");
            }
        }

        //Selected Item
        private ITADReport _ITADReportSelectedItem;
        public ITADReport ITADReportSelectedItem
        {
            get { return _ITADReportSelectedItem; }
            set
            {
                _ITADReportSelectedItem = value;

                OnPropertyChanged("ITADReportSelectedItem");
            }
        }


        //HOLD ANY SAVED FILES
        private ObservableCollection<string> _ITADAttachedFiles;
        public ObservableCollection<string> ITADAttachedFiles
        {
            get { return _ITADAttachedFiles; }
            set
            {
                _ITADAttachedFiles = value;
                OnPropertyChanged("ITADAttachedFiles");
            }
        }

        //Assets Processed During Session
        private ObservableCollection<string> _ITADAssetsProcessed;
        public ObservableCollection<string> ITADAssetsProcessed
        {
            get { return _ITADAssetsProcessed; }
            set
            {
                _ITADAssetsProcessed = value;
                OnPropertyChanged("ITADAssetsProcessed");
            }
        }


        //ITADReportCollection to hold db records
        private ObservableCollection<ITADReport> _ITADReportCollection;
        public ObservableCollection<ITADReport> ITADReportCollection
        {
            get { return _ITADReportCollection; }
            set
            {
                _ITADReportCollection = value;
                OnPropertyChanged("ITADReportCollection");
            }
        }

        //Processes List
        private ObservableCollection<ITADProcessesPerformed> _ITADProcessesCollection;
        public ObservableCollection<ITADProcessesPerformed> ITADProcessesCollection
        {
            get { return _ITADProcessesCollection; }
            set
            {
                _ITADProcessesCollection = value;
                OnPropertyChanged("ITADAssetsProcessed");
            }
        }

        //Testing List
        private ObservableCollection<ITADTestingPerformed> _ITADTestingCollection;
        public ObservableCollection<ITADTestingPerformed> ITADTestingCollection
        {
            get { return _ITADTestingCollection; }
            set
            {
                _ITADTestingCollection = value;
                OnPropertyChanged("ITADTestingCollection");
            }
        }


        //Category List
        private ObservableCollection<ITADDeviceCategories> _ITADCategoryCollection;
        public ObservableCollection<ITADDeviceCategories> ITADCategoryCollection
        {
            get { return _ITADCategoryCollection; }
            set
            {
                _ITADCategoryCollection = value;
                OnPropertyChanged("ITADCategoryCollection");
            }
        }

        //Verification List
        private ObservableCollection<ITADVerificationPerformed> _ITADVerificationCollection;
        public ObservableCollection<ITADVerificationPerformed> ITADVerificationCollection
        {
            get { return _ITADVerificationCollection; }
            set
            {
                _ITADVerificationCollection = value;
                OnPropertyChanged("ITADVerificationCollection");
            }
        }



        //Method to identify the correct folder path in PO Test Reports folder based off todays date
        public string GetFolderPath(string dt)
        {


            //Just needs DateTimeNow.ToShortDateString()
            string returnpath = string.Empty;



            string WhichMonth = dt.Substring(3, 2);
            string WhichYear = dt.Substring(6, 4);

            //Strip Month information first

            switch (WhichMonth)
            {

                case "01":
                    WhichMonth = "01-Jan";
                    returnpath = @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Technical\PURCHASE ORDER - TEST REPORTS\" + WhichYear + @"\" + WhichMonth + @"\";
                    break;
                case "02":
                    WhichMonth = "02-Feb";
                    returnpath = @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Technical\PURCHASE ORDER - TEST REPORTS\" + WhichYear + @"\" + WhichMonth + @"\";
                    break;
                case "03":
                    WhichMonth = "03-Mar";
                    returnpath = @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Technical\PURCHASE ORDER - TEST REPORTS\" + WhichYear + @"\" + WhichMonth + @"\";
                    break;
                case "04":
                    WhichMonth = "04-Apr";
                    returnpath = @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Technical\PURCHASE ORDER - TEST REPORTS\" + WhichYear + @"\" + WhichMonth + @"\";
                    break;
                case "05":
                    WhichMonth = "05-May";
                    returnpath = @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Technical\PURCHASE ORDER - TEST REPORTS\" + WhichYear + @"\" + WhichMonth + @"\";
                    break;
                case "06":
                    WhichMonth = "06-Jun";
                    returnpath = @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Technical\PURCHASE ORDER - TEST REPORTS\" + WhichYear + @"\" + WhichMonth + @"\";
                    break;
                case "07":
                    WhichMonth = "07-Jul";
                    returnpath = @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Technical\PURCHASE ORDER - TEST REPORTS\" + WhichYear + @"\" + WhichMonth + @"\";
                    break;
                case "08":
                    WhichMonth = "08-Aug";
                    returnpath = @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Technical\PURCHASE ORDER - TEST REPORTS\" + WhichYear + @"\" + WhichMonth + @"\";
                    break;
                case "09":
                    WhichMonth = "09-Sep";
                    returnpath = @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Technical\PURCHASE ORDER - TEST REPORTS\" + WhichYear + @"\" + WhichMonth + @"\";
                    break;
                case "10":
                    WhichMonth = "10-Oct";
                    returnpath = @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Technical\PURCHASE ORDER - TEST REPORTS\" + WhichYear + @"\" + WhichMonth + @"\";
                    break;
                case "11":
                    WhichMonth = "11-Nov";
                    returnpath = @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Technical\PURCHASE ORDER - TEST REPORTS\" + WhichYear + @"\" + WhichMonth + @"\";
                    break;
                case "12":
                    WhichMonth = "12-Dec";
                    returnpath = @"" + StaticFunctions.UncPathToUse.Replace(@"Warehouse General\", "") + @"Testing\Technical\PURCHASE ORDER - TEST REPORTS\" + WhichYear + @"\" + WhichMonth + @"\";
                    break;

            }


            //MessageBox.Show(returnpath);


            return returnpath;
        }






        //SHOW DIALOG  Methods
        public async void DialogMessage(string Header, string Message)
        {
            var window = App.Current.Windows.OfType<MetroWindow>().FirstOrDefault(x => x.IsActive);
            if (window != null)
            {
                await window.ShowMessageAsync(Header, Message, MessageDialogStyle.Affirmative);
            }

        }

        public async void ProgressDialogProgress(string Header, string Message)
        {
            // Show...
            ProgressDialogController controller = await dialogCoordinator.ShowProgressAsync(this, Header, Message);
            controller.SetIndeterminate();

            // Do your work... 

            // Close...
            await controller.CloseAsync();
        }




        //COMMANDS

        //Load Content Command used with content control and user controls
        //private LoadContentCommand _LoadContentCommand = null;
        //public LoadContentCommand LoadContentCommand
        //{
        //    get { return _LoadContentCommand; }
        //}

        //Generate Checklist Command
        private GenerateChecklistCommand _GenerateChecklistCommand = null;
        public GenerateChecklistCommand GenerateChecklistCommand
        {
            get { return _GenerateChecklistCommand; }
        }

        //Preview Checklist Command
        private PreviewChecklistCommand _PreviewChecklistCommand = null;
        public PreviewChecklistCommand PreviewChecklistCommand
        {
            get { return _PreviewChecklistCommand; }
        }

        //Read Checklist Command
        private ReadChecklistCommand _ReadChecklistCommand = null;
        public ReadChecklistCommand ReadChecklistCommand
        {
            get { return _ReadChecklistCommand; }
        }

        //Print Save Doc Checklist Command
        private PrintSaveDocCommand _PrintSaveDocCommand = null;
        public PrintSaveDocCommand PrintSaveDocCommand
        {
            get { return _PrintSaveDocCommand; }
        }

        //Print SaveFlow Document Command
        private PrintSaveFlowDocumentCommand _PrintSaveFlowDocumentCommand = null;
        public PrintSaveFlowDocumentCommand PrintSaveFlowDocumentCommand
        {
            get { return _PrintSaveFlowDocumentCommand; }
        }


        //Signature Command
        private SigCommand _SigCommand = null;
        public SigCommand SigCommand
        {
            get { return _SigCommand; }
        }

        //Add FilesCommand
        private AddFilesCommand _AddFilesCommand = null;
        public AddFilesCommand AddFilesCommand
        {
            get { return _AddFilesCommand; }
        }

        //Add FoldersCommand
        private AddFoldersCommand _AddFoldersCommand = null;
        public AddFoldersCommand AddFoldersCommand
        {
            get { return _AddFoldersCommand; }
        }

        //Add FilesCommand Dell
        private AddFilesDellCommand _AddFilesDellCommand = null;
        public AddFilesDellCommand AddFilesDellCommand
        {
            get { return _AddFilesDellCommand; }
        }

        //Add FilesCommand Lenovo
        private AddFilesLenovoCommand _AddFilesLenovoCommand = null;
        public AddFilesLenovoCommand AddFilesLenovoCommand
        {
            get { return _AddFilesLenovoCommand; }
        }

        //Select Folder Command 
        private SelectFolderCommand _SelectFolderCommand = null;
        public SelectFolderCommand SelectFolderCommand
        {
            get { return _SelectFolderCommand; }
        }

        //IP Scanner Search Command
        private IPScannerSearchCommand _IPScannerSearchCommand = null;
        public IPScannerSearchCommand IPScannerSearchCommand
        {
            get { return _IPScannerSearchCommand; }
        }

        //Read Servers Processed Command
        private ReadServersProcessedCommand _ReadServersProcessedCommand = null;
        public ReadServersProcessedCommand ReadServersProcessedCommand
        {
            get { return _ReadServersProcessedCommand; }
        }

        // CONTROL EVENTS


        //Sales Order Number
        //public void SalesOrderNoTBX_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //MessageBox.Show("");
        //}


        //********IP MAC PING METHODS ***********


        //public struct IPMacVendor
        //{
        //    public IPMacVendor(string ip, string mac, string vendor)
        //    {
        //        IpAddress = ip;
        //        MacAddress = mac;
        //        Vendor = vendor;
        //    }
        //    public string IpAddress;
        //    public string MacAddress;
        //    public string Vendor;
        //}

        public string CurrentIP { get; set; }
        public string CurrentMac { get; set; }
        public string CurrentVendor { get; set; }

        //Collection to Add Items
        // public ObservableCollection<IPMacVendor> ListIPMacVendor { get; set; }

        //LOOK UP MAC ADDRESS VENDOR
        public async Task<string> LookupMac(string MacAddress)
        {
            var uri = new Uri("http://api.macvendors.com/" + WebUtility.UrlEncode(MacAddress));
            using (var wc = new HttpClient())
                return await wc.GetStringAsync(uri);
        }
        public void OutputMacResults(ObservableCollection<string> maccollection)
        {
            foreach (var mac in new string[] { "88:53:2E:67:07:BE", "FC:FB:FB:01:FA:21", "D4:F4:6F:C9:EF:8D" })
                Console.WriteLine(mac + "\t" + LookupMac(mac).Result);
            Console.ReadLine();
        }

        public string GetMacFromIP(string ip)
        {
            var macIpPairs = GetAllMacAddressesAndIppairs();
            int index = macIpPairs.FindIndex(x => x.IpAddress == ip);
            if (index >= 0)
            {
                return macIpPairs[index].MacAddress.ToUpper();
            }
            else
            {
                return null;
            }
        }


        //Properties for IP Scanner Range
        private string _IpRangeFrom;

        public string IpRangeFrom
        {
            get { return _IpRangeFrom; }
            set
            {
                _IpRangeFrom = value;

                OnPropertyChanged("IpRangeFrom");
            }
        }

        private string _IpRangeTo;

        public string IpRangeTo
        {
            get { return _IpRangeTo; }
            set
            {
                _IpRangeTo = value;

                OnPropertyChanged("IpRangeTo");
            }
        }

        //OBTAINS A LIST OF ALL MACADDRESSES ON SUBNET
        public List<MacIpPair> GetAllMacAddressesAndIppairs()
        {
            List<MacIpPair> mip = new List<MacIpPair>();
            System.Diagnostics.Process pProcess = new System.Diagnostics.Process();
            pProcess.StartInfo.FileName = "arp";
            pProcess.StartInfo.Arguments = "-a ";
            pProcess.StartInfo.UseShellExecute = false;
            pProcess.StartInfo.RedirectStandardOutput = true;
            pProcess.StartInfo.CreateNoWindow = true;
            pProcess.Start();
            string cmdOutput = pProcess.StandardOutput.ReadToEnd();
            string pattern = @"(?<ip>([0-9]{1,3}\.?){4})\s*(?<mac>([a-f0-9]{2}-?){6})";

            foreach (Match m in Regex.Matches(cmdOutput, pattern, RegexOptions.IgnoreCase))
            {
                mip.Add(new MacIpPair()
                {
                    MacAddress = m.Groups["mac"].Value,
                    IpAddress = m.Groups["ip"].Value
                });
            }

            return mip;
        }
        public struct MacIpPair
        {
            public string MacAddress;
            public string IpAddress;
        }

        public void scanLiveHosts(string ipFrom, string ipTo)
        {

            try
            {
                if (!String.IsNullOrEmpty(ipFrom) && !String.IsNullOrEmpty(ipTo))
                {
                    IPAddress from = IPAddress.Parse(ipFrom);
                    IPAddress to = IPAddress.Parse(ipTo);
                    StartingIP = ipFrom;
                    EndingIP = ipTo;
                    //IpRangeFrom = ipFrom;
                    //IpRangeTo = ipTo;
                }
                else
                {
                    //default range
                    IPAddress from = IPAddress.Parse("192.168.100.1");
                    IPAddress to = IPAddress.Parse("192.168.103.254");
                    //IpRangeFrom = "192.168.101.1";
                    //IpRangeTo = "192.168.101.254";
                    StartingIP = "192.168.100.1";
                    EndingIP = "192.168.103.254";
                }

            }
            catch (FormatException fe)
            {
                MessageBox.Show(fe.Message);
                return;
            }


            int lastF = StartingIP.LastIndexOf(".");
            //int subn = StartingIP.Replace
            int lastT = EndingIP.LastIndexOf(".");
            string frm = StartingIP.Substring(lastF + 1);
            string tto = EndingIP.Substring(lastT + 1);
            int result = 0;
            //System.Diagnostics.Debug.WriteLine(frm + " " + tto);
            int count = 0;

            try
            {

                //CHECK FOR RANGE AND ADJUST TOTALS

                if (StartingIP.Substring(0, lastT + 1) == EndingIP.Substring(0, lastT + 1))
                {
                    //MessageBox.Show("Yes subnets match");
                }
                else
                {
                    // MessageBox.Show("No Subnets don't match");
                }


                //for loop
                for (int i = int.Parse(frm); i <= int.Parse(tto); i++)
                {
                    //MessageBox.Show(count.ToString()EndingIP.Substring(0, lastT + 1));

                    //if(StartingIP.Substring(0,lastT + 1) == EndingIP.Substring(0, lastT + 1))
                    //{

                    //    string address = EndingIP.Substring(0, lastT + 1);
                    //    sendAsyncPingPacket(address + i);
                    //}
                    //else
                    //{
                    //    string address = StartingIP.Substring(0, lastT + 1);
                    //    sendAsyncPingPacket(address + i);
                    //}


                    string address = EndingIP.Substring(0, lastT + 1);
                    sendAsyncPingPacket(address + i);

                    // System.Diagnostics.Debug.WriteLine(address);
                    count = count + 1;
                    // IPHostEntry ip = Dns.GetHostEntry(address + i);
                    //IPAddress hostIPAddress = IPAddress.Parse(address + i);

                    //MessageBox.Show(address);
                    //await System.Threading.Tasks.Task.Run(() => sendAsyncPingPacket(address + i));



                    //CurrentIP = address + i;

                    //MessageBox.Show(CurrentMac);

                    // IPHostEntry ip = Dns.GetHostByAddress(address + i);   //GetHostByName(Dns.GetHostName());
                    //ipFrom.Text = ip.AddressList[0].ToString();
                    //ipTo.Text = ip.AddressList[0].ToString();

                    //IPResults.Add(ip.AddressList[1].ToString());
                    result += 1;
                }

                //  MessageBox.Show(count.ToString());
            }
            catch (SocketException se)
            {
                MessageBox.Show(se.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }


        //PING
        public async void sendAsyncPingPacket(string hostToPing)
        {
            try
            {
                // int timeout = 5000;
                // AutoResetEvent waiter = new AutoResetEvent(false);
                Ping pingPacket = new Ping();
                //ping completion event reaised
                pingPacket.PingCompleted += new PingCompletedEventHandler(PingCompletedCallback);
                string data = "Ping test check";
                byte[] byteBuffer = Encoding.ASCII.GetBytes(data);
                PingOptions pingOptions = new PingOptions(64, true);

                //MessageBox.Show("Hello");

                // IPResults.Add("Time to live: " +  pingOptions.Ttl.ToString());
                //("Time to live: {0}", pingOptions.Ttl);
                //Console.WriteLine("Don't fragment: {0}", pingOptions.DontFragment);
                //pingPacket.Send(hostToPing, timeout, byteBuffer, pingOptions, waiter);
                // MessageBox.Show(CurrentIP);

                await pingPacket.SendPingAsync(hostToPing, 3000);
                //pingPacket.Send(hostToPing);

                // ListIPMacVendor.Add(new IPMacVendor(hostToPing, GetMacFromIP(hostToPing), ""));


                //192.168.101.20

                //do something useful
                //waiter.WaitOne();
                //IPResults.Add("Ping RoundTrip returned, Do something useful here...");
            }
            catch (PingException pe)
            {
                string except = pe.Message;
                //IPResults.Add("INVALID IP ADDRESS FOUND");
            }
            catch (Exception ex)
            {
                string except = ex.Message;
                //IPResults.Add("Exceptin " + ex.Message);
            }

        }






        private void PingCompletedCallback(object sender, PingCompletedEventArgs e)
        {
            try
            {
                if (e.Cancelled)
                {
                    //*IPResults.Add("Ping canceled.");

                    // Let the main thread resume. 
                    // UserToken is the AutoResetEvent object that the main thread 
                    // is waiting for.
                    // ((AutoResetEvent)e.UserState).Set();
                }

                // If an error occurred, display the exception to the user.
                if (e.Error != null)
                {
                    //*IPResults.Add(CurrentIP + " Ping failed>>>>>>>>>>>>> ");

                    //IPResults.Add(CurrentIP + "Ping failed>>>>>>>>>>>>> ");
                    //this will print exception
                    //Console.WriteLine (e.Error.ToString ());

                    // Let the main thread resume. 
                    // ((AutoResetEvent)e.UserState).Set();
                }

                PingReply reply = e.Reply;

                // lookup mac from ip

                //string macAdd = GetMacFromIP(CurrentIP);
                //MessageBox.Show(macAdd);
                //get vendor info
                // string macVend = ""; //= await LookupMac(GetMacFromIP(CurrentIP));
                //CurrentMac = GetMacFromIP(CurrentIP);
                if (reply == null)
                    return;

                //if reply ok;
                if (reply.Status == IPStatus.Success)
                {

                    //MessageBox.Show(reply.Address.ToString());

                    string ipv4 = reply.Address.ToString();
                    string mac = GetMacFromIP(reply.Address.ToString());
                    if (mac != null)
                    {
                        string vendor = FindVendor(mac.Substring(0, 8).Replace("-", "")).Trim();
                        //MessageBox.Show(Dns.GetHostEntry(ipv4).HostName); 
                        //string host = Dns.GetHostEntry(ipv4).HostName;
                        //string host = Dns.GetHostByAddress(ipv4).HostName;
                        //IPResults.Add("IPV4: " + reply.Address.ToString() + "  Mac: " + GetMacFromIP(reply.Address.ToString()) + "     " +  vendor);
                        //IPResults.Add(new IPInfo(ipv4, host, mac, vendor));

                        IPResults.Add(new IPInfo(ipv4, "", mac, vendor));
                    }





                }
                //Write Results  Gets ping reply, Mac Address and the Vendor from Mac Address
                //    DisplayReply(reply);

                // Let the main thread resume.
                //((AutoResetEvent)e.UserState).Set();
            }
            catch (PingException pe)
            {
                string except = pe.Message;
                //IPResults.Add("INVALID IP ADDRESS");
            }
            catch (Exception ex)
            {
                string except = ex.Message;
                // MessageBox.Show("Came Here");
                //IPResults.Add("Exception " + ex.Message);
            }
        }


        public string DoGetHostEntry(IPAddress address)
        {
            IPHostEntry host = Dns.GetHostEntry(address);

            return host.HostName;
            // Console.WriteLine($"GetHostEntry({address}) returns HostName: {host.HostName}");
        }



        public async Task<Dictionary<string, bool>> PingListAsync(Dictionary<string, bool> HostList, int sec = 3)
        {

            // Create a buffer of 32 bytes of data to be transmitted.
            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            // set a quick TTL
            PingOptions options = new PingOptions(20, true);

            // internal support Task to handle Ping Exceptions like "host not found"
            async Task<KeyValuePair<string, bool>> PingHost(string host)
            {
                try
                {
                    var pingresult = await Task.Run(() => new Ping().SendPingAsync(host, sec * 1000, buffer, options));
                    //t.Wait();
                    if (pingresult.Status == IPStatus.Success)
                        return new KeyValuePair<string, bool>(host, true);
                    else
                        return new KeyValuePair<string, bool>(host, false);

                }
                catch
                {
                    return new KeyValuePair<string, bool>(host, false);
                }

            }

            //Using Tasks >>
            var watch = new Stopwatch();
            watch.Start();
            var tasksb = HostList.Select(HostName => PingHost(HostName.Key.ToString()));

            var pinglist = await Task.WhenAll(tasksb);


            foreach (var pingreply in pinglist)
            {
                HostList[pingreply.Key] = pingreply.Value;
            }

            watch.Stop();
            //Log.Debug("PingList (Tasks) Time elapsed: " + watch.Elapsed);
            //Using Tasks <<

            Debug.WriteLine(watch.Elapsed);

            return HostList;

        }


        public string FindVendor(string partialmac)
        {


            string line = "";
            using (StreamReader sr = new StreamReader(@"" + @"\\pinnacle.local\tech_resources\Nebula\Application\mac_vendors\mac-vendor.txt"))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Contains(partialmac))
                    {
                        return line.Substring(7);

                    }
                    //Debug.WriteLine(line.Substring(7));
                    //MessageBox.Show(line.Substring(7).Trim());
                }
            }

            return "";
        }



        public void DisplayReply(PingReply reply)
        {
            if (reply == null)
                return;

            //IPResults.Add("ping status: " + reply.Status);
            if (reply.Status == IPStatus.Success)
            {
                // CurrentMac = GetMacFromIP(CurrentIP);
                //* IPResults.Add(reply.Address.ToString() + " " + CurrentMac);

                //IPResults.Add(reply.Address.ToString() + " " + Vendor + " " + CurrentMac);
                //IPResults.Add("Address: " + reply.Address.ToString());
                //IPResults.Add("RoundTrip time: " + reply.RoundtripTime.ToString());
                //IPResults.Add("Time to live: " + reply.Options.Ttl.ToString());
                ////Console.WriteLine ("Don't fragment: {0}", reply.Options.DontFragment);
                //IPResults.Add("Buffer size: " + reply.Buffer.Length.ToString());
            }
        }



        // Saves and create URI Path to QR Image, required for flowdocument cloning
        public BitmapImage EmbedChecklistQR(string UriPath, BitmapImage QRProperty)
        {
            try
            {


                ////IMPORTANT AS QR THROWS EXCEPTION IF NOT A URI PATH
                ////Generate a file of the bitmap
                BitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(QRProperty));
                //MessageBox.Show(UriPath);
                using (FileStream fileStream = new FileStream(UriPath, System.IO.FileMode.Create, FileAccess.Write, FileShare.Read))
                {

                    encoder.Save(fileStream);
                    fileStream.Flush();
                    fileStream.Close();
                }


                // Create source.
                BitmapImage bi = new BitmapImage();
                // BitmapImage.UriSource must be in a BeginInit/EndInit block.
                bi.BeginInit();
                //*** Required not to lock file ***
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.UriSource = new Uri(UriPath, UriKind.Absolute);
                bi.EndInit();

                //Return Bitmap Image with full absolute path
                return bi;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + ", Please check for illegal characters in the Serial No Field");
                return null;
            }
        }




    }
}
