﻿using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Nebula.Commands;
using Nebula.Commands.XTerminal;
using Nebula.Models;
using Nebula.Helpers;
using Nebula.Views;
using MahApps.Metro.Controls;
using System.Windows.Documents;
using System.Windows;
using System.Collections.ObjectModel;
using System.Threading;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Sockets;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;
using System.Windows.Media;

namespace Nebula.ViewModels
{
    public class ServerTabViewModel : HasPropertyChanged
    {
        public ServerTabViewModel()
        {
            TabColorS1 = Brushes.MediumPurple;
            TabColorS2 = Brushes.MediumPurple;
            TabColorS3 = Brushes.MediumPurple;
            TabColorS4 = Brushes.MediumPurple;
            TabColorS5 = Brushes.MediumPurple;
            TabColorS6 = Brushes.MediumPurple;
            TabColorS7 = Brushes.MediumPurple;
            TabColorS8 = Brushes.MediumPurple;
            TabColorS9 = Brushes.MediumPurple;
            TabColorS10 = Brushes.MediumPurple;
            TabColorS11 = Brushes.MediumPurple;
            TabColorS12 = Brushes.MediumPurple;
            TabColorS13 = Brushes.MediumPurple;
            TabColorS14 = Brushes.MediumPurple;
            TabColorS15 = Brushes.MediumPurple;
            TabColorS16 = Brushes.MediumPurple;

            CurrentView1 = new LenovoServerView();
            CurrentView2 = new LenovoServerView();
            CurrentView3 = new LenovoServerView();
            CurrentView4 = new LenovoServerView();
            CurrentView5 = new LenovoServerView();
            CurrentView6 = new LenovoServerView();
            CurrentView7 = new LenovoServerView();
            CurrentView8 = new LenovoServerView();
            CurrentView9 = new LenovoServerView();
            CurrentView10 = new LenovoServerView();
            CurrentView11 = new LenovoServerView();
            CurrentView12 = new LenovoServerView();
            CurrentView13 = new LenovoServerView();
            CurrentView14 = new LenovoServerView();
            CurrentView15 = new LenovoServerView();
            CurrentView16 = new LenovoServerView();


        }

        private UserControl _CurrentView1;

        public UserControl CurrentView1
        {
            get { return _CurrentView1; }
            set
            {
                _CurrentView1 = value;
                OnPropertyChanged("CurrentView1");
            }
        }


        private UserControl _CurrentView2;

        public UserControl CurrentView2
        {
            get { return _CurrentView2; }
            set
            {
                _CurrentView2 = value;
                OnPropertyChanged("CurrentView2");
            }
        }

        private UserControl _CurrentView3;

        public UserControl CurrentView3
        {
            get { return _CurrentView3; }
            set
            {
                _CurrentView3 = value;
                OnPropertyChanged("CurrentView3");
            }
        }

        private UserControl _CurrentView4;

        public UserControl CurrentView4
        {
            get { return _CurrentView4; }
            set
            {
                _CurrentView4 = value;
                OnPropertyChanged("CurrentView4");
            }
        }


        private UserControl _CurrentView5;

        public UserControl CurrentView5
        {
            get { return _CurrentView5; }
            set
            {
                _CurrentView5 = value;
                OnPropertyChanged("CurrentView5");
            }
        }


        private UserControl _CurrentView6;

        public UserControl CurrentView6
        {
            get { return _CurrentView6; }
            set
            {
                _CurrentView6 = value;
                OnPropertyChanged("CurrentView6");
            }
        }


        private UserControl _CurrentView7;

        public UserControl CurrentView7
        {
            get { return _CurrentView7; }
            set
            {
                _CurrentView7 = value;
                OnPropertyChanged("CurrentView7");
            }
        }


        private UserControl _CurrentView8;

        public UserControl CurrentView8
        {
            get { return _CurrentView8; }
            set
            {
                _CurrentView8 = value;
                OnPropertyChanged("CurrentView8");
            }
        }


        private UserControl _CurrentView9;

        public UserControl CurrentView9
        {
            get { return _CurrentView9; }
            set
            {
                _CurrentView9 = value;
                OnPropertyChanged("CurrentView9");
            }
        }

        private UserControl _CurrentView10;

        public UserControl CurrentView10
        {
            get { return _CurrentView10; }
            set
            {
                _CurrentView10 = value;
                OnPropertyChanged("CurrentView10");
            }
        }

     

        private UserControl _CurrentView11;

        public UserControl CurrentView11
        {
            get { return _CurrentView11; }
            set
            {
                _CurrentView11 = value;
                OnPropertyChanged("CurrentView11");
            }
        }


        private UserControl _CurrentView12;

        public UserControl CurrentView12
        {
            get { return _CurrentView12; }
            set
            {
                _CurrentView12 = value;
                OnPropertyChanged("CurrentView12");
            }
        }


        private UserControl _CurrentView13;

        public UserControl CurrentView13
        {
            get { return _CurrentView13; }
            set
            {
                _CurrentView13 = value;
                OnPropertyChanged("CurrentView13");
            }
        }


        private UserControl _CurrentView14;

        public UserControl CurrentView14
        {
            get { return _CurrentView14; }
            set
            {
                _CurrentView14 = value;
                OnPropertyChanged("CurrentView14");
            }
        }


        private UserControl _CurrentView15;

        public UserControl CurrentView15
        {
            get { return _CurrentView15; }
            set
            {
                _CurrentView15 = value;
                OnPropertyChanged("CurrentView15");
            }
        }

        private UserControl _CurrentView16;

        public UserControl CurrentView16
        {
            get { return _CurrentView16; }
            set
            {
                _CurrentView16 = value;
                OnPropertyChanged("CurrentView16");
            }
        }


        //MULTI SERVER TAB HEADER PROPERTIES

        private string _Server1TabHeader = "Server1";

        public string Server1TabHeader
        {
            get { return _Server1TabHeader; }
            set
            {
                _Server1TabHeader = value;
                OnPropertyChanged("Server1TabHeader");
            }
        }


        private string _Server2TabHeader = "Server2";

        public string Server2TabHeader
        {
            get { return _Server2TabHeader; }
            set
            {
                _Server2TabHeader = value;
                OnPropertyChanged("Server2TabHeader");
            }
        }

        private string _Server3TabHeader = "Server3";

        public string Server3TabHeader
        {
            get { return _Server3TabHeader; }
            set
            {
                _Server3TabHeader = value;
                OnPropertyChanged("Server3TabHeader");
            }
        }



        private string _Server4TabHeader = "Server4";

        public string Server4TabHeader
        {
            get { return _Server4TabHeader; }
            set
            {
                _Server4TabHeader = value;
                OnPropertyChanged("Server4TabHeader");
            }
        }

        private string _Server5TabHeader = "Server5";

        public string Server5TabHeader
        {
            get { return _Server5TabHeader; }
            set
            {
                _Server5TabHeader = value;
                OnPropertyChanged("Server5TabHeader");
            }
        }

        private string _Server6TabHeader = "Server6";

        public string Server6TabHeader
        {
            get { return _Server6TabHeader; }
            set
            {
                _Server6TabHeader = value;
                OnPropertyChanged("Server6TabHeader");
            }
        }


        private string _Server7TabHeader = "Server7";

        public string Server7TabHeader
        {
            get { return _Server7TabHeader; }
            set
            {
                _Server7TabHeader = value;
                OnPropertyChanged("Server7TabHeader");
            }
        }


        private string _Server8TabHeader = "Server8";

        public string Server8TabHeader
        {
            get { return _Server8TabHeader; }
            set
            {
                _Server8TabHeader = value;
                OnPropertyChanged("Server8TabHeader");
            }
        }


        private string _Server9TabHeader = "Server9";

        public string Server9TabHeader
        {
            get { return _Server9TabHeader; }
            set
            {
                _Server9TabHeader = value;
                OnPropertyChanged("Server9TabHeader");
            }
        }


        private string _Server10TabHeader = "Server10";

        public string Server10TabHeader
        {
            get { return _Server10TabHeader; }
            set
            {
                _Server10TabHeader = value;
                OnPropertyChanged("Server10TabHeader");
            }
        }


        private string _Server11TabHeader = "Server11";

        public string Server11TabHeader
        {
            get { return _Server11TabHeader; }
            set
            {
                _Server11TabHeader = value;
                OnPropertyChanged("Server11TabHeader");
            }
        }


        private string _Server12TabHeader = "Server12";

        public string Server12TabHeader
        {
            get { return _Server12TabHeader; }
            set
            {
                _Server12TabHeader = value;
                OnPropertyChanged("Server12TabHeader");
            }
        }


        private string _Server13TabHeader = "Server13";

        public string Server13TabHeader
        {
            get { return _Server13TabHeader; }
            set
            {
                _Server13TabHeader = value;
                OnPropertyChanged("Server13TabHeader");
            }
        }

        private string _Server14TabHeader = "Server14";

        public string Server14TabHeader
        {
            get { return _Server14TabHeader; }
            set
            {
                _Server14TabHeader = value;
                OnPropertyChanged("Server14TabHeader");
            }
        }

        private string _Server15TabHeader = "Server15";

        public string Server15TabHeader
        {
            get { return _Server15TabHeader; }
            set
            {
                _Server15TabHeader = value;
                OnPropertyChanged("Server15TabHeader");
            }
        }


        private string _Server16TabHeader = "Server16";

        public string Server16TabHeader
        {
            get { return _Server16TabHeader; }
            set
            {
                _Server16TabHeader = value;
                OnPropertyChanged("Server16TabHeader");
            }
        }




        // server tabs
        private Brush _TabColorS1;

        public Brush TabColorS1
        {
            get { return _TabColorS1; }
            set
            {
                _TabColorS1 = value;
                OnPropertyChanged("TabColorS1");
            }
        }


        private Brush _TabColorS2;

        public Brush TabColorS2
        {
            get { return _TabColorS2; }
            set
            {
                _TabColorS2 = value;
                OnPropertyChanged("TabColorS2");
            }
        }

        private Brush _TabColorS3;

        public Brush TabColorS3
        {
            get { return _TabColorS3; }
            set
            {
                _TabColorS3 = value;
                OnPropertyChanged("TabColorS3");
            }
        }

        private Brush _TabColorS4;

        public Brush TabColorS4
        {
            get { return _TabColorS4; }
            set
            {
                _TabColorS4 = value;
                OnPropertyChanged("TabColorS4");
            }
        }


        private Brush _TabColorS5;

        public Brush TabColorS5
        {
            get { return _TabColorS5; }
            set
            {
                _TabColorS5 = value;
                OnPropertyChanged("TabColorS5");
            }
        }


        private Brush _TabColorS6;

        public Brush TabColorS6
        {
            get { return _TabColorS6; }
            set
            {
                _TabColorS6 = value;
                OnPropertyChanged("TabColorS6");
            }
        }


        private Brush _TabColorS7;

        public Brush TabColorS7
        {
            get { return _TabColorS7; }
            set
            {
                _TabColorS7 = value;
                OnPropertyChanged("TabColorS7");
            }
        }


        private Brush _TabColorS8;

        public Brush TabColorS8
        {
            get { return _TabColorS8; }
            set
            {
                _TabColorS8 = value;
                OnPropertyChanged("TabColorS8");
            }
        }


        private Brush _TabColorS9;

        public Brush TabColorS9
        {
            get { return _TabColorS9; }
            set
            {
                _TabColorS9 = value;
                OnPropertyChanged("TabColorS9");
            }
        }

        private Brush _TabColorS10;

        public Brush TabColorS10
        {
            get { return _TabColorS10; }
            set
            {
                _TabColorS10 = value;
                OnPropertyChanged("TabColorS10");
            }
        }

        private Brush _TabColorS11;

        public Brush TabColorS11
        {
            get { return _TabColorS11; }
            set
            {
                _TabColorS11 = value;
                OnPropertyChanged("TabColorS11");
            }
        }


        private Brush _TabColorS12;

        public Brush TabColorS12
        {
            get { return _TabColorS12; }
            set
            {
                _TabColorS12 = value;
                OnPropertyChanged("TabColorS12");
            }
        }


        private Brush _TabColorS13;

        public Brush TabColorS13
        {
            get { return _TabColorS13; }
            set
            {
                _TabColorS13 = value;
                OnPropertyChanged("TabColorS13");
            }
        }


        private Brush _TabColorS14;

        public Brush TabColorS14
        {
            get { return _TabColorS14; }
            set
            {
                _TabColorS14 = value;
                OnPropertyChanged("TabColorS14");
            }
        }


        private Brush _TabColorS15;

        public Brush TabColorS15
        {
            get { return _TabColorS15; }
            set
            {
                _TabColorS15 = value;
                OnPropertyChanged("TabColorS15");
            }
        }


        private Brush _TabColorS16;

        public Brush TabColorS16
        {
            get { return _TabColorS16; }
            set
            {
                _TabColorS16 = value;
                OnPropertyChanged("TabColorS16");
            }
        }


    }
}
