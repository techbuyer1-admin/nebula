﻿using System;
using System.Collections.ObjectModel;
using MahApps.Metro.IconPacks;
using Nebula.Navigation;

namespace Nebula.ViewModels
{
    public class ShellViewModel 
    {
        private static readonly ObservableCollection<MenuItemMahapps> AppMenu = new ObservableCollection<MenuItemMahapps>();
        private static readonly ObservableCollection<MenuItemMahapps> AppOptionsMenu = new ObservableCollection<MenuItemMahapps>();

        public ObservableCollection<MenuItemMahapps> Menu => AppMenu;

        public ObservableCollection<MenuItemMahapps> OptionsMenu => AppOptionsMenu;

        public ShellViewModel()
        {
            // Build the menus
            this.Menu.Add(new MenuItemMahapps()
            {
                Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.BugSolid },
                Label = "Bugs",
                //NavigationType = typeof(BugsPage),
                //NavigationDestination = new Uri("Views/BugsPage.xaml", UriKind.RelativeOrAbsolute)
            });
            this.Menu.Add(new MenuItemMahapps()
            {
                Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.UserSolid },
                Label = "User",
                //NavigationType = typeof(UserPage),
                //NavigationDestination = new Uri("Views/UserPage.xaml", UriKind.RelativeOrAbsolute)
            });
            this.Menu.Add(new MenuItemMahapps()
            {
                Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.CoffeeSolid },
                Label = "Break",
                //NavigationType = typeof(BreakPage),
                //NavigationDestination = new Uri("Views/BreakPage.xaml", UriKind.RelativeOrAbsolute)
            });
            this.Menu.Add(new MenuItemMahapps()
            {
                Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.FontAwesomeBrands },
                Label = "Awesome",
                //NavigationType = typeof(AwesomePage),
                //NavigationDestination = new Uri("Views/AwesomePage.xaml", UriKind.RelativeOrAbsolute)
            });

            this.OptionsMenu.Add(new MenuItemMahapps()
            {
                Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.CogsSolid },
                Label = "Settings",
                //NavigationType = typeof(SettingsPage),
                //NavigationDestination = new Uri("Views/SettingsPage.xaml", UriKind.RelativeOrAbsolute)
            });
            this.OptionsMenu.Add(new MenuItemMahapps()
            {
                Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.InfoCircleSolid },
                Label = "About",
                //NavigationType = typeof(AboutPage),
                //NavigationDestination = new Uri("Views/AboutPage.xaml", UriKind.RelativeOrAbsolute)
            });
        }
    }
}