﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Management;
using System.Printing;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
//using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using System.Windows.Xps.Serialization;
using System.Xml;
using Nebula.Commands;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;
using Nebula.Views;

namespace Nebula.Paginators
{
    public class DocumentPaginatorWrapper : DocumentPaginator


    {

        Size m_PageSize;
        Size m_Margin;
        DocumentPaginator m_Paginator;
        Typeface m_Typeface;

        //Constructor   
        public DocumentPaginatorWrapper()
        {

        }

        public DocumentPaginatorWrapper(DocumentPaginator paginator, Size pageSize, Size margin)
        {
            m_PageSize = pageSize;
            m_Margin = margin;
            m_Paginator = paginator;
            m_Paginator.PageSize = new Size(m_PageSize.Width - margin.Width * 2, m_PageSize.Height - margin.Height * 2);

        }





        Rect Move(Rect rect)
        {
            if (rect.IsEmpty)
            {
                return rect;
            }
            else
            {
                return new Rect(rect.Left + m_Margin.Width, rect.Top + m_Margin.Height, rect.Width, rect.Height);
            }
        }





        public override DocumentPage GetPage(int pageNumber)
        {
            DocumentPage page = m_Paginator.GetPage(pageNumber);
            // Create a wrapper visual for transformation and add extras
            ContainerVisual newpage = new ContainerVisual();
            DrawingVisual title = new DrawingVisual();
            using (DrawingContext ctx = title.RenderOpen())
            {
                if (m_Typeface == null)
                {
                    m_Typeface = new Typeface("Times New Roman");
                }
                //original now depriciated
                //FormattedText text = new FormattedText("Page " + (pageNumber + 1), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, m_Typeface, 14, Brushes.Black);

                FormattedText text = new FormattedText("Page " + (pageNumber + 1), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, m_Typeface, 14, Brushes.Black, VisualTreeHelper.GetDpi(newpage).PixelsPerDip);
                ctx.DrawText(text, new Point(0, -96 / 4)); // 1/4 inch above page content
            }

            DrawingVisual background = new DrawingVisual();

            using (DrawingContext ctx = background.RenderOpen())
            {
                ctx.DrawRectangle(new SolidColorBrush(Color.FromRgb(240, 240, 240)), null, page.ContentBox);
            }

            newpage.Children.Add(background); // Scale down page and center
            ContainerVisual smallerPage = new ContainerVisual();
            smallerPage.Children.Add(page.Visual);
            smallerPage.Transform = new MatrixTransform(0.95, 0, 0, 0.95, 0.025 * page.ContentBox.Width, 0.025 * page.ContentBox.Height);
            newpage.Children.Add(smallerPage);
            newpage.Children.Add(title);
            newpage.Transform = new TranslateTransform(m_Margin.Width, m_Margin.Height);
            return new DocumentPage(newpage, m_PageSize, Move(page.BleedBox), Move(page.ContentBox));
        }

        public override bool IsPageCountValid
        {
            get
            {
                return m_Paginator.IsPageCountValid;
            }
        }

        public override int PageCount
        {
            get
            {
                return m_Paginator.PageCount;
            }

        }

        public override Size PageSize
        {
            get
            {
                return m_Paginator.PageSize;
            }
            set
            {
                m_Paginator.PageSize = value;
            }
        }


        public override IDocumentPaginatorSource Source
        {
            get
            {
                return m_Paginator.Source;
            }
        }



        public static int SaveAsXps(string fileName)
        {

            object doc;
            FileInfo fileInfo = new FileInfo(fileName);


            using (FileStream file = fileInfo.OpenRead())
            {
                System.Windows.Markup.ParserContext context = new System.Windows.Markup.ParserContext();
                context.BaseUri = new Uri(fileInfo.FullName, UriKind.Absolute);
                doc = System.Windows.Markup.XamlReader.Load(file, context);
            }


            if (!(doc is IDocumentPaginatorSource))
            {
                Console.WriteLine("DocumentPaginatorSource expected");
                return -1;
            }


            using (Package container = Package.Open(fileName + ".xps", FileMode.Create))
            {
                using (XpsDocument xpsDoc = new XpsDocument(container, CompressionOption.Maximum))
                {
                    XpsSerializationManager rsm = new XpsSerializationManager(new XpsPackagingPolicy(xpsDoc), false);
                    DocumentPaginator paginator = ((IDocumentPaginatorSource)doc).DocumentPaginator;

                    // 8 inch x 6 inch, with half inch margin
                    paginator = new DocumentPaginatorWrapper(paginator, new Size(768, 676), new Size(48, 48));
                    rsm.SaveAsXaml(paginator);

                }


            }

            Console.WriteLine("{0} generated.", fileName + ".xps");
            return 0;
        }

        //Preview of flowdocument
        //https://stackoverflow.com/questions/2322064/how-can-i-produce-a-print-preview-of-a-flowdocument-in-a-wpf-application
        //https://talk.remobjects.com/t/wpf-printing-a-flow-document/6850
        // Highlight parts of the flow document when searching
        //https://stackoverflow.com/questions/18760702/highlight-parts-of-text-in-flowdocument



      



        public void PrintFlowDocPDF(FlowDocument FlowDoc)
        {


            //Clone the source document
            var str = XamlWriter.Save(FlowDoc);
            var stringReader = new System.IO.StringReader(str);
            var xmlReader = XmlReader.Create(stringReader);
            var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

            // get information about the dimensions of the seleted printer+media.
            System.Printing.PrintDocumentImageableArea ia = null;
            //System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);

            //docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter()
           // Create a local print server
            LocalPrintServer ps = new LocalPrintServer();

            // Get the default print queue
            PrintQueue pq = ps.GetPrintQueue("Microsoft Print to PDF");
            //pq.AddJob("nick", @"C:\Users\N.Myers\Desktop\Invoice App", true);
            pq.AddJob("nick");

            // Get an XpsDocumentWriter for the default print queue
            System.Windows.Xps.XpsDocumentWriter docWriter = PrintQueue.CreateXpsDocumentWriter(pq);

           

            if (docWriter != null && ia != null)
            {
                DocumentPaginator paginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;

                // Change the PageSize and PagePadding for the document to match the CanvasSize for the printer device.
                paginator.PageSize = new Size(ia.MediaSizeWidth, ia.MediaSizeHeight);
                Thickness t = new Thickness(72);  // copy.PagePadding;
                CloneDoc.PagePadding = new Thickness(
                                 Math.Max(ia.OriginWidth, t.Left),
                                   Math.Max(ia.OriginHeight, t.Top),
                                   Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
                                   Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));



                CloneDoc.ColumnWidth = double.PositiveInfinity;
                //copy.PageWidth = 528; // allow the page to be the natural with of the output device

                // Send content to the printer.
                docWriter.Write(paginator);
            }


        }



        private XpsDocumentWriter GetPrintXpsDocumentWriter()
        {
            // Create a local print server
            LocalPrintServer ps = new LocalPrintServer();

            // Get the default print queue
            PrintQueue pq = ps.DefaultPrintQueue;

            //ps.
            // MessageBox.Show(ps.ConnectToPrintQueue("Microsoft Print to PDF").ToString());

            // Get the default print queue
            // PrintQueue pq1 =ps. ;

            // Get an XpsDocumentWriter for the default print queue
            XpsDocumentWriter xpsdw = PrintQueue.CreateXpsDocumentWriter(pq);
            return xpsdw;
        }// end:GetPrintXpsDocumentWriter()



        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }


        public static string GetDefaultPrinter()
        {
            PrinterSettings settings = new PrinterSettings();
            string defaultPrinterName = settings.PrinterName;


            return defaultPrinterName;
        }

        public static void SetDefaultPrinterQueue(string printer)
        {
            // Create a local print server
            //LocalPrintServer ps = new LocalPrintServer();
            //PrintQueue pq = ps.DefaultPrintQueue;

            object printerName = printer;



            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");

            ManagementObjectCollection collection = searcher.Get();



            foreach (ManagementObject currentObject in collection)

            {

                if (currentObject["name"].ToString() == printerName.ToString())

                {

                    currentObject.InvokeMethod("SetDefaultPrinter", new object[] { printerName });

                }

            }


            //ps.DefaultPrintQueue("Microsoft Print to PDF");
        }



        //TO SAVE AND PRINT IN ONE STEP
        public void PrintPDFTest()
        {
            System.Windows.Forms.PrintDialog pd = new System.Windows.Forms.PrintDialog();
            System.Windows.Forms.DialogResult userResp = pd.ShowDialog();
            PrintDocument prtDoc = new PrintDocument();
            if (userResp == System.Windows.Forms.DialogResult.OK)
            {
                if (prtDoc.PrinterSettings.PrinterName == "Microsoft Print to PDF")
                {   // force a reasonable filename
                    //string basename = Path.GetFileNameWithoutExtension();
                    //string directory = Path.GetDirectoryName(myFileName);

                    string basename = "TEST DOC";
                    string directory = @"C:\Users\N.Myers\Desktop\Invoice App\";


                    prtDoc.PrinterSettings.PrintToFile = true;
                    // confirm the user wants to use that name
                    System.Windows.Forms.SaveFileDialog pdfSaveDialog = new System.Windows.Forms.SaveFileDialog();
                    pdfSaveDialog.InitialDirectory = directory;
                    pdfSaveDialog.FileName = basename + ".pdf";
                    pdfSaveDialog.Filter = "PDF File|*.pdf";
                    userResp = pdfSaveDialog.ShowDialog();
                    if (userResp != System.Windows.Forms.DialogResult.Cancel)
                        prtDoc.PrinterSettings.PrintFileName = pdfSaveDialog.FileName;
                }

                if (userResp != System.Windows.Forms.DialogResult.Cancel)  // in case they canceled the save as dialog
                {
                    prtDoc.Print();
                }
            }
        }

        // TO Strip out invalid
        // filters control characters but allows only properly-formed surrogate sequences
        private static Regex _invalidXMLChars = new Regex(@"(?<![\uD800-\uDBFF])[\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|[\x00-\x08\x0B\x0C\x0E-\x1F\x7F-\x9F\uFEFF\uFFFE\uFFFF]", RegexOptions.Compiled);


        /// <summary>
        /// removes any unusual unicode characters that can't be encoded into XML
        /// </summary>
        public static string RemoveInvalidXMLChars(string text)
        {
            if (string.IsNullOrEmpty(text)) return "";
            return _invalidXMLChars.Replace(text, "");
        }

        private String filterContent(String content)
        {
            return content.Replace("[^\\u0009\\u000a\\u000d\\u0000-\\uD7FF\\uE000-\\uFFFD]", "");
        }

        // BEST METHOD TO USE
        public void PrintFlowDoc(FlowDocument FlowDoc)
        {
            try
            {



                //Clone the source document
                var str = XamlWriter.Save(FlowDoc);
                str.Replace("\u0000", "");
                str.Replace("\u0001", "");
                str.Replace("\u0002", "");
                //remove any invalid chars before passing
                var stringReader = new System.IO.StringReader(filterContent(str));
                var xmlReader = XmlReader.Create(stringReader);

                var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

                // get information about the dimensions of the seleted printer+media.
                System.Printing.PrintDocumentImageableArea ia = null;

                

                // // Create a local print server
                // LocalPrintServer ps = new LocalPrintServer();

                // // Get the default print queue
                // PrintQueue pq = ps.DefaultPrintQueue;



                // // Get an XpsDocumentWriter for the default print queue
                //= PrintQueue.CreateXpsDocumentWriter(pq);
                // passes in the printqueue CreateXPSDocumentWriter that calls upon a standar print dialog 
                System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);



                if (docWriter != null && ia != null)
                {
                    DocumentPaginator paginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;

                    // Change the PageSize and PagePadding for the document to match the CanvasSize for the printer device.
                    paginator.PageSize = new Size(ia.MediaSizeWidth, ia.MediaSizeHeight);
                    Thickness t = new Thickness(72);  // copy.PagePadding;
                    CloneDoc.PagePadding = new Thickness(
                                     Math.Max(ia.OriginWidth, t.Left),
                                       Math.Max(ia.OriginHeight, t.Top),
                                       Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
                                       Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));



                    CloneDoc.ColumnWidth = double.PositiveInfinity;
                 

                    //SetDefaultPrinterQueue("Microsoft Print to PDF");

                    docWriter.Write(paginator);
                  
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.Message);
            }
        }


        // BEST METHOD TO USE
        public void PrintFlowDoc2(FlowDocument FlowDoc)
        {


            //Clone the source document
            var str = XamlWriter.Save(FlowDoc);
            var stringReader = new System.IO.StringReader(str);
            var xmlReader = XmlReader.Create(stringReader);
            var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

            // get information about the dimensions of the seleted printer+media.
            System.Printing.PrintDocumentImageableArea ia = null;

            // passes in the printqueue CreateXPSDocumentWriter that calls upon a standar print dialog 
            System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);



            if (docWriter != null && ia != null)
            {
                DocumentPaginator paginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;

                // Change the PageSize and PagePadding for the document to match the CanvasSize for the printer device.
                paginator.PageSize = new Size(ia.MediaSizeWidth, ia.MediaSizeHeight);
                Thickness t = new Thickness(72);  // copy.PagePadding;
                CloneDoc.PagePadding = new Thickness(
                                 Math.Max(ia.OriginWidth, t.Left),
                                   Math.Max(ia.OriginHeight, t.Top),
                                   Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
                                   Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));



                CloneDoc.ColumnWidth = double.PositiveInfinity;
                //copy.PageWidth = 528; // allow the page to be the natural with of the output device

                // Send content to the printer.

                //myPrinters.SetDefaultPrinter("Microsoft Print to PDF");

                //SetDefaultPrinterQueue("Microsoft Print to PDF");

                docWriter.Write(paginator);
                //PrintDialog pd = new PrintDialog();
                //pd.


                //System.Windows.Xps.XpsDocumentWriter docWriter2 = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);

                //myPrinters.SetDefaultPrinter("TEST1 (MX-C300W)");

                //docWriter2.Write(paginator);
            }


        }



        // NEW Method 

        public void FlowDocument2Pdf(FlowDocument fd, string SavePath)
        {
            

            //Clone the source document
            var str = XamlWriter.Save(fd);
            var stringReader = new System.IO.StringReader(str);
            var xmlReader = XmlReader.Create(stringReader);
            var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

           // CloneDoc.DataContext = passedVM;

            DocumentPaginator Clonepaginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;
        
            //A4 Dimensions
            Clonepaginator.PageSize = new Size(793, 1122);


            Thickness t = new Thickness(72);  // copy.PagePadding;
            CloneDoc.PagePadding = new Thickness(
                             Math.Max(0, t.Left),
                               Math.Max(0, t.Top),
                               Math.Max(793 - (0 + 793), t.Right),
                               Math.Max(1122 - (0 + 1122), t.Bottom));



            CloneDoc.ColumnWidth = double.PositiveInfinity;

            //End A4 Dimensions

            //Convert Flow Doc to Fixed First
            var paginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;
            var package = Package.Open(new MemoryStream(), FileMode.Create, FileAccess.ReadWrite);
            var packUri = new Uri("pack://temp.xps");
            PackageStore.RemovePackage(packUri);
            PackageStore.AddPackage(packUri, package);
            var xps = new XpsDocument(package, CompressionOption.NotCompressed, packUri.ToString());
            XpsDocument.CreateXpsDocumentWriter(xps).Write(paginator);
            FixedDocument fddoc = xps.GetFixedDocumentSequence().References[0].GetDocument(true);

            //Fixed document to PDF
            var ms = new MemoryStream();
            var package2 = Package.Open(ms, FileMode.Create);
            var doc = new XpsDocument(package2);
            var writer = XpsDocument.CreateXpsDocumentWriter(doc);
            writer.Write(fddoc.DocumentPaginator);

            //docWriter.Write(fddoc.DocumentPaginator);
            doc.Close();
            package2.Close();

            // Get XPS file bytes
            var bytes = ms.ToArray();
            ms.Dispose();

            //Set save path
            var outputFilePath = SavePath;

            
                // Print to PDF
                PdfFilePrinter.PrintXpsToPdf(bytes, outputFilePath, "Document Title");
            
           


        }


        //WORKS WITH DATA BINDING
        public void FlowDocument2PdfServerReports(FlowDocument fd, string SavePath)
        {
          

            //Clone the source document
            var str = XamlWriter.Save(fd);
            var stringReader = new System.IO.StringReader(str);
            var xmlReader = XmlReader.Create(stringReader);
            var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

            DocumentPaginator Clonepaginator = ((IDocumentPaginatorSource)fd).DocumentPaginator;

       
            Thickness t = new Thickness(72);  // copy.PagePadding;
           fd.PagePadding = new Thickness(
                             Math.Max(0, t.Left),
                               Math.Max(0, t.Top),
                               Math.Max(793 - (0 + 793), t.Right),
                               Math.Max(1122 - (0 + 1122), t.Bottom));
            


            fd.ColumnWidth = double.PositiveInfinity;
            
           
            //End A4 Dimensions

            //Convert Flow Doc to Fixed First
            var paginator = ((IDocumentPaginatorSource)fd).DocumentPaginator;
            var package = Package.Open(new MemoryStream(), FileMode.Create, FileAccess.ReadWrite);
            var packUri = new Uri("pack://temp.xps");
            PackageStore.RemovePackage(packUri);
            PackageStore.AddPackage(packUri, package);
            var xps = new XpsDocument(package, CompressionOption.NotCompressed, packUri.ToString());
            XpsDocument.CreateXpsDocumentWriter(xps).Write(paginator);
            FixedDocument fddoc = xps.GetFixedDocumentSequence().References[0].GetDocument(true);

           

            //Fixed document to PDF
            var ms = new MemoryStream();
            var package2 = Package.Open(ms, FileMode.Create);
            var doc = new XpsDocument(package2);
            var writer = XpsDocument.CreateXpsDocumentWriter(doc);
            writer.Write(fddoc.DocumentPaginator);
          
            //docWriter.Write(fddoc.DocumentPaginator);
                doc.Close();
                package2.Close();

                // Get XPS file bytes
                var bytes = ms.ToArray();
                ms.Dispose();

            // Print to PDF
            var outputFilePath = SavePath;
            PdfFilePrinter.PrintXpsToPdf(bytes, outputFilePath, "Document Title");


        }

        public static FlowDocument Clone(FlowDocument document)

        {

            TextRange sourceDocument = new TextRange(document.ContentStart, document.ContentEnd);

            MemoryStream stream = new MemoryStream();

            sourceDocument.Save(stream, DataFormats.XamlPackage);

            FlowDocument flowDocumentCopy = new FlowDocument();

            TextRange copyDocumentRange = new TextRange(flowDocumentCopy.ContentStart, flowDocumentCopy.ContentEnd);

            copyDocumentRange.Load(stream, DataFormats.XamlPackage);

            return flowDocumentCopy;

        }



        public static FixedDocument CreateFixedDocument(double pageWidth, double pageHeight, UIElement content)
        {
            if (content == null)
            {
                throw new ArgumentNullException("content");
            }

            if (pageWidth <= 0 || pageHeight <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            //RenderTargetBitmap renderTarget = new RenderTargetBitmap(Convert.ToInt32(content.RenderSize.Width), Convert.ToInt32(content.RenderSize.Height),
            //    Constants.ScreenDPI, Constants.ScreenDPI, PixelFormats.Pbgra32);
            //renderTarget.Render(content);

            FixedDocument doc = new FixedDocument();
            Size pageSize = new Size(pageWidth, pageHeight);
            doc.DocumentPaginator.PageSize = pageSize;
            FixedPage fixedPage = new FixedPage();
            fixedPage.Width = pageWidth;
            fixedPage.Height = pageHeight;
            //Image image = new Image();
            //image.Height = pageHeight;
            //image.Width = pageWidth;
            //image.Stretch = Stretch.Uniform;
            //image.StretchDirection = StretchDirection.Both;
            //image.Source = renderTarget;
           
            TextBlock myTB = new TextBlock();
            myTB.Text = "TEST PAGE";
            fixedPage.Children.Add(myTB);


            PageContent pageContent = new PageContent();
            ((IAddChild)pageContent).AddChild(fixedPage);
            doc.Pages.Add(pageContent);
            return doc;
        }







        public static void FixedDocument2Pdf(FixedDocument fd)
        {
            // Convert FixedDocument to XPS file in memory
            var ms = new MemoryStream();
            var package = Package.Open(ms, FileMode.Create);
            var doc = new XpsDocument(package);
            var writer = XpsDocument.CreateXpsDocumentWriter(doc);
            writer.Write(fd.DocumentPaginator);
            doc.Close();
            package.Close();

            // Get XPS file bytes
            var bytes = ms.ToArray();
            ms.Dispose();

            // Print to PDF
            var outputFilePath = @"C:\tmp\test.pdf";
            PdfFilePrinter.PrintXpsToPdf(bytes, outputFilePath, "Document Title");
        }



















        // used to call on the Microsoft PDF Document to prompt saving then another call to print to the default printer
        public void SaveDocAndPrint(FlowDocument flowdocument)
        {
            try
            {

          
            //MessageBox.Show(vm.ReportBody);

            DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

            // dpw.PrintDocs(System.Printing.PageMediaSizeName.ISOA4);

            //Get the current Default Printer
            string DefPrinter = DocumentPaginatorWrapper.GetDefaultPrinter();

            //MessageBox.Show(DocumentPaginatorWrapper.GetDefaultPrinter());

            //Set Default temp to the Microsoft PDF Virtual printer
            DocumentPaginatorWrapper.SetDefaultPrinterQueue("Microsoft Print to PDF");

            // print to PDF
            dpw.PrintFlowDoc(flowdocument);

            //delay between switching of default printers
            //await PutTaskDelay(1000);


            //MessageBox.Show(DocumentPaginatorWrapper.GetDefaultPrinter());

            //Set default printer back to Original
            DocumentPaginatorWrapper.SetDefaultPrinterQueue(DefPrinter);

            //print again, this time to a printer
            dpw.PrintFlowDoc(flowdocument);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            // PrintPDFTest();
        }

        public void PrintDoc(FlowDocument flowdocument)
        {
            try
            {


                //MessageBox.Show(vm.ReportBody);

                DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

                // dpw.PrintDocs(System.Printing.PageMediaSizeName.ISOA4);

                //Get the current Default Printer
                string DefPrinter = DocumentPaginatorWrapper.GetDefaultPrinter();

                //MessageBox.Show(DocumentPaginatorWrapper.GetDefaultPrinter());

                ////Set Default temp to the Microsoft PDF Virtual printer
                //DocumentPaginatorWrapper.SetDefaultPrinterQueue("Microsoft Print to PDF");

                //// print to PDF
                //dpw.PrintFlowDoc(flowdocument);

                //delay between switching of default printers
                //await PutTaskDelay(1000);


                //MessageBox.Show(DocumentPaginatorWrapper.GetDefaultPrinter());

                //Set default printer back to Original
                DocumentPaginatorWrapper.SetDefaultPrinterQueue(DefPrinter);

                //print again, this time to a printer
                dpw.PrintFlowDoc(flowdocument);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            // PrintPDFTest();
        }

        // used to call on the Microsoft PDF Document to prompt saving then another call to print to the default printer
        public void SaveDocAndPrintITAD(FlowDocument flowdocument)
        {
            try
            {
                    

                DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

                // dpw.PrintDocs(System.Printing.PageMediaSizeName.ISOA4);

                //Get the current Default Printer
                string DefPrinter = DocumentPaginatorWrapper.GetDefaultPrinter();


                //Set default printer back to Original
                //DocumentPaginatorWrapper.SetDefaultPrinterQueue(DefPrinter);

                //Set Default temp to the Microsoft PDF Virtual printer
                DocumentPaginatorWrapper.SetDefaultPrinterQueue("Microsoft Print to PDF");

                //print again, this time to a printer
                dpw.PrintFlowDoc(flowdocument);


                //Set default printer back to Original
                DocumentPaginatorWrapper.SetDefaultPrinterQueue(DefPrinter);


            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            // PrintPDFTest();
        }

        public void SaveDocPDF(FlowDocument flowdocument)
        {
            try
            {

           

            DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

            // dpw.PrintDocs(System.Printing.PageMediaSizeName.ISOA4);

            //Get the current Default Printer
            string DefPrinter = DocumentPaginatorWrapper.GetDefaultPrinter();

            //MessageBox.Show(DocumentPaginatorWrapper.GetDefaultPrinter());

            //Set Default temp to the Microsoft PDF Virtual printer
            DocumentPaginatorWrapper.SetDefaultPrinterQueue("Microsoft Print to PDF");
            
            
            // print to PDF
            dpw.PrintFlowDoc(flowdocument);
            

            //delay between switching of default printers
            //await PutTaskDelay(1000);


            //MessageBox.Show(DocumentPaginatorWrapper.GetDefaultPrinter());

            //Set default printer back to Original
            DocumentPaginatorWrapper.SetDefaultPrinterQueue(DefPrinter);

                //print again, this time to a printer
                //dpw.PrintFlowDoc(flowdocument);

                // PrintPDFTest();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.Message);
            }


        }


        public void SaveDocGOTPDF(FlowDocument fd,string SavePath)
        {
           
            ////Change mouse cursor 
            Mouse.OverrideCursor = Cursors.Wait;

            //Clone the source document
            var str = XamlWriter.Save(fd);
            var stringReader = new System.IO.StringReader(str);
            var xmlReader = XmlReader.Create(stringReader);
            var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

            DocumentPaginator Clonepaginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;

            //A4 Dimensions
            Clonepaginator.PageSize = new Size(1122, 793);

            //PageMediaSize pageSize = null;
            //pageSize = new PageMediaSize(PageMediaSizeName.ISOA4);

            //  MessageBox.Show(pageSize.Width.ToString() + " X " + pageSize.Height.ToString());


            Thickness t = new Thickness(72);  // copy.PagePadding;
            CloneDoc.PagePadding = new Thickness(
                             Math.Max(0, t.Left),
                               Math.Max(0, t.Top),
                             Math.Max(1122 - (0 + 1122), t.Right),
                             Math.Max(793 - (0 + 793), t.Bottom));



            CloneDoc.ColumnWidth = double.PositiveInfinity;

            //End A4 Dimensions

            //Convert Flow Doc to Fixed First
            var paginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;
            var package = Package.Open(new MemoryStream(), FileMode.Create, FileAccess.ReadWrite);
            var packUri = new Uri("pack://temp.xps");
            PackageStore.RemovePackage(packUri);
            PackageStore.AddPackage(packUri, package);
            var xps = new XpsDocument(package, CompressionOption.NotCompressed, packUri.ToString());
            XpsDocument.CreateXpsDocumentWriter(xps).Write(paginator);
            FixedDocument fddoc = xps.GetFixedDocumentSequence().References[0].GetDocument(true);

            //Fixed document to PDF
            var ms = new MemoryStream();
            var package2 = Package.Open(ms, FileMode.Create);
            var doc = new XpsDocument(package2);
            var writer = XpsDocument.CreateXpsDocumentWriter(doc);
            writer.Write(fddoc.DocumentPaginator);

            //docWriter.Write(fddoc.DocumentPaginator);
            doc.Close();
            package2.Close();

            // Get XPS file bytes
            var bytes = ms.ToArray();
            ms.Dispose();

            // Print to PDF
            var outputFilePath = SavePath;
            PdfFilePrinter.PrintXpsToPdf(bytes, outputFilePath, "Document Title");


            //PART ONE FIXED DOC 
            //FixedDocument fddoc = new FixedDocument();
            //Size pageSize = new Size(700, 1100);
            //fddoc.DocumentPaginator.PageSize = pageSize;
            //FixedPage fixedPage = new FixedPage();
            //fixedPage.Width = 700;
            //fixedPage.Height = 1100;
            ////Image image = new Image();
            ////image.Height = pageHeight;
            ////image.Width = pageWidth;
            ////image.Stretch = Stretch.Uniform;
            ////image.StretchDirection = StretchDirection.Both;
            ////image.Source = renderTarget;

            //TextBlock myTB = new TextBlock();
            //myTB.Text = "TEST PAGE";
            //fixedPage.Children.Add(myTB);


            //PageContent pageContent = new PageContent();
            //((IAddChild)pageContent).AddChild(fixedPage);
            //fddoc.Pages.Add(pageContent);
            //END FIRST PART

            //2ND PART


            ////MessageBox.Show(vm.ReportBody);

            //DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

            ////dpw.PrintDocs(System.Printing.PageMediaSizeName.ISOA4);


            ////Get the current Default Printer
            //string DefPrinter = DocumentPaginatorWrapper.GetDefaultPrinter();

            ////MessageBox.Show(DocumentPaginatorWrapper.GetDefaultPrinter());

            ////Set Default temp to the Microsoft PDF Virtual printer
            //DocumentPaginatorWrapper.SetDefaultPrinterQueue("Microsoft Print to PDF");


            //// print to PDF
            //dpw.PrintFlowDoc(flowdocument);


            ////delay between switching of default printers
            ////await PutTaskDelay(1000);


            ////MessageBox.Show(DocumentPaginatorWrapper.GetDefaultPrinter());

            ////Set default printer back to Original
            //DocumentPaginatorWrapper.SetDefaultPrinterQueue(DefPrinter);

            ////print again, this time to a printer
            ////dpw.PrintFlowDoc(flowdocument);

            //// PrintPDFTest();
        }

        public void SaveCollectionDocPDF(FlowDocument flowdocument)
        {
          

            DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();
            //Set to A4
            dpw.PrintDocs(System.Printing.PageMediaSizeName.ISOA4);

            //Get the current Default Printer
            string DefPrinter = DocumentPaginatorWrapper.GetDefaultPrinter();

          
            //Set Default temp to the Microsoft PDF Virtual printer
            DocumentPaginatorWrapper.SetDefaultPrinterQueue("Microsoft Print to PDF");

            //Create Print Dialog
            PrintDialog printDlg = new PrintDialog();

            //Arrange Size
            flowdocument.PagePadding = new Thickness(40);
            flowdocument.PageWidth = 793;// a4 width in px
            flowdocument.PageHeight = 1100;
            //IMPORTANT For document to fit PDF
            flowdocument.ColumnWidth = flowdocument.PageWidth;

            //Set Flowdocument as Paginator sOURCE
            IDocumentPaginatorSource idpSource = flowdocument;

           
            //Show Print Dialog
            if (printDlg.ShowDialog() == true)
            {
                printDlg.PrintDocument(idpSource.DocumentPaginator, "Test Certificate");
            }


            //Set default printer back to Original
            DocumentPaginatorWrapper.SetDefaultPrinterQueue(DefPrinter);

        
        }

        //private StreamReader streamToPrint;

        public void PrintToPDF()
        {

            // generate a file name as the current date/time in unix timestamp format
            string file = (string)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();

            // the directory to store the output.
            string directory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            // initialize PrintDocument object
            PrintDocument doc = new PrintDocument()
            {
                PrinterSettings = new PrinterSettings()
                {
                    // set the printer to 'Microsoft Print to PDF'
                    PrinterName = "Microsoft Print to PDF",

                    // tell the object this document will print to file
                    PrintToFile = true,

                    // set the filename to whatever you like (full path)
                    PrintFileName = Path.Combine(directory, file + ".pdf"),
                }
            };
            doc.DocumentName = "";
            doc.Print();
        }


        public void PrintDirect(string printerName)
        {

            //// generate a file name as the current date/time in unix timestamp format
            //string file = (string)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();

            //// the directory to store the output.
            //string directory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            // initialize PrintDocument object
            PrintDocument doc = new PrintDocument()
            {
                PrinterSettings = new PrinterSettings()
                {
                    // set the printer to 'Microsoft Print to PDF'
                    PrinterName = printerName,

                    // tell the object this document will print to file
                   // PrintToFile = true,

                    // set the filename to whatever you like (full path)
                   // PrintFileName = "Label",
                }
            };
            doc.DocumentName = "Nebula Product Label";
            doc.Print();
        }



        public static Boolean send4PrintFlow(List<string> printList, string printTitle)
        {
            // Create the print dialog object and set options
            PrintDialog pDialog = new PrintDialog();
            pDialog.PageRangeSelection = PageRangeSelection.AllPages;
            pDialog.UserPageRangeEnabled = true;

            // Display the dialog. This returns true if the user presses the Print button.
            Nullable<Boolean> print = pDialog.ShowDialog();
            if (print == true)
            {

                foreach (string doc2P in printList)
                {
                    XpsDocument xpsDocument = new XpsDocument(doc2P, FileAccess.ReadWrite);
                    FixedDocumentSequence fixedDocSeq = xpsDocument.GetFixedDocumentSequence();
                    pDialog.PrintDocument(fixedDocSeq.DocumentPaginator, printTitle);
                }

                return true;

            }
            else if (print == false)
            {
                // printdialog was cancelled
                return false;
            }

            return false;
        }




        //private void listAllPrinters()
        //{
        //    foreach (var item in PrinterSettings.InstalledPrinters)
        //    {
        //        this.listBox1.Items.Add(item.ToString());
        //    }
        //}

        //private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        //{
        //    string pname = this.listBox1.SelectedItem.ToString();
        //    myPrinters.SetDefaultPrinter(pname);
        //}


        //public void Form1()
        //{
        //    //InitializeComponent();
        //    listAllPrinters();
        //}

        public static class myPrinters
        {
            [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
            public static extern bool SetDefaultPrinter(string Name);

          
        }





        public void PrintStandardVisual(Visual TheVisual)
        {
            PrintDialog dlg = new PrintDialog();
            if (dlg.ShowDialog() == true)
            {
                dlg.PrintVisual(TheVisual, "Print Button");
                //dlg.Prin
            }
        }

        //public void PrintDirectVisualZEBRA(Visual TheVisual, string printQueue, string numberofCopies, PageOrientation orientation)
        //{

        //    System.Windows.FrameworkElement e = TheVisual as System.Windows.FrameworkElement;
        //    if (e == null)
        //        return;

        //    PrintDialog dlg = new PrintDialog();

        //    string networkLocation = "";

        //    //StackPanel VisualToPrint = new StackPanel();

        //    //VisualToPrint = (StackPanel)TheVisual;



        //    if (printQueue.Contains(@"\"))
        //    {
        //        //Network printer


        //        //Strip out network portion

        //        //strip out the printer name portion
        //        string stripOut = printQueue.Substring(printQueue.LastIndexOf('\\'));
        //        //Extract the network portion
        //        networkLocation = printQueue.Replace(stripOut, "");
        //        //Extract the printer name
        //        printQueue = printQueue.Replace(networkLocation + @"\", "");
        //        dlg.PrintQueue = new PrintQueue(new PrintServer(networkLocation), printQueue); //printQueue; // this will be your printer. any of these: new PrintServer().GetPrintQueues()

        //        //DefaultPrinterCB.Items.Add(pkInstalledPrinters.Substring(pkInstalledPrinters.LastIndexOf('\\') + 1));
        //    }
        //    else
        //    {
        //        //Local Printer
        //        dlg.PrintQueue = new PrintQueue(new PrintServer(), printQueue); //printQueue; // this will be your printer. any of these: new PrintServer().GetPrintQueues()
        //        //DefaultPrinterCB.Items.Add(pkInstalledPrinters);
        //    }


        //    dlg.PrintTicket.CopyCount = Convert.ToInt32(numberofCopies); // number of copies
        //    dlg.PrintTicket.PageOrientation = orientation; //PageOrientation.Landscape;



        //    //store original scale
        //    Transform originalScale = e.LayoutTransform;
        //    //get selected printer capabilities
        //    System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);

        //    //get scale of the print wrt to screen of WPF visual
        //   // double scale = Math.Min(capabilities.PageImageableArea.ExtentWidth / e.ActualWidth, capabilities.PageImageableArea.ExtentHeight / e.ActualHeight);

        //    double scale = Math.Min(e.ActualWidth /e.ActualWidth, e.ActualHeight / e.ActualHeight);
        //    ////MessageBox.Show(scale.ToString());

        //    ////Transform the Visual to scale
        //    //e.LayoutTransform = new ScaleTransform(scale, scale);

        //    //Declare Size Var
        //    System.Windows.Size sz;


        //        //get the size of the printer page   Added custom scale 
        //    sz = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth / 1.05, capabilities.PageImageableArea.ExtentHeight / 1.6 );

        //    //sz = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);

        //    //MessageBox.Show(sz.Width + " x " + sz.Height);

        //    //update the layout of the visual to the printer page size.
        //    e.Measure(sz);
        //    //Arrange the Visual and put an offset of 6 for the Height
        //    e.Arrange(new System.Windows.Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight + 4), sz));




        //    //now print the visual to printer to fit on the one page.
        //    dlg.PrintVisual(e, "Nebula Label");

        //    //apply the original transform.
        //    e.LayoutTransform = originalScale;

        //    //dlg.PrintTicket.PageMediaType.
        //    //if (dlg.ShowDialog() == true)
        //    //{
        //    // dlg.PrintVisual(VisualToPrint, "Nebula Label");
        //    //dlg.Prin
        //    // }
        //}

        public void PrintDirectVisualZEBRA(Visual TheVisual, string printQueue, string numberofCopies, PageOrientation orientation, string selectedLabel)
        {

            try
            {

          

            System.Windows.FrameworkElement e = TheVisual as System.Windows.FrameworkElement;
            if (e == null)
                return;



            PrintDialog dlg = new PrintDialog();



            string networkLocation = "";

            // dlg.ShowDialog()
            //PrintServer ps = new PrintServer();
            //ps.
            //dlg.PrintQueue.ho = printQueue.

            //MessageBox.Show(printQueue);

            if (printQueue.Contains(@"\"))
            {
                //Network printer


                //Strip out network portion

                //strip out the printer name portion
                string stripOut = printQueue.Substring(printQueue.LastIndexOf('\\'));
                //Extract the network portion
                networkLocation = printQueue.Replace(stripOut, "");
                //Extract the printer name
                printQueue = printQueue.Replace(networkLocation + @"\", "");
                dlg.PrintQueue = new PrintQueue(new PrintServer(networkLocation), printQueue); //printQueue; // this will be your printer. any of these: new PrintServer().GetPrintQueues()

                //DefaultPrinterCB.Items.Add(pkInstalledPrinters.Substring(pkInstalledPrinters.LastIndexOf('\\') + 1));
            }
            else
            {
                //Local Printer
                dlg.PrintQueue = new PrintQueue(new PrintServer(), printQueue); //printQueue; // this will be your printer. any of these: new PrintServer().GetPrintQueues()
                //DefaultPrinterCB.Items.Add(pkInstalledPrinters);
            }


            dlg.PrintTicket.CopyCount = Convert.ToInt32(numberofCopies); // number of copies
            dlg.PrintTicket.PageOrientation = orientation; //PageOrientation.Landscape;


            ////store original scale
            Transform originalScale = e.LayoutTransform;


            double scale = 0;
            ////get selected printer capabilities
            System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);

            //get scale of the print wrt to screen of WPF visual
            //Small but fits
            if (selectedLabel.Contains("Zebra - Asset Label"))
            {
               
                scale = Math.Min(e.ActualWidth / e.ActualWidth, e.ActualHeight / e.ActualHeight);
                
            }
            else
            {
                scale = Math.Min(e.ActualWidth / e.ActualWidth, e.ActualHeight / e.ActualHeight);
            }
             
          

            //////Transform the Visual to scale
            e.LayoutTransform = new ScaleTransform(scale, scale);

            ////Declare Size Var
            System.Windows.Size sz;


            //get the size of the printer page
                      
            if(selectedLabel.Contains("Zebra - Asset Label"))
            {
                   // sz = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth / 0.0, capabilities.PageImageableArea.ExtentHeight + 10.0);

                    //MessageBox.Show(new System.Windows.Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight + 10), sz).ToString());
                    sz = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth / 1.05, capabilities.PageImageableArea.ExtentHeight / 1.0);

                    //MessageBox.Show(new System.Windows.Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight + 5), sz).ToString());
                    //sz = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                    //sz = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth / 1.05, capabilities.PageImageableArea.ExtentHeight / 1.6);
                    //update the layout of the visual to the printer page size.
                    e.Measure(sz);
                    e.Arrange(new System.Windows.Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight + 5), sz));
            }
            else
            {
                    sz = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth / 1.05, capabilities.PageImageableArea.ExtentHeight / 1.6);
                    //sz = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
                    //update the layout of the visual to the printer page size.
                    e.Measure(sz);
                    e.Arrange(new System.Windows.Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight + 5), sz));
            }



                //if (dlg.ShowDialog() == true)
                //{
                dlg.PrintVisual(e, "Nebula Label");
                // }
               
                //If asset label then don't apply original scale to visual
                if (selectedLabel.Contains("Zebra - Asset Label"))
                {
                    ////apply the original transform.
                    e.LayoutTransform = originalScale;
                }
                else
                {
                    ////apply the original transform.
                    e.LayoutTransform = originalScale;
                }
               

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void PrintDirectVisualDYMOBROTHER(Visual TheVisual, string printQueue, string numberofCopies, PageOrientation orientation)
        {

            try
            {

         
            System.Windows.FrameworkElement e = TheVisual as System.Windows.FrameworkElement;
            if (e == null)
                return;



            PrintDialog dlg = new PrintDialog();

           

            string networkLocation = "";

            // dlg.ShowDialog()
            //PrintServer ps = new PrintServer();
            //ps.
            //dlg.PrintQueue.ho = printQueue.

            //MessageBox.Show(printQueue);

            if (printQueue.Contains(@"\"))
            {
                //Network printer


                //Strip out network portion

                //strip out the printer name portion
                string stripOut = printQueue.Substring(printQueue.LastIndexOf('\\'));
                //Extract the network portion
                networkLocation = printQueue.Replace(stripOut,"");
                //Extract the printer name
                printQueue = printQueue.Replace(networkLocation + @"\", "");
                dlg.PrintQueue = new PrintQueue(new PrintServer(networkLocation), printQueue); //printQueue; // this will be your printer. any of these: new PrintServer().GetPrintQueues()

                //DefaultPrinterCB.Items.Add(pkInstalledPrinters.Substring(pkInstalledPrinters.LastIndexOf('\\') + 1));
            }
            else
            {
                //Local Printer
                dlg.PrintQueue = new PrintQueue(new PrintServer(), printQueue); //printQueue; // this will be your printer. any of these: new PrintServer().GetPrintQueues()
                //DefaultPrinterCB.Items.Add(pkInstalledPrinters);
            }

          
            dlg.PrintTicket.CopyCount = Convert.ToInt32(numberofCopies); // number of copies
            dlg.PrintTicket.PageOrientation = orientation; //PageOrientation.Landscape;


            ////store original scale
            Transform originalScale = e.LayoutTransform;
            ////get selected printer capabilities
            System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);

                ////get scale of the print wrt to screen of WPF visual
                /////Small but fits
                double scale = Math.Min(capabilities.PageImageableArea.ExtentWidth / e.ActualWidth, capabilities.PageImageableArea.ExtentHeight / e.ActualHeight);
                //double scale = Math.Min(capabilities.PageImageableArea.ExtentWidth / 1.05, capabilities.PageImageableArea.ExtentHeight / 1.6);

                //////Transform the Visual to scale / 1.05
               e.LayoutTransform = new ScaleTransform(scale, scale);

                ////Declare Size Var
                System.Windows.Size sz;


                //get the size of the printer page

                sz = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);

                //MessageBox.Show(sz.Width + " x " + sz.Height);

                ////update the layout of the visual to the printer page size.
                e.Measure(sz);
                e.Arrange(new System.Windows.Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), sz));

               

                //dlg.PrintTicket.PageMediaType.
                //if (dlg.ShowDialog() == true)
                //{
                dlg.PrintVisual(e, "Nebula Label");
            //dlg.Prin
            // }

            //apply the original transform.
            e.LayoutTransform = originalScale;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        //public void PrintDirectVisualDYMOBROTHER(Visual TheVisual, string printQueue, string numberofCopies, PageOrientation orientation)
        //{

        //    try
        //    {


        //        System.Windows.FrameworkElement e = TheVisual as System.Windows.FrameworkElement;
        //        if (e == null)
        //            return;



        //        PrintDialog dlg = new PrintDialog();



        //        string networkLocation = "";

        //        // dlg.ShowDialog()
        //        //PrintServer ps = new PrintServer();
        //        //ps.
        //        //dlg.PrintQueue.ho = printQueue.

        //        //MessageBox.Show(printQueue);

        //        if (printQueue.Contains(@"\"))
        //        {
        //            //Network printer


        //            //Strip out network portion

        //            //strip out the printer name portion
        //            string stripOut = printQueue.Substring(printQueue.LastIndexOf('\\'));
        //            //Extract the network portion
        //            networkLocation = printQueue.Replace(stripOut, "");
        //            //Extract the printer name
        //            printQueue = printQueue.Replace(networkLocation + @"\", "");
        //            dlg.PrintQueue = new PrintQueue(new PrintServer(networkLocation), printQueue); //printQueue; // this will be your printer. any of these: new PrintServer().GetPrintQueues()

        //            //DefaultPrinterCB.Items.Add(pkInstalledPrinters.Substring(pkInstalledPrinters.LastIndexOf('\\') + 1));
        //        }
        //        else
        //        {
        //            //Local Printer
        //            dlg.PrintQueue = new PrintQueue(new PrintServer(), printQueue); //printQueue; // this will be your printer. any of these: new PrintServer().GetPrintQueues()
        //                                                                            //DefaultPrinterCB.Items.Add(pkInstalledPrinters);
        //        }


        //        dlg.PrintTicket.CopyCount = Convert.ToInt32(numberofCopies); // number of copies
        //        dlg.PrintTicket.PageOrientation = orientation; //PageOrientation.Landscape;


        //        ////store original scale
        //        Transform originalScale = e.LayoutTransform;
        //        ////get selected printer capabilities
        //        System.Printing.PrintCapabilities capabilities = dlg.PrintQueue.GetPrintCapabilities(dlg.PrintTicket);

        //        ////get scale of the print wrt to screen of WPF visual
        //        /////Small but fits
        //        double scale = Math.Min(capabilities.PageImageableArea.ExtentWidth / e.ActualWidth, capabilities.PageImageableArea.ExtentHeight / e.ActualHeight);
        //        //double scale = Math.Min(capabilities.PageImageableArea.ExtentWidth / 1.05, capabilities.PageImageableArea.ExtentHeight / 1.6);

        //        //////Transform the Visual to scale / 1.05
        //        e.LayoutTransform = new ScaleTransform(scale, scale);

        //        ////Declare Size Var
        //        System.Windows.Size sz;


        //        //get the size of the printer page

        //        sz = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);

        //        //MessageBox.Show(sz.Width + " x " + sz.Height);

        //        ////update the layout of the visual to the printer page size.
        //        e.Measure(sz);
        //        e.Arrange(new System.Windows.Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), sz));



        //        //dlg.PrintTicket.PageMediaType.
        //        //if (dlg.ShowDialog() == true)
        //        //{
        //        dlg.PrintVisual(e, "Nebula Label");
        //        //dlg.Prin
        //        // }

        //        //apply the original transform.
        //        e.LayoutTransform = originalScale;

        //    }
        //    catch (Exception ex)
        //    {

        //        MessageBox.Show(ex.Message);
        //    }
        //}


        public void PrintVisual(FixedPage FPage)
        {
            PrintDialog dlg = new PrintDialog();
            if (dlg.ShowDialog() == true)
            {
                dlg.PrintVisual(FPage, "Print Button");
            }
        }

        //for cloning an already created xaml element
        public static T CloneXaml<T>(T source)
        {
            string xaml = XamlWriter.Save(source);
            StringReader sr = new StringReader(xaml);
            XmlReader xr = XmlReader.Create(sr);
            //MessageBox.Show(XamlReader.Load(xr).ToString());
            return (T)XamlReader.Load(xr);
        }

        public void PrintFixedDoc(FixedPage FPage)
        {

            // select printer and get printer settings
            PrintDialog pd = new PrintDialog();
            if (pd.ShowDialog() != true) return;
            // create a document
            FixedDocument document = new FixedDocument(); //FDoc;
            document.DocumentPaginator.PageSize = new Size(pd.PrintableAreaWidth, pd.PrintableAreaHeight);
            // add the page to the document
            PageContent page1Content = new PageContent();
            FixedPage MyFP = new FixedPage();
            //make a copy of the XAML ELEMENT
            var copy = CloneXaml(FPage);
            ((IAddChild)page1Content).AddChild(copy);
            document.Pages.Add(page1Content);
            // and print
            pd.PrintDocument(document.DocumentPaginator, "Invoice");



        }



        public static FixedDocument GetFixedDocument(FrameworkElement toPrint, PrintDialog printDialog, Thickness margin)
        {
            PrintCapabilities capabilities = printDialog.PrintQueue.GetPrintCapabilities(printDialog.PrintTicket);
            Size pageSize = new Size(printDialog.PrintableAreaWidth, printDialog.PrintableAreaHeight);
            Size visibleSize = new Size(capabilities.PageImageableArea.ExtentWidth - margin.Left - margin.Right, capabilities.PageImageableArea.ExtentHeight - margin.Top - margin.Bottom);
            FixedDocument fixedDoc = new FixedDocument();
            //If the toPrint visual is not displayed on screen we neeed to measure and arrange it   
            toPrint.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            toPrint.Arrange(new Rect(new Point(0, 0), toPrint.DesiredSize));
            //   
            Size size = toPrint.DesiredSize;
            //Will assume for simplicity the control fits horizontally on the page   
            double yOffset = 0;
            while (yOffset < size.Height)
            {
                VisualBrush vb = new VisualBrush(toPrint);
                vb.Stretch = Stretch.None;
                vb.AlignmentX = AlignmentX.Left;
                vb.AlignmentY = AlignmentY.Top;
                vb.ViewboxUnits = BrushMappingMode.Absolute;
                vb.TileMode = TileMode.None;
                vb.Viewbox = new Rect(0, yOffset, visibleSize.Width, visibleSize.Height);

                PageContent pageContent = new PageContent();
                FixedPage page = new FixedPage();
                ((IAddChild)pageContent).AddChild(page);
                fixedDoc.Pages.Add(pageContent);
                page.Width = pageSize.Width;
                page.Height = pageSize.Height;
                Canvas canvas = new Canvas();
                FixedPage.SetLeft(canvas, capabilities.PageImageableArea.OriginWidth);
                FixedPage.SetTop(canvas, capabilities.PageImageableArea.OriginHeight);
                canvas.Width = visibleSize.Width;
                canvas.Height = visibleSize.Height;
                canvas.Background = vb;
                canvas.Margin = margin;

                page.Children.Add(canvas);
                yOffset += visibleSize.Height;
            }
            return fixedDoc;
        }


        public static void ShowPrintPreview(FixedDocument fixedDoc)
        {
            var wnd = new Window();
            var viewer = new DocumentViewer();
            viewer.Document = fixedDoc;
            wnd.Content = viewer;
            wnd.ShowDialog();
        }

        public static void PrintNoPreview(PrintDialog printDialog, FixedDocument fixedDoc)
        {
            printDialog.PrintDocument(fixedDoc.DocumentPaginator, "Test Print No Preview");

        }


        public void CodeBehindFlowDocumentSample()
        {
            //https://www.wpf-tutorial.com/rich-text-controls/creating-flowdocument-from-code-behind/
            //https://www.wpf-tutorial.com/rich-text-controls/advanced-flowdocument-content/
            //https://www.wpf-tutorial.com/rich-text-controls/richtextbox-control/
            // Stack panel add margin
            //https://stackoverflow.com/questions/932510/how-do-i-space-out-the-child-elements-of-a-stackpanel

            FlowDocument doc = new FlowDocument();

            Paragraph p = new Paragraph(new Run("Hello, world!"));
            p.FontSize = 36;
            doc.Blocks.Add(p);

            p = new Paragraph(new Run("The ultimate programming greeting!"));
            p.FontSize = 14;
            p.FontStyle = FontStyles.Italic;
            p.TextAlignment = TextAlignment.Left;
            p.Foreground = Brushes.Gray;
            doc.Blocks.Add(p);

            //Load Document to the FlowDocument viewer control
            //fdViewer.Document = doc;
        }


        public void PrintFlowDocOld(FlowDocument FlowDoc)
        {
            //Clone the source document
            var str = XamlWriter.Save(FlowDoc);
            var stringReader = new System.IO.StringReader(str);
            var xmlReader = XmlReader.Create(stringReader);
            var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

            //Now print using PrintDialog
            var pd = new PrintDialog();

            if (pd.ShowDialog().Value)
            {
                CloneDoc.PageHeight = pd.PrintableAreaHeight;
                CloneDoc.PageWidth = pd.PrintableAreaWidth;
                IDocumentPaginatorSource idocument = CloneDoc as IDocumentPaginatorSource;

              

                pd.PrintDocument(idocument.DocumentPaginator, "Printing FlowDocument");
            }
        }




        public void DoThePrint(FlowDocument document)
        {
            // Clone the source document's content into a new FlowDocument.
            // This is because the pagination for the printer needs to be
            // done differently than the pagination for the displayed page.
            // We print the copy, rather that the original FlowDocument.
            System.IO.MemoryStream s = new System.IO.MemoryStream();
            TextRange source = new TextRange(document.ContentStart, document.ContentEnd);

            source.Save(s, DataFormats.Xaml);
            FlowDocument copy = new FlowDocument();
            TextRange dest = new TextRange(copy.ContentStart, copy.ContentEnd);
            dest.Load(s, DataFormats.Xaml);

            // Create a XpsDocumentWriter object, implicitly opening a Windows common print dialog,
            // and allowing the user to select a printer.

            // get information about the dimensions of the seleted printer+media.
            System.Printing.PrintDocumentImageableArea ia = null;
            System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);

            if (docWriter != null && ia != null)
            {
                DocumentPaginator paginator = ((IDocumentPaginatorSource)copy).DocumentPaginator;

                // Change the PageSize and PagePadding for the document to match the CanvasSize for the printer device.
                paginator.PageSize = new Size(ia.MediaSizeWidth, ia.MediaSizeHeight);
                Thickness t = new Thickness(72);  // copy.PagePadding;
                copy.PagePadding = new Thickness(
                                 Math.Max(ia.OriginWidth, t.Left),
                                   Math.Max(ia.OriginHeight, t.Top),
                                   Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
                                   Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));

                copy.ColumnWidth = double.PositiveInfinity;
                //copy.PageWidth = 528; // allow the page to be the natural with of the output device

                // Send content to the printer.
                docWriter.Write(paginator);
            }

        }



        //https://www.c-sharpcorner.com/forums/how-to-set-a4-size-for-flowdocument-under-window-in-wpf-c-sharp
        public void PrintDocs(PageMediaSizeName pagesize)
        {
            PrintDialog printDlg = new System.Windows.Controls.PrintDialog();
            //printDlg.PrintQueue.
            PageMediaSize pageSize = null;
            bool bA4 = true;

            if (bA4)
            {
                pageSize = new PageMediaSize(PageMediaSizeName.ISOA4);
            }
            else
            {
                pageSize = new PageMediaSize(PageMediaSizeName.ISOA4);
            }

            printDlg.PrintTicket.PageMediaSize = pageSize;
            printDlg.ShowDialog();

            PrintTicket pt = printDlg.PrintTicket;
            Double printableWidth = pt.PageMediaSize.Width.Value;
            Double printableHeight = pt.PageMediaSize.Height.Value;
            
            var pageSize2 = new Size(printDlg.PrintableAreaWidth, printDlg.PrintableAreaHeight);
        }










    }//cls
}//ns
