﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Nebula.ViewModels;
using Nebula.Models;
using Nebula.LenovoServers;
using Nebula.CiscoServers;
using Newtonsoft.Json;
//using System.Text.Json;
using System.IO;
using System.Xml.Linq;
using Renci.SshNet;
using Renci.SshNet.Common;
using System.Data;
using Simple.OData.Client;
using System.Text.RegularExpressions;

namespace Nebula.Helpers
{
    public class JsonParser
    {
        MainViewModel vm;
        public JsonParser(MainViewModel passedviewmodel)
        {
           vm = passedviewmodel;
        }

      

        public static Gen8Info ReadFullServerJson(string jsonserverinfo, string jsonfirmware)
        {
            //declare json object
            JObject serverinfo = JObject.Parse(jsonserverinfo);
            JObject firmwareLevels = JObject.Parse(jsonfirmware);


            Gen8Info gen8 = new Gen8Info();

            //FIRMWARE
            //extract bios current
            string bioscurname = (string)firmwareLevels.SelectToken("Current.SystemRomActive[0].Name");
            gen8.BiosCurrentVersion = (string)firmwareLevels.SelectToken("Current.SystemRomActive[0].VersionString");
            //extract bios backup
            string biosbakname = (string)firmwareLevels.SelectToken("Current.SystemRomBackup[0].Name");
            gen8.BiosBackupVersion = (string)firmwareLevels.SelectToken("Current.SystemRomBackup[0].VersionString");
            //extract ilo
            string iloname = (string)firmwareLevels.SelectToken("Current.SystemBMC[0].Name");
            gen8.iLOVersion = (string)firmwareLevels.SelectToken("Current.SystemBMC[0].VersionString");
            //extract inteligent provisioning
            string intprovname = (string)firmwareLevels.SelectToken("Current.IntelligentProvisioning[0].Name");
            gen8.IntelligentProvisioningVersion = (string)firmwareLevels.SelectToken("Current.IntelligentProvisioning[0].VersionString");
            //SPS Firmware
            string spsname = (string)firmwareLevels.SelectToken("Current.SPSFirmwareVersionData[0].Name");
            gen8.SPSVersion = (string)firmwareLevels.SelectToken("Current.SPSFirmwareVersionData[0].VersionString");


            //SERVER INFO

            //extract bios current
            //MessageBox.Show((string)serverinfo.SelectToken("Manufacturer"));
            gen8.Manufacturer = (string)serverinfo.SelectToken("Manufacturer");
            gen8.Model = (string)serverinfo.SelectToken("Model");
            gen8.AssetTag = (string)serverinfo.SelectToken("AssetTag");
            gen8.HostName = (string)serverinfo.SelectToken("HostName");
            gen8.ChassisSerial = (string)serverinfo.SelectToken("SerialNumber");
            //extract ilo
            gen8.HealthStatus = (string)serverinfo.SelectToken("Status.Health");
            gen8.SKU = (string)serverinfo.SelectToken("SKU");
            gen8.TotalMemory = (string)serverinfo.SelectToken("Memory.TotalSystemMemoryGB");
            gen8.MemoryHealth = (string)serverinfo.SelectToken("Memory.Status.HealthRollUp");
            //extract inteligent provisioning
            //string model = (string)firmwareLevels.SelectToken("Model");
            gen8.IntelligentProvisioningLocation = (string)serverinfo.SelectToken("Oem.Hp.IntelligentProvisioningLocation");
            //string intprovversion = (string)firmwareLevels.SelectToken("Oem.Hp.IntelligentProvisioningVersion");
            gen8.PostState = (string)serverinfo.SelectToken("Oem.Hp.PostState");

            gen8.CPUFamily = (string)serverinfo.SelectToken("Processors.ProcessorFamily");
            gen8.CPUCount = (string)serverinfo.SelectToken("Processors.Count");
            gen8.CPUHealth = (string)serverinfo.SelectToken("Memory.Status.HealthRollUp");
            //power
            gen8.PowerState = (string)serverinfo.SelectToken("PowerState");
            gen8.PowerAutoOn = (string)serverinfo.SelectToken("Oem.Hp.PowerAutoOn");
            gen8.PowerOnDelay = (string)serverinfo.SelectToken("Oem.Hp.PowerOnDelay");
            gen8.PowerAutoOn = (string)serverinfo.SelectToken("Oem.Hp.PowerAutoOn");
            gen8.PowerRegulatorMode = (string)serverinfo.SelectToken("Oem.Hp.PowerRegulatorMode");
            gen8.PowerAllocationLimit = (string)serverinfo.SelectToken("Oem.Hp.PowerAllocationLimit");

            //SPS Firmware
            //string spsname = (string)firmwareLevels.SelectToken("Current.SPSFirmwareVersionData[0].Name");
            //string spsversion = (string)firmwareLevels.SelectToken("Current.SPSFirmwareVersionData[0].VersionString");

            //List<Gen8Info> gen8server = new List<Gen8Info>();
            //gen8server.Add(gen8);
            //firmwarelist.Add("Model                    " + model);
            //firmwarelist.Add("Chassis Serial           " + chassisserial);
            //firmwarelist.Add("Asset Tag                " + asstag);
            //firmwarelist.Add("Bios Current Version     " + bioscurversion);
            //firmwarelist.Add("HostName                 " + hostname);
            //firmwarelist.Add("Health Status            " + status);
            //firmwarelist.Add("SKU                      " + sku);
            //firmwarelist.Add("Total Memory             " + totalmemory + "GB");
            //firmwarelist.Add("Memory Health            " + memorystatus);
            //firmwarelist.Add("Intelligent Provisioning Location " + intprovloc);
            //firmwarelist.Add("Intelligent Provisioning " + intprovversion);
            //firmwarelist.Add("PostState " + poststate);
            //foreach (var itm in firmwarelist)
            //{
            //    MessageBox.Show(itm);
            //}

            //MessageBox.Show(gen8.Manufacturer);

            return gen8;
        }

        //public void ReadFullServerJsonTest(string jsonserverinfo)
        //{
        //    //declare json object

        //    //HPBIOS myserverinfo = new HPBIOS();

          


        //    JArray arrayinfo = JArray.Parse(jsonserverinfo);
        //    //MessageBox.Show(arrayinfo.Count.ToString()); 


        //    HPBIOS myserverinfo  = JsonConvert.DeserializeObject<HPBIOS>(jsonserverinfo);

        //    //myserverinfo.ServerInfo[0].Comments

        //    //= JObject.Parse(arrayinfo[0].Type);
        //    JObject comments = JObject.Parse(arrayinfo[0].ToString());
        //    JObject hpbios = JObject.Parse(arrayinfo[1].ToString());
        //    JObject chassis = JObject.Parse(arrayinfo[2].ToString());
        //    JObject firmware = JObject.Parse(arrayinfo[3].ToString());
        //    JObject computersystem = JObject.Parse(arrayinfo[4].ToString());
        //    JObject manager = JObject.Parse(arrayinfo[5].ToString());

        //    Gen8Info gen8 = new Gen8Info();
        //}


        //XML FROM RIBCL 
        public GET_EMBEDDED_HEALTH_DATA_GEN8.GET_EMBEDDED_HEALTH_DATA GetHealthSummaryGen8(string serverip, string serverno)
        {

            GET_EMBEDDED_HEALTH_DATA_GEN8.GET_EMBEDDED_HEALTH_DATA HealthSum = new GET_EMBEDDED_HEALTH_DATA_GEN8.GET_EMBEDDED_HEALTH_DATA();

            string ServerInfoJson;
            //if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth.xml") || File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth1.xml")
            //    File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth2.xml") || File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth3.xml")
            //    File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth4.xml")))
            //{

                switch (serverno)
                {
                       case "S1":

                        //Full Info
                        using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml"))
                        {
                        //STRIP OUT THE INVALID HEADERS THAT STOP JSON.NET READING CORRECTLY
                        ServerInfoJson = r.ReadToEnd().Replace("Server IP is : " + serverip + ":443", "")
                                                      .Replace("<?xml version=\"1.0\"?>", "")
                                                      .Replace("<RIBCL VERSION=\"2.23\">", "")
                                                      .Replace("<RESPONSE", "")
                                                      .Replace("  STATUS=\"0x0000\"", "")
                                                      .Replace("MESSAGE='No error'", "")
                                                      .Replace("   />", "")
                                                      .Replace("</RIBCL>", "")
                                                      .Replace("Script succeeded for IP:" + serverip + ":443", "").Trim()
                                                      .Replace("N/A", "0")
                                                      .Replace("Version 0x34", "Version")
                                                      .Replace("Version 0x01", "Version")
                                                      .Replace("<FIRMWARE_VERSION VALUE = ", "<FIRMWARE_VERSION Value =");
                                                      //.Replace("<HP_SMART_MEMORY VALUE =", "<HP_SMART_MEMORY  VALUE =");
                                                      //.Replace("Type = \"Smart\"", "")
                                                      //.Replace("Type = \"Unknown\"", "");
                                                      //.Replace("<NUMBER_OF_SOCKETS VALUE =", "<NUMBER_OF_SOCKETS value =")
                                                      //.Replace("<TOTAL_MEMORY_SIZE VALUE =", "<TOTAL_MEMORY_SIZE value =")
                                                      //.Replace("<OPERATING_FREQUENCY VALUE =", "<OPERATING_FREQUENCY value =")
                                                      //.Replace("<OPERATING_VOLTAGE VALUE =", "<OPERATING_VOLTAGE value =")
                                                      //.Replace("<PART NUMBER =", "<PART number =")

                        //.Replace("<TYPE VALUE =", "<TYPE value =")
                        //.Replace("<SIZE VALUE =", "<SIZE value =")
                        //.Replace("<FREQUENCY VALUE =", "<FREQUENCY value =")
                        //.Replace("<MINIMUM_VOLTAGE VALUE =", "<MINIMUM_VOLTAGE value =")
                        //.Replace("<RANKS VALUE =", "<RANKS value =")
                        //.Replace("<TECHNOLOGY VALUE =", "<TECHNOLOGY value =");


                        //.Replace("Type =", "TYPE="); 
                    }



                        //StaticFunctions.
                        File.WriteAllText(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml", ServerInfoJson);


                   

                    // Read the Health Dat XML File
                    HealthSum = XMLTools.ReadFromXmlString<GET_EMBEDDED_HEALTH_DATA_GEN8.GET_EMBEDDED_HEALTH_DATA>(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml");



                        break;
                  
            }
            //}
            //else
            //{
            //    return null;
            //}
             
                return HealthSum;


        }

      


        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }


        public CiscoServerInfo GetRESTCisco(string jsonserverinfo, string serverip)
        {
            //var definition = new { Links = "" };
            ////List<string> mylinks = JsonConvert.DeserializeObject<List<string>>(jsonserverinfo);

            //var customer1 = JsonConvert.DeserializeAnonymousType(json1, definition);

            //using (StreamReader file = File.OpenText(@"" + vm.myDocs + @"\HPTOOLS\Cisco\CURL\bin\cimc.json"))
            //{
            //    JsonSerializer serializer = new JsonSerializer();
            //    CiscoCIMC imc = (CiscoCIMC)serializer.Deserialize(file, typeof(CiscoCIMC));

            //    foreach (var itm in imc.Links.ManagerForChassis)
            //    {
            //        MessageBox.Show(itm);
            //    }

            //    foreach (var itm in imc.Links.ManagerForServers)
            //    {
            //        MessageBox.Show(itm);
            //    }

            //    MessageBox.Show(imc.odataid);

            //    MessageBox.Show(imc.VirtualMedia.odataid);
            //    MessageBox.Show(imc.Model);
            //}

            // ///https://docs.microsoft.com/en-us/odata/client/getting-started

            // var client = new ODataClient("https://192.168.101.90/redfish/v1/odata/");


            //MessageBox.Show(client.For<>);


            //var totalPackageCount = client.For<FeedPackage>("packages").Count().FindScalarAsync<int>().Result;

            //var people = await client
            //    .For<People>()
            //    .Key("scottketchum")
            //    .NavigateTo<Trip>()
            //    .Key(0)
            //    .Function("GetInvolvedPeople")
            //    .ExecuteAsEnumerableAsync();






            ////works with file
            //using (StreamReader file = File.OpenText(jsonserverinfo))
            //{
            //    JsonSerializer serializer = new JsonSerializer();
            //    CiscoServerRESTSchema imc = (CiscoServerRESTSchema)serializer.Deserialize(file, typeof(CiscoServerRESTSchema));

             
            //    MessageBox.Show(imc.Name);

            //    MessageBox.Show(imc.Systems.odataid);
            //    MessageBox.Show(imc.Managers.odataid);
            //    MessageBox.Show(imc.Chassis.odataid);

              
            //    MessageBox.Show(imc.Id);
            //    MessageBox.Show(imc.RedfishVersion);

               

            //    MessageBox.Show(imc.SessionService.odataid);




            



            //    //if(imc.Links != null)
            //    //MessageBox.Show(imc.Links.Sessions.odataid);

            //    //MessageBox.Show(imc.Model);
            //}



            RestClient rClient = new RestClient();

            rClient.endPoint = jsonserverinfo;
            // debugOutput("RESTClient Object created.");

            string strJSON = string.Empty;

            strJSON = rClient.makeRequest();

            Console.WriteLine(strJSON);

            JsonSerializer serializer = new JsonSerializer();
            //CiscoServerRESTSchema imc = (CiscoServerRESTSchema)serializer.Deserialize(strJSON, typeof(CiscoServerRESTSchema));

            CiscoServerRESTSchema imc = new CiscoServerRESTSchema();

            var customer2 = JsonConvert.DeserializeAnonymousType(strJSON, imc);



            MessageBox.Show(customer2.Name);

            MessageBox.Show(customer2.Systems.odataid);
            MessageBox.Show(customer2.Managers.odataid);
            MessageBox.Show(customer2.Chassis.odataid);


            MessageBox.Show(customer2.Id);
            MessageBox.Show(customer2.RedfishVersion);



          //  MessageBox.Show(imc.SessionService.odataid);






            CiscoServerInfo Cisco = new CiscoServerInfo();



            return Cisco;
        }


            //Cisco Server Info
        public CiscoServerInfo ReadFullServerInfoJsonCisco(string jsonserverinfo, string serverip, string serverno)
        {
            //declare json object
            //JObject serverinfo = JObject.Parse(jsonserverinfo);

            //array to object
            JArray arrayinfo = JArray.Parse(jsonserverinfo);
            //MessageBox.Show(arrayinfo.Count.ToString()); 
            JObject comments = JObject.Parse(arrayinfo[0].ToString());
            JObject hpbios = JObject.Parse(arrayinfo[1].ToString());
            JObject chassis = JObject.Parse(arrayinfo[2].ToString());
            JObject firmware = JObject.Parse(arrayinfo[3].ToString());
            JObject computersystem = JObject.Parse(arrayinfo[4].ToString());
            JObject manager = JObject.Parse(arrayinfo[5].ToString());

            CiscoServerInfo Cisco = new CiscoServerInfo();
            //SDCARD Detection

          


         

          

            return Cisco;

            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //    //Console.WriteLine(ex.Message);
            //    return Cisco;
            //}
        }






       

        public Gen8Info ReadFullServerInfoJsonGen8(string jsonserverinfo,string serverip, string serverno)
        {



            try
            {


                //declare json object
                //JObject serverinfo = JObject.Parse(jsonserverinfo);

                //array to object
                JArray arrayinfo = JArray.Parse(jsonserverinfo);

            //MessageBox.Show(arrayinfo.Count().ToString());

            //MessageBox.Show(arrayinfo.Count.ToString()); 
            JObject comments = JObject.Parse(arrayinfo[0].ToString());
            JObject hpbios = JObject.Parse(arrayinfo[1].ToString());
            JObject chassis = JObject.Parse(arrayinfo[2].ToString());
            JObject firmware = JObject.Parse(arrayinfo[3].ToString());
            JObject computersystem = JObject.Parse(arrayinfo[4].ToString());
            JObject manager = JObject.Parse(arrayinfo[5].ToString());
            //JObject disk1 = JObject.Parse(arrayinfo[6].ToString());
            //JObject disk2 = JObject.Parse(arrayinfo[7].ToString());

            Gen8Info gen8 = new Gen8Info();

            //SDCARD Detection

            JObject SDCardinfo;
            //check if JSON File is there
            if(File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "SDCARD.json"))
            {
            //check for presence of sd card
            using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "SDCARD.json"))
            {
                //STRIP OUT THE INCORRECT HEADERS THAT STOP JSON.NET READING CORRECTLY
                SDCardinfo = JObject.Parse(r.ReadToEnd());
            }
           
            gen8.SDCardInserted = (string)SDCardinfo.SelectToken("SDCard.Status.State");
                //string bioscurname = (string)hpbios.SelectToken("HpBios.restv1Systems1Bios.PowerRegulator");
            }


            //string bioscurname = (string)hpbios.SelectToken("HpBios.restv1Systems1Bios.PowerRegulator");

            // MessageBox.Show(bioscurname);

            //FIRMWARE
            //FIRMWARE
            //extract bios current
            // string bioscurname = (string)serverinfo.SelectToken("Current.SystemRomActive[0].Name");
            gen8.BiosCurrentVersion = (string)firmware.SelectToken("Firmware.Path.Current.SystemRomActive[0].VersionString");
            //extract bios backup
            //string biosbakname = (string)serverinfo.SelectToken("Current.SystemRomBackup[0].Name");
            gen8.BiosBackupVersion = (string)firmware.SelectToken("Firmware.Path.Current.SystemRomBackup[0].VersionString");
            //extract ilo
            //string iloname = (string)serverinfo.SelectToken("Current.SystemBMC[0].Name");
            gen8.iLOVersion = (string)firmware.SelectToken("Firmware.Path.Current.SystemBMC[0].VersionString");
            //extract inteligent provisioning
            //string intprovname = (string)serverinfo.SelectToken("Current.IntelligentProvisioning[0].Name");
            gen8.IntelligentProvisioningVersion = (string)firmware.SelectToken("Firmware.Path.Current.IntelligentProvisioning[0].VersionString");
            //SPS Firmware
            //string spsname = (string)serverinfo.SelectToken("Current.SPSFirmwareVersionData[0].Name");
            gen8.SPSVersion = (string)firmware.SelectToken("Firmware.Path.Current.SPSFirmwareVersionData[0].VersionString");

            //SERVER INFO

            //extract bios current
            //MessageBox.Show((string)serverinfo.SelectToken("Manufacturer"));
            gen8.Manufacturer = (string)computersystem.SelectToken("ComputerSystem.Path.Manufacturer");
            gen8.Model = (string)computersystem.SelectToken("ComputerSystem.Path.Model");
            gen8.AssetTag = (string)computersystem.SelectToken("ComputerSystem.Path.AssetTag");

            string hn = (string)computersystem.SelectToken("ComputerSystem.Path.HostName");
            if(hn != null)
            gen8.HostName = hn.Trim();
            string cs = (string)computersystem.SelectToken("ComputerSystem.Path.SerialNumber");
            if (cs != null)
            gen8.ChassisSerial = cs.Trim();
            //extract ilo
            gen8.HealthStatus = (string)computersystem.SelectToken("ComputerSystem.Path.Status.Health");
            string sku = (string)computersystem.SelectToken("ComputerSystem.Path.SKU");
            if (sku != null)
            gen8.SKU = sku.Trim();
            gen8.TotalMemory = (string)computersystem.SelectToken("ComputerSystem.Path.Memory.TotalSystemMemoryGB");
            gen8.MemoryHealth = (string)computersystem.SelectToken("ComputerSystem.Path.Memory.Status.HealthRollUp");
            //extract inteligent provisioning
            //string model = (string)firmwareLevels.SelectToken("Model");
            gen8.IntelligentProvisioningLocation = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hp.IntelligentProvisioningLocation");
            //string intprovversion = (string)firmwareLevels.SelectToken("Oem.Hp.IntelligentProvisioningVersion");
            gen8.PostState = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hp.PostState");

            gen8.CPUFamily = (string)computersystem.SelectToken("ComputerSystem.Path.Processors.ProcessorFamily");
            gen8.CPUCount = (string)computersystem.SelectToken("ComputerSystem.Path.Processors.Count");
            gen8.CPUHealth = (string)computersystem.SelectToken("ComputerSystem.Path.Memory.Status.HealthRollUp");
            //power
            gen8.PowerState = (string)computersystem.SelectToken("PowerState");
            gen8.PowerAutoOn = (string)computersystem.SelectToken("Oem.Hp.PowerAutoOn");
            gen8.PowerOnDelay = (string)computersystem.SelectToken("Oem.Hp.PowerOnDelay");
            gen8.PowerAutoOn = (string)computersystem.SelectToken("Oem.Hp.PowerAutoOn");
            gen8.PowerRegulatorMode = (string)computersystem.SelectToken("Oem.Hp.PowerRegulatorMode");
            gen8.PowerAllocationLimit = (string)computersystem.SelectToken("Oem.Hp.PowerAllocationLimit");

            //iLO Self Test
            gen8.NVRAMDataNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[0].Notes");
            gen8.NVRAMDataStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[0].Status");
            gen8.NVRAMSpaceNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[1].Notes");
            gen8.NVRAMSpaceStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[1].Status");
            gen8.EmbeddedFlashSDCardNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[2].Notes");
            gen8.EmbeddedFlashSDCardStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[2].Status");
            gen8.EEPROMNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[3].Notes");
            gen8.EEPROMStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[3].Status");
            gen8.HostRomNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[4].Notes");
            gen8.HostRomStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[4].Status");
            gen8.SupportedHostNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[5].Notes");
            gen8.SupportedHostStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[5].Status");
            gen8.PowerManagementControllerNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[6].Notes");
            gen8.PowerManagementControllerStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[6].Status");
            gen8.CPLDPAL0Notes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[7].Notes");
            gen8.CPLDPAL0Status = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[7].Status");
            gen8.CPLDPAL1Notes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[8].Notes");
            gen8.CPLDPAL1Status = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[8].Status");

            //ILO License   public string ILOLicense { get; set; }
            gen8.ILOLicense = (string)manager.SelectToken("Manager.Path.Oem.Hp.License.LicenseString");

            //Drive Detection

            //SDCARD Detection

            //PHYSICAL DISKS
            JArray diskarrayinfo = JArray.Parse(jsonserverinfo);
    

            //check if JSON File is there
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "Disks.json"))
            {
                //array to object
               
                //JObject o1 = JObject.Parse(File.ReadAllText(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "Disks.json"));

                //foreach(var)


                ////check for presence of sd card
                // Gen 10 simulation using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\192.168.101.77Disks.json"))
                using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "Disks.json"))
                {

                    ////Read from File
                    //// string streamString = r.ReadToEnd();

                    string streamString = "";

                    string HeaderString = "";
                    string PathString = "";

                    // Use while not null pattern in while loop.
                    string line;
                    while ((line = r.ReadLine()) != null)
                    {
                        // Insert logic here.
                        // ... The "line" variable is a line in the file.
                        // ... Add it to our List.



                        if (line.Contains("@odata") || line.Contains("href"))
                        {

                        }
                        else
                        {

                            //Gen8,9
                            if (line.Contains("HpSmartStorageDiskDrive"))
                            {
                                HeaderString = "HpSmartStorageDiskDrive" + Regex.Match(line, @"HpSmartStorageDiskDrive(.+?):").Groups[1].Value.Replace("\"", "");
                                line = line.Replace(HeaderString, "Disk");
                                //  MessageBox.Show(HeaderString);
                            }
                            //Gen10
                            if (line.Contains("#HpeSmartStorageDiskDrive."))
                            {
                                HeaderString = "#HpeSmartStorageDiskDrive." + Regex.Match(line, @"#HpeSmartStorageDiskDrive.(.+?):").Groups[1].Value.Replace("\"", "");
                                line = line.Replace(HeaderString, "Disk");
                                //  MessageBox.Show(HeaderString);
                            }


                        }


                        if (line.Contains("@odata") || line.Contains("href"))
                        {

                        }
                        else
                        {

                            //gen8,9 
                            if (line.Contains("/rest/v1/Systems/1/SmartStorage/ArrayControllers/"))
                            {
                                // MessageBox.Show(PathString);

                                PathString = "/rest/v1/Systems/1/SmartStorage/ArrayControllers/" + Regex.Match(line, @"/rest/v1/Systems/1/SmartStorage/ArrayControllers/(.+?):").Groups[1].Value.Replace("\"", "");
                                line = line.Replace(PathString, "Path");

                            }

                            //gen10

                            if (line.Contains("/redfish/v1/Systems/1/SmartStorage/ArrayControllers/"))
                            {
                                // MessageBox.Show(PathString);

                                PathString = "/redfish/v1/Systems/1/SmartStorage/ArrayControllers/" + Regex.Match(line, @"/redfish/v1/Systems/1/SmartStorage/ArrayControllers/(.+?):").Groups[1].Value.Replace("\"", "");
                                line = line.Replace(PathString, "Path");

                            }

                        }

                        streamString += line;


                    }


                    diskarrayinfo = JArray.Parse(streamString);


                }

                //Create holding array
                foreach (JObject disk in diskarrayinfo)
                {
                  
                    if((string)disk.SelectToken("Disk.Path.Model") != null)
                    {

                        //MessageBox.Show((string)disk.SelectToken("Disk.Path.Model"));
                        HPPhysicalDrivesModel drive = new HPPhysicalDrivesModel();

                        
                        drive.Model = (string)disk.SelectToken("Disk.Path.Model");
                        drive.Capacity = (string)disk.SelectToken("Disk.Path.CapacityGB");
                        if ((bool)disk.SelectToken("Disk.Path.EncryptedDrive") == false)
                        {
                            drive.Encryption = "No";
                        }
                        else
                        {
                            drive.Encryption = "Yes";
                        }

                        drive.SerialNumber = (string)disk.SelectToken("Disk.Path.SerialNumber");
                        drive.MediaType = (string)disk.SelectToken("Disk.Path.MediaType");
                        drive.Location = (string)disk.SelectToken("Disk.Path.Location");
                        drive.Interface = (string)disk.SelectToken("Disk.Path.InterfaceType");
                        drive.InterfaceSpeed = (string)disk.SelectToken("Disk.Path.InterfaceSpeedMbps");
                        drive.BlockSize = (string)disk.SelectToken("Disk.Path.BlockSizeBytes");
                        drive.Health = (string)disk.SelectToken("Disk.Path.Status.Health");
                        gen8.PhysicalDriveInfo.Add(drive);
                        //gen8.StorageInfo.LogicalDrives.Add(new HealthLogicalDriveGen8("PHYSICAL ONLY", "", "", "", "", "", drive));
                    }
                  
                                     
                }


            }



            //READ SEPARATE XML FILE
            //read xml health summary
           GET_EMBEDDED_HEALTH_DATA_GEN8.GET_EMBEDDED_HEALTH_DATA HealthSum = new GET_EMBEDDED_HEALTH_DATA_GEN8.GET_EMBEDDED_HEALTH_DATA();

               // GET_EMBEDDED_HEALTH_DATA_GEN8MB.GET_EMBEDDED_HEALTH_DATA HealthSum = new GET_EMBEDDED_HEALTH_DATA_GEN8MB.GET_EMBEDDED_HEALTH_DATA();

                switch (serverno)
                {
                  
                    case "S1":
                        HealthSum = GetHealthSummaryGen8(serverip, "S1");


                        break;
              
                }


                //await PutTaskDelay(5000);

      

             

           if (HealthSum != null)
            {

                    if (HealthSum.HEALTH_AT_A_GLANCE.BIOS_HARDWARE.STATUS != null)
                        gen8.BiosHardwareStatus = HealthSum.HEALTH_AT_A_GLANCE.BIOS_HARDWARE.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.PROCESSOR != null)
                        gen8.ProcessorStatus = HealthSum.HEALTH_AT_A_GLANCE.PROCESSOR.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.MEMORY != null)
                        gen8.MemoryStatus = HealthSum.HEALTH_AT_A_GLANCE.MEMORY.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.NETWORK != null)
                        gen8.NetworkStatus = HealthSum.HEALTH_AT_A_GLANCE.NETWORK.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.STORAGE != null)
                        gen8.StorageStatus = HealthSum.HEALTH_AT_A_GLANCE.STORAGE.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.TEMPERATURE != null)
                        gen8.TemperatureStatus = HealthSum.HEALTH_AT_A_GLANCE.TEMPERATURE.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.FANS != null)
                        gen8.FansStatus = HealthSum.HEALTH_AT_A_GLANCE.FANS.FirstOrDefault().STATUS.ToString();
                    //gen8.FansRedundancy = HealthSum.HEALTH_AT_A_GLANCE.FANS.FirstOrDefault().REDUNDANCY.ToString();
                    if (HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES != null)
                        gen8.PowerSuppliesStatus = HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES.FirstOrDefault().STATUS.ToString();
              
                
                //FANS Collection
                if (HealthSum.HEALTH_AT_A_GLANCE.FANS != null)
                foreach (var itm in HealthSum.HEALTH_AT_A_GLANCE.FANS)
                {
                    //MessageBox.Show(itm.STATUS);

                    gen8.FansRedundancy = itm.REDUNDANCY;
                }


                //POWER SUPPLIES Collection
                if (HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES != null)
                foreach (var itm in HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES)
                {

                    //gen8.PowerSuppliesStatus = itm.STATUS.FirstOrDefault().ToString(); 
                    gen8.PowerSupplyRedundancy = itm.REDUNDANCY;
                }


                //Read rest of Data in XML file
                //FANS
                gen8.FanInfo = new System.Collections.ObjectModel.ObservableCollection<HealthFansGen8>();

                if (HealthSum.FANS != null)
                foreach (var itm in HealthSum.FANS)
                {
                    gen8.FanInfo.Add(new HealthFansGen8(itm.LABEL.VALUE, itm.ZONE.VALUE, itm.STATUS.VALUE, itm.SPEED.VALUE + "%"));
                }
                //TEMPERATURES
                gen8.TemperatureInfo = new System.Collections.ObjectModel.ObservableCollection<HealthTemperatureGen8>();

                if (HealthSum.TEMPERATURE != null)
                foreach (var itm in HealthSum.TEMPERATURE)
                {

                    gen8.TemperatureInfo.Add(new HealthTemperatureGen8(itm.LABEL.VALUE, itm.LOCATION.VALUE, itm.STATUS.VALUE, itm.CURRENTREADING.VALUE + " " + itm.CURRENTREADING.UNIT, itm.CAUTION.VALUE + " " + itm.CURRENTREADING.UNIT, itm.CRITICAL.VALUE + " " + itm.CURRENTREADING.UNIT));
                }

                    //CPU
                 gen8.CPUInfo = new System.Collections.ObjectModel.ObservableCollection<HealthCPUGen8>();
                
                if (HealthSum.PROCESSORS != null)
                foreach (var itm in HealthSum.PROCESSORS)
                {
                    gen8.CPUInfo.Add(new HealthCPUGen8(itm.LABEL.VALUE, itm.NAME.VALUE, itm.STATUS.VALUE, itm.SPEED.VALUE, itm.EXECUTION_TECHNOLOGY.VALUE, itm.MEMORY_TECHNOLOGY.VALUE, itm.INTERNAL_L1_CACHE.VALUE, itm.INTERNAL_L2_CACHE.VALUE, itm.INTERNAL_L3_CACHE.VALUE));
                }


                gen8.MemoryInfo = new System.Collections.ObjectModel.ObservableCollection<HealthMemoryGen8>();
                    //MEMORY
               if (HealthSum.MEMORY != null)
               {


                        //HealthSum.MEMORY.ADVANCED_MEMORY_PROTECTION
                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_1 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_1)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_1 != null)
                                    {
                                        gen8.MemoryInfo.Add(new HealthMemoryGen8("CPU1", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }


                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_2 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_2)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_2 != null)
                                    {
                                        gen8.MemoryInfo.Add(new HealthMemoryGen8("CPU2", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        //HealthSum.MEMORY.ADVANCED_MEMORY_PROTECTION
                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_3 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_3)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_3 != null)
                                    {
                                        gen8.MemoryInfo.Add(new HealthMemoryGen8("CPU3", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }


                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_4 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_4)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_4 != null)
                                    {
                                        gen8.MemoryInfo.Add(new HealthMemoryGen8("CPU4", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }



                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_1 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_1)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_1 != null)
                                    {
                                        gen8.MemoryInfo.Add(new HealthMemoryGen8("Memory_Board_1", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }


                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_2 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_2)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_2 != null)
                                    {
                                        gen8.MemoryInfo.Add(new HealthMemoryGen8("Memory_Board_2", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_3 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_3)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_3 != null)
                                    {
                                        gen8.MemoryInfo.Add(new HealthMemoryGen8("Memory_Board_3", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_4 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_4)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_4 != null)
                                    {
                                        gen8.MemoryInfo.Add(new HealthMemoryGen8("Memory_Board_4", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_5 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_5)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_5 != null)
                                    {
                                        gen8.MemoryInfo.Add(new HealthMemoryGen8("Memory_Board_5", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_6 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_6)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_6 != null)
                                    {
                                        gen8.MemoryInfo.Add(new HealthMemoryGen8("Memory_Board_6", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_7 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_7)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_7 != null)
                                    {
                                        gen8.MemoryInfo.Add(new HealthMemoryGen8("Memory_Board_7", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_8 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_8)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_8 != null)
                                    {
                                        Console.WriteLine(itm.SOCKET.VALUE.ToString());
                                        gen8.MemoryInfo.Add(new HealthMemoryGen8("Memory_Board_8", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }


                    }

                    //Count Populated Memory
                    int populatedCount = 0;
               foreach(var itm in gen8.MemoryInfo)
               {

                   if(itm.Status != "Not Present")
                   {
                            //Increase by 1
                            populatedCount += 1;
                   }

               }
                    // Assign to property
                    gen8.PopulatedSlots = populatedCount.ToString();


                    //NETWORK 
                    gen8.NICInfo = new System.Collections.ObjectModel.ObservableCollection<HealthNetworkGen8>();
                    if (HealthSum.NIC_INFORMATION != null)
                    {
                       

                        if (HealthSum.NIC_INFORMATION != null)
                            foreach (var itm in HealthSum.NIC_INFORMATION)
                            {
                                if (HealthSum.NIC_INFORMATION != null)
                                {
                                    if (itm.NETWORK_PORT != null)
                                    if (itm.NETWORK_PORT != null || itm.PORT_DESCRIPTION != null || itm.LOCATION != null || itm.IP_ADDRESS != null || itm.MAC_ADDRESS != null || itm.STATUS != null)
                                    {
                                        gen8.NICInfo.Add(new HealthNetworkGen8(itm.NETWORK_PORT.VALUE, itm.PORT_DESCRIPTION.VALUE, itm.LOCATION.VALUE, itm.IP_ADDRESS.VALUE, itm.MAC_ADDRESS.VALUE, itm.STATUS.VALUE));
                                    }
                                }
                            }

                    }

                    //Assign to ILOPORT Status for test report
                    gen8.ILONetworkPort = gen8.NICInfo.FirstOrDefault().Status;




                    if (HealthSum.POWER_SUPPLIES != null)
                    {
                        //POWER
                        gen8.PowerInfo = new System.Collections.ObjectModel.ObservableCollection<HealthPowerGen8>();

                        //HealthSum.
                        if (HealthSum.POWER_SUPPLIES.SUPPLY != null)
                            foreach (var itm in HealthSum.POWER_SUPPLIES.SUPPLY)
                            {
                                if (itm.PRESENT != null)
                                if (itm.PRESENT != null || itm.STATUS != null || itm.MODEL != null || itm.SPARE != null || itm.SERIAL_NUMBER != null || itm.CAPACITY != null || itm.FIRMWARE_VERSION != null)
                                {
                                    gen8.PowerInfo.Add(new HealthPowerGen8(itm.LABEL.VALUE, itm.PRESENT.VALUE, itm.STATUS.VALUE, itm.MODEL.VALUE, itm.SPARE.VALUE, itm.SERIAL_NUMBER.VALUE, itm.SPARE.VALUE, itm.SERIAL_NUMBER.VALUE, itm.CAPACITY.VALUE, itm.FIRMWARE_VERSION.VALUE.ToString()));
                                }

                            }

                    }

                if(HealthSum.STORAGE.CONTROLLER != null)
                    {
          
                //STORAGE
                HealthStorageGen8 controllerinfo = new HealthStorageGen8();
                if (HealthSum.STORAGE.CONTROLLER.LABEL.VALUE != null)
                controllerinfo.Label = HealthSum.STORAGE.CONTROLLER.LABEL.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.STATUS.VALUE != null)
                            controllerinfo.Status = HealthSum.STORAGE.CONTROLLER.STATUS.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.CONTROLLER_STATUS.VALUE != null)
                            controllerinfo.ControllerStatus = HealthSum.STORAGE.CONTROLLER.CONTROLLER_STATUS.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.SERIAL_NUMBER.VALUE != null)
                            controllerinfo.Serial = HealthSum.STORAGE.CONTROLLER.SERIAL_NUMBER.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.MODEL.VALUE != null)
                            controllerinfo.Model = HealthSum.STORAGE.CONTROLLER.MODEL.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.FW_VERSION.VALUE.ToString() != null)
                            controllerinfo.Firmware = HealthSum.STORAGE.CONTROLLER.FW_VERSION.VALUE.ToString();

                if (HealthSum.STORAGE.CONTROLLER.DRIVE_ENCLOSURE != null)
                foreach (var itm in HealthSum.STORAGE.CONTROLLER.DRIVE_ENCLOSURE)
                {
                    controllerinfo.DriveEnclosure.Add(new DriveEnclosureInfoGen8(itm.LABEL.VALUE, itm.STATUS.VALUE, itm.DRIVE_BAY.VALUE.ToString()));
                    //MessageBox.Show(itm.LABEL.VALUE + "  " +);
                }

                        //Logical Drive

                        //if (HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.LABEL != null)
                        //    controllerinfo.LogicalDrive.Label = HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.LABEL.VALUE.ToString();
                        //if (HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.STATUS != null)
                        //    controllerinfo.LogicalDrive.Status = HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.STATUS.VALUE.ToString();
                        //if (HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.CAPACITY != null)
                        //    controllerinfo.LogicalDrive.Capacity = HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.CAPACITY.VALUE.ToString();
                        //if (HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.FAULT_TOLERANCE != null)
                        //    controllerinfo.LogicalDrive.FaultTolerance = HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.FAULT_TOLERANCE.VALUE.ToString();
                        //if (HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.LOGICAL_DRIVE_TYPE != null)
                        //    controllerinfo.LogicalDrive.LogicalDriveType = HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.LOGICAL_DRIVE_TYPE.VALUE.ToString();
                        //if (HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.ENCRYPTION_STATUS != null)
                        //    controllerinfo.LogicalDrive.Encryption = HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.ENCRYPTION_STATUS.VALUE.ToString();
                        //if (HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.ENCRYPTION_STATUS != null)
                        //    controllerinfo.LogicalDrive.Encryption = HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE.ENCRYPTION_STATUS.VALUE.ToString();

                        if (HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE != null)
                        foreach (var itm in HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE)
                        {
                            controllerinfo.LogicalDrives.Add(new HealthLogicalDriveGen8(itm.LABEL.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.CAPACITY.VALUE.ToString(),itm.FAULT_TOLERANCE.VALUE.ToString(), itm.LOGICAL_DRIVE_TYPE.VALUE.ToString(), itm.ENCRYPTION_STATUS.VALUE.ToString(),itm.PHYSICAL_DRIVE));
                            //MessageBox.Show(itm.FW_VERSION.VALUE.ToString());

                            //foreach (var itm2 in itm.PHYSICAL_DRIVE)
                            //{
                            //    controllerinfo.LogicalDrives...Add(new PhysicalDrivesGen8(itm.LABEL.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.SERIAL_NUMBER.VALUE.ToString(), itm.MODEL.VALUE.ToString(), itm.CAPACITY.VALUE.ToString(), itm.MARKETING_CAPACITY.VALUE.ToString(), itm.LOCATION.VALUE.ToString(), itm.FW_VERSION.VALUE.ToString(), itm.DRIVE_CONFIGURATION.VALUE.ToString(), itm.ENCRYPTION_STATUS.VALUE.ToString(), itm.MEDIA_TYPE.VALUE.ToString()));
                            //    //MessageBox.Show(itm.FW_VERSION.VALUE.ToString());
                            //}

                            // itm.PHYSICAL_DRIVE.
                        }






                       // if (HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE != null)
              
                
                //end Logical Drive


                  gen8.StorageInfo = controllerinfo;
                }
                }





                //if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.ServerIPv4 + "ServerHealth.xml"))
                //{
                //    File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.ServerIPv4 + "ServerHealth.xml");
                //}
                //if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.ServerIPv4 + "ServerHealthTrimmed.xml"))
                //{
                //    File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.ServerIPv4 + "ServerHealthTrimmed.xml");
                //}


                return gen8;

            }
            catch (Exception ex)
            {
                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                //if (vm.WarningMessage1 != "")
                //{
                //    if (ServerTab != null)
                //    {
                //        ServerTab.Foreground = Brushes.MediumPurple;
                //    }
                //}
                //else
                //{
                //    if (ServerTab != null)
                //    {
                //        ServerTab.Foreground = Brushes.Green;
                //    }

                //}

                return null;
            }

        }



        public GET_EMBEDDED_HEALTH_DATA_GEN9.GET_EMBEDDED_HEALTH_DATA GetHealthSummaryGen9(string serverip, string serverno)
        {

            GET_EMBEDDED_HEALTH_DATA_GEN9.GET_EMBEDDED_HEALTH_DATA HealthSum = new GET_EMBEDDED_HEALTH_DATA_GEN9.GET_EMBEDDED_HEALTH_DATA();

            string ServerInfoJson;
            //if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth.xml") || File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth1.xml")
            //    File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth2.xml") || File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth3.xml")
            //    File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth4.xml")))
            //{

            switch (serverno)
            {
              
                case "S1":

                    //Full Info
                    using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml"))
                    {
                        //STRIP OUT THE INVALID HEADERS THAT STOP JSON.NET READING CORRECTLY
                        ServerInfoJson = r.ReadToEnd().Replace("Server IP is : " + serverip + ":443", "")
                                                      .Replace("<?xml version=\"1.0\"?>", "")
                                                      .Replace("<RIBCL VERSION=\"2.23\">", "")
                                                      .Replace("<RESPONSE", "")
                                                      .Replace("  STATUS=\"0x0000\"", "")
                                                      .Replace("MESSAGE='No error'", "")
                                                      .Replace("   />", "")
                                                      .Replace("</RIBCL>", "")
                                                      .Replace("Script succeeded for IP:" + serverip + ":443", "").Trim()
                                                      .Replace("N/A", "0")
                                                      .Replace("Version 0x34", "Version")
                                                      .Replace("Version 0x01", "Version")
                                                      .Replace("<FIRMWARE_VERSION VALUE = ", "<FIRMWARE_VERSION Value=");


                    }



                    //StaticFunctions.
                    //MessageBox.Show("Here");
                    File.WriteAllText(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml", ServerInfoJson);


                    HealthSum = XMLTools.ReadFromXmlString<GET_EMBEDDED_HEALTH_DATA_GEN9.GET_EMBEDDED_HEALTH_DATA>(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml");



                    break;
              


            }
            //}
            //else
            //{
            //    return null;
            //}

            return HealthSum;


        }




        public Gen9Info ReadFullServerInfoJsonGen9(string jsonserverinfo,string serverip, string serverno)
        {
            //declare json object
            //JObject serverinfo = JObject.Parse(jsonserverinfo);
            try
            { 
            //array to object
            JArray arrayinfo = JArray.Parse(jsonserverinfo);
            //MessageBox.Show(arrayinfo.Count.ToString()); 
            JObject comments = JObject.Parse(arrayinfo[0].ToString());
            JObject hpbios = JObject.Parse(arrayinfo[1].ToString());
            JObject chassis = JObject.Parse(arrayinfo[2].ToString());
            JObject firmware = JObject.Parse(arrayinfo[3].ToString());
            JObject computersystem = JObject.Parse(arrayinfo[4].ToString());
            JObject manager = JObject.Parse(arrayinfo[5].ToString());

            Gen9Info gen9 = new Gen9Info();
            //SDCARD Detection

            JObject SDCardinfo;
            //check if JSON File is there
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "SDCARD.json"))
            {
                //check for presence of sd card
                using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "SDCARD.json"))
                {
                    //STRIP OUT THE INCORRECT HEADERS THAT STOP JSON.NET READING CORRECTLY
                    SDCardinfo = JObject.Parse(r.ReadToEnd());
                }


                gen9.SDCardInserted = (string)SDCardinfo.SelectToken("SDCard.Status.State");
            }
            //string bioscurname = (string)hpbios.SelectToken("HpBios.restv1Systems1Bios.PowerRegulator");

            //MessageBox.Show(bioscurname);

            //FIRMWARE
            //FIRMWARE
            //extract bios current
            // string bioscurname = (string)serverinfo.SelectToken("Current.SystemRomActive[0].Name");
            gen9.BiosCurrentVersion = (string)firmware.SelectToken("Firmware.Path.Current.SystemRomActive[0].VersionString");
            //extract bios backup
            //string biosbakname = (string)serverinfo.SelectToken("Current.SystemRomBackup[0].Name");
            gen9.BiosBackupVersion = (string)firmware.SelectToken("Firmware.Path.Current.SystemRomBackup[0].VersionString");
            //extract ilo
            //string iloname = (string)serverinfo.SelectToken("Current.SystemBMC[0].Name");
            gen9.iLOVersion = (string)firmware.SelectToken("Firmware.Path.Current.SystemBMC[0].VersionString");
            //extract inteligent provisioning
            //string intprovname = (string)serverinfo.SelectToken("Current.IntelligentProvisioning[0].Name");
            gen9.IntelligentProvisioningVersion = (string)firmware.SelectToken("Firmware.Path.Current.IntelligentProvisioning[0].VersionString");
            //SPS Firmware
            //string spsname = (string)serverinfo.SelectToken("Current.SPSFirmwareVersionData[0].Name");
            gen9.SPSVersion = (string)firmware.SelectToken("Firmware.Path.Current.SPSFirmwareVersionData[0].VersionString");





          
            //SERVER INFO

            //extract bios current
            //MessageBox.Show((string)serverinfo.SelectToken("Manufacturer"));
            gen9.Manufacturer = (string)computersystem.SelectToken("ComputerSystem.Path.Manufacturer");
            gen9.Model = (string)computersystem.SelectToken("ComputerSystem.Path.Model");
            gen9.AssetTag = (string)computersystem.SelectToken("ComputerSystem.Path.AssetTag");

            string hn = (string)computersystem.SelectToken("ComputerSystem.Path.HostName");
            if (hn != null)
               gen9.HostName = hn.Trim();
            string cs = (string)computersystem.SelectToken("ComputerSystem.Path.SerialNumber");
           if (cs != null)
                gen9.ChassisSerial = cs.Trim();
            //extract ilo
            gen9.HealthStatus = (string)computersystem.SelectToken("ComputerSystem.Path.Status.Health");
            string sku = (string)computersystem.SelectToken("ComputerSystem.Path.SKU");
            if (sku != null)
                gen9.SKU = sku.Trim();
            gen9.TotalMemory = (string)computersystem.SelectToken("ComputerSystem.Path.Memory.TotalSystemMemoryGB");
            gen9.MemoryHealth = (string)computersystem.SelectToken("ComputerSystem.Path.Memory.Status.HealthRollUp");
            //extract inteligent provisioning
            //string model = (string)firmwareLevels.SelectToken("Model");
            gen9.IntelligentProvisioningLocation = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hp.IntelligentProvisioningLocation");
            //string intprovversion = (string)firmwareLevels.SelectToken("Oem.Hp.IntelligentProvisioningVersion");
            gen9.PostState = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hp.PostState");

            gen9.CPUFamily = (string)computersystem.SelectToken("ComputerSystem.Path.Processors.ProcessorFamily");
            gen9.CPUCount = (string)computersystem.SelectToken("ComputerSystem.Path.Processors.Count");
            gen9.CPUHealth = (string)computersystem.SelectToken("ComputerSystem.Path.Memory.Status.HealthRollUp");
            //power
            gen9.PowerState = (string)computersystem.SelectToken("PowerState");
            gen9.PowerAutoOn = (string)computersystem.SelectToken("Oem.Hp.PowerAutoOn");
            gen9.PowerOnDelay = (string)computersystem.SelectToken("Oem.Hp.PowerOnDelay");
            gen9.PowerAutoOn = (string)computersystem.SelectToken("Oem.Hp.PowerAutoOn");
            gen9.PowerRegulatorMode = (string)computersystem.SelectToken("Oem.Hp.PowerRegulatorMode");
            gen9.PowerAllocationLimit = (string)computersystem.SelectToken("Oem.Hp.PowerAllocationLimit");

            //iLO Self Test
            gen9.NVRAMDataNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[0].Notes");
            gen9.NVRAMDataStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[0].Status");
            gen9.NVRAMSpaceNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[1].Notes");
            gen9.NVRAMSpaceStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[1].Status");
            gen9.EmbeddedFlashSDCardNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[2].Notes");
            gen9.EmbeddedFlashSDCardStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[2].Status");
            gen9.EEPROMNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[3].Notes");
            gen9.EEPROMStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[3].Status");
            gen9.HostRomNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[4].Notes");
            gen9.HostRomStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[4].Status");
            gen9.SupportedHostNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[5].Notes");
            gen9.SupportedHostStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[5].Status");
            gen9.PowerManagementControllerNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[6].Notes");
            gen9.PowerManagementControllerStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[6].Status");
            gen9.CPLDPAL0Notes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[7].Notes");
            gen9.CPLDPAL0Status = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[7].Status");
            gen9.CPLDPAL1Notes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[8].Notes");
            gen9.CPLDPAL1Status = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[8].Status");


            //ILO License   public string ILOLicense { get; set; }
            gen9.ILOLicense = (string)manager.SelectToken("Manager.Path.Oem.Hp.License.LicenseString");


            //PHYSICAL DISKS
            JArray diskarrayinfo = JArray.Parse(jsonserverinfo);


            //check if JSON File is there
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "Disks.json"))
            {
                //array to object

                //JObject o1 = JObject.Parse(File.ReadAllText(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "Disks.json"));

                //foreach(var)


                ////check for presence of sd card
                using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "Disks.json"))
                {

                    ////Read from File
                    //// string streamString = r.ReadToEnd();

                    string streamString = "";

                    string HeaderString = "";
                    string PathString = "";

                    // Use while not null pattern in while loop.
                    string line;
                    while ((line = r.ReadLine()) != null)
                    {
                        // Insert logic here.
                        // ... The "line" variable is a line in the file.
                        // ... Add it to our List.



                        if (line.Contains("@odata") || line.Contains("href"))
                        {

                        }
                        else
                        {

                            //Gen8,9
                            if (line.Contains("HpSmartStorageDiskDrive"))
                            {
                                HeaderString = "HpSmartStorageDiskDrive" + Regex.Match(line, @"HpSmartStorageDiskDrive(.+?):").Groups[1].Value.Replace("\"", "");
                                line = line.Replace(HeaderString, "Disk");
                                //  MessageBox.Show(HeaderString);
                            }
                            //Gen10
                            if (line.Contains("#HpeSmartStorageDiskDrive."))
                            {
                                HeaderString = "#HpeSmartStorageDiskDrive." + Regex.Match(line, @"#HpeSmartStorageDiskDrive.(.+?):").Groups[1].Value.Replace("\"", "");
                                line = line.Replace(HeaderString, "Disk");
                                //  MessageBox.Show(HeaderString);
                            }


                        }


                        if (line.Contains("@odata") || line.Contains("href"))
                        {

                        }
                        else
                        {

                            //gen8,9 
                            if (line.Contains("/rest/v1/Systems/1/SmartStorage/ArrayControllers/"))
                            {
                                // MessageBox.Show(PathString);

                                PathString = "/rest/v1/Systems/1/SmartStorage/ArrayControllers/" + Regex.Match(line, @"/rest/v1/Systems/1/SmartStorage/ArrayControllers/(.+?):").Groups[1].Value.Replace("\"", "");
                                line = line.Replace(PathString, "Path");

                            }

                            //gen10

                            if (line.Contains("/redfish/v1/Systems/1/SmartStorage/ArrayControllers/"))
                            {
                                // MessageBox.Show(PathString);

                                PathString = "/redfish/v1/Systems/1/SmartStorage/ArrayControllers/" + Regex.Match(line, @"/redfish/v1/Systems/1/SmartStorage/ArrayControllers/(.+?):").Groups[1].Value.Replace("\"", "");
                                line = line.Replace(PathString, "Path");

                            }

                        }

                        streamString += line;


                    }


                    diskarrayinfo = JArray.Parse(streamString);

                    
                }

                //Create holding array
                foreach (JObject disk in diskarrayinfo)
                {
                    ////MessageBox.Show(disk.SelectTokens)
                    //MessageBox.Show((string)disk.SelectToken("Disk.Path.Model"));
                    //MessageBox.Show((string)disk.SelectToken("Disk.Path.Status.Health"));
                    //string[] strarr = new string[2];

                    if ((string)disk.SelectToken("Disk.Path.Model") != null)
                    {

                        //MessageBox.Show((string)disk.SelectToken("Disk.Path.Model"));
                        HPPhysicalDrivesModel drive = new HPPhysicalDrivesModel();


                        drive.Model = (string)disk.SelectToken("Disk.Path.Model");
                        drive.Capacity = (string)disk.SelectToken("Disk.Path.CapacityGB");
                        if ((bool)disk.SelectToken("Disk.Path.EncryptedDrive") == false)
                        {
                            drive.Encryption = "No";
                        }
                        else
                        {
                            drive.Encryption = "Yes";
                        }
                        drive.SerialNumber = (string)disk.SelectToken("Disk.Path.SerialNumber");
                        drive.MediaType = (string)disk.SelectToken("Disk.Path.MediaType");
                        drive.Location = (string)disk.SelectToken("Disk.Path.Location");
                        drive.Interface = (string)disk.SelectToken("Disk.Path.InterfaceType");
                        drive.InterfaceSpeed = (string)disk.SelectToken("Disk.Path.InterfaceSpeedMbps");
                        drive.BlockSize = (string)disk.SelectToken("Disk.Path.BlockSizeBytes");
                        drive.Health = (string)disk.SelectToken("Disk.Path.Status.Health");
                        gen9.PhysicalDriveInfo.Add(drive);
                        //gen8.StorageInfo.LogicalDrives.Add(new HealthLogicalDriveGen8("PHYSICAL ONLY", "", "", "", "", "", drive));
                    }


                }


            }




            //READ SEPARATE XML FILE
            //read xml health summary
            GET_EMBEDDED_HEALTH_DATA_GEN9.GET_EMBEDDED_HEALTH_DATA HealthSum = new GET_EMBEDDED_HEALTH_DATA_GEN9.GET_EMBEDDED_HEALTH_DATA();

            switch (serverno)
            {
              
                case "S1":
                    HealthSum = GetHealthSummaryGen9(serverip, "S1");
                    break;
             
            }



            //try
            //{

                if (HealthSum != null)
                {
                    if (HealthSum.HEALTH_AT_A_GLANCE.BIOS_HARDWARE.STATUS != null)
                    gen9.BiosHardwareStatus = HealthSum.HEALTH_AT_A_GLANCE.BIOS_HARDWARE.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.PROCESSOR != null)
                    gen9.ProcessorStatus = HealthSum.HEALTH_AT_A_GLANCE.PROCESSOR.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.MEMORY != null)
                    gen9.MemoryStatus = HealthSum.HEALTH_AT_A_GLANCE.MEMORY.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.NETWORK != null)
                    gen9.NetworkStatus = HealthSum.HEALTH_AT_A_GLANCE.NETWORK.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.STORAGE != null)
                    gen9.StorageStatus = HealthSum.HEALTH_AT_A_GLANCE.STORAGE.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.TEMPERATURE != null)
                    gen9.TemperatureStatus = HealthSum.HEALTH_AT_A_GLANCE.TEMPERATURE.STATUS;
                    if(HealthSum.HEALTH_AT_A_GLANCE.FANS != null)
                    gen9.FansStatus = HealthSum.HEALTH_AT_A_GLANCE.FANS.FirstOrDefault().STATUS.ToString();
                    //gen9.FansRedundancy = HealthSum.HEALTH_AT_A_GLANCE.FANS.FirstOrDefault().REDUNDANCY.ToString();
                    if(HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES != null)
                    gen9.PowerSuppliesStatus = HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES.FirstOrDefault().STATUS.ToString();
                    //gen9.PowerSupplyRedundancy = HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES.FirstOrDefault().REDUNDANCY.ToString();

                    //FANS Collection
                    if (HealthSum.HEALTH_AT_A_GLANCE.FANS != null)
                        foreach (var itm in HealthSum.HEALTH_AT_A_GLANCE.FANS)
                        {
                            //MessageBox.Show(itm.STATUS);

                            gen9.FansRedundancy = itm.REDUNDANCY;
                        }


                    //POWER SUPPLIES Collection
                    if (HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES != null)
                        foreach (var itm in HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES)
                        {

                            //gen9.PowerSuppliesStatus = itm.STATUS.FirstOrDefault().ToString(); 
                            gen9.PowerSupplyRedundancy = itm.REDUNDANCY;
                        }


                    //Read rest of Data in XML file
                    //FANS
                    gen9.FanInfo = new System.Collections.ObjectModel.ObservableCollection<HealthFansGen9>();

                    if (HealthSum.FANS != null)
                        foreach (var itm in HealthSum.FANS)
                        {
                            gen9.FanInfo.Add(new HealthFansGen9(itm.LABEL.VALUE, itm.ZONE.VALUE, itm.STATUS.VALUE, itm.SPEED.VALUE + "%"));
                        }
                    //TEMPERATURES
                    gen9.TemperatureInfo = new System.Collections.ObjectModel.ObservableCollection<HealthTemperatureGen9>();

                    if (HealthSum.TEMPERATURE != null)
                        foreach (var itm in HealthSum.TEMPERATURE)
                        {

                            gen9.TemperatureInfo.Add(new HealthTemperatureGen9(itm.LABEL.VALUE, itm.LOCATION.VALUE, itm.STATUS.VALUE, itm.CURRENTREADING.VALUE + " " + itm.CURRENTREADING.UNIT, itm.CAUTION.VALUE + " " + itm.CURRENTREADING.UNIT, itm.CRITICAL.VALUE + " " + itm.CURRENTREADING.UNIT));
                        }

                    //CPU
                    gen9.CPUInfo = new System.Collections.ObjectModel.ObservableCollection<HealthCPUGen9>();

                    if (HealthSum.PROCESSORS != null)
                        foreach (var itm in HealthSum.PROCESSORS)
                        {
                            gen9.CPUInfo.Add(new HealthCPUGen9(itm.LABEL.VALUE, itm.NAME.VALUE, itm.STATUS.VALUE, itm.SPEED.VALUE, itm.EXECUTION_TECHNOLOGY.VALUE, itm.MEMORY_TECHNOLOGY.VALUE, itm.INTERNAL_L1_CACHE.VALUE, itm.INTERNAL_L2_CACHE.VALUE, itm.INTERNAL_L3_CACHE.VALUE));
                        }

                    //MEMORY
                    if (HealthSum.MEMORY != null)
                    {

                        gen9.MemoryInfo = new System.Collections.ObjectModel.ObservableCollection<HealthMemoryGen9>();
                        //HealthSum.MEMORY.ADVANCED_MEMORY_PROTECTION
                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {

                            if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_1 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_1)
                            {

                                gen9.MemoryInfo.Add(new HealthMemoryGen9("CPU1", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE, itm.PART.NUMBER, itm.TYPE.VALUE, itm.SIZE.VALUE, itm.FREQUENCY.VALUE, itm.MINIMUM_VOLTAGE.VALUE, itm.RANKS.VALUE, itm.TECHNOLOGY.VALUE));

                            }
                        }


                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_2 != null)
                            foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_2)
                            {

                                gen9.MemoryInfo.Add(new HealthMemoryGen9("CPU2", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE, itm.PART.NUMBER, itm.TYPE.VALUE, itm.SIZE.VALUE, itm.FREQUENCY.VALUE, itm.MINIMUM_VOLTAGE.VALUE, itm.RANKS.VALUE, itm.TECHNOLOGY.VALUE));

                            }
                        }


                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_3 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_3)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_3 != null)
                                    {
                                        gen9.MemoryInfo.Add(new HealthMemoryGen9("CPU3", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }


                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_4 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_4)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_4 != null)
                                    {
                                        gen9.MemoryInfo.Add(new HealthMemoryGen9("CPU4", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }



                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_1 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_1)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_1 != null)
                                    {
                                        gen9.MemoryInfo.Add(new HealthMemoryGen9("Memory_Board_1", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }


                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_2 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_2)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_2 != null)
                                    {
                                        gen9.MemoryInfo.Add(new HealthMemoryGen9("Memory_Board_2", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_3 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_3)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_3 != null)
                                    {
                                        gen9.MemoryInfo.Add(new HealthMemoryGen9("Memory_Board_3", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_4 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_4)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_4 != null)
                                    {
                                        gen9.MemoryInfo.Add(new HealthMemoryGen9("Memory_Board_4", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_5 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_5)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_5 != null)
                                    {
                                        gen9.MemoryInfo.Add(new HealthMemoryGen9("Memory_Board_5", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_6 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_6)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_6 != null)
                                    {
                                        gen9.MemoryInfo.Add(new HealthMemoryGen9("Memory_Board_6", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_7 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_7)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_7 != null)
                                    {
                                        gen9.MemoryInfo.Add(new HealthMemoryGen9("Memory_Board_7", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_8 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_8)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_8 != null)
                                    {
                                        gen9.MemoryInfo.Add(new HealthMemoryGen9("Memory_Board_8", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }




                    }


                    //Count Populated Memory
                    int populatedCount = 0;
                foreach (var itm in gen9.MemoryInfo)
                {

                    if (itm.Status != "Not Present")
                    {
                        //Increase by 1
                        populatedCount += 1;
                    }

                }
                // Assign to property
                gen9.PopulatedSlots = populatedCount.ToString();





                //NETWORK 

                if (HealthSum.NIC_INFORMATION != null)
                    {
                        gen9.NICInfo = new System.Collections.ObjectModel.ObservableCollection<HealthNetworkGen9>();

                        if (HealthSum.NIC_INFORMATION != null)
                        if (HealthSum.NIC_INFORMATION.NIC != null)
                        {
                            foreach (var itm in HealthSum.NIC_INFORMATION.NIC)
                            {
                                if (itm.NETWORK_PORT != null)
                                if (itm.NETWORK_PORT != null || itm.PORT_DESCRIPTION != null || itm.LOCATION != null || itm.IP_ADDRESS != null || itm.MAC_ADDRESS != null || itm.STATUS != null)
                                {
                                    gen9.NICInfo.Add(new HealthNetworkGen9(itm.NETWORK_PORT.VALUE, itm.PORT_DESCRIPTION.VALUE, itm.LOCATION.VALUE, itm.IP_ADDRESS.VALUE, itm.MAC_ADDRESS.VALUE, itm.STATUS.VALUE));
                                }
                               
                            }
                        }
                           

                    }

                //Assign to ILOPORT Status for test report
                gen9.ILONetworkPort = gen9.NICInfo.FirstOrDefault().Status;


                if (HealthSum.POWER_SUPPLIES != null)
                    {
                        //POWER
                        gen9.PowerInfo = new System.Collections.ObjectModel.ObservableCollection<HealthPowerGen9>();

                        //HealthSum.
                        if (HealthSum.POWER_SUPPLIES.SUPPLY != null)
                            foreach (var itm in HealthSum.POWER_SUPPLIES.SUPPLY)
                            {

                          
                                if (HealthSum.POWER_SUPPLIES.SUPPLY != null)
                                {

                                if(itm.PRESENT != null)
                                if (itm.PRESENT != null || itm.STATUS != null || itm.MODEL != null || itm.SPARE != null || itm.SERIAL_NUMBER != null || itm.CAPACITY != null || itm.FIRMWARE_VERSION != null)
                                {
                                    gen9.PowerInfo.Add(new HealthPowerGen9(itm.LABEL.VALUE, itm.PRESENT.VALUE, itm.STATUS.VALUE, itm.MODEL.VALUE, itm.SPARE.VALUE, itm.SERIAL_NUMBER.VALUE, itm.SPARE.VALUE, itm.SERIAL_NUMBER.VALUE, itm.CAPACITY.VALUE, itm.FIRMWARE_VERSION.VALUE.ToString()));
                                }
                                        
                                }

                           

                            //if(itm.LABEL.VALUE != null || itm.PRESENT.VALUE != null || itm.STATUS.VALUE != null || itm.MODEL.VALUE != null || itm.SPARE.VALUE != null || itm.SERIAL_NUMBER.VALUE != null || itm.SPARE.VALUE != null|| itm.SERIAL_NUMBER.VALUE != null || itm.CAPACITY.VALUE != null)
                            //  gen9.PowerInfo.Add(new HealthPowerGen9(itm.LABEL.VALUE.ToString(), itm.PRESENT.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.MODEL.VALUE.ToString(), itm.SPARE.VALUE.ToString(), itm.SERIAL_NUMBER.VALUE.ToString(), itm.SPARE.VALUE.ToString(), itm.SERIAL_NUMBER.VALUE.ToString(), itm.CAPACITY.VALUE.ToString(), itm.FIRMWARE_VERSION.VALUE.ToString()));
                            //gen9.PowerInfo.Add(new HealthPowerGen9(itm.LABEL.VALUE.ToString(), itm.PRESENT.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.MODEL.VALUE.ToString(), itm.SPARE.VALUE.ToString(), itm.SERIAL_NUMBER.VALUE.ToString(), itm.SPARE.VALUE.ToString(), itm.SERIAL_NUMBER.VALUE.ToString(), itm.CAPACITY.VALUE.ToString(), ""));
                        }

                    }

                    if (HealthSum.STORAGE.CONTROLLER != null)
                    {

                        //STORAGE
                        HealthStorageGen9 controllerinfo = new HealthStorageGen9();
                        if (HealthSum.STORAGE.CONTROLLER.LABEL.VALUE != null)
                            controllerinfo.Label = HealthSum.STORAGE.CONTROLLER.LABEL.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.STATUS.VALUE != null)
                            controllerinfo.Status = HealthSum.STORAGE.CONTROLLER.STATUS.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.CONTROLLER_STATUS.VALUE != null)
                            controllerinfo.ControllerStatus = HealthSum.STORAGE.CONTROLLER.CONTROLLER_STATUS.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.SERIAL_NUMBER.VALUE != null)
                            controllerinfo.Serial = HealthSum.STORAGE.CONTROLLER.SERIAL_NUMBER.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.MODEL.VALUE != null)
                            controllerinfo.Model = HealthSum.STORAGE.CONTROLLER.MODEL.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.FW_VERSION.VALUE.ToString() != null)
                            controllerinfo.Firmware = HealthSum.STORAGE.CONTROLLER.FW_VERSION.VALUE.ToString();
                        //MessageBox.Show(controllerinfo.Model);
                        if (HealthSum.STORAGE.CONTROLLER.DRIVE_ENCLOSURE != null)
                            foreach (var itm in HealthSum.STORAGE.CONTROLLER.DRIVE_ENCLOSURE)
                            {
                                controllerinfo.DriveEnclosure.Add(new DriveEnclosureInfoGen9(itm.LABEL.VALUE, itm.STATUS.VALUE, itm.DRIVE_BAY.VALUE.ToString()));
                                //MessageBox.Show(itm.LABEL.VALUE + "  " +);
                            }



                        if (HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE != null)
                        foreach (var itm in HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE)
                        {
                            controllerinfo.LogicalDrives.Add(new HealthLogicalDriveGen9(itm.LABEL.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.CAPACITY.VALUE.ToString(), itm.FAULT_TOLERANCE.VALUE.ToString(), itm.LOGICAL_DRIVE_TYPE.VALUE.ToString(), itm.ENCRYPTION_STATUS.VALUE.ToString(), itm.PHYSICAL_DRIVE));
                            //MessageBox.Show(itm.FW_VERSION.VALUE.ToString());

                            //foreach (var itm2 in itm.PHYSICAL_DRIVE)
                            //{
                            //    controllerinfo.LogicalDrives...Add(new PhysicalDrivesGen8(itm.LABEL.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.SERIAL_NUMBER.VALUE.ToString(), itm.MODEL.VALUE.ToString(), itm.CAPACITY.VALUE.ToString(), itm.MARKETING_CAPACITY.VALUE.ToString(), itm.LOCATION.VALUE.ToString(), itm.FW_VERSION.VALUE.ToString(), itm.DRIVE_CONFIGURATION.VALUE.ToString(), itm.ENCRYPTION_STATUS.VALUE.ToString(), itm.MEDIA_TYPE.VALUE.ToString()));
                            //    //MessageBox.Show(itm.FW_VERSION.VALUE.ToString());
                            //}

                            // itm.PHYSICAL_DRIVE.
                        }





                        gen9.StorageInfo = controllerinfo;
                    }

                }
             


                        return gen9;

            }
            catch (Exception ex)
            {
                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;
                //Console.WriteLine(ex.Message);
                return null;
            }
        }



        public GET_EMBEDDED_HEALTH_DATA_GEN10.GET_EMBEDDED_HEALTH_DATA GetHealthSummaryGen10(string serverip, string serverno)
        {

            GET_EMBEDDED_HEALTH_DATA_GEN10.GET_EMBEDDED_HEALTH_DATA HealthSum = new GET_EMBEDDED_HEALTH_DATA_GEN10.GET_EMBEDDED_HEALTH_DATA();

            string ServerInfoJson;
            //if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth.xml") || File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth1.xml")
            //    File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth2.xml") || File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth3.xml")
            //    File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\ServerHealth4.xml")))
            //{

            switch (serverno)
            {
               
                case "S1":

                    //Full Info
                    using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml"))
                    {
                        //STRIP OUT THE INVALID HEADERS THAT STOP JSON.NET READING CORRECTLY
                        ServerInfoJson = r.ReadToEnd().Replace("Server IP is : " + serverip + ":443", "")
                                                      .Replace("<?xml version=\"1.0\"?>", "")
                                                      .Replace("<RIBCL VERSION=\"2.23\">", "")
                                                      .Replace("<RESPONSE", "")
                                                      .Replace("  STATUS=\"0x0000\"", "")
                                                      .Replace("MESSAGE='No error'", "")
                                                      .Replace("   />", "")
                                                      .Replace("</RIBCL>", "")
                                                      .Replace("Script succeeded for IP:" + serverip + ":443", "").Trim()
                                                      .Replace("N/A", "0")
                                                      //.Replace(".", "_")
                                                      .Replace("Version 0x34", "Version")
                                                      .Replace("Version 0x01", "Version")
                                                      //.Replace("<FIRMWARE_NAME VALUE = ", "<FIRMWARE_NAME Value=")
                                                      .Replace("<FIRMWARE_VERSION VALUE = ", "<FIRMWARE_VERSION Value = ")
                                                      //.Replace("<FIRMWARE_FAMILY VALUE = ", "<FIRMWARE_FAMILY Value=")
                                                      ;


                    }


                    //if(ServerInfoJson.Contains("FIRMWARE_VERSION VALUE ="))
                    //{
                    //    ServerInfoJson = ServerInfoJson.Replace("FIRMWARE_VERSION VALUE =", "FIRMWARE_VERSION Value = ");
                    //}

                    //StaticFunctions.
                    File.WriteAllText(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml", ServerInfoJson);
                    //await PutTaskDelay(3000);


                    //XElement root = XElement.Parse(ServerInfoJson);
                    ////XElement root = XElement.Load("TestConfig.xml");
                    //IEnumerable<XElement> tests =
                    //    from el in root.Elements("FIRMWARE_INFORMATION")
                    //    where (string)el.Element("INDEX_1") == "Examp2.EXE"
                    //    select el;
                    //foreach (XElement el in tests)
                    //    Console.WriteLine((string)el.Attribute("TestId"));
                    //MessageBox.Show(doc.Document.DescendantNodes);


                    HealthSum = XMLTools.ReadFromXmlString<GET_EMBEDDED_HEALTH_DATA_GEN10.GET_EMBEDDED_HEALTH_DATA>(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml");



                    break;
              
            }
            //}
            //else
            //{
            //    return null;
            //}

            return HealthSum;


        }

        public Gen10Info ReadFullServerInfoJsonGen10(string jsonserverinfo,string jsonfirmwareinfo, string serverip, string serverno)
        {

            try
            {


                //declare json object
            //JObject serverinfo = JObject.Parse(jsonserverinfo);

                //Json ALL Info
                //array to object
            JArray arrayinfo = JArray.Parse(jsonserverinfo);
            //MessageBox.Show(arrayinfo.Count.ToString()); 
            JObject comments = JObject.Parse(arrayinfo[0].ToString());
            JObject hpbios = JObject.Parse(arrayinfo[1].ToString());
            JObject chassis = JObject.Parse(arrayinfo[2].ToString());
            //JObject firmware = JObject.Parse(arrayinfo[3].ToString());
            JObject computersystem = JObject.Parse(arrayinfo[3].ToString());
            JObject manager = JObject.Parse(arrayinfo[4].ToString());


            //Firmware Info
            if(jsonfirmwareinfo != null)
            {
              
            }

            JObject firmwareinfo = JObject.Parse(jsonfirmwareinfo);

            Gen10Info gen10 = new Gen10Info();

            //SDCARD Detection
            JObject SDCardinfo;
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "SDCARD.json"))
            {
                //check for presence of sd card
                using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "SDCARD.json"))
                {
                    //STRIP OUT THE INCORRECT HEADERS THAT STOP JSON.NET READING CORRECTLY
                    SDCardinfo = JObject.Parse(r.ReadToEnd());
                }


                gen10.SDCardInserted = (string)SDCardinfo.SelectToken("SDCard.Status.State");

            }
            //string bioscurname = (string)hpbios.SelectToken("HpBios.restv1Systems1Bios.PowerRegulator");
            //string bioscurname = (string)hpbios.SelectToken("HpBios.restv1Systems1Bios.PowerRegulator");

            // MessageBox.Show(bioscurname);

            //FIRMWARE
            //FIRMWARE
            //extract bios current
            // string bioscurname = (string)serverinfo.SelectToken("Current.SystemRomActive[0].Name");
            gen10.BiosCurrentVersion = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.Bios.Current.VersionString");// (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.Bios.Current.VersionString");

             
                //extract bios backup
            //string biosbakname = (string)serverinfo.SelectToken("Current.SystemRomBackup[0].Name");
            gen10.BiosBackupVersion = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.Bios.Backup.VersionString");
            //extract ilo
            //string iloname = (string)serverinfo.SelectToken("Current.SystemBMC[0].Name");
            gen10.iLOVersion = (string)manager.SelectToken("Manager.Path.Oem.Hpe.Firmware.Current.VersionString");
            //extract inteligent provisioning
            //string intprovname = (string)serverinfo.SelectToken("Current.IntelligentProvisioning[0].Name");
            gen10.IntelligentProvisioningVersion = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.IntelligentProvisioning.VersionString");
            //SPS Firmware
            //string spsname = (string)serverinfo.SelectToken("Current.SPSFirmwareVersionData[0].Name");
            //gen10.SPSVersion = (string)chassis.SelectToken("Chassis.Path.Oem.Hpe.Firmware.SPSFirmwareVersionData.Current.VersionString");
            // string bioscurname = (string)serverinfo.SelectToken("Current.SystemRomActive[0].Name");


            //Alternative to get all firmware from a Gen10
            JsonSerializer serializer = new JsonSerializer();
           
            Gen10FirmwareJson gen10fw = new Gen10FirmwareJson();
            gen10fw = (Gen10FirmwareJson)serializer.Deserialize(new JTokenReader(firmwareinfo), typeof(Gen10FirmwareJson));

            ///To Access Collections From Restful api returned Json
            ///1) Use rawget if not accessible via a main keyword, see API Reference for Types
            ///2) Use the redfish data uri path to the information you require
            ///3) Once file is exported, copy and paste special inside a new class under models
            ///4) use JsonSerializer to deserialize the information into an instance of the class you just created
            ///5) Create a foreach loop and loop through the collection pulling out the information you require

            foreach (var itm in gen10fw.Members)
            {
               // MessageBox.Show(itm.Name);
                //switch (itm.Name)
                //{
                //    case itm.Name.Contains(""):
                //        break;
                //}
                if (itm.Name.Contains("Innovation Engine"))
                {
                    gen10.InnovationEngine = itm.Version;
                }

                if (itm.Name.Contains("Intelligent Provisioning"))
                {
                    gen10.IntelligentProvisioningVersion = itm.Version;
                }

                if (itm.Name.Contains("Server Platform Services (SPS) Firmware"))
                {
                        gen10.SPSVersion = itm.Version;
                }

                    //    MessageBox.Show(itm.SelectToken("Members.Name").ToString());firmwareinfo.SelectTokens("Members")
                    //   itm.SingleOrDefault
                    //   // if(itm.SelectToken)
                    //    //fwlst.Add(itm);
                }




       


            //SERVER INFO
            //to get firmware from gen10
            //rawget /redfish/v1/updateservice/firmwareinventory/ --expand -f firmware.json

            //extract bios current
            //MessageBox.Show((string)serverinfo.SelectToken("Manufacturer"));
            gen10.Manufacturer = (string)computersystem.SelectToken("ComputerSystem.Path.Manufacturer");
            gen10.Model = (string)computersystem.SelectToken("ComputerSystem.Path.Model");
            gen10.AssetTag = (string)computersystem.SelectToken("ComputerSystem.Path.AssetTag");

            string hn = (string)computersystem.SelectToken("ComputerSystem.Path.HostName");
            gen10.HostName = hn;
            string cs = (string)computersystem.SelectToken("ComputerSystem.Path.SerialNumber");
            gen10.ChassisSerial = cs;
            //extract ilo
            gen10.HealthStatus = (string)computersystem.SelectToken("ComputerSystem.Path.Status.Health");
            string sku = (string)computersystem.SelectToken("ComputerSystem.Path.SKU");
            gen10.SKU = sku;
            gen10.TotalMemory = (string)computersystem.SelectToken("ComputerSystem.Path.MemorySummary.TotalSystemMemoryGiB");
            gen10.MemoryHealth = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.AggregateHealthStatus.Memory.Status.Health");
            //extract inteligent provisioning
            //string model = (string)firmwareLevels.SelectToken("Model");
            gen10.IntelligentProvisioningLocation = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.IntelligentProvisioningLocation");
            //string intprovversion = (string)firmwareLevels.SelectToken("Oem.Hp.IntelligentProvisioningVersion");
            gen10.PostState = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.PostState");

            gen10.CPUFamily = (string)computersystem.SelectToken("ComputerSystem.Path.ProcessorSummary.Model");
            if(gen10.CPUFamily != null)
            gen10.CPUFamily = gen10.CPUFamily.Trim();
            gen10.CPUCount = (string)computersystem.SelectToken("ComputerSystem.Path.ProcessorSummary.Count");
            gen10.CPUHealth = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.AggregateHealthStatus.Processors.Status.Health");
            //power
            gen10.PowerState = (string)computersystem.SelectToken("PowerState");
            gen10.PowerAutoOn = (string)computersystem.SelectToken("Oem.Hpe.PowerAutoOn");
            gen10.PowerOnDelay = (string)computersystem.SelectToken("Oem.Hpe.PowerOnDelay");
            gen10.PowerAutoOn = (string)computersystem.SelectToken("Oem.Hpe.PowerAutoOn");
            gen10.PowerRegulatorMode = (string)computersystem.SelectToken("Oem.Hpe.PowerRegulatorMode");
            gen10.PowerAllocationLimit = (string)computersystem.SelectToken("Oem.Hpe.PowerAllocationLimit");

            //iLO Self Test
            gen10.NVRAMDataNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[0].Notes");
            gen10.NVRAMDataStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[0].Status");
            gen10.NVRAMSpaceNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[1].Notes");
            gen10.NVRAMSpaceStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[1].Status");
            gen10.EmbeddedFlashSDCardNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[2].Notes");
            gen10.EmbeddedFlashSDCardStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[2].Status");
            gen10.EEPROMNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[3].Notes");
            gen10.EEPROMStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[3].Status");
            gen10.HostRomNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[4].Notes");
            gen10.HostRomStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[4].Status");
            gen10.SupportedHostNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[5].Notes");
            gen10.SupportedHostStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[5].Status");
            gen10.PowerManagementControllerNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[6].Notes");
            gen10.PowerManagementControllerStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[6].Status");
            gen10.CPLDPAL0Notes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[7].Notes");
            gen10.CPLDPAL0Status = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[7].Status");
            gen10.CPLDPAL1Notes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[8].Notes");
            gen10.CPLDPAL1Status = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[8].Status");

            //ILO License   public string ILOLicense { get; set; }
            gen10.ILOLicense = (string)manager.SelectToken("Manager.Path.Oem.Hpe.License.LicenseString");

                //READ SEPARATE XML FILE


                //PHYSICAL DISKS
                JArray diskarrayinfo = JArray.Parse(jsonserverinfo);


                //check if JSON File is there
                if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "Disks.json"))
                {
                    //array to object

                    //JObject o1 = JObject.Parse(File.ReadAllText(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "Disks.json"));

                    //foreach(var)


                    ////check for presence of sd card
                    using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "Disks.json"))
                    {

                        ////Read from File
                        //// string streamString = r.ReadToEnd();

                        string streamString = "";

                        string HeaderString = "";
                        string PathString = "";

                        // Use while not null pattern in while loop.
                        string line;
                        while ((line = r.ReadLine()) != null)
                        {
                            // Insert logic here.
                            // ... The "line" variable is a line in the file.
                            // ... Add it to our List.



                            if (line.Contains("@odata") || line.Contains("href"))
                            {

                            }
                            else
                            {

                                //Gen8,9
                                //if (line.Contains("HpSmartStorageDiskDrive"))
                                //{
                                //    HeaderString = "HpSmartStorageDiskDrive" + Regex.Match(line, @"HpSmartStorageDiskDrive(.+?):").Groups[1].Value.Replace("\"", "");
                                //    line = line.Replace(HeaderString, "Disk");
                                //    //  MessageBox.Show(HeaderString);
                                //}
                                //Gen10
                                if (line.Contains("#HpeSmartStorageDiskDrive."))
                                {
                                    HeaderString = "#HpeSmartStorageDiskDrive." + Regex.Match(line, @"#HpeSmartStorageDiskDrive.(.+?):").Groups[1].Value.Replace("\"", "");
                                    line = line.Replace(HeaderString, "Disk");
                                    //  MessageBox.Show(HeaderString);
                                }


                            }


                            if (line.Contains("@odata") || line.Contains("href"))
                            {

                            }
                            else
                            {

                                //gen8,9 
                                //if (line.Contains("/rest/v1/Systems/1/SmartStorage/ArrayControllers/"))
                                //{
                                //    // MessageBox.Show(PathString);

                                //    PathString = "/rest/v1/Systems/1/SmartStorage/ArrayControllers/" + Regex.Match(line, @"/rest/v1/Systems/1/SmartStorage/ArrayControllers/(.+?):").Groups[1].Value.Replace("\"", "");
                                //    line = line.Replace(PathString, "Path");

                                //}

                                //gen10

                                if (line.Contains("/redfish/v1/Systems/1/SmartStorage/ArrayControllers/"))
                                {
                                    // MessageBox.Show(PathString);

                                    PathString = "/redfish/v1/Systems/1/SmartStorage/ArrayControllers/" + Regex.Match(line, @"/redfish/v1/Systems/1/SmartStorage/ArrayControllers/(.+?):").Groups[1].Value.Replace("\"", "");
                                    line = line.Replace(PathString, "Path");

                                }

                            }

                            streamString += line;


                        }


                        diskarrayinfo = JArray.Parse(streamString);

                    }

                   // MessageBox.Show(diskarrayinfo.ToString());

                    //Create holding array
                    foreach (JObject disk in diskarrayinfo)
                    {
                       // MessageBox.Show();

                        ////MessageBox.Show(disk.SelectTokens)
                        //MessageBox.Show((string)disk.SelectToken("Disk.Path.Model"));
                        //MessageBox.Show((string)disk.SelectToken("Disk.Path.Status.Health"));
                        //string[] strarr = new string[2];

                        if ((string)disk.SelectToken("Disk.Path.Model") != null)
                        {

                        

                            HPPhysicalDrivesModel drive = new HPPhysicalDrivesModel();

                            drive.Model = (string)disk.SelectToken("Disk.Path.Model");
                            drive.Capacity = (string)disk.SelectToken("Disk.Path.CapacityGB");
                            if ((bool)disk.SelectToken("Disk.Path.EncryptedDrive") == false)
                            {
                                drive.Encryption = "No";
                            }
                            else
                            {
                                drive.Encryption = "Yes";
                            }
                            drive.SerialNumber = (string)disk.SelectToken("Disk.Path.SerialNumber");
                            drive.MediaType = (string)disk.SelectToken("Disk.Path.MediaType");
                            drive.Location = (string)disk.SelectToken("Disk.Path.Location");
                            drive.Interface = (string)disk.SelectToken("Disk.Path.InterfaceType");
                            drive.InterfaceSpeed = (string)disk.SelectToken("Disk.Path.InterfaceSpeedMbps");
                            drive.BlockSize = (string)disk.SelectToken("Disk.Path.BlockSizeBytes");
                            drive.Health = (string)disk.SelectToken("Disk.Path.Status.Health");
                            gen10.PhysicalDriveInfo.Add(drive);
                            //gen8.StorageInfo.LogicalDrives.Add(new HealthLogicalDriveGen8("PHYSICAL ONLY", "", "", "", "", "", drive));
                        }


                    }


                }




                //read xml health summary
                GET_EMBEDDED_HEALTH_DATA_GEN10.GET_EMBEDDED_HEALTH_DATA HealthSum = new GET_EMBEDDED_HEALTH_DATA_GEN10.GET_EMBEDDED_HEALTH_DATA();

            switch (serverno)
            {
              
                case "S1":
                    HealthSum = GetHealthSummaryGen10(serverip, "S1");
                    break;
               

                }
                //OLD MAPPINGS


                //extract bios backup
                //string biosbakname = (string)serverinfo.SelectToken("Current.SystemRomBackup[0].Name");

                //extract ilo
                //string iloname = (string)serverinfo.SelectToken("Current.SystemBMC[0].Name");

                //extract inteligent provisioning
                //string intprovname = (string)serverinfo.SelectToken("Current.IntelligentProvisioning[0].Name");

                //SPS Firmware
                //string spsname = (string)serverinfo.SelectToken("Current.SPSFirmwareVersionData[0].Name");
                //FIRMWARE
                //decimal dec = HealthSum.FIRMWARE_INFORMATION.INDEX_11.FIRMWARE_VERSION.VALUE;
               // MessageBox.Show(HealthSum.FIRMWARE_INFORMATION.INDEX_11.FIRMWARE_VERSION.VALUE);
                //MessageBox.Show(HealthSum.FIRMWARE_INFORMATION.INDEX_11.FIRMWARE_NAME.VALUE);
           //gen10.InnovationEngine = HealthSum.FIRMWARE_INFORMATION.INDEX_11.FIRMWARE_VERSION.VALUE.ToString();
                //gen10.BiosCurrentVersion = HealthSum.FIRMWARE_INFORMATION.INDEX_2.FIRMWARE_VERSION.VALUE;
                //gen10.BiosBackupVersion = HealthSum.FIRMWARE_INFORMATION.INDEX_3.FIRMWARE_VERSION.VALUE;
                //gen10.iLOVersion = HealthSum.FIRMWARE_INFORMATION.INDEX_1.FIRMWARE_VERSION.VALUE;
                //gen10.IntelligentProvisioningVersion = HealthSum.FIRMWARE_INFORMATION.INDEX_10.FIRMWARE_VERSION.VALUE;
                //gen10.SPSVersion = HealthSum.FIRMWARE_INFORMATION.INDEX_8.FIRMWARE_VERSION.VALUE;

            if(HealthSum != null)
                {

       
            if(HealthSum.HEALTH_AT_A_GLANCE.BIOS_HARDWARE != null)
            gen10.BiosHardwareStatus = HealthSum.HEALTH_AT_A_GLANCE.BIOS_HARDWARE.STATUS;
            if (HealthSum.HEALTH_AT_A_GLANCE.PROCESSOR != null)
            gen10.ProcessorStatus = HealthSum.HEALTH_AT_A_GLANCE.PROCESSOR.STATUS;
            if (HealthSum.HEALTH_AT_A_GLANCE.MEMORY != null)
            gen10.MemoryStatus = HealthSum.HEALTH_AT_A_GLANCE.MEMORY.STATUS;
            if (HealthSum.HEALTH_AT_A_GLANCE.NETWORK != null)
            gen10.NetworkStatus = HealthSum.HEALTH_AT_A_GLANCE.NETWORK.STATUS;
            if (HealthSum.HEALTH_AT_A_GLANCE.STORAGE != null)
            gen10.StorageStatus = HealthSum.HEALTH_AT_A_GLANCE.STORAGE.STATUS;
            if (HealthSum.HEALTH_AT_A_GLANCE.TEMPERATURE != null)
            gen10.TemperatureStatus = HealthSum.HEALTH_AT_A_GLANCE.TEMPERATURE.STATUS;
            if (HealthSum.HEALTH_AT_A_GLANCE.FANS != null)
            gen10.FansStatus = HealthSum.HEALTH_AT_A_GLANCE.FANS.FirstOrDefault().STATUS.ToString();
                //gen10.FansRedundancy = HealthSum.HEALTH_AT_A_GLANCE.FANS.FirstOrDefault().REDUNDANCY.ToString();
            if (HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES != null)
            gen10.PowerSuppliesStatus = HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES.FirstOrDefault().STATUS.ToString();
                //gen10.PowerSupplyRedundancy = HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES.FirstOrDefault().REDUNDANCY.ToString();
                //FANS Collection
            if (HealthSum.HEALTH_AT_A_GLANCE.FANS != null)
            foreach (var itm in HealthSum.HEALTH_AT_A_GLANCE.FANS)
            {
                //MessageBox.Show(itm.STATUS);

                gen10.FansRedundancy = itm.REDUNDANCY;
            }

                //POWER SUPPLIES Collection
            if (HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES != null)
            foreach (var itm in HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES)
            {

                //gen10.PowerSuppliesStatus = itm.STATUS.FirstOrDefault().ToString(); 
                gen10.PowerSupplyRedundancy = itm.REDUNDANCY;
            }


            //Read rest of Data in XML file
            //FANS
            gen10.FanInfo = new System.Collections.ObjectModel.ObservableCollection<HealthFansGen10>();

            if (HealthSum.FANS != null)
            foreach (var itm in HealthSum.FANS)
            {
                gen10.FanInfo.Add(new HealthFansGen10(itm.LABEL.VALUE, itm.ZONE.VALUE, itm.STATUS.VALUE, itm.SPEED.VALUE + "%"));
            }
            //TEMPERATURES
            gen10.TemperatureInfo = new System.Collections.ObjectModel.ObservableCollection<HealthTemperatureGen10>();

            if (HealthSum.TEMPERATURE != null)
            foreach (var itm in HealthSum.TEMPERATURE)
            {

                gen10.TemperatureInfo.Add(new HealthTemperatureGen10(itm.LABEL.VALUE, itm.LOCATION.VALUE, itm.STATUS.VALUE, itm.CURRENTREADING.VALUE + " " + itm.CURRENTREADING.UNIT, itm.CAUTION.VALUE + " " + itm.CURRENTREADING.UNIT, itm.CRITICAL.VALUE + " " + itm.CURRENTREADING.UNIT));
            }

            //CPU
            gen10.CPUInfo = new System.Collections.ObjectModel.ObservableCollection<HealthCPUGen10>();

            if (HealthSum.PROCESSORS != null)
            foreach (var itm in HealthSum.PROCESSORS)
            {
                gen10.CPUInfo.Add(new HealthCPUGen10(itm.LABEL.VALUE, itm.NAME.VALUE, itm.STATUS.VALUE, itm.SPEED.VALUE, itm.EXECUTION_TECHNOLOGY.VALUE, itm.MEMORY_TECHNOLOGY.VALUE, itm.INTERNAL_L1_CACHE.VALUE, itm.INTERNAL_L2_CACHE.VALUE, itm.INTERNAL_L3_CACHE.VALUE));
            }

            //MEMORY
            gen10.MemoryInfo = new System.Collections.ObjectModel.ObservableCollection<HealthMemoryGen10>();
                //HealthSum.MEMORY.ADVANCED_MEMORY_PROTECTION
           if (HealthSum.MEMORY.MEMORY_DETAILS != null)
            {
                        if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_1 != null)
                        {
                            foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_1)
                            {
                                if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_1 != null)
                                    gen10.MemoryInfo.Add(new HealthMemoryGen10("CPU1", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE, itm.PART.NUMBER, itm.TYPE.VALUE, itm.SIZE.VALUE, itm.FREQUENCY.VALUE, itm.MINIMUM_VOLTAGE.VALUE, itm.RANKS.VALUE, itm.TECHNOLOGY.VALUE));

                            }
                        }
                       


                        if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_2 != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_2 != null)
                            foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_2)
                            {

                                gen10.MemoryInfo.Add(new HealthMemoryGen10("CPU2", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE, itm.PART.NUMBER, itm.TYPE.VALUE, itm.SIZE.VALUE, itm.FREQUENCY.VALUE, itm.MINIMUM_VOLTAGE.VALUE, itm.RANKS.VALUE, itm.TECHNOLOGY.VALUE));

                            }
                        }
                           


                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_3 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_3)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_3 != null)
                                    {
                                        gen10.MemoryInfo.Add(new HealthMemoryGen10("CPU3", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }


                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_4 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_4)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_4 != null)
                                    {
                                        gen10.MemoryInfo.Add(new HealthMemoryGen10("CPU4", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }




                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_1 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_1)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_1 != null)
                                    {
                                        gen10.MemoryInfo.Add(new HealthMemoryGen10("Memory_Board_1", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }


                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_2 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_2)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_2 != null)
                                    {
                                        gen10.MemoryInfo.Add(new HealthMemoryGen10("Memory_Board_2", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_3 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_3)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_3 != null)
                                    {
                                        gen10.MemoryInfo.Add(new HealthMemoryGen10("Memory_Board_3", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_4 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_4)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_4 != null)
                                    {
                                        gen10.MemoryInfo.Add(new HealthMemoryGen10("Memory_Board_4", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_5 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_5)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_5 != null)
                                    {
                                        gen10.MemoryInfo.Add(new HealthMemoryGen10("Memory_Board_5", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_6 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_6)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_6 != null)
                                    {
                                        gen10.MemoryInfo.Add(new HealthMemoryGen10("Memory_Board_6", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_7 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_7)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_7 != null)
                                    {
                                        gen10.MemoryInfo.Add(new HealthMemoryGen10("Memory_Board_7", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }

                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_8 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_8)
                                {
                                    if (HealthSum.MEMORY.MEMORY_DETAILS.Memory_Board_8 != null)
                                    {
                                        gen10.MemoryInfo.Add(new HealthMemoryGen10("Memory_Board_8", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE.ToString(), itm.PART.NUMBER.ToString(), itm.TYPE.VALUE.ToString(), itm.SIZE.VALUE.ToString(), itm.FREQUENCY.VALUE.ToString(), itm.MINIMUM_VOLTAGE.VALUE.ToString(), itm.RANKS.VALUE.ToString(), itm.TECHNOLOGY.VALUE.ToString()));
                                    }
                                }
                        }


                    }

                    //Count Populated Memory
                    int populatedCount = 0;
                    foreach (var itm in gen10.MemoryInfo)
                    {

                        if (itm.Status != "Not Present")
                        {
                            //Increase by 1
                            populatedCount += 1;
                        }

                    }
                    // Assign to property 
                    gen10.PopulatedSlots = populatedCount.ToString();




            //NETWORK 
            gen10.NICInfo = new System.Collections.ObjectModel.ObservableCollection<HealthNetworkGen10>();

            if (HealthSum.NIC_INFORMATION.NIC != null)
            {

            // Add ILO Network port
            gen10.NICInfo.Add(new HealthNetworkGen10(HealthSum.NIC_INFORMATION.iLO.NETWORK_PORT.VALUE, HealthSum.NIC_INFORMATION.iLO.PORT_DESCRIPTION.VALUE, HealthSum.NIC_INFORMATION.iLO.LOCATION.VALUE, HealthSum.NIC_INFORMATION.iLO.IP_ADDRESS.VALUE, HealthSum.NIC_INFORMATION.iLO.MAC_ADDRESS.VALUE, HealthSum.NIC_INFORMATION.iLO.STATUS.VALUE));

            //Assign to ILOPORT Status for test report
            gen10.ILONetworkPort = gen10.NICInfo.FirstOrDefault().Status;


               foreach (var itm in HealthSum.NIC_INFORMATION.NIC)
                {
                            if (itm != null)
                            if (itm.NETWORK_PORT != null || itm.PORT_DESCRIPTION != null || itm.LOCATION != null || itm.IP_ADDRESS != null || itm.MAC_ADDRESS != null || itm.STATUS != null)
                            {
                               
                                gen10.NICInfo.Add(new HealthNetworkGen10(itm.NETWORK_PORT.VALUE, itm.PORT_DESCRIPTION.VALUE, itm.LOCATION.VALUE, itm.IP_ADDRESS.VALUE, itm.MAC_ADDRESS.VALUE, itm.STATUS.VALUE));
                            }
                  }

            }

         

            //POWER
            gen10.PowerInfo = new System.Collections.ObjectModel.ObservableCollection<HealthPowerGen10>();

            //HealthSum.
            if (HealthSum.POWER_SUPPLIES.SUPPLY != null)
            foreach (var itm in HealthSum.POWER_SUPPLIES.SUPPLY)
            {
                            if (itm.PRESENT != null)
                            if (itm.PRESENT != null || itm.STATUS != null || itm.MODEL != null || itm.SPARE != null || itm.SERIAL_NUMBER != null || itm.CAPACITY != null || itm.FIRMWARE_VERSION != null)
                            {
                                gen10.PowerInfo.Add(new HealthPowerGen10(itm.LABEL.VALUE, itm.PRESENT.VALUE, itm.STATUS.VALUE, itm.MODEL.VALUE, itm.SPARE.VALUE, itm.SERIAL_NUMBER.VALUE, itm.SPARE.VALUE, itm.SERIAL_NUMBER.VALUE, itm.CAPACITY.VALUE, itm.FIRMWARE_VERSION.VALUE.ToString()));
                            }
                        }

                
                if (HealthSum.STORAGE.CONTROLLER != null)
                {

                    //STORAGE
                    HealthStorageGen10 controllerinfo = new HealthStorageGen10();
                    if (HealthSum.STORAGE.CONTROLLER.LABEL.VALUE != null)
                        controllerinfo.Label = HealthSum.STORAGE.CONTROLLER.LABEL.VALUE;
                    if (HealthSum.STORAGE.CONTROLLER.STATUS.VALUE != null)
                        controllerinfo.Status = HealthSum.STORAGE.CONTROLLER.STATUS.VALUE;
                    if (HealthSum.STORAGE.CONTROLLER.CONTROLLER_STATUS.VALUE != null)
                        controllerinfo.ControllerStatus = HealthSum.STORAGE.CONTROLLER.CONTROLLER_STATUS.VALUE;
                    if (HealthSum.STORAGE.CONTROLLER.SERIAL_NUMBER.VALUE != null)
                        controllerinfo.Serial = HealthSum.STORAGE.CONTROLLER.SERIAL_NUMBER.VALUE;
                    if (HealthSum.STORAGE.CONTROLLER.MODEL.VALUE != null)
                        controllerinfo.Model = HealthSum.STORAGE.CONTROLLER.MODEL.VALUE;
                    if (HealthSum.STORAGE.CONTROLLER.FW_VERSION.VALUE.ToString() != null)
                        controllerinfo.Firmware = HealthSum.STORAGE.CONTROLLER.FW_VERSION.VALUE.ToString();
                    //MessageBox.Show(controllerinfo.Model);
                    if (HealthSum.STORAGE.CONTROLLER.DRIVE_ENCLOSURE != null)
                        foreach (var itm in HealthSum.STORAGE.CONTROLLER.DRIVE_ENCLOSURE)
                        {
                            controllerinfo.DriveEnclosure.Add(new DriveEnclosureInfoGen10(itm.LABEL.VALUE, itm.STATUS.VALUE, itm.DRIVE_BAY.VALUE.ToString()));
                            //MessageBox.Show(itm.LABEL.VALUE + "  " +);
                        }



                    if (HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE != null)
                        foreach (var itm in HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE)
                        {
                            controllerinfo.LogicalDrives.Add(new HealthLogicalDriveGen10(itm.LABEL.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.CAPACITY.VALUE.ToString(), itm.FAULT_TOLERANCE.VALUE.ToString(), itm.LOGICAL_DRIVE_TYPE.VALUE.ToString(), itm.ENCRYPTION_STATUS.VALUE.ToString(), itm.PHYSICAL_DRIVE));
                            //MessageBox.Show(itm.FW_VERSION.VALUE.ToString());

                            //foreach (var itm2 in itm.PHYSICAL_DRIVE)
                            //{
                            //    controllerinfo.LogicalDrives...Add(new PhysicalDrivesGen8(itm.LABEL.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.SERIAL_NUMBER.VALUE.ToString(), itm.MODEL.VALUE.ToString(), itm.CAPACITY.VALUE.ToString(), itm.MARKETING_CAPACITY.VALUE.ToString(), itm.LOCATION.VALUE.ToString(), itm.FW_VERSION.VALUE.ToString(), itm.DRIVE_CONFIGURATION.VALUE.ToString(), itm.ENCRYPTION_STATUS.VALUE.ToString(), itm.MEDIA_TYPE.VALUE.ToString()));
                            //    //MessageBox.Show(itm.FW_VERSION.VALUE.ToString());
                            //}

                            // itm.PHYSICAL_DRIVE.
                        }
                 

                    gen10.StorageInfo = controllerinfo;
                        //STORAGE
                    }
                }





                return gen10;

            }
            catch (Exception ex)
            {

                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                return null;
            }
        }


        public Gen10Info ReadFullServerInfoJsonBGen10(string jsonserverinfo, string jsonfirmwareinfo, string serverip, string serverno)
        {

            try
            {


                //declare json object
                //JObject serverinfo = JObject.Parse(jsonserverinfo);

                //Json ALL Info
                //array to object
                JArray arrayinfo = JArray.Parse(jsonserverinfo);
                //MessageBox.Show(arrayinfo.Count.ToString()); 
                JObject comments = JObject.Parse(arrayinfo[0].ToString());
                JObject hpbios = JObject.Parse(arrayinfo[1].ToString());
                JObject chassisenclosure = JObject.Parse(arrayinfo[2].ToString());
                JObject chassis = JObject.Parse(arrayinfo[3].ToString());
                JObject computersystem = JObject.Parse(arrayinfo[4].ToString());
                JObject manager = JObject.Parse(arrayinfo[5].ToString());


                //Firmware Info
                if (jsonfirmwareinfo != null)
                {

                }

                JObject firmwareinfo = JObject.Parse(jsonfirmwareinfo);

                Gen10Info gen10 = new Gen10Info();

                //SDCARD Detection
                JObject SDCardinfo;
                if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "SDCARD.json"))
                {
                    //check for presence of sd card
                    using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "SDCARD.json"))
                    {
                        //STRIP OUT THE INCORRECT HEADERS THAT STOP JSON.NET READING CORRECTLY
                        SDCardinfo = JObject.Parse(r.ReadToEnd());
                    }


                    gen10.SDCardInserted = (string)SDCardinfo.SelectToken("SDCard.Status.State");

                }
                //string bioscurname = (string)hpbios.SelectToken("HpBios.restv1Systems1Bios.PowerRegulator");
                //string bioscurname = (string)hpbios.SelectToken("HpBios.restv1Systems1Bios.PowerRegulator");

                // MessageBox.Show(bioscurname);

                //FIRMWARE
                //FIRMWARE
                //extract bios current
                // string bioscurname = (string)serverinfo.SelectToken("Current.SystemRomActive[0].Name");
                gen10.BiosCurrentVersion = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.Bios.Current.VersionString");
                //extract bios backup
                //string biosbakname = (string)serverinfo.SelectToken("Current.SystemRomBackup[0].Name");
                gen10.BiosBackupVersion = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.Bios.Backup.VersionString");
                //extract ilo
                //MessageBox.Show((string)manager.SelectToken("Manager.Path.Oem.Hpe.Firmware.Current.VersionString"));
                //string iloname = (string)serverinfo.SelectToken("Current.SystemBMC[0].Name");
                gen10.iLOVersion = (string)manager.SelectToken("Manager.Path.Oem.Hpe.Firmware.Current.VersionString");
                //extract inteligent provisioning
                //string intprovname = (string)serverinfo.SelectToken("Current.IntelligentProvisioning[0].Name");
                gen10.IntelligentProvisioningVersion = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.IntelligentProvisioning.VersionString");
                //SPS Firmware
                //string spsname = (string)serverinfo.SelectToken("Current.SPSFirmwareVersionData[0].Name");
                //gen10.SPSVersion = (string)chassis.SelectToken("Chassis.Path.Oem.Hpe.Firmware.SPSFirmwareVersionData.Current.VersionString");
                // string bioscurname = (string)serverinfo.SelectToken("Current.SystemRomActive[0].Name");


                //Alternative to get all firmware from a Gen10
                JsonSerializer serializer = new JsonSerializer();

                Gen10FirmwareJson gen10fw = new Gen10FirmwareJson();
                gen10fw = (Gen10FirmwareJson)serializer.Deserialize(new JTokenReader(firmwareinfo), typeof(Gen10FirmwareJson));

                ///To Access Collections From Restful api returned Json
                ///1) Use rawget if not accessible via a main keyword, see API Reference for Types
                ///2) Use the redfish data uri path to the information you require
                ///3) Once file is exported, copy and paste special inside a new class under models
                ///4) use JsonSerializer to deserialize the information into an instance of the class you just created
                ///5) Create a foreach loop and loop through the collection pulling out the information you require

                foreach (var itm in gen10fw.Members)
                {
                    // MessageBox.Show(itm.Name);
                    //switch (itm.Name)
                    //{
                    //    case itm.Name.Contains(""):
                    //        break;
                    //}


                    if (itm.Name.Contains("iLO"))
                    {
                        gen10.iLOVersion = itm.Version;
                    }
                    if (itm.Name.Contains("Innovation Engine"))
                    {
                        gen10.InnovationEngine = itm.Version;
                    }

                    if (itm.Name.Contains("Intelligent Provisioning"))
                    {
                        gen10.IntelligentProvisioningVersion = itm.Version;
                    }

                    if (itm.Name.Contains("Server Platform Services (SPS) Firmware"))
                    {
                        gen10.SPSVersion = itm.Version;
                    }

                    //    MessageBox.Show(itm.SelectToken("Members.Name").ToString());firmwareinfo.SelectTokens("Members")
                    //   itm.SingleOrDefault
                    //   // if(itm.SelectToken)
                    //    //fwlst.Add(itm);
                }







                //SERVER INFO
                //to get firmware from gen10
                //rawget /redfish/v1/updateservice/firmwareinventory/ --expand -f firmware.json

                //extract bios current
                //MessageBox.Show((string)serverinfo.SelectToken("Manufacturer"));
                gen10.Manufacturer = (string)computersystem.SelectToken("ComputerSystem.Path.Manufacturer");
                gen10.Model = (string)computersystem.SelectToken("ComputerSystem.Path.Model");
                gen10.AssetTag = (string)computersystem.SelectToken("ComputerSystem.Path.AssetTag");

                string hn = (string)computersystem.SelectToken("ComputerSystem.Path.HostName");
                if (hn != null)
                    gen10.HostName = hn.Trim();
                string cs = (string)computersystem.SelectToken("ComputerSystem.Path.SerialNumber");
                if (cs != null)
                    gen10.ChassisSerial = cs.Trim();
                //extract ilo
                gen10.HealthStatus = (string)computersystem.SelectToken("ComputerSystem.Path.Status.Health");
                string sku = (string)computersystem.SelectToken("ComputerSystem.Path.SKU");
                if (sku != null)
                    gen10.SKU = sku.Trim();
                gen10.TotalMemory = (string)computersystem.SelectToken("ComputerSystem.Path.MemorySummary.TotalSystemMemoryGiB");
                gen10.MemoryHealth = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.AggregateHealthStatus.Memory.Status.Health");
                //extract inteligent provisioning
                //string model = (string)firmwareLevels.SelectToken("Model");
                gen10.IntelligentProvisioningLocation = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.IntelligentProvisioningLocation");
                //string intprovversion = (string)firmwareLevels.SelectToken("Oem.Hp.IntelligentProvisioningVersion");
                gen10.PostState = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.PostState");

                gen10.CPUFamily = (string)computersystem.SelectToken("ComputerSystem.Path.ProcessorSummary.Model");
                if (gen10.CPUFamily != null)
                    gen10.CPUFamily = gen10.CPUFamily.Trim();
                gen10.CPUCount = (string)computersystem.SelectToken("ComputerSystem.Path.ProcessorSummary.Count");
                gen10.CPUHealth = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hpe.AggregateHealthStatus.Processors.Status.Health");
                //power
                gen10.PowerState = (string)computersystem.SelectToken("PowerState");
                gen10.PowerAutoOn = (string)computersystem.SelectToken("Oem.Hpe.PowerAutoOn");
                gen10.PowerOnDelay = (string)computersystem.SelectToken("Oem.Hpe.PowerOnDelay");
                gen10.PowerAutoOn = (string)computersystem.SelectToken("Oem.Hpe.PowerAutoOn");
                gen10.PowerRegulatorMode = (string)computersystem.SelectToken("Oem.Hpe.PowerRegulatorMode");
                gen10.PowerAllocationLimit = (string)computersystem.SelectToken("Oem.Hpe.PowerAllocationLimit");

                //iLO Self Test
                gen10.NVRAMDataNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[0].Notes");
                gen10.NVRAMDataStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[0].Status");
                gen10.NVRAMSpaceNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[1].Notes");
                gen10.NVRAMSpaceStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[1].Status");
                gen10.EmbeddedFlashSDCardNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[2].Notes");
                gen10.EmbeddedFlashSDCardStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[2].Status");
                gen10.EEPROMNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[3].Notes");
                gen10.EEPROMStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[3].Status");
                gen10.HostRomNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[4].Notes");
                gen10.HostRomStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[4].Status");
                gen10.SupportedHostNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[5].Notes");
                gen10.SupportedHostStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[5].Status");
                gen10.PowerManagementControllerNotes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[6].Notes");
                gen10.PowerManagementControllerStatus = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[6].Status");
                gen10.CPLDPAL0Notes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[7].Notes");
                gen10.CPLDPAL0Status = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[7].Status");
                gen10.CPLDPAL1Notes = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[8].Notes");
                gen10.CPLDPAL1Status = (string)manager.SelectToken("Manager.Path.Oem.Hpe.iLOSelfTestResults[8].Status");

                //READ SEPARATE XML FILE


                //ILO License   public string ILOLicense { get; set; }
                gen10.ILOLicense = (string)manager.SelectToken("Manager.Path.Oem.Hpe.License.LicenseString");


                //read xml health summary
                GET_EMBEDDED_HEALTH_DATA_GEN10.GET_EMBEDDED_HEALTH_DATA HealthSum = new GET_EMBEDDED_HEALTH_DATA_GEN10.GET_EMBEDDED_HEALTH_DATA();

                switch (serverno)
                {
                     case "S1":
                        HealthSum = GetHealthSummaryGen10(serverip, "S1");
                        break;
                   
                }



                //PHYSICAL DISKS
                JArray diskarrayinfo = JArray.Parse(jsonserverinfo);


                //check if JSON File is there
                if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "Disks.json"))
                {
                    //array to object

                    //JObject o1 = JObject.Parse(File.ReadAllText(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "Disks.json"));

                    //foreach(var)


                    ////check for presence of sd card
                    using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "Disks.json"))
                    {
                        ////Read from File
                        //// string streamString = r.ReadToEnd();

                        string streamString = "";

                        string HeaderString = "";
                        string PathString = "";

                        // Use while not null pattern in while loop.
                        string line;
                        while ((line = r.ReadLine()) != null)
                        {
                            // Insert logic here.
                            // ... The "line" variable is a line in the file.
                            // ... Add it to our List.



                            if (line.Contains("@odata") || line.Contains("href"))
                            {

                            }
                            else
                            {

                                //Gen8,9
                                if (line.Contains("HpSmartStorageDiskDrive"))
                                {
                                    HeaderString = "HpSmartStorageDiskDrive" + Regex.Match(line, @"HpSmartStorageDiskDrive(.+?):").Groups[1].Value.Replace("\"", "");
                                    line = line.Replace(HeaderString, "Disk");
                                    //  MessageBox.Show(HeaderString);
                                }
                                //Gen10
                                if (line.Contains("#HpeSmartStorageDiskDrive."))
                                {
                                    HeaderString = "#HpeSmartStorageDiskDrive." + Regex.Match(line, @"#HpeSmartStorageDiskDrive.(.+?):").Groups[1].Value.Replace("\"", "");
                                    line = line.Replace(HeaderString, "Disk");
                                    //  MessageBox.Show(HeaderString);
                                }


                            }


                            if (line.Contains("@odata") || line.Contains("href"))
                            {

                            }
                            else
                            {

                                //gen8,9 
                                if (line.Contains("/rest/v1/Systems/1/SmartStorage/ArrayControllers/"))
                                {
                                    // MessageBox.Show(PathString);

                                    PathString = "/rest/v1/Systems/1/SmartStorage/ArrayControllers/" + Regex.Match(line, @"/rest/v1/Systems/1/SmartStorage/ArrayControllers/(.+?):").Groups[1].Value.Replace("\"", "");
                                    line = line.Replace(PathString, "Path");

                                }

                                //gen10

                                if (line.Contains("/redfish/v1/Systems/1/SmartStorage/ArrayControllers/"))
                                {
                                    // MessageBox.Show(PathString);

                                    PathString = "/redfish/v1/Systems/1/SmartStorage/ArrayControllers/" + Regex.Match(line, @"/redfish/v1/Systems/1/SmartStorage/ArrayControllers/(.+?):").Groups[1].Value.Replace("\"", "");
                                    line = line.Replace(PathString, "Path");

                                }

                            }

                            streamString += line;


                        }


                        diskarrayinfo = JArray.Parse(streamString);


                    }

                    //Create holding array
                    foreach (JObject disk in diskarrayinfo)
                    {
                        ////MessageBox.Show(disk.SelectTokens)
                        //MessageBox.Show((string)disk.SelectToken("Disk.Path.Model"));
                        //MessageBox.Show((string)disk.SelectToken("Disk.Path.Status.Health"));
                        //string[] strarr = new string[2];

                        if ((string)disk.SelectToken("Disk.Path.Model") != null)
                        {

                            //MessageBox.Show((string)disk.SelectToken("Disk.Path.Model"));
                            HPPhysicalDrivesModel drive = new HPPhysicalDrivesModel();

                            drive.Model = (string)disk.SelectToken("Disk.Path.Model");
                            drive.Capacity = (string)disk.SelectToken("Disk.Path.CapacityGB");
                            if ((bool)disk.SelectToken("Disk.Path.EncryptedDrive") == false)
                            {
                                drive.Encryption = "No";
                            }
                            else
                            {
                                drive.Encryption = "Yes";
                            }
                            drive.SerialNumber = (string)disk.SelectToken("Disk.Path.SerialNumber");
                            drive.MediaType = (string)disk.SelectToken("Disk.Path.MediaType");
                            drive.Location = (string)disk.SelectToken("Disk.Path.Location");
                            drive.Interface = (string)disk.SelectToken("Disk.Path.InterfaceType");
                            drive.Health = (string)disk.SelectToken("Disk.Path.Status.Health");
                            gen10.PhysicalDriveInfo.Add(drive);
                            //gen8.StorageInfo.LogicalDrives.Add(new HealthLogicalDriveGen8("PHYSICAL ONLY", "", "", "", "", "", drive));
                        }


                    }


                }



                //OLD MAPPINGS


                //extract bios backup
                //string biosbakname = (string)serverinfo.SelectToken("Current.SystemRomBackup[0].Name");

                //extract ilo
                //string iloname = (string)serverinfo.SelectToken("Current.SystemBMC[0].Name");

                //extract inteligent provisioning
                //string intprovname = (string)serverinfo.SelectToken("Current.IntelligentProvisioning[0].Name");

                //SPS Firmware
                //string spsname = (string)serverinfo.SelectToken("Current.SPSFirmwareVersionData[0].Name");
                //FIRMWARE
                //decimal dec = HealthSum.FIRMWARE_INFORMATION.INDEX_11.FIRMWARE_VERSION.VALUE;
                // MessageBox.Show(HealthSum.FIRMWARE_INFORMATION.INDEX_11.FIRMWARE_VERSION.VALUE);
                //MessageBox.Show(HealthSum.FIRMWARE_INFORMATION.INDEX_11.FIRMWARE_NAME.VALUE);
                //gen10.InnovationEngine = HealthSum.FIRMWARE_INFORMATION.INDEX_11.FIRMWARE_VERSION.VALUE.ToString();
                //gen10.BiosCurrentVersion = HealthSum.FIRMWARE_INFORMATION.INDEX_2.FIRMWARE_VERSION.VALUE;
                //gen10.BiosBackupVersion = HealthSum.FIRMWARE_INFORMATION.INDEX_3.FIRMWARE_VERSION.VALUE;
                //gen10.iLOVersion = HealthSum.FIRMWARE_INFORMATION.INDEX_1.FIRMWARE_VERSION.VALUE;
                //gen10.IntelligentProvisioningVersion = HealthSum.FIRMWARE_INFORMATION.INDEX_10.FIRMWARE_VERSION.VALUE;
                //gen10.SPSVersion = HealthSum.FIRMWARE_INFORMATION.INDEX_8.FIRMWARE_VERSION.VALUE;

                if (HealthSum != null)
                {


                    if (HealthSum.HEALTH_AT_A_GLANCE.BIOS_HARDWARE != null)
                        gen10.BiosHardwareStatus = HealthSum.HEALTH_AT_A_GLANCE.BIOS_HARDWARE.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.PROCESSOR != null)
                        gen10.ProcessorStatus = HealthSum.HEALTH_AT_A_GLANCE.PROCESSOR.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.MEMORY != null)
                        gen10.MemoryStatus = HealthSum.HEALTH_AT_A_GLANCE.MEMORY.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.NETWORK != null)
                        gen10.NetworkStatus = HealthSum.HEALTH_AT_A_GLANCE.NETWORK.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.STORAGE != null)
                        gen10.StorageStatus = HealthSum.HEALTH_AT_A_GLANCE.STORAGE.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.TEMPERATURE != null)
                        gen10.TemperatureStatus = HealthSum.HEALTH_AT_A_GLANCE.TEMPERATURE.STATUS;
                    if (HealthSum.HEALTH_AT_A_GLANCE.FANS != null)
                        gen10.FansStatus = HealthSum.HEALTH_AT_A_GLANCE.FANS.FirstOrDefault().STATUS.ToString();
                    //gen10.FansRedundancy = HealthSum.HEALTH_AT_A_GLANCE.FANS.FirstOrDefault().REDUNDANCY.ToString();
                    if (HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES != null)
                        gen10.PowerSuppliesStatus = HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES.FirstOrDefault().STATUS.ToString();
                    //gen10.PowerSupplyRedundancy = HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES.FirstOrDefault().REDUNDANCY.ToString();
                    //FANS Collection
                    if (HealthSum.HEALTH_AT_A_GLANCE.FANS != null)
                        foreach (var itm in HealthSum.HEALTH_AT_A_GLANCE.FANS)
                        {
                            //MessageBox.Show(itm.STATUS);

                            gen10.FansRedundancy = itm.REDUNDANCY;
                        }

                    //POWER SUPPLIES Collection
                    if (HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES != null)
                        foreach (var itm in HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES)
                        {

                            //gen10.PowerSuppliesStatus = itm.STATUS.FirstOrDefault().ToString(); 
                            gen10.PowerSupplyRedundancy = itm.REDUNDANCY;
                        }


                    //Read rest of Data in XML file
                    //FANS
                    gen10.FanInfo = new System.Collections.ObjectModel.ObservableCollection<HealthFansGen10>();

                    if (HealthSum.FANS != null)
                        foreach (var itm in HealthSum.FANS)
                        {
                            gen10.FanInfo.Add(new HealthFansGen10(itm.LABEL.VALUE, itm.ZONE.VALUE, itm.STATUS.VALUE, itm.SPEED.VALUE + "%"));
                        }
                    //TEMPERATURES
                    gen10.TemperatureInfo = new System.Collections.ObjectModel.ObservableCollection<HealthTemperatureGen10>();

                    if (HealthSum.TEMPERATURE != null)
                        foreach (var itm in HealthSum.TEMPERATURE)
                        {

                            gen10.TemperatureInfo.Add(new HealthTemperatureGen10(itm.LABEL.VALUE, itm.LOCATION.VALUE, itm.STATUS.VALUE, itm.CURRENTREADING.VALUE + " " + itm.CURRENTREADING.UNIT, itm.CAUTION.VALUE + " " + itm.CURRENTREADING.UNIT, itm.CRITICAL.VALUE + " " + itm.CURRENTREADING.UNIT));
                        }

                    //CPU
                    gen10.CPUInfo = new System.Collections.ObjectModel.ObservableCollection<HealthCPUGen10>();

                    if (HealthSum.PROCESSORS != null)
                        foreach (var itm in HealthSum.PROCESSORS)
                        {
                            gen10.CPUInfo.Add(new HealthCPUGen10(itm.LABEL.VALUE, itm.NAME.VALUE, itm.STATUS.VALUE, itm.SPEED.VALUE, itm.EXECUTION_TECHNOLOGY.VALUE, itm.MEMORY_TECHNOLOGY.VALUE, itm.INTERNAL_L1_CACHE.VALUE, itm.INTERNAL_L2_CACHE.VALUE, itm.INTERNAL_L3_CACHE.VALUE));
                        }

                    //MEMORY
                    gen10.MemoryInfo = new System.Collections.ObjectModel.ObservableCollection<HealthMemoryGen10>();
                    //HealthSum.MEMORY.ADVANCED_MEMORY_PROTECTION
                    if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_1)
                        {

                            gen10.MemoryInfo.Add(new HealthMemoryGen10("CPU1", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE, itm.PART.NUMBER, itm.TYPE.VALUE, itm.SIZE.VALUE, itm.FREQUENCY.VALUE, itm.MINIMUM_VOLTAGE.VALUE, itm.RANKS.VALUE, itm.TECHNOLOGY.VALUE));

                        }


                    if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_2)
                        {

                            gen10.MemoryInfo.Add(new HealthMemoryGen10("CPU2", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE, itm.PART.NUMBER, itm.TYPE.VALUE, itm.SIZE.VALUE, itm.FREQUENCY.VALUE, itm.MINIMUM_VOLTAGE.VALUE, itm.RANKS.VALUE, itm.TECHNOLOGY.VALUE));

                        }



                    //Count Populated Memory
                    int populatedCount = 0;
                    foreach (var itm in gen10.MemoryInfo)
                    {

                        if (itm.Status != "Not Present")
                        {
                            //Increase by 1
                            populatedCount += 1;
                        }

                    }
                    // Assign to property
                    gen10.PopulatedSlots = populatedCount.ToString();




                    //NETWORK 
                    gen10.NICInfo = new System.Collections.ObjectModel.ObservableCollection<HealthNetworkGen10>();

                    if (HealthSum.NIC_INFORMATION.NIC != null)
                    {

                        // Add ILO Network port
                        gen10.NICInfo.Add(new HealthNetworkGen10(HealthSum.NIC_INFORMATION.iLO.NETWORK_PORT.VALUE, HealthSum.NIC_INFORMATION.iLO.PORT_DESCRIPTION.VALUE, HealthSum.NIC_INFORMATION.iLO.LOCATION.VALUE, HealthSum.NIC_INFORMATION.iLO.IP_ADDRESS.VALUE, HealthSum.NIC_INFORMATION.iLO.MAC_ADDRESS.VALUE, HealthSum.NIC_INFORMATION.iLO.STATUS.VALUE));

                        //Assign to ILOPORT Status for test report
                        gen10.ILONetworkPort = gen10.NICInfo.FirstOrDefault().Status;


                        foreach (var itm in HealthSum.NIC_INFORMATION.NIC)
                        {
                            if (itm != null)
                                if (itm.NETWORK_PORT != null || itm.PORT_DESCRIPTION != null || itm.LOCATION != null || itm.IP_ADDRESS != null || itm.MAC_ADDRESS != null || itm.STATUS != null)
                                {

                                    gen10.NICInfo.Add(new HealthNetworkGen10(itm.NETWORK_PORT.VALUE, itm.PORT_DESCRIPTION.VALUE, itm.LOCATION.VALUE, itm.IP_ADDRESS.VALUE, itm.MAC_ADDRESS.VALUE, itm.STATUS.VALUE));
                                }
                        }

                    }



                    //POWER
                    gen10.PowerInfo = new System.Collections.ObjectModel.ObservableCollection<HealthPowerGen10>();

                    //HealthSum.
                    if (HealthSum.POWER_SUPPLIES.SUPPLY != null)
                        foreach (var itm in HealthSum.POWER_SUPPLIES.SUPPLY)
                        {
                            if (itm.PRESENT != null)
                            if (itm.PRESENT != null || itm.STATUS != null || itm.MODEL != null || itm.SPARE != null || itm.SERIAL_NUMBER != null || itm.CAPACITY != null || itm.FIRMWARE_VERSION != null)
                            {
                                gen10.PowerInfo.Add(new HealthPowerGen10(itm.LABEL.VALUE, itm.PRESENT.VALUE, itm.STATUS.VALUE, itm.MODEL.VALUE, itm.SPARE.VALUE, itm.SERIAL_NUMBER.VALUE, itm.SPARE.VALUE, itm.SERIAL_NUMBER.VALUE, itm.CAPACITY.VALUE, itm.FIRMWARE_VERSION.VALUE.ToString()));
                            }
                        }


                    if (HealthSum.STORAGE.CONTROLLER != null)
                    {

                        //STORAGE
                        HealthStorageGen10 controllerinfo = new HealthStorageGen10();
                        if (HealthSum.STORAGE.CONTROLLER.LABEL.VALUE != null)
                            controllerinfo.Label = HealthSum.STORAGE.CONTROLLER.LABEL.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.STATUS.VALUE != null)
                            controllerinfo.Status = HealthSum.STORAGE.CONTROLLER.STATUS.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.CONTROLLER_STATUS.VALUE != null)
                            controllerinfo.ControllerStatus = HealthSum.STORAGE.CONTROLLER.CONTROLLER_STATUS.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.SERIAL_NUMBER.VALUE != null)
                            controllerinfo.Serial = HealthSum.STORAGE.CONTROLLER.SERIAL_NUMBER.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.MODEL.VALUE != null)
                            controllerinfo.Model = HealthSum.STORAGE.CONTROLLER.MODEL.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.FW_VERSION.VALUE.ToString() != null)
                            controllerinfo.Firmware = HealthSum.STORAGE.CONTROLLER.FW_VERSION.VALUE.ToString();
                            //MessageBox.Show(controllerinfo.Model);
                        if (HealthSum.STORAGE.CONTROLLER.DRIVE_ENCLOSURE != null)
                            foreach (var itm in HealthSum.STORAGE.CONTROLLER.DRIVE_ENCLOSURE)
                            {
                                controllerinfo.DriveEnclosure.Add(new DriveEnclosureInfoGen10(itm.LABEL.VALUE, itm.STATUS.VALUE, itm.DRIVE_BAY.VALUE.ToString()));
                                //MessageBox.Show(itm.LABEL.VALUE + "  " +);
                            }



                        if (HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE != null)
                            foreach (var itm in HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE)
                            {
                                controllerinfo.LogicalDrives.Add(new HealthLogicalDriveGen10(itm.LABEL.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.CAPACITY.VALUE.ToString(), itm.FAULT_TOLERANCE.VALUE.ToString(), itm.LOGICAL_DRIVE_TYPE.VALUE.ToString(), itm.ENCRYPTION_STATUS.VALUE.ToString(), itm.PHYSICAL_DRIVE));
                                //MessageBox.Show(itm.FW_VERSION.VALUE.ToString());

                                //foreach (var itm2 in itm.PHYSICAL_DRIVE)
                                //{
                                //    controllerinfo.LogicalDrives...Add(new PhysicalDrivesGen8(itm.LABEL.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.SERIAL_NUMBER.VALUE.ToString(), itm.MODEL.VALUE.ToString(), itm.CAPACITY.VALUE.ToString(), itm.MARKETING_CAPACITY.VALUE.ToString(), itm.LOCATION.VALUE.ToString(), itm.FW_VERSION.VALUE.ToString(), itm.DRIVE_CONFIGURATION.VALUE.ToString(), itm.ENCRYPTION_STATUS.VALUE.ToString(), itm.MEDIA_TYPE.VALUE.ToString()));
                                //    //MessageBox.Show(itm.FW_VERSION.VALUE.ToString());
                                //}

                                // itm.PHYSICAL_DRIVE.
                            }


                        gen10.StorageInfo = controllerinfo;
                        //STORAGE
                    }
                }





                return gen10;

            }
            catch (Exception ex)
            {

                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                return null;
            }
        }





        public GenGenericInfo ReadFullServerInfoJsonGenGeneric(string jsonserverinfo, string serverip, string serverno)
        {
            //declare json object
            //JObject serverinfo = JObject.Parse(jsonserverinfo);

            //array to object
            JArray arrayinfo = JArray.Parse(jsonserverinfo);
            //MessageBox.Show(arrayinfo.Count.ToString()); 
            //if(arrayinfo.Count >)
            JObject comments = JObject.Parse(arrayinfo[0].ToString());
            JObject hpbios = JObject.Parse(arrayinfo[1].ToString());
            JObject chassis = JObject.Parse(arrayinfo[2].ToString());
            //JObject firmware = JObject.Parse(arrayinfo[3].ToString());
            JObject computersystem = JObject.Parse(arrayinfo[3].ToString());
            JObject manager = JObject.Parse(arrayinfo[4].ToString());

            GenGenericInfo gen = new GenGenericInfo();
            //SDCARD Detection

            JObject SDCardinfo;
            //check if JSON File is there
            if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "SDCARD.json"))
            {
                //check for presence of sd card
                using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + serverip + "SDCARD.json"))
                {
                    //STRIP OUT THE INCORRECT HEADERS THAT STOP JSON.NET READING CORRECTLY
                    SDCardinfo = JObject.Parse(r.ReadToEnd());
                }


                gen.SDCardInserted = (string)SDCardinfo.SelectToken("SDCard.Status.State");
            }
            //string bioscurname = (string)hpbios.SelectToken("HpBios.restv1Systems1Bios.PowerRegulator");

            //MessageBox.Show(bioscurname);

            //FIRMWARE
            //FIRMWARE
            //extract bios current
            //// string bioscurname = (string)serverinfo.SelectToken("Current.SystemRomActive[0].Name");
            //gen.BiosCurrentVersion = (string)firmware.SelectToken("Firmware.Path.Current.SystemRomActive[0].VersionString");
            ////extract bios backup
            ////string biosbakname = (string)serverinfo.SelectToken("Current.SystemRomBackup[0].Name");
            //gen.BiosBackupVersion = (string)firmware.SelectToken("Firmware.Path.Current.SystemRomBackup[0].VersionString");
            ////extract ilo
            ////string iloname = (string)serverinfo.SelectToken("Current.SystemBMC[0].Name");
            //gen.iLOVersion = (string)firmware.SelectToken("Firmware.Path.Current.SystemBMC[0].VersionString");
            ////extract inteligent provisioning
            ////string intprovname = (string)serverinfo.SelectToken("Current.IntelligentProvisioning[0].Name");
            //gen.IntelligentProvisioningVersion = (string)firmware.SelectToken("Firmware.Path.Current.IntelligentProvisioning[0].VersionString");
            ////SPS Firmware
            ////string spsname = (string)serverinfo.SelectToken("Current.SPSFirmwareVersionData[0].Name");
            //gen.SPSVersion = (string)firmware.SelectToken("Firmware.Path.Current.SPSFirmwareVersionData[0].VersionString");






            //SERVER INFO

            //extract bios current
            //MessageBox.Show((string)serverinfo.SelectToken("Manufacturer"));
            gen.Manufacturer = (string)computersystem.SelectToken("ComputerSystem.Path.Manufacturer");
            gen.Model = (string)computersystem.SelectToken("ComputerSystem.Path.Model");
            gen.AssetTag = (string)computersystem.SelectToken("ComputerSystem.Path.AssetTag");

            string hn = (string)computersystem.SelectToken("ComputerSystem.Path.HostName");
            gen.HostName = hn;
            string cs = (string)computersystem.SelectToken("ComputerSystem.Path.SerialNumber");
            gen.ChassisSerial = cs;
            //extract ilo
            gen.HealthStatus = (string)computersystem.SelectToken("ComputerSystem.Path.Status.Health");
            string sku = (string)computersystem.SelectToken("ComputerSystem.Path.SKU");
            gen.SKU = sku;
            gen.TotalMemory = (string)computersystem.SelectToken("ComputerSystem.Path.Memory.TotalSystemMemoryGB");
            gen.MemoryHealth = (string)computersystem.SelectToken("ComputerSystem.Path.Memory.Status.HealthRollUp");
            //extract inteligent provisioning
            //string model = (string)firmwareLevels.SelectToken("Model");
            gen.IntelligentProvisioningLocation = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hp.IntelligentProvisioningLocation");
            //string intprovversion = (string)firmwareLevels.SelectToken("Oem.Hp.IntelligentProvisioningVersion");
            gen.PostState = (string)computersystem.SelectToken("ComputerSystem.Path.Oem.Hp.PostState");

            gen.CPUFamily = (string)computersystem.SelectToken("ComputerSystem.Path.Processors.ProcessorFamily");
            gen.CPUCount = (string)computersystem.SelectToken("ComputerSystem.Path.Processors.Count");
            gen.CPUHealth = (string)computersystem.SelectToken("ComputerSystem.Path.Memory.Status.HealthRollUp");
            //power
            gen.PowerState = (string)computersystem.SelectToken("PowerState");
            gen.PowerAutoOn = (string)computersystem.SelectToken("Oem.Hp.PowerAutoOn");
            gen.PowerOnDelay = (string)computersystem.SelectToken("Oem.Hp.PowerOnDelay");
            gen.PowerAutoOn = (string)computersystem.SelectToken("Oem.Hp.PowerAutoOn");
            gen.PowerRegulatorMode = (string)computersystem.SelectToken("Oem.Hp.PowerRegulatorMode");
            gen.PowerAllocationLimit = (string)computersystem.SelectToken("Oem.Hp.PowerAllocationLimit");

            //iLO Self Test
            gen.NVRAMDataNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[0].Notes");
            gen.NVRAMDataStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[0].Status");
            gen.NVRAMSpaceNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[1].Notes");
            gen.NVRAMSpaceStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[1].Status");
            gen.EmbeddedFlashSDCardNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[2].Notes");
            gen.EmbeddedFlashSDCardStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[2].Status");
            gen.EEPROMNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[3].Notes");
            gen.EEPROMStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[3].Status");
            gen.HostRomNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[4].Notes");
            gen.HostRomStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[4].Status");
            gen.SupportedHostNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[5].Notes");
            gen.SupportedHostStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[5].Status");
            gen.PowerManagementControllerNotes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[6].Notes");
            gen.PowerManagementControllerStatus = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[6].Status");
            gen.CPLDPAL0Notes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[7].Notes");
            gen.CPLDPAL0Status = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[7].Status");
            gen.CPLDPAL1Notes = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[8].Notes");
            gen.CPLDPAL1Status = (string)manager.SelectToken("Manager.Path.Oem.Hp.iLOSelfTestResults[8].Status");

            //READ SEPARATE XML FILE
            //read xml health summary
            GET_EMBEDDED_HEALTH_DATA_GEN9.GET_EMBEDDED_HEALTH_DATA HealthSum = new GET_EMBEDDED_HEALTH_DATA_GEN9.GET_EMBEDDED_HEALTH_DATA();

            switch (serverno)
            {
                //case "S0":
                //    HealthSum = GetHealthSummaryGen9(serverip, "S0");
                //    break;
                case "S1":
                    HealthSum = GetHealthSummaryGen9(serverip, "S1");
                    break;
                case "S2":
                    HealthSum = GetHealthSummaryGen9(serverip, "S2");
                    break;
                case "S3":
                    HealthSum = GetHealthSummaryGen9(serverip, "S3");
                    break;
                case "S4":
                    HealthSum = GetHealthSummaryGen9(serverip, "S4");
                    break;
                case "S5":
                    HealthSum = GetHealthSummaryGen9(serverip, "S5");
                    break;
                case "S6":
                    HealthSum = GetHealthSummaryGen9(serverip, "S6");
                    break;
                case "S7":
                    HealthSum = GetHealthSummaryGen9(serverip, "S7");
                    break;
                case "S8":
                    HealthSum = GetHealthSummaryGen9(serverip, "S8");
                    break;
                case "S9":
                    HealthSum = GetHealthSummaryGen9(serverip, "S9");
                    break;
                case "S10":
                    HealthSum = GetHealthSummaryGen9(serverip, "S10");
                    break;
                case "S11":
                    HealthSum = GetHealthSummaryGen9(serverip, "S11");
                    break;
                case "S12":
                    HealthSum = GetHealthSummaryGen9(serverip, "S12");
                    break;
                case "S13":
                    HealthSum = GetHealthSummaryGen9(serverip, "S13");
                    break;
                case "S14":
                    HealthSum = GetHealthSummaryGen9(serverip, "S14");
                    break;
                case "S15":
                    HealthSum = GetHealthSummaryGen9(serverip, "S15");
                    break;
                case "S16":
                    HealthSum = GetHealthSummaryGen9(serverip, "S16");
                    break;

            }



            try
            {


                if (HealthSum != null)
                {
                    gen.BiosHardwareStatus = HealthSum.HEALTH_AT_A_GLANCE.BIOS_HARDWARE.STATUS;
                    gen.ProcessorStatus = HealthSum.HEALTH_AT_A_GLANCE.PROCESSOR.STATUS;
                    gen.MemoryStatus = HealthSum.HEALTH_AT_A_GLANCE.MEMORY.STATUS;
                    gen.NetworkStatus = HealthSum.HEALTH_AT_A_GLANCE.NETWORK.STATUS;
                    gen.StorageStatus = HealthSum.HEALTH_AT_A_GLANCE.STORAGE.STATUS;
                    gen.TemperatureStatus = HealthSum.HEALTH_AT_A_GLANCE.TEMPERATURE.STATUS;
                    gen.FansStatus = HealthSum.HEALTH_AT_A_GLANCE.FANS.FirstOrDefault().STATUS.ToString();
                    //gen.FansRedundancy = HealthSum.HEALTH_AT_A_GLANCE.FANS.FirstOrDefault().REDUNDANCY.ToString();
                    gen.PowerSuppliesStatus = HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES.FirstOrDefault().STATUS.ToString();
                    //gen.PowerSupplyRedundancy = HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES.FirstOrDefault().REDUNDANCY.ToString();

                    //FANS Collection
                    if (HealthSum.HEALTH_AT_A_GLANCE.FANS != null)
                        foreach (var itm in HealthSum.HEALTH_AT_A_GLANCE.FANS)
                        {
                            //MessageBox.Show(itm.STATUS);

                            gen.FansRedundancy = itm.REDUNDANCY;
                        }


                    //POWER SUPPLIES Collection
                    if (HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES != null)
                        foreach (var itm in HealthSum.HEALTH_AT_A_GLANCE.POWER_SUPPLIES)
                        {

                            //gen.PowerSuppliesStatus = itm.STATUS.FirstOrDefault().ToString(); 
                            gen.PowerSupplyRedundancy = itm.REDUNDANCY;
                        }


                    //Read rest of Data in XML file
                    //FANS
                    gen.FanInfo = new System.Collections.ObjectModel.ObservableCollection<HealthFansGenGeneric>();

                    if (HealthSum.FANS != null)
                        foreach (var itm in HealthSum.FANS)
                        {
                            gen.FanInfo.Add(new HealthFansGenGeneric(itm.LABEL.VALUE, itm.ZONE.VALUE, itm.STATUS.VALUE, itm.SPEED.VALUE + "%"));
                        }
                    //TEMPERATURES
                    gen.TemperatureInfo = new System.Collections.ObjectModel.ObservableCollection<HealthTemperatureGenGeneric>();

                    if (HealthSum.TEMPERATURE != null)
                        foreach (var itm in HealthSum.TEMPERATURE)
                        {

                            gen.TemperatureInfo.Add(new HealthTemperatureGenGeneric(itm.LABEL.VALUE, itm.LOCATION.VALUE, itm.STATUS.VALUE, itm.CURRENTREADING.VALUE + " " + itm.CURRENTREADING.UNIT, itm.CAUTION.VALUE + " " + itm.CURRENTREADING.UNIT, itm.CRITICAL.VALUE + " " + itm.CURRENTREADING.UNIT));
                        }

                    //CPU
                    gen.CPUInfo = new System.Collections.ObjectModel.ObservableCollection<HealthCPUGenGeneric>();

                    if (HealthSum.PROCESSORS != null)
                        foreach (var itm in HealthSum.PROCESSORS)
                        {
                            gen.CPUInfo.Add(new HealthCPUGenGeneric(itm.LABEL.VALUE, itm.NAME.VALUE, itm.STATUS.VALUE, itm.SPEED.VALUE, itm.EXECUTION_TECHNOLOGY.VALUE, itm.MEMORY_TECHNOLOGY.VALUE, itm.INTERNAL_L1_CACHE.VALUE, itm.INTERNAL_L2_CACHE.VALUE, itm.INTERNAL_L3_CACHE.VALUE));
                        }

                    //MEMORY
                    if (HealthSum.MEMORY != null)
                    {

                        gen.MemoryInfo = new System.Collections.ObjectModel.ObservableCollection<HealthMemoryGenGeneric>();
                        //HealthSum.MEMORY.ADVANCED_MEMORY_PROTECTION
                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_1 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_1)
                            {

                                gen.MemoryInfo.Add(new HealthMemoryGenGeneric("CPU1", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE, itm.PART.NUMBER, itm.TYPE.VALUE, itm.SIZE.VALUE, itm.FREQUENCY.VALUE, itm.MINIMUM_VOLTAGE.VALUE, itm.RANKS.VALUE, itm.TECHNOLOGY.VALUE));

                            }
                        }


                        if (HealthSum.MEMORY.MEMORY_DETAILS != null)
                        {
                            if (HealthSum.MEMORY.MEMORY_DETAILS.CPU_2 != null)
                                foreach (var itm in HealthSum.MEMORY.MEMORY_DETAILS.CPU_2)
                            {

                                gen.MemoryInfo.Add(new HealthMemoryGenGeneric("CPU2", itm.SOCKET.VALUE.ToString(), itm.STATUS.VALUE, itm.HP_SMART_MEMORY.VALUE, itm.PART.NUMBER, itm.TYPE.VALUE, itm.SIZE.VALUE, itm.FREQUENCY.VALUE, itm.MINIMUM_VOLTAGE.VALUE, itm.RANKS.VALUE, itm.TECHNOLOGY.VALUE));

                            }
                        }
                            
                    }
                    //NETWORK 

                    if (HealthSum.NIC_INFORMATION != null)
                    {
                        gen.NICInfo = new System.Collections.ObjectModel.ObservableCollection<HealthNetworkGenGeneric>();

                        if (HealthSum.NIC_INFORMATION != null)
                            foreach (var itm in HealthSum.NIC_INFORMATION.NIC)
                            {
                                if (itm.NETWORK_PORT != null)
                                if (itm.NETWORK_PORT != null || itm.PORT_DESCRIPTION != null || itm.LOCATION != null || itm.IP_ADDRESS != null || itm.MAC_ADDRESS != null || itm.STATUS != null)
                                {
                                    gen.NICInfo.Add(new HealthNetworkGenGeneric(itm.NETWORK_PORT.VALUE, itm.PORT_DESCRIPTION.VALUE, itm.LOCATION.VALUE, itm.IP_ADDRESS.VALUE, itm.MAC_ADDRESS.VALUE, itm.STATUS.VALUE));
                                }
                               
                            }

                    }


                    if (HealthSum.POWER_SUPPLIES != null)
                    {
                        //POWER
                        gen.PowerInfo = new System.Collections.ObjectModel.ObservableCollection<HealthPowerGenGeneric>();

                        //HealthSum.
                        if (HealthSum.POWER_SUPPLIES.SUPPLY != null)
                            foreach (var itm in HealthSum.POWER_SUPPLIES.SUPPLY)
                            {
                                if(itm.PRESENT != null)
                                if (itm.PRESENT != null || itm.STATUS != null || itm.MODEL != null || itm.SPARE != null || itm.SERIAL_NUMBER != null || itm.CAPACITY != null || itm.FIRMWARE_VERSION != null)
                                {
                                    gen.PowerInfo.Add(new HealthPowerGenGeneric(itm.LABEL.VALUE, itm.PRESENT.VALUE, itm.STATUS.VALUE, itm.MODEL.VALUE, itm.SPARE.VALUE, itm.SERIAL_NUMBER.VALUE, itm.SPARE.VALUE, itm.SERIAL_NUMBER.VALUE, itm.CAPACITY.VALUE, itm.FIRMWARE_VERSION.VALUE.ToString()));
                                }
                            }

                    }

                    if (HealthSum.STORAGE.CONTROLLER != null)
                    {

                        //STORAGE
                        HealthStorageGenGeneric controllerinfo = new HealthStorageGenGeneric();
                        if (HealthSum.STORAGE.CONTROLLER.LABEL.VALUE != null)
                            controllerinfo.Label = HealthSum.STORAGE.CONTROLLER.LABEL.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.STATUS.VALUE != null)
                            controllerinfo.Status = HealthSum.STORAGE.CONTROLLER.STATUS.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.CONTROLLER_STATUS.VALUE != null)
                            controllerinfo.ControllerStatus = HealthSum.STORAGE.CONTROLLER.CONTROLLER_STATUS.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.SERIAL_NUMBER.VALUE != null)
                            controllerinfo.Serial = HealthSum.STORAGE.CONTROLLER.SERIAL_NUMBER.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.MODEL.VALUE != null)
                            controllerinfo.Model = HealthSum.STORAGE.CONTROLLER.MODEL.VALUE;
                        if (HealthSum.STORAGE.CONTROLLER.FW_VERSION.VALUE.ToString() != null)
                            controllerinfo.Firmware = HealthSum.STORAGE.CONTROLLER.FW_VERSION.VALUE.ToString();

                        if (HealthSum.STORAGE.CONTROLLER.DRIVE_ENCLOSURE != null)
                            foreach (var itm in HealthSum.STORAGE.CONTROLLER.DRIVE_ENCLOSURE)
                            {
                                controllerinfo.DriveEnclosure.Add(new DriveEnclosureInfoGenGeneric(itm.LABEL.VALUE, itm.STATUS.VALUE, itm.DRIVE_BAY.VALUE.ToString()));
                                //MessageBox.Show(itm.LABEL.VALUE + "  " +);
                            }



                        //if (HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE != null)
                        //    foreach (var itm in HealthSum.STORAGE.CONTROLLER.LOGICAL_DRIVE)
                        //    {
                        //        controllerinfo.LogicalDrives.Add(new HealthLogicalDriveGen10(itm.LABEL.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.CAPACITY.VALUE.ToString(), itm.FAULT_TOLERANCE.VALUE.ToString(), itm.LOGICAL_DRIVE_TYPE.VALUE.ToString(), itm.ENCRYPTION_STATUS.VALUE.ToString(), itm.PHYSICAL_DRIVE));
                        //        //MessageBox.Show(itm.FW_VERSION.VALUE.ToString());

                        //        //foreach (var itm2 in itm.PHYSICAL_DRIVE)
                        //        //{
                        //        //    controllerinfo.LogicalDrives...Add(new PhysicalDrivesGen8(itm.LABEL.VALUE.ToString(), itm.STATUS.VALUE.ToString(), itm.SERIAL_NUMBER.VALUE.ToString(), itm.MODEL.VALUE.ToString(), itm.CAPACITY.VALUE.ToString(), itm.MARKETING_CAPACITY.VALUE.ToString(), itm.LOCATION.VALUE.ToString(), itm.FW_VERSION.VALUE.ToString(), itm.DRIVE_CONFIGURATION.VALUE.ToString(), itm.ENCRYPTION_STATUS.VALUE.ToString(), itm.MEDIA_TYPE.VALUE.ToString()));
                        //        //    //MessageBox.Show(itm.FW_VERSION.VALUE.ToString());
                        //        //}

                        //        // itm.PHYSICAL_DRIVE.
                        //    }


                    }
                }





                        return gen;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //Console.WriteLine(ex.Message);
                return gen;
            }
        }



        /// <summary>
        /// DELL SERVERS
        /// </summary>
        /// <param name="millSecs"></param>
        /// <returns></returns>

        //CREATION OF INSTALL SET FOR GEN 10 Servers
        public void CreateGen10InstallSet(string WhichServer, string ServerIP,string BiosPath,string IEnginePath,string SPSPath, string ILOPath)
        {
            
            if (vm.ServerType == "HPGen10RackMount" || vm.ServerType == "HPGen10Blade")
            {

                int UpdatesCount = 0;


                List<string> updateList = new List<string>();
                List<Sequence> InstallSequence = new List<Sequence>();

                if (ILOPath != "")
                {
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "ILOUpdate.bin"))
                    {
                        UpdatesCount = UpdatesCount + 1;
                        updateList.Add(ServerIP + "ILOUpdate.bin");
                    }

                }
                if (IEnginePath != "")
                {
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "IENGUpdate.bin"))
                    {
                        UpdatesCount = UpdatesCount + 1;
                        updateList.Add(ServerIP + "IENGUpdate.bin");
                    }
                }
                if (BiosPath != "")
                {
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "BIOSUpdate.bin"))
                    {
                        UpdatesCount = UpdatesCount + 1;
                        updateList.Add(ServerIP + "BIOSUpdate.bin");
                    }
                }
                if (SPSPath != "")
                {
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + ServerIP + "SPSUpdate.bin"))
                    {
                        UpdatesCount = UpdatesCount + 1;
                        updateList.Add(ServerIP + "SPSUpdate.bin");
                    }
                }
               

                //ILO5InstallSet ilo5 = new ILO5InstallSet();

                //ilo5.
                Rootobject ro = new Rootobject();
                ro.Name = "Updates" + DateTime.Now.ToShortDateString().Replace("/","-");
                //ro.Name = "Updates";
                ro.Description = "Firmware Install Set";
                //ro.Sequence = new Sequence[UpdatesCount + 2];
                ro.Sequence = new Sequence[UpdatesCount];
                ro.IsRecovery = false;

                //Start Sequence  Doesn't seem to run 
                //Sequence seqStart = new Sequence();
                //seqStart.Name = "Wait";
                //seqStart.UpdatableBy = new string[1];
                //seqStart.UpdatableBy[0] = "RuntimeAgent";
                //seqStart.Command = "Wait";
                ////seqStart.Filename = "";
                //seqStart.WaitTimeSeconds = 60;
                ////Add to Install Sequence List
                //InstallSequence.Add(seqStart);

             



                //Loop through the Update List
                foreach(var itm in updateList)
                {
                    
                    Sequence seqUpdate = new Sequence();
                   
                    if (itm.Contains("IENG"))
                    {
                        seqUpdate.Name = "IE Firmware";
                    }
                    if (itm.Contains("SPS"))
                    {
                        seqUpdate.Name = "SPS Firmware";
                    }
                    if (itm.Contains("ILO")|| itm.Contains("ilo"))
                    {
                        seqUpdate.Name = "ILO Firmware";
                    }
                    if (itm.Contains("BIOS"))
                    {
                        seqUpdate.Name = "BIOS Firmware";
                    }
                    seqUpdate.UpdatableBy = new string[1];
                    seqUpdate.UpdatableBy[0] = "Bmc";
                    seqUpdate.Command = "ApplyUpdate";
                    seqUpdate.Filename = itm;
                    seqUpdate.WaitTimeSeconds = 0;
                    //Add to Install Sequence List
                    InstallSequence.Add(seqUpdate);
                }

               


                //End Sequence Reboot
                //Sequence seqEnd = new Sequence();
                //seqEnd.Name = "Reboot";
                //seqEnd.UpdatableBy = new string[1];
                //seqEnd.UpdatableBy[0] = "RuntimeAgent";
                //seqEnd.Command = "ResetServer";
                ////Add to Install Sequence List
                //InstallSequence.Add(seqEnd);


              
                //Loop through the sequence list and add to root object
                int i = 0;

                foreach (var itm in InstallSequence)
                {
                    ro.Sequence[i] = itm;
                    i++;
                }


                JsonSerializer serializer = new JsonSerializer();
                //serializer.Converters.Add(new JavaScriptDateTimeConverter());
                serializer.NullValueHandling = NullValueHandling.Ignore;
                // Create the json doc WhichServer is either S1, S2 S3 S4
                using (StreamWriter sw = new StreamWriter(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + WhichServer + "InstallSet.json"))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, ro);
                    // {"ExpiryDate":new Date(1230375600000),"Price":0}
                }



            }

        }





        //GEN 10 Summary Report

        public Gen10SummaryReport.diagOutput ReadGen10SummaryXMLReports(string filePath)
        {
            //Full Info
            //using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml"))
            //{

            //}
            //Gen10DiagnosticReport.diagOutput diagReport = new Gen10DiagnosticReport.diagOutput();


            //StaticFunctions.
            //File.WriteAllText(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml", ServerInfoJson);


            //diagReport.diagCollection.Add(XMLTools.ReadFromXmlString<Gen10DiagnosticReport.diagOutput>(@"" + vm.dsktop + @"\Gen10 Test Reports\2020-12-11-10_26_17-CZ3032YX98-testlog.xml"));

            return XMLTools.ReadFromXmlString<Gen10SummaryReport.diagOutput>(filePath);
        }

        //GEN 10 Diagnostic Report

        public Gen10DiagnosticReport.diagOutput ReadGen10DiagnosticXMLReports(string filePath)
        {
            //Full Info
            //using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealth1.xml"))
            //{

            //}
            //Gen10DiagnosticReport.diagOutput diagReport = new Gen10DiagnosticReport.diagOutput();


            //StaticFunctions.
            //File.WriteAllText(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + "ServerHealthTrimmed1.xml", ServerInfoJson);


            //diagReport.diagCollection.Add(XMLTools.ReadFromXmlString<Gen10DiagnosticReport.diagOutput>(@"" + vm.dsktop + @"\Gen10 Test Reports\2020-12-11-10_26_17-CZ3032YX98-testlog.xml"));

            return XMLTools.ReadFromXmlString<Gen10DiagnosticReport.diagOutput>(filePath);
        }


        //LENOVO Inventory Data

        public LenovoServerInfo GetLenovoServerData(string serverip, string serverno, string DataPath, string HealthData)
        {

            //try
            //{


            // LENOVO READ XML DATA FUNCTION
            //Create New Insance
            onecli LenovoInventoryData = new onecli();


            //MessageBox.Show(DataPath);

            // Read from captured XML
            LenovoInventoryData = XMLTools.ReadFromXmlString<onecli>(DataPath);

            // MessageBox.Show(LenovoInventoryData.repository.CPU.GetValue);

            LenovoServerInfo lenInfo = new LenovoServerInfo();


            if (DataPath != "")
            {



             


                // ADD THIS TO GET SERVER INFO SCRIPT

                //// Summary Info
                if (LenovoInventoryData.repository.ComputerSystem_IPMI.Manufacturer.value != null)
                {
                    lenInfo.Manufacturer = LenovoInventoryData.repository.ComputerSystem_IPMI.Manufacturer.value;
                    lenInfo.Model = LenovoInventoryData.repository.ComputerSystem_IPMI.ProductName.value;
                    lenInfo.SystemSerialNumber = LenovoInventoryData.repository.ComputerSystem_IPMI.SerialNumber.value;
                    lenInfo.SystemUUID = LenovoInventoryData.repository.ComputerSystem_IPMI.SystemUUID.value;

                    //lenInfo.BoardSerialNumber = LenovoInventoryData.repository.SystemBoard.value;
                }


                //System Board Array
                if (LenovoInventoryData.repository.SystemBoard != null)
                    foreach (var item in LenovoInventoryData.repository.SystemBoard)
                    {
                        if (item.ElementName.value == "system board 1")
                        {
                            //Add MainBoard Serial to front page
                            lenInfo.BoardSerialNumber = item.SerialNumber.value.ToString();
                        }
                        else
                        {
                            //Add to the collection
                            LenovoSystemBoard boardInfo = new LenovoSystemBoard();
                            boardInfo.ElementName = item.ElementName.value.ToString();
                            boardInfo.Manufacturer = item.Manufacturer.value.ToString();
                            boardInfo.Model = item.Model.value.ToString();
                            boardInfo.PartNumber = item.PartNumber.value.ToString();
                            boardInfo.FRUNumber = item.FRUNumber.value.ToString();
                            boardInfo.SerialNumber = item.SerialNumber.value.ToString();

                            //Add to the collection
                            lenInfo.LenovoSystemBoardCollection.Add(boardInfo);
                        }
                    }

                //CPU Collection
                int CPUCount = 0;

                //CPU Environment Array
                if (LenovoInventoryData.repository.CPU != null)
                    foreach (var item in LenovoInventoryData.repository.CPU)
                    {

                        //Add to the collection
                        LenovoCPU cpuInfo = new LenovoCPU();
                        cpuInfo.Name = item.Name.value.ToString();
                        cpuInfo.CPUFamily = item.Family.value.ToString();
                        cpuInfo.HealthState = item.HealthState.value.ToString();
                        cpuInfo.CorePerCPU = item.CorePerCPU.value.ToString();
                        cpuInfo.ThreadPerCore = item.ThreadPerCore.value.ToString();
                        cpuInfo.CurrentFrequency = item.CurrentClockSpeed.value.ToString();
                        cpuInfo.MaxTurboFrequency = item.MaxClockSpeed.value.ToString();
                        cpuInfo.L1Cache = item.L1Cache.value.ToString();
                        cpuInfo.L2Cache = item.L2Cache.value.ToString();
                        cpuInfo.L3Cache = item.L3Cache.value.ToString();



                        //Add to the collection
                        lenInfo.LenovoCPUCollection.Add(cpuInfo);

                    }


                //Memory Array
                if (LenovoInventoryData.repository.DIMM != null)
                    foreach (var item in LenovoInventoryData.repository.DIMM)
                    {

                        //Add to the collection
                        LenovoMemory dimmInfo = new LenovoMemory();
                        dimmInfo.BankLabel = item.BankLabel.value.ToString();
                        dimmInfo.ElementName = item.ElementName.value.ToString();
                        dimmInfo.Manufacturer = item.Manufacturer.value.ToString();
                        dimmInfo.Model = item.Model.value.ToString();
                        dimmInfo.PartNumber = item.PartNumber.value.ToString();
                        dimmInfo.SerialNumber = item.SerialNumber.value.ToString();
                        dimmInfo.MaxMemorySpeed = item.MaxMemorySpeed.value.ToString();
                        dimmInfo.Capacity = MyExtensions.ToSize(Convert.ToDouble(item.Capacity.value.ToString().Replace("MB", "")), MyExtensions.SizeUnits.MB);
                        dimmInfo.FRUPartNumber = item.FRUPartNumber.value.ToString();

                        //Add to the collection
                        lenInfo.LenovoMemoryCollection.Add(dimmInfo);

                    }

                //Memory Single Properties
                lenInfo.SysMemMaxCapacitySize = MyExtensions.ToSize(Convert.ToDouble(LenovoInventoryData.repository.MemoryStateOOB.Capacity.value.ToString().Replace("MB", "")), MyExtensions.SizeUnits.MB);
                lenInfo.TotalMemory = MyExtensions.ToSize(Convert.ToDouble(LenovoInventoryData.repository.MemoryStateOOB.NumberOfBlocks.value.ToString().Replace("MB", "")), MyExtensions.SizeUnits.MB);
                lenInfo.MemoryRollupStatus = LenovoInventoryData.repository.MemoryStateOOB.HealthState.value.ToString();
                lenInfo.MemoryBlockSize = LenovoInventoryData.repository.MemoryStateOOB.BlockSize.value.ToString();


                //Fan Array
                if (LenovoInventoryData.repository.Fan != null)
                    foreach (var item in LenovoInventoryData.repository.Fan)
                    {

                        //Add to the collection
                        LenovoFans fanInfo = new LenovoFans();
                        fanInfo.ElementName = item.ElementName.value.ToString();
                        fanInfo.HealthState = item.HealthState.value.ToString();
                        fanInfo.CurrentSpeed = item.DesiredSpeed.value.ToString();

                        //Add to the collection
                        lenInfo.LenovoFanCollection.Add(fanInfo);

                    }

                //string powerIssue = "";
                //Power Array
                if (LenovoInventoryData.repository.Power != null)
                    foreach (var item in LenovoInventoryData.repository.Power)
                    {

                        //Add to the collection
                        LenovoPower powerInfo = new LenovoPower();
                        powerInfo.ElementName = item.ElementName.value.ToString();
                        powerInfo.HealthState = item.HealthState.value.ToString();
                        if (powerInfo.HealthState != "Healthy" || powerInfo.HealthState != "Unknown")
                        {
                            if (powerInfo.HealthState == "Unknown")
                            {

                            }
                            else
                            {
                                lenInfo.PSRollupStatus = item.HealthState.value.ToString();
                            }

                              
                        }
                        else
                        {

                            //Set Health State
                            lenInfo.PSRollupStatus = item.HealthState.value.ToString();

                        }


                        powerInfo.TotalOutputPower = item.TotalOutputPower.value.ToString();


                        //Add to the collection
                        lenInfo.LenovoPowerCollection.Add(powerInfo);

                    }

                //Final Check For Any Issue
                //if (powerIssue == "Faulty")
                //{
                //    lenInfo.PSRollupStatus = "Faulty";
                //}



                //PCI Network Adaptor Array
                if (LenovoInventoryData.repository.PCIAdapter != null)
                    foreach (var item in LenovoInventoryData.repository.PCIPort)
                    {

                        //Add to the collection
                        LenovoNetwork networkInfo = new LenovoNetwork();
                        networkInfo.Name = item.Name.value.ToString();
                        networkInfo.LinkTechnology = item.LinkTechnology.value.ToString();
                        networkInfo.MaxSpeed = item.MaxSpeed.value.ToString();
                        networkInfo.EnabledState = item.EnabledState.value.ToString();
                        networkInfo.PortAvailability = item.PortAvailability.value.ToString();
                        networkInfo.RequestedState = item.RequestedState.value.ToString();


                        //Add to the collection
                        lenInfo.LenovoNetworkCollection.Add(networkInfo);

                    }

                //Storage Controller Information
                if (LenovoInventoryData.repository.StorageDevice != null)
                {
                    LenovoStorage storageInfo = new LenovoStorage();
                    storageInfo.Name = LenovoInventoryData.repository.StorageDevice.Name.value;
                    storageInfo.Model = LenovoInventoryData.repository.StorageDevice.Model.value;
                    storageInfo.SerialNumber = LenovoInventoryData.repository.StorageDevice.SerialNum.value;
                    storageInfo.Manufacturer = LenovoInventoryData.repository.StorageDevice.Manufacturer.value;
                    storageInfo.PartNumber = LenovoInventoryData.repository.StorageDevice.PartNumber.value;
                    storageInfo.Model = LenovoInventoryData.repository.StorageDevice.Model.value;
                    storageInfo.FRUNumber = LenovoInventoryData.repository.StorageDevice.FRUNumber.value;
                    storageInfo.CacheStatus = LenovoInventoryData.repository.StorageDevice.CacheStatus.value.ToString();
                    storageInfo.CacheSerialNumber = LenovoInventoryData.repository.StorageDevice.CacheSerialNum.value.ToString();
                    storageInfo.CacheMemorySize = LenovoInventoryData.repository.StorageDevice.CacheMemorySize.value.ToString();


                    //Add to the collection
                    lenInfo.LenovoStorageCollection.Add(storageInfo);
                }



                //Storage Software Identity Array
                if (LenovoInventoryData.repository.StorageSoftwareIdentity != null)
                    foreach (var item in LenovoInventoryData.repository.StorageSoftwareIdentity)
                    {

                        //Add to the collection
                        LenovoStorageSoftware storagesoftInfo = new LenovoStorageSoftware();
                        storagesoftInfo.ElementName = item.ElementName.value.ToString();
                        storagesoftInfo.ProductName = item.ProductName.value.ToString();
                        storagesoftInfo.Manufacturer = item.Manufacturer.value.ToString();
                        storagesoftInfo.Name = item.Name.value.ToString();
                        storagesoftInfo.VersionString = item.VersionString.value.ToString();
                        storagesoftInfo.Classifications = item.Classifications.value.ToString();
                        storagesoftInfo.ClassificationsDescriptions = item.ClassificationDescriptions.value.ToString();
                        storagesoftInfo.IdentityInfoValue = item.IdentityInfoValue.value.ToString();
                        storagesoftInfo.IdentityInfoType = item.IdentityInfoType.value.ToString();
                        storagesoftInfo.SoftwareID = item.SoftwareID.value.ToString();
                        storagesoftInfo.SoftwareRole = item.SoftwareRole.value.ToString();
                        storagesoftInfo.SoftwareStatus = item.SoftwareStatus.value.ToString();



                        //Add to the collection
                        lenInfo.LenovoStorageSoftwareCollection.Add(storagesoftInfo);

                    }




                //BMC Environment Array
                if (LenovoInventoryData.repository.IMMNumericSensor != null)
                    foreach (var item in LenovoInventoryData.repository.IMMNumericSensor)
                    {

                        //Add to the collection
                        LenovoBMC bmcInfo = new LenovoBMC();
                        bmcInfo.ElementName = item.ElementName.value.ToString();
                        bmcInfo.CurrentReading = item.CurrentReading.value.ToString();
                        bmcInfo.BaseUnit = item.BaseUnit.value.ToString();

                        //update front page
                        if (bmcInfo.ElementName == "Ambient Temp")
                        {
                            lenInfo.AmbientTemperature = bmcInfo.CurrentReading + " C";
                        }

                        //Add to the collection
                        lenInfo.LenovoBMCCollection.Add(bmcInfo);

                    }





                // Health Mapping
                //CPU Collection Health
                //string cpuIssue = "";
                foreach (var cpuitem in lenInfo.LenovoCPUCollection)
                {
                    if (cpuitem.HealthState != "Healthy" || cpuitem.HealthState != "Unknown")
                    {
                        //HEALTH STATUSES
                        //lenInfo.CPURollupStatus = "Faulty";
                        //cpuIssue = "Faulty";
                        if(cpuitem.HealthState == "Unknown")
                        {

                        }
                        else
                        {
                            lenInfo.CPURollupStatus = cpuitem.HealthState;
                        }
                       
                    }
                    else
                    {
                        //HEALTH STATUSES
                        lenInfo.CPURollupStatus = cpuitem.HealthState;

                    }

                    //UPDATE FRONT SCREEN VALUES

                    lenInfo.CPUFamily = cpuitem.CPUFamily;
                    lenInfo.CurrentClockSpeed = cpuitem.CurrentFrequency;
                    lenInfo.MaxClockSpeed = cpuitem.MaxTurboFrequency;
                    lenInfo.CorePerCPU = cpuitem.CorePerCPU;
                    lenInfo.ThreadPerCore = cpuitem.ThreadPerCore;


                    CPUCount += 1;

                }

                //Final Check For Any Issue
                //if (cpuIssue == "Faulty")
                //{
                //    lenInfo.CPURollupStatus = "Faulty";
                //}

                lenInfo.CPUCount = CPUCount.ToString();





                //FANS
               // string fanIssue = "";

                foreach (var fanitem in lenInfo.LenovoFanCollection)
                {
                    if (fanitem.HealthState != "Healthy" || fanitem.HealthState != "Unknown")
                    {
                        ////HEALTH STATUSES
                        //lenInfo.FanRollupStatus = "Fault";
                        //fanIssue = "Fault";
                        if (fanitem.HealthState == "Unknown")
                        {

                        }
                        else
                        {

                            lenInfo.FanRollupStatus = fanitem.HealthState;
                        }
                    }
                    else
                    {
                        //HEALTH STATUSES
                        lenInfo.FanRollupStatus = fanitem.HealthState;

                    }

                    //UPDATE FRONT SCREEN VALUES


                }

            

                //Firmware
                if (LenovoInventoryData.repository.XFirmwareBuildID != null)
                    foreach (var item in LenovoInventoryData.repository.XFirmwareBuildID)
                    {

                        //Add to the collection
                        LenovoFirmware firmwareInfo = new LenovoFirmware();
                        firmwareInfo.Name = item.Name.value.ToString();
                        firmwareInfo.BuildID = item.BuildID.value.ToString();
                        firmwareInfo.BuildIDBackup = item.BuildIDBackup.value.ToString();
                        firmwareInfo.BuildDate = item.BuildDate.value.ToString();
                        firmwareInfo.BuildDateBackup = item.BuildID.value.ToString();

                        switch (firmwareInfo.Name)
                        {
                            case "UEFI":
                                lenInfo.BiosCurrentVersion = firmwareInfo.BuildID;
                                lenInfo.BiosReleaseDate = firmwareInfo.BuildDate;
                                break;
                            case "BMC":
                                lenInfo.IMMCurrentVersion = firmwareInfo.BuildID;
                                lenInfo.IMMReleaseDate = firmwareInfo.BuildDate;
                                break;
                            case "Preboot DSA":
                                lenInfo.DSACurrentVersion = firmwareInfo.BuildID;
                                lenInfo.DSAReleaseDate = firmwareInfo.BuildDate;
                                break;
                        }

                        //Add to the collection
                        lenInfo.LenovoFirmwareCollection.Add(firmwareInfo);

                    }

                //Split string up after every newline
                var result = HealthData.Split(new string[] { "\n" }, StringSplitOptions.None);

                //Read the Health Data From SSH
                StringReader strReader = new StringReader(HealthData);
                //MessageBox.Show(HealthData);
                foreach (var line in result)
                {
                    if (line.Contains("Cooling Devices"))
                    {
                        lenInfo.FanRollupStatus = line.Replace("Cooling Devices", "").Trim();
                    }
                    else if (line.Contains("Power Modules"))
                    {
                        lenInfo.PSRollupStatus = line.Replace("Power Modules", "").Trim();
                    }
                    else if (line.Contains("Storage"))
                    {
                        lenInfo.StorageRollupStatus = line.Replace("Storage", "").Trim();
                    }
                    else if (line.Contains("Processors"))
                    {
                        lenInfo.CPURollupStatus = line.Replace("Processors", "").Trim();
                    }
                    else if (line.Contains("Memory"))
                    {
                        lenInfo.MemoryRollupStatus = line.Replace("Memory", "").Trim();
                    }
                    else if (line.Contains("Power"))
                    {
                        lenInfo.PowerState = line.Replace("Power", "").Replace("system>", "").Trim();
                    }
                }



                return lenInfo;

            }
            else
            {
                return lenInfo;
            }

            //}
            //catch (Exception ex)
            //{
            //    vm.WarningMessage1 = "Error: " + ex.Message;
            //    vm.ProgressPercentage1 = 0;
            //    vm.ProgressIsActive1 = false;
            //    vm.ProgressVisibility1 = Visibility.Hidden;
            //    vm.ProgressMessage1 = "Scripts Cancelled!";
            //    vm.CancelScriptS1 = false;


            //    return null;

            
            //}

        }



        public string ExecuteCommandSsh(string host, int port, string username, string password, string commands)
        {
            var returnMessage = string.Empty;

            try
            {
                if (commands == string.Empty)
                {
                    MessageBox.Show("Please enter a Command in the Command Textbox");
                }
                else
                {
                    using (var client = new SshClient(host, port, username, password))
                    {
                        //Create the command string
                        var fullCommand = string.Empty;
                        foreach (var command in commands)
                        {
                            fullCommand += command + "\n";
                        }

                        client.Connect();
                        var sshCommand = client.CreateCommand(fullCommand);
                        sshCommand.CommandTimeout = new TimeSpan(0, 0, 10);

                        try
                        {
                            sshCommand.Execute();
                            // MessageBox.Show(sshCommand.Result);
                            returnMessage = sshCommand.Result;
                        }
                        catch (SshOperationTimeoutException)
                        {
                            returnMessage = sshCommand.Result;
                        }


                        client.Disconnect();
                    }
                }



            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return returnMessage;
        }



        public string SendSSHCommand(string host, string username, string password, string command)
        {
            string dataReturned = "";


          
                try
                {


                    //Set up the SSH connection
                    using (var client = new SshClient(host,22, username, password))
                    {

                    //Accept Host key
                    client.HostKeyReceived += delegate (object sender, HostKeyEventArgs e)
                    {
                        e.CanTrust = true;
                    };


                    client.Connect();


                        //   MessageBox.Show(client.RunCommand(command).Result);


                    //set string to data returned
                    dataReturned += client.RunCommand(command).Result;
                    //disconnect ssh client
                    client.Disconnect();
                    //return string data
                    return dataReturned;
                    }
            }
            catch (Exception ex)
            {
                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;


                return "";


            }



        }



        //END LENOVO











        //timed delay
        //async Task PutTaskDelay(int millSecs)
        //{
        //    await Task.Delay(millSecs);
        //}



    }
}
