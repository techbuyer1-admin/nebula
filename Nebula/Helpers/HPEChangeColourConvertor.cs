﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;

namespace Nebula.Helpers
{
    public class HPEChangeColourConvertor : IValueConverter
    {
         object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
           
            string status = (value as string);
            if (status != null)
            if (status == "OK"  || status == "Healthy" || status == "Present" || status == "Normal" || status.Contains("Present") || status == "Redundant" || status == "InPostDiscoveryComplete" || status == "FinishedPost" || status == "PASS" || status == "On" || status == "0") return new SolidColorBrush(Colors.Green);
            else if (status == "Not Redundant" || status == "Other" || status == "Unavailable" || status == "Warning" || status == "InPost" || status == "Unknown" || status == "None" || status ==  "Not Installed" || status == "Warning") return new SolidColorBrush(Colors.Goldenrod);
            else if (status == "Degraded" || status == "Fault" || status == "Critical" || status == "Critical Failure"  || status == "Failure" || status == "Faulty" || status == "Failed" || status == "FAIL" || status == "Error" || status == "PowerOff" || status == "Off") return new SolidColorBrush(Colors.OrangeRed);
            else if (status == "Informational" || status == "PENDING") return new SolidColorBrush(Colors.MediumPurple);
            //else if (status.Contains("OK")) return new Run("OK" ) { Foreground = Brushes.Green };
            //else if (status.Contains("Ok")) return new Run("Ok") { Foreground = Brushes.Green }; Critical Failure
            // SolidColorBrush(Colors.Green);
            // default return value of lime green      else if (status == "Failed" || status == "Error") return new SolidColorBrush(Colors.Red);
            return new SolidColorBrush(Colors.MediumPurple);
        }

       object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}


//Power State None, Unknown, Reset, PowerOff, InPost, InPostDiscoveryComplete, FinishedPost.