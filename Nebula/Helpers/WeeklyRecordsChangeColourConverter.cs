﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;

namespace Nebula.Helpers
{
    public class WeeklyRecordsChangeColourConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            int status = (int)value;
             // if value greater than 1 then colour green
                if (status > 0 ) return new SolidColorBrush(Colors.Green);
                else if (status == 0) return new SolidColorBrush(Colors.MediumPurple);
            //else if (status.Contains("OK")) return new Run("OK" ) { Foreground = Brushes.Green };
            //else if (status.Contains("Ok")) return new Run("Ok") { Foreground = Brushes.Green };
            // SolidColorBrush(Colors.Green);
            // default return value of lime green      else if (status == "Failed" || status == "Error") return new SolidColorBrush(Colors.Red);
            return new SolidColorBrush(Colors.MediumPurple);
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}


//Power State None, Unknown, Reset, PowerOff, InPost, InPostDiscoveryComplete, FinishedPost.
