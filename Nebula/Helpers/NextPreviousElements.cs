﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nebula.ViewModels;

namespace Nebula.Helpers
{
    public class NextPreviousElements
    {
        int docindex = 0;
        MainViewModel vm;
        public NextPreviousElements()
        {

        }

        public NextPreviousElements(MainViewModel PassedViewModel)
        {
            vm = PassedViewModel;
        }

        // INT VERSION
        public int GetNextElementInt(IList<string> list)
        {
            if (docindex == list.Count - 1)
            {
                //MessageBox.Show("Next > list count 1 " + docindex.ToString());
                //docindex = docindex;
                //index = index;
                //pick from the collection the report body at that index
                //vm.ReportBody = vm.ReportCollection.ElementAt(docindex);
                //extract the serial number from the selected report body
                // vm.SerialNo = ExtractSerialNumber(vm.ReportBody);
            }
            else
            {
                //MessageBox.Show("Next Increase by 1 " + docindex.ToString());
                //index++;
                docindex++;
                //pick from the collection the report body at that index
                vm.ReportBody = vm.ReportCollection.ElementAt(docindex);
                //extract the serial number from the selected report body
                vm.SerialNo = vm.ExtractSerialNumber(vm.ReportBody);
            }


            //return index;
            return docindex;
        }

        public int GetPreviousElementInt(IList<string> list)
        {
            if (docindex <= 0)
            {
                //MessageBox.Show("Prev index < zero " + docindex.ToString());
                docindex = 0;
                docindex = 0;
                //pick from the collection the report body at that index
                //vm.ReportBody = vm.ReportCollection.ElementAt(docindex);
                //extract the serial number from the selected report body
                //vm.SerialNo = ExtractSerialNumber(vm.ReportBody);
            }
            else
            {
                // MessageBox.Show("Prev reduce by 1 " + docindex.ToString());
                docindex--;
                //pick from the collection the report body at that index
                vm.ReportBody = vm.ReportCollection.ElementAt(docindex);
                //extract the serial number from the selected report body
                vm.SerialNo = vm.ExtractSerialNumber(vm.ReportBody);
            }


            //return index;
            return docindex;
        }

        // STRING VERSION
        public string GetNextElement(IList<string> list, int index)
        {
            if ((index > list.Count - 1) || (index < 0))
                throw new Exception("Invalid index");

            else if (index == list.Count - 1)
                index = 0;

            else
                index++;

            return list[index];
        }

        public string GetPreviousElement(IList<string> list, int index)
        {
            if ((index > list.Count - 1) || (index < 0))
                throw new Exception("Invalid index");

            else if (index == list.Count - 1)
                index = 0;

            else
                index--;

            return list[index];
        }




        //FOR ARRAY

        public string GetNextElement(string[] strArray, int index)
        {
            if ((index > strArray.Length - 1) || (index < 0))
                throw new Exception("Invalid index");

            else if (index == strArray.Length - 1)
                index = 0;

            else
                index++;

            return strArray[index];
        }

        public string GetPreviousElement(string[] strArray, int index)
        {
            if ((index > strArray.Length - 1) || (index < 0))
                throw new Exception("Invalid index");

            else if (index == strArray.Length - 1)
                index = 0;

            else
                index--;

            return strArray[index];
        }

    }
}
