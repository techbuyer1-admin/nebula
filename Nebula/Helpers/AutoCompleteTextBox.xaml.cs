﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shapes;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.Views;

namespace Nebula.Helpers
{
    /// <summary>
    /// Interaction logic for AutoCompleteTextBox.xaml
    /// </summary>
    public partial class AutoCompleteTextBox : UserControl
    {
        public AutoCompleteTextBox()
        {
            InitializeComponent();
        }

        //DEPENDANCY PROPERTY DECLARATION Dependancy Property and Backing Property
        //Dependancy Property for collection
        public static readonly DependencyProperty AutoSuggestionListProperty = DependencyProperty
      .Register("AutoSuggestionList", typeof(ObservableCollection<HPPartNumbers>), typeof(AutoCompleteTextBox),
          new FrameworkPropertyMetadata
          {
              DefaultValue = new ObservableCollection<HPPartNumbers>(),
              DefaultUpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged,
          });

        //Backing Property
        public ObservableCollection<HPPartNumbers> AutoSuggestionList
        {
            get
            {
                return (ObservableCollection<HPPartNumbers>)this.GetValue(AutoSuggestionListProperty);
            }
            set { this.SetValue(AutoSuggestionListProperty, value); }
        }
        //END DEPENDANCY PROPERTY DECLARATION 

        //DEPENDANCY PROPERTY DECLARATION Dependancy Property and Backing Property
      

        //WORKS FOR MAPPING TO ANOTHER TEXTBOX FIELD
        public static DependencyProperty TxtBoxValueProperty = DependencyProperty.Register("TxtBoxValue", typeof(string), typeof(AutoCompleteTextBox));

        public string TxtBoxValue
        {
            get { return (string)GetValue(TxtBoxValueProperty); }
            set
            {
                SetValue(TxtBoxValueProperty, value);
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == TxtBoxValueProperty)
            {
                // Do whatever you want with it
            }
        }
        //END DEPENDANCY PROPERTY DECLARATION 



        /// <summary>  
        ///  Open Auto Suggestion box method  
        /// </summary>  
        private void OpenAutoSuggestionBox()
        {
            try
            {
                // Enable.  
                this.autoListPopup.Visibility = Visibility.Visible;
                this.autoListPopup.IsOpen = true;
                this.autoList.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                // Info.  
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Console.Write(ex);
            }
        }



        #region Close Auto Suggestion box method  

        /// <summary>  
        ///  Close Auto Suggestion box method  
        /// </summary>  
        private void CloseAutoSuggestionBox()
        {
            try
            {
                // Enable.  
                this.autoListPopup.Visibility = Visibility.Collapsed;
                this.autoListPopup.IsOpen = false;
                this.autoList.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                // Info.  
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Console.Write(ex);
            }
        }

        #endregion

        #region Auto Text Box text changed the method  

        /// <summary>  
        ///  Auto Text Box text changed method.  
        /// </summary>  
        /// <param name="sender">Sender parameter</param>  
        /// <param name="e">Event parameter</param>  
        private void AutoTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                // Verification.  
                if (string.IsNullOrEmpty(this.autoTextBox.Text))
                {
                    // Disable.  
                    this.CloseAutoSuggestionBox();

                    // Info.  
                    return;
                }

                // Enable.  
                this.OpenAutoSuggestionBox();

                // Settings.  
                this.autoList.ItemsSource = this.AutoSuggestionList.Where(p => p.SparePartNumber.ToLower().Contains(this.autoTextBox.Text.ToLower())).ToList();
                //this.autoList.DisplayMemberPath = "SparePartNumber"; 

                //this.autoList.ItemsSource = this.AutoSuggestionList.Where(p => p.SparePartNumber.ToLower().Contains(this.autoTextBox.Text.ToLower())).ToList();
                //Set Exposed Dependancy Property


            }
            catch (Exception ex)
            {
                // Info.  
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Console.Write(ex);
            }
        }

        #endregion

        #region Auto list selection changed method  

        /// <summary>  
        ///  Auto list selection changed method.  
        /// </summary>  
        /// <param name="sender">Sender parameter</param>  
        /// <param name="e">Event parameter</param>  
        private void AutoList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                // Verification.  
                if (this.autoList.SelectedIndex <= -1)
                {
                    // Disable.  
                    this.CloseAutoSuggestionBox();

                    // Info.  
                    return;
                }

                // Disable.  
                this.CloseAutoSuggestionBox();

                // Settings.

                //this.autoTextBox.Text = this.autoList.SelectedItem.ToString();
                //Pull in the HPPartNumber Object and assign the part-Number
                HPPartNumbers hppn = (HPPartNumbers)this.autoList.SelectedValue;
                this.autoTextBox.Text = hppn.SparePartNumber;

                this.autoList.SelectedIndex = -1;

               //autoTextBox.Text = this.autoTextBox.Text;
            }
            catch (Exception ex)
            {
                // Info.  
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Console.Write(ex);
            }
        }

        #endregion
    }
}
