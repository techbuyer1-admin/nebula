﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace Nebula.Helpers
{
   public class RestClient
    {
        public enum httpVerb
        {
            GET,
            POST,
            PUT,
            DELETE
        }





        public string endPoint { get; set; }
        public httpVerb httpMethod { get; set; }

        //Default Constructor
        public RestClient()
        {
            //Ignore invalid SSL Certs
            ServicePointManager.ServerCertificateValidationCallback += (mender, certificate, chain, sslPolicyErrors) => true;

            //Set endpoint to nothing
            endPoint = "";
            //set the http request verb
            httpMethod = httpVerb.GET;

            

        }

        public string makeRequest()
        {
            string strResponseValue = string.Empty;

            var request = (HttpWebRequest)WebRequest.Create(endPoint);

            request.Method = httpMethod.ToString();
          
            var username = "admin";
            var password = "C!5c0Password";
            string encoded = System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1")
                                           .GetBytes(username + ":" + password));
            request.Headers.Add("Authorization", "Basic " + encoded);


           // request.PreAuthenticate = true;

            HttpWebResponse response = null;

            try
            {
                response = (HttpWebResponse)request.GetResponse();


                //Proecess the resppnse stream... (could be JSON, XML or HTML etc..._

                using (Stream responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            //Get returned JSON, XML, HTML
                            strResponseValue = reader.ReadToEnd();
                            //strResponseValue = reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //We catch non Http 200 responses here.
                strResponseValue = "{\"errorMessages\":[\"" + ex.Message.ToString() + "\"],\"errors\":{}}";
            }
            finally
            {
                if (response != null)
                {
                    ((IDisposable)response).Dispose();
                }
            }

            return strResponseValue;

        }
    }

}



