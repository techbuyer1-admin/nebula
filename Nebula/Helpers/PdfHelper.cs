﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace Nebula.Helpers
{

    public sealed class PDFHelper
    {

        private PDFHelper()
        {
        }

        public static PDFHelper Instance { get; } = new PDFHelper();

        internal void SaveImageAsPdf(string imageFileName, string pdfFileName, int width = 600, bool deleteImage = false)
        {
            using (var document = new PdfDocument())
            {
                PdfPage page = document.AddPage();
                using (XImage img = XImage.FromFile(imageFileName))
                {
                    // Calculate new height to keep image ratio
                    var height = (int)(((double)width / (double)img.PixelWidth) * img.PixelHeight);

                    // Change PDF Page size to match image
                    page.Width = width;
                    page.Height = height;

                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    gfx.DrawImage(img, 0, 0, width, height);
                }
                document.Save(pdfFileName);
            }

            //if (deleteImage)
            //    File.Delete(imageFileName);
        }



        public static void CombinePDF(List<string> files2Combine,string filePath)
        {

            //Byte Array List to hold each files bytes
            List<byte[]> pdfList = new List<byte[]>();

            byte[] Combined;

            //string fileURL,string fileURL2
            //Loop through list
            foreach (var f in files2Combine)
            {
                //Read File Bytes and add to Byte Array List
                pdfList.Add(File.ReadAllBytes(f));
            }

            //Combine the arrays
            Combined = CombinePDFs(pdfList); // Combine(a, b); //ConcatByteArrays(a, b);


            //https://stackoverflow.com/questions/61081400/how-to-properly-return-a-pdf-inside-a-json-response

            //string s = Convert.ToBase64String(Combined);

            // and save back - System.IO.File.WriteAll* makes sure all bytes are written properly.
            System.IO.File.WriteAllBytes(filePath, Combined);


        }


        //PDFSHARP EXAMPLE FREE LICENSE
        public static byte[] CombinePDFs(List<byte[]> srcPDFs)
        {
            using (var ms = new MemoryStream())
            {
                using (var resultPDF = new PdfDocument(ms))
                {
                    foreach (var pdf in srcPDFs)
                    {
                        using (var src = new MemoryStream(pdf))
                        {
                            using (var srcPDF = PdfReader.Open(src, PdfDocumentOpenMode.Import))
                            {
                                for (var i = 0; i < srcPDF.PageCount; i++)
                                {
                                    resultPDF.AddPage(srcPDF.Pages[i]);
                                }
                            }
                        }
                    }
                    resultPDF.Save(ms);
                    return ms.ToArray();
                }
            }
        }



        public static byte[] ConcatByteArrays(params byte[][] arrays)
        {
            return arrays.SelectMany(x => x).ToArray();
        }





    }

}