﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.ViewModels;
using Nebula.Properties;
using Nebula.Models;
using Nebula.Helpers;
using MahApps.Metro.Controls;
using Nebula.Commands;
using Nebula.Views;

namespace Nebula.Helpers
{
    /// <summary>
    /// Interaction logic for AutoCompletePartCodeTB.xaml
    /// </summary>
    public partial class AutoCompletePartCodeTB : UserControl
    {
        #region Private properties.  

        /// <summary>  
        /// Auto suggestion list property.  
        /// </summary>  
        //private List<string> autoSuggestionList = new List<string>();
        //private List<string> autoSuggestionList = new List<string>();

        #endregion

        #region Default Constructor  

        /// <summary>  
        /// Initializes a new instance of the <see cref="AutoCompletePartCodeTB" /> class.  
        /// </summary>  
        public AutoCompletePartCodeTB()
        {
            try
            {
                // Initialization.  
                this.InitializeComponent();
                //AutoSuggestionList = new List<string>();

            }
            catch (Exception ex)
            {
                // Info.  
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Console.Write(ex);
            }
        }

        #endregion

        #region Protected / Public properties.  

        /// <summary>  
        /// Gets or sets Auto suggestion list property.  
        /// </summary>  
        /// 


        // WHOLE DEPENDANCY PROPERTY
        //REQUIRES X:NAME TO BE SET ON USING VIEW

        public static readonly DependencyProperty AutoSuggestionListProperty = DependencyProperty
        .Register("AutoSuggestionList", typeof(List<string>), typeof(AutoCompletePartCodeTB),
            new FrameworkPropertyMetadata
            {
                DefaultValue = new List<string>(),
                DefaultUpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged,
            });

        // public static readonly DependencyProperty AutoSuggestionListProperty = DependencyProperty
        //.Register("AutoSuggestionList", typeof(ObservableCollection<HPPartNumbers>), typeof(AutoCompletePartCodeTB),
        //    new FrameworkPropertyMetadata
        //    {
        //        DefaultValue = new ObservableCollection<HPPartNumbers>(),
        //        DefaultUpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged,
        //    });

        public List<string> AutoSuggestionList
        {
            get
            {
                return (List<string>)this.GetValue(AutoSuggestionListProperty);
            }
            set { this.SetValue(AutoSuggestionListProperty, value); }
        }

        //public ObservableCollection<HPPartNumbers> AutoSuggestionList
        //{
        //    get
        //    {
        //        return (ObservableCollection<HPPartNumbers>)this.GetValue(AutoSuggestionListProperty);
        //    }
        //    set { this.SetValue(AutoSuggestionListProperty, value); }
        //}

        //TEXT SELECTED DEPENDANCY PROPERTY
        public static readonly DependencyProperty TextSelectedProperty = DependencyProperty
     .Register("TextSelected", typeof(string), typeof(AutoCompletePartCodeTB),
         new FrameworkPropertyMetadata
         {
             DefaultValue = "",
             DefaultUpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged,
         });

        //   public string TextSelected
        //   {
        //       get
        //       {
        //           return (string)this.GetValue(TextSelectedProperty);
        //       }
        //       set { this.SetValue(TextSelectedProperty, value);  }
        //   }

        public string TextBoxText
        {
            get { return (string)GetValue(TextBoxTextProperty); }
            set { SetValue(TextBoxTextProperty, value); }
        }

        //public static readonly DependencyProperty TextBoxTextProperty =
        //    DependencyProperty.Register("TextBoxText", typeof(string), typeof(AutoCompleteTextBoxUserControl), new UIPropertyMetadata(""));

        public static readonly DependencyProperty TextBoxTextProperty =
           DependencyProperty.Register("TextBoxText", typeof(string), typeof(AutoCompletePartCodeTB), new FrameworkPropertyMetadata
           {
               DefaultValue = "",
               DefaultUpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged,
           });


        // END WHOLE DEPENDANCY PROPERTY
        #endregion

        #region Open Auto Suggestion box method  

        /// <summary>  
        ///  Open Auto Suggestion box method  
        /// </summary>  
        private void OpenAutoSuggestionBox()
        {
            try
            {
                // Enable.  
                this.autoListPopup.Visibility = Visibility.Visible;
                this.autoListPopup.IsOpen = true;
                this.autoList.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                // Info.  
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Console.Write(ex);
            }
        }

        #endregion

        #region Close Auto Suggestion box method  

        /// <summary>  
        ///  Close Auto Suggestion box method  
        /// </summary>  
        private void CloseAutoSuggestionBox()
        {
            try
            {
                // Enable.  
                this.autoListPopup.Visibility = Visibility.Collapsed;
                this.autoListPopup.IsOpen = false;
                this.autoList.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                // Info.  
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Console.Write(ex);
            }
        }

        #endregion

        #region Auto Text Box text changed the method  

        /// <summary>  
        ///  Auto Text Box text changed method.  
        /// </summary>  
        /// <param name="sender">Sender parameter</param>  
        /// <param name="e">Event parameter</param>  
        private void AutoTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                // Verification.  
                if (string.IsNullOrEmpty(this.autoTextBox.Text))
                {
                    // Disable.  
                    this.CloseAutoSuggestionBox();

                    // Info.  
                    return;
                }

                // Enable.  
                this.OpenAutoSuggestionBox();

                // Settings.  
                this.autoList.ItemsSource = this.AutoSuggestionList.Where(p => p.ToLower().Contains(this.autoTextBox.Text.ToLower())).ToList();

                //this.autoList.ItemsSource = this.AutoSuggestionList.Where(p => p.SparePartNumber.ToLower().Contains(this.autoTextBox.Text.ToLower())).ToList();
                //Set Exposed Dependancy Property


            }
            catch (Exception ex)
            {
                // Info.  
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Console.Write(ex);
            }
        }

        #endregion

        #region Auto list selection changed method  

        /// <summary>  
        ///  Auto list selection changed method.  
        /// </summary>  
        /// <param name="sender">Sender parameter</param>  
        /// <param name="e">Event parameter</param>  
        private void AutoList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                // Verification.  
                if (this.autoList.SelectedIndex <= -1)
                {
                    // Disable.  
                    this.CloseAutoSuggestionBox();

                    // Info.  
                    return;
                }

                // Disable.  
                this.CloseAutoSuggestionBox();

                // Settings.

                this.autoTextBox.Text = this.autoList.SelectedItem.ToString();

                this.autoList.SelectedIndex = -1;

                TextBoxText = this.autoTextBox.Text;
            }
            catch (Exception ex)
            {
                // Info.  
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Console.Write(ex);
            }
        }

        #endregion
    }
}

