﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Helpers
{
    public static class DateTimeExtensions
    {
        //An extension to work out the start of the week
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }

        //public static DateTime StartOfMonth(this DateTime dt, DayOfWeek startOfWeek)
        //{
        //    int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
        //    return dt.AddDays(-1 * diff).Date;
        //}

        //Example Usage
        // DateTime dt = DateTime.Now.StartOfWeek(DayOfWeek.Monday);
    }
}
