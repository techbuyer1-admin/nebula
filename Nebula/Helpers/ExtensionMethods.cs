﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Markup;
using System.Xml;

namespace Nebula.Helpers
{
    public static class ExtensionMethods
    {
        public static T XamlClone<T>(this T original)
       where T : class
        {
            if (original == null)
                return null;

            object clone;
            using (var stream = new MemoryStream())
            {
                XamlWriter.Save(original, stream);
                stream.Seek(0, SeekOrigin.Begin);
                clone = XamlReader.Load(stream);
            }

            if (clone is T)
                return (T)clone;
            else
                return null;
        }


        public static string ToUpperCheckForNull(this string input)
        {

            string retval = input;

            if (!String.IsNullOrEmpty(retval))
            {
                retval = retval.ToUpper();
            }

            return retval;

        }

        public static string ToUpperCorrectCasing(this string text)
        {
            //  /[.:?!]\\s[a-z]/ matches letters following a space and punctuation,
            //  /^(?:\\s+)?[a-z]/  matches the first letter in a string (with optional leading spaces)
            Regex regexCasing = new Regex("(?:[.:?!]\\s[a-z]|^(?:\\s+)?[a-z])", RegexOptions.Multiline);
            

            //  First ensure all characters are lower case.  
            //  (In my case it comes all in caps; this line may be omitted depending upon your needs)        
            text = text.ToLower();

            //  Capitalize each match in the regular expression, using a lambda expression
            text = regexCasing.Replace(text, s => s.Value.ToUpper());

            //  Return the new string.
            return text;

        }

        public static string ToUpperUserFullName(this string text)
        {
            //  /[.:?!]\\s[a-z]/ matches letters following a space and punctuation,
            //  /^(?:\\s+)?[a-z]/  matches the first letter in a string (with optional leading spaces)
           
            Regex regexCasing = new Regex("(?:[.:?!][a-z]|^(?:\\s+)?[a-z])", RegexOptions.Multiline);

            //  First ensure all characters are lower case.  
            //  (In my case it comes all in caps; this line may be omitted depending upon your needs)        
            text = text.ToLower();

            //  Capitalize each match in the regular expression, using a lambda expression
            text = regexCasing.Replace(text, s => s.Value.ToUpper());

            //  Return the new string.
            return text;

        }



    }



  

}
