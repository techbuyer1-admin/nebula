﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Nebula.Helpers
{
    class Gen8Json
    {

    }
    

    public partial class HpGen8
    {
        [JsonProperty("@odata.context")]
        public string OdataContext { get; set; }

        [JsonProperty("@odata.id")]
        public string OdataId { get; set; }

        [JsonProperty("@odata.type")]
        public string OdataType { get; set; }

        [JsonProperty("Actions")]
        public HpGen8Actions Actions { get; set; }

        [JsonProperty("AssetTag")]
        public string AssetTag { get; set; }

        [JsonProperty("AvailableActions")]
        public AvailableAction[] AvailableActions { get; set; }

        [JsonProperty("Bios")]
        public HpGen8Bios Bios { get; set; }

        [JsonProperty("BiosVersion")]
        public string BiosVersion { get; set; }

        [JsonProperty("Boot")]
        public Boot Boot { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("EthernetInterfaces")]
        public Ces EthernetInterfaces { get; set; }

        [JsonProperty("HostCorrelation")]
        public HostCorrelation HostCorrelation { get; set; }

        [JsonProperty("HostName")]
        public string HostName { get; set; }

        //[JsonProperty("Id")]
        //[JsonConverter(typeof(ParseStringConverter))]
        public long Id { get; set; }

        [JsonProperty("IndicatorLED")]
        public string IndicatorLed { get; set; }

        [JsonProperty("LogServices")]
        public Ces LogServices { get; set; }

        [JsonProperty("Manufacturer")]
        public string Manufacturer { get; set; }

        [JsonProperty("Memory")]
        public Memory Memory { get; set; }

        [JsonProperty("MemorySummary")]
        public MemorySummary MemorySummary { get; set; }

        [JsonProperty("Model")]
        public string Model { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Oem")]
        public Oem Oem { get; set; }

        [JsonProperty("Power")]
        public string Power { get; set; }

        [JsonProperty("PowerState")]
        public string PowerState { get; set; }

        [JsonProperty("ProcessorSummary")]
        public ProcessorSummary ProcessorSummary { get; set; }

        [JsonProperty("Processors")]
        public Processors Processors { get; set; }

        [JsonProperty("SKU")]
        public string Sku { get; set; }

        [JsonProperty("SerialNumber")]
        public string SerialNumber { get; set; }

        [JsonProperty("Status")]
        public HpGen8Status Status { get; set; }

        [JsonProperty("SystemType")]
        public string SystemType { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("UUID")]
        public string Uuid { get; set; }

        [JsonProperty("links")]
        public HpGen8Links Links { get; set; }
    }

    public partial class HpGen8Actions
    {
        [JsonProperty("#ComputerSystem.Reset")]
        public SystemReset ComputerSystemReset { get; set; }
    }

    public partial class SystemReset
    {
        [JsonProperty("ResetType@Redfish.AllowableValues")]
        public string[] ResetTypeRedfishAllowableValues { get; set; }

        [JsonProperty("target")]
        public string Target { get; set; }
    }

    public partial class AvailableAction
    {
        [JsonProperty("Action")]
        public string Action { get; set; }

        [JsonProperty("Capabilities")]
        public Capability[] Capabilities { get; set; }
    }

    public partial class Capability
    {
        [JsonProperty("AllowableValues")]
        public string[] AllowableValues { get; set; }

        [JsonProperty("PropertyName")]
        public string PropertyName { get; set; }
    }

    public partial class HpGen8Bios
    {
        [JsonProperty("Current")]
        public Current Current { get; set; }
    }

    public partial class Current
    {
        [JsonProperty("VersionString")]
        public string VersionString { get; set; }
    }

    public partial class Boot
    {
        [JsonProperty("BootSourceOverrideEnabled")]
        public string BootSourceOverrideEnabled { get; set; }

        [JsonProperty("BootSourceOverrideSupported")]
        public string[] BootSourceOverrideSupported { get; set; }

        [JsonProperty("BootSourceOverrideTarget")]
        public string BootSourceOverrideTarget { get; set; }
    }

    public partial class Ces
    {
        [JsonProperty("@odata.id")]
        public string OdataId { get; set; }
    }

    public partial class HostCorrelation
    {
        [JsonProperty("HostMACAddress")]
        public string[] HostMacAddress { get; set; }

        [JsonProperty("HostName")]
        public string HostName { get; set; }

        [JsonProperty("IPAddress")]
        public string[] IpAddress { get; set; }
    }

    public partial class HpGen8Links
    {
        [JsonProperty("Chassis")]
        public EthernetInterfaces[] Chassis { get; set; }

        [JsonProperty("EthernetInterfaces")]
        public EthernetInterfaces EthernetInterfaces { get; set; }

        [JsonProperty("Logs")]
        public EthernetInterfaces Logs { get; set; }

        [JsonProperty("ManagedBy")]
        public EthernetInterfaces[] ManagedBy { get; set; }

        [JsonProperty("Processors")]
        public EthernetInterfaces Processors { get; set; }

        [JsonProperty("self")]
        public EthernetInterfaces Self { get; set; }
    }

    public partial class EthernetInterfaces
    {
        [JsonProperty("href")]
        public string Href { get; set; }
    }

    public partial class Memory
    {
        [JsonProperty("Status")]
        public MemoryStatus Status { get; set; }

        [JsonProperty("TotalSystemMemoryGB")]
        public long TotalSystemMemoryGb { get; set; }
    }

    public partial class MemoryStatus
    {
        [JsonProperty("HealthRollUp")]
        public string HealthRollUp { get; set; }
    }

    public partial class MemorySummary
    {
        [JsonProperty("Status")]
        public MemoryStatus Status { get; set; }

        [JsonProperty("TotalSystemMemoryGiB")]
        public long TotalSystemMemoryGiB { get; set; }
    }

    public partial class Oem
    {
        [JsonProperty("Hp")]
        public Hp Hp { get; set; }
    }

    public partial class Hp
    {
        [JsonProperty("@odata.type")]
        public string OdataType { get; set; }

        [JsonProperty("Actions")]
        public HpActions Actions { get; set; }

        [JsonProperty("AvailableActions")]
        public AvailableAction[] AvailableActions { get; set; }

        [JsonProperty("Bios")]
        public HpBios Bios { get; set; }

        [JsonProperty("DeviceDiscoveryComplete")]
        public DeviceDiscoveryComplete DeviceDiscoveryComplete { get; set; }

        [JsonProperty("IntelligentProvisioningIndex")]
        public long IntelligentProvisioningIndex { get; set; }

        [JsonProperty("IntelligentProvisioningLocation")]
        public string IntelligentProvisioningLocation { get; set; }

        [JsonProperty("IntelligentProvisioningVersion")]
        public string IntelligentProvisioningVersion { get; set; }

        [JsonProperty("PostState")]
        public string PostState { get; set; }

        [JsonProperty("PowerAllocationLimit")]
        public long PowerAllocationLimit { get; set; }

        [JsonProperty("PowerAutoOn")]
        public string PowerAutoOn { get; set; }

        [JsonProperty("PowerOnDelay")]
        public string PowerOnDelay { get; set; }

        [JsonProperty("PowerRegulatorMode")]
        public string PowerRegulatorMode { get; set; }

        [JsonProperty("PowerRegulatorModesSupported")]
        public string[] PowerRegulatorModesSupported { get; set; }

        [JsonProperty("TrustedModules")]
        public TrustedModule[] TrustedModules { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("VirtualProfile")]
        public string VirtualProfile { get; set; }

        [JsonProperty("links")]
        public HpLinks Links { get; set; }
    }

    public partial class HpActions
    {
        [JsonProperty("#HpComputerSystemExt.PowerButton")]
        public HpComputerSystemExtPowerButton HpComputerSystemExtPowerButton { get; set; }

        [JsonProperty("#HpComputerSystemExt.SystemReset")]
        public SystemReset HpComputerSystemExtSystemReset { get; set; }
    }

    public partial class HpComputerSystemExtPowerButton
    {
        [JsonProperty("PushType@Redfish.AllowableValues")]
        public string[] PushTypeRedfishAllowableValues { get; set; }

        [JsonProperty("target")]
        public string Target { get; set; }
    }

    public partial class HpBios
    {
        [JsonProperty("Backup")]
        public Backup Backup { get; set; }

        [JsonProperty("Bootblock")]
        public Backup Bootblock { get; set; }

        [JsonProperty("Current")]
        public Backup Current { get; set; }

        [JsonProperty("UefiClass")]
        public long UefiClass { get; set; }
    }

    public partial class Backup
    {
        [JsonProperty("Date")]
        public string Date { get; set; }

        [JsonProperty("Family")]
        public string Family { get; set; }

        [JsonProperty("VersionString")]
        public string VersionString { get; set; }
    }

    public partial class DeviceDiscoveryComplete
    {
        [JsonProperty("AMSDeviceDiscovery")]
        public string AmsDeviceDiscovery { get; set; }

        [JsonProperty("DeviceDiscovery")]
        public string DeviceDiscovery { get; set; }

        [JsonProperty("SmartArrayDiscovery")]
        public string SmartArrayDiscovery { get; set; }
    }

    public partial class HpLinks
    {
        [JsonProperty("BIOS")]
        public EthernetInterfaces Bios { get; set; }

        [JsonProperty("EthernetInterfaces")]
        public EthernetInterfaces EthernetInterfaces { get; set; }

        [JsonProperty("FirmwareInventory")]
        public EthernetInterfaces FirmwareInventory { get; set; }

        [JsonProperty("Memory")]
        public EthernetInterfaces Memory { get; set; }

        [JsonProperty("NetworkAdapters")]
        public EthernetInterfaces NetworkAdapters { get; set; }

        [JsonProperty("PCIDevices")]
        public EthernetInterfaces PciDevices { get; set; }

        [JsonProperty("PCISlots")]
        public EthernetInterfaces PciSlots { get; set; }

        [JsonProperty("SmartStorage")]
        public EthernetInterfaces SmartStorage { get; set; }

        [JsonProperty("SoftwareInventory")]
        public EthernetInterfaces SoftwareInventory { get; set; }
    }

    public partial class TrustedModule
    {
        [JsonProperty("Status")]
        public string Status { get; set; }
    }

    public partial class ProcessorSummary
    {
        [JsonProperty("Count")]
        public long Count { get; set; }

        [JsonProperty("Model")]
        public string Model { get; set; }

        [JsonProperty("Status")]
        public MemoryStatus Status { get; set; }
    }

    public partial class Processors
    {
        [JsonProperty("Count")]
        public long Count { get; set; }

        [JsonProperty("ProcessorFamily")]
        public string ProcessorFamily { get; set; }

        [JsonProperty("Status")]
        public MemoryStatus Status { get; set; }
    }

    public partial class HpGen8Status
    {
        [JsonProperty("Health")]
        public string Health { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }
    }


}
