﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Nebula.ViewModels;
using Nebula.Commands;

namespace Nebula.Helpers
{
    public class XMLTools
    {

        public XMLTools(MainViewModel passedvm)
        {
                
        }
        private static void WriteXMLDoc()
        {
           // new XDocument(new XElement("root", new XElement("someNode", "someValue"))).Save("foo.xml");
        }


        //SERIALIZE OBJECTS WRITE AND READ
        /// <summary>
        /// Serializes an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializableObject"></param>
        /// <param name="fileName"></param>
        public void SerializeObject<T>(T serializableObject, string fileName)
        {
            if (serializableObject == null) { return; }

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(serializableObject.GetType());
                using (MemoryStream stream = new MemoryStream())
                {
                    serializer.Serialize(stream, serializableObject);
                    stream.Position = 0;
                    xmlDocument.Load(stream);
                    xmlDocument.Save(fileName);
                }
            }
            catch (Exception ex)
            {
                //Log exception here
                Console.WriteLine(ex.Message);
            }
        }


        /// <summary>
        /// Deserializes an xml file into an object list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public T DeSerializeObject<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) { return default(T); }

            T objectOut = default(T);

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                string xmlString = xmlDocument.OuterXml;

                using (StringReader read = new StringReader(xmlString))
                {
                    Type outType = typeof(T);

                    XmlSerializer serializer = new XmlSerializer(outType);
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        objectOut = (T)serializer.Deserialize(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log exception here
                Console.WriteLine(ex.Message);
            }

            return objectOut;
        }










        //https://stackoverflow.com/questions/6115721/how-to-save-restore-serializable-object-to-from-file


        //WRITE TO BINARY

        /// <summary>
        /// Writes the given object instance to a binary file.
        /// <para>Object type (and all child types) must be decorated with the [Serializable] attribute.</para>
        /// <para>To prevent a variable from being serialized, decorate it with the [NonSerialized] attribute; cannot be applied to properties.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the binary file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the binary file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        public static void WriteToBinaryFile<T>(string filePath, T objectToWrite, bool append = false)
        {
            using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        /// <summary>
        /// Reads an object instance from a binary file.
        /// </summary>
        /// <typeparam name="T">The type of object to read from the binary file.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the binary file.</returns>
        public static T ReadFromBinaryFile<T>(string filePath)
        {
            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return (T)binaryFormatter.Deserialize(stream);
            }
        }

        //WRITE TO XML

        /// <summary>
        /// Writes the given object instance to an XML file.
        /// <para>Only Public properties and variables will be written to the file. These can be any type though, even other classes.</para>
        /// <para>If there are public properties/variables that you do not want written to the file, decorate them with the [XmlIgnore] attribute.</para>
        /// <para>Object type must have a parameterless constructor.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        public static void WriteToXmlFile<T>(string filePath, T objectToWrite, bool append = false) where T : new()
        {
            
            //Check if file already exists
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                //write the amended file
                TextWriter writer = null;
                try
                {
                    var serializer = new XmlSerializer(typeof(T));
                    writer = new StreamWriter(filePath, append);
                    serializer.Serialize(writer, objectToWrite);
                }
                finally
                {
                    if (writer != null)
                        writer.Close();
                }
            }
            else
            {

                TextWriter writer = null;
                try
                {
                    var serializer = new XmlSerializer(typeof(T));
                    writer = new StreamWriter(filePath, append);
                    serializer.Serialize(writer, objectToWrite);
                }
                finally
                {
                    if (writer != null)
                        writer.Close();
                }
            }


        }

        /// <summary>
        /// Reads an object instance from an XML file.
        /// <para>Object type must have a parameterless constructor.</para>
        /// </summary>
        /// <typeparam name="T">The type of object to read from the file.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the XML file.</returns>
        public static T ReadFromXmlFile<T>(string filePath) where T : new()
        {

            TextReader reader = null;
            XmlSerializer serializer = null;

            try
            {
                if (!string.IsNullOrEmpty(filePath))
                {
                    serializer = new XmlSerializer(typeof(T));
                    reader = new StreamReader(filePath);
                    return (T)serializer.Deserialize(reader);
                }

                return (T)serializer.Deserialize(reader);
            }
            catch (Exception ex)
            {
              
                Console.WriteLine(ex.Message);
                             
                return new T(); // (T)serializer.Deserialize(reader);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        /// <returns>Returns a string from an XML file.</returns>
        public static string ReadFromXmlFileAsString(string filePath)
        {
            TextReader reader = null;
            try
            {
                //var serializer = new XmlSerializer(typeof(T));
                reader = new StreamReader(filePath);
                return reader.ReadToEnd();
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        public static T ReadFromXmlString<T>(string text) where T : new()
        {
            TextReader reader = null;
            try
            {
              // MessageBox.Show(text);

               var serializer = new XmlSerializer(typeof(T));
               reader = new StreamReader(text);
               // Console.WriteLine(reader);
               return (T)serializer.Deserialize(reader);
                
            }
            catch (Exception ex)
            {
              //MessageBox.Show(ex.Message + "\n" + text); //" Server Health Data was unable to process due to the server being in post or powered off. Please click Get Server Info once the server has finished post.");

               return default;
               
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
           
        }

        public static T XmlParserGeneric<T>(string text)
        {
            TextReader reader = null;
            try
            {
              //  MessageBox.Show(text);

                var serializer = new XmlSerializer(typeof(T));
                reader = new StreamReader(text);
                // Console.WriteLine(reader);
                return (T)serializer.Deserialize(reader);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + text); //" Server Health Data was unable to process due to the server being in post or powered off. Please click Get Server Info once the server has finished post.");

                return default;

            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

        }

        //WRITE TO JSON
        // <summary>
        /// Writes the given object instance to a Json file.
        /// <para>Object type must have a parameterless constructor.</para>
        /// <para>Only Public properties and variables will be written to the file. These can be any type though, even other classes.</para>
        /// <para>If there are public properties/variables that you do not want written to the file, decorate them with the [JsonIgnore] attribute.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        //public static void WriteToJsonFile<T>(string filePath, T objectToWrite, bool append = false) where T : new()
        //{
        //    TextWriter writer = null;
        //    try
        //    {
        //        var contentsToWriteToFile = JsonConvert.SerializeObject(objectToWrite);
        //        writer = new StreamWriter(filePath, append);
        //        writer.Write(contentsToWriteToFile);
        //    }
        //    finally
        //    {
        //        if (writer != null)
        //            writer.Close();
        //    }
        //}

        /// <summary>
        /// Reads an object instance from an Json file.
        /// <para>Object type must have a parameterless constructor.</para>
        /// </summary>
        /// <typeparam name="T">The type of object to read from the file.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the Json file.</returns>
        //public static T ReadFromJsonFile<T>(string filePath) where T : new()
        //{
        //    TextReader reader = null;
        //    try
        //    {
        //        reader = new StreamReader(filePath);
        //        var fileContents = reader.ReadToEnd();
        //        return JsonConvert.DeserializeObject<T>(fileContents);
        //    }
        //    finally
        //    {
        //        if (reader != null)
        //            reader.Close();
        //    }
        //}



        //USAGE
        // Write the contents of the variable someClass to a file.
        //WriteToBinaryFile<SomeClass>("C:\someClass.txt", object1);


        //IMAGE TO BYTE ARRAY TO STORE IN XML

        //public static byte[] ConvertToBytes(BitmapImage bitmapImage)
        //{
        //    using (var ms = new MemoryStream())
        //    {
        //        var btmMap = new WriteableBitmap(bitmapImage.PixelWidth, bitmapImage.PixelHeight);

        //        // write an image into the stream
        //        btmMap.SaveJpeg(ms, bitmapImage.PixelWidth, bitmapImage.PixelHeight, 0, 100);

        //        return ms.ToArray();
        //    }
        //}







        // Read the file contents back into a variable.
        //SomeClass object1 = ReadFromBinaryFile<SomeClass>("C:\someClass.txt");


        private static void GenerateXml()
        {
            //XmlDocument doc = new XmlDocument();
            //XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            //doc.AppendChild(docNode);

            //XmlNode InvoiceAddressNode = doc.CreateElement("InvoiceAddress");
            //doc.AppendChild(InvoiceAddressNode);

            //XmlNode ContactNameNode = doc.CreateElement("address");
            //XmlAttribute productAttribute = doc.CreateAttribute("id");
            //productAttribute.Value = "01";
            //productNode.Attributes.Append(productAttribute);
            //productsNode.AppendChild(productNode);

            //XmlNode nameNode = doc.CreateElement("Name");
            //nameNode.AppendChild(doc.CreateTextNode("Java"));
            //productNode.AppendChild(nameNode);
            //XmlNode priceNode = doc.CreateElement("Price");
            //priceNode.AppendChild(doc.CreateTextNode("Free"));
            //productNode.AppendChild(priceNode);

            //// Create and add another product node.
            //productNode = doc.CreateElement("product");
            //productAttribute = doc.CreateAttribute("id");
            //productAttribute.Value = "02";
            //productNode.Attributes.Append(productAttribute);
            //productsNode.AppendChild(productNode);
            //nameNode = doc.CreateElement("Name");
            //nameNode.AppendChild(doc.CreateTextNode("C#"));
            //productNode.AppendChild(nameNode);
            //priceNode = doc.CreateElement("Price");
            //priceNode.AppendChild(doc.CreateTextNode("Free"));
            //productNode.AppendChild(priceNode);

            //doc.Save(Console.Out);
        }

        private static void ReadXml()
        {
            //XmlDocument doc = new XmlDocument();
            //XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            //doc.AppendChild(docNode);

            //XmlNode productsNode = doc.CreateElement("products");
            //doc.AppendChild(productsNode);

            //XmlNode productNode = doc.CreateElement("product");
            //XmlAttribute productAttribute = doc.CreateAttribute("id");
            //productAttribute.Value = "01";
            //productNode.Attributes.Append(productAttribute);
            //productsNode.AppendChild(productNode);

            //XmlNode nameNode = doc.CreateElement("Name");
            //nameNode.AppendChild(doc.CreateTextNode("Java"));
            //productNode.AppendChild(nameNode);
            //XmlNode priceNode = doc.CreateElement("Price");
            //priceNode.AppendChild(doc.CreateTextNode("Free"));
            //productNode.AppendChild(priceNode);

            //// Create and add another product node.
            //productNode = doc.CreateElement("product");
            //productAttribute = doc.CreateAttribute("id");
            //productAttribute.Value = "02";
            //productNode.Attributes.Append(productAttribute);
            //productsNode.AppendChild(productNode);
            //nameNode = doc.CreateElement("Name");
            //nameNode.AppendChild(doc.CreateTextNode("C#"));
            //productNode.AppendChild(nameNode);
            //priceNode = doc.CreateElement("Price");
            //priceNode.AppendChild(doc.CreateTextNode("Free"));
            //productNode.AppendChild(priceNode);

            //doc.Save(Console.Out);
        }

    }
}

