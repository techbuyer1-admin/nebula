﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Helpers
{
    [Serializable]
    public class DriveTestImportTotal : HasPropertyChanged
    {


        //to hold user value
        private string _TotalOnImport;
        public string TotalOnImport
        {
            get { return _TotalOnImport; }
            set
            {
                _TotalOnImport = value.ToUpper();
                OnPropertyChanged("TotalOnImport");
            }
        }
    }
}
