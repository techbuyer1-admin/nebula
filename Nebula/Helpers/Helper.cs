﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Nebula.Helpers
{
    [Serializable]
    public class Helper
    {
        // find the parent window of the controlHelper.GetDecendantFromName
        public static Window GetParentWindow(DependencyObject child)
        {
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            if (parentObject == null)
            {
                return null;
            }

            Window parent = parentObject as Window;
            if (parent != null)
            {
                return parent;
            }
            else
            {
                return GetParentWindow(parentObject);
            }
        }


        //TO find controls via the visual tree when multiple instances of view models are used and binding is not an option

        /// <summary>
        /// Finds a Child of a given item in the visual tree. 
        /// </summary>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="childName">x:Name or Name of child. </param>
        /// <returns>The first parent item that matches the submitted type parameter. 
        /// If not matching item can be found, 
        /// a null parent is being returned.</returns>
        public static T FindChild<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }



        /// <summary>
        /// //STATIC METHODS to call it you do it direct without making an instance  var frame = Helper.GetDescendantFromName(Application.Current.MainWindow, "ContentFrame") as ModernFrame;
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="name"></param>
        /// <returns></returns>
    public static FrameworkElement GetDescendantFromName(DependencyObject parent, string name)
    {
        var count = VisualTreeHelper.GetChildrenCount(parent);

        if (count < 1)
        {
            return null;
        }

        for (var i = 0; i < count; i++)
        {
            var frameworkElement = VisualTreeHelper.GetChild(parent, i) as FrameworkElement;
            if (frameworkElement != null)
            {
                if (frameworkElement.Name == name)
                {
                    return frameworkElement;
                }

                frameworkElement = GetDescendantFromName(frameworkElement, name);
                if (frameworkElement != null)
                {
                    return frameworkElement;
                }
            }
        }

        return null;
    }

        public static FlowDocument GetDescendantFlowDocFromName(DependencyObject parent, string name)
        {
            var count = VisualTreeHelper.GetChildrenCount(parent);

            if (count < 1)
            {
                return null;
            }

            for (var i = 0; i < count; i++)
            {
                var frameworkElement = VisualTreeHelper.GetChild(parent, i) as FlowDocument;
                if (frameworkElement != null)
                {
                    if (frameworkElement.Name == name)
                    {
                        return frameworkElement;
                    }

                    //frameworkElement = GetDescendantFromName(frameworkElement, name);
                    //if (frameworkElement != null)
                    //{
                    //    return frameworkElement;
                    //}
                }
            }

            return null;
        }


        // Use this to find all controls on a particular form or window.
        //public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        public static IEnumerable<T> FindControlsOfType<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindControlsOfType<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        public static IEnumerable<T> FindVisualParent<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject parent = VisualTreeHelper.GetParent(depObj);
                    if (parent != null && parent is T)
                    {
                        yield return (T)parent;
                    }

                    foreach (T parentOfParent in FindVisualParent<T>(parent))
                    {
                        yield return parentOfParent;
                    }
                }
            }
        }

        /// <summary>
        /// Looks for a child control within a parent by name
        /// </summary>
        public static DependencyObject FindChild(DependencyObject parent, string name)
        {
            // confirm parent and name are valid.
            if (parent == null || string.IsNullOrEmpty(name)) return null;

            if (parent is FrameworkElement && (parent as FrameworkElement).Name == name) return parent;

            DependencyObject result = null;

            if (parent is FrameworkElement) (parent as FrameworkElement).ApplyTemplate();

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                result = FindChild(child, name);
                if (result != null) break;
            }

            return result;
        }

        /// <summary>
        /// Looks for a child control within a parent by type
        /// </summary>
        public static T FindChild<T>(DependencyObject parent)
            where T : DependencyObject
        {
            // confirm parent is valid.
            if (parent == null) return null;
            if (parent is T) return parent as T;

            DependencyObject foundChild = null;

            if (parent is FrameworkElement) (parent as FrameworkElement).ApplyTemplate();

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                foundChild = FindChild<T>(child);
                if (foundChild != null) break;
            }

            return foundChild as T;
        }


        //public static DependencyObject FindChild(DependencyObject reference, string childName, Type childType)
        //{
        //    DependencyObject foundChild = null;
        //    if (reference != null)
        //    {
        //        int childrenCount = VisualTreeHelper.GetChildrenCount(reference);
        //        for (int i = 0; i < childrenCount; i++)
        //        {
        //            var child = VisualTreeHelper.GetChild(reference, i);
        //            // If the child is not of the request child type child
        //            if (child.GetType() != childType)
        //            {
        //                // recursively drill down the tree
        //                foundChild = FindChild(child, childName, childType);
        //            }
        //            else if (!string.IsNullOrEmpty(childName))
        //            {
        //                var frameworkElement = child as FrameworkElement;
        //                // If the child's name is set for search
        //                if (frameworkElement != null && frameworkElement.Name == childName)
        //                {
        //                    // if the child's name is of the request name
        //                    foundChild = child;
        //                    break;
        //                }
        //            }
        //            else
        //            {
        //                // child element found.
        //                foundChild = child;
        //                break;
        //            }
        //        }
        //    }
        //    return foundChild;
        //}




        //public static FrameworkElement GetDescendantFromNameGrp(DependencyObject parent, string name)
        //{
        //    var count = VisualTreeHelper.GetChildrenCount(parent);

        //    if (count < 1)
        //    {
        //        return null;
        //    }

        //    for (var i = 0; i < count; i++)
        //    {
        //        var frameworkElement = VisualTreeHelper.GetChild(parent, i) as FirstFloor.ModernUI.Presentation.LinkGroup;
        //        if (frameworkElement != null)
        //        {
        //            if (frameworkElement.Name == name)
        //            {
        //                return frameworkElement;
        //            }

        //            frameworkElement = GetDescendantFromName(frameworkElement, name);
        //            if (frameworkElement != null)
        //            {
        //                return frameworkElement;
        //            }
        //        }
        //    }

        //    return null;
        //}


    }
}
