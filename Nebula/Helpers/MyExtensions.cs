﻿using System;
using System.Text.RegularExpressions;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;

public static class MyExtensions
{
    public static bool Like(this string s, string pattern, RegexOptions options = RegexOptions.IgnoreCase)
    {
        return Regex.IsMatch(s, pattern, options);
    }


    //For Memory
    public enum SizeUnits
    {
        Byte, KB, MB, GB, TB, PB, EB, ZB, YB
    }

    public static string ToSize(double value, SizeUnits unit)
    {
       // return (value / (double)Math.Pow(1024, (Int64)unit)).ToString("0.00" + unit.ToString());

        return (value / 1024).ToString("0.0" + " GB");
    }
    //call above like string h = x.ToSize(MyExtension.SizeUnits.KB);

    //Bytes For File Sizes
    public static string ToFileSize(double bytesvalue)
    {
        string[] suffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        for (int i = 0; i < suffixes.Length; i++)
        {
            if (bytesvalue <= (Math.Pow(1024, i + 1)))
            {
                return ThreeNonZeroDigits(bytesvalue / Math.Pow(1024, i)) + " " + suffixes[i];
            }
        }

        return ThreeNonZeroDigits(bytesvalue / Math.Pow(1024, suffixes.Length - 1)) + " " + suffixes[suffixes.Length - 1];
    }

    private static string ThreeNonZeroDigits(double value)
    {
        if (value >= 100)
        {
            // No digits after the decimal.
            return value.ToString("0,0");
        }
        else if (value >= 10)
        {
            // One digit after the decimal.
            return value.ToString("0.0");
        }
        else
        {
            // Two digits after the decimal.
            return value.ToString("0.00");
        }
    }



    //3RD OPTION
    public static string ToPrettySize(this float Size)
    {
        return ConvertToPrettySize(Size, 0);
    }
    public static string ToPrettySize(this int Size)
    {
        return ConvertToPrettySize(Size, 0);
    }
    private static string ConvertToPrettySize(float Size, int R)
    {
        float F = Size / 1024f;
        if (F < 1)
        {
            switch (R)
            {
                case 0:
                    return string.Format("{0:0.00} byte", Size);
                case 1:
                    return string.Format("{0:0.00} kb", Size);
                case 2:
                    return string.Format("{0:0.00} mb", Size);
                case 3:
                    return string.Format("{0:0.00} gb", Size);
                case 4:
                    return string.Format("{0:0.00} tb", Size);
            }
        }
        return ConvertToPrettySize(F, ++R);
    }



    

}
