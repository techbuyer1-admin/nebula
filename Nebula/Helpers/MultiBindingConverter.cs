﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Nebula.Helpers
{
    public class FindCommandParameters
    {
        public string Property1 { get; set; }
        public string Property2 { get; set; }
    }

    public class MultiBindingConverter : IMultiValueConverter
    {
        //HELPFUL LINKS
        //http://blog.csainty.com/2009/12/wpf-multibinding-and.html
        //http://stackoverflow.com/questions/1350598/passing-two-command-parameters-using-a-wpf-binding
        //http://stackoverflow.com/questions/12601684/how-to-pass-multiple-parameters-as-commandparameter-in-invokecommandaction-in-wp
        //public object Convert(object[] values)
        //{
        //   return values.Clone();
        //}

        //public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        //{
        //    var findCommandParameters = new FindCommandParameters();
        //    findCommandParameters.Property1 = (string)values[0];
        //    findCommandParameters.Property1 = (string)values[1];
        //    return findCommandParameters;


        //    //return values.Clone();
        //}

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            //return String.Format("{0} {1}", values[0], values[1]).Clone();
            return values.Clone();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }




}// end ns