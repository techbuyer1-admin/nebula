﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;

namespace Nebula.Helpers
{
    public class DriveTestColourConvertor : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {

          
            string status = (value as string);
                if (status != null)
                {
                    if (status == "HDD" || status == "SSD" || status.Contains("SPP") || status.Contains("PASS") || status.Contains("Done")) return new SolidColorBrush(Colors.Green);
                    else if (status == "Not Redundant" || status == "InPost" || status == "Unknown" || status == "None" || status == "Not Installed" || status == "Warning") return new SolidColorBrush(Colors.Goldenrod);
                    else if (status == "Audible Griding Noise" || status == "Other" || status == "Bad Sectors" || status == "Cosmetic Damage" || status == "Damaged" || status == "Failed To Detect" || status == "Pre Wipe Failed" || status == "Drive Errors" || status == "Failed To Wipe" || status == "Low Health Below 70%" || status == "Predictive Failure" || status == "Remapped Sectors" || status == "Failed" || status == "Failed" || status == "FAIL" || status == "Error" || status == "PowerOff" || status == "Off") return new SolidColorBrush(Colors.OrangeRed);
                    else if (status == "MISSING" || status == "SERIAL MISMATCH") return new SolidColorBrush(Colors.Gold);
                    else if (status == "Informational" || status == "PENDING") return new SolidColorBrush(Colors.MediumPurple);


                    //if (status.All(char.IsDigit))
                    //{
                        int val;
                        bool number = int.TryParse(status,out val);

                        if(number == true)
                        {
                            if (val > 0) return new SolidColorBrush(Colors.Goldenrod);
                            if (val < 0) return new SolidColorBrush(Colors.Red);
                            if (val == 0) return new SolidColorBrush(Colors.Green);
                        }

                        //}
                }
                  

                return new SolidColorBrush(Colors.Transparent);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new SolidColorBrush(Colors.Transparent);
            }
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}