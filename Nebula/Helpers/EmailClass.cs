﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Nebula.Commands;
using Nebula.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using MsOutlook = Microsoft.Office.Interop.Outlook;
/// <summary>
/// Requires reference to Microsoft.Office.Interop.Outlook
/// </summary>

namespace Nebula.Helpers
{
    public class EmailClass
    {
        //viewmodels supported
        MainViewModel vm;
        DriveTestViewModel vm2;
        GoodsInOutViewModel vm3;

        public EmailClass()
        {

        }

        public EmailClass(MainViewModel passedvm)
        {

            vm = passedvm;
        }

        public EmailClass(DriveTestViewModel passedvm)
        {

            vm2 = passedvm;
        }

        public EmailClass(GoodsInOutViewModel passedvm)
        {

            vm3 = passedvm;
        }


        public enum MailSendType
        {
            SendDirect = 0,
            ShowModal = 1,
            ShowModeless = 2,
        }


        public async void ZipFolderAndSendMail()
        {

            string mydocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string zipName = "";
            //Get the root path location from current date
            // string rootpath = vm.GetFolderPath(DateTime.Now.ToShortDateString());
            //rootpath = mydocs + @"\";
            //MessageBox.Show(drives);



            using (System.Windows.Forms.FolderBrowserDialog openFolderDialog = new System.Windows.Forms.FolderBrowserDialog())
            {

                openFolderDialog.RootFolder = Environment.SpecialFolder.MyComputer;


                string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");

                //MessageBox.Show(Environment.SpecialFolder.MyDocuments.ToString());

                if (openFolderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {


                    // MessageBox.Show(openFolderDialog.SelectedPath);

                    zipName = ZipClass.CreateZip(openFolderDialog.SelectedPath, openFolderDialog.SelectedPath);

                    //Add attachments
                    string[] attachFiles = new string[]
                    {
                  @"" + zipName + @".zip"
                    };


                    // MessageBox.Show(@"" + zipName + @".zip");
                    //Create Outlook Object
                    //SendMailWithOutlook("Test Reports", "", "", attachFiles, MailSendType.ShowModal);
                    //Create Outlook Object
                    bool emailAttachSuccess = await System.Threading.Tasks.Task.Run(() => SendMailWithOutlook("Test Reports", "", "", attachFiles, MailSendType.ShowModal));


                    //  MessageBox.Show(emailAttachSuccess.ToString());

                    //Check if email attach was a success
                    if (emailAttachSuccess == true)
                    {
                        //Delete temp dir

                        if (Directory.Exists(zipName))
                        {
                            var files = Directory.GetFiles(zipName);
                            foreach (var file in files)
                            {
                                //Clear temp directory of files
                                if (File.Exists(file))
                                    File.Delete(file);
                            }

                            Directory.Delete(zipName);
                        }
                        //Delete temp Zip
                        // MessageBox.Show("@" + mydocs + @"\" + zipName + @".zip");
                        if (File.Exists(zipName + ".zip"))
                        {

                            File.Delete(zipName + ".zip");
                        }
                    }
                    else
                    {
                        //Launch file location
                        System.Diagnostics.Process.Start("explorer.exe", "/select, " + zipName + "");
                    }

                }// dialog ok




            }// end using
        }

        public async void SearchZipFolderAndSendMail(string pathtofolder)
        {

            string mydocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string zipName = "";
            //Get the root path location from current date
            // string rootpath = vm.GetFolderPath(DateTime.Now.ToShortDateString());
            //rootpath = mydocs + @"\";
            //MessageBox.Show(drives);



            //using (System.Windows.Forms.FolderBrowserDialog openFolderDialog = new System.Windows.Forms.FolderBrowserDialog())
            //{

            //openFolderDialog.RootFolder = Environment.SpecialFolder.MyComputer;


            string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");

            //MessageBox.Show(Environment.SpecialFolder.MyDocuments.ToString());

            //if (openFolderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{


            // MessageBox.Show(openFolderDialog.SelectedPath);

            zipName = ZipClass.CreateZip(pathtofolder, pathtofolder);

            //Add attachments
            string[] attachFiles = new string[]
            {
                  @"" + zipName + @".zip"
            };


            //Create Outlook Object
            bool emailAttachSuccess = await System.Threading.Tasks.Task.Run(() => SendMailWithOutlook("Test Reports", "", "", attachFiles, MailSendType.ShowModal));


            //  MessageBox.Show(emailAttachSuccess.ToString());

            //Check if email attach was a success
            if (emailAttachSuccess == true)
            {
                //Delete temp dir
                //MessageBox.Show(zipName);
                if (Directory.Exists(zipName))
                {
                    var files = Directory.GetFiles(zipName);
                    foreach (var file in files)
                    {
                        //Clear temp directory of files
                        if (File.Exists(file))
                            File.Delete(file);
                    }

                    Directory.Delete(zipName);
                }
                //Delete temp Zip
                // MessageBox.Show("@" + mydocs + @"\" + zipName + @".zip");
                if (File.Exists(zipName + ".zip"))
                {

                    File.Delete(zipName + ".zip");
                }


                //Trigger message to Drive Processing user
                if (vm2 != null)
                {
                    vm2.ProcessedMessageColour = Brushes.Green;
                    vm2.ProcessedMessage = "Reports Zipped & Attached successfully!";

                }

            }
            else
            {
                //If attachment fails due to size remove zip
                if (File.Exists(zipName + ".zip"))
                {

                    File.Delete(zipName + ".zip");
                }



                if (Directory.Exists(zipName))
                {

                    var files = Directory.GetFiles(zipName);

                    if (!Directory.Exists(zipName + @"\Reports"))
                        Directory.CreateDirectory(zipName + @"\Reports");

                    //MessageBox.Show("Check folder " + zipName + @"\Reports");

                    foreach (var file in files)
                    {
                        //Clear temp directory of files
                        if (File.Exists(file))
                        {
                            //Create sub directory
                            if (Directory.Exists(zipName + @"\Reports"))
                            {
                                //MessageBox.Show("Move FROM " + file + " TO " + file.Replace(zipName, zipName + @"\Reports"));

                                //Move files to sub folder, Replace old path to new path
                                File.Move(file, file.Replace(zipName, zipName + @"\Reports"));
                            }


                        }


                    }

                }

                //Move Report
                foreach (string attachedFile in attachFiles)
                {
                    if (attachedFile.Contains(".pdf"))
                    {
                        // MessageBox.Show("Move FROM " + attachedFile + " TO " + attachedFile.Replace(mydocs, zipName));

                        //Move report to sub folder, Replace old path to new path
                        File.Move(attachedFile, attachedFile.Replace(mydocs, zipName));
                    }
                }

                //Trigger message to Drive Processing user
                if (vm != null)
                {
                    vm2.ProcessedMessageColour = Brushes.Red;
                    vm2.ProcessedMessage = "Email Attachment failed! Attachment size exceeded the limit of Outlook 365.";
                }


                System.Diagnostics.Process.Start("explorer.exe", "/select, " + zipName + "");
            }


        }





        public async void SearchZipFolder(string pathtofolder, string reportPDFPath)
        {

            string mydocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string zipName = "";
            string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");


            zipName = ZipClass.CreateZip(pathtofolder, pathtofolder);

            //Add attachments
            string[] attachFiles = new string[]
            {
                  @"" + zipName + @".zip",
                  reportPDFPath
            };



            //Create Outlook Object
            bool emailAttachSuccess = await System.Threading.Tasks.Task.Run(() => SendMailWithOutlook("Test Reports", "", "", attachFiles, MailSendType.ShowModal));


            //  MessageBox.Show(emailAttachSuccess.ToString());

            //Check if email attach was a success
            if (emailAttachSuccess == true)
            {
                //Delete temp dir
                //MessageBox.Show(zipName);
                if (Directory.Exists(zipName))
                {
                    var files = Directory.GetFiles(zipName);
                    foreach (var file in files)
                    {
                        //Clear temp directory of files
                        if (File.Exists(file))
                            File.Delete(file);
                    }

                    Directory.Delete(zipName);
                }
                //Delete temp Zip
                // MessageBox.Show("@" + mydocs + @"\" + zipName + @".zip");
                if (File.Exists(zipName + ".zip"))
                {

                    File.Delete(zipName + ".zip");
                }

                //Trigger message to Drive Processing user
                if (vm2 != null)
                {
                    vm2.ProcessedMessageColour = Brushes.Green;
                    vm2.ProcessedMessage = "Reports Zipped & Attached successfully!";

                }


            }
            else
            {
                //If attachment fails due to size remove zip
                if (File.Exists(zipName + ".zip"))
                {

                    File.Delete(zipName + ".zip");
                }



                if (Directory.Exists(zipName))
                {

                    var files = Directory.GetFiles(zipName);

                    if (!Directory.Exists(zipName + @"\Reports"))
                        Directory.CreateDirectory(zipName + @"\Reports");

                    //MessageBox.Show("Check folder " + zipName + @"\Reports");

                    foreach (var file in files)
                    {
                        //Clear temp directory of files
                        if (File.Exists(file))
                        {
                            //Create sub directory
                            if (Directory.Exists(zipName + @"\Reports"))
                            {
                                //MessageBox.Show("Move FROM " + file + " TO " + file.Replace(zipName, zipName + @"\Reports"));

                                //Move files to sub folder, Replace old path to new path
                                File.Move(file, file.Replace(zipName, zipName + @"\Reports"));
                            }


                        }


                    }



                }

                //Move Report
                foreach (string attachedFile in attachFiles)
                {
                    if (attachedFile.Contains(".pdf"))
                    {
                        // MessageBox.Show("Move FROM " + attachedFile + " TO " + attachedFile.Replace(mydocs, zipName));

                        //Move report to sub folder, Replace old path to new path
                        File.Move(attachedFile, attachedFile.Replace(mydocs, zipName));
                    }
                }


                //Trigger message to Drive Processing user
                if (vm2 != null)
                {
                    vm2.ProcessedMessageColour = Brushes.Red;
                    vm2.ProcessedMessage = "Email Attachment failed!";
                }


                System.Diagnostics.Process.Start("explorer.exe", "/select, " + zipName + "");
            }

        }

        //Attach Standard PDF Report
        public void AttachFilesAndSendMail(string[] pathtoFile, string serialNumber)
        {

            string mydocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);


            string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");



            //Create Outlook Object
            SendMailWithOutlook(serialNumber + " Report", "", "", pathtoFile, MailSendType.ShowModal);

        }






        public bool SendMailWithOutlook(string subject, string htmlBody, string recipients, string[] filePaths, MailSendType sendType)
        {
            try
            {
                // create the outlook application.
                MsOutlook.Application outlookApp = new MsOutlook.Application();
                if (outlookApp == null)
                    return false;

                // create a new mail item.
                MsOutlook.MailItem mail = (MsOutlook.MailItem)outlookApp.CreateItem(MsOutlook.OlItemType.olMailItem);




                //Add attachments.
                if (filePaths != null)
                {
                    foreach (string file in filePaths)
                    {
                        //attach the file
                        // MessageBox.Show(file);
                        if (File.Exists(file))
                        {
                            MsOutlook.Attachment oAttach = mail.Attachments.Add(file);
                        }



                    }
                }

                mail.Subject = subject;
                mail.To = recipients;

                if (sendType == MailSendType.SendDirect)
                    mail.Send();
                else if (sendType == MailSendType.ShowModal)
                    mail.Display(true);
                else if (sendType == MailSendType.ShowModeless)
                    mail.Display(false);


                // set html body. 
                // add the body of the email
                mail.HTMLBody = mail.HTMLBody;

                mail = null;

                if (outlookApp != null)
                {
                    //outlookApp.Quit();
                    outlookApp = null;
                }


                return true;
            }
            catch (Exception ex)
            {


                //Check if error attachment too big
                if (ex.Message.Contains("server allows."))
                {

                    //Needs to call dialog on original thread
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        //Notify end user of attatchment size restriction
                        vm2.SystemMessageDialog("Email Exception Notification", ex.Message + "\n\nIf this is required to be emailed, please create smaller zip files & send on separate Emails. The folder location will now be shown.");
                    });



                    //Add additional message onto COM Exception
                    //MessageBox.Show(ex.Message + "\n\nIf this is required to be emailed please create smaller zip files & send on separate Emails. The folder location will now be shown.");
                }
                else
                {

                    //Return Exception
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        vm2.SystemMessageDialog("Email Exception Notification", ex.Message);
                        //MessageBox.Show(ex.Message);
                    });


                }


                return false;
            }
        }




        public bool DrivetestSendMailWithOutlook(string subject, string body, string recipients, string[] filePaths, MailSendType sendType)
        {
            try
            {
                // create the outlook application.
                MsOutlook.Application outlookApp = new MsOutlook.Application();
                if (outlookApp == null)
                    return false;

                // create a new mail item.
                MsOutlook.MailItem mail = (MsOutlook.MailItem)outlookApp.CreateItem(MsOutlook.OlItemType.olMailItem);




                //Add attachments.
                if (filePaths != null)
                {
                    foreach (string file in filePaths)
                    {
                        //attach the file
                        // MessageBox.Show(file);
                        if (File.Exists(file))
                        {
                            MsOutlook.Attachment oAttach = mail.Attachments.Add(file);
                        }



                    }
                }

                mail.Subject = subject;
                mail.To = recipients;
                mail.HTMLBody = mail.HTMLBody + body;

                if (sendType == MailSendType.SendDirect)
                    mail.Send();
                else if (sendType == MailSendType.ShowModal)
                    mail.Display(true);
                else if (sendType == MailSendType.ShowModeless)
                    mail.Display(false);


                // set html body. 
                // add the body of the email
                // assigning like this places the signature
                //mail.HTMLBody = mail.HTMLBody;






                mail = null;
                outlookApp = null;
                return true;
            }
            catch (Exception ex)
            {
                //Check if error attachment too big
                if (ex.Message.Contains("server allows."))
                {

                    //Needs to call dialog on original thread
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        //Notify end user of attatchment size restriction
                        vm2.SystemMessageDialog(ex.Message + "\n\nIf this is required to be emailed, please create smaller zip files & send on separate Emails. The folder location will now be shown.", "Email Exception Notification");
                    });


                    //Add additional message onto COM Exception
                    //MessageBox.Show(ex.Message + "\n\nIf this is required to be emailed please create smaller zip files & send on separate Emails. The folder location will now be shown.");
                }
                else
                {
                    //Return Exception
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        //Notify end user of attatchment size restriction
                        vm2.SystemMessageDialog(ex.Message, "Email Exception Notification");
                    });

                    //MessageBox.Show(ex.Message);
                }


                return false;
            }
        }


    }





}
