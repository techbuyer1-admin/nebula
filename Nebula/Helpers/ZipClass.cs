﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;
using System.Windows;
using System.IO;

namespace Nebula.Helpers
{

    public class ZipClass
    {

        public string dsktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);   //@"" + dsktop + @"
        public string myDocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);   //@"" + myDocs + @"


        public string StartPath { get; set; }
        public string ExtractPath { get; set; }

        public string ZipSavePath { get; set; }

        public ZipClass(string startpath, string extractpath, string z)
        {
            //StartPath = startpath;
            //ExtractPath = extractpath;


        }

        public static string CreateZip(string startPath, string zipPath)
        {
        string myDocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            try
            {

         


            //Extract folder name from path
            int lastSlash = startPath.LastIndexOf(@"\");

            //Create a local copy to my documents
            if(Directory.Exists(@"" + myDocs + @"\" + startPath.Substring(lastSlash + 1)))
            {

            }
            else
            {
                Directory.CreateDirectory(@"" + myDocs + @"\" + startPath.Substring(lastSlash + 1));
            }

            // LOOP THROUGH CONTENTS AND COPY ALL BUT PICTURES TO THE TEMPORARY DIRECTORY
            if(Directory.Exists(startPath))
            {

                var files = Directory.GetFiles(startPath);

                foreach(var file in files)
                {
                    if(file.Contains(".jpg")|| file.Contains(".JPG") || file.Contains(".jpeg") || file.Contains(".JPEG")
                       || file.Contains(".png") || file.Contains(".PNG")|| file.Contains(".gif") || file.Contains(".GIF") || file.Contains(".bmp") || file.Contains(".BMP"))
                    {

                    }
                    else
                    {
                        //get last index of backslash
                       int filelastslash = file.LastIndexOf(@"\");
                        // MessageBox.Show(file);
                        //MessageBox.Show(@"" + myDocs + @"\" + startPath.Substring(lastSlash + 1) + @"\" + file.Substring(filelastslash + 1));
                        if (File.Exists(file))
                        {
                            if (File.Exists(@"" + myDocs + @"\" + startPath.Substring(lastSlash + 1) + @"\" + file.Substring(filelastslash + 1)))
                            {

                            }
                            else
                            {
                                File.Copy(file, @"" + myDocs + @"\" + startPath.Substring(lastSlash + 1) + @"\" + file.Substring(filelastslash + 1));
                            }
                           
                        }
                       

                    }
                   
                }
            }


            //ORIGINAL CODE

            // Select a folder you want to zip its content
            //if(File.Exists(myDocs + @"\" + startPath.Substring(lastSlash + 1) + ".zip"))
            //    {
            //        //Delete file before copying
            //        File.Delete(myDocs + @"\" + startPath.Substring(lastSlash + 1) + ".zip");
            //        //Create ZIP
            //        ZipFile.CreateFromDirectory(startPath, myDocs + @"\" + startPath.Substring(lastSlash + 1) + ".zip", CompressionLevel.Fastest, false);
            //    }
            //    else
            //    {
            //        //Creat ZIP
            //        ZipFile.CreateFromDirectory(startPath, myDocs + @"\" + startPath.Substring(lastSlash + 1) + ".zip", CompressionLevel.Fastest, false);
            //    }

            //NEW CODE
            if (File.Exists(myDocs + @"\" + startPath.Substring(lastSlash + 1) + ".zip"))
            {
                //Delete file before copying
                File.Delete(myDocs + @"\" + startPath.Substring(lastSlash + 1) + ".zip");
                //Create ZIP
                ZipFile.CreateFromDirectory(@"" + myDocs + @"\" + startPath.Substring(lastSlash + 1), myDocs + @"\" + startPath.Substring(lastSlash + 1) + ".zip", CompressionLevel.Fastest, false);
            }
            else
            {
                //Creat ZIP
                ZipFile.CreateFromDirectory(@"" + myDocs + @"\" + startPath.Substring(lastSlash + 1), myDocs + @"\" + startPath.Substring(lastSlash + 1) + ".zip", CompressionLevel.Fastest, false);
            }



            return @"" + myDocs + @"\" + startPath.Substring(lastSlash + 1);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                return "";
            }
        }


        //private void CompressToZip(string fileName, Dictionary<string, byte[]> fileList)
        //{
        //    using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
        //    {
        //        foreach (var file in fileList)
        //        {
        //            var demoFile = archive.CreateEntry(file.Key);

        //            using (var entryStream = demoFile.Open())
        //            using (var b = new BinaryWriter(entryStream))
        //            {
        //                b.Write(file.Value);
        //            }
        //        }
        //    }
        //}




        public static void ExtractZip(string zipPath, string extractPath)
        {
            ZipFile.ExtractToDirectory(zipPath, extractPath);
        }


    }






}
