﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Nebula.Helpers
{
    public static class RichTextBoxExtensions
    {
        //WPF Rich Text Box extension method
        public static void AppendText(this RichTextBox box, string text, string color)
        {
            BrushConverter bc = new BrushConverter();
            TextRange tr = new TextRange(box.Document.ContentEnd, box.Document.ContentEnd);
            tr.Text = text;
            try
            {
                tr.ApplyPropertyValue(TextElement.ForegroundProperty,
                    bc.ConvertFromString(color));
            }
            catch (FormatException) { }
        }

        //how to call

       // myRichTextBox.AppendText("My text", "CornflowerBlue");

        //QUICKER METHOD
        //Paragraph paragraph = new Paragraph();
        //Run run = new Run("MyText");
        //paragraph.Inlines.Add(run);
        //myRichTextBox.Document.Blocks.Add(paragraph);

    }

}
