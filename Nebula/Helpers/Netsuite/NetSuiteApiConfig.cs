﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula.Helpers.Netsuite
{
    public class NetSuiteApiConfig : IApiConfig
    {
        public string AccountId { get; } = "5168576_SB2"; //<Enter NetSuite AccountID>

        public string ClientId { get; } = "a635b433f8b3431baebdb8570e6e65151663011a4ed017c2bc2e420d3b633702"; //<Enter Integration ClientId>
        public string ClientSecret { get; } = "04c242ab1cffdc59926e84284f7842cbaa2e1bdbe775b6a75bb11b656a5f9fc2"; //<Enter Integration ClientSecret>

        public string TokenId { get; } = "6377807003a9393c533041a3b782a1278ec2b056957a74128d0b826446c898d8"; //<Enter Access TokenId>
        public string TokenSecret { get; } = "7e83cc5c0602e06edbfe8d777616abb3956ce260a11f17d64dcc9d10b3dbeabd"; //<Enter Access TokenSecret>

        public string ApiRoot { get; } = $"https://5168576-sb2.suitetalk.api.netsuite.com/services/rest/record/v1"; //Just the api root.
        public string ApiSuiteQL { get; } = $"https://5168576-sb2.suitetalk.api.netsuite.com/services/rest/query/v1/suiteql"; //For Netsuites built in SQL
    }
}
