﻿using System.Text;
using System.Text.Json;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Diagnostics;
using System.Net.Http;
using Nebula.Helpers.Netsuite.Models;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nebula.Helpers.Netsuite
{
    public class NetSuiteApiClient_UsingHttpClient
    {
        //Static fields
        private static NetSuiteApiConfig _config;
        private static readonly HttpClient _httpClient;

        //Constructor Pull In Config Settings and create new Http Client
        static NetSuiteApiClient_UsingHttpClient()
        {
            _config = new NetSuiteApiConfig();
            _httpClient = new HttpClient();
            //Required for suiteql query endpoints
            _httpClient.DefaultRequestHeaders.Add("Prefer", "transient");
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        //Http Request 
        private HttpRequestMessage CreateHttpRequestMessage(HttpMethod httpMethod, string requestUrl)
        {
            // Include Authorisation method
            var oauth1 = new OAuth1HeaderGenerator(_config, httpMethod, requestUrl);
            // Create request
            var httpRequest = new HttpRequestMessage(httpMethod, requestUrl);
            httpRequest.Headers.Authorization = oauth1.CreateAuthenticationHeaderValue();
            

            //Return request
            return httpRequest;
        }

        // Find Customer ID's
        public async Task<List<string>> FindCustomerIds(int limit)
        {
            var url = _config.ApiRoot + "/customer?limit=" + limit;

            var httpRequest = CreateHttpRequestMessage(HttpMethod.Get, url);


            var httpResponse = await _httpClient.SendAsync(httpRequest);
            var responseJson = await httpResponse.Content.ReadAsStringAsync();

            var response = JsonSerializer.Deserialize<NsFindIdsResponse>(responseJson);

            return response.items.Select(i => i.id).ToList();
        }

        // Find Customer ID's Using SuiteQL
        public async Task<string> FindEmployeeIdsUsingSuiteQL(int limit)
        {
            var url = _config.ApiSuiteQL + "?limit=1000&offset=0"; //empty sting for limits and offsets if required

            var httpRequest = CreateHttpRequestMessage(HttpMethod.Post, url);

          

            ////https://www.linkedin.com/pulse/netsuite-release-20202-features-series-suiteql-rest-web-alvarez/
            ///
            ///https://netsuiteprofessionals.com/blog/question/installing-suiteql-query-tool/
            /////https://timdietrich.me/blog/netsuite-suiteql-query-api/

            //StringContent jsonContent = new StringContent("{\"reference\": \"PO10102\",\"operative\": \"Haylie Campbell\",\"status\": \"Backlog\"}");

            //StringContent jsonContent = new StringContent("{\"q\": \"SELECT Item.id, Item.itemid, Item.purchasedescription, Item.quantityonhand, Item.reorderpoint, ( Item.reorderpoint - Item.quantityonhand ) AS quantitytoorder, Item.cost, Item.leadtime, ItemVendor.vendor, Vendor.companyName AS VendorName FROM Item INNER JOIN ItemVendor ON ( ( ItemVendor.item = Item.id ) AND ( ItemVendor.preferredvendor = '\"'\"'T'\"'\"' ) ) INNER JOIN Vendor ON ( Vendor.id = ItemVendor.vendor ) WHERE ( reorderpoint > 0 ) AND ( quantityonhand <= reorderpoint ) ORDER BY Vendor.companyName ASC, Item.itemid ASC\"}");

            //StringContent jsonContent = new StringContent("{\"q\": \"SELECT employee.entityid AS entityidRAW, employee.firstname AS firstnameRAW, employee.lastname AS lastnameRAW FROM employee WHERE employee.lastname= 'Myers'\"}");

            //StringContent jsonContent = new StringContent("{\"q\": \"SELECT CONCAT(firstname,lastname) as fullname FROM employee\"}");

            StringContent jsonContent = new StringContent("{\"q\": \"SELECT Item.id, Item.itemid, Item.purchasedescription, Item.quantityonhand FROM Item\"}");


            //StringContent jsonContent = new StringContent("{\"q\": \"SELECT employee WHERE employee.entityid = 'E1376'\"}");

            //StringContent jsonContent = new StringContent("{}");

            jsonContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            //jsonContent.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            httpRequest.Content = jsonContent;

            //var httpResponse = await _httpClient.PostAsync(url, jsonContent);
            //var responseJson = await httpResponse.Content.ReadAsStringAsync();

            var httpResponse = await _httpClient.SendAsync(httpRequest);
            var responseJson = await httpResponse.Content.ReadAsStringAsync();


            //var response = JsonSerializer.Deserialize<string>(responseJson);
            System.Console.WriteLine(responseJson);
             return responseJson;
        
        }

        //Get Customer Infor from ID
        public async Task<NsCustomer> GetCustomer(int customerId)
        {
            var url = _config.ApiRoot + "/customer/" + customerId + "?fields=companyName,email,entityId";

            var httpRequest = CreateHttpRequestMessage(HttpMethod.Get, url);

            var httpResponse = await _httpClient.SendAsync(httpRequest);
            var responseJson = await httpResponse.Content.ReadAsStringAsync();

            System.Console.WriteLine(responseJson);

            var customer = JsonSerializer.Deserialize<NsCustomer>(responseJson);

            return customer;
        }

        //Get Customer Infor from ID String C1075 C1069
        public async Task<NsCustomer> GetCustomer(string customerId)
        {
            var url = _config.ApiRoot + "/entity/" + customerId;

            var httpRequest = CreateHttpRequestMessage(HttpMethod.Get, url);

            var httpResponse = await _httpClient.SendAsync(httpRequest);
            var responseJson = await httpResponse.Content.ReadAsStringAsync();

            System.Console.WriteLine(responseJson);

            var customer = JsonSerializer.Deserialize<NsCustomer>(responseJson);

            return customer;
        }


        public async Task<NsCustomer> GetPartCodes(string reqParams)
        {
            var url = _config.ApiRoot + "/inventoryItem/" + reqParams;

            var httpRequest = CreateHttpRequestMessage(HttpMethod.Get, url);

            var httpResponse = await _httpClient.SendAsync(httpRequest);
            var responseJson = await httpResponse.Content.ReadAsStringAsync();

            System.Console.WriteLine(responseJson);

            var customer = JsonSerializer.Deserialize<NsCustomer>(responseJson);

            return customer;
        }
    }
}
