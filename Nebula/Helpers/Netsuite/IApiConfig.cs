﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Nebula.Helpers.Netsuite
{
    public interface IApiConfig
    {
       string AccountId { get; }

       string ClientId { get; }
       string ClientSecret { get; }

       string TokenId { get; }
       string TokenSecret { get; }

       string ApiRoot { get; }

    }
}
