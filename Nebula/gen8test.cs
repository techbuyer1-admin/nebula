﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nebula
{
    public class Gen8test
    {




        // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class GET_EMBEDDED_HEALTH_DATA
        {

            private GET_EMBEDDED_HEALTH_DATAFAN[] fANSField;

            private GET_EMBEDDED_HEALTH_DATATEMP[] tEMPERATUREField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIES pOWER_SUPPLIESField;

            private object vRMField;

            private GET_EMBEDDED_HEALTH_DATAPROCESSOR[] pROCESSORSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORY mEMORYField;

            private GET_EMBEDDED_HEALTH_DATANIC[] nIC_INFORMATIONField;

            private GET_EMBEDDED_HEALTH_DATASTORAGE sTORAGEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATION fIRMWARE_INFORMATIONField;

            private GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCE hEALTH_AT_A_GLANCEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("FAN", IsNullable = false)]
            public GET_EMBEDDED_HEALTH_DATAFAN[] FANS
            {
                get
                {
                    return this.fANSField;
                }
                set
                {
                    this.fANSField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("TEMP", IsNullable = false)]
            public GET_EMBEDDED_HEALTH_DATATEMP[] TEMPERATURE
            {
                get
                {
                    return this.tEMPERATUREField;
                }
                set
                {
                    this.tEMPERATUREField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIES POWER_SUPPLIES
            {
                get
                {
                    return this.pOWER_SUPPLIESField;
                }
                set
                {
                    this.pOWER_SUPPLIESField = value;
                }
            }

            /// <remarks/>
            public object VRM
            {
                get
                {
                    return this.vRMField;
                }
                set
                {
                    this.vRMField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("PROCESSOR", IsNullable = false)]
            public GET_EMBEDDED_HEALTH_DATAPROCESSOR[] PROCESSORS
            {
                get
                {
                    return this.pROCESSORSField;
                }
                set
                {
                    this.pROCESSORSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORY MEMORY
            {
                get
                {
                    return this.mEMORYField;
                }
                set
                {
                    this.mEMORYField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("NIC", IsNullable = false)]
            public GET_EMBEDDED_HEALTH_DATANIC[] NIC_INFORMATION
            {
                get
                {
                    return this.nIC_INFORMATIONField;
                }
                set
                {
                    this.nIC_INFORMATIONField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGE STORAGE
            {
                get
                {
                    return this.sTORAGEField;
                }
                set
                {
                    this.sTORAGEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATION FIRMWARE_INFORMATION
            {
                get
                {
                    return this.fIRMWARE_INFORMATIONField;
                }
                set
                {
                    this.fIRMWARE_INFORMATIONField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCE HEALTH_AT_A_GLANCE
            {
                get
                {
                    return this.hEALTH_AT_A_GLANCEField;
                }
                set
                {
                    this.hEALTH_AT_A_GLANCEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFAN
        {

            private GET_EMBEDDED_HEALTH_DATAFANZONE zONEField;

            private GET_EMBEDDED_HEALTH_DATAFANLABEL lABELField;

            private GET_EMBEDDED_HEALTH_DATAFANSTATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATAFANSPEED sPEEDField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFANZONE ZONE
            {
                get
                {
                    return this.zONEField;
                }
                set
                {
                    this.zONEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFANLABEL LABEL
            {
                get
                {
                    return this.lABELField;
                }
                set
                {
                    this.lABELField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFANSTATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFANSPEED SPEED
            {
                get
                {
                    return this.sPEEDField;
                }
                set
                {
                    this.sPEEDField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFANZONE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFANLABEL
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFANSTATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFANSPEED
        {

            private byte vALUEField;

            private string uNITField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string UNIT
            {
                get
                {
                    return this.uNITField;
                }
                set
                {
                    this.uNITField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATATEMP
        {

            private GET_EMBEDDED_HEALTH_DATATEMPLABEL lABELField;

            private GET_EMBEDDED_HEALTH_DATATEMPLOCATION lOCATIONField;

            private GET_EMBEDDED_HEALTH_DATATEMPSTATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATATEMPCURRENTREADING cURRENTREADINGField;

            private GET_EMBEDDED_HEALTH_DATATEMPCAUTION cAUTIONField;

            private GET_EMBEDDED_HEALTH_DATATEMPCRITICAL cRITICALField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATATEMPLABEL LABEL
            {
                get
                {
                    return this.lABELField;
                }
                set
                {
                    this.lABELField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATATEMPLOCATION LOCATION
            {
                get
                {
                    return this.lOCATIONField;
                }
                set
                {
                    this.lOCATIONField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATATEMPSTATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATATEMPCURRENTREADING CURRENTREADING
            {
                get
                {
                    return this.cURRENTREADINGField;
                }
                set
                {
                    this.cURRENTREADINGField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATATEMPCAUTION CAUTION
            {
                get
                {
                    return this.cAUTIONField;
                }
                set
                {
                    this.cAUTIONField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATATEMPCRITICAL CRITICAL
            {
                get
                {
                    return this.cRITICALField;
                }
                set
                {
                    this.cRITICALField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATATEMPLABEL
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATATEMPLOCATION
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATATEMPSTATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATATEMPCURRENTREADING
        {

            private byte vALUEField;

            private string uNITField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string UNIT
            {
                get
                {
                    return this.uNITField;
                }
                set
                {
                    this.uNITField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATATEMPCAUTION
        {

            private byte vALUEField;

            private string uNITField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string UNIT
            {
                get
                {
                    return this.uNITField;
                }
                set
                {
                    this.uNITField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATATEMPCRITICAL
        {

            private byte vALUEField;

            private string uNITField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string UNIT
            {
                get
                {
                    return this.uNITField;
                }
                set
                {
                    this.uNITField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIES
        {

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARY pOWER_SUPPLY_SUMMARYField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLY[] sUPPLYField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARY POWER_SUPPLY_SUMMARY
            {
                get
                {
                    return this.pOWER_SUPPLY_SUMMARYField;
                }
                set
                {
                    this.pOWER_SUPPLY_SUMMARYField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("SUPPLY")]
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLY[] SUPPLY
            {
                get
                {
                    return this.sUPPLYField;
                }
                set
                {
                    this.sUPPLYField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARY
        {

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYPRESENT_POWER_READING pRESENT_POWER_READINGField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYPOWER_MANAGEMENT_CONTROLLER_FIRMWARE_VERSION pOWER_MANAGEMENT_CONTROLLER_FIRMWARE_VERSIONField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYPOWER_SYSTEM_REDUNDANCY pOWER_SYSTEM_REDUNDANCYField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYHP_POWER_DISCOVERY_SERVICES_REDUNDANCY_STATUS hP_POWER_DISCOVERY_SERVICES_REDUNDANCY_STATUSField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYHIGH_EFFICIENCY_MODE hIGH_EFFICIENCY_MODEField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYPRESENT_POWER_READING PRESENT_POWER_READING
            {
                get
                {
                    return this.pRESENT_POWER_READINGField;
                }
                set
                {
                    this.pRESENT_POWER_READINGField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYPOWER_MANAGEMENT_CONTROLLER_FIRMWARE_VERSION POWER_MANAGEMENT_CONTROLLER_FIRMWARE_VERSION
            {
                get
                {
                    return this.pOWER_MANAGEMENT_CONTROLLER_FIRMWARE_VERSIONField;
                }
                set
                {
                    this.pOWER_MANAGEMENT_CONTROLLER_FIRMWARE_VERSIONField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYPOWER_SYSTEM_REDUNDANCY POWER_SYSTEM_REDUNDANCY
            {
                get
                {
                    return this.pOWER_SYSTEM_REDUNDANCYField;
                }
                set
                {
                    this.pOWER_SYSTEM_REDUNDANCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYHP_POWER_DISCOVERY_SERVICES_REDUNDANCY_STATUS HP_POWER_DISCOVERY_SERVICES_REDUNDANCY_STATUS
            {
                get
                {
                    return this.hP_POWER_DISCOVERY_SERVICES_REDUNDANCY_STATUSField;
                }
                set
                {
                    this.hP_POWER_DISCOVERY_SERVICES_REDUNDANCY_STATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYHIGH_EFFICIENCY_MODE HIGH_EFFICIENCY_MODE
            {
                get
                {
                    return this.hIGH_EFFICIENCY_MODEField;
                }
                set
                {
                    this.hIGH_EFFICIENCY_MODEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYPRESENT_POWER_READING
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYPOWER_MANAGEMENT_CONTROLLER_FIRMWARE_VERSION
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYPOWER_SYSTEM_REDUNDANCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYHP_POWER_DISCOVERY_SERVICES_REDUNDANCY_STATUS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESPOWER_SUPPLY_SUMMARYHIGH_EFFICIENCY_MODE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLY
        {

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYLABEL lABELField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYPRESENT pRESENTField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYSTATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYPDS pDSField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYHOTPLUG_CAPABLE hOTPLUG_CAPABLEField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYMODEL mODELField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYSPARE sPAREField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYSERIAL_NUMBER sERIAL_NUMBERField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYCAPACITY cAPACITYField;

            private GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYFIRMWARE_VERSION fIRMWARE_VERSIONField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYLABEL LABEL
            {
                get
                {
                    return this.lABELField;
                }
                set
                {
                    this.lABELField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYPRESENT PRESENT
            {
                get
                {
                    return this.pRESENTField;
                }
                set
                {
                    this.pRESENTField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYSTATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYPDS PDS
            {
                get
                {
                    return this.pDSField;
                }
                set
                {
                    this.pDSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYHOTPLUG_CAPABLE HOTPLUG_CAPABLE
            {
                get
                {
                    return this.hOTPLUG_CAPABLEField;
                }
                set
                {
                    this.hOTPLUG_CAPABLEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYMODEL MODEL
            {
                get
                {
                    return this.mODELField;
                }
                set
                {
                    this.mODELField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYSPARE SPARE
            {
                get
                {
                    return this.sPAREField;
                }
                set
                {
                    this.sPAREField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYSERIAL_NUMBER SERIAL_NUMBER
            {
                get
                {
                    return this.sERIAL_NUMBERField;
                }
                set
                {
                    this.sERIAL_NUMBERField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYCAPACITY CAPACITY
            {
                get
                {
                    return this.cAPACITYField;
                }
                set
                {
                    this.cAPACITYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYFIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYLABEL
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYPRESENT
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYSTATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYPDS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYHOTPLUG_CAPABLE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYMODEL
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYSPARE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYSERIAL_NUMBER
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYCAPACITY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPOWER_SUPPLIESSUPPLYFIRMWARE_VERSION
        {

            private decimal valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public decimal Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPROCESSOR
        {

            private GET_EMBEDDED_HEALTH_DATAPROCESSORLABEL lABELField;

            private GET_EMBEDDED_HEALTH_DATAPROCESSORNAME nAMEField;

            private GET_EMBEDDED_HEALTH_DATAPROCESSORSTATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATAPROCESSORSPEED sPEEDField;

            private GET_EMBEDDED_HEALTH_DATAPROCESSOREXECUTION_TECHNOLOGY eXECUTION_TECHNOLOGYField;

            private GET_EMBEDDED_HEALTH_DATAPROCESSORMEMORY_TECHNOLOGY mEMORY_TECHNOLOGYField;

            private GET_EMBEDDED_HEALTH_DATAPROCESSORINTERNAL_L1_CACHE iNTERNAL_L1_CACHEField;

            private GET_EMBEDDED_HEALTH_DATAPROCESSORINTERNAL_L2_CACHE iNTERNAL_L2_CACHEField;

            private GET_EMBEDDED_HEALTH_DATAPROCESSORINTERNAL_L3_CACHE iNTERNAL_L3_CACHEField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPROCESSORLABEL LABEL
            {
                get
                {
                    return this.lABELField;
                }
                set
                {
                    this.lABELField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPROCESSORNAME NAME
            {
                get
                {
                    return this.nAMEField;
                }
                set
                {
                    this.nAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPROCESSORSTATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPROCESSORSPEED SPEED
            {
                get
                {
                    return this.sPEEDField;
                }
                set
                {
                    this.sPEEDField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPROCESSOREXECUTION_TECHNOLOGY EXECUTION_TECHNOLOGY
            {
                get
                {
                    return this.eXECUTION_TECHNOLOGYField;
                }
                set
                {
                    this.eXECUTION_TECHNOLOGYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPROCESSORMEMORY_TECHNOLOGY MEMORY_TECHNOLOGY
            {
                get
                {
                    return this.mEMORY_TECHNOLOGYField;
                }
                set
                {
                    this.mEMORY_TECHNOLOGYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPROCESSORINTERNAL_L1_CACHE INTERNAL_L1_CACHE
            {
                get
                {
                    return this.iNTERNAL_L1_CACHEField;
                }
                set
                {
                    this.iNTERNAL_L1_CACHEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPROCESSORINTERNAL_L2_CACHE INTERNAL_L2_CACHE
            {
                get
                {
                    return this.iNTERNAL_L2_CACHEField;
                }
                set
                {
                    this.iNTERNAL_L2_CACHEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAPROCESSORINTERNAL_L3_CACHE INTERNAL_L3_CACHE
            {
                get
                {
                    return this.iNTERNAL_L3_CACHEField;
                }
                set
                {
                    this.iNTERNAL_L3_CACHEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPROCESSORLABEL
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPROCESSORNAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPROCESSORSTATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPROCESSORSPEED
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPROCESSOREXECUTION_TECHNOLOGY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPROCESSORMEMORY_TECHNOLOGY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPROCESSORINTERNAL_L1_CACHE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPROCESSORINTERNAL_L2_CACHE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAPROCESSORINTERNAL_L3_CACHE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORY
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYADVANCED_MEMORY_PROTECTION aDVANCED_MEMORY_PROTECTIONField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARY mEMORY_DETAILS_SUMMARYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS mEMORY_DETAILSField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYADVANCED_MEMORY_PROTECTION ADVANCED_MEMORY_PROTECTION
            {
                get
                {
                    return this.aDVANCED_MEMORY_PROTECTIONField;
                }
                set
                {
                    this.aDVANCED_MEMORY_PROTECTIONField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARY MEMORY_DETAILS_SUMMARY
            {
                get
                {
                    return this.mEMORY_DETAILS_SUMMARYField;
                }
                set
                {
                    this.mEMORY_DETAILS_SUMMARYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS MEMORY_DETAILS
            {
                get
                {
                    return this.mEMORY_DETAILSField;
                }
                set
                {
                    this.mEMORY_DETAILSField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYADVANCED_MEMORY_PROTECTION
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYADVANCED_MEMORY_PROTECTIONAMP_MODE_STATUS aMP_MODE_STATUSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYADVANCED_MEMORY_PROTECTIONCONFIGURED_AMP_MODE cONFIGURED_AMP_MODEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYADVANCED_MEMORY_PROTECTIONAVAILABLE_AMP_MODES aVAILABLE_AMP_MODESField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYADVANCED_MEMORY_PROTECTIONAMP_MODE_STATUS AMP_MODE_STATUS
            {
                get
                {
                    return this.aMP_MODE_STATUSField;
                }
                set
                {
                    this.aMP_MODE_STATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYADVANCED_MEMORY_PROTECTIONCONFIGURED_AMP_MODE CONFIGURED_AMP_MODE
            {
                get
                {
                    return this.cONFIGURED_AMP_MODEField;
                }
                set
                {
                    this.cONFIGURED_AMP_MODEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYADVANCED_MEMORY_PROTECTIONAVAILABLE_AMP_MODES AVAILABLE_AMP_MODES
            {
                get
                {
                    return this.aVAILABLE_AMP_MODESField;
                }
                set
                {
                    this.aVAILABLE_AMP_MODESField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYADVANCED_MEMORY_PROTECTIONAMP_MODE_STATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYADVANCED_MEMORY_PROTECTIONCONFIGURED_AMP_MODE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYADVANCED_MEMORY_PROTECTIONAVAILABLE_AMP_MODES
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARY
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1 memory_Board_1Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2 memory_Board_2Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3 memory_Board_3Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4 memory_Board_4Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5 memory_Board_5Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6 memory_Board_6Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7 memory_Board_7Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8 memory_Board_8Field;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1 Memory_Board_1
            {
                get
                {
                    return this.memory_Board_1Field;
                }
                set
                {
                    this.memory_Board_1Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2 Memory_Board_2
            {
                get
                {
                    return this.memory_Board_2Field;
                }
                set
                {
                    this.memory_Board_2Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3 Memory_Board_3
            {
                get
                {
                    return this.memory_Board_3Field;
                }
                set
                {
                    this.memory_Board_3Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4 Memory_Board_4
            {
                get
                {
                    return this.memory_Board_4Field;
                }
                set
                {
                    this.memory_Board_4Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5 Memory_Board_5
            {
                get
                {
                    return this.memory_Board_5Field;
                }
                set
                {
                    this.memory_Board_5Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6 Memory_Board_6
            {
                get
                {
                    return this.memory_Board_6Field;
                }
                set
                {
                    this.memory_Board_6Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7 Memory_Board_7
            {
                get
                {
                    return this.memory_Board_7Field;
                }
                set
                {
                    this.memory_Board_7Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8 Memory_Board_8
            {
                get
                {
                    return this.memory_Board_8Field;
                }
                set
                {
                    this.memory_Board_8Field = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1NUMBER_OF_SOCKETS nUMBER_OF_SOCKETSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1TOTAL_MEMORY_SIZE tOTAL_MEMORY_SIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1OPERATING_FREQUENCY oPERATING_FREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1OPERATING_VOLTAGE oPERATING_VOLTAGEField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1NUMBER_OF_SOCKETS NUMBER_OF_SOCKETS
            {
                get
                {
                    return this.nUMBER_OF_SOCKETSField;
                }
                set
                {
                    this.nUMBER_OF_SOCKETSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1TOTAL_MEMORY_SIZE TOTAL_MEMORY_SIZE
            {
                get
                {
                    return this.tOTAL_MEMORY_SIZEField;
                }
                set
                {
                    this.tOTAL_MEMORY_SIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1OPERATING_FREQUENCY OPERATING_FREQUENCY
            {
                get
                {
                    return this.oPERATING_FREQUENCYField;
                }
                set
                {
                    this.oPERATING_FREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1OPERATING_VOLTAGE OPERATING_VOLTAGE
            {
                get
                {
                    return this.oPERATING_VOLTAGEField;
                }
                set
                {
                    this.oPERATING_VOLTAGEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1NUMBER_OF_SOCKETS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1TOTAL_MEMORY_SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1OPERATING_FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_1OPERATING_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2NUMBER_OF_SOCKETS nUMBER_OF_SOCKETSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2TOTAL_MEMORY_SIZE tOTAL_MEMORY_SIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2OPERATING_FREQUENCY oPERATING_FREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2OPERATING_VOLTAGE oPERATING_VOLTAGEField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2NUMBER_OF_SOCKETS NUMBER_OF_SOCKETS
            {
                get
                {
                    return this.nUMBER_OF_SOCKETSField;
                }
                set
                {
                    this.nUMBER_OF_SOCKETSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2TOTAL_MEMORY_SIZE TOTAL_MEMORY_SIZE
            {
                get
                {
                    return this.tOTAL_MEMORY_SIZEField;
                }
                set
                {
                    this.tOTAL_MEMORY_SIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2OPERATING_FREQUENCY OPERATING_FREQUENCY
            {
                get
                {
                    return this.oPERATING_FREQUENCYField;
                }
                set
                {
                    this.oPERATING_FREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2OPERATING_VOLTAGE OPERATING_VOLTAGE
            {
                get
                {
                    return this.oPERATING_VOLTAGEField;
                }
                set
                {
                    this.oPERATING_VOLTAGEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2NUMBER_OF_SOCKETS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2TOTAL_MEMORY_SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2OPERATING_FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_2OPERATING_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3NUMBER_OF_SOCKETS nUMBER_OF_SOCKETSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3TOTAL_MEMORY_SIZE tOTAL_MEMORY_SIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3OPERATING_FREQUENCY oPERATING_FREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3OPERATING_VOLTAGE oPERATING_VOLTAGEField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3NUMBER_OF_SOCKETS NUMBER_OF_SOCKETS
            {
                get
                {
                    return this.nUMBER_OF_SOCKETSField;
                }
                set
                {
                    this.nUMBER_OF_SOCKETSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3TOTAL_MEMORY_SIZE TOTAL_MEMORY_SIZE
            {
                get
                {
                    return this.tOTAL_MEMORY_SIZEField;
                }
                set
                {
                    this.tOTAL_MEMORY_SIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3OPERATING_FREQUENCY OPERATING_FREQUENCY
            {
                get
                {
                    return this.oPERATING_FREQUENCYField;
                }
                set
                {
                    this.oPERATING_FREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3OPERATING_VOLTAGE OPERATING_VOLTAGE
            {
                get
                {
                    return this.oPERATING_VOLTAGEField;
                }
                set
                {
                    this.oPERATING_VOLTAGEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3NUMBER_OF_SOCKETS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3TOTAL_MEMORY_SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3OPERATING_FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_3OPERATING_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4NUMBER_OF_SOCKETS nUMBER_OF_SOCKETSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4TOTAL_MEMORY_SIZE tOTAL_MEMORY_SIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4OPERATING_FREQUENCY oPERATING_FREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4OPERATING_VOLTAGE oPERATING_VOLTAGEField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4NUMBER_OF_SOCKETS NUMBER_OF_SOCKETS
            {
                get
                {
                    return this.nUMBER_OF_SOCKETSField;
                }
                set
                {
                    this.nUMBER_OF_SOCKETSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4TOTAL_MEMORY_SIZE TOTAL_MEMORY_SIZE
            {
                get
                {
                    return this.tOTAL_MEMORY_SIZEField;
                }
                set
                {
                    this.tOTAL_MEMORY_SIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4OPERATING_FREQUENCY OPERATING_FREQUENCY
            {
                get
                {
                    return this.oPERATING_FREQUENCYField;
                }
                set
                {
                    this.oPERATING_FREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4OPERATING_VOLTAGE OPERATING_VOLTAGE
            {
                get
                {
                    return this.oPERATING_VOLTAGEField;
                }
                set
                {
                    this.oPERATING_VOLTAGEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4NUMBER_OF_SOCKETS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4TOTAL_MEMORY_SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4OPERATING_FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_4OPERATING_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5NUMBER_OF_SOCKETS nUMBER_OF_SOCKETSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5TOTAL_MEMORY_SIZE tOTAL_MEMORY_SIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5OPERATING_FREQUENCY oPERATING_FREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5OPERATING_VOLTAGE oPERATING_VOLTAGEField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5NUMBER_OF_SOCKETS NUMBER_OF_SOCKETS
            {
                get
                {
                    return this.nUMBER_OF_SOCKETSField;
                }
                set
                {
                    this.nUMBER_OF_SOCKETSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5TOTAL_MEMORY_SIZE TOTAL_MEMORY_SIZE
            {
                get
                {
                    return this.tOTAL_MEMORY_SIZEField;
                }
                set
                {
                    this.tOTAL_MEMORY_SIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5OPERATING_FREQUENCY OPERATING_FREQUENCY
            {
                get
                {
                    return this.oPERATING_FREQUENCYField;
                }
                set
                {
                    this.oPERATING_FREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5OPERATING_VOLTAGE OPERATING_VOLTAGE
            {
                get
                {
                    return this.oPERATING_VOLTAGEField;
                }
                set
                {
                    this.oPERATING_VOLTAGEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5NUMBER_OF_SOCKETS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5TOTAL_MEMORY_SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5OPERATING_FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_5OPERATING_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6NUMBER_OF_SOCKETS nUMBER_OF_SOCKETSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6TOTAL_MEMORY_SIZE tOTAL_MEMORY_SIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6OPERATING_FREQUENCY oPERATING_FREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6OPERATING_VOLTAGE oPERATING_VOLTAGEField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6NUMBER_OF_SOCKETS NUMBER_OF_SOCKETS
            {
                get
                {
                    return this.nUMBER_OF_SOCKETSField;
                }
                set
                {
                    this.nUMBER_OF_SOCKETSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6TOTAL_MEMORY_SIZE TOTAL_MEMORY_SIZE
            {
                get
                {
                    return this.tOTAL_MEMORY_SIZEField;
                }
                set
                {
                    this.tOTAL_MEMORY_SIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6OPERATING_FREQUENCY OPERATING_FREQUENCY
            {
                get
                {
                    return this.oPERATING_FREQUENCYField;
                }
                set
                {
                    this.oPERATING_FREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6OPERATING_VOLTAGE OPERATING_VOLTAGE
            {
                get
                {
                    return this.oPERATING_VOLTAGEField;
                }
                set
                {
                    this.oPERATING_VOLTAGEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6NUMBER_OF_SOCKETS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6TOTAL_MEMORY_SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6OPERATING_FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_6OPERATING_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7NUMBER_OF_SOCKETS nUMBER_OF_SOCKETSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7TOTAL_MEMORY_SIZE tOTAL_MEMORY_SIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7OPERATING_FREQUENCY oPERATING_FREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7OPERATING_VOLTAGE oPERATING_VOLTAGEField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7NUMBER_OF_SOCKETS NUMBER_OF_SOCKETS
            {
                get
                {
                    return this.nUMBER_OF_SOCKETSField;
                }
                set
                {
                    this.nUMBER_OF_SOCKETSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7TOTAL_MEMORY_SIZE TOTAL_MEMORY_SIZE
            {
                get
                {
                    return this.tOTAL_MEMORY_SIZEField;
                }
                set
                {
                    this.tOTAL_MEMORY_SIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7OPERATING_FREQUENCY OPERATING_FREQUENCY
            {
                get
                {
                    return this.oPERATING_FREQUENCYField;
                }
                set
                {
                    this.oPERATING_FREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7OPERATING_VOLTAGE OPERATING_VOLTAGE
            {
                get
                {
                    return this.oPERATING_VOLTAGEField;
                }
                set
                {
                    this.oPERATING_VOLTAGEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7NUMBER_OF_SOCKETS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7TOTAL_MEMORY_SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7OPERATING_FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_7OPERATING_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8NUMBER_OF_SOCKETS nUMBER_OF_SOCKETSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8TOTAL_MEMORY_SIZE tOTAL_MEMORY_SIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8OPERATING_FREQUENCY oPERATING_FREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8OPERATING_VOLTAGE oPERATING_VOLTAGEField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8NUMBER_OF_SOCKETS NUMBER_OF_SOCKETS
            {
                get
                {
                    return this.nUMBER_OF_SOCKETSField;
                }
                set
                {
                    this.nUMBER_OF_SOCKETSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8TOTAL_MEMORY_SIZE TOTAL_MEMORY_SIZE
            {
                get
                {
                    return this.tOTAL_MEMORY_SIZEField;
                }
                set
                {
                    this.tOTAL_MEMORY_SIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8OPERATING_FREQUENCY OPERATING_FREQUENCY
            {
                get
                {
                    return this.oPERATING_FREQUENCYField;
                }
                set
                {
                    this.oPERATING_FREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8OPERATING_VOLTAGE OPERATING_VOLTAGE
            {
                get
                {
                    return this.oPERATING_VOLTAGEField;
                }
                set
                {
                    this.oPERATING_VOLTAGEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8NUMBER_OF_SOCKETS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8TOTAL_MEMORY_SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8OPERATING_FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS_SUMMARYMemory_Board_8OPERATING_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILS
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1[] memory_Board_1Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2[] memory_Board_2Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3[] memory_Board_3Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4[] memory_Board_4Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5[] memory_Board_5Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6[] memory_Board_6Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7[] memory_Board_7Field;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8[] memory_Board_8Field;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("Memory_Board_1")]
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1[] Memory_Board_1
            {
                get
                {
                    return this.memory_Board_1Field;
                }
                set
                {
                    this.memory_Board_1Field = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("Memory_Board_2")]
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2[] Memory_Board_2
            {
                get
                {
                    return this.memory_Board_2Field;
                }
                set
                {
                    this.memory_Board_2Field = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("Memory_Board_3")]
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3[] Memory_Board_3
            {
                get
                {
                    return this.memory_Board_3Field;
                }
                set
                {
                    this.memory_Board_3Field = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("Memory_Board_4")]
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4[] Memory_Board_4
            {
                get
                {
                    return this.memory_Board_4Field;
                }
                set
                {
                    this.memory_Board_4Field = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("Memory_Board_5")]
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5[] Memory_Board_5
            {
                get
                {
                    return this.memory_Board_5Field;
                }
                set
                {
                    this.memory_Board_5Field = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("Memory_Board_6")]
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6[] Memory_Board_6
            {
                get
                {
                    return this.memory_Board_6Field;
                }
                set
                {
                    this.memory_Board_6Field = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("Memory_Board_7")]
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7[] Memory_Board_7
            {
                get
                {
                    return this.memory_Board_7Field;
                }
                set
                {
                    this.memory_Board_7Field = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("Memory_Board_8")]
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8[] Memory_Board_8
            {
                get
                {
                    return this.memory_Board_8Field;
                }
                set
                {
                    this.memory_Board_8Field = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1SOCKET sOCKETField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1STATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1HP_SMART_MEMORY hP_SMART_MEMORYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1PART pARTField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1TYPE tYPEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1SIZE sIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1FREQUENCY fREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1MINIMUM_VOLTAGE mINIMUM_VOLTAGEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1RANKS rANKSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1TECHNOLOGY tECHNOLOGYField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1SOCKET SOCKET
            {
                get
                {
                    return this.sOCKETField;
                }
                set
                {
                    this.sOCKETField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1STATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1HP_SMART_MEMORY HP_SMART_MEMORY
            {
                get
                {
                    return this.hP_SMART_MEMORYField;
                }
                set
                {
                    this.hP_SMART_MEMORYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1PART PART
            {
                get
                {
                    return this.pARTField;
                }
                set
                {
                    this.pARTField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1TYPE TYPE
            {
                get
                {
                    return this.tYPEField;
                }
                set
                {
                    this.tYPEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1SIZE SIZE
            {
                get
                {
                    return this.sIZEField;
                }
                set
                {
                    this.sIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1FREQUENCY FREQUENCY
            {
                get
                {
                    return this.fREQUENCYField;
                }
                set
                {
                    this.fREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1MINIMUM_VOLTAGE MINIMUM_VOLTAGE
            {
                get
                {
                    return this.mINIMUM_VOLTAGEField;
                }
                set
                {
                    this.mINIMUM_VOLTAGEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1RANKS RANKS
            {
                get
                {
                    return this.rANKSField;
                }
                set
                {
                    this.rANKSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1TECHNOLOGY TECHNOLOGY
            {
                get
                {
                    return this.tECHNOLOGYField;
                }
                set
                {
                    this.tECHNOLOGYField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1SOCKET
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1STATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1HP_SMART_MEMORY
        {

            private string vALUEField;

            private string typeField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string Type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1PART
        {

            private string nUMBERField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string NUMBER
            {
                get
                {
                    return this.nUMBERField;
                }
                set
                {
                    this.nUMBERField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1TYPE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1MINIMUM_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1RANKS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_1TECHNOLOGY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2SOCKET sOCKETField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2STATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2HP_SMART_MEMORY hP_SMART_MEMORYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2PART pARTField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2TYPE tYPEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2SIZE sIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2FREQUENCY fREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2MINIMUM_VOLTAGE mINIMUM_VOLTAGEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2RANKS rANKSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2TECHNOLOGY tECHNOLOGYField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2SOCKET SOCKET
            {
                get
                {
                    return this.sOCKETField;
                }
                set
                {
                    this.sOCKETField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2STATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2HP_SMART_MEMORY HP_SMART_MEMORY
            {
                get
                {
                    return this.hP_SMART_MEMORYField;
                }
                set
                {
                    this.hP_SMART_MEMORYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2PART PART
            {
                get
                {
                    return this.pARTField;
                }
                set
                {
                    this.pARTField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2TYPE TYPE
            {
                get
                {
                    return this.tYPEField;
                }
                set
                {
                    this.tYPEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2SIZE SIZE
            {
                get
                {
                    return this.sIZEField;
                }
                set
                {
                    this.sIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2FREQUENCY FREQUENCY
            {
                get
                {
                    return this.fREQUENCYField;
                }
                set
                {
                    this.fREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2MINIMUM_VOLTAGE MINIMUM_VOLTAGE
            {
                get
                {
                    return this.mINIMUM_VOLTAGEField;
                }
                set
                {
                    this.mINIMUM_VOLTAGEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2RANKS RANKS
            {
                get
                {
                    return this.rANKSField;
                }
                set
                {
                    this.rANKSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2TECHNOLOGY TECHNOLOGY
            {
                get
                {
                    return this.tECHNOLOGYField;
                }
                set
                {
                    this.tECHNOLOGYField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2SOCKET
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2STATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2HP_SMART_MEMORY
        {

            private string vALUEField;

            private string typeField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string Type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2PART
        {

            private string nUMBERField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string NUMBER
            {
                get
                {
                    return this.nUMBERField;
                }
                set
                {
                    this.nUMBERField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2TYPE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2MINIMUM_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2RANKS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_2TECHNOLOGY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3SOCKET sOCKETField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3STATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3HP_SMART_MEMORY hP_SMART_MEMORYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3PART pARTField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3TYPE tYPEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3SIZE sIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3FREQUENCY fREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3MINIMUM_VOLTAGE mINIMUM_VOLTAGEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3RANKS rANKSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3TECHNOLOGY tECHNOLOGYField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3SOCKET SOCKET
            {
                get
                {
                    return this.sOCKETField;
                }
                set
                {
                    this.sOCKETField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3STATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3HP_SMART_MEMORY HP_SMART_MEMORY
            {
                get
                {
                    return this.hP_SMART_MEMORYField;
                }
                set
                {
                    this.hP_SMART_MEMORYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3PART PART
            {
                get
                {
                    return this.pARTField;
                }
                set
                {
                    this.pARTField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3TYPE TYPE
            {
                get
                {
                    return this.tYPEField;
                }
                set
                {
                    this.tYPEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3SIZE SIZE
            {
                get
                {
                    return this.sIZEField;
                }
                set
                {
                    this.sIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3FREQUENCY FREQUENCY
            {
                get
                {
                    return this.fREQUENCYField;
                }
                set
                {
                    this.fREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3MINIMUM_VOLTAGE MINIMUM_VOLTAGE
            {
                get
                {
                    return this.mINIMUM_VOLTAGEField;
                }
                set
                {
                    this.mINIMUM_VOLTAGEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3RANKS RANKS
            {
                get
                {
                    return this.rANKSField;
                }
                set
                {
                    this.rANKSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3TECHNOLOGY TECHNOLOGY
            {
                get
                {
                    return this.tECHNOLOGYField;
                }
                set
                {
                    this.tECHNOLOGYField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3SOCKET
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3STATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3HP_SMART_MEMORY
        {

            private string vALUEField;

            private string typeField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string Type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3PART
        {

            private string nUMBERField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string NUMBER
            {
                get
                {
                    return this.nUMBERField;
                }
                set
                {
                    this.nUMBERField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3TYPE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3MINIMUM_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3RANKS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_3TECHNOLOGY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4SOCKET sOCKETField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4STATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4HP_SMART_MEMORY hP_SMART_MEMORYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4PART pARTField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4TYPE tYPEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4SIZE sIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4FREQUENCY fREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4MINIMUM_VOLTAGE mINIMUM_VOLTAGEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4RANKS rANKSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4TECHNOLOGY tECHNOLOGYField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4SOCKET SOCKET
            {
                get
                {
                    return this.sOCKETField;
                }
                set
                {
                    this.sOCKETField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4STATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4HP_SMART_MEMORY HP_SMART_MEMORY
            {
                get
                {
                    return this.hP_SMART_MEMORYField;
                }
                set
                {
                    this.hP_SMART_MEMORYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4PART PART
            {
                get
                {
                    return this.pARTField;
                }
                set
                {
                    this.pARTField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4TYPE TYPE
            {
                get
                {
                    return this.tYPEField;
                }
                set
                {
                    this.tYPEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4SIZE SIZE
            {
                get
                {
                    return this.sIZEField;
                }
                set
                {
                    this.sIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4FREQUENCY FREQUENCY
            {
                get
                {
                    return this.fREQUENCYField;
                }
                set
                {
                    this.fREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4MINIMUM_VOLTAGE MINIMUM_VOLTAGE
            {
                get
                {
                    return this.mINIMUM_VOLTAGEField;
                }
                set
                {
                    this.mINIMUM_VOLTAGEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4RANKS RANKS
            {
                get
                {
                    return this.rANKSField;
                }
                set
                {
                    this.rANKSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4TECHNOLOGY TECHNOLOGY
            {
                get
                {
                    return this.tECHNOLOGYField;
                }
                set
                {
                    this.tECHNOLOGYField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4SOCKET
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4STATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4HP_SMART_MEMORY
        {

            private string vALUEField;

            private string typeField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string Type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4PART
        {

            private string nUMBERField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string NUMBER
            {
                get
                {
                    return this.nUMBERField;
                }
                set
                {
                    this.nUMBERField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4TYPE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4MINIMUM_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4RANKS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_4TECHNOLOGY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5SOCKET sOCKETField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5STATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5HP_SMART_MEMORY hP_SMART_MEMORYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5PART pARTField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5TYPE tYPEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5SIZE sIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5FREQUENCY fREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5MINIMUM_VOLTAGE mINIMUM_VOLTAGEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5RANKS rANKSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5TECHNOLOGY tECHNOLOGYField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5SOCKET SOCKET
            {
                get
                {
                    return this.sOCKETField;
                }
                set
                {
                    this.sOCKETField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5STATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5HP_SMART_MEMORY HP_SMART_MEMORY
            {
                get
                {
                    return this.hP_SMART_MEMORYField;
                }
                set
                {
                    this.hP_SMART_MEMORYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5PART PART
            {
                get
                {
                    return this.pARTField;
                }
                set
                {
                    this.pARTField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5TYPE TYPE
            {
                get
                {
                    return this.tYPEField;
                }
                set
                {
                    this.tYPEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5SIZE SIZE
            {
                get
                {
                    return this.sIZEField;
                }
                set
                {
                    this.sIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5FREQUENCY FREQUENCY
            {
                get
                {
                    return this.fREQUENCYField;
                }
                set
                {
                    this.fREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5MINIMUM_VOLTAGE MINIMUM_VOLTAGE
            {
                get
                {
                    return this.mINIMUM_VOLTAGEField;
                }
                set
                {
                    this.mINIMUM_VOLTAGEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5RANKS RANKS
            {
                get
                {
                    return this.rANKSField;
                }
                set
                {
                    this.rANKSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5TECHNOLOGY TECHNOLOGY
            {
                get
                {
                    return this.tECHNOLOGYField;
                }
                set
                {
                    this.tECHNOLOGYField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5SOCKET
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5STATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5HP_SMART_MEMORY
        {

            private string vALUEField;

            private string typeField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string Type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5PART
        {

            private string nUMBERField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string NUMBER
            {
                get
                {
                    return this.nUMBERField;
                }
                set
                {
                    this.nUMBERField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5TYPE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5MINIMUM_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5RANKS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_5TECHNOLOGY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6SOCKET sOCKETField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6STATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6HP_SMART_MEMORY hP_SMART_MEMORYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6PART pARTField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6TYPE tYPEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6SIZE sIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6FREQUENCY fREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6MINIMUM_VOLTAGE mINIMUM_VOLTAGEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6RANKS rANKSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6TECHNOLOGY tECHNOLOGYField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6SOCKET SOCKET
            {
                get
                {
                    return this.sOCKETField;
                }
                set
                {
                    this.sOCKETField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6STATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6HP_SMART_MEMORY HP_SMART_MEMORY
            {
                get
                {
                    return this.hP_SMART_MEMORYField;
                }
                set
                {
                    this.hP_SMART_MEMORYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6PART PART
            {
                get
                {
                    return this.pARTField;
                }
                set
                {
                    this.pARTField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6TYPE TYPE
            {
                get
                {
                    return this.tYPEField;
                }
                set
                {
                    this.tYPEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6SIZE SIZE
            {
                get
                {
                    return this.sIZEField;
                }
                set
                {
                    this.sIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6FREQUENCY FREQUENCY
            {
                get
                {
                    return this.fREQUENCYField;
                }
                set
                {
                    this.fREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6MINIMUM_VOLTAGE MINIMUM_VOLTAGE
            {
                get
                {
                    return this.mINIMUM_VOLTAGEField;
                }
                set
                {
                    this.mINIMUM_VOLTAGEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6RANKS RANKS
            {
                get
                {
                    return this.rANKSField;
                }
                set
                {
                    this.rANKSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6TECHNOLOGY TECHNOLOGY
            {
                get
                {
                    return this.tECHNOLOGYField;
                }
                set
                {
                    this.tECHNOLOGYField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6SOCKET
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6STATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6HP_SMART_MEMORY
        {

            private string vALUEField;

            private string typeField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string Type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6PART
        {

            private string nUMBERField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string NUMBER
            {
                get
                {
                    return this.nUMBERField;
                }
                set
                {
                    this.nUMBERField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6TYPE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6MINIMUM_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6RANKS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_6TECHNOLOGY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7SOCKET sOCKETField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7STATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7HP_SMART_MEMORY hP_SMART_MEMORYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7PART pARTField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7TYPE tYPEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7SIZE sIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7FREQUENCY fREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7MINIMUM_VOLTAGE mINIMUM_VOLTAGEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7RANKS rANKSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7TECHNOLOGY tECHNOLOGYField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7SOCKET SOCKET
            {
                get
                {
                    return this.sOCKETField;
                }
                set
                {
                    this.sOCKETField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7STATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7HP_SMART_MEMORY HP_SMART_MEMORY
            {
                get
                {
                    return this.hP_SMART_MEMORYField;
                }
                set
                {
                    this.hP_SMART_MEMORYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7PART PART
            {
                get
                {
                    return this.pARTField;
                }
                set
                {
                    this.pARTField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7TYPE TYPE
            {
                get
                {
                    return this.tYPEField;
                }
                set
                {
                    this.tYPEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7SIZE SIZE
            {
                get
                {
                    return this.sIZEField;
                }
                set
                {
                    this.sIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7FREQUENCY FREQUENCY
            {
                get
                {
                    return this.fREQUENCYField;
                }
                set
                {
                    this.fREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7MINIMUM_VOLTAGE MINIMUM_VOLTAGE
            {
                get
                {
                    return this.mINIMUM_VOLTAGEField;
                }
                set
                {
                    this.mINIMUM_VOLTAGEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7RANKS RANKS
            {
                get
                {
                    return this.rANKSField;
                }
                set
                {
                    this.rANKSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7TECHNOLOGY TECHNOLOGY
            {
                get
                {
                    return this.tECHNOLOGYField;
                }
                set
                {
                    this.tECHNOLOGYField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7SOCKET
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7STATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7HP_SMART_MEMORY
        {

            private string vALUEField;

            private string typeField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string Type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7PART
        {

            private string nUMBERField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string NUMBER
            {
                get
                {
                    return this.nUMBERField;
                }
                set
                {
                    this.nUMBERField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7TYPE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7MINIMUM_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7RANKS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_7TECHNOLOGY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8
        {

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8SOCKET sOCKETField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8STATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8HP_SMART_MEMORY hP_SMART_MEMORYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8PART pARTField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8TYPE tYPEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8SIZE sIZEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8FREQUENCY fREQUENCYField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8MINIMUM_VOLTAGE mINIMUM_VOLTAGEField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8RANKS rANKSField;

            private GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8TECHNOLOGY tECHNOLOGYField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8SOCKET SOCKET
            {
                get
                {
                    return this.sOCKETField;
                }
                set
                {
                    this.sOCKETField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8STATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8HP_SMART_MEMORY HP_SMART_MEMORY
            {
                get
                {
                    return this.hP_SMART_MEMORYField;
                }
                set
                {
                    this.hP_SMART_MEMORYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8PART PART
            {
                get
                {
                    return this.pARTField;
                }
                set
                {
                    this.pARTField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8TYPE TYPE
            {
                get
                {
                    return this.tYPEField;
                }
                set
                {
                    this.tYPEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8SIZE SIZE
            {
                get
                {
                    return this.sIZEField;
                }
                set
                {
                    this.sIZEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8FREQUENCY FREQUENCY
            {
                get
                {
                    return this.fREQUENCYField;
                }
                set
                {
                    this.fREQUENCYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8MINIMUM_VOLTAGE MINIMUM_VOLTAGE
            {
                get
                {
                    return this.mINIMUM_VOLTAGEField;
                }
                set
                {
                    this.mINIMUM_VOLTAGEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8RANKS RANKS
            {
                get
                {
                    return this.rANKSField;
                }
                set
                {
                    this.rANKSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8TECHNOLOGY TECHNOLOGY
            {
                get
                {
                    return this.tECHNOLOGYField;
                }
                set
                {
                    this.tECHNOLOGYField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8SOCKET
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8STATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8HP_SMART_MEMORY
        {

            private string vALUEField;

            private string typeField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string Type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8PART
        {

            private string nUMBERField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string NUMBER
            {
                get
                {
                    return this.nUMBERField;
                }
                set
                {
                    this.nUMBERField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8TYPE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8SIZE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8FREQUENCY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8MINIMUM_VOLTAGE
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8RANKS
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAMEMORYMEMORY_DETAILSMemory_Board_8TECHNOLOGY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATANIC
        {

            private GET_EMBEDDED_HEALTH_DATANICNETWORK_PORT nETWORK_PORTField;

            private GET_EMBEDDED_HEALTH_DATANICPORT_DESCRIPTION pORT_DESCRIPTIONField;

            private GET_EMBEDDED_HEALTH_DATANICLOCATION lOCATIONField;

            private GET_EMBEDDED_HEALTH_DATANICMAC_ADDRESS mAC_ADDRESSField;

            private GET_EMBEDDED_HEALTH_DATANICIP_ADDRESS iP_ADDRESSField;

            private GET_EMBEDDED_HEALTH_DATANICSTATUS sTATUSField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATANICNETWORK_PORT NETWORK_PORT
            {
                get
                {
                    return this.nETWORK_PORTField;
                }
                set
                {
                    this.nETWORK_PORTField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATANICPORT_DESCRIPTION PORT_DESCRIPTION
            {
                get
                {
                    return this.pORT_DESCRIPTIONField;
                }
                set
                {
                    this.pORT_DESCRIPTIONField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATANICLOCATION LOCATION
            {
                get
                {
                    return this.lOCATIONField;
                }
                set
                {
                    this.lOCATIONField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATANICMAC_ADDRESS MAC_ADDRESS
            {
                get
                {
                    return this.mAC_ADDRESSField;
                }
                set
                {
                    this.mAC_ADDRESSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATANICIP_ADDRESS IP_ADDRESS
            {
                get
                {
                    return this.iP_ADDRESSField;
                }
                set
                {
                    this.iP_ADDRESSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATANICSTATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATANICNETWORK_PORT
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATANICPORT_DESCRIPTION
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATANICLOCATION
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATANICMAC_ADDRESS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATANICIP_ADDRESS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATANICSTATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGE
        {

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLER cONTROLLERField;

            private GET_EMBEDDED_HEALTH_DATASTORAGEDISCOVERY_STATUS dISCOVERY_STATUSField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLER CONTROLLER
            {
                get
                {
                    return this.cONTROLLERField;
                }
                set
                {
                    this.cONTROLLERField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGEDISCOVERY_STATUS DISCOVERY_STATUS
            {
                get
                {
                    return this.dISCOVERY_STATUSField;
                }
                set
                {
                    this.dISCOVERY_STATUSField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLER
        {

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERLABEL lABELField;

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERSTATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERCONTROLLER_STATUS cONTROLLER_STATUSField;

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERSERIAL_NUMBER sERIAL_NUMBERField;

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERMODEL mODELField;

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERFW_VERSION fW_VERSIONField;

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERENCRYPTION_STATUS eNCRYPTION_STATUSField;

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERENCRYPTION_SELF_TEST_STATUS eNCRYPTION_SELF_TEST_STATUSField;

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERENCRYPTION_CSP_STATUS eNCRYPTION_CSP_STATUSField;

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERDRIVE_ENCLOSURE[] dRIVE_ENCLOSUREField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERLABEL LABEL
            {
                get
                {
                    return this.lABELField;
                }
                set
                {
                    this.lABELField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERSTATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERCONTROLLER_STATUS CONTROLLER_STATUS
            {
                get
                {
                    return this.cONTROLLER_STATUSField;
                }
                set
                {
                    this.cONTROLLER_STATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERSERIAL_NUMBER SERIAL_NUMBER
            {
                get
                {
                    return this.sERIAL_NUMBERField;
                }
                set
                {
                    this.sERIAL_NUMBERField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERMODEL MODEL
            {
                get
                {
                    return this.mODELField;
                }
                set
                {
                    this.mODELField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERFW_VERSION FW_VERSION
            {
                get
                {
                    return this.fW_VERSIONField;
                }
                set
                {
                    this.fW_VERSIONField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERENCRYPTION_STATUS ENCRYPTION_STATUS
            {
                get
                {
                    return this.eNCRYPTION_STATUSField;
                }
                set
                {
                    this.eNCRYPTION_STATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERENCRYPTION_SELF_TEST_STATUS ENCRYPTION_SELF_TEST_STATUS
            {
                get
                {
                    return this.eNCRYPTION_SELF_TEST_STATUSField;
                }
                set
                {
                    this.eNCRYPTION_SELF_TEST_STATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERENCRYPTION_CSP_STATUS ENCRYPTION_CSP_STATUS
            {
                get
                {
                    return this.eNCRYPTION_CSP_STATUSField;
                }
                set
                {
                    this.eNCRYPTION_CSP_STATUSField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("DRIVE_ENCLOSURE")]
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERDRIVE_ENCLOSURE[] DRIVE_ENCLOSURE
            {
                get
                {
                    return this.dRIVE_ENCLOSUREField;
                }
                set
                {
                    this.dRIVE_ENCLOSUREField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERLABEL
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERSTATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERCONTROLLER_STATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERSERIAL_NUMBER
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERMODEL
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERFW_VERSION
        {

            private decimal vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public decimal VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERENCRYPTION_STATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERENCRYPTION_SELF_TEST_STATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERENCRYPTION_CSP_STATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERDRIVE_ENCLOSURE
        {

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERDRIVE_ENCLOSURELABEL lABELField;

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERDRIVE_ENCLOSURESTATUS sTATUSField;

            private GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERDRIVE_ENCLOSUREDRIVE_BAY dRIVE_BAYField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERDRIVE_ENCLOSURELABEL LABEL
            {
                get
                {
                    return this.lABELField;
                }
                set
                {
                    this.lABELField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERDRIVE_ENCLOSURESTATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERDRIVE_ENCLOSUREDRIVE_BAY DRIVE_BAY
            {
                get
                {
                    return this.dRIVE_BAYField;
                }
                set
                {
                    this.dRIVE_BAYField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERDRIVE_ENCLOSURELABEL
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERDRIVE_ENCLOSURESTATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGECONTROLLERDRIVE_ENCLOSUREDRIVE_BAY
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGEDISCOVERY_STATUS
        {

            private GET_EMBEDDED_HEALTH_DATASTORAGEDISCOVERY_STATUSSTATUS sTATUSField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATASTORAGEDISCOVERY_STATUSSTATUS STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATASTORAGEDISCOVERY_STATUSSTATUS
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATION
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_1 iNDEX_1Field;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_2 iNDEX_2Field;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_3 iNDEX_3Field;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_4 iNDEX_4Field;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_5 iNDEX_5Field;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_6 iNDEX_6Field;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_7 iNDEX_7Field;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_8 iNDEX_8Field;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_9 iNDEX_9Field;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_10 iNDEX_10Field;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_11 iNDEX_11Field;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_12 iNDEX_12Field;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_13 iNDEX_13Field;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_1 INDEX_1
            {
                get
                {
                    return this.iNDEX_1Field;
                }
                set
                {
                    this.iNDEX_1Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_2 INDEX_2
            {
                get
                {
                    return this.iNDEX_2Field;
                }
                set
                {
                    this.iNDEX_2Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_3 INDEX_3
            {
                get
                {
                    return this.iNDEX_3Field;
                }
                set
                {
                    this.iNDEX_3Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_4 INDEX_4
            {
                get
                {
                    return this.iNDEX_4Field;
                }
                set
                {
                    this.iNDEX_4Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_5 INDEX_5
            {
                get
                {
                    return this.iNDEX_5Field;
                }
                set
                {
                    this.iNDEX_5Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_6 INDEX_6
            {
                get
                {
                    return this.iNDEX_6Field;
                }
                set
                {
                    this.iNDEX_6Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_7 INDEX_7
            {
                get
                {
                    return this.iNDEX_7Field;
                }
                set
                {
                    this.iNDEX_7Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_8 INDEX_8
            {
                get
                {
                    return this.iNDEX_8Field;
                }
                set
                {
                    this.iNDEX_8Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_9 INDEX_9
            {
                get
                {
                    return this.iNDEX_9Field;
                }
                set
                {
                    this.iNDEX_9Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_10 INDEX_10
            {
                get
                {
                    return this.iNDEX_10Field;
                }
                set
                {
                    this.iNDEX_10Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_11 INDEX_11
            {
                get
                {
                    return this.iNDEX_11Field;
                }
                set
                {
                    this.iNDEX_11Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_12 INDEX_12
            {
                get
                {
                    return this.iNDEX_12Field;
                }
                set
                {
                    this.iNDEX_12Field = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_13 INDEX_13
            {
                get
                {
                    return this.iNDEX_13Field;
                }
                set
                {
                    this.iNDEX_13Field = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_1
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_1FIRMWARE_NAME fIRMWARE_NAMEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_1FIRMWARE_VERSION fIRMWARE_VERSIONField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_1FIRMWARE_NAME FIRMWARE_NAME
            {
                get
                {
                    return this.fIRMWARE_NAMEField;
                }
                set
                {
                    this.fIRMWARE_NAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_1FIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_1FIRMWARE_NAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_1FIRMWARE_VERSION
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_2
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_2FIRMWARE_NAME fIRMWARE_NAMEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_2FIRMWARE_VERSION fIRMWARE_VERSIONField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_2FIRMWARE_NAME FIRMWARE_NAME
            {
                get
                {
                    return this.fIRMWARE_NAMEField;
                }
                set
                {
                    this.fIRMWARE_NAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_2FIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_2FIRMWARE_NAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_2FIRMWARE_VERSION
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_3
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_3FIRMWARE_NAME fIRMWARE_NAMEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_3FIRMWARE_VERSION fIRMWARE_VERSIONField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_3FIRMWARE_NAME FIRMWARE_NAME
            {
                get
                {
                    return this.fIRMWARE_NAMEField;
                }
                set
                {
                    this.fIRMWARE_NAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_3FIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_3FIRMWARE_NAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_3FIRMWARE_VERSION
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_4
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_4FIRMWARE_NAME fIRMWARE_NAMEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_4FIRMWARE_VERSION fIRMWARE_VERSIONField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_4FIRMWARE_NAME FIRMWARE_NAME
            {
                get
                {
                    return this.fIRMWARE_NAMEField;
                }
                set
                {
                    this.fIRMWARE_NAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_4FIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_4FIRMWARE_NAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_4FIRMWARE_VERSION
        {

            private byte vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_5
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_5FIRMWARE_NAME fIRMWARE_NAMEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_5FIRMWARE_VERSION fIRMWARE_VERSIONField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_5FIRMWARE_NAME FIRMWARE_NAME
            {
                get
                {
                    return this.fIRMWARE_NAMEField;
                }
                set
                {
                    this.fIRMWARE_NAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_5FIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_5FIRMWARE_NAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_5FIRMWARE_VERSION
        {

            private decimal vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public decimal VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_6
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_6FIRMWARE_NAME fIRMWARE_NAMEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_6FIRMWARE_VERSION fIRMWARE_VERSIONField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_6FIRMWARE_FAMILY fIRMWARE_FAMILYField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_6FIRMWARE_NAME FIRMWARE_NAME
            {
                get
                {
                    return this.fIRMWARE_NAMEField;
                }
                set
                {
                    this.fIRMWARE_NAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_6FIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_6FIRMWARE_FAMILY FIRMWARE_FAMILY
            {
                get
                {
                    return this.fIRMWARE_FAMILYField;
                }
                set
                {
                    this.fIRMWARE_FAMILYField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_6FIRMWARE_NAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_6FIRMWARE_VERSION
        {

            private decimal vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public decimal VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_6FIRMWARE_FAMILY
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_7
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_7FIRMWARE_NAME fIRMWARE_NAMEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_7FIRMWARE_VERSION fIRMWARE_VERSIONField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_7FIRMWARE_NAME FIRMWARE_NAME
            {
                get
                {
                    return this.fIRMWARE_NAMEField;
                }
                set
                {
                    this.fIRMWARE_NAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_7FIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_7FIRMWARE_NAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_7FIRMWARE_VERSION
        {

            private decimal vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public decimal VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_8
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_8FIRMWARE_NAME fIRMWARE_NAMEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_8FIRMWARE_VERSION fIRMWARE_VERSIONField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_8FIRMWARE_NAME FIRMWARE_NAME
            {
                get
                {
                    return this.fIRMWARE_NAMEField;
                }
                set
                {
                    this.fIRMWARE_NAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_8FIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_8FIRMWARE_NAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_8FIRMWARE_VERSION
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_9
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_9FIRMWARE_NAME fIRMWARE_NAMEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_9FIRMWARE_VERSION fIRMWARE_VERSIONField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_9FIRMWARE_NAME FIRMWARE_NAME
            {
                get
                {
                    return this.fIRMWARE_NAMEField;
                }
                set
                {
                    this.fIRMWARE_NAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_9FIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_9FIRMWARE_NAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_9FIRMWARE_VERSION
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_10
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_10FIRMWARE_NAME fIRMWARE_NAMEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_10FIRMWARE_VERSION fIRMWARE_VERSIONField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_10FIRMWARE_NAME FIRMWARE_NAME
            {
                get
                {
                    return this.fIRMWARE_NAMEField;
                }
                set
                {
                    this.fIRMWARE_NAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_10FIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_10FIRMWARE_NAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_10FIRMWARE_VERSION
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_11
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_11FIRMWARE_NAME fIRMWARE_NAMEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_11FIRMWARE_VERSION fIRMWARE_VERSIONField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_11FIRMWARE_NAME FIRMWARE_NAME
            {
                get
                {
                    return this.fIRMWARE_NAMEField;
                }
                set
                {
                    this.fIRMWARE_NAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_11FIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_11FIRMWARE_NAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_11FIRMWARE_VERSION
        {

            private decimal vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public decimal VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_12
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_12FIRMWARE_NAME fIRMWARE_NAMEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_12FIRMWARE_VERSION fIRMWARE_VERSIONField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_12FIRMWARE_NAME FIRMWARE_NAME
            {
                get
                {
                    return this.fIRMWARE_NAMEField;
                }
                set
                {
                    this.fIRMWARE_NAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_12FIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_12FIRMWARE_NAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_12FIRMWARE_VERSION
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_13
        {

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_13FIRMWARE_NAME fIRMWARE_NAMEField;

            private GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_13FIRMWARE_VERSION fIRMWARE_VERSIONField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_13FIRMWARE_NAME FIRMWARE_NAME
            {
                get
                {
                    return this.fIRMWARE_NAMEField;
                }
                set
                {
                    this.fIRMWARE_NAMEField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_13FIRMWARE_VERSION FIRMWARE_VERSION
            {
                get
                {
                    return this.fIRMWARE_VERSIONField;
                }
                set
                {
                    this.fIRMWARE_VERSIONField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_13FIRMWARE_NAME
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAFIRMWARE_INFORMATIONINDEX_13FIRMWARE_VERSION
        {

            private string vALUEField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string VALUE
            {
                get
                {
                    return this.vALUEField;
                }
                set
                {
                    this.vALUEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCE
        {

            private GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEBIOS_HARDWARE bIOS_HARDWAREField;

            private GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEFANS[] fANSField;

            private GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCETEMPERATURE tEMPERATUREField;

            private GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEPOWER_SUPPLIES[] pOWER_SUPPLIESField;

            private GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEPROCESSOR pROCESSORField;

            private GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEMEMORY mEMORYField;

            private GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCENETWORK nETWORKField;

            private GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCESTORAGE sTORAGEField;

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEBIOS_HARDWARE BIOS_HARDWARE
            {
                get
                {
                    return this.bIOS_HARDWAREField;
                }
                set
                {
                    this.bIOS_HARDWAREField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("FANS")]
            public GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEFANS[] FANS
            {
                get
                {
                    return this.fANSField;
                }
                set
                {
                    this.fANSField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCETEMPERATURE TEMPERATURE
            {
                get
                {
                    return this.tEMPERATUREField;
                }
                set
                {
                    this.tEMPERATUREField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("POWER_SUPPLIES")]
            public GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEPOWER_SUPPLIES[] POWER_SUPPLIES
            {
                get
                {
                    return this.pOWER_SUPPLIESField;
                }
                set
                {
                    this.pOWER_SUPPLIESField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEPROCESSOR PROCESSOR
            {
                get
                {
                    return this.pROCESSORField;
                }
                set
                {
                    this.pROCESSORField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEMEMORY MEMORY
            {
                get
                {
                    return this.mEMORYField;
                }
                set
                {
                    this.mEMORYField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCENETWORK NETWORK
            {
                get
                {
                    return this.nETWORKField;
                }
                set
                {
                    this.nETWORKField = value;
                }
            }

            /// <remarks/>
            public GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCESTORAGE STORAGE
            {
                get
                {
                    return this.sTORAGEField;
                }
                set
                {
                    this.sTORAGEField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEBIOS_HARDWARE
        {

            private string sTATUSField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEFANS
        {

            private string sTATUSField;

            private string rEDUNDANCYField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string REDUNDANCY
            {
                get
                {
                    return this.rEDUNDANCYField;
                }
                set
                {
                    this.rEDUNDANCYField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCETEMPERATURE
        {

            private string sTATUSField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEPOWER_SUPPLIES
        {

            private string sTATUSField;

            private string rEDUNDANCYField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string REDUNDANCY
            {
                get
                {
                    return this.rEDUNDANCYField;
                }
                set
                {
                    this.rEDUNDANCYField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEPROCESSOR
        {

            private string sTATUSField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCEMEMORY
        {

            private string sTATUSField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCENETWORK
        {

            private string sTATUSField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class GET_EMBEDDED_HEALTH_DATAHEALTH_AT_A_GLANCESTORAGE
        {

            private string sTATUSField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }
        }






    }

}


