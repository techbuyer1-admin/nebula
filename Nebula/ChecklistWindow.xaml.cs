﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Nebula.ViewModels;
using Nebula.Views;

namespace Nebula
{
    /// <summary>
    /// Interaction logic for ChecklistWindow.xaml
    /// </summary>
    public partial class ChecklistWindow : MetroWindow
    {

        GoodsInOutViewModel vm;

      
        public ChecklistWindow(GoodsInOutViewModel PassedViewModel)
        {
            InitializeComponent();

            vm = PassedViewModel;


            this.DataContext = vm;

        }
    }
}
