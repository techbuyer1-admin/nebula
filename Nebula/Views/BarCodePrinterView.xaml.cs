﻿using Microsoft.VisualBasic.FileIO;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Printing;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.Models;
using Nebula.Helpers;
using Nebula.Paginators;
using Nebula.Properties;
using Nebula.ViewModels;
using System.Windows.Forms.Integration;
using Microsoft.Office.Interop.Excel;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using System.Collections.ObjectModel;
using System.Data.SQLite;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for BarCodePrinterView.xaml
    /// </summary>
    /// 



    public partial class BarCodePrinterView : UserControl
    {
        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);
        //declare viewmodel
        MainViewModel vm;     //
        //string CodeType = "Barcode";
        string CodeType = "QRCode";
        //Application settings
        private Settings settings = Settings.Default;


        public BarCodePrinterView()
        {
            InitializeComponent();
        }

        public BarCodePrinterView(MainViewModel PassedViewModel)
        {
            InitializeComponent();

            vm = PassedViewModel;

            //Set Initial Dates for Label Search
            vm.DateFrom = DateTimeExtensions.StartOfWeek(DateTime.Now, DayOfWeek.Monday);
            vm.DateTo = DateTime.Now;



            //Colapse all labels
            StandardLabelZebra.Visibility = Visibility.Visible;
            AssetLabelZebra.Visibility = Visibility.Collapsed;
            MediumLabelZebra.Visibility = Visibility.Collapsed;
            LargeLabelZebra.Visibility = Visibility.Collapsed;
            StandardLabelDYMO.Visibility = Visibility.Collapsed;
            StandardLabelBROTHER.Visibility = Visibility.Collapsed;
            CustomLabelZebra.Visibility = Visibility.Collapsed;
            CustomLabelDYMO.Visibility = Visibility.Collapsed;
            CustomLabelBROTHER.Visibility = Visibility.Collapsed;

            vm.SelectedBarcode = "Zebra - Standard Label";

            vm.QRPartSubTitle = "Part No";
            vm.QRInfoSubTitle = "Meta";
            vm.ShowInfoQR = Visibility.Collapsed;
            vm.ShowSerialQR = Visibility.Collapsed;
            vm.ShowPartQR = Visibility.Collapsed;
            vm.EraseITIconVisibility = Visibility.Collapsed;

            //Set the label with an offset
            //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, -30);
            //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, -30);
            //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, -30);
            //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, -30);
            //StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, -30);

            StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
            AssetLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
            MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
            LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
            StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
            StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
            CustomLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
            CustomLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
            CustomLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);

            //Clear Serial Number Text Box
            SerialNoTXTB.Text = "";
            //CLEAR ORDER REF
            vm.OrderRefNo = "";

            //set default values for barcode height and barcode bar width
            vm.BarcodeHeight = 40;
            vm.BarcodeBarWidth = 1;

            if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\Inventory.csv"))
            {

            }
            else
            {
                //File.Copy(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\Inventory.csv", vm.myDocs + @"\Inventory.csv");
            }

            //Copy the excel with query to netsuite local to user
            if (File.Exists(@"" + vm.myDocs + @"\NS Item WQ.xlsx"))
            {





                //call async function
                GetExcelData();

            }
            else
            {


                if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\NS Item WQ.xlsx"))
                {
                    File.Copy(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\NS Item WQ.xlsx", @"" + vm.myDocs + @"\NS Item WQ.xlsx");
                    //call async function
                    GetExcelData();



                }
                else
                {
                    File.Copy(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\NS Item WQ.xlsx", @"" + vm.myDocs + @"\NS Item WQ.xlsx");



                    //call async function
                    GetExcelData();

                }




            }


            //Readin Printers
            PopulateInstalledPrintersCombo();


            R2ZebSTD.Visibility = Visibility.Collapsed;
            R2ZebMED.Visibility = Visibility.Collapsed;
            R2ZebLRG.Visibility = Visibility.Collapsed;
            R2DymoSTD.Visibility = Visibility.Collapsed;
            R2BroSTD.Visibility = Visibility.Collapsed;
            //Custom labels
            R2ZebCUS.Visibility = Visibility.Collapsed;
            R2DymoCUS.Visibility = Visibility.Collapsed;
            R2BroCUS.Visibility = Visibility.Collapsed;


            //R2 Check, Exclude UK Itad, purely for C1
            if (vm.CurrentUserLocation == "United Kingdom")
            {
             
                foreach (var ptr in DefaultPrinterCB.Items)
                {
                    //Console.WriteLine(ptr);
                    //Capture UKCLS
                    if (ptr.ToString().Contains("ZT230-FrontWH") || ptr.ToString().Contains(@"\\ukcls") || ptr.ToString().Contains(@"\\sageserver") || ptr.ToString().Contains(@"GB-HGC1"))
                    {
                        R2ZebSTD.Visibility = Visibility.Visible;
                        R2ZebMED.Visibility = Visibility.Visible;
                        R2ZebLRG.Visibility = Visibility.Visible;
                        R2DymoSTD.Visibility = Visibility.Visible;
                        R2BroSTD.Visibility = Visibility.Visible;
                        //Custom labels
                        R2ZebCUS.Visibility = Visibility.Visible;
                        R2DymoCUS.Visibility = Visibility.Visible;
                        R2BroCUS.Visibility = Visibility.Visible;
                    }
                    else if (ptr.ToString().Contains("ITAD") || ptr.ToString().Contains("GB-ITAD"))
                    {
                        R2ZebSTD.Visibility = Visibility.Collapsed;
                        R2ZebMED.Visibility = Visibility.Collapsed;
                        R2ZebLRG.Visibility = Visibility.Collapsed;
                        R2DymoSTD.Visibility = Visibility.Collapsed;
                        R2BroSTD.Visibility = Visibility.Collapsed;
                        //Custom labels
                        R2ZebCUS.Visibility = Visibility.Collapsed;
                        R2DymoCUS.Visibility = Visibility.Collapsed;
                        R2BroCUS.Visibility = Visibility.Collapsed;
                    }
                }
            }

            

            //Load Settings
            LoadSettings();


            //ADD 600 TO THE NUMBER OF COPIES
            for (int i = 1; i < 601; i++)
            {
                NumberOfCopiesCB.Items.Add(i);
            }








        }






        private void LoadSettings()
        {

            if (DefaultPrinterCB.Items.Count > 0)
            {

                //DefaultPrinterCB.Text = settings.DefaultLabelPrinter;
                DefaultPrinterCB.SelectedIndex = DefaultPrinterCB.Items.IndexOf(settings.DefaultLabelPrinter);
            }


        }

        private void SaveSettings()
        {
            if (DefaultPrinterCB.SelectedItem.ToString() != string.Empty || DefaultPrinterCB.SelectedItem.ToString() != "")
            {

                settings.DefaultLabelPrinter = DefaultPrinterCB.SelectedItem.ToString();


                settings.Save();
            }

        }

        private void DefaultPrinterCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void SetPrinterBTN_Click(object sender, RoutedEventArgs e)
        {

            //MessageBox.Show(DefaultPrinterCB.Text);
            SaveSettings();

            // MessageBox.Show(settings.DefaultLabelPrinter);
        }

        private void PopulateInstalledPrintersCombo()
        {
            // Add list of installed printers found to the combo box.
            // The pkInstalledPrinters string will be used to provide the display string.
            try
            {

           

            string pkInstalledPrinters;
            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                pkInstalledPrinters = PrinterSettings.InstalledPrinters[i];

                //Console.WriteLine(pkInstalledPrinters);

                if (pkInstalledPrinters.Contains("Zebra") || pkInstalledPrinters.Contains("zebra") || pkInstalledPrinters.Contains("ZT") || pkInstalledPrinters.Contains("ZDesigner") || pkInstalledPrinters.Contains("DYMO") || pkInstalledPrinters.Contains("PDF") || pkInstalledPrinters.Contains("BROTHER") || pkInstalledPrinters.Contains("Brother") || pkInstalledPrinters.Contains("brother") || pkInstalledPrinters.Contains("QL-") || pkInstalledPrinters.Contains("ql-"))
                {
                    //check if network printer
                    //if (pkInstalledPrinters.Contains(@"\"))
                    //{
                    //    //Network printer
                    //    DefaultPrinterCB.Items.Add(pkInstalledPrinters.Substring(pkInstalledPrinters.LastIndexOf('\\') + 1));
                    //}
                    //else
                    //{
                    //    //Local Printer
                    //    DefaultPrinterCB.Items.Add(pkInstalledPrinters);
                    //}

                    DefaultPrinterCB.Items.Add(pkInstalledPrinters);

                }

            }

            if (DefaultPrinterCB.Items.Count != 0)
            {
                //Check for Local DYMO Printer
                foreach (string prt in DefaultPrinterCB.Items)
                {
                    if (prt.Contains("DYMO"))
                    {
                        //Select the correct label
                        LabelSizeCB.SelectedIndex = 1;
                        vm.SelectedBarcode = "DYMO - Standard Label";
                        QRBARToggle.IsChecked = false;
                        CodeType = "QRCode";
                        StandardLabelDYMOImage.Width = 50;
                        StandardLabelDYMOImage.Height = 50;
                        StandardLabelBROTHERImage.Width = 50;
                        StandardLabelBROTHERImage.Height = 50;
                        StandardLabelZebraImage.Width = 50;
                        StandardLabelZebraImage.Height = 50;
                        AssetLabelZebraImage.Width = 50;
                        AssetLabelZebraImage.Height = 50;
                        MediumLabelZebraImage.Width = 50;
                        MediumLabelZebraImage.Height = 50;
                        LargeLabelZebraImage.Width = 50;
                        LargeLabelZebraImage.Height = 50;
                        CustomInfoLabelZebraImage.Width = 50;
                        CustomInfoLabelZebraImage.Height = 50;
                        CustomInfoLabelDYMOImage.Width = 50;
                        CustomInfoLabelDYMOImage.Height = 50;
                        CustomInfoLabelBROTHERImage.Width = 50;
                        CustomInfoLabelBROTHERImage.Height = 50;
                        //Recreate Label
                        // LabelsToGenerate();
                        //Select Default Printer
                        DefaultPrinterCB.SelectedIndex = 0;

                        //Save Settings
                        //MessageBox.Show(DefaultPrinterCB.Text); CreateQRCode(GenerateMetaData(numberOfCopies));
                        SaveSettings();
                        //MessageBox.Show("Local DYMO Present");
                    }

                    if (prt.Contains("BROTHER"))
                    {
                        //Select the correct label
                        LabelSizeCB.SelectedIndex = 1;
                        vm.SelectedBarcode = "BROTHER - Standard Label";
                        QRBARToggle.IsChecked = false;
                        CodeType = "QRCode";
                        StandardLabelDYMOImage.Width = 50;
                        StandardLabelDYMOImage.Height = 50;
                        StandardLabelBROTHERImage.Width = 50;
                        StandardLabelBROTHERImage.Height = 50;
                        StandardLabelZebraImage.Width = 50;
                        StandardLabelZebraImage.Height = 50;
                        AssetLabelZebraImage.Width = 50;
                        AssetLabelZebraImage.Height = 50;
                        MediumLabelZebraImage.Width = 50;
                        MediumLabelZebraImage.Height = 50;
                        LargeLabelZebraImage.Width = 50;
                        LargeLabelZebraImage.Height = 50;
                        CustomInfoLabelZebraImage.Width = 50;
                        CustomInfoLabelZebraImage.Height = 50;
                        CustomInfoLabelDYMOImage.Width = 50;
                        CustomInfoLabelDYMOImage.Height = 50;
                        CustomInfoLabelBROTHERImage.Width = 50;
                        CustomInfoLabelBROTHERImage.Height = 50;
                        //Recreate Label
                        // LabelsToGenerate();
                        //Select Default Printer
                        DefaultPrinterCB.SelectedIndex = 0;

                        //Save Settings
                        //MessageBox.Show(DefaultPrinterCB.Text);
                        SaveSettings();
                        //MessageBox.Show("Local DYMO Present");
                    }

                }

            }



            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        private void ToggleSwitch_Checked(object sender, RoutedEventArgs e)
        {
            CodeType = "Barcode";
            //Recreate Label
            LabelsToGenerate();
            //CodeType = "QRCode";
        }

        private void ToggleSwitch_Unchecked(object sender, RoutedEventArgs e)
        {

            CodeType = "QRCode";
            //Recreate Label
            LabelsToGenerate();

            //   QueryResultsLV.SelectionMode = SelectionMode.Multiple;

            //foreach(NetsuiteInventory record in QueryResultsLV.SelectedItems)
            //{
            //       MessageBox.Show(record.PrintQuantity.ToString()); 
            //}

            //NetsuiteInventory RecordRow = QueryResultsLV.Items.GetItemAt(ItemIndex) as NetsuiteInventory;
            //vm.BarcodeDescription = RecordRow.ProductDescription;
            //vm.BarcodeData = RecordRow.ProductCode;

        }



        private async void GetExcelData()
        {

            try
            {
                //Refresh the data
                RefreshBTN.IsEnabled = false;


                //Clear Collections and current search
                vm.NetsuiteInventoryData.Clear();
                vm.NetsuiteQueryData.Clear();
                vm.NetsuiteInventoryCount = 0;
                vm.BarcodeData = "";
                vm.BarcodeDescription = "";
                vm.BarcodeSearch = "";





                if (File.Exists(@"" + vm.myDocs + @"\Inventory.csv"))
                {
                    // Get local copy of file
                    string inv = @"" + vm.myDocs + @"\Inventory.csv";

                    // Check if file was is older than a day since last updated, if so then rerun inventory check
                    FileInfo fi = new FileInfo(inv);
                    if (fi.LastWriteTime < DateTime.Now.AddDays(-1))
                    {
                        //Read in the data from excel direct await 
                        ProgressBar.IsIndeterminate = true;

                        //Run inventory check
                        await System.Threading.Tasks.Task.Run(() => ExcelToCSV(@"" + vm.myDocs + @"\NS Item WQ.xlsx", @"" + vm.myDocs + @"\Inventory.csv"));
                    }
                    else
                    {
                        //skip
                    }

                    //Skip reloading part
                    //await PutTaskDelay(20000);
                }
                else
                {
                    //Read in the data from excel direct await 
                    ProgressBar.IsIndeterminate = true;
                    //Run Inventory check again
                    //await PutTaskDelay(20000);
                    await System.Threading.Tasks.Task.Run(() => ExcelToCSV(@"" + vm.myDocs + @"\NS Item WQ.xlsx", @"" + vm.myDocs + @"\Inventory.csv"));
                }




                if (File.Exists(@"" + vm.myDocs + @"\NS Item WQ.xlsx"))
                {
                    //Read in the data from csv
                    await System.Threading.Tasks.Task.Run(() => vm.ReadInventoryDataCommand.Execute(@"" + vm.myDocs + @"\Inventory.csv"));
                    // vm.ReadInventoryDataCommand.Execute(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\Inventory.csv");
                }
                else
                {
                    MessageBox.Show("File is missing, please contact support");
                }



                // await ReadExcel(@"C:\Users\N.Myers\Desktop\Excel Test\NS Item WQ.xlsx");

                ProgressBar.IsIndeterminate = false;

                //set focus to search field
                SearchTXTB.Focus();

                RefreshBTN.IsEnabled = true;

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }



        public void GenerateLabel()
        {
            if (vm != null)
                if (vm.BarcodeData != null || vm.BarcodeData != "" || vm.BarcodeData != string.Empty)
                {


                    switch (CodeType)
                    {
                        case "Barcode":
                            //StandardLabelZebraImage.Width = 220;
                            AlterBarWidthHeight();
                            StandardLabelZebraImage.Width = double.NaN; //252
                            AssetLabelZebraImage.Width = double.NaN; //252
                            MediumLabelZebraImage.Width = double.NaN;
                            LargeLabelZebraImage.Width = double.NaN;
                            StandardLabelDYMOImage.Width = double.NaN;
                            StandardLabelBROTHERImage.Width = double.NaN;

                            StandardLabelZebraImage.Height = 50;
                            AssetLabelZebraImage.Height = 50;
                            MediumLabelZebraImage.Height = 50;
                            LargeLabelZebraImage.Height = 50;
                            StandardLabelDYMOImage.Height = 50;
                            StandardLabelBROTHERImage.Height = 50;


                            //StandardLabelZebraImage.Width = 246; //252

                            //MediumLabelZebraImage.Width = 280;

                            //LargeLabelZebraImage.Width = 320;

                            switch (LabelSizeCB.Text)
                            {
                                case "Zebra - Standard Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        StandardLabelZebraImage.Source = CreateBarcode();

                                    }
                                    else
                                    {
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        AssetLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }

                                    break;
                                case "Zebra - Asset Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        AssetLabelZebraImage.Source = CreateBarcode();

                                    }
                                    else
                                    {
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        AssetLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }

                                    break;

                                case "Zebra - Medium Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        MediumLabelZebraImage.Source = CreateBarcode();

                                    }
                                    else
                                    {
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        AssetLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }

                                    break;
                                case "Zebra - Large Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        LargeLabelZebraImage.Source = CreateBarcode();

                                    }
                                    else
                                    {
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        AssetLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }

                                    break;
                                case "DYMO - Standard Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        StandardLabelDYMOImage.Source = CreateBarcode();

                                    }
                                    else
                                    {
                                        StandardLabelBROTHERImage.Source = null; //CreateQRCode("");
                                        StandardLabelDYMOImage.Source = null; //CreateQRCode("");
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }

                                    break;
                                case "BROTHER - Standard Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        StandardLabelBROTHERImage.Source = CreateBarcode();

                                    }
                                    else
                                    {
                                        StandardLabelBROTHERImage.Source = null; //CreateQRCode("");
                                        StandardLabelDYMOImage.Source = null; //CreateQRCode("");
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }

                                    break;
                            }




                            break;

                        case "QRCode":
                            //QRCODE
                            //BarWidthHeightCB.SelectedIndex = 3;
                           
                            StandardLabelZebraImage.Width = 50;
                            StandardLabelZebraImage.Height = 50;
                            AssetLabelZebraImage.Width = 50;
                            AssetLabelZebraImage.Height = 50;
                            StandardLabelDYMOImage.Width = 50;
                            StandardLabelDYMOImage.Height = 50;
                            StandardLabelBROTHERImage.Width = 50;
                            StandardLabelBROTHERImage.Height = 50;
                            MediumLabelZebraImage.Width = 50;
                            MediumLabelZebraImage.Height = 50;
                            LargeLabelZebraImage.Width = 50;
                            LargeLabelZebraImage.Height = 50;
                            CustomInfoLabelZebraImage.Width = 50;
                            CustomInfoLabelZebraImage.Height = 50;
                            CustomInfoLabelDYMOImage.Width = 50;
                            CustomInfoLabelDYMOImage.Height = 50;
                            CustomInfoLabelBROTHERImage.Width = 50;
                            CustomInfoLabelBROTHERImage.Height = 50;

                            switch (LabelSizeCB.Text)
                            {
                                case "Zebra - Standard Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        //if exclude meta is checked don't add to label
                                        if (vm.ExcludeMeta == true && vm.ExcludeAll == false)
                                        {
                                            vm.ShowInfoQR = Visibility.Collapsed;
                                            vm.ShowPartQR = Visibility.Visible;
                                           
                                        }
                                        else if(vm.ExcludeMeta == true && vm.ExcludeAll == true)
                                        {
                                            vm.ShowInfoQR = Visibility.Collapsed;
                                            vm.ShowPartQR = Visibility.Collapsed;
                                           
                                        }
                                        else
                                        {
                                            StandardInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(NumberOfCopiesCB.Text));
                                            vm.ShowInfoQR = Visibility.Visible;
                                            vm.ShowPartQR = Visibility.Visible;
                                           
                                        }


                                        StandardLabelZebraImage.Source = CreateQRCode(vm.BarcodeData);


                                    }
                                    else
                                    {
                                        vm.ShowInfoQR = Visibility.Collapsed;
                                        vm.ShowPartQR = Visibility.Collapsed;
                                      
                                        StandardLabelZebraImage.Source = null; //CreateQRCode(""); DateTime.Now.ToShortDateString() + " " + 
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }


                                    break;

                                case "Zebra - Asset Label":
                                    
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        //if exclude meta is checked don't add to label
                                        if (vm.ExcludeMeta == true && vm.ExcludeAll == false)
                                        {
                                            vm.ShowInfoQR = Visibility.Collapsed;
                                            vm.ShowPartQR = Visibility.Visible;

                                        }
                                        else if (vm.ExcludeMeta == true && vm.ExcludeAll == true)
                                        {
                                            vm.ShowInfoQR = Visibility.Collapsed;
                                            vm.ShowPartQR = Visibility.Collapsed;

                                        }
                                        else
                                        {
                                            //MessageBox.Show("Data= " + vm.BarcodeData);
                                            //AssetInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(NumberOfCopiesCB.Text));
                                            vm.ShowInfoQR = Visibility.Visible;
                                            vm.ShowPartQR = Visibility.Visible;

                                        }


                                        AssetLabelZebraImage.Source = CreateQRCode(vm.BarcodeData);

                                        //MessageBox.Show("Data= " + AssetLabelZebraImage.ActualWidth.ToString());
                                    }
                                    else
                                    {
                                        vm.ShowInfoQR = Visibility.Collapsed;
                                        vm.ShowPartQR = Visibility.Collapsed;

                                        StandardLabelZebraImage.Source = null; //CreateQRCode(""); DateTime.Now.ToShortDateString() + " " + 
                                        AssetLabelZebraImage.Source = null; //CreateQRCode(""); DateTime.Now.ToShortDateString() + " " + 
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }


                                    break;
                                case "Zebra - Medium Label":

                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                       
                                        //if exclude meta is checked don't add to label
                                        if (vm.ExcludeMeta == true && vm.ExcludeAll == false)
                                        {
                                            vm.ShowInfoQR = Visibility.Collapsed;
                                            vm.ShowPartQR = Visibility.Visible;
                                           
                                        }
                                        else if (vm.ExcludeMeta == true && vm.ExcludeAll == true)
                                        {
                                            vm.ShowInfoQR = Visibility.Collapsed;
                                            vm.ShowPartQR = Visibility.Collapsed;
                                           
                                        }
                                        else
                                        {

                                            MediumInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(NumberOfCopiesCB.Text));
                                            vm.ShowInfoQR = Visibility.Visible;
                                            vm.ShowPartQR = Visibility.Visible;
                                    

                                        }


                                        MediumLabelZebraImage.Source = CreateQRCode(vm.BarcodeData);



                                    }
                                    else
                                    {
                                        vm.ShowInfoQR = Visibility.Collapsed;
                                        vm.ShowPartQR = Visibility.Collapsed;
                                       
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        AssetLabelZebraImage.Source = null; //CreateQRCode(""); DateTime.Now.ToShortDateString() + " " + 
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }

                                    break;
                                case "Zebra - Large Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        //if exclude meta is checked don't add to label
                                        if (vm.ExcludeMeta == true && vm.ExcludeAll == false)
                                        {
                                            vm.ShowInfoQR = Visibility.Collapsed;
                                            vm.ShowPartQR = Visibility.Visible;
                                          
                                        }
                                        else if (vm.ExcludeMeta == true && vm.ExcludeAll == true)
                                        {
                                            vm.ShowInfoQR = Visibility.Collapsed;
                                            vm.ShowPartQR = Visibility.Collapsed;
                                           
                                        }
                                        else
                                        {

                                            LargeInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(NumberOfCopiesCB.Text));
                                            vm.ShowInfoQR = Visibility.Visible;
                                            vm.ShowPartQR = Visibility.Visible;
                                            
                                        }


                                        LargeLabelZebraImage.Source = CreateQRCode(vm.BarcodeData);



                                    }
                                    else
                                    {
                                        vm.ShowInfoQR = Visibility.Collapsed;
                                        vm.ShowPartQR = Visibility.Collapsed;
                                        vm.ShowSerialQR = Visibility.Collapsed;
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        AssetLabelZebraImage.Source = null; //CreateQRCode(""); DateTime.Now.ToShortDateString() + " " + 
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }


                                    break;
                                case "DYMO - Standard Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        //if exclude meta is checked don't add to label
                                        if (vm.ExcludeMeta == true && vm.ExcludeAll == false)
                                        {
                                            vm.ShowInfoQR = Visibility.Collapsed;
                                            vm.ShowPartQR = Visibility.Visible;
                                           
                                        }
                                        else if (vm.ExcludeMeta == true && vm.ExcludeAll == true)
                                        {
                                            vm.ShowInfoQR = Visibility.Collapsed;
                                            vm.ShowPartQR = Visibility.Collapsed;
                                           
                                        }
                                        else
                                        {
                                            StandardInfoLabelDYMOImage.Source = CreateQRCode(GenerateMetaData(NumberOfCopiesCB.Text));
                                            vm.ShowInfoQR = Visibility.Visible;
                                            vm.ShowPartQR = Visibility.Visible;
                                           
                                        }

                                        StandardLabelDYMOImage.Source = CreateQRCode(vm.BarcodeData);



                                    }
                                    else
                                    {
                                        vm.ShowInfoQR = Visibility.Collapsed;
                                        vm.ShowPartQR = Visibility.Collapsed;
                                      
                                        StandardLabelBROTHERImage.Source = null; //CreateQRCode("");
                                        StandardLabelDYMOImage.Source = null; //CreateQRCode("");
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        AssetLabelZebraImage.Source = null; //CreateQRCode(""); DateTime.Now.ToShortDateString() + " " + 
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }


                                    break;
                                case "BROTHER - Standard Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        //if exclude meta is checked don't add to label
                                        if (vm.ExcludeMeta == true && vm.ExcludeAll == false)
                                        {
                                            vm.ShowInfoQR = Visibility.Collapsed;
                                            vm.ShowPartQR = Visibility.Visible;
                                            
                                        }
                                        else if (vm.ExcludeMeta == true && vm.ExcludeAll == true)
                                        {
                                            vm.ShowInfoQR = Visibility.Collapsed;
                                            vm.ShowPartQR = Visibility.Collapsed;
                                           
                                        }
                                        else
                                        {
                                            StandardInfoLabelBROTHERImage.Source = CreateQRCode(GenerateMetaData(NumberOfCopiesCB.Text));
                                            vm.ShowInfoQR = Visibility.Visible;
                                            vm.ShowPartQR = Visibility.Visible;
                                           
                                        }


                                        StandardLabelBROTHERImage.Source = CreateQRCode(vm.BarcodeData);



                                    }
                                    else
                                    {
                                        vm.ShowInfoQR = Visibility.Collapsed;
                                        vm.ShowPartQR = Visibility.Collapsed;
                                       
                                        StandardLabelBROTHERImage.Source = null; //CreateQRCode("");
                                        StandardLabelDYMOImage.Source = null; //CreateQRCode("");
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        AssetLabelZebraImage.Source = null; //CreateQRCode(""); DateTime.Now.ToShortDateString() + " " + 
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }

                                    break;

                                case "Zebra - Custom Standard Label":

                                
                                    vm.ShowInfoQR = Visibility.Visible;
                                    

                                    //Goods In
                                    if (!string.IsNullOrEmpty(vm.BarcodeSerialNo) && !string.IsNullOrEmpty(vm.OrderRefNo))
                                        CustomInfoLabelZebraImage.Source = CreateQRCode(vm.BarcodeSerialNo + ", " + vm.OrderRefNo);
                                    //Goods Out
                                    if (!string.IsNullOrEmpty(vm.BarcodeSerialNo))
                                        CustomSerialLabelZebraImage.Source = CreateQRCode(vm.BarcodeSerialNo);

                                    break;

                                case "DYMO - Custom Standard Label":

                                    vm.ShowInfoQR = Visibility.Visible;


                                    //Goods In
                                    if (!string.IsNullOrEmpty(vm.BarcodeSerialNo) && !string.IsNullOrEmpty(vm.OrderRefNo))
                                        CustomInfoLabelDYMOImage.Source = CreateQRCode(vm.BarcodeSerialNo + ", " + vm.OrderRefNo);
                                    //Goods Out
                                    if (!string.IsNullOrEmpty(vm.BarcodeSerialNo))
                                        CustomSerialLabelDYMOImage.Source = CreateQRCode(vm.BarcodeSerialNo);



                                    break;

                                case "BROTHER - Custom Standard Label":

                                    vm.ShowInfoQR = Visibility.Visible;


                                    //Goods In
                                    if (!string.IsNullOrEmpty(vm.BarcodeSerialNo) && !string.IsNullOrEmpty(vm.OrderRefNo))
                                        CustomInfoLabelBROTHERImage.Source = CreateQRCode(vm.BarcodeSerialNo + ", " + vm.OrderRefNo);
                                    //Goods Out
                                    if (!string.IsNullOrEmpty(vm.BarcodeSerialNo))
                                        CustomSerialLabelBROTHERImage.Source = CreateQRCode(vm.BarcodeSerialNo);


                                    break;

                            }

                            break;
                    }


                }
        }




        public string GenerateMetaData(string Copies)
        {

            string returnData = "";
            //if (string.IsNullOrEmpty(vm.OrderRefNo))
            //{
            //    //Generate Meta Data String
            //    returnData = DateTime.Now.ToShortDateString() + "," + vm.CurrentUserInitials + "," + Copies + ",None" + "," + vm.BarcodeData + "," + vm.BarcodeSerialNo;
            //}
            //else
            //{
            //    //Generate Meta Data String
            //    returnData = DateTime.Now.ToShortDateString() + "," + vm.CurrentUserInitials + "," + Copies + "," + vm.OrderRefNo + "," + vm.BarcodeData + "," + vm.BarcodeSerialNo;
            //}


            //returnData = DateTime.Now.ToShortDateString() + "," + vm.CurrentUserInitials + "," + Copies + "," + vm.OrderRefNo.ToUpperCheckForNull() + "," + vm.BarcodeData + "," + vm.BarcodeSerialNo;

            returnData = DateTime.Now.ToShortDateString() + "," + vm.CurrentUserInitials + "," + Copies + "," + vm.OrderRefNo.ToUpperCheckForNull() + "," + vm.BarcodeData + ",";
            // MessageBox.Show(returnData);

            return returnData;
        }






        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            //generate a new label
            GenerateLabel();


        }

     
        private void SerialNoTXTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check if sn blank or exclude all checked by user 
            if (String.IsNullOrWhiteSpace(vm.BarcodeSerialNo) != true && ExcludeAllCB.IsChecked == false)
            {
               
                StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                AssetLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                CustomLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                CustomLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                CustomLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                vm.ShowSerialQR = Visibility.Visible;
                vm.QRSubTitle = "Serial No";
                vm.ShowSerialTitle = Visibility.Visible;
                StandardSerialLabelZebraImage.Source = CreateQRCode(vm.BarcodeSerialNo);
                //StandardInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(NumberOfCopiesCB.Text));
                MediumSerialLabelZebraImage.Source = StandardSerialLabelZebraImage.Source; // CreateQRCode(vm.BarcodeSerialNo);
                //MediumInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(NumberOfCopiesCB.Text));
                LargeSerialLabelZebraImage.Source = StandardSerialLabelZebraImage.Source; // CreateQRCode(vm.BarcodeSerialNo);
                //LargeInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(NumberOfCopiesCB.Text));
                StandardSerialLabelDYMOImage.Source = StandardSerialLabelDYMOImage.Source; //CreateQRCode(vm.BarcodeSerialNo);
                //StandardInfoLabelDYMOImage.Source = CreateQRCode(GenerateMetaData(NumberOfCopiesCB.Text));
                StandardSerialLabelBROTHERImage.Source = StandardSerialLabelDYMOImage.Source; // CreateQRCode(vm.BarcodeSerialNo);
                //Custom Labels                                                                               //StandardInfoLabelBROTHERImage.Source = CreateQRCode(GenerateMetaData(NumberOfCopiesCB.Text));
                StandardSerialLabelZebraImage.Source = StandardSerialLabelZebraImage.Source; // CreateQRCode(vm.BarcodeSerialNo);
                StandardSerialLabelDYMOImage.Source = StandardSerialLabelDYMOImage.Source; // CreateQRCode(vm.BarcodeSerialNo);
                StandardSerialLabelBROTHERImage.Source = StandardSerialLabelZebraImage.Source; // CreateQRCode(vm.BarcodeSerialNo);
            }
            else
            {
                StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                AssetLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                CustomLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                CustomLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                CustomLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                vm.ShowSerialQR = Visibility.Collapsed;

            }
        }




        //public void GeneratePng(Visual mapGrid, string pngURI)
        //{
        //    double dpi = App.SelectedDPI;
        //    double scale = dpi / 96;
        //    Rect bounds = VisualTreeHelper.GetDescendantBounds(mapGrid);
        //    RenderTargetBitmap rt = new RenderTargetBitmap((int)(bounds.Width * scale),
        //                                                   (int)(bounds.Height * scale),
        //                                                    dpi,
        //                                                    dpi,
        //                                                    PixelFormats.Pbgra32);
        //    DrawingVisual dv = new DrawingVisual();
        //    using (DrawingContext ctx = dv.RenderOpen())
        //    {
        //        VisualBrush vb = new VisualBrush(mapGrid);
        //        ctx.DrawRectangle(vb, null, new Rect(new Point(), bounds.Size));
        //    }
        //    rt.Render(dv);
        //    PngBitmapEncoder png = new PngBitmapEncoder();
        //    png.Frames.Add(BitmapFrame.Create(rt));
        //    using (Stream streamPng = File.Create(pngURI))
        //    {
        //        png.Save(streamPng);
        //    }
        //}

        private static Bitmap ResizeImage(Bitmap mg, System.Drawing.Size newSize)
        {
            double ratio = 0d;
            double myThumbWidth = 0d;
            double myThumbHeight = 0d;
            int x = 0;
            int y = 0;

            Bitmap bp;


            if ((mg.Width / Convert.ToDouble(newSize.Width)) > (mg.Height /
            Convert.ToDouble(newSize.Height)))
                ratio = Convert.ToDouble(mg.Width) / Convert.ToDouble(newSize.Width);
            else
                ratio = Convert.ToDouble(mg.Height) / Convert.ToDouble(newSize.Height);
            myThumbHeight = Math.Ceiling(mg.Height / ratio);
            myThumbWidth = Math.Ceiling(mg.Width / ratio);

            System.Drawing.Size thumbSize = new System.Drawing.Size((int)myThumbWidth, (int)myThumbHeight);
            bp = new Bitmap(newSize.Width, newSize.Height);
            x = (newSize.Width - thumbSize.Width) / 2;
            y = (newSize.Height - thumbSize.Height);

            System.Drawing.Graphics g = Graphics.FromImage(bp);
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            System.Drawing.Rectangle rect = new System.Drawing.Rectangle(x, y, thumbSize.Width, thumbSize.Height);
            g.DrawImage(mg, rect, 0, 0, mg.Width, mg.Height, GraphicsUnit.Pixel);

            return bp;
        }

        public BitmapImage CreateBarcode()
        {

            BitmapImage bi = new BitmapImage();

            //AlterBarWidthHeight();




            ////Set the serial number to text on the clipboard  Use control v

            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
            b.BarWidth = vm.BarcodeBarWidth;

            //b.Alignment = BarcodeLib.AlignmentPositions.LEFT;
            b.StandardizeLabel = false; // (bool)StandardizeLabelCHK.IsChecked;
            //System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.CODE128, "UCS-MR-1X162RY-AB-REF", System.Drawing.Color.Black, System.Drawing.Color.White, 290, 120);

            // if (vm.BarcodeData != null || vm.BarcodeData != "" || vm.BarcodeData != string.Empty)
            if (string.IsNullOrWhiteSpace(vm.BarcodeData))
            {


            }
            else
            {
                // System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.CODE128, vm.BarcodeData, System.Drawing.Color.Black, System.Drawing.Color.White, 680, 50);



                //Create an image object from the barcode
                //MessageBox.Show(vm.BarcodeHeight.ToString());

                //WORKING
                //System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.CODE128, vm.BarcodeData, System.Drawing.Color.Black, System.Drawing.Color.White, 680, vm.BarcodeHeight);


                System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.CODE128, vm.BarcodeData, System.Drawing.Color.Black, System.Drawing.Color.White, 600, 30);
                //System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.CODE128, vm.BarcodeData, System.Drawing.Color.Black, System.Drawing.Color.White, 4000, vm.BarcodeHeight);

                // Winforms Image we want to get the WPF Image from...
                //from file
                // System.Drawing.Image imgWinForms = System.Drawing.Image.FromFile("test.png");

                System.Drawing.Image imgWinForms = img;

                // ImageSource ...

                // MessageBox.Show(img.Width + " || " + img.Height);

                bi.BeginInit();

                MemoryStream ms = new MemoryStream();

                // Save to a memory stream...

                imgWinForms.Save(ms, ImageFormat.Bmp);

                // Rewind the stream...

                ms.Seek(0, SeekOrigin.Begin);

                // Tell the WPF image to use this stream...

                bi.StreamSource = ms;
                bi.DecodePixelWidth = img.Width;
                //bi.DecodePixelWidth = 200;
                //bi.DecodePixelHeight = 30;
                //bi.DecodePixelWidth = img.Height;
                //       // bi.DecodePixelHeight = 40;

                bi.EndInit();


                //CreatePNG(bi);

                return bi;
            }



            return bi;



        }




        //Generate QRCODE Image
        public BitmapImage CreateQRCode(string stringData)
        {
            try
            {

         
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(stringData, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(50, System.Drawing.Color.Black, System.Drawing.Color.White, null, 1, 1, true);



            return ToBitmapImage(qrCodeImage);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return null;
            }
        }

        //BITMAP TO BITMAP IMAGE
        public BitmapImage ToBitmapImage(Bitmap bitmap)
        {
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();

                return bitmapImage;
            }
        }

        ////BITMAP IMAGE TO BITMAP
        private Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            return new Bitmap(bitmapImage.StreamSource);
        }



        public BitmapSource ConvertToBitmapSource(UIElement element)
        {
            var target = new RenderTargetBitmap((int)(element.RenderSize.Width), (int)(element.RenderSize.Height), 96, 96, PixelFormats.Pbgra32);
            var brush = new VisualBrush(element);

            var visual = new DrawingVisual();
            var drawingContext = visual.RenderOpen();


            drawingContext.DrawRectangle(brush, null, new Rect(new System.Windows.Point(0, 0),
                new System.Windows.Point(element.RenderSize.Width, element.RenderSize.Height)));

            drawingContext.PushOpacityMask(brush);

            drawingContext.Close();

            target.Render(visual);

            return target;
        }


        public void CreatePNG(BitmapImage bi)
        {

            //FormattedText text = new FormattedText("Barcode Test",
            //new CultureInfo("en-us"),
            //FlowDirection.LeftToRight,
            //new Typeface(this.FontFamily, FontStyles.Normal, FontWeights.Normal, new FontStretch()),
            //this.FontSize, System.Windows.Media.Brushes.White);

            // The Visual to use as the source of the RenderTargetBitmap.
            DrawingVisual drawingVisual = new DrawingVisual();
            DrawingContext drawingContext = drawingVisual.RenderOpen();
            drawingContext.DrawImage(bi, new Rect(0, 0, bi.Width, bi.Height));
            //drawingContext.DrawText(text, new System.Windows.Point(bi.Height / 2, 0));
            drawingContext.Close();

            // The BitmapSource that is rendered with a Visual.
            RenderTargetBitmap rtb = new RenderTargetBitmap(bi.PixelWidth, bi.PixelHeight, 96, 96, PixelFormats.Pbgra32);
            rtb.Render(drawingVisual);

            // Encoding the RenderBitmapTarget as a PNG file.
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(rtb));
            using (Stream stm = File.Create(@"C:\Users\N.Myers\Desktop\For USA\new.png"))
            {
                png.Save(stm);
            }
        }

        private async void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            ComboBox cmb = (ComboBox)sender;
            //MessageBox.Show(cmb.SelectedItem.ToString());
            if (StandardLabelZebra != null)
            {
               
                StandardLabelZebra.Visibility = Visibility.Collapsed;
                AssetLabelZebra.Visibility = Visibility.Collapsed;
                MediumLabelZebra.Visibility = Visibility.Collapsed;
                LargeLabelZebra.Visibility = Visibility.Collapsed;
                StandardLabelDYMO.Visibility = Visibility.Collapsed;
                StandardLabelBROTHER.Visibility = Visibility.Collapsed;
                CustomLabelZebra.Visibility = Visibility.Collapsed;
                CustomLabelDYMO.Visibility = Visibility.Collapsed;
                CustomLabelBROTHER.Visibility = Visibility.Collapsed;

                //MessageBox.Show(((ComboBoxItem)cmb.SelectedItem).Content.ToString());
                //generate a new label
                //GenerateLabel();
                string selected = ((ComboBoxItem)cmb.SelectedItem).Content.ToString();

                switch (selected)
                {
                    case "Zebra - Standard Label":
                        // Show Label
                        StandardLabelZebra.Visibility = Visibility.Visible;
                        vm.SelectedBarcode = "Zebra - Standard Label";

                        break;
                    case "Zebra - Asset Label":
                        // Show Label
                        AssetLabelZebra.Visibility = Visibility.Visible;
                        vm.SelectedBarcode = "Zebra - Asset Label";
                        //Set 
                        DescriptionCodeFontCB.SelectedIndex = 5;
                        ProductCodeFontCB.SelectedIndex = 5;
                        break;
                    case "Zebra - Medium Label":
                        // Show Label
                        MediumLabelZebra.Visibility = Visibility.Visible;
                        vm.SelectedBarcode = "Zebra - Medium Label";

                        break;

                    case "Zebra - Large Label":
                        // MessageBox.Show(cmb.SelectedItem.ToString());
                        // Show Label
                        LargeLabelZebra.Visibility = Visibility.Visible;
                        vm.SelectedBarcode = "Zebra - Large Label";

                        break;

                    case "DYMO - Standard Label":
                        // Show Label
                        StandardLabelDYMO.Visibility = Visibility.Visible;
                        vm.SelectedBarcode = "DYMO - Standard Label";

                        break;
                    case "BROTHER - Standard Label":
                        // Show Label
                        StandardLabelBROTHER.Visibility = Visibility.Visible;
                        vm.SelectedBarcode = "BROTHER - Standard Label";

                        break;
                    case "Zebra - Custom Standard Label":

                        //MessageBox.Show("Selected Custom Label");
                        // Show Label
                        CustomLabelZebra.Visibility = Visibility.Visible;
                        vm.SelectedBarcode = "Zebra - Custom Standard Label";
                        //MessageBox.Show(vm.SelectedBarcode);
                        break;
                    case "DYMO - Custom Standard Label":

                        //MessageBox.Show("Selected Custom Label");
                        // Show Label
                        CustomLabelDYMO.Visibility = Visibility.Visible;
                        vm.SelectedBarcode = "DYMO - Custom Standard Label";
                        //MessageBox.Show(vm.SelectedBarcode);
                        break;
                    case "BROTHER - Custom Standard Label":

                        //MessageBox.Show("Selected Custom Label");
                        // Show Label
                        CustomLabelBROTHER.Visibility = Visibility.Visible;
                        vm.SelectedBarcode = "BROTHER - Custom Standard Label";
                        //MessageBox.Show(vm.SelectedBarcode);
                        break;
                }
                await PutTaskDelay(300);

                //generate a new label
                GenerateLabel();
            }

        }

        private void SearchBTN_Click(object sender, RoutedEventArgs e)
        {




            //clear the listview collection items
            vm.NetsuiteQueryData.Clear();


            //Search Inventory
            SearchInventory();
        }

        private void SearchTXTB_KeyDown(object sender, KeyEventArgs e)
        {
            //Check if enter key was hit in textbox
            if (e.Key == Key.Enter)
            {
                //clear the listview collection items
                vm.NetsuiteQueryData.Clear();
                // set progress bar to true
                //Search Inventory
                SearchInventory();
            }
        }



        public void SearchInventory()
        {
            try
            {



                if (String.IsNullOrWhiteSpace(vm.BarcodeSearch))
                {

                }
                else
                {
                    ProgressBar.IsIndeterminate = true;
                    //await PutTaskDelay(1000); ESPRIMOE920-i7-4790-8GB-0HDD-REF-C

                    // MessageBox.Show("Came in"); vm.BarcodeSearch != null || vm.BarcodeSearch != "" || vm.BarcodeSearch != string.Empty ||

                    //IEnumerable<NetsuiteInventory> foos1 = vm.NetsuiteInventoryData.Where(c => c.ProductCode == vm.BarcodeData);
                    IEnumerable<NetsuiteInventory> results = from c in vm.NetsuiteInventoryData
                                                             where c.ProductCode.StartsWith(vm.BarcodeSearch) || c.ProductCode.StartsWith(vm.BarcodeSearch.ToUpper())
                                                             select c;

                    //find results and add to query collection
                    foreach (var itm in results)
                    {
                        vm.NetsuiteQueryData.Add(itm);
                        //MessageBox.Show(itm.ProductDescription);

                    }

                    // set progress bar to false
                    ProgressBar.IsIndeterminate = false;
                }

                // return "";

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }



        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

        private void QueryResultsLV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int ItemIndex;

            NumberOfCopiesCB.SelectedIndex = 0;

            //Reshow 
            if (ExcludeMetaCB.IsChecked == false)
            {
                vm.ShowInfoTitle = Visibility.Visible;
                vm.ShowInfoQR = Visibility.Visible;
                vm.ShowPartQR = Visibility.Visible;
            }

            ItemIndex = QueryResultsLV.SelectedIndex;
            //MessageBox.Show(ItemIndex.ToString());
            if (ItemIndex != -1)
            {
                NetsuiteInventory RecordRow = QueryResultsLV.Items.GetItemAt(ItemIndex) as NetsuiteInventory;
                vm.BarcodeDescription = RecordRow.ProductDescription;
                vm.BarcodeData = RecordRow.ProductCode;



                //Change Bar Height Based on product code length
                switch (CodeType)
                {
                    case "Barcode":
                        //BarWidthHeightCB.SelectedIndex = 2;
                        //BarWidthCB.SelectedIndex = 0;
                        AlterBarWidthHeight();
                        break;

                    case "QRCode":
                        //QRCODE

                        break;
                }


            }
        }

        private void QueryResultsLV_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            //Change Bar Height Based on product code length
            //AlterBarWidthHeight();
        }


        private void AlterBarWidthHeight()
        {



            if (vm.BarcodeData.Count() >= 28)
            {
                //MessageBox.Show(vm.BarcodeData.Count().ToString());
                BarWidthHeightCB.SelectedIndex = 6; //40
                                                    // vm.BarcodeHeight = 45;
            }
            else if (vm.BarcodeData.Count() >= 25 && vm.BarcodeData.Count() < 28)
            {
                // MessageBox.Show(vm.BarcodeData.Count().ToString());
                //MessageBox.Show("Here");
                BarWidthHeightCB.SelectedIndex = 5; //35
                                                    // vm.BarcodeHeight = 40;
            }
            else if (vm.BarcodeData.Count() >= 21 && vm.BarcodeData.Count() < 25)
            {
                //MessageBox.Show(vm.BarcodeData.Count().ToString());
                //MessageBox.Show("Here");
                BarWidthHeightCB.SelectedIndex = 4; //40
                                                    // vm.BarcodeHeight = 35;
            }
            else if (vm.BarcodeData.Count() >= 15 && vm.BarcodeData.Count() < 21)
            {
                //MessageBox.Show(vm.BarcodeData.Count().ToString());
                //MessageBox.Show("Here");
                BarWidthHeightCB.SelectedIndex = 3; //40
                                                    // vm.BarcodeHeight = 35;
            }
            else if (vm.BarcodeData.Count() >= 12 && vm.BarcodeData.Count() < 15)
            {
                //MessageBox.Show(vm.BarcodeData.Count().ToString());
                //MessageBox.Show("Here");
                BarWidthHeightCB.SelectedIndex = 3; //30
                                                    // vm.BarcodeHeight = 30;
            }
            else if (vm.BarcodeData.Count() >= 6 && vm.BarcodeData.Count() < 12)
            {
                //MessageBox.Show(vm.BarcodeData.Count().ToString());
                //MessageBox.Show("Here");
                BarWidthHeightCB.SelectedIndex = 1; //20
                                                    // vm.BarcodeHeight = 15;
            }
            else if (vm.BarcodeData.Count() <= 5)
            {
                //MessageBox.Show(vm.BarcodeData.Count().ToString());
                //MessageBox.Show("Here");
                BarWidthHeightCB.SelectedIndex = 1; //10
                //vm.BarcodeHeight = 10;
            }


            vm.BarcodeProductIDCount = vm.BarcodeData.Count().ToString();



            //else
            //{

            //    BarWidthHeightCB.SelectedIndex = 2; //25
            //    MessageBox.Show(vm.BarcodeData.Count().ToString());
            //    // vm.BarcodeHeight = 25;
            //}


        }





        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            //clear the listview collection items
            vm.NetsuiteQueryData.Clear();
            vm.BarcodeSearch = "";
            vm.BarcodeData = "";
            vm.BarcodeDescription = "";
            vm.BarcodeSerialNo = "";
        }



        //Excel file needs trusting to run the query
        //


        public Task ReadExcel(string excelPath)
        {

            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            if (excelApp != null)
            {
                Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excelApp.Workbooks.Open(excelPath, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Microsoft.Office.Interop.Excel.Worksheet excelWorksheet = (Microsoft.Office.Interop.Excel.Worksheet)excelWorkbook.Sheets[1];
                excelApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Range excelRange = excelWorksheet.UsedRange;
                int rowCount = excelRange.Rows.Count;
                int colCount = excelRange.Columns.Count;

                for (int i = 1; i <= rowCount; i++)
                {

                    //pull in the worksheet range and assign to system array
                    Microsoft.Office.Interop.Excel.Range range = excelWorksheet.get_Range("A" + i.ToString(), "B" + i.ToString());
                    System.Array myvalues = (System.Array)range.Cells.Value;

                    //Convert to a string array                   
                    string[] strArray = ConvertToStringArray(myvalues);

                    //create new instance of netsuite item and assign array value
                    NetsuiteInventory nitm = new NetsuiteInventory();
                    nitm.ProductCode = strArray[0];
                    nitm.ProductDescription = strArray[1];

                    //add row to collection
                    vm.NetsuiteInventoryData.Add(nitm);

                    //for (int j = 1; j <= colCount; j++)
                    //{
                    ////Microsoft.Office.Interop.Excel.Range range = (excelWorksheet.Cells[i, 1] as Microsoft.Office.Interop.Excel.Range);




                    //string cellValue1 = range.Value.ToString();
                    //    string cellValue2 = range.Value.ToString();
                    //MessageBox.Show(cellValue);
                    //    //do anything
                    //}
                }

                excelWorkbook.Close();
                excelApp.Quit();
            }



            // excelApp.Quit();

            return Task.CompletedTask;
        }




        string[] ConvertToStringArray(System.Array values)
        {

            // create a new string array
            string[] theArray = new string[values.Length];

            // loop through the 2-D System.Array and populate the 1-D String Array
            for (int i = 1; i <= values.Length; i++)
            {
                if (values.GetValue(1, i) == null)
                    theArray[i - 1] = "";
                else
                    theArray[i - 1] = (string)values.GetValue(1, i).ToString();
            }

            return theArray;
        }

        //Hold Excel App process id
        //this Thread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
        public Microsoft.Office.Interop.Excel.Application app = null;


        //exporting excel into csv file
        public async void ExcelToCSV(string excelPath, string exportPath)
        {
            //this Thread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            //Microsoft.Office.Interop.Excel.Application app = null;

            //InteropExcel.Workbooks wkbks = null;
            Microsoft.Office.Interop.Excel.Workbook wb = null;


            try
            {


                String fromFile = excelPath;
                String toFile = exportPath;

                app = new Microsoft.Office.Interop.Excel.Application();
                wb = app.Workbooks.Open(fromFile, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                app.DisplayAlerts = false;

                // Loop through and end query on each workbook then run refresh all after
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    foreach (QueryTable table in worksheet.QueryTables)
                        table.BackgroundQuery = false;
                }


                // refresh the excel data
                wb.RefreshAll();

                //Listed way but never finishes
                //app.CalculateUntilAsyncQueriesDone();

                //** old delay **
                // await PutTaskDelay(16000);

                //wb.Save();

                //await PutTaskDelay(5000);
                // this does not throw exception if file doesnt exist
                //File.Delete(toFile);
                //await PutTaskDelay(12000);
                wb.SaveAs(toFile, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSVWindows, Type.Missing, Type.Missing, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, false, Type.Missing, Type.Missing, Type.Missing);
                //await PutTaskDelay(8000);
                //wb.Close(true, Type.Missing, Type.Missing);

                //app.Quit();
                if (wb != null)
                {
                    //app.DisplayAlerts = false;
                    // wb.Close(false);
                    wb.Close(true, Type.Missing, Type.Missing);
                    // Marshal.FinalReleaseComObject(wb);
                    wb = null;
                }

                //if (wkbks != null)
                //{
                //    wkbks.Close();
                //    Marshal.FinalReleaseComObject(wkbks);
                //    wkbks = null;
                //}

                if (app != null)
                {
                    // Close Excel.
                    app.Quit();
                    // Marshal.ReleaseComObject(app);
                    // Marshal.FinalReleaseComObject(app);
                    await PutTaskDelay(1000);
                    //Terminate PID Instance
                    TerminateExcelProcess(app);

                    app = null;


                }




            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            // return Task.CompletedTask;
        }


        //CAPTURE PID FOR EXCEL PROCESS

        [DllImport("user32.dll")]
        static extern int GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);

        Process GetExcelProcess(Microsoft.Office.Interop.Excel.Application excelApp)
        {
            try
            {


                int id;
                GetWindowThreadProcessId(excelApp.Hwnd, out id);
                return Process.GetProcessById(id);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);

                return null;
            }
        }

        void TerminateExcelProcess(Microsoft.Office.Interop.Excel.Application excelApp)
        {
            var process = GetExcelProcess(excelApp);
            if (process != null)
            {
                process.Kill();
            }
        }



        private void RefreshBTN_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists(@"" + vm.myDocs + @"\Inventory.csv"))
            {
                File.Delete(@"" + vm.myDocs + @"\Inventory.csv");
            }

            //call async function
            GetExcelData();
        }

        private void BarWidthHeightCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            //If Barcode height change readjust
            if (vm != null)
            {
                //get the combobox handle
                ComboBox cmb = (ComboBox)sender;
                //set barcode height property
                vm.BarcodeHeight = Convert.ToInt32(((ComboBoxItem)cmb.SelectedItem).Content.ToString());

                //call method to generate for all labels
                LabelsToGenerate();
            }

        }

        private void BarWidthCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //If Barcode height change readjust
            if (vm != null)
            {
                //get the combobox handle
                ComboBox cmb = (ComboBox)sender;
                //set barcode height property
                vm.BarcodeBarWidth = Convert.ToInt32(((ComboBoxItem)cmb.SelectedItem).Content.ToString());

                //call method to generate for all labels
                LabelsToGenerate();


            }
        }



        private void LabelsToGenerate()
        {

            //generate a new label
            GenerateLabel();

        }

        private void ProductCodeFontCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //MessageBox.Show(ProductCodeFontCB.SelectedValue.ToString());
            //Recreate Label

            LabelsToGenerate();
        }

        private void DescriptionCodeFontCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LabelsToGenerate();
        }

        private async void AddBTN_Click(object sender, RoutedEventArgs e)
        {
            // Check if item is selected before attempting to add
            if (QueryResultsLV.SelectedItems.Count > 0)
            {
                if (ExcludeAllCB.IsChecked == true)
                {
                    //Add custom label to list
                    NetsuiteInventory nsi = new NetsuiteInventory();
                    nsi.PrintQuantity = NumberOfCopiesCB.Text;
                    nsi.ProductCode = vm.BarcodeData;
                    nsi.ProductDescription = vm.BarcodeDescription;
                    nsi.SerialNumber = vm.BarcodeSerialNo.ToUpperCheckForNull();
                    vm.NetsuiteMultiPrint.Add(nsi);
                }
                else
                {
                    //Get Selected Item
                    NetsuiteInventory nsiSI = new NetsuiteInventory();
                    nsiSI = (NetsuiteInventory)QueryResultsLV.SelectedItem;
                    //Create new inventory object
                    NetsuiteInventory nsi = new NetsuiteInventory();
                    nsi.ProductDescription = nsiSI.ProductDescription;
                    nsi.ProductCode = nsiSI.ProductCode;
                    nsi.SerialNumber = vm.BarcodeSerialNo.ToUpperCheckForNull();
                    nsi.PONumber = vm.OrderRefNo.ToUpperCheckForNull();
                    nsi.PrintQuantity = "1";
                    vm.NetsuiteMultiPrint.Add(nsi);
                }

                //Clear Serial Field
                vm.BarcodeSerialNo = "";
            }
            else
            {
                //Create Brush Converter
                var bc = new BrushConverter();
                //Show message
                ProgressMsg.Visibility = Visibility.Visible;
                ProgressMsg.Foreground = System.Windows.Media.Brushes.Red;
                ProgressMsg.Text = "Please select an item before trying to add.";
                await PutTaskDelay(2000);
                ProgressMsg.Foreground = (System.Windows.Media.Brush)bc.ConvertFrom("#FF0f89bb");
                ProgressMsg.Visibility = Visibility.Hidden;
                ProgressMsg.Text = "";
            }
        }

        private async void PrintLabels(string numberOfCopies)
        {
            if (DefaultPrinterCB.Text != "")
            {
                PrintingProgressBar.IsIndeterminate = true;

                DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();
                //dpw.PrintStandardVisual(StandardLabelZebraEntireBarcode);

               

                switch (vm.SelectedBarcode)
                {
                    case "Zebra - Standard Label":

                        switch (CodeType)
                        {
                            case "Barcode":
                                //OFFSET FOR ZEBRA PRINTER
                                // MessageBox.Show(vm.SelectedBarcode);
                                //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                                // MessageBox.Show(vm.SelectedBarcode);
                                //dpw.PrintStandardVisual(StandardLabelZebraEntireBarcode);

                                // StandardLabelZebraImage.Margin = new Thickness(0, 0, 20, 0);
                                //StandardSerialLabelZebraImage.Margin = new Thickness(0, 0, 14, 0);
                                await PutTaskDelay(800);

                                dpw.PrintDirectVisualZEBRA(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);

                                StandardInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));
                                StandardLabelBROTHERImage.Source = CreateQRCode(vm.BarcodeData);
                                //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //StandardSerialLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                // StandardLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                break;

                            case "QRCode":
                                //QRCODE
                                //  dpw.PrintStandardVisual(StandardLabelZebraEntireBarcode);
                                //dpw.PrintDirectVisual(StandardLabelZebraEntireBarcode, new PrintQueue(new PrintServer(((System.Printing.PrintQueue)DefaultPrinterCB.SelectedItem).HostingPrintServer.Name), (string)DefaultPrinterCB.SelectedValue), (int)NumberOfCopiesCB.SelectedValue, PageOrientation.Portrait);
                                //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                                //MessageBox.Show("Standard Label");
                                // MessageBox.Show(numberOfCopies);
                                //Regenerate Meta Label + "-" + vm.CurrentUserLocation DateTime.Now.ToString() + " " + vm.CurrentUser + " " + 
                                StandardInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));

                                await PutTaskDelay(800);
                                //Print
                                dpw.PrintDirectVisualZEBRA(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);

                                //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                StandardInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));
                                StandardLabelZebraImage.Source = CreateQRCode(vm.BarcodeData);
                                break;
                        }

                        break;

                    case "Zebra - Asset Label":

                        switch (CodeType)
                        {
                            case "Barcode":
                                //OFFSET FOR ZEBRA PRINTER
                                // MessageBox.Show(vm.SelectedBarcode);
                                //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                                // MessageBox.Show(vm.SelectedBarcode);
                                //dpw.PrintStandardVisual(StandardLabelZebraEntireBarcode);

                                // StandardLabelZebraImage.Margin = new Thickness(0, 0, 20, 0);
                                //StandardSerialLabelZebraImage.Margin = new Thickness(0, 0, 14, 0);
                                await PutTaskDelay(800);

                                dpw.PrintDirectVisualZEBRA(AssetLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);

                                //AssetInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));
                                //AssetLabelBROTHERImage.Source = CreateQRCode(vm.BarcodeData);
                                //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //StandardSerialLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                // StandardLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                break;

                            case "QRCode":
                                //QRCODE
                                //  dpw.PrintStandardVisual(StandardLabelZebraEntireBarcode);
                                //dpw.PrintDirectVisual(StandardLabelZebraEntireBarcode, new PrintQueue(new PrintServer(((System.Printing.PrintQueue)DefaultPrinterCB.SelectedItem).HostingPrintServer.Name), (string)DefaultPrinterCB.SelectedValue), (int)NumberOfCopiesCB.SelectedValue, PageOrientation.Portrait);
                                //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                                //MessageBox.Show("Standard Label");
                                // MessageBox.Show(numberOfCopies);
                                //Regenerate Meta Label + "-" + vm.CurrentUserLocation DateTime.Now.ToString() + " " + vm.CurrentUser + " " + 
                                //AssetInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));

                                await PutTaskDelay(800);
                                //Print
                                dpw.PrintDirectVisualZEBRA(AssetLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);

                                //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //AssetInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));
                                //AssetLabelBROTHERImage.Source = CreateQRCode(vm.BarcodeData);
                                break;
                        }

                        break;

                    case "Zebra - Medium Label":
                        switch (CodeType)
                        {
                            case "Barcode":
                                //OFFSET FOR ZEBRA PRINTER
                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //MediumLabelZebraImage.Margin = new Thickness(0, 0, 20, 0);
                                //MediumSerialLabelZebraImage.Margin = new Thickness(0, 0, 14, 0);
                                await PutTaskDelay(800);
                                //dpw.PrintStandardVisual(MediumLabelZebraEntireBarcode);
                                dpw.PrintDirectVisualZEBRA(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);
                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //MediumSerialLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                //MediumLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                break;

                            case "QRCode":
                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                                //Regenerate Meta Label
                                MediumInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));

                                await PutTaskDelay(800);
                                //Print
                                dpw.PrintDirectVisualZEBRA(MediumLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);

                                MediumInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));
                                MediumLabelZebraImage.Source = CreateQRCode(vm.BarcodeData);
                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //dpw.PrintStandardVisual(MediumLabelZebraEntireBarcode);
                                break;
                        }

                        break;
                    case "Zebra - Large Label":
                        switch (CodeType)
                        {
                            case "Barcode":
                                //OFFSET FOR ZEBRA PRINTER
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //LargeLabelZebraImage.Margin = new Thickness(0, 0, 20, 0);
                                //LargeSerialLabelZebraImage.Margin = new Thickness(0, 0, 14, 0);
                                await PutTaskDelay(800);
                                dpw.PrintDirectVisualZEBRA(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);
                                //dpw.PrintStandardVisual(LargeLabelZebraEntireBarcode);
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //LargeSerialLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                //LargeLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                break;

                            case "QRCode":

                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                await PutTaskDelay(800);
                                //Regenerate Meta Label
                                LargeInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));
                                //Print
                                dpw.PrintDirectVisualZEBRA(LargeLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                LargeInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));
                                LargeLabelZebraImage.Source = CreateQRCode(vm.BarcodeData);

                                break;
                        }

                        break;
                    case "DYMO - Standard Label":

                        //if (DefaultPrinterCB.Text.Contains("DYMO"))
                        //{
                        //    dpw.PrintDirectVisual(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                        //}
                        //else
                        //{
                        //}
                        switch (CodeType)
                        {
                            case "Barcode":
                                //OFFSET FOR ZEBRA PRINTER
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 14, 0);
                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 14, 0);
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                dpw.PrintDirectVisualDYMOBROTHER(StandardLabelDYMOEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //dpw.PrintStandardVisual(LargeLabelZebraEntireBarcode);
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //LargeSerialLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                //LargeLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                break;

                            case "QRCode":
                                //QRCODE
                                //Regenerate Meta Label
                                StandardInfoLabelDYMOImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));
                                await PutTaskDelay(800);

                                //Print
                                dpw.PrintDirectVisualDYMOBROTHER(StandardLabelDYMOEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                                StandardInfoLabelDYMOImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));
                                StandardLabelDYMOImage.Source = CreateQRCode(vm.BarcodeData);

                                break;
                        }

                        break;

                    case "BROTHER - Standard Label":

                        //if (DefaultPrinterCB.Text.Contains("DYMO"))
                        //{
                        //    dpw.PrintDirectVisual(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                        //}
                        //else
                        //{
                        //}
                        switch (CodeType)
                        {
                            case "Barcode":
                                //OFFSET FOR ZEBRA PRINTER
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 14, 0);
                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 14, 0);
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                dpw.PrintDirectVisualDYMOBROTHER(StandardLabelBROTHEREntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //dpw.PrintStandardVisual(LargeLabelZebraEntireBarcode);
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //LargeSerialLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                //LargeLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                break;

                            case "QRCode":
                                //QRCODE
                                //Regenerate Meta Label
                                StandardInfoLabelBROTHERImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));

                                await PutTaskDelay(800);
                                //Print
                                dpw.PrintDirectVisualDYMOBROTHER(StandardLabelBROTHEREntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                                StandardInfoLabelBROTHERImage.Source = CreateQRCode(GenerateMetaData(numberOfCopies));
                                StandardLabelBROTHERImage.Source = CreateQRCode(vm.BarcodeData);

                                break;


                     
                                }

                                break;

                    case "Zebra - Custom Standard Label":
                        
                        switch (CodeType)
                        {
                            case "Barcode":

                                await PutTaskDelay(800);

                               

                                dpw.PrintDirectVisualZEBRA(CustomLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);

                                

                                break;

                            case "QRCode":
                                //QRCODE
                              
                                await PutTaskDelay(800);
                                //Print
                                dpw.PrintDirectVisualZEBRA(CustomLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);

                                break;
                        }

                        break;


                    case "DYMO - Custom Standard Label":

                        switch (CodeType)
                        {
                            case "Barcode":
                                //OFFSET FOR DYMO PRINTER
                               
                                await PutTaskDelay(800);
                                dpw.PrintDirectVisualDYMOBROTHER(CustomLabelDYMOEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                                //dpw.PrintDirectVisualDYMOBROTHER(CustomLabelDYMOEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);

                            

                                break;

                            case "QRCode":
                                //QRCODE
                              
                                await PutTaskDelay(800);
                                //Print
                                dpw.PrintDirectVisualDYMOBROTHER(CustomLabelDYMOEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                                //dpw.PrintDirectVisualDYMOBROTHER(CustomLabelDYMOEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);

                             

                                break;
                        }

                        break;

                    case "BROTHER - Custom Standard Label":

                        switch (CodeType)
                        {
                            case "Barcode":
                                //OFFSET FOR BROTHER PRINTER
                               
                                await PutTaskDelay(800);

                                dpw.PrintDirectVisualDYMOBROTHER(CustomLabelBROTHEREntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);

                              

                                break;

                            case "QRCode":
                                //QRCODE
                               

                                await PutTaskDelay(800);
                                //Print
                                dpw.PrintDirectVisualDYMOBROTHER(CustomLabelBROTHEREntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                                //dpw.PrintDirectVisualZEBRA(CustomLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);

                                break;


                        }

                        break;
                }

                ////Get the text from the clipboard
                //Clipboard.GetText(TextDataFormat.UnicodeText);
                // dpw.PrintStandardVisual(vm.SelectedBarcode);

                //Clear Serial Number Text Box
                SerialNoTXTB.Text = "";
                //CLEAR ORDER REF
                vm.OrderRefNo = "";
                //Clear Firmware Text Box
                vm.FWRef = "";
                //Hide EraseIT
                //EraseITIcon.Visibility = Visibility.Collapsed;

                PrintingProgressBar.IsIndeterminate = false;
                //dpw.PrintStandardVisual(TheBarcode);
            }
            else
            {
                MessageBox.Show("Please Select Your Default Label Printer!", "Printer Error");
            }
        }



        private void PrintBTNS1_Click(object sender, RoutedEventArgs e)
        {

            //MessageBox.Show(NumberOfCopiesCB.Text);

            if (ExcludeMetaCB.IsChecked == true || ExcludeAllCB.IsChecked == true)
            {

                //Print Single Label
                PrintSingleLabel();


            }
            else
            {

                //if (String.IsNullOrEmpty(POSORefTXTB.Text))
                //{
                //    MessageBox.Show("Please enter an Order Reference. Can be Purchase,Sales,RMA etc....\nFor Custom Labels, Please tick either Exclude Meta or Exclude All");
                //    POSORefTXTB.Focus();
                //    // vm.DisplayMessage("Order Reference field blank. Label not printed.","Missing Reference","","","");
                //    //vm.display
                //}
                //else
                //{
                //Print Single Label
                PrintSingleLabel();

                //}

            }






        }

        private void PrintMultiBTNS1_Click(object sender, RoutedEventArgs e)
        {


            if (ExcludeMetaCB.IsChecked == true || ExcludeAllCB.IsChecked == true)
            {

                //Print List Of Labels
                PrintMultiLabel();


            }
            else
            {


                //Print List Of Labels
                PrintMultiLabel();
            }


        }


        private void PrintSingleLabel()
        {
            if (DefaultPrinterCB.Text != "")
            {
                try
                {
                    if (SerialNoTXTB.Text == "")
                    {
                        StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        AssetLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                        //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, -30);
                        //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, -30);
                        //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, -30);
                        //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, -30);
                        //StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, -30);
                        //SerialSPZSL.Visibility = Visibility.Collapsed;
                        PrintLabels(NumberOfCopiesCB.Text);

                        StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        AssetLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                    }
                    else
                    {

                        StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        AssetLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                        //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, -30);
                        //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, -30);
                        //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, -30);
                        // StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, -30);
                        //StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, -30);
                        //SerialSPZSL.Visibility = Visibility.Visible;
                        PrintLabels(NumberOfCopiesCB.Text);

                        StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        AssetLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                    }



                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please Select Your Default Label Printer!", "Printer Error");
            }



        }


        private async void PrintMultiLabel()
        {

            try
            {

        

            vm.BarcodeData = "";
            vm.BarcodeDescription = "";
            vm.PONumber = "";
            vm.BarcodeSerialNo = "";
            vm.FWRef = "";
            //Hide EraseIT
            vm.EraseITUsed = false;
            vm.EraseITIconVisibility = Visibility.Collapsed;

             //Create collection to send to the db
             ObservableCollection<NetsuiteInventory> customLabels = new ObservableCollection<NetsuiteInventory>();

            //Check for zero print quantity entered
            if (vm.NetsuiteMultiPrint.Where(x => x.PrintQuantity == "0").Any())
            {
                ProgressMsg.Visibility = Visibility.Visible;
                ProgressMsg.Text = "An item in your list contains a zero Print Quantity! Please correct and retry.";
            }
            else
            {
                //CUSTOM LABEL MULTIPRINT
                //if (vm.NetsuiteMultiPrint.Where(x => x.PONumber != "").Any())
                //{

                //Capture label index
                int previousLabelIndex = LabelSizeCB.SelectedIndex;

                if (DefaultPrinterCB.Text != "")
                {

                    //Loop Through all items in the multi print LV And Generate and print
                    foreach (NetsuiteInventory selItm in MultiPrintLV.Items)
                    {

                

                    //CHECK IF CUSTOM LABEL
                    if (selItm.ProductDescription == "Test Report Label")
                    {

                           
                            switch (vm.SelectedBarcode)
                            {
                             case "Zebra - Standard Label":
                                 //*** Important *** Set Custom Label
                                 LabelSizeCB.SelectedIndex = 6;
                                 vm.SelectedBarcode = "Zebra - Custom Standard Label";

                                 break;
                             case "DYMO - Standard Label":
                                 //*** Important *** Set Custom Label
                                 LabelSizeCB.SelectedIndex = 7;
                                 vm.SelectedBarcode = "DYMO - Custom Standard Label";
                                 break;
                             case "BROTHER - Standard Label":
                                 //*** Important *** Set Custom Label
                                 LabelSizeCB.SelectedIndex = 8;
                                 vm.SelectedBarcode = "BROTHER - Custom Standard Label";
                                 break;
                            }

                          
                       
                        //Set Technician Field
                        vm.BarcodeTestedBy = vm.CurrentUserInitials.Replace("Technician Name:", "").Trim() + " " + DateTime.Now.ToShortDateString();
                        selItm.TestedByOperative = vm.BarcodeTestedBy;



                            //if (SerialNoTXTB.Text == "")
                            //{
                            
                            StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            AssetLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            CustomLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            CustomLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            CustomLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                            ////Check if hide meta is checked
                            if (ExcludeMetaCB.IsChecked == true && ExcludeAllCB.IsChecked == false)
                                {

                                    //Set back to default
                                    ProductCodeFontCB.SelectedIndex = 5;
                                    DescriptionCodeFontCB.SelectedIndex = 0;
                                  

                                    vm.ShowInfoTitle = Visibility.Hidden;
                                    vm.ShowInfoQR = Visibility.Hidden;
                                    vm.ShowPartQR = Visibility.Visible;

                                }



                                ProgressMsg.Visibility = Visibility.Visible;
                                ProgressRing.IsActive = true;
                       

                            //MessageBox.Show(selItm.ProductDescription + " || " + selItm.ProductCode + " || " + selItm.PrintQuantity);
                            //Populate The Field Data
                            vm.OrderRefNo = selItm.PONumber;
                            //Set Serial Number
                            vm.BarcodeSerialNo = selItm.SerialNumber;
                            vm.FWRef = selItm.CurrentFirmware;

                          
                            //Add to Observable Collection to be used to add to database
                            customLabels.Add(selItm);
                          

                            //ProgressMsg.Text = "Now Printing " + selItm.ProductCode;
                            //Generate Labels
                            LabelsToGenerate();
                            await PutTaskDelay(1500);
                            //print number of copies
                            PrintLabels(selItm.PrintQuantity.ToString());

                            await PutTaskDelay(1000);



                            //vm.PONumber = "";
                            //vm.BarcodeSerialNo = "";

                            //}

                        }
                    else
                    {

                      
                        //*** Important *** Set previous label index
                        LabelSizeCB.SelectedIndex = previousLabelIndex;
                        //vm.SelectedBarcode = "Zebra - Custom Standard Label";


                        //NORMAL LABEL

                        
                        StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        AssetLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        CustomLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);


                            ProgressMsg.Visibility = Visibility.Visible;
                            ProgressRing.IsActive = true;
                          
                            //Populate The Field Data
                            vm.BarcodeDescription = selItm.ProductDescription;
                            vm.BarcodeData = selItm.ProductCode;
                            vm.PONumber = selItm.PONumber;
                            vm.BarcodeSerialNo = selItm.SerialNumber;
                            vm.FWRef = selItm.CurrentFirmware;
                            //ERASE IT
                                if (selItm.EraseITUsed == true)
                                {
                                    //Show EraseIT
                                    vm.EraseITUsed = true;
                                    vm.EraseITIconVisibility = Visibility.Visible;
                                }
                                else
                                {

                                    //Hide EraseIT
                                    vm.EraseITUsed = false;
                                    vm.EraseITIconVisibility = Visibility.Collapsed;
                                }


                              //MessageBox.Show("Label Generated");
                                //ProgressMsg.Text = "Now Printing " + selItm.ProductCode;
                                //Generate Labels
                                LabelsToGenerate();
                            await PutTaskDelay(1500);
                            //print number of copies
                            PrintLabels(selItm.PrintQuantity.ToString());

                            await PutTaskDelay(1000);
                            //}

                           
                            StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            AssetLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            CustomLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            CustomLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                            CustomLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                        }
                    }//loop end

                    
                    //LABEL DB INSERT
                    //Insert to DB using Transaction
                    //await System.Threading.Tasks.Task.Run(() => InsertCustomLabelsDB(customLabels));

                   

                    //Labels finished printing reset values
                    ProgressMsg.Text = "Printing Complete!";
                    await PutTaskDelay(2000);

                   //Clear multi print list
                   //foreach (var itm in vm.NetsuiteMultiPrint)
                   //{

                   //}


                   // vm.NetsuiteMultiPrint.Clear();
                    vm.BarcodeData = "";
                    vm.BarcodeDescription = "";
                    vm.PONumber = "";
                    vm.BarcodeSerialNo = "";
                    vm.FWRef = "";
                    //Hide EraseIT
                    vm.EraseITUsed = false;
                    vm.EraseITIconVisibility = Visibility.Collapsed;

                    //Hide progress
                    ProgressMsg.Visibility = Visibility.Collapsed;
                    ProgressRing.IsActive = false;

                    //*** Important *** Set previous label index
                    LabelSizeCB.SelectedIndex = previousLabelIndex;


                }
                else
                {

                    vm.BarcodeData = "";
                    vm.BarcodeDescription = "";
                    vm.PONumber = "";
                    vm.BarcodeSerialNo = "";
                    vm.FWRef = "";

                    //*** Important *** Set previous label index
                    LabelSizeCB.SelectedIndex = previousLabelIndex;

                    //Hide progress
                    ProgressMsg.Visibility = Visibility.Collapsed;
                    ProgressRing.IsActive = false;
                    MessageBox.Show("Please Select Your Default Label Printer!", "Printer Error");
                    return;
                }



            }


            }
            catch (Exception ex)
            {
                vm.DialogMessage("Multi-Print Exception", ex.Message);
            }

        }


        private async void PrintMultiTestReportLabel()
        {

            vm.BarcodeData = "";
            vm.BarcodeDescription = "";
            vm.PONumber = "";
            vm.BarcodeSerialNo = "";

            //Create collection to send to the db
            ObservableCollection<NetsuiteInventory> customLabels = new ObservableCollection<NetsuiteInventory>();

            //Check if items are selected
            if(RePrintLV.SelectedItems.Count == 0)
            {
                vm.SystemMessageDialog("Nebula Notification", "Please select the labels you require to print!");
            }
            else
            {
                //Check for zero print quantity entered
                if (vm.NetsuiteMultiPrint.Where(x => x.PrintQuantity == "0").Any())
                {
                    ProgressMsg.Visibility = Visibility.Visible;
                    ProgressMsg.Text = "An item in your list contains a zero Print Quantity! Please correct and retry.";
                }
                else
                {
                    //CUSTOM LABEL MULTIPRINT
                    //if (vm.NetsuiteMultiPrint.Where(x => x.PONumber != "").Any())
                    //{

                    //Capture label index
                    int previousLabelIndex = LabelSizeCB.SelectedIndex;

                    if (DefaultPrinterCB.Text != "")
                    {

                        //Loop Through all items in the multi print LV And Generate and print
                        foreach (NetsuiteInventory selItm in RePrintLV.SelectedItems)
                        {



                            //CHECK IF CUSTOM LABEL
                            if (selItm.ProductDescription == "Test Report Label")
                            {


                                switch (vm.SelectedBarcode)
                                {
                                    case "Zebra - Standard Label":
                                        //*** Important *** Set Custom Label
                                        LabelSizeCB.SelectedIndex = 6;
                                        vm.SelectedBarcode = "Zebra - Custom Standard Label";

                                        break;
                                    case "DYMO - Standard Label":
                                        //*** Important *** Set Custom Label
                                        LabelSizeCB.SelectedIndex = 7;
                                        vm.SelectedBarcode = "DYMO - Custom Standard Label";
                                        break;
                                    case "BROTHER - Standard Label":
                                        //*** Important *** Set Custom Label
                                        LabelSizeCB.SelectedIndex = 8;
                                        vm.SelectedBarcode = "BROTHER - Custom Standard Label";
                                        break;
                                }



                                //Set Technician Field
                                //vm.BarcodeTestedBy = vm.CurrentUserInitials.Replace("Technician Name:", "").Trim() + " " + DateTime.Now.ToShortDateString();
                                //selItm.TestedByOperative = vm.BarcodeTestedBy;

                                vm.BarcodeTestedBy = selItm.TestedByOperative;
                           
                                //Populate The Field Data
                                vm.OrderRefNo = selItm.PONumber;
                                //Set Serial Number
                                vm.BarcodeSerialNo = selItm.SerialNumber;


                                //if (SerialNoTXTB.Text == "")
                                //{

                                StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                AssetLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                StandardLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                CustomLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                CustomLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                CustomLabelBROTHEREntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                                ////Check if hide meta is checked
                                if (ExcludeMetaCB.IsChecked == true && ExcludeAllCB.IsChecked == false)
                                {

                                    //Set back to default
                                    ProductCodeFontCB.SelectedIndex = 5;
                                    DescriptionCodeFontCB.SelectedIndex = 0;


                                    vm.ShowInfoTitle = Visibility.Hidden;
                                    vm.ShowInfoQR = Visibility.Hidden;
                                    vm.ShowPartQR = Visibility.Visible;

                                }



                                ProgressMsg.Visibility = Visibility.Visible;
                                ProgressRing.IsActive = true;


                           

                                //Add to Observable Collection to be used to add to database
                                customLabels.Add(selItm);


                                //ProgressMsg.Text = "Now Printing " + selItm.ProductCode;
                                //Generate Labels
                                LabelsToGenerate();
                                await PutTaskDelay(1500);
                                //print number of copies
                                PrintLabels(selItm.PrintQuantity.ToString());

                                await PutTaskDelay(1000);


                            }

                        }

                        //*** Important *** Set previous label index
                        LabelSizeCB.SelectedIndex = previousLabelIndex;

                        //Labels finished printing reset values
                        ProgressMsg.Text = "Printing Complete!";
                        await PutTaskDelay(2000);

                        //Clear multi print list
                        vm.TestReportLabels.Clear();
                        vm.BarcodeData = "";
                        vm.BarcodeDescription = "";
                        vm.PONumber = "";
                        vm.BarcodeSerialNo = "";

                        //Hide progress
                        ProgressMsg.Visibility = Visibility.Collapsed;
                        ProgressRing.IsActive = false;


                    }
                    else
                    {

                        vm.BarcodeData = "";
                        vm.BarcodeDescription = "";
                        vm.PONumber = "";
                        vm.BarcodeSerialNo = "";

                        //*** Important *** Set previous label index
                        LabelSizeCB.SelectedIndex = previousLabelIndex;

                        //Hide progress
                        ProgressMsg.Visibility = Visibility.Collapsed;
                        ProgressRing.IsActive = false;
                        vm.SystemMessageDialog("Printer Error", "Please Select Your Default Label Printer!");
                      
                        return;
                    }
                }
            }

      
        }



        private void ClearMultiBTNS1_Click(object sender, RoutedEventArgs e)
        {
            vm.NetsuiteMultiPrint.Clear();
        }

        private void ImportMultiBTNS1_Click(object sender, RoutedEventArgs e)
        {

            //IMPORT CSV 
            //LOOP THROUGH LIST AND QUERY BARCODE using System.Windows.Forms;

            using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
            {

                //Requires GUID for MyComputer Or ThisPC Folder
                openFileDialog.InitialDirectory = vm.downld; //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                openFileDialog.Filter = "All files (*.*)|*.*";
                openFileDialog.FilterIndex = 0;
                openFileDialog.Multiselect = true;
                openFileDialog.RestoreDirectory = true;

                // string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");

                //MessageBox.Show(rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime);

                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    foreach (var file in openFileDialog.FileNames)
                    {

                        // MessageBox.Show(file);
                        if (File.Exists(file))
                        {
                            //Will Create a new file for each year

                            //Uses a VB class to process the file instead of streamreader
                            using (TextFieldParser parser = new TextFieldParser(file))
                            {
                                parser.TextFieldType = FieldType.Delimited;
                                parser.SetDelimiters(",");
                                while (!parser.EndOfData)
                                {
                                    //Processing row
                                    string currentLine = parser.ReadLine();
                                    if (currentLine.Contains("Quantity") || currentLine.Contains("Server Build CTO") || currentLine.Contains("End of Group") || currentLine.ToUpper().Contains("VARIOUS") || currentLine.ToUpper().Contains("FAILED PART"))
                                    {
                                        //Dont include headers
                                    }
                                    else
                                    {
                                        vm.NetsuiteMultiPrint.Add(FromCsv(currentLine));
                                    }



                                }
                            }
                        }

                    }


                }

            }

        }

        public NetsuiteInventory FromCsv(string csvLine)
        {
            try
            {


                if (csvLine != null)
                {

                    Regex CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                    string[] values = CSVParser.Split(csvLine);
                    //string[] values = csvLine.Split(',');


                    NetsuiteInventory sproc = new NetsuiteInventory();

                    switch (values.Count().ToString())
                    {
                        case "5":
                            if (LabelSizeCB.Text == "Zebra - Asset Label")
                            {

                                //Set Label As Asset Label if not already
                                LabelSizeCB.SelectedIndex = 1;

                                if (values[0] != string.Empty)
                                    sproc.ProductCode = Convert.ToString(values[0]);
                                if (values[1] != string.Empty)
                                    sproc.ProductDescription = Convert.ToString(values[1]);
                                if (values[2] != string.Empty)
                                    sproc.CurrentFirmware = Convert.ToString(values[2]);
                                //Replace any negative vaules
                                if (values[3] != string.Empty)
                                {

                                    if (sproc.ProductDescription != null)
                                    {
                                        //Check if Memory and reduce quantity to 1, to reduce
                                        if (sproc.ProductDescription.Contains(" MEMORY ") || sproc.ProductDescription.Contains(" Memory ") || sproc.ProductDescription.Contains(" memory ") || sproc.ProductDescription.Contains(" MEM ") || sproc.ProductDescription.Contains(" mem ")
                                            && sproc.ProductDescription.Contains("DIMM ") || sproc.ProductDescription.Contains(" Dimm") || sproc.ProductDescription.Contains(" KIT") || sproc.ProductDescription.Contains(" Kit") || sproc.ProductDescription.Contains(" kit")
                                            || sproc.ProductDescription.Contains(" ECC ") || sproc.ProductDescription.Contains(" Ecc ") || sproc.ProductDescription.Contains(" ecc ") || sproc.ProductDescription.Contains(" DDR") || sproc.ProductDescription.Contains(" Ddr") || sproc.ProductDescription.Contains(" ddr"))
                                        {
                                            sproc.PrintQuantity = "1";

                                        }
                                        else
                                        {
                                            sproc.PrintQuantity = Convert.ToString(values[3].Replace("-", ""));
                                        }
                                    }
                                    else
                                    {
                                        sproc.PrintQuantity = Convert.ToString(values[3].Replace("-", ""));
                                    }
                                    //}
                                }
                            }
                            else
                            {


                                if (values[0] != string.Empty)
                                    sproc.ProductCode = Convert.ToString(values[0]);
                                if (values[1] != string.Empty)
                                    sproc.ProductDescription = Convert.ToString(values[1]);
                                //Replace any negative vaules
                                if (values[3] != string.Empty)
                                {
                                    if (sproc.ProductDescription != null)
                                    {
                                        //Check if Memory and reduce quantity to 1, to reduce
                                        if (sproc.ProductDescription.Contains(" MEMORY ") || sproc.ProductDescription.Contains(" Memory ") || sproc.ProductDescription.Contains(" memory ") || sproc.ProductDescription.Contains(" MEM ") || sproc.ProductDescription.Contains(" mem ")
                                            && sproc.ProductDescription.Contains("DIMM ") || sproc.ProductDescription.Contains(" Dimm") || sproc.ProductDescription.Contains(" KIT") || sproc.ProductDescription.Contains(" Kit") || sproc.ProductDescription.Contains(" kit")
                                            || sproc.ProductDescription.Contains(" ECC ") || sproc.ProductDescription.Contains(" Ecc ") || sproc.ProductDescription.Contains(" ecc ") || sproc.ProductDescription.Contains(" DDR") || sproc.ProductDescription.Contains(" Ddr") || sproc.ProductDescription.Contains(" ddr"))
                                        {
                                            sproc.PrintQuantity = "1";

                                        }
                                        else
                                        {
                                            sproc.PrintQuantity = Convert.ToString(values[3].Replace("-", ""));
                                        }
                                    }
                                    else
                                    {
                                        sproc.PrintQuantity = Convert.ToString(values[3].Replace("-", ""));
                                    }

                                }

                            }
                            break;
                        case "6":
                            //ITAD ASSET LABEL IMPORT
                            if (LabelSizeCB.Text == "Zebra - Asset Label")
                            {

                                //Set Label As Asset Label if not already
                                LabelSizeCB.SelectedIndex = 1;


                                if (values[0] != string.Empty)
                                    sproc.ProductCode = Convert.ToString(values[0]);
                                if (values[1] != string.Empty)
                                    sproc.ProductDescription = Convert.ToString(values[1]);
                                if (values[2] != string.Empty)
                                    sproc.SerialNumber = Convert.ToString(values[2]);
                                //Replace any negative vaules
                                if (values[4] != string.Empty)
                                {

                                    if (sproc.ProductDescription != null)
                                    {
                                        //Check if Memory and reduce quantity to 1, to reduce
                                        if (sproc.ProductDescription.Contains(" MEMORY ") || sproc.ProductDescription.Contains(" Memory ") || sproc.ProductDescription.Contains(" memory ") || sproc.ProductDescription.Contains(" MEM ") || sproc.ProductDescription.Contains(" mem ")
                                            && sproc.ProductDescription.Contains("DIMM ") || sproc.ProductDescription.Contains(" Dimm") || sproc.ProductDescription.Contains(" KIT") || sproc.ProductDescription.Contains(" Kit") || sproc.ProductDescription.Contains(" kit")
                                            || sproc.ProductDescription.Contains(" ECC ") || sproc.ProductDescription.Contains(" Ecc ") || sproc.ProductDescription.Contains(" ecc ") || sproc.ProductDescription.Contains(" DDR") || sproc.ProductDescription.Contains(" Ddr") || sproc.ProductDescription.Contains(" ddr"))
                                        {
                                            sproc.PrintQuantity = "1";

                                        }
                                        else
                                        {
                                            sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                        }
                                    }
                                    else
                                    {
                                        sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                    }

                                }
                                else
                                {
                                    if (values[1] != string.Empty)
                                        sproc.ProductCode = Convert.ToString(values[1]);
                                    if (values[2] != string.Empty)
                                        sproc.ProductDescription = Convert.ToString(values[2]);
                                    //Replace any negative vaules
                                    if (values[4] != string.Empty)
                                    {

                                        if (sproc.ProductDescription != null)
                                        {
                                            //Check if Memory and reduce quantity to 1, to reduce
                                            if (sproc.ProductDescription.Contains(" MEMORY ") || sproc.ProductDescription.Contains(" Memory ") || sproc.ProductDescription.Contains(" memory ") || sproc.ProductDescription.Contains(" MEM ") || sproc.ProductDescription.Contains(" mem ")
                                                && sproc.ProductDescription.Contains("DIMM ") || sproc.ProductDescription.Contains(" Dimm") || sproc.ProductDescription.Contains(" KIT") || sproc.ProductDescription.Contains(" Kit") || sproc.ProductDescription.Contains(" kit")
                                                || sproc.ProductDescription.Contains(" ECC ") || sproc.ProductDescription.Contains(" Ecc ") || sproc.ProductDescription.Contains(" ecc ") || sproc.ProductDescription.Contains(" DDR") || sproc.ProductDescription.Contains(" Ddr") || sproc.ProductDescription.Contains(" ddr"))
                                            {
                                                sproc.PrintQuantity = "1";

                                            }
                                            else
                                            {
                                                sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                            }
                                        }
                                        else
                                        {
                                            sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                        }
                                        //}
                                    }
                                }
                            }
                         
                            break;
                        case "7":
                            //ITAD ASSET LABEL IMPORT
                            if (LabelSizeCB.Text == "Zebra - Asset Label")
                            {

                                //Set Label As Asset Label if not already
                                LabelSizeCB.SelectedIndex = 1;

                                if (values[0] != string.Empty)
                                    sproc.ProductDescription = Convert.ToString(values[0]);
                                if (values[1] != string.Empty)
                                    sproc.ProductCode = Convert.ToString(values[1]);
                               
                                //Replace any negative vaules
                                if (values[5] != string.Empty)
                                {

                                    if (sproc.ProductDescription != null)
                                    {
                                        //Check if Memory and reduce quantity to 1, to reduce
                                        if (sproc.ProductDescription.Contains(" MEMORY ") || sproc.ProductDescription.Contains(" Memory ") || sproc.ProductDescription.Contains(" memory ") || sproc.ProductDescription.Contains(" MEM ") || sproc.ProductDescription.Contains(" mem ")
                                            && sproc.ProductDescription.Contains("DIMM ") || sproc.ProductDescription.Contains(" Dimm") || sproc.ProductDescription.Contains(" KIT") || sproc.ProductDescription.Contains(" Kit") || sproc.ProductDescription.Contains(" kit")
                                            || sproc.ProductDescription.Contains(" ECC ") || sproc.ProductDescription.Contains(" Ecc ") || sproc.ProductDescription.Contains(" ecc ") || sproc.ProductDescription.Contains(" DDR") || sproc.ProductDescription.Contains(" Ddr") || sproc.ProductDescription.Contains(" ddr"))
                                        {
                                            sproc.PrintQuantity = "1";

                                        }
                                        else
                                        {
                                            sproc.PrintQuantity = Convert.ToString(values[5].Replace("-", ""));
                                        }
                                    }
                                    else
                                    {
                                        sproc.PrintQuantity = Convert.ToString(values[5].Replace("-", ""));
                                    }
                                    //}
                                }
                            }
                            else
                            {
                                if (values[2] != string.Empty)
                                    sproc.ProductCode = Convert.ToString(values[2]);
                                if (values[3] != string.Empty)
                                    sproc.ProductDescription = Convert.ToString(values[3]);
                                //Replace any negative vaules
                                if (values[5] != string.Empty)
                                {

                                    if (sproc.ProductDescription != null)
                                    {
                                        //Check if Memory and reduce quantity to 1, to reduce
                                        if (sproc.ProductDescription.Contains(" MEMORY ") || sproc.ProductDescription.Contains(" Memory ") || sproc.ProductDescription.Contains(" memory ") || sproc.ProductDescription.Contains(" MEM ") || sproc.ProductDescription.Contains(" mem ")
                                            && sproc.ProductDescription.Contains("DIMM ") || sproc.ProductDescription.Contains(" Dimm") || sproc.ProductDescription.Contains(" KIT") || sproc.ProductDescription.Contains(" Kit") || sproc.ProductDescription.Contains(" kit")
                                            || sproc.ProductDescription.Contains(" ECC ") || sproc.ProductDescription.Contains(" Ecc ") || sproc.ProductDescription.Contains(" ecc ") || sproc.ProductDescription.Contains(" DDR") || sproc.ProductDescription.Contains(" Ddr") || sproc.ProductDescription.Contains(" ddr"))
                                        {
                                            sproc.PrintQuantity = "1";

                                        }
                                        else
                                        {
                                            sproc.PrintQuantity = Convert.ToString(values[5].Replace("-", ""));
                                        }
                                    }
                                    else
                                    {
                                        sproc.PrintQuantity = Convert.ToString(values[5].Replace("-", ""));
                                    }
                                    //}
                                }
                            }

                          
                            break;
                        case "8":
                            if (values[1] != string.Empty)
                                sproc.ProductCode = Convert.ToString(values[1]);
                            if (values[2] != string.Empty)
                                sproc.ProductDescription = Convert.ToString(values[2]);
                            //Replace any negative vaules
                            if (values[4] != string.Empty)
                            {
                                if (sproc.ProductDescription != null)
                                {
                                    //Check if Memory and reduce quantity to 1, to reduce
                                    if (sproc.ProductDescription.Contains(" MEMORY ") || sproc.ProductDescription.Contains(" Memory ") || sproc.ProductDescription.Contains(" memory ") || sproc.ProductDescription.Contains(" MEM ") || sproc.ProductDescription.Contains(" mem ")
                                        && sproc.ProductDescription.Contains("DIMM ") || sproc.ProductDescription.Contains(" Dimm") || sproc.ProductDescription.Contains(" KIT") || sproc.ProductDescription.Contains(" Kit") || sproc.ProductDescription.Contains(" kit")
                                        || sproc.ProductDescription.Contains(" ECC ") || sproc.ProductDescription.Contains(" Ecc ") || sproc.ProductDescription.Contains(" ecc ") || sproc.ProductDescription.Contains(" DDR") || sproc.ProductDescription.Contains(" Ddr") || sproc.ProductDescription.Contains(" ddr"))
                                    {
                                        sproc.PrintQuantity = "1";

                                    }
                                    else
                                    {
                                        sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                    }
                                }
                                else
                                {
                                    sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                }
                            }
                            break;
                        //Extra Column with PO\SO\RMA Number
                        case "10":
                            if (values[1] != string.Empty)
                                sproc.ProductCode = Convert.ToString(values[1]);
                            if (values[2] != string.Empty)
                                sproc.ProductDescription = Convert.ToString(values[2]);
                            //Replace any negative vaules
                            if (values[4] != string.Empty)
                            {
                                //Check if Memory and reduce quantity to 1, to reduce
                                if (sproc.ProductDescription != null)
                                {
                                    //Check if Memory and reduce quantity to 1, to reduce
                                    if (sproc.ProductDescription.Contains(" MEMORY ") || sproc.ProductDescription.Contains(" Memory ") || sproc.ProductDescription.Contains(" memory ") || sproc.ProductDescription.Contains(" MEM ") || sproc.ProductDescription.Contains(" mem ")
                                        && sproc.ProductDescription.Contains("DIMM ") || sproc.ProductDescription.Contains(" Dimm") || sproc.ProductDescription.Contains(" KIT") || sproc.ProductDescription.Contains(" Kit") || sproc.ProductDescription.Contains(" kit")
                                        || sproc.ProductDescription.Contains(" ECC ") || sproc.ProductDescription.Contains(" Ecc ") || sproc.ProductDescription.Contains(" ecc ") || sproc.ProductDescription.Contains(" DDR") || sproc.ProductDescription.Contains(" Ddr") || sproc.ProductDescription.Contains(" ddr"))
                                    {
                                        sproc.PrintQuantity = "1";

                                    }
                                    else
                                    {
                                        sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                    }
                                }
                                else
                                {
                                    sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                }
                            }
                            break;
                    }




                    return sproc;



                }

                return null;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

        }



        private void DeleteSelectedBTNS1_Click(object sender, RoutedEventArgs e)
        {
            try
            {


                List<NetsuiteInventory> DeleteList = new List<NetsuiteInventory>();

                foreach (NetsuiteInventory itm in MultiPrintLV.SelectedItems)
                {

                    DeleteList.Add(itm);
                    // MessageBox.Show(itm.ProductCode);
                }

                foreach (NetsuiteInventory itm in DeleteList)
                {


                    vm.NetsuiteMultiPrint.Remove(itm);
                    // MessageBox.Show(itm.ProductCode);
                }


                DeleteList.Clear();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        private void SearchTXTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Clear Description and Barcode Data
            vm.BarcodeDescription = "";
            vm.BarcodeData = "";

        }

        private void NumberOfCopiesCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Recreate the META QR Code. 
            //if(vm.BarcodeDescription != "")
            //{
            //    MessageBox.Show(DateTime.Now.ToShortDateString() + "-" + vm.CurrentUserInitials + "-" + NumberOfCopiesCB.Text);

            //    StandardInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData());
            //    MediumInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData());
            //    LargeInfoLabelZebraImage.Source = CreateQRCode(GenerateMetaData());
            //    StandardInfoLabelDYMOImage.Source = CreateQRCode(GenerateMetaData());
            //    StandardInfoLabelBROTHERImage.Source = CreateQRCode(GenerateMetaData());
            //}


        }

     

        private void POSORefTXTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            //generate a new label
            GenerateLabel();
        }

        private async void BarcodeUC_Unloaded(object sender, RoutedEventArgs e)
        {
            //clear collections
            vm.NetsuiteQueryData.Clear();
            vm.NetsuiteMultiPrint.Clear();
            //Close any excels down

            if (app != null)
            {
                //Terminate PID Instance
                TerminateExcelProcess(app);

            }

        }

        private void AddRangeBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Clear list
                vm.NetsuiteMultiPrint.Clear();
                //Set Description Font

                //Set QR Options
                ExcludeMetaCB.IsChecked = true;
                ExcludeAllCB.IsChecked = false;



                for (int i = Convert.ToInt32(RangeFromTXT.Text); i < Convert.ToInt32(RangeToTXT.Text) + 1; i++)
                {
                    //// Add number range labels 
                    /// if (Enumerable.Range(1,100).Contains(x))

                    //Add range label to list
                    NetsuiteInventory nsi = new NetsuiteInventory();
                    nsi.PrintQuantity = "1";
                    // Use Decimal Format Specifier and length of string to add leading zero's
                    nsi.ProductCode = i.ToString("D" + RangeFromTXT.Text.Length.ToString());
                    //nsi.ProductDescription = i.ToString("D" + RangeFromTXT.Text.Length.ToString()) + " of " + RangeToTXT.Text;
                    if (String.IsNullOrEmpty(DescTXT.Text))
                    {
                        //Just add the number as a description value
                        nsi.ProductDescription = "# " + i.ToString("D" + RangeFromTXT.Text.Length.ToString());
                    }
                    else
                    {
                        //Add the specified description
                        nsi.ProductDescription = DescTXT.Text;
                    }

                    nsi.SerialNumber = "";
                    vm.NetsuiteMultiPrint.Add(nsi);

                    //Clear Serial Field
                    vm.BarcodeSerialNo = "";

                }


            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }



        }

        private void MotherboardPresetCB_Checked(object sender, RoutedEventArgs e)
        {
            if (MotherboardPresetCB.IsChecked == true)
            {


                vm.ExcludeAll = true;
                ExcludeAllCB.IsEnabled = false;
                ExcludeMetaCB.IsEnabled = false;

                if (LabelSizeCB.Text == "Zebra - Asset Label")
                {
                    //Set Description font size
                    DescriptionCodeFontCB.SelectedIndex = 5;
                }
                else
                {
                    //Set Description font size
                    DescriptionCodeFontCB.SelectedIndex = 5;
                }
                

                //vm.ExcludeMeta = true;
                //MotherboardPresetCB.IsEnabled = false;
                //ExcludeMetaCB.IsEnabled = false;
                vm.ShowInfoQR = Visibility.Collapsed;
                vm.ShowInfoTitle = Visibility.Collapsed;
                vm.ShowSerialQR = Visibility.Collapsed;
                vm.ShowPartQR = Visibility.Collapsed;



                //Populate fields with predefined text. Add newline on end
                vm.BarcodeDescription = App.Current.FindResource("QRLblMainboardTitle1").ToString() + "\n"; // "System Board Spares\n";
                vm.BarcodeData = App.Current.FindResource("QRLblMainboardTitle2").ToString() + "\n"; // "Controller PN\n";
                vm.BarcodeImage = null;
                vm.OrderRefNo = "";

                //Set multiline on textboxes
                DescriptionTXT.AcceptsReturn = true;
                PartCodeTXT.AcceptsReturn = true;

                // Set Focus on Description Text Box
                DescriptionTXT.Focus();
                DescriptionTXT.CaretIndex = DescriptionTXT.Text.Length;
                //DescriptionTXT.ScrollToEnd();
                //TRIGGER CAPS LOCK
                const int KEYEVENTF_EXTENDEDKEY = 0x1;
                const int KEYEVENTF_KEYUP = 0x2;
                keybd_event(0x14, 0x45, KEYEVENTF_EXTENDEDKEY, (UIntPtr)0);
                keybd_event(0x14, 0x45, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP,
                (UIntPtr)0);


                //R2 Check, Exclude UK Itad, purely for C1
                if (vm.CurrentUserLocation == "United Kingdom")
                {

                    foreach (var ptr in DefaultPrinterCB.Items)
                    {
                        //Console.WriteLine(ptr);
                        //Capture UKCLS
                        if (ptr.ToString().Contains("ZT230-FrontWH") || ptr.ToString().Contains(@"\\ukcls") || ptr.ToString().Contains(@"\\sageserver"))
                        {
                            R2ZebSTD.Visibility = Visibility.Collapsed;
                            R2ZebMED.Visibility = Visibility.Collapsed;
                            R2ZebLRG.Visibility = Visibility.Collapsed;
                            R2DymoSTD.Visibility = Visibility.Collapsed;
                            R2BroSTD.Visibility = Visibility.Collapsed;
                            R2ZebCUS.Visibility = Visibility.Collapsed;
                            R2DymoCUS.Visibility = Visibility.Collapsed;
                            R2BroCUS.Visibility = Visibility.Collapsed;
                        }
                        else if (ptr.ToString().Contains("ITAD") || ptr.ToString().Contains("GB-ITAD"))
                        {
                            R2ZebSTD.Visibility = Visibility.Collapsed;
                            R2ZebMED.Visibility = Visibility.Collapsed;
                            R2ZebLRG.Visibility = Visibility.Collapsed;
                            R2DymoSTD.Visibility = Visibility.Collapsed;
                            R2BroSTD.Visibility = Visibility.Collapsed;
                            R2ZebCUS.Visibility = Visibility.Collapsed;
                            R2DymoCUS.Visibility = Visibility.Collapsed;
                            R2BroCUS.Visibility = Visibility.Collapsed;
                        }
                    }
                }

             

                StandardLabelZebraImage.Visibility = Visibility.Hidden;
                StandardZebraDesc.Width = 240;
                StandardZebraDesc.Margin = new Thickness(0, 20, 0, 10);
                StandardLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);

                AssetLabelZebraImage.Visibility = Visibility.Hidden;
                AssetZebraDesc.Width = 186;
                AssetZebraDesc.Margin = new Thickness(0, 20, 0, 10);
                AssetLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);

                MediumLabelZebraImage.Visibility = Visibility.Hidden;
                MediumZebraDesc.Width = 260;
                MediumZebraDesc.Margin = new Thickness(0, 20, 0, 10);
                MediumLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);

                LargeLabelZebraImage.Visibility = Visibility.Hidden;
                LargeZebraDesc.Width = 260;
                LargeZebraDesc.Margin = new Thickness(0, 20, 0, 10);
                LargeLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);

                StandardLabelDYMOImage.Visibility = Visibility.Hidden;
                StandardDYMODesc.Width = 240;
                StandardDYMODesc.Margin = new Thickness(0, 20, 0, 10);
                StandardLabelDYMOImage.Margin = new Thickness(0, 0, 0, 0);

                StandardLabelBROTHERImage.Visibility = Visibility.Hidden;
                StandardBROTHERDesc.Width = 240;
                StandardBROTHERDesc.Margin = new Thickness(0, 20, 0, 10);
                StandardLabelBROTHERImage.Margin = new Thickness(0, 0, 0, 0);

                //CustomLabelZebraEntireBarcode.Visibility = Visibility.Hidden;
                //CustomLabelDYMOEntireBarcode.Visibility = Visibility.Hidden;
                //CustomLabelBROTHEREntireBarcode.Visibility = Visibility.Hidden;

                //generate a new label
                GenerateLabel();


            }


        }

        private void MotherboardPresetCB_Unchecked(object sender, RoutedEventArgs e)
        {
            if (MotherboardPresetCB.IsChecked == false)
            {


                vm.BarcodeDescription = "";
                vm.BarcodeData = "";
                vm.BarcodeImage = null;

                //If Asset Label set font size higher
                if (LabelSizeCB.Text == "Zebra - Asset Label")
                {
                    //Set Description font size
                    DescriptionCodeFontCB.SelectedIndex = 5;
                }
                else
                {
                    //Set Description font size
                    DescriptionCodeFontCB.SelectedIndex = 0;
                }
             
                //vm.ExcludeMeta = false;
                //MotherboardPresetCB.IsEnabled = true;
                //ExcludeMetaCB.IsEnabled = true;
                //UnSet multiline on textboxes
                DescriptionTXT.AcceptsReturn = false;
                PartCodeTXT.AcceptsReturn = false;
                

                //vm.ExcludeAll = false;
                ExcludeAllCB.IsEnabled = true;
                ExcludeMetaCB.IsEnabled = true;
                vm.ExcludeAll = false;

               
             


                //TRIGGER CAPS LOCK
                const int KEYEVENTF_EXTENDEDKEY = 0x1;
                const int KEYEVENTF_KEYUP = 0x2;
                keybd_event(0x14, 0x45, KEYEVENTF_EXTENDEDKEY, (UIntPtr)0);
                keybd_event(0x14, 0x45, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP,
                (UIntPtr)0);

                StandardLabelZebraImage.Visibility = Visibility.Visible;
                StandardZebraDesc.Width = 240;
                StandardZebraDesc.Margin = new Thickness(0, 0, 0, 0);
                StandardLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                
                AssetLabelZebraImage.Visibility = Visibility.Visible;
                AssetZebraDesc.Width = 186;
                AssetZebraDesc.Margin = new Thickness(0, 0, 0, 0);
                AssetLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                
                MediumLabelZebraImage.Visibility = Visibility.Visible;
                MediumZebraDesc.Width = 260;
                MediumZebraDesc.Margin = new Thickness(0, 0, 0, 0);
                MediumLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
               
                LargeLabelZebraImage.Visibility = Visibility.Visible;
                LargeZebraDesc.Width = 260;
                LargeZebraDesc.Margin = new Thickness(0, 0, 0, 0);
                LargeLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
             
                StandardLabelDYMOImage.Visibility = Visibility.Visible;
                StandardDYMODesc.Width = 240;
                StandardDYMODesc.Margin = new Thickness(0, 0, 0, 0);
                StandardLabelDYMOImage.Margin = new Thickness(0, 0, 0, 0);
             
                StandardLabelBROTHERImage.Visibility = Visibility.Visible;
                StandardBROTHERDesc.Width = 240;
                StandardBROTHERDesc.Margin = new Thickness(0, 0, 0, 0);
                StandardLabelBROTHERImage.Margin = new Thickness(0, 0, 0, 0);

                //CustomLabelZebraEntireBarcode.Visibility = Visibility.Visible;
                //CustomLabelDYMOEntireBarcode.Visibility = Visibility.Visible;
                //CustomLabelBROTHEREntireBarcode.Visibility = Visibility.Visible;

               

                if (MotherboardPresetCB.IsChecked == false && ExcludeAllCB.IsChecked == false && ExcludeMetaCB.IsChecked == false)
                {
                    if(string.IsNullOrEmpty(vm.BarcodeDescription) && string.IsNullOrEmpty(vm.BarcodeData))
                    {
                        if(LabelSizeCB.Text == "Zebra - Asset Label")
                        {
                            vm.ShowInfoQR = Visibility.Visible;
                            vm.ShowInfoTitle = Visibility.Visible;
                            vm.ShowSerialQR = Visibility.Visible;
                            vm.ShowPartQR = Visibility.Visible;
                        }
                  
                    }
                    else
                    {
                        vm.ShowInfoQR = Visibility.Visible;
                        vm.ShowInfoTitle = Visibility.Visible;
                        //Check if Serial has any text in it
                        if (!string.IsNullOrEmpty(SerialNoTXTB.Text))
                        {
                            vm.ShowSerialQR = Visibility.Visible;
                        }
                        if (!string.IsNullOrEmpty(PartCodeTXT.Text))
                        {
                            vm.ShowPartQR = Visibility.Visible;
                        }
                      
                    }
              
                }




                //generate a new label
                GenerateLabel();

            }


        }


        private void ExcludeMetaCB_Checked(object sender, RoutedEventArgs e)
        {
            if (vm.ExcludeMeta == true)
            {
                //MotherboardPresetCB.IsEnabled = false;
                vm.ShowInfoQR = Visibility.Collapsed;
                vm.ShowInfoTitle = Visibility.Collapsed;
                vm.ShowSerialQR = Visibility.Collapsed;
                //vm.ShowPartQR = Visibility.Collapsed;
                vm.OrderRefNo = "";
                POSORefTXTB.Focus();


                //generate a new label
                GenerateLabel();
            }

        }

        private void ExcludeMetaCB_Unchecked(object sender, RoutedEventArgs e)
        {
            if (vm.ExcludeMeta == false)
            {
                //MotherboardPresetCB.IsEnabled = true;
                vm.ShowInfoQR = Visibility.Visible;
                vm.ShowInfoTitle = Visibility.Visible;
                //Check if Serial has any text in it
                if (!string.IsNullOrEmpty(SerialNoTXTB.Text))
                {
                    vm.ShowSerialQR = Visibility.Visible;
                }
                vm.ShowPartQR = Visibility.Visible;
                vm.OrderRefNo = "";

                POSORefTXTB.Focus();

                //generate a new label
                GenerateLabel();
            }
        }

        private void ExcludeAllCB_Checked(object sender, RoutedEventArgs e)
        {
            if (vm.ExcludeAll == true)
            {
                vm.ExcludeMeta = true;
                //MotherboardPresetCB.IsEnabled = false;
                //ExcludeMetaCB.IsEnabled = false;
                vm.ShowInfoQR = Visibility.Collapsed;
                vm.ShowInfoTitle = Visibility.Collapsed;
                vm.ShowSerialQR = Visibility.Collapsed;
                vm.ShowPartQR = Visibility.Collapsed;
                vm.BarcodeDescription = "";
                vm.BarcodeData = "";
                //R2 Check, Exclude UK Itad, purely for C1
                if (vm.CurrentUserLocation == "United Kingdom")
                {

                    foreach (var ptr in DefaultPrinterCB.Items)
                    {
                        //Console.WriteLine(ptr);
                        //Capture UKCLS
                        if (ptr.ToString().Contains("ZT230-FrontWH") || ptr.ToString().Contains(@"\\ukcls") || ptr.ToString().Contains(@"\\sageserver"))
                        {
                            R2ZebSTD.Visibility = Visibility.Collapsed;
                            R2ZebMED.Visibility = Visibility.Collapsed;
                            R2ZebLRG.Visibility = Visibility.Collapsed;
                            R2DymoSTD.Visibility = Visibility.Collapsed;
                            R2BroSTD.Visibility = Visibility.Collapsed;
                        }
                        else if (ptr.ToString().Contains("ITAD") || ptr.ToString().Contains("GB-ITAD"))
                        {
                            R2ZebSTD.Visibility = Visibility.Collapsed;
                            R2ZebMED.Visibility = Visibility.Collapsed;
                            R2ZebLRG.Visibility = Visibility.Collapsed;
                            R2DymoSTD.Visibility = Visibility.Collapsed;
                            R2BroSTD.Visibility = Visibility.Collapsed;
                        }
                    }
                }


                vm.OrderRefNo = "";
                QueryResultsLV.SelectedIndex = -1;
                //Set Description font size
                DescriptionCodeFontCB.SelectedIndex = 5;

                StandardLabelZebraImage.Visibility = Visibility.Hidden;
                StandardZebraDesc.Width = 240;
                StandardZebraDesc.Margin = new Thickness(0, 20, 0, 30);
                StandardLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
             
                AssetLabelZebraImage.Visibility = Visibility.Hidden;
                AssetZebraDesc.Width = 186;
                AssetZebraDesc.Margin = new Thickness(0, 0, 0, 0);
                AssetLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);

                MediumLabelZebraImage.Visibility = Visibility.Hidden;
                MediumZebraDesc.Width = 260;
                MediumZebraDesc.Margin = new Thickness(0, 20, 0, 30);
                MediumLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);

                LargeLabelZebraImage.Visibility = Visibility.Hidden;
                LargeZebraDesc.Width = 260;
                LargeZebraDesc.Margin = new Thickness(0, 20, 0, 30);
                LargeLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);

                StandardLabelDYMOImage.Visibility = Visibility.Hidden;
                StandardDYMODesc.Width = 240;
                StandardDYMODesc.Margin = new Thickness(0, 20, 0, 30);
                StandardLabelDYMOImage.Margin = new Thickness(0, 0, 0, 0);

                StandardLabelBROTHERImage.Visibility = Visibility.Hidden;
                StandardBROTHERDesc.Width = 240;
                StandardBROTHERDesc.Margin = new Thickness(0, 20, 0, 30);
                StandardLabelBROTHERImage.Margin = new Thickness(0, 0, 0, 0);


                //generate a new label
                GenerateLabel();

            }
        }

        private void ExcludeAllCB_Unchecked(object sender, RoutedEventArgs e)
        {
            if (vm.ExcludeAll == false)
            {
                vm.ExcludeMeta = false;
                //MotherboardPresetCB.IsEnabled = true;
                //ExcludeMetaCB.IsEnabled = true;
                if (string.IsNullOrEmpty(vm.BarcodeDescription) && string.IsNullOrEmpty(vm.BarcodeData))
                {

                }
                else
                {
                    vm.ShowInfoQR = Visibility.Visible;
                    vm.ShowInfoTitle = Visibility.Visible;
                    //Check if Serial has any text in it
                    if (!string.IsNullOrEmpty(SerialNoTXTB.Text))
                    {
                        vm.ShowSerialQR = Visibility.Visible;
                    }
                    vm.ShowPartQR = Visibility.Visible;
                }


                //R2 Check, Exclude UK Itad, purely for C1
                if (vm.CurrentUserLocation == "United Kingdom")
                {

                    foreach (var ptr in DefaultPrinterCB.Items)
                    {
                        //Console.WriteLine(ptr);
                        //Capture UKCLS
                        if (ptr.ToString().Contains("ZT230-FrontWH") || ptr.ToString().Contains(@"\\ukcls") || ptr.ToString().Contains(@"\\sageserver"))
                        {
                            R2ZebSTD.Visibility = Visibility.Visible;
                            R2ZebMED.Visibility = Visibility.Visible;
                            R2ZebLRG.Visibility = Visibility.Visible;
                            R2DymoSTD.Visibility = Visibility.Visible;
                            R2BroSTD.Visibility = Visibility.Visible;
                        }
                        else if (ptr.ToString().Contains("ITAD") || ptr.ToString().Contains("GB-ITAD"))
                        {
                            R2ZebSTD.Visibility = Visibility.Collapsed;
                            R2ZebMED.Visibility = Visibility.Collapsed;
                            R2ZebLRG.Visibility = Visibility.Collapsed;
                            R2DymoSTD.Visibility = Visibility.Collapsed;
                            R2BroSTD.Visibility = Visibility.Collapsed;
                        }
                    }
                }


                QueryResultsLV.SelectedIndex = 0;
                vm.OrderRefNo = "";

                StandardLabelZebraImage.Visibility = Visibility.Visible;
                StandardZebraDesc.Width = 240;
                StandardZebraDesc.Margin = new Thickness(0, 0, 0, 0);
                StandardLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
           
                AssetLabelZebraImage.Visibility = Visibility.Visible;
                AssetZebraDesc.Width = 186;
                AssetZebraDesc.Margin = new Thickness(0, 0, 0, 0);
                AssetLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
               
                MediumLabelZebraImage.Visibility = Visibility.Visible;
                MediumZebraDesc.Width = 260;
                MediumZebraDesc.Margin = new Thickness(0, 0, 0, 0);
                MediumLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                
                LargeLabelZebraImage.Visibility = Visibility.Visible;
                LargeZebraDesc.Width = 260;
                LargeZebraDesc.Margin = new Thickness(0, 0, 0, 0);
                LargeLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
               
                StandardLabelDYMOImage.Visibility = Visibility.Visible;
                StandardDYMODesc.Width = 240;
                StandardDYMODesc.Margin = new Thickness(0, 0, 0, 0);
                StandardLabelDYMOImage.Margin = new Thickness(0, 0, 0, 0);
              
                StandardLabelBROTHERImage.Visibility = Visibility.Visible;
                StandardBROTHERDesc.Width = 240;
                StandardBROTHERDesc.Margin = new Thickness(0, 0, 0, 0);
                StandardLabelBROTHERImage.Margin = new Thickness(0, 0, 0, 0);


                //generate a new label
                GenerateLabel();
            }
        }

        private async void FindBTNS_Click(object sender, RoutedEventArgs e)
        {

            //Query DB for Labels
            FindLabels();

        }


        public async void FindLabels()
        {
            //SEARCH LabelDetails DB
            try
            {

          

            string search = SearchTXTB2.Text;

            //check for empty search filter
            if (string.IsNullOrEmpty(search))
            {
                //Clear collection
                vm.TestReportLabels.Clear();


                //GET DATE VALUES SELECTED ON THE DATEPICKERS
                var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                var dateTo = vm.DateTo.ToString("yyyy-MM-dd");
                //Get Results with filter
                await Task.Run(() => SearchLabelsDB("SELECT * FROM LabelRecords WHERE DateCreated BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ORDER BY  POSONumber ASC"));
                //await Task.Run(() => SearchLabelsDB("SELECT * FROM LabelRecords ORDER BY SerialNo ASC"));

               // vm.SystemMessageDialog("Nebula Notification", "Please enter an Order Reference or Serial No");

            }
            else
            {
                //Clear collection
                vm.TestReportLabels.Clear();


                //Get Results with filter
                await Task.Run(() => SearchLabelsDB("SELECT * FROM LabelRecords WHERE SerialNo LIKE '" + search + "' OR POSONumber LIKE '" + search + "' ORDER BY  POSONumber ASC"));
            }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        public void SearchLabelsDB(string sqlquery)
        {

            //Set String to DB Path
            string cs = "";


            //Point DB to Test Databases
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\LabelDetails.db";
            }
            else
            {

                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/LabelDetails/LabelDetails.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/LabelDetails/LabelDetails.db";
                }
                else
                {

                    //Set connection string to Live DB
                    //Use DFS Hosted DB for multiple site access
                    //cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/LabelDetails.db";
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\LabelDetails.db";
                }
               
            }

            //Connection to SQL Lite DB
            //if all criteria met create the job
            SQLiteConnection conn;
            conn = CreateConnection(cs);


            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = sqlquery;

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            while (sqlite_datareader.Read())
            {


                ITADReport itm = new ITADReport();
                if (sqlite_datareader.GetString(1) != null)
                    itm.DateCreated = sqlite_datareader.GetDateTime(1);
                if (sqlite_datareader.GetString(2) != null)
                    itm.POSONumber= sqlite_datareader.GetString(2);
                if (sqlite_datareader.GetString(3) != null)
                    itm.AssetNo = sqlite_datareader.GetString(3);
                if (sqlite_datareader.GetString(4) != null)
                    itm.SerialNo = sqlite_datareader.GetString(4);
                if (sqlite_datareader.GetString(5) != null)
                    itm.Manufacturer = sqlite_datareader.GetString(5);
                if (sqlite_datareader.GetString(6) != null)
                    itm.ModelNo = sqlite_datareader.GetString(6);
                if (sqlite_datareader.GetString(7) != null)
                    itm.DeviceCategory = sqlite_datareader.GetString(7);
                if (sqlite_datareader.GetString(8) != null)
                    itm.ProcessPerformed = sqlite_datareader.GetString(8);
                if (sqlite_datareader.GetString(9) != null)
                    itm.TestingPerformed = sqlite_datareader.GetString(9);
                if (sqlite_datareader.GetString(10) != null)
                    itm.Notes = sqlite_datareader.GetString(10);
                if (sqlite_datareader.GetString(11) != null)
                    itm.ReportBody = sqlite_datareader.GetString(11);
                if (sqlite_datareader.GetString(12) != null)
                    itm.SavePath = sqlite_datareader.GetString(12);
                if (sqlite_datareader.GetString(13) != null)
                    itm.TestedBy = sqlite_datareader.GetString(13);
               

                //Create 
                NetsuiteInventory label = new NetsuiteInventory();
                label.ProcessedDate = itm.DateCreated;
                label.ProductCode = "Custom";
                label.ProductDescription = "Test Report Label";
                label.PONumber = itm.POSONumber;
                label.SerialNumber = itm.SerialNo;
                label.PrintQuantity = "1";
                label.ReportPath = itm.SavePath;
                label.TestedByOperative = itm.TestedBy;



                //Update Collection via dispatcher
                App.Current.Dispatcher.Invoke((System.Action)delegate // <--- HERE
                {
                    vm.TestReportLabels.Add(label);
                 
                });


            }



        }


        //DB
        //INSERT Custom Labels into DB
        public void InsertCustomLabelsDB(ObservableCollection<NetsuiteInventory> LabelList)
        {

            //Set String to DB Path
            string cs = "";


            //Point DB to Test Databases
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\LabelDetails.db";
            }
            else
            {
                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/LabelDetails/LabelDetails.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/LabelDetails/LabelDetails.db";
                }
                else
                {

                    //Set connection string to Live DB
                    //Use DFS Hosted DB for multiple site access
                    //cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/LabelDetails.db";
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\LabelDetails.db";
                }
            }

            //Connection to SQL Lite DB
            //if all criteria met create the job
            SQLiteConnection conn;
            conn = CreateConnection(cs);

            //Create Query List
            List<string> myQueries = new List<string>();



            //ADD TO DB

            try
            {

                //LOOP THROUGH ADDED COLLECTION
                foreach (NetsuiteInventory itm in LabelList)
                {
                    //Console.WriteLine(itm.PONumber  + " " + itm.SerialNumber + " " + itm.TestedByOperative);
                    //Add query to list
                    myQueries.Add("INSERT INTO LabelRecords (DateCreated, POSONumber, AssetNo, SerialNo, Manufacturer, ModelNo, DeviceCategory, ProcessPerformed, TestingPerformed, Notes, ReportBody, SavePath, TestedBy)" +
                        " VALUES('" + DateTime.Now.ToString("yyyy-MM-dd") + "','" + itm.PONumber + "','" + "" + "','" + itm.SerialNumber + "','" + "" + "','" + "" + "','" + "" + "','" + "" + "','" + "" + "','" + "" + "','" + "" + "','" + itm.ReportPath + "','" + itm.TestedByOperative + "')");

                }




                //Create a transaction
                using (var tra = conn.BeginTransaction())
                {
                    try
                    {
                        //Console.WriteLine(myQueries.Count().ToString() + "\n");

                        foreach (var myQuery in myQueries)
                        {
                            //Console.WriteLine(myQuery.ToString());

                            using (var cd = new SQLiteCommand(myQuery, conn, tra))
                            {
                                cd.ExecuteNonQuery();
                            }
                        }

                        tra.Commit();
                    }
                    catch (Exception ex)
                    {
                        tra.Rollback();
                        //Console.Error.Writeline("I did nothing, because something wrong happened: {0}", ex);
                        throw;
                    }
                }


                //Close DB
                conn.Close();


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        //Create Connection to DB
        static SQLiteConnection CreateConnection(string pathtodb)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message);
            }
            return sqlite_conn;
        }

        private void PrintMultiBTNS2_Click(object sender, RoutedEventArgs e)
        {
            PrintMultiTestReportLabel();
        }

       
        private void ClearMultiBTNS2_Click(object sender, RoutedEventArgs e)
        {
            vm.TestReportLabels.Clear();
        }

        private void SearchTXTB2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //Query DB for Labels
                FindLabels();
            }
        }

        private void LaunchBTN_Click(object sender, RoutedEventArgs e)
        {
            //Loop through the selected items and launch the report
            foreach(NetsuiteInventory selectedLabels in RePrintLV.SelectedItems)
            {

                if (File.Exists(selectedLabels.ReportPath))
                {
                    //Open the CSV
                    StaticFunctions.OpenFileCommand(@"" + selectedLabels.ReportPath, "");
                }

            }


           
        }

        private async void EmailReportsBTN_Click(object sender, RoutedEventArgs e)
        {
            // Get Search value
            string orderRef = RefTXTB.Text;

            if (string.IsNullOrEmpty(orderRef))
            {

                //Send Message
                vm.SystemMessageDialog("ZIP & Email Reports", "Please enter a valid reference");

            }
            else
            {

                try
                {

           

                // Use the Path.Combine method to safely append the file name to the path.
                if (Directory.Exists(vm.myDocs + @"\" + orderRef.ToUpper() + "_Reports"))
                {
                    //Remove if already exists
                    Directory.Delete(vm.myDocs + @"\" + orderRef.ToUpper() + "_Reports", true);

                    //Create Temp Folder
                    Directory.CreateDirectory(vm.myDocs + @"\" + orderRef.ToUpper() + "_Reports");

                }
                else
                {
                    //Create Temp Folder
                    Directory.CreateDirectory(vm.myDocs + @"\" + orderRef.ToUpper() + "_Reports");
                }


                    string fName = "";

                    // MessageBox.Show(vm.TestReportLabels.Count().ToString());
                    //Loop through collection
                    foreach (NetsuiteInventory nsi in vm.TestReportLabels)
                    {
                    fName = nsi.ReportPath;

                    if (File.Exists(fName))
                    {

                      


                        //Copy to temp directory prior to zipping
                       if (Directory.Exists(vm.myDocs + @"\" + orderRef.ToUpper() + "_Reports"))
                       {
                           //Copy all files based off search 
                           File.Copy(fName, System.IO.Path.Combine(vm.myDocs + @"\" + orderRef.ToUpper() + "_Reports" + @"\" + fName.Substring(fName.LastIndexOf(@"\") + 1)), true);

                       }

                       
                    }

                       // MessageBox.Show(fName.ToString());
                        // MessageBox.Show(System.IO.Path.Combine(vm.myDocs + @"\" + orderRef.ToUpper() + "_Reports" + @"\" + fName.Substring(fName.LastIndexOf(@"\") + 1)));

                }// end foreach


               // MessageBox.Show("Prior to zip");

               //if(vm.myDocs + @"\" + orderRef.ToUpper() + "_Reports" + @"\" + fName.Substring(fName.LastIndexOf(@"\") + 1))
               //{

               //}

                //zip folder and attach to email 
                EmailClass zipemail = new EmailClass(vm);
                await System.Threading.Tasks.Task.Run(() => zipemail.SearchZipFolderAndSendMail(vm.myDocs + @"\" + orderRef.ToUpper() + "_Reports"));


                RefTXTB.Text = "";

                }
                catch (Exception ex)
                {
                    //Display custom dialog
                    vm.SystemMessageDialog("Label Printing", ex.Message);
                }

            }

        }

        private void DeleteSelectedBTNS2_Click(object sender, RoutedEventArgs e)
        {
            //Select all items
        }

        private void SelectAllBTNS2_Click(object sender, RoutedEventArgs e)
        {
            //Check list is populated
            if(RePrintLV.Items.Count > 0)
            {

                //Select all items
                RePrintLV.SelectAll();
             
            }
         
        }

        private void ClearSelectionBTNS2_Click(object sender, RoutedEventArgs e)
        {
            //Clear selected items
            RePrintLV.SelectedItems.Clear();
        }

        private void EraseITCB_Checked(object sender, RoutedEventArgs e)
        {
            if (vm.EraseITUsed == true)
            {
                //Clear other Checkboxes
                vm.ExcludeMeta = false;
                vm.ExcludeAll = false;
                MotherboardPresetCB.IsChecked = false;
                //Show icon for EraseIT Wiped
                vm.EraseITIconVisibility = Visibility.Visible;
            }

        }

        private void EraseITCB_Unchecked(object sender, RoutedEventArgs e)
        {
            if (vm.EraseITUsed == false)
            {
                //Clear other Checkboxes
                vm.ExcludeMeta = false;
                vm.ExcludeAll = false;
                MotherboardPresetCB.IsChecked = false;
                //Hide icon for EraseIT Wiped 
                vm.EraseITIconVisibility = Visibility.Collapsed;
            }
        }

        private void DescriptionTXT_GotFocus(object sender, RoutedEventArgs e)
        {
           
            //Select all text when got focus
            if (MotherboardPresetCB.IsChecked == false)
            {
                DescriptionTXT.SelectAll();
            }
           
        }

        private void PartCodeTXT_GotFocus(object sender, RoutedEventArgs e)
        {
           
            //Select all text when got focus
            if (MotherboardPresetCB.IsChecked == false)
            {
                PartCodeTXT.SelectAll();
            }
        }



        private void DescriptionTXT_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //Select all text when got focus
            if (MotherboardPresetCB.IsChecked == false)
            {
                DescriptionTXT.SelectAll();
            }
        }

        private void PartCodeTXT_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //Select all text when got focus
            if (MotherboardPresetCB.IsChecked == false)
            {
                PartCodeTXT.SelectAll();
            }
        }
    }
}
