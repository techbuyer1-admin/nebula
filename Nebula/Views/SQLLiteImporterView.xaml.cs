﻿using Microsoft.VisualBasic.FileIO;
using Nebula.Models;
using Nebula.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for SQLLiteImporterView.xaml
    /// </summary>
    public partial class SQLLiteImporterView : UserControl
    {

        MainViewModel vm;

        public SQLLiteImporterView()
        {
            InitializeComponent();
        }


        public SQLLiteImporterView(MainViewModel passedViewModel)
        {
            InitializeComponent();

            vm = passedViewModel;

            this.DataContext = vm;
        }

        private void ImportSQLLiteBTN_Click(object sender, RoutedEventArgs e)
        {

            //IMPORT CSV 
            //LOOP THROUGH LIST AND QUERY BARCODE using System.Windows.Forms;
            //https://www.pluralsight.com/guides/building-a-generic-csv-writer-reader-using-reflection

            using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
                {


                    //Requires GUID for MyComputer Or ThisPC Folder
                    openFileDialog.InitialDirectory = vm.downld; //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                    openFileDialog.Filter = "CSV files (*.csv)|*.csv";
                    openFileDialog.FilterIndex = 0;
                    openFileDialog.Multiselect = true;
                    openFileDialog.RestoreDirectory = true;

                    Mouse.OverrideCursor = Cursors.Wait;

                    //Clear import collection
                    //vm.ImportSerialsCollection.Clear();

                    if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        foreach (var file in openFileDialog.FileNames)
                        {

                            // MessageBox.Show(file);
                            if (File.Exists(file))
                            {

                                ////Uses a VB class to process the file instead of streamreader
                                using (TextFieldParser parser = new TextFieldParser(file))
                                {
                                    //parser.TextFieldType = FieldType.Delimited;
                                    //parser.SetDelimiters(",");
                                    while (!parser.EndOfData)
                                    {
                                        //Processing row
                                        string currentLine = parser.ReadLine();


                                        //New method to handle commas in field data
                                        Regex Regexparser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                                        string[] partserial = Regexparser.Split(currentLine);

                                        ImportSerials imp = new ImportSerials();
                                        imp.partcode = partserial[0];
                                        imp.serialno = partserial[1];

                                        //if (vm.ImportSerialsCollection.Where(x => x.serialno == partserial[1]).Any())
                                        //{
                                        //    vm.ImportMessage = "Duplicate Serial found! Please correct the CSV & import again. Serial: " + partserial[1];
                                        //    vm.ImportMessageColour = Brushes.Red;
                                        //    vm.ProgressIsActive = false;
                                        //    vm.ProcessedMessageColour = Brushes.Red;

                                        //    //Change mouse cursor 
                                        //    Mouse.OverrideCursor = null;
                                        //    //Exit Import
                                        //    return;
                                        //}
                                        //else
                                        //{
                                        //    //Import to the collection
                                        //    vm.ImportSerialsCollection.Add(imp);
                                        //}




                                    }
                                }
                            }
                        }
                    }


                //        int LineCount = 0;

                //        //ADD TO SCANNED SERIAL COLLECTION HERE

                //        foreach (NetsuiteInventory nsi in vm.NetsuiteImportCollection)
                //        {
                //            //Set as Zero
                //            LineCount = 0;

                //            //Set Serials remaining to Quantity
                //            nsi.SerialsRemaining = Convert.ToInt32(nsi.Quantity);


                //            //Loop through Serial Import and add if 
                //            foreach (ImportSerials imps in vm.ImportSerialsCollection)
                //            {

                //                //Check if import serial partcode matches selected line
                //                if (imps.partcode == nsi.ProductCode)
                //                {
                //                    NetsuiteInventory ns = new NetsuiteInventory();
                //                    ns.PONumber = vm.PONumber;
                //                    ns.ProductCode = nsi.ProductCode;
                //                    ns.ProductDescription = nsi.ProductDescription;
                //                    ns.SerialNumber = imps.serialno;
                //                    ns.Quantity = "1";
                //                    ns.ReceivedByOperative = vm.CurrentUserInitials;
                //                    ns.POQuantity = nsi.Quantity;
                //                    ns.POLineUnitPrice = nsi.POLineUnitPrice;
                //                    ns.POUnitAmount = nsi.POUnitAmount;
                //                    ns.ReportPath = "";
                //                    ns.TestedByOperative = "";
                //                    ns.DriveFailedReason = "";
                //                    ns.DriveFailedReasonSpecify = "";
                //                    ns.DriveResult = "PENDING";
                //                    ns.WipeSoftware = "";
                //                    ns.ITADRef = "";


                //                    //get how many are already scanned
                //                    nsi.SerialsRemaining -= 1;
                //                    //Increment LineCount
                //                    LineCount += 1;


                //                    //Check serials remaining of current line and mark as done
                //                    if (nsi.SerialsRemaining == 0)
                //                    {
                //                        //Set Complete Status if all serials are scannned
                //                        nsi.Complete = "Done";

                //                    }
                //                    //Add to Scanned Serial Collection
                //                    vm.ScannedSerialCollection.Add(ns);
                //                }


                //            }

                //            //Check if blank count and skip adding
                //            if (LineCount != 0)
                //            {

                //                //Add to Line Comparison Collection
                //                NetsuiteInventory nst = new NetsuiteInventory();
                //                nst.ProductCode = nsi.ProductCode;
                //                nst.ProductDescription = nsi.ProductDescription;
                //                nst.LineType = nsi.LineType;
                //                nst.Quantity = LineCount.ToString();

                //                vm.NetsuiteScannedCollection.Add(nst);

                //            }


                //        }


                //        Mouse.OverrideCursor = null;

                //    }

                //    //Deselect item in the list
                //    lvValue.SelectedIndex = 0;
                //    vm.ImportMessageColour = Brushes.Green;
                //    vm.ImportMessage = "Please Add All Extra's before starting the Serial Scan!";

            }
        }

        private void SelectSQLLiteDBTN_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
