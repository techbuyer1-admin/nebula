﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.ViewModels;
using Nebula.Views;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for GoodsOutToolsView.xaml
    /// </summary>
    public partial class GoodsOutToolsView : UserControl
    {
        public GoodsOutToolsView()
        {
            //set the static departments var for use in Server Page Loading
            StaticFunctions.Department = "GoodsOutTech";
            InitializeComponent();
        }

      
    }
}
