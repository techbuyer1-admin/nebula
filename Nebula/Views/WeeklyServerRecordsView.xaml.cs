﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;
using System.Diagnostics;
using System.IO;
using System.Web.Script.Serialization;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Net.Security;
using Newtonsoft.Json;
using System.Xml;
using System.Data;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using Nebula.Paginators;
using System.IO.Packaging;
using System.Windows.Markup;
using System.Windows.Xps.Packaging;
using MahApps.Metro.Controls.Dialogs;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for WeeklyServerRecordsView.xaml
    /// </summary>
    public partial class WeeklyServerRecordsView : UserControl
    {

        ////declare viewmodel
        GoodsInOutViewModel vm;

        //if all criteria met create the job
        SQLiteConnection sqlite_conn;

        //SQL Connection string      
        public string cs = "";

     
        public WeeklyServerRecordsView()
        {
            InitializeComponent();

            //Check if Test Mode Checked
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\WeeklyRecords.db";
            }
            else
            {

                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/WeeklyRecords/WeeklyRecords.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/WeeklyRecords/WeeklyRecords.db";
                }
                else
                {

                    //Set connection string to Live DB
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\WeeklyRecords.db";
                }

            
            }

        }


        public WeeklyServerRecordsView(GoodsInOutViewModel PassedViewModel)
        {
            InitializeComponent();

            vm = PassedViewModel;
            DataContext = vm;


            //Hide Report button 
            //Console.WriteLine(vm.FullUser);
            if(vm.FullUser.Contains("N.Myers") || vm.FullUser.Contains("F.Borghese"))
            {
                ExportUserBTN.Visibility = Visibility.Visible;
            }
            else
            {
                //Hide for all others
                ExportUserBTN.Visibility = Visibility.Collapsed;
            }

          
            //Check if Test Mode Checked
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\WeeklyRecords.db";
            }
            else
            {
                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/WeeklyRecords/WeeklyRecords.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/WeeklyRecords/WeeklyRecords.db";
                }
                else
                {

                    //Set connection string to Live DB
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\WeeklyRecords.db";
                }
            
            }

            //Get data for current week
            GetInitialData();

        }

        //Get initial data separated out to allow async function to run
        public async void GetInitialData()
        {


            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            vm.WeeklyServerRecordCollection.Clear();

            //Set Initial Dates for Weekly Records
            vm.DateFrom = DateTimeExtensions.StartOfWeek(DateTime.Now, DayOfWeek.Monday);
            vm.DateTo = DateTime.Now;

            //Monday To Monday
            await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE date(Day) BETWEEN date('" + vm.DateFrom.ToString("yyyy-MM-dd") + "') AND date('" + vm.DateTo.ToString("yyyy-MM-dd") + "') ORDER BY id DESC"));

            //Set current filter
            vm.CurrentFilter = "Monday To Monday";

            //All Data
            //ReadAllData(sqlite_conn);
            sqlite_conn.Close();


        }



        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime dt = DateTime.Now.StartOfWeek(DayOfWeek.Monday);

            if(dt.ToShortDateString() == DateTime.Now.ToShortDateString())
            {
               // MessageBox.Show("Check if file already exists, if not Create a new XML File for the week " + dt.ToShortDateString());
            }
            else
            {
              // MessageBox.Show("Days Not Matched  " + dt.ToShortDateString());
            }


           

        }


        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

       

     

        private void TESTBTN2_Click(object sender, RoutedEventArgs e)
        {
        }

        private void TestCodeButton_Click(object sender, RoutedEventArgs e)
        {
            //            The following is the procedure to generate the class:
            //Copy JSON or XML string. JSON. ...
            //Go to Edit > Paste Sepcial > Paste JSON As Classes or Paste XML As Classes.
            //Visual Studio generates a class structure for the developer as in the following:
            //26 Jul 2015


            // string ServerInfoJson;

            // //Full Info
            // using (StreamReader r = new StreamReader(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\192.168.101.248All.json"))
            // {
            //     //STRIP OUT THE INCORRECT HEADERS THAT STOP JSON.NET READING CORRECTLY
            //     ServerInfoJson = r.ReadToEnd().Replace("HpBios.1.0.0", ".")
            //                                   .Replace("/rest/v1/Systems/1/Bios", "/")
            //                                   .Replace("Chassis.1.0.0", ".")
            //                                   .Replace("/rest/v1/Chassis/1", "/")
            //                                   .Replace("FwSwVersionInventory.1.2.0", ".")
            //                                   .Replace("/rest/v1/Systems/1/FirmwareInventory", "/")
            //                                   .Replace("ComputerSystem.1.0.1", ".")
            //                                   .Replace("/rest/v1/Systems/1", "/")
            //                                   .Replace("HpiLOLicense.1.0.0", ".")
            //                                   .Replace("/rest/v1/Managers/1", "/")
            //                                   .Replace("Manager.1.0.0", ".")
            //                                   .Replace("/rest/v1/Managers/1", "/");

            // }


            // //declare json object

            // HPBIOS myserverinfo = new HPBIOS();




            // JArray arrayinfo = JArray.Parse(ServerInfoJson);
            // //MessageBox.Show(arrayinfo.Count.ToString()); 


            //// HPBIOS myserverinfo = JsonConvert.DeserializeObject<HPBIOS>(JArray.Parse(arrayinfo[0].ToString()));

            // //myserverinfo.ServerInfo[0].Comments

            // //= JObject.Parse(arrayinfo[0].Type);
            // //JObject comments = JObject.Parse(arrayinfo[0].ToString());
            // //JObject hpbios = JObject.Parse(arrayinfo[1].ToString());
            // //JObject chassis = JObject.Parse(arrayinfo[2].ToString());
            // //JObject firmware = JObject.Parse(arrayinfo[3].ToString());
            // //JObject computersystem = JObject.Parse(arrayinfo[4].ToString());
            // //JObject manager = JObject.Parse(arrayinfo[5].ToString());

            // JObject comments = JObject.Parse(arrayinfo[0].ToString());
            // JObject hpbios = JObject.Parse(arrayinfo[1].ToString());
            // JObject chassis = JObject.Parse(arrayinfo[2].ToString());
            // JObject firmware = JObject.Parse(arrayinfo[3].ToString());
            // JObject computersystem = JObject.Parse(arrayinfo[4].ToString());
            // JObject manager = JObject.Parse(arrayinfo[5].ToString());

            // // JsonParser.ReadFullServerJsonTest(ServerInfoJson);

            // //myserverinfo.ServerInfo[0].Comments = comments;//JsonConvert.DeserializeObject<HPBIOS>(comments);





            //FOR WRITING THE BIOS AND ILO FIRMWARE RIBCL FILES

            string biosfwPath = @"C:\Users\N.Myers\Desktop\FTPFirmware\BIOS_Firmware\CPQP7013.bin\";
            string ilofwPath = @"C:\Users\N.Myers\Desktop\FTPFirmware\iLO_Firmware\ilo4_273.bin\";

            //MessageBox.Show(ServerInfoJson);
            //var x = new XmlDocument();
            //x.Load(@"C:\Users\N.Myers\Documents\HPTOOLS\HPCONFIGUTIL\ServerHeatlh.xml");
            //var ns = x.SelectSingleNode("GET_EMBEDDED_HEALTH_DATA");

            //var res = ns.OuterXml;

            //bios firmware file to create
            string BIOSFWFile = "<RIBCL VERSION=\"2.0\">\r" +
                                  " <LOGIN USER_LOGIN=\"Administrator\" PASSWORD=\"password\">\r" +
                                  "<RIB_INFO MODE=\"write\">\r" +
                                  "<TPM_ENABLED VALUE=\"Yes\"/>\r" +
                                  "<UPDATE_RIB_FIRMWARE IMAGE_LOCATION=\"" + biosfwPath + "\"/>\r" +
                                  "</RIB_INFO>\r" +
                                  "</LOGIN>\r" +
                                  "</RIBCL>";
            //ilo firmware file to create
            string ILOFWFile =  "<RIBCL VERSION=\"2.0\">\r" +
                                   " <LOGIN USER_LOGIN=\"Administrator\" PASSWORD=\"password\">\r" +
                                   "<RIB_INFO MODE=\"write\">\r" +
                                   "<TPM_ENABLED VALUE=\"Yes\"/>\r" +
                                   "<UPDATE_RIB_FIRMWARE IMAGE_LOCATION=\"" + ilofwPath + "\"/>\r" +
                                   "</RIB_INFO>\r" +
                                   "</LOGIN>\r" +
                                   "</RIBCL>";

            File.WriteAllText(@"C:\Users\N.Myers\Documents\HPTOOLS\HPCONFIGUTIL\BIOS_FirmwareUpdate.xml", BIOSFWFile);


            File.WriteAllText(@"C:\Users\N.Myers\Documents\HPTOOLS\HPCONFIGUTIL\iLO_FirmwareUpdate.xml", ILOFWFile);










            ///OLD TOOL METHOD FOR XML
            ///
            string ServerInfoJson;

            //Full Info
            using (StreamReader r = new StreamReader(@"C:\Users\N.Myers\Documents\HPTOOLS\HPCONFIGUTIL\ServerHeatlh.xml"))
            {
                //STRIP OUT THE INVALID HEADERS THAT STOP JSON.NET READING CORRECTLY
                ServerInfoJson = r.ReadToEnd().Replace("Server IP is : 192.168.101.248:443", "")
                                              .Replace("<?xml version=\"1.0\"?>", "")
                                              .Replace("<RIBCL VERSION=\"2.23\">", "")
                                              .Replace("<RESPONSE", "")
                                              .Replace("  STATUS=\"0x0000\"", "")
                                              .Replace("MESSAGE='No error'", "")
                                              .Replace("   />", "")
                                              .Replace("</RIBCL>", "")
                                              .Replace("Script succeeded for IP:192.168.101.248:443", "").Trim();
                                             

            }

         

            //StaticFunctions.
            File.WriteAllText(@"C:\Users\N.Myers\Documents\HPTOOLS\HPCONFIGUTIL\ServerHealthTrimmed.xml", ServerInfoJson);


            GET_EMBEDDED_HEALTH_DATA_GEN8.GET_EMBEDDED_HEALTH_DATA me = XMLTools.ReadFromXmlString<GET_EMBEDDED_HEALTH_DATA_GEN8.GET_EMBEDDED_HEALTH_DATA>(@"C:\Users\N.Myers\Documents\HPTOOLS\HPCONFIGUTIL\ServerHealthTrimmed.xml");

            //MessageBox.Show(me.HEALTH_AT_A_GLANCE.PROCESSOR.STATUS);
            //MessageBox.Show(me.HEALTH_AT_A_GLANCE.MEMORY.STATUS);
            //MessageBox.Show(me.HEALTH_AT_A_GLANCE.BIOS_HARDWARE.STATUS);
            //MessageBox.Show(me.HEALTH_AT_A_GLANCE.STORAGE.STATUS);
            //MessageBox.Show(me.HEALTH_AT_A_GLANCE.TEMPERATURE.STATUS);
            //MessageBox.Show(me.HEALTH_AT_A_GLANCE.NETWORK.STATUS);


            foreach (var itm in me.HEALTH_AT_A_GLANCE.POWER_SUPPLIES)
            {
                MessageBox.Show(itm.STATUS +
                                itm.REDUNDANCY
                                );
            }

            foreach (var itm in me.FANS)
            {
                MessageBox.Show(itm.LABEL.VALUE + "\r" +
                                itm.SPEED.VALUE + "\r" +
                                itm.STATUS.VALUE + "\r" +
                                itm.ZONE.VALUE
                                );
            }

            foreach (var itm in me.TEMPERATURE)
            {
                MessageBox.Show(itm.LABEL.VALUE + "\r" +
                                itm.LOCATION.VALUE + "\r" +
                                itm.STATUS.VALUE + "\r" +
                                itm.CRITICAL.VALUE + "\r" +
                                itm.CAUTION.VALUE + "\r" +
                                itm.CURRENTREADING.VALUE + itm.CURRENTREADING.UNIT + "\r" +
                                itm.CAUTION.VALUE
                                );
            }
        }

        private void WeeklyServersLV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
         

            int ItemIndex;
            ItemIndex = WeeklyServersLV.SelectedIndex;
            //MessageBox.Show(ItemIndex.ToString());
            if(ItemIndex != -1)
            {
                WeeklyServerRecordsModel RecordRow = WeeklyServersLV.Items.GetItemAt(ItemIndex) as WeeklyServerRecordsModel;
                vm.UpdateWeeklyRecord = RecordRow;
            }
          
          

          //  MessageBox.Show(vm.WeeklyServerRecord.SONumber);

        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //var item = sender as ListViewItem;
            //if (item != null && item.IsSelected)
            //{
            //    MessageBox.Show(item.ToString());
            //    //Do your stuff
            //}
        }


        public void CalculateTotals()
        {
            //Zero each of the properties job.POTestingQuantity
            vm.BuildTotals = 0;
            vm.MotherboardTotals = 0;
            vm.CTOTotals = 0;
            vm.POTestingTotals = 0;
            vm.PartTestingTotals = 0;
            vm.RMATotals = 0;
            vm.ClientGearTotals = 0;

            foreach (WeeklyServerRecordsModel itm in WeeklyServersLV.Items)
            {

                //switch (itm.JobType)
                //{
                //    case "Build":
                //        vm.BuildTotals += itm.BuildQuantity;
                //        // MessageBox.Show(vm.WeekComm.BuildTotals.ToString());
                //        break;
                //    case "Build to update FW":
                //        vm.BuildTotals += itm.BuildFWQuantity;
                //        // MessageBox.Show(vm.WeekComm.BuildTotals.ToString());
                //        break;
                //    case "Build to Test":
                //        vm.BuildTotals += itm.BuildTestQuantity;
                //        // MessageBox.Show(vm.WeekComm.BuildTotals.ToString());
                //        break;
                //    case "RMA Replace Build":
                //        vm.BuildTotals += itm.RMAReplaceBuildQuantity;
                //        // MessageBox.Show(vm.WeekComm.BuildTotals.ToString());
                //        break;
                //    case "CTO":
                //        vm.CTOTotals += itm.CTOQuantity;
                //        //MessageBox.Show(itm.Quantity.ToString());
                //        break;
                //    case "Motherboard":
                //        vm.MotherboardTotals += itm.MotherboardQuantity;
                //        //MessageBox.Show(itm.Quantity.ToString());
                //        break;
                //}


                //new method to use for separation totals
                if (itm.BuildQuantity > 0 || itm.BuildFWQuantity > 0 || itm.BuildTestQuantity > 0)
                {
                    vm.BuildTotals += itm.BuildQuantity + itm.BuildFWQuantity + itm.BuildTestQuantity;
                }
               
                if (itm.CTOQuantity > 0)
                {
                    vm.CTOTotals += itm.CTOQuantity;
                }
               
                if (itm.MotherboardQuantity > 0)
                {
                    //MessageBox.Show(itm.MotherboardQuantity.ToString());
                    vm.MotherboardTotals += itm.MotherboardQuantity; 
                }

                if (itm.POTestingQuantity > 0)
                {
                    //MessageBox.Show(itm.MotherboardQuantity.ToString());
                    vm.POTestingTotals += itm.POTestingQuantity;
                }

                if (itm.RMAReplaceBuildQuantity > 0)
                {
                    
                    vm.RMATotals += itm.RMAReplaceBuildQuantity;
                }

                if (itm.PartTestingQuantity > 0)
                {

                    vm.PartTestingTotals += itm.PartTestingQuantity;
                }

                if (itm.ClientGearQuantity > 0)
                {

                    vm.ClientGearTotals += itm.ClientGearQuantity;
                }



                //Totals not included in report

                //if (itm.CPUTestQuantity > 0)
                //{

                //}
                //if (itm.CPUZQuantity > 0)
                //{

                //}

                //if (itm.DIMMTestQuantity > 0)
                //{

                //}

                //if (itm.IOBoardQuantity > 0)
                //{

                //}

                //if (itm.PlugAndPlayQuantity > 0)
                //{

                //}

                //if (itm.POTestingQuantity > 0)
                //{

                //}

                //if (itm.RAMReplaceQuantity > 0)
                //{

                //}








            }

        }



       

        private async void AddJobBTN_Click(object sender, RoutedEventArgs e)
        {

            //Check form has been filled out
            if (string.IsNullOrWhiteSpace(vm.AddWeeklyRecord.SONumber))
            {
                MessageBox.Show("The Sales Order field is required!", "Create Record", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (string.IsNullOrWhiteSpace(vm.AddWeeklyRecord.ServerModel))
            {
                MessageBox.Show("The Server Model field is required!", "Create Record", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            //else if (vm.AddWeeklyRecord.Quantity == 0)
            //{
            //    MessageBox.Show("Please enter a Quantity!", "Create Record", MessageBoxButton.OK, MessageBoxImage.Warning);
            //}
            else if (string.IsNullOrWhiteSpace(vm.AddWeeklyRecord.SalesRep))
            {
                MessageBox.Show("Sales Rep field is required!", "Create Record", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            //else if (string.IsNullOrWhiteSpace(vm.AddWeeklyRecord.JobType))
            //{
            //    MessageBox.Show("The Job Type field is required!", "Create Record", MessageBoxButton.OK, MessageBoxImage.Warning);
            //}
           else if (string.IsNullOrWhiteSpace(vm.AddWeeklyRecord.Status))
            {
                MessageBox.Show("The Status field is required!", "Create Record", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {

                //if all criteria met create the job
                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);

              


                    //Task of bool returned If returning a task of a Task<type> then always use await instead of .Result as will deadlock
                    //Check if Order Ref already exists 
                     bool result = await CheckSONumber();


                     //Check if PO Exists in DB
                    if (result == false)
                    {
                        //Display message to end user        
                        vm.NotificationMessageColour = Brushes.Red;
                        vm.NotificationMessage = @"SO\PO Order Reference Already exists in the DataBase! User Declined adding";

                    }
                    else
                    {

                    //CreateTable(sqlite_conn);
                    InsertData(sqlite_conn);


                    vm.WeeklyServerRecordCollection.Clear();

                    //GET DATE VALUES SELECTED ON THE DATEPICKERS
                    var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                    var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

                    //Read partial data
                    await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE date(Day) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') ORDER BY id DESC"));
               
                    //Close db
                    sqlite_conn.Close();

                    //reset object to default values
                    vm.AddWeeklyRecord.SONumber = string.Empty;
                    vm.AddWeeklyRecord.ServerModel = string.Empty;
                    vm.AddWeeklyRecord.SalesRep = string.Empty;
                    vm.AddWeeklyRecord.Technician = string.Empty;
                    vm.AddWeeklyRecord.Status = string.Empty;
                    vm.AddWeeklyRecord.Quantity = 0;
                    vm.AddWeeklyRecord.BuildQuantity = 0;
                    vm.AddWeeklyRecord.BuildFWQuantity = 0;
                    vm.AddWeeklyRecord.BuildTestQuantity = 0;
                    vm.AddWeeklyRecord.CPUTestQuantity = 0;
                    vm.AddWeeklyRecord.CPUZQuantity = 0;
                    vm.AddWeeklyRecord.CTOQuantity = 0;
                    vm.AddWeeklyRecord.DIMMTestQuantity = 0;
                    vm.AddWeeklyRecord.IOBoardQuantity = 0;
                    vm.AddWeeklyRecord.MotherboardQuantity = 0;
                    vm.AddWeeklyRecord.RAMReplaceQuantity = 0;
                    vm.AddWeeklyRecord.RMAReplaceBuildQuantity = 0;
                    vm.AddWeeklyRecord.PlugAndPlayQuantity = 0;
                    vm.AddWeeklyRecord.POTestingQuantity = 0;
                    vm.AddWeeklyRecord.PartTestingQuantity = 0;
                    vm.AddWeeklyRecord.ClientGearQuantity = 0;

                    vm.AddWeeklyRecord.Issues = string.Empty;
                    vm.AddWeeklyRecord.ActionTaken = string.Empty;
                    vm.AddWeeklyRecord.JobType = string.Empty;
                    //Highlight added item
                    WeeklyServersLV.SelectedIndex = 0;

                }
            }
        }


        private async Task<bool> CheckSONumber()
        {

            try
            {


                //Point DB to Test Databases
                if (StaticFunctions.TestMode == true)
                {
                    //Set connection string to Offline Test DB
                    cs = @"URI=file:" + @"C:\Users\n.myers\Desktop\Data\WeeklyRecords.db";
                }
                else
                {
                    if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/WeeklyRecords/WeeklyRecords.db"))
                    {
                        //TEMP Workaround File Server issue
                        Console.WriteLine("Workaround DB");
                        // Set connection string to Live DB
                        cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/WeeklyRecords/WeeklyRecords.db";
                    }
                    else
                    {

                        //Set connection string to Live DB
                        cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\WeeklyRecords.db";
                    }

                   
                }

                //Check if PO Number Already Exists in DB
                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);
                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;

                Console.WriteLine(vm.AddWeeklyRecord.SONumber);

                //string sqlquery = "SELECT * FROM Jobs WHERE SONumber LIKE '" + vm.AddWeeklyRecord.SONumber + "'";

                string sqlquery = "SELECT * FROM Jobs WHERE SONumber = '" + vm.AddWeeklyRecord.SONumber + "'";

                sqlite_cmd = sqlite_conn.CreateCommand();


                try
                {
                    //MAIN SEARCH QUERY

                    //CHECK IF INDEX THERE
                    //Hold return values
                    string indexThere = "";
                    sqlite_cmd.CommandText = sqlquery;
                    sqlite_datareader = sqlite_cmd.ExecuteReader();


                    Console.WriteLine(sqlite_datareader.Read().ToString());
                    //Use read to pull in details
                    while (sqlite_datareader.Read())
                    {
                        indexThere = sqlite_datareader.GetString(2);

                        Console.WriteLine(indexThere);
                    }

                    //Close Data Reader
                    sqlite_datareader.Close();

                    //Close Db
                    sqlite_conn.Close();

                  
                    //If indexThere then Lauch Custom Dialog
                    if (indexThere == vm.AddWeeklyRecord.SONumber)
                    {

                        //Present Dialog Warning to user
                        MessageDialogResult answer = await vm.SystemMessageDialogYesNoCancel("Nebula Notification", @"SO\PO Already exists!!! Do you want to continue?", "YesNo");

                        if (answer.ToString() == "Affirmative")
                        {
                            return true;

                        }
                        else if (answer.ToString() == "Negative")
                        {
                            return false;
                        }
                        else if (answer.ToString() == "Canceled")
                        {
                            return false;
                        }
                        else
                        {
                            return false;
                        }



                    }
                    else
                    {

                      return true;
                    }

                  
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);

                    return false;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //vm.ImportMessage = ex.Message;
                //Close Db
                //sqlite_conn.Close();
                return false;
            }
        }

        private async void UpdateJobBTN_Click(object sender, RoutedEventArgs e)
        {

            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            //CreateTable(sqlite_conn);
            //InsertData(sqlite_conn);


            //vm.WeeklyServerRecordCollection.Clear();
            //ReadData(sqlite_conn);
            //MessageBox.Show("Now Update");

            UpdateData(sqlite_conn);
           // MessageBox.Show("Update Complete");
            vm.WeeklyServerRecordCollection.Clear();
            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //MessageBox.Show(dateFrom + " To " + dateTo);

            // string SQLcmd = $"SELECT * FROM sorteos WHERE DATE(fecha) BETWEEN ('{FechaInicio}') AND ('{FechaFinal}')";

            //ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE date(Day) BETWEEN date('2020-10-05') AND date('2020-10-05')");

            if (vm.CurrentFilter == "On Hold Jobs")
            {
                await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE Status = 'On Hold' ORDER BY id DESC"));
            }
            else
            {
                await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE date(Day) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') ORDER BY id DESC"));
            }

            sqlite_conn.Close();

            //reset object to default values
            vm.UpdateWeeklyRecord.id = 0;
            vm.UpdateWeeklyRecord.SONumber = string.Empty;
            vm.UpdateWeeklyRecord.ServerModel = string.Empty;
            vm.UpdateWeeklyRecord.SalesRep = string.Empty;
            vm.UpdateWeeklyRecord.Technician = string.Empty;
            vm.UpdateWeeklyRecord.Status = string.Empty;
            vm.UpdateWeeklyRecord.Quantity = 0;
            vm.UpdateWeeklyRecord.BuildQuantity = 0;
            vm.UpdateWeeklyRecord.BuildFWQuantity = 0;
            vm.UpdateWeeklyRecord.BuildTestQuantity = 0;
            vm.UpdateWeeklyRecord.CPUTestQuantity = 0;
            vm.UpdateWeeklyRecord.CPUZQuantity = 0;
            vm.UpdateWeeklyRecord.CTOQuantity = 0;
            vm.UpdateWeeklyRecord.DIMMTestQuantity = 0;
            vm.UpdateWeeklyRecord.IOBoardQuantity = 0;
            vm.UpdateWeeklyRecord.MotherboardQuantity = 0;
            vm.UpdateWeeklyRecord.RAMReplaceQuantity = 0;
            vm.UpdateWeeklyRecord.RMAReplaceBuildQuantity = 0;
            vm.UpdateWeeklyRecord.PlugAndPlayQuantity = 0;
            vm.UpdateWeeklyRecord.POTestingQuantity = 0;
            vm.UpdateWeeklyRecord.PartTestingQuantity = 0;
            vm.UpdateWeeklyRecord.ClientGearQuantity = 0;
            vm.UpdateWeeklyRecord.Issues = string.Empty;
            vm.UpdateWeeklyRecord.ActionTaken = string.Empty;
            vm.UpdateWeeklyRecord.JobType = string.Empty;

          
        }

        private async void DeleteJobBTN_Click(object sender, RoutedEventArgs e)
        {
           MessageBoxResult mbr = MessageBox.Show("Are you sure you want to delete this item?", "Delete Entry", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (mbr == MessageBoxResult.Yes)
            {
                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);
                //CreateTable(sqlite_conn);
                DeleteData(sqlite_conn);


                vm.WeeklyServerRecordCollection.Clear();
                //GET DATE VALUES SELECTED ON THE DATEPICKERS
                var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

              
                if (vm.CurrentFilter == "On Hold Jobs")
                {
                    await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE Status = 'On Hold' ORDER BY id DESC"));
                }
                else
                {
                    await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE date(Day) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') ORDER BY id DESC"));
                }
               

                sqlite_conn.Close();

                //reset object to default values
                vm.UpdateWeeklyRecord.id = 0;
                vm.UpdateWeeklyRecord.SONumber = string.Empty;
                vm.UpdateWeeklyRecord.ServerModel = string.Empty;
                vm.UpdateWeeklyRecord.SalesRep = string.Empty;
                vm.UpdateWeeklyRecord.Technician = string.Empty;
                vm.UpdateWeeklyRecord.Status = string.Empty;
                vm.UpdateWeeklyRecord.Quantity = 0;
                vm.UpdateWeeklyRecord.BuildQuantity = 0;
                vm.UpdateWeeklyRecord.BuildFWQuantity = 0;
                vm.UpdateWeeklyRecord.BuildTestQuantity = 0;
                vm.UpdateWeeklyRecord.CPUTestQuantity = 0;
                vm.UpdateWeeklyRecord.CPUZQuantity = 0;
                vm.UpdateWeeklyRecord.CTOQuantity = 0;
                vm.UpdateWeeklyRecord.DIMMTestQuantity = 0;
                vm.UpdateWeeklyRecord.IOBoardQuantity = 0;
                vm.UpdateWeeklyRecord.MotherboardQuantity = 0;
                vm.UpdateWeeklyRecord.RAMReplaceQuantity = 0;
                vm.UpdateWeeklyRecord.RMAReplaceBuildQuantity = 0;
                vm.UpdateWeeklyRecord.PlugAndPlayQuantity = 0;
                vm.UpdateWeeklyRecord.POTestingQuantity = 0;
                vm.UpdateWeeklyRecord.PartTestingQuantity = 0;
                vm.UpdateWeeklyRecord.ClientGearQuantity = 0;
                vm.UpdateWeeklyRecord.Issues = string.Empty;
                vm.UpdateWeeklyRecord.ActionTaken = string.Empty;
                vm.UpdateWeeklyRecord.JobType = string.Empty;



            }
            else if (mbr == MessageBoxResult.No)
            {
                //do something else
            }
            

           






        }


        private async void QueryJobBTN_Click(object sender, RoutedEventArgs e)
        {
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            //vm.WeeklyServerRecordCollection.Clear();
            vm.WeeklyServerReportCollection.Clear();

           //GET DATE VALUES SELECTED ON THE DATEPICKERS
           var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
           var dateTo = vm.DateTo.ToString("yyyy-MM-dd");
           Mouse.OverrideCursor = Cursors.Wait;
           //ProgressBar.IsIndeterminate = true;

          
            //Check if PO Only has been selected
        if (POOnlyCB.IsChecked == true)
        {
                //if (vm.CurrentFilter == "Purchase Orders Only")
                //{
                await System.Threading.Tasks.Task.Run(() => ReadDataForReport(sqlite_conn, "SELECT * FROM Jobs WHERE POTestingQuantity > 0 AND date(Day) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') ORDER BY id DESC"));
                //}
              
        }
        else
        {
                if (vm.CurrentFilter == "On Hold Jobs")
                {
                    await System.Threading.Tasks.Task.Run(() => ReadDataForReport(sqlite_conn, "SELECT * FROM Jobs WHERE Status = 'On Hold' ORDER BY id DESC"));
                }
                else
                {
                    await System.Threading.Tasks.Task.Run(() => ReadDataForReport(sqlite_conn, "SELECT * FROM Jobs WHERE date(Day) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') ORDER BY id DESC"));
                }
        }


            //Create Flow Document

            FlowDocument fd = new FlowDocument();

            FlowDocument ReportFD = new FlowDocument();
            Section MainSection = new Section();
            MainSection.Padding = new Thickness(1);
            MainSection.Margin = new Thickness(0);
            Section mySectionReportHeader = new Section();
            mySectionReportHeader.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#0f89bd");  //(Brush)bc.ConvertFrom("#CC119EDA");


            BitmapImage bmp0 = new BitmapImage();

            bmp0.BeginInit();
            bmp0.UriSource = new Uri("pack://application:,,,/Images/TechBuyerLogoCenteredReports.png", UriKind.Absolute);
            bmp0.EndInit();

            //Create Block ui container and add image as a child
            BlockUIContainer blockUIImage = new BlockUIContainer();
            blockUIImage.Padding = new Thickness(10);
            blockUIImage.Margin = new Thickness(10);
            Image img0 = new Image();
            //Image.HorizontalAlignmentProperty. = HorizontalAlignment.Center;
            img0.Source = bmp0;
            img0.Width = 150;
            img0.Height = 75;
            blockUIImage.Child = img0;


            Paragraph ReportHeader = new Paragraph(new Run("Weekly Server Report"));
            ReportHeader.Padding = new Thickness(0, 1, 0, 1);
            ReportHeader.FontSize = 19;
            ReportHeader.FontFamily = new FontFamily("Segoe UI");
            ReportHeader.Foreground = Brushes.White;
            ReportHeader.TextAlignment = TextAlignment.Center;
            ReportHeader.Margin = new Thickness(0, 1, 0, 1);
            //Report date or title header
            Paragraph ReportDateHeader = new Paragraph();


            ////Report date or title header
            //Paragraph ReportDateHeader = new Paragraph();
            if (vm.CurrentFilter == "On Hold Jobs")
            {
                ReportDateHeader = new Paragraph(new Run("Jobs On Hold"));
                ReportDateHeader.Padding = new Thickness(1);
                ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
                ReportDateHeader.FontSize = 16;
                ReportDateHeader.Foreground = Brushes.YellowGreen;
                ReportDateHeader.TextAlignment = TextAlignment.Center;
                ReportDateHeader.FontWeight = FontWeights.Bold;
            }
            else
            {
                ReportDateHeader = new Paragraph(new Run("" + vm.DateFrom.ToShortDateString() + " - " + vm.DateTo.ToShortDateString() + ""));
                ReportDateHeader.Padding = new Thickness(1);
                ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
                ReportDateHeader.FontSize = 16;
                ReportDateHeader.Foreground = Brushes.YellowGreen;
                ReportDateHeader.TextAlignment = TextAlignment.Center;
                ReportDateHeader.FontWeight = FontWeights.Bold;
            }


            //Add image to section
            mySectionReportHeader.Blocks.Add(blockUIImage);

            //Add Paragraph to Header
            mySectionReportHeader.Blocks.Add(ReportHeader);
            mySectionReportHeader.Blocks.Add(ReportDateHeader);

            //Totals 
            //Section mySectionTotals = new Section();
            //mySectionTotals.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#0f89bd"); //(Brush)bc.ConvertFrom("#CC119EDA");
            //Paragraph ReportTotalsHeader = new Paragraph(new Run("Report Totals"));
            //ReportTotalsHeader.Padding = new Thickness(1);
            //ReportTotalsHeader.FontFamily = new FontFamily("Segoe UI");
            //ReportTotalsHeader.FontSize = 15;
            //ReportTotalsHeader.Foreground = Brushes.White;
            //ReportTotalsHeader.TextAlignment = TextAlignment.Center;

            CalculateTotals();

            //MessageBox.Show("Builds=" + vm.BuildTotals + "Bulds=" + vm.CTOTotals);

            Paragraph ReportTotals = new Paragraph(new Run("Builds " + vm.BuildTotals + " | RMA Builds " + vm.RMATotals + " | CTO " + vm.CTOTotals + " | Motherboards " + vm.MotherboardTotals + "" + " | Purchase Order Testing " + vm.POTestingTotals + "" + " | Part Testing " + vm.PartTestingTotals  + "" + " | Client Gear " + vm.ClientGearTotals + ""));
            ReportTotals.Padding = new Thickness(1);
            ReportTotals.FontFamily = new FontFamily("Segoe UI");
            ReportTotals.FontSize = 15;
            ReportTotals.Foreground = Brushes.White;
            ReportTotals.TextAlignment = TextAlignment.Center;

            //Add Paragraph to Header
            //mySectionTotals.Blocks.Add(ReportTotalsHeader);
            //mySectionTotals.Blocks.Add(ReportTotals);

            //Add Paragraph to Header
            //mySectionReportHeader.Blocks.Add(ReportTotalsHeader);
            mySectionReportHeader.Blocks.Add(ReportTotals);



            //Records
            Section mySectionRecordsHeader = new Section();
            Section mySectionRecords = new Section();

            Table RecordTable = new Table();
            RecordTable.CellSpacing = 2;
            RecordTable.TextAlignment = TextAlignment.Center;
            //RecordTable.BorderBrush = Brushes.Black;
            //RecordTable.BorderThickness = new Thickness(.5);

            // Create 6 columns and add them to the table's Columns collection.
            //int numberOfColumns = 22;
            int numberOfColumns = 9;
            for (int x = 0; x < numberOfColumns; x++)
            {
                RecordTable.Columns.Add(new TableColumn());


                //ALTER THE COLUMN WIDTHS
                if (x == 0 || x == 1 || x == 2)
                {
                    RecordTable.Columns[x].Width = new GridLength(6, GridUnitType.Star);
                }
                else if (x == 3)
                {
                    RecordTable.Columns[x].Width = new GridLength(8, GridUnitType.Star);
                }
                else if (x == 6 || x == 7)
                {
                    RecordTable.Columns[x].Width = new GridLength(15, GridUnitType.Star);
                }
                else
                {
                    RecordTable.Columns[x].Width = new GridLength(3, GridUnitType.Star);
                }


            }

            // Create and add an empty TableRowGroup to hold the table's Rows.
            RecordTable.RowGroups.Add(new TableRowGroup());

            // Add the first (title) row.
            RecordTable.RowGroups[0].Rows.Add(new TableRow());

            // Alias the current working row for easy reference.
            TableRow currentRow = RecordTable.RowGroups[0].Rows[0];

            //// Add the second (header) row.
            //RecordTable.RowGroups[0].Rows.Add(new TableRow());
            currentRow = RecordTable.RowGroups[0].Rows[0];
            // Global formatting for the title row.
            currentRow.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#0f89bd");
            currentRow.Foreground = Brushes.White;
            currentRow.FontSize = 20;

            currentRow.FontWeight = System.Windows.FontWeights.Bold;

            // Global formatting for the header row.
            currentRow.FontSize = 9;
            currentRow.FontWeight = FontWeights.Normal;
            currentRow.FontFamily = new FontFamily("Segoe UI");
            // Add cells with content to the second row.
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Day"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("SO Number"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Server Model"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Job Type"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Sales Rep"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Technician"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Issues"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Action Taken"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Status"))));



            // Add Headers to table
            mySectionRecords.Blocks.Add(RecordTable);

            //Start of Row To start adding collection
            int RowIndex = 1;

            //Add Loop Through Records

            foreach (WeeklyServerRecordsModel itm in vm.WeeklyServerReportCollection)
            {

                RecordTable.RowGroups[0].Rows.Add(new TableRow());
                currentRow = RecordTable.RowGroups[0].Rows[RowIndex];
                currentRow.FontWeight = FontWeights.Normal;
                currentRow.FontSize = 8;
                currentRow.Foreground = Brushes.Black;
                currentRow.FontFamily = new FontFamily("Segoe UI");


                // Add cells with content to the third row.
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.Day.ToShortDateString().Replace("//", "-")))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.SONumber))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.ServerModel))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.JobType))));

                string jobTypes = "";

                if (itm.BuildQuantity > 0 || itm.BuildFWQuantity > 0 || itm.BuildTestQuantity > 0)
                {
                    int buildCount = itm.BuildQuantity + itm.BuildFWQuantity + itm.BuildTestQuantity;

                    jobTypes += "Builds Quantity = " + buildCount.ToString() + "\r";
                }
                if (itm.RMAReplaceBuildQuantity > 0)
                {
                    int RMAbuildCount = itm.RMAReplaceBuildQuantity;

                    jobTypes += "RMA Builds Quantity = " + RMAbuildCount.ToString() + "\r";
                }
                //if (itm.CPUTestQuantity > 0)
                //{
                //    jobTypes += "CPU Test Quantity = " + itm.CPUTestQuantity.ToString() + "\r";
                //}
                //if (itm.CPUZQuantity > 0)
                //{
                //    jobTypes += "CPUZ Quantity = " + itm.CPUZQuantity.ToString() + "\r";
                //}
                if (itm.CTOQuantity > 0)
                {
                    jobTypes += "CTO Quantity = " + itm.CTOQuantity.ToString() + "\r";
                }
                //if (itm.CPUTestQuantity > 0)
                //{
                //    jobTypes += "CPU Test Quantity = " + itm.CPUTestQuantity.ToString() + "\r";
                //}
                //if (itm.DIMMTestQuantity > 0)
                //{
                //    jobTypes += "DIMM Test Quantity = " + itm.DIMMTestQuantity.ToString() + "\r";
                //}
                //if (itm.IOBoardQuantity > 0)
                //{
                //    jobTypes += "IO Board Quantity = " + itm.IOBoardQuantity.ToString() + "\r";
                //}
                if (itm.MotherboardQuantity > 0)
                {
                    jobTypes += "Motherboard Quantity = " + itm.MotherboardQuantity.ToString() + "\r";
                }
                //if (itm.PlugAndPlayQuantity > 0)
                //{
                //    jobTypes += "Plug & Play Quantity = " + itm.PlugAndPlayQuantity.ToString() + "\r";
                //}
                if (itm.POTestingQuantity > 0)
                {
                    jobTypes += "PO Testing Quantity = " + itm.POTestingQuantity.ToString() + "\r";
                }
                if (itm.PartTestingQuantity > 0)
                {
                    jobTypes += "Part Testing Quantity = " + itm.PartTestingQuantity.ToString() + "\r";
                }
                if (itm.ClientGearQuantity > 0)
                {
                    jobTypes += "Client Gear Quantity = " + itm.ClientGearQuantity.ToString() + "\r";
                }

                // MessageBox.Show(jobTypes);


                //JOB TYPES
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(jobTypes))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.SalesRep))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.Technician))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.Issues))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.ActionTaken))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.Status))));


                //Increment By 1  RecordTable.RowGroups[0]
                RowIndex++;
            }


            //To be used to create row colouring, needs to target the row groups
            for (int n = 1; n < RecordTable.RowGroups[0].Rows.Count(); n++)
            {
                //for border
                //currentRow.Cells[n].BorderThickness = new Thickness(0,1,0,1);
                //currentRow.Cells[n].BorderBrush = Brushes.Black;
                if (n % 2 == 0)
                    RecordTable.RowGroups[0].Rows[n].Background = Brushes.AliceBlue;

                else
                    RecordTable.RowGroups[0].Rows[n].Background = Brushes.White;
            }

            //Add Table To Section
            mySectionRecords.Blocks.Add(RecordTable);



            //1 Add Header To Main Section
            MainSection.Blocks.Add(mySectionReportHeader);
            //2 Add Totals Section To Main Section
            //MainSection.Blocks.Add(mySectionTotals);
            //3 Add Records to Main Section
            MainSection.Blocks.Add(mySectionRecords);


            //Add Main Section to flowdocument
            ReportFD.Blocks.Add(MainSection);

            if (POOnlyCB.IsChecked == true)
            {
                Clipboard.SetText("Purchase Order Jobs Report " + vm.DateFrom.ToShortDateString().Replace("/", "-") + " To " + vm.DateTo.ToShortDateString().Replace("/", "-"), TextDataFormat.UnicodeText);

            }
            else
            {
                if (vm.CurrentFilter == "On Hold Jobs")
                {
                    Clipboard.SetText("Current Jobs On Hold Report " + DateTime.Now.ToShortDateString().Replace("/", "-"), TextDataFormat.UnicodeText);
                }
                else
                {
                    Clipboard.SetText("Weekly Server Totals Report " + vm.DateFrom.ToShortDateString().Replace("/", "-") + " To " + vm.DateTo.ToShortDateString().Replace("/", "-"), TextDataFormat.UnicodeText);
                }
            }


            await PutTaskDelay(500);



            //Close Db
            sqlite_conn.Close();
           
            //Call print to pdf function
            vm.PrintSaveFlowDocumentCommand.Execute(ReportFD);
            //vm.PrintSaveFlowDocumentCommand.Execute(await System.Threading.Tasks.Task.Run(() => GenerateReport()));


            //ProgressBar.IsIndeterminate = false;
            Mouse.OverrideCursor = null;
        }


        public FlowDocument GenerateReport()
        {
            //Create Flow Document

            FlowDocument ReportFD = new FlowDocument();
            Section MainSection = new Section();
            MainSection.Padding = new Thickness(4);
            Section mySectionReportHeader = new Section();
            mySectionReportHeader.Background = Brushes.MediumPurple; //(Brush)bc.ConvertFrom("#CC119EDA");
            Paragraph ReportHeader = new Paragraph(new Run("Weekly Server Records Report"));
            ReportHeader.Padding = new Thickness(10);
            ReportHeader.FontSize = 30;
            ReportHeader.FontFamily = new FontFamily("Segoe UI");
            ReportHeader.Foreground = Brushes.White;
            ReportHeader.TextAlignment = TextAlignment.Center;

            //Report date or title header
            Paragraph ReportDateHeader = new Paragraph();
            if (vm.CurrentFilter == "On Hold Jobs")
            {
                ReportDateHeader = new Paragraph(new Run("Jobs On Hold"));
                ReportDateHeader.Padding = new Thickness(4);
                ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
                ReportDateHeader.FontSize = 20;
                ReportDateHeader.Foreground = Brushes.White;
                ReportDateHeader.TextAlignment = TextAlignment.Center;
            }
            else
            {
                ReportDateHeader = new Paragraph(new Run("" + vm.DateFrom.ToShortDateString() + " - " + vm.DateTo.ToShortDateString() + ""));
                ReportDateHeader.Padding = new Thickness(10);
                ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
                ReportDateHeader.FontSize = 20;
                ReportDateHeader.Foreground = Brushes.White;
                ReportDateHeader.TextAlignment = TextAlignment.Center;
            }



            //Add Paragraph to Header
            mySectionReportHeader.Blocks.Add(ReportHeader);
            mySectionReportHeader.Blocks.Add(ReportDateHeader);

            //Totals 
            Section mySectionTotals = new Section();
            mySectionTotals.Background = Brushes.MediumPurple; //(Brush)bc.ConvertFrom("#CC119EDA");
            Paragraph ReportTotalsHeader = new Paragraph(new Run("Report Totals"));
            ReportTotalsHeader.Padding = new Thickness(10);
            ReportTotalsHeader.FontFamily = new FontFamily("Segoe UI");
            ReportTotalsHeader.FontSize = 18;
            ReportTotalsHeader.Foreground = Brushes.White;
            ReportTotalsHeader.TextAlignment = TextAlignment.Center;

            CalculateTotals();

            //MessageBox.Show("Builds=" + vm.BuildTotals + "Bulds=" + vm.CTOTotals);

            Paragraph ReportTotals = new Paragraph(new Run("Builds " + vm.BuildTotals + " | RMA Builds " + vm.RMATotals + " | CTO " + vm.CTOTotals + " | Motherboards " + vm.MotherboardTotals + "" + " | Purchase Order Testing " + vm.POTestingTotals + "" + " | Part Testing " + vm.PartTestingTotals + "" + " | Client Gear " + vm.ClientGearTotals + ""));
            ReportTotals.Padding = new Thickness(4);
            ReportTotals.FontFamily = new FontFamily("Segoe UI");
            ReportTotals.FontSize = 10;
            ReportTotals.Foreground = Brushes.White;
            ReportTotals.TextAlignment = TextAlignment.Center;

            //Add Paragraph to Header
            mySectionTotals.Blocks.Add(ReportTotalsHeader);
            mySectionTotals.Blocks.Add(ReportTotals);

            //Records
            Section mySectionRecordsHeader = new Section();
            Section mySectionRecords = new Section();

            Table RecordTable = new Table();
            RecordTable.CellSpacing = 2;
            RecordTable.TextAlignment = TextAlignment.Center;
            //RecordTable.BorderBrush = Brushes.Black;
            //RecordTable.BorderThickness = new Thickness(.5);

            // Create 6 columns and add them to the table's Columns collection.
            //int numberOfColumns = 22;
            int numberOfColumns = 9;
            for (int x = 0; x < numberOfColumns; x++)
            {
                RecordTable.Columns.Add(new TableColumn());

                // Set alternating background colors for the middle colums.
                //if (x % 2 == 0)
                // RecordTable.Columns[x].Background = Brushes.AliceBlue;

                //else
                // RecordTable.Columns[x].Background = Brushes.White;

                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Day"))));   0
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("SO Number")))); 1
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Server Model")))); 2
                ////currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Job Type"))));
                ////currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Quantity"))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Build")))); 3
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("BuildFW")))); 4
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("BuildTest")))); 5
                ////currentRow.Cells.Add(new TableCell(new Paragraph(new Run("CPUTest"))));
                ////currentRow.Cells.Add(new TableCell(new Paragraph(new Run("CPUZ"))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("CTO")))); 6
                ////currentRow.Cells.Add(new TableCell(new Paragraph(new Run("DIMM"))));
                ////currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Quantity"))));
                ////currentRow.Cells.Add(new TableCell(new Paragraph(new Run("IO"))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Mboard")))); 7
                ////currentRow.Cells.Add(new TableCell(new Paragraph(new Run("RAMRep"))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("RMABuild")))); 8
                ////currentRow.Cells.Add(new TableCell(new Paragraph(new Run("P&P"))));
                ////currentRow.Cells.Add(new TableCell(new Paragraph(new Run("POTest"))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Sales Rep")))); 9
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Technician")))); 10
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Issues")))); 11
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Action Taken")))); 12
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Status")))); 13

                //ALTER THE COLUMN WIDTHS
                if (x == 0 || x == 1 || x == 2)
                {
                    RecordTable.Columns[x].Width = new GridLength(6, GridUnitType.Star);
                }
                else if (x == 3)
                {
                    RecordTable.Columns[x].Width = new GridLength(8, GridUnitType.Star);
                }
                else if (x == 6 || x == 7)
                {
                    RecordTable.Columns[x].Width = new GridLength(15, GridUnitType.Star);
                }
                else
                {
                    RecordTable.Columns[x].Width = new GridLength(3, GridUnitType.Star);
                }


            }

            // Create and add an empty TableRowGroup to hold the table's Rows.
            RecordTable.RowGroups.Add(new TableRowGroup());

            // Add the first (title) row.
            RecordTable.RowGroups[0].Rows.Add(new TableRow());

            // Alias the current working row for easy reference.
            TableRow currentRow = RecordTable.RowGroups[0].Rows[0];



            //// Add the header row with content,
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Records"))));
            //// and set the row to span all 6 columns.
            //currentRow.Cells[0].ColumnSpan = 10;

            //// Add the second (header) row.
            //RecordTable.RowGroups[0].Rows.Add(new TableRow());
            currentRow = RecordTable.RowGroups[0].Rows[0];
            // Global formatting for the title row.
            currentRow.Background = Brushes.MediumPurple;
            currentRow.Foreground = Brushes.White;
            currentRow.FontSize = 20;

            currentRow.FontWeight = System.Windows.FontWeights.Bold;

            // Global formatting for the header row.
            currentRow.FontSize = 9;
            currentRow.FontWeight = FontWeights.Normal;
            currentRow.FontFamily = new FontFamily("Segoe UI");
            // Add cells with content to the second row.
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Day"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("SO Number"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Server Model"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Job Type"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Quantity"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Build"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("BuildFW"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("BuildTest"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("CPUTest"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("CPUZ"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("CTO"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("DIMM"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Quantity"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("IO"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Mboard"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("RAMRep"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("RMABuild"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("P&P"))));
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("POTest"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Sales Rep"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Technician"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Issues"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Action Taken"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Status"))));






            // Add Headers to table
            mySectionRecords.Blocks.Add(RecordTable);

            //Start of Row To start adding collection
            int RowIndex = 1;

            //Add Loop Through Records

            foreach (WeeklyServerRecordsModel itm in WeeklyServersLV.Items)
            {

                RecordTable.RowGroups[0].Rows.Add(new TableRow());
                currentRow = RecordTable.RowGroups[0].Rows[RowIndex];
                currentRow.FontWeight = FontWeights.Normal;
                currentRow.FontSize = 8;
                currentRow.Foreground = Brushes.Black;
                currentRow.FontFamily = new FontFamily("Segoe UI");

                //Add Table
                //Paragraph ReportRecord = new Paragraph(new Run("" + itm.Day + "|" + itm.SONumber + "|" + itm.ServerModel + "|" + itm.JobType + "|" + itm.Quantity 
                //+ "|" + itm.SalesRep + "|" + itm.Technician + "|" + itm.Issues + "|" + itm.ActionTaken + "|" + itm.Status));
                //ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
                //ReportDateHeader.FontSize = 10;
                //ReportDateHeader.Foreground = Brushes.MediumPurple;
                //ReportDateHeader.TextAlignment = TextAlignment.Left;

                // Add cells with content to the third row.
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.Day.ToShortDateString().Replace("//", "-")))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.SONumber))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.ServerModel))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.JobType))));

                string jobTypes = "";

                if (itm.BuildQuantity > 0 || itm.BuildFWQuantity > 0 || itm.BuildTestQuantity > 0)
                {
                    int buildCount = itm.BuildQuantity + itm.BuildFWQuantity + itm.BuildTestQuantity;

                    jobTypes += "Builds Quantity = " + buildCount.ToString() + "\r";
                }
                if (itm.RMAReplaceBuildQuantity > 0)
                {
                    int RMAbuildCount = itm.RMAReplaceBuildQuantity;

                    jobTypes += "RMA Builds Quantity = " + RMAbuildCount.ToString() + "\r";
                }
                //if (itm.CPUTestQuantity > 0)
                //{
                //    jobTypes += "CPU Test Quantity = " + itm.CPUTestQuantity.ToString() + "\r";
                //}
                //if (itm.CPUZQuantity > 0)
                //{
                //    jobTypes += "CPUZ Quantity = " + itm.CPUZQuantity.ToString() + "\r";
                //}
                if (itm.CTOQuantity > 0)
                {
                    jobTypes += "CTO Quantity = " + itm.CTOQuantity.ToString() + "\r";
                }
                //if (itm.CPUTestQuantity > 0)
                //{
                //    jobTypes += "CPU Test Quantity = " + itm.CPUTestQuantity.ToString() + "\r";
                //}
                //if (itm.DIMMTestQuantity > 0)
                //{
                //    jobTypes += "DIMM Test Quantity = " + itm.DIMMTestQuantity.ToString() + "\r";
                //}
                //if (itm.IOBoardQuantity > 0)
                //{
                //    jobTypes += "IO Board Quantity = " + itm.IOBoardQuantity.ToString() + "\r";
                //}
                if (itm.MotherboardQuantity > 0)
                {
                    jobTypes += "Motherboard Quantity = " + itm.MotherboardQuantity.ToString() + "\r";
                }
                //if (itm.PlugAndPlayQuantity > 0)
                //{
                //    jobTypes += "Plug & Play Quantity = " + itm.PlugAndPlayQuantity.ToString() + "\r";
                //}
                if (itm.POTestingQuantity > 0)
                {
                    jobTypes += "PO Testing Quantity = " + itm.POTestingQuantity.ToString() + "\r";
                }
                if (itm.PartTestingQuantity > 0)
                {
                    jobTypes += "Part Testing Quantity = " + itm.PartTestingQuantity.ToString() + "\r";
                }
                if (itm.ClientGearQuantity > 0)
                {
                    jobTypes += "Client Gear Quantity = " + itm.ClientGearQuantity.ToString() + "\r";
                }

                // MessageBox.Show(jobTypes);


                //JOB TYPES
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(jobTypes))));

                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.BuildQuantity.ToString()))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.BuildFWQuantity.ToString()))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.BuildTestQuantity.ToString()))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.RMAReplaceBuildQuantity.ToString()))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.CPUTestQuantity.ToString()))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.CPUZQuantity.ToString()))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.CTOQuantity.ToString()))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.DIMMTestQuantity.ToString()))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.IOBoardQuantity.ToString()))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.MotherboardQuantity.ToString()))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.RAMReplaceQuantity.ToString()))));

                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.PlugAndPlayQuantity.ToString()))));
                //currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.POTestingQuantity.ToString()))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.SalesRep))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.Technician))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.Issues))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.ActionTaken))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.Status))));




                //for (int n = 0; n < currentRow.Cells.Count(); n++)
                //{
                //    //for border
                //    //currentRow.Cells[n].BorderThickness = new Thickness(0,1,0,1);
                //    //currentRow.Cells[n].BorderBrush = Brushes.Black;
                //    if (n % 2 == 0)
                //        currentRow.Cells[n].Background = Brushes.AliceBlue;

                //    else
                //        currentRow.Cells[n].Background = Brushes.White;
                //}


                //Increment By 1  RecordTable.RowGroups[0]
                RowIndex++;
            }


            //To be used to create row colouring, needs to target the row groups
            for (int n = 1; n < RecordTable.RowGroups[0].Rows.Count(); n++)
            {
                //for border
                //currentRow.Cells[n].BorderThickness = new Thickness(0,1,0,1);
                //currentRow.Cells[n].BorderBrush = Brushes.Black;
                if (n % 2 == 0)
                    RecordTable.RowGroups[0].Rows[n].Background = Brushes.AliceBlue;

                else
                    RecordTable.RowGroups[0].Rows[n].Background = Brushes.White;
            }

            //Add Table To Section
            mySectionRecords.Blocks.Add(RecordTable);



            //1 Add Header To Main Section
            MainSection.Blocks.Add(mySectionReportHeader);
            //2 Add Totals Section To Main Section
            MainSection.Blocks.Add(mySectionTotals);
            //3 Add Records to Main Section
            MainSection.Blocks.Add(mySectionRecords);


            //Add Main Section to flowdocument
            ReportFD.Blocks.Add(MainSection);


            //await PutTaskDelay(500);

          


            return ReportFD;
        }






        static SQLiteConnection CreateConnection(string pathtodb)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            //sqlite_conn = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            //sqlite_conn = new SQLiteConnection("Data Source=" + pathtodb + "; Version = 3; New = True; Compress = True; ");
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sqlite_conn;
        }




       public void CreateTable(SQLiteConnection conn)
        {

            SQLiteCommand sqlite_cmd;
            string Createsql = "CREATE TABLE SampleTable(Col1 VARCHAR(20), Col2 INT)";
            string Createsql1 = "CREATE TABLE SampleTable1(Col1 VARCHAR(20), Col2 INT)";
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = Createsql;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_cmd.CommandText = Createsql1;
            sqlite_cmd.ExecuteNonQuery();

        }

        public void InsertData(SQLiteConnection conn)
        {
           
           SQLiteCommand sqlite_cmd;
           sqlite_cmd = conn.CreateCommand();
           //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
           sqlite_cmd.CommandText = "INSERT INTO Jobs (Day, SONumber, ServerModel, Quantity, Technician, SalesRep, JobType, Issues, ActionTaken, Status, BuildQuantity, BuildFWQuantity, BuildTestQuantity, CPUTestQuantity, CPUZQuantity, CTOQuantity, DIMMTestQuantity, IOBoardQuantity, MotherboardQuantity, RAMReplaceQuantity, RMAReplaceBuildQuantity, PlugAndPlayQuantity, POTestingQuantity, PartTestingQuantity, ClientGearQuantity) VALUES(@day, @sonumber,@servermodel,@quantity,@technician,@salesrep,@jobtype,@issues,@actiontaken,@status,@buildquantity,@buildfwquantity,@buildtestquantity,@cputestquantity,@cpuzquantity,@ctoquantity,@dimmtestquantity,@ioboardquantity,@motherboardquantity,@ramreplacequantity,@rmareplacebuildquantity,@plugandplayquantity,@potestingquantity,@parttestingquantity,@clientgearquantity)";
           sqlite_cmd.CommandType = CommandType.Text;
            //Date must be in this format
           sqlite_cmd.Parameters.Add(new SQLiteParameter("@day", DateTime.Now.ToString("yyyy-MM-dd")));
           sqlite_cmd.Parameters.Add(new SQLiteParameter("@sonumber", vm.AddWeeklyRecord.SONumber + ""));
           sqlite_cmd.Parameters.Add(new SQLiteParameter("@servermodel", vm.AddWeeklyRecord.ServerModel + ""));
           sqlite_cmd.Parameters.Add(new SQLiteParameter("@quantity", vm.AddWeeklyRecord.Quantity));
           sqlite_cmd.Parameters.Add(new SQLiteParameter("@technician", vm.CurrentUserInitials));
           sqlite_cmd.Parameters.Add(new SQLiteParameter("@salesrep", vm.AddWeeklyRecord.SalesRep + ""));
           sqlite_cmd.Parameters.Add(new SQLiteParameter("@jobtype", vm.AddWeeklyRecord.JobType + ""));
           sqlite_cmd.Parameters.Add(new SQLiteParameter("@issues", vm.AddWeeklyRecord.Issues + ""));
           sqlite_cmd.Parameters.Add(new SQLiteParameter("@actiontaken", vm.AddWeeklyRecord.ActionTaken + ""));
           sqlite_cmd.Parameters.Add(new SQLiteParameter("@status", vm.AddWeeklyRecord.Status + ""));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@buildquantity", vm.AddWeeklyRecord.BuildQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@buildfwquantity", vm.AddWeeklyRecord.BuildFWQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@buildtestquantity", vm.AddWeeklyRecord.BuildTestQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@cputestquantity", vm.AddWeeklyRecord.CPUTestQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@cpuzquantity", vm.AddWeeklyRecord.CPUZQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@ctoquantity", vm.AddWeeklyRecord.CTOQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@dimmtestquantity", vm.AddWeeklyRecord.DIMMTestQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@ioboardquantity", vm.AddWeeklyRecord.IOBoardQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@motherboardquantity", vm.AddWeeklyRecord.MotherboardQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@ramreplacequantity", vm.AddWeeklyRecord.RAMReplaceQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@rmareplacebuildquantity", vm.AddWeeklyRecord.RMAReplaceBuildQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@plugandplayquantity", vm.AddWeeklyRecord.PlugAndPlayQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@potestingquantity", vm.AddWeeklyRecord.POTestingQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@parttestingquantity", vm.AddWeeklyRecord.PartTestingQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@clientgearquantity", vm.AddWeeklyRecord.ClientGearQuantity));
            sqlite_cmd.ExecuteNonQuery();
        }

        public void DeleteData(SQLiteConnection conn)
        {
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.CommandText = "DELETE FROM Jobs WHERE id = @id;";
            sqlite_cmd.CommandType = CommandType.Text;
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@id", vm.UpdateWeeklyRecord.id));
            //sqlite_cmd.Parameters.Add(new SQLiteParameter("@day", DateTime.Now));
            //sqlite_cmd.Parameters.Add(new SQLiteParameter("@sonumber", vm.WeeklyRecord.SONumber + ""));
            //sqlite_cmd.Parameters.Add(new SQLiteParameter("@servermodel", vm.WeeklyRecord.ServerModel + ""));
            //sqlite_cmd.Parameters.Add(new SQLiteParameter("@quantity", vm.WeeklyRecord.Quantity));
            //sqlite_cmd.Parameters.Add(new SQLiteParameter("@technician", vm.CurrentUserInitials));
            //sqlite_cmd.Parameters.Add(new SQLiteParameter("@salesrep", vm.WeeklyRecord.SalesRep + ""));
            //sqlite_cmd.Parameters.Add(new SQLiteParameter("@jobtype", vm.WeeklyRecord.JobType + ""));
            //sqlite_cmd.Parameters.Add(new SQLiteParameter("@issues", vm.WeeklyRecord.Issues + ""));
            //sqlite_cmd.Parameters.Add(new SQLiteParameter("@actiontaken", vm.WeeklyRecord.ActionTaken + ""));
            //sqlite_cmd.Parameters.Add(new SQLiteParameter("@status", vm.WeeklyRecord.Status + ""));
            sqlite_cmd.ExecuteNonQuery();
        }


        public void UpdateData(SQLiteConnection conn)
        {
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            //sqlite_cmd.CommandText = "UPDATE Jobs SET Technician = 'Nick', Status = 'Complete' WHERE id = '1'; ";
            sqlite_cmd.CommandText = "UPDATE Jobs SET SONumber = @sonumber, ServerModel = @servermodel, Quantity = @quantity, SalesRep = @salesrep, JobType = @jobtype, Issues = @issues, ActionTaken = @actiontaken, Status = @status," +
                " BuildQuantity = @buildquantity, BuildFWQuantity = @buildfwquantity, BuildTestQuantity = @buildtestquantity , CPUTestQuantity = @cputestquantity , CPUZQuantity = @cpuzquantity , CTOQuantity = @ctoquantity ," +
                " DIMMTestQuantity = @dimmtestquantity , IOBoardQuantity = @ioboardquantity , MotherboardQuantity = @motherboardquantity, RAMReplaceQuantity = @ramreplacequantity, RMAReplaceBuildQuantity = @rmareplacebuildquantity," +
                " PlugAndPlayQuantity = @plugandplayquantity , POTestingQuantity = @potestingquantity , PartTestingQuantity = @parttestingquantity, ClientGearQuantity = @clientgearquantity WHERE id = @id;";
           
            sqlite_cmd.CommandType = CommandType.Text;
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@id", vm.UpdateWeeklyRecord.id));
            //sqlite_cmd.Parameters.Add(new SQLiteParameter("@day", DateTime.Now));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@sonumber", vm.UpdateWeeklyRecord.SONumber + ""));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@servermodel", vm.UpdateWeeklyRecord.ServerModel + ""));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@quantity", vm.UpdateWeeklyRecord.Quantity));
            //sqlite_cmd.Parameters.Add(new SQLiteParameter("@technician", vm.CurrentUserInitials));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@salesrep", vm.UpdateWeeklyRecord.SalesRep + ""));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@jobtype", vm.UpdateWeeklyRecord.JobType + ""));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@issues", vm.UpdateWeeklyRecord.Issues + ""));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@actiontaken", vm.UpdateWeeklyRecord.ActionTaken + ""));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@status", vm.UpdateWeeklyRecord.Status + ""));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@buildquantity", vm.UpdateWeeklyRecord.BuildQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@buildfwquantity", vm.UpdateWeeklyRecord.BuildFWQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@buildtestquantity", vm.UpdateWeeklyRecord.BuildTestQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@cputestquantity", vm.UpdateWeeklyRecord.CPUTestQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@cpuzquantity", vm.UpdateWeeklyRecord.CPUZQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@ctoquantity", vm.UpdateWeeklyRecord.CTOQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@dimmtestquantity", vm.UpdateWeeklyRecord.DIMMTestQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@ioboardquantity", vm.UpdateWeeklyRecord.IOBoardQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@motherboardquantity", vm.UpdateWeeklyRecord.MotherboardQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@ramreplacequantity", vm.UpdateWeeklyRecord.RAMReplaceQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@rmareplacebuildquantity", vm.UpdateWeeklyRecord.RMAReplaceBuildQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@plugandplayquantity", vm.UpdateWeeklyRecord.PlugAndPlayQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@potestingquantity", vm.UpdateWeeklyRecord.POTestingQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@parttestingquantity", vm.UpdateWeeklyRecord.PartTestingQuantity));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@clientgearquantity", vm.UpdateWeeklyRecord.ClientGearQuantity));
            sqlite_cmd.ExecuteNonQuery();
        }

        public async void ReadAllData(SQLiteConnection conn)
        {
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Jobs";

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            while (sqlite_datareader.Read())
            {


                WeeklyServerRecordsModel job = new WeeklyServerRecordsModel();
                if (sqlite_datareader[0].GetType() != typeof(DBNull))
                    job.id = sqlite_datareader.GetInt32(0);
                if (sqlite_datareader[1].GetType() != typeof(DBNull))
                    job.Day = Convert.ToDateTime(sqlite_datareader.GetString(1));
                if (sqlite_datareader[2].GetType() != typeof(DBNull))
                    job.SONumber = sqlite_datareader.GetString(2);
                if (sqlite_datareader[3].GetType() != typeof(DBNull))
                    job.ServerModel = sqlite_datareader.GetString(3);
                if (sqlite_datareader[4].GetType() != typeof(DBNull))
                    job.Quantity = sqlite_datareader.GetInt32(4);
                if (sqlite_datareader[5].GetType() != typeof(DBNull))
                    job.Technician = sqlite_datareader.GetString(5);
                if (sqlite_datareader[6].GetType() != typeof(DBNull))
                    job.SalesRep = sqlite_datareader.GetString(6);
                if (sqlite_datareader[7].GetType() != typeof(DBNull))
                    job.JobType = sqlite_datareader.GetString(7);
                if (sqlite_datareader[8].GetType() != typeof(DBNull))
                    job.Issues = sqlite_datareader.GetString(8);
                if (sqlite_datareader[9].GetType() != typeof(DBNull))
                    job.ActionTaken = sqlite_datareader.GetString(9);
                if (sqlite_datareader[10].GetType() != typeof(DBNull))
                    job.Status = sqlite_datareader.GetString(10);
                if (sqlite_datareader[11].GetType() != typeof(DBNull))
                    job.BuildQuantity = sqlite_datareader.GetInt32(11);
                if (sqlite_datareader[12].GetType() != typeof(DBNull))
                    job.BuildFWQuantity = sqlite_datareader.GetInt32(12);
                if (sqlite_datareader[13].GetType() != typeof(DBNull))
                    job.BuildTestQuantity = sqlite_datareader.GetInt32(13);
                if (sqlite_datareader[14].GetType() != typeof(DBNull))
                    job.CPUTestQuantity = sqlite_datareader.GetInt32(14);
                if (sqlite_datareader[15].GetType() != typeof(DBNull))
                    job.CPUZQuantity = sqlite_datareader.GetInt32(15);
                if (sqlite_datareader[16].GetType() != typeof(DBNull))
                    job.CTOQuantity = sqlite_datareader.GetInt32(16);
                if (sqlite_datareader[17].GetType() != typeof(DBNull))
                    job.DIMMTestQuantity = sqlite_datareader.GetInt32(17);
                if (sqlite_datareader[18].GetType() != typeof(DBNull))
                    job.IOBoardQuantity = sqlite_datareader.GetInt32(18);
                if (sqlite_datareader[19].GetType() != typeof(DBNull))
                    job.MotherboardQuantity = sqlite_datareader.GetInt32(19);
                if (sqlite_datareader[20].GetType() != typeof(DBNull))
                    job.RAMReplaceQuantity = sqlite_datareader.GetInt32(20);
                if (sqlite_datareader[21].GetType() != typeof(DBNull))
                    job.RMAReplaceBuildQuantity = sqlite_datareader.GetInt32(21);
                if (sqlite_datareader[22].GetType() != typeof(DBNull))
                    job.PlugAndPlayQuantity = sqlite_datareader.GetInt32(22);
                if (sqlite_datareader[23].GetType() != typeof(DBNull))
                    job.POTestingQuantity = sqlite_datareader.GetInt32(23);
                if (sqlite_datareader[24].GetType() != typeof(DBNull))
                    job.PartTestingQuantity = sqlite_datareader.GetInt32(24);
                if (sqlite_datareader[25].GetType() != typeof(DBNull))
                    job.ClientGearQuantity = sqlite_datareader.GetInt32(25);

                vm.WeeklyServerRecordCollection.Add(job);


                // string myreader = sqlite_datareader.GetString(0);
                //  MessageBox.Show(sqlite_datareader.GetString(1).ToString());

              

            }

            await PutTaskDelay(400);


            CalculateTotals();
            // conn.Close();
        }


        //To check the presence of a column in a table
        private bool CheckColumnExists(SQLiteConnection conn, string tableName, string columnName)
        {
        
                var cmd = conn.CreateCommand();
                cmd.CommandText = string.Format("PRAGMA table_info({0})", tableName);

                var reader = cmd.ExecuteReader();
                int nameIndex = reader.GetOrdinal("Name");
                while (reader.Read())
                {
                    if (reader.GetString(nameIndex).Equals(columnName))
                    {
                  
                        return true;
                    }
                }
           
            return false;
        }


        public async void ReadData(SQLiteConnection conn, string sqlquery)
        {
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            //Create Command
            sqlite_cmd = conn.CreateCommand();
            vm.NotificationMessage = "";
            

            //****** ADD ANY CHANGES TO COLUMS HERE, WILL UPDATE EXTERNAL DATABASE SOURCES WITH NO INTERVENTION ****
            //Check for new columns pressence, if not present add
            if (CheckColumnExists(conn, "Jobs", "POTestingQuantity") != true)
            {
                //Check for existense of columns, if not add
                sqlite_cmd.CommandText = "ALTER TABLE Jobs ADD COLUMN POTestingQuantity INTEGER DEFAULT 0;";
                sqlite_cmd.ExecuteNonQuery();
            }
            //Check if Part Testing column is present
            if (CheckColumnExists(conn,"Jobs","PartTestingQuantity") != true)
            {
                //Check for existense of columns, if not add
                sqlite_cmd.CommandText = "ALTER TABLE Jobs ADD COLUMN PartTestingQuantity INTEGER DEFAULT 0;";
                sqlite_cmd.ExecuteNonQuery();
            }
            //Check if Client Gear column is present
            if (CheckColumnExists(conn, "Jobs", "ClientGearQuantity") != true)
            {
                //Check for existense of columns, if not add
                sqlite_cmd.CommandText = "ALTER TABLE Jobs ADD COLUMN ClientGearQuantity INTEGER DEFAULT 0;";
                sqlite_cmd.ExecuteNonQuery();
            }
            //****** END ******


            //Run Read query
            sqlite_cmd.CommandText = sqlquery;

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            while (sqlite_datareader.Read())
            {


                WeeklyServerRecordsModel job = new WeeklyServerRecordsModel();
                job.id = sqlite_datareader.GetInt32(0);
                job.Day = Convert.ToDateTime(sqlite_datareader.GetString(1));
                job.SONumber = sqlite_datareader.GetString(2).ToUpper();
                job.ServerModel = sqlite_datareader.GetString(3);
                job.Quantity = sqlite_datareader.GetInt32(4);
                job.Technician = sqlite_datareader.GetString(5);
                job.SalesRep = sqlite_datareader.GetString(6);
                job.JobType = sqlite_datareader.GetString(7);
                job.Issues = sqlite_datareader.GetString(8);
                job.ActionTaken = sqlite_datareader.GetString(9);
                job.Status = sqlite_datareader.GetString(10);
                job.BuildQuantity = sqlite_datareader.GetInt32(11);
                job.BuildFWQuantity = sqlite_datareader.GetInt32(12);
                job.BuildTestQuantity = sqlite_datareader.GetInt32(13);
                job.CPUTestQuantity = sqlite_datareader.GetInt32(14);
                job.CPUZQuantity = sqlite_datareader.GetInt32(15);
                job.CTOQuantity = sqlite_datareader.GetInt32(16);
                job.DIMMTestQuantity = sqlite_datareader.GetInt32(17);
                job.IOBoardQuantity = sqlite_datareader.GetInt32(18);
                job.MotherboardQuantity = sqlite_datareader.GetInt32(19);
                job.RAMReplaceQuantity = sqlite_datareader.GetInt32(20);
                job.RMAReplaceBuildQuantity = sqlite_datareader.GetInt32(21);
                job.PlugAndPlayQuantity = sqlite_datareader.GetInt32(22);
                job.POTestingQuantity = sqlite_datareader.GetInt32(23);
                job.PartTestingQuantity = sqlite_datareader.GetInt32(24);
                job.ClientGearQuantity = sqlite_datareader.GetInt32(25);


                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    vm.CurrentFilter = "Please Wait...";
                    vm.WeeklyServerRecordCollection.Add(job);
                });
                


                // string myreader = sqlite_datareader.GetString(0);
                //  MessageBox.Show(sqlite_datareader.GetString(1).ToString());
            }

            await PutTaskDelay(400);
            

            CalculateTotals();
          
            // conn.Close();
        }



        public async void ReadDataForReport(SQLiteConnection conn, string sqlquery)
        {
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            //Create Command
            sqlite_cmd = conn.CreateCommand();


            //****** ADD ANY CHANGES TO COLUMS HERE, WILL UPDATE EXTERNAL DATABASE SOURCES WITH NO INTERVENTION ****
            //Check for new columns pressence, if not present add
            if (CheckColumnExists(conn, "Jobs", "POTestingQuantity") != true)
            {
                //Check for existense of columns, if not add
                sqlite_cmd.CommandText = "ALTER TABLE Jobs ADD COLUMN POTestingQuantity INTEGER DEFAULT 0;";
                sqlite_cmd.ExecuteNonQuery();
            }
            //Check if Part Testing column is present
            if (CheckColumnExists(conn, "Jobs", "PartTestingQuantity") != true)
            {
                //Check for existense of columns, if not add
                sqlite_cmd.CommandText = "ALTER TABLE Jobs ADD COLUMN PartTestingQuantity INTEGER DEFAULT 0;";
                sqlite_cmd.ExecuteNonQuery();
            }

            //Check if Client Gear column is present
            if (CheckColumnExists(conn, "Jobs", "ClientGearQuantity") != true)
            {
                //Check for existense of columns, if not add
                sqlite_cmd.CommandText = "ALTER TABLE Jobs ADD COLUMN ClientGearQuantity INTEGER DEFAULT 0;";
                sqlite_cmd.ExecuteNonQuery();
            }
            //****** END ******


            //Run Read query
            sqlite_cmd.CommandText = sqlquery;

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            while (sqlite_datareader.Read())
            {


                WeeklyServerRecordsModel job = new WeeklyServerRecordsModel();
                job.id = sqlite_datareader.GetInt32(0);
                job.Day = Convert.ToDateTime(sqlite_datareader.GetString(1));
                job.SONumber = sqlite_datareader.GetString(2);
                job.ServerModel = sqlite_datareader.GetString(3);
                job.Quantity = sqlite_datareader.GetInt32(4);
                job.Technician = sqlite_datareader.GetString(5);
                job.SalesRep = sqlite_datareader.GetString(6);
                job.JobType = sqlite_datareader.GetString(7);
                job.Issues = sqlite_datareader.GetString(8);
                job.ActionTaken = sqlite_datareader.GetString(9);
                job.Status = sqlite_datareader.GetString(10);
                job.BuildQuantity = sqlite_datareader.GetInt32(11);
                job.BuildFWQuantity = sqlite_datareader.GetInt32(12);
                job.BuildTestQuantity = sqlite_datareader.GetInt32(13);
                job.CPUTestQuantity = sqlite_datareader.GetInt32(14);
                job.CPUZQuantity = sqlite_datareader.GetInt32(15);
                job.CTOQuantity = sqlite_datareader.GetInt32(16);
                job.DIMMTestQuantity = sqlite_datareader.GetInt32(17);
                job.IOBoardQuantity = sqlite_datareader.GetInt32(18);
                job.MotherboardQuantity = sqlite_datareader.GetInt32(19);
                job.RAMReplaceQuantity = sqlite_datareader.GetInt32(20);
                job.RMAReplaceBuildQuantity = sqlite_datareader.GetInt32(21);
                job.PlugAndPlayQuantity = sqlite_datareader.GetInt32(22);
                job.POTestingQuantity = sqlite_datareader.GetInt32(23);
                job.PartTestingQuantity = sqlite_datareader.GetInt32(24);
                job.ClientGearQuantity = sqlite_datareader.GetInt32(25);


                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    vm.CurrentFilter = "Please Wait...";
                    vm.WeeklyServerReportCollection.Add(job);
                });



                // string myreader = sqlite_datareader.GetString(0);
                //  MessageBox.Show(sqlite_datareader.GetString(1).ToString());
            }

            await PutTaskDelay(400);
            
            CalculateTotals();
            vm.CurrentFilter = "";
            // conn.Close();
        }

        //public static void ListViewToCSV(ListView listView, string filePath, bool includeHidden)
        //{
        //    //make header string
        //    StringBuilder result = new StringBuilder();
        //    WriteCSVRow(result, listView.Columns.Count, i => includeHidden || listView.Columns[i].Width > 0, i => listView.Columns[i].Text);

        //    //export data rows
        //    foreach (ListViewItem listItem in listView.Items)
        //        WriteCSVRow(result, listView.Columns.Count, i => includeHidden || listView.Columns[i].Width > 0, i => listItem.SubItems[i].Text);

        //    File.WriteAllText(filePath, result.ToString());
        //}

        //private static void WriteCSVRow(StringBuilder result, int itemsCount, Func<int, bool> isColumnNeeded, Func<int, string> columnValue)
        //{
        //    bool isFirstTime = true;
        //    for (int i = 0; i < itemsCount; i++)
        //    {
        //        if (!isColumnNeeded(i))
        //            continue;

        //        if (!isFirstTime)
        //            result.Append(",");
        //        isFirstTime = false;

        //        result.Append(String.Format("\"{0}\"", columnValue(i)));
        //    }
        //    result.AppendLine();
        //}



        public static void ReadReplace(string csvpath)
        {

            //string text = File.ReadAllText(csvpath);

            //using (StreamReader sr = new StreamReader(csvpath))
            //{
            //    String line;
            //    while ((line = sr.ReadLine()) != null)
            //    {
            //        string[] Column = line.Split(',');

            //        foreach(var itm in Column)
            //        {
            //           // MessageBox.Show(itm.ToString());
            //        }

            //       // string serviceId = Column[4].Trim('"');
            //        //string appendedServiceId = serviceId + "a";

            //       // text = text.Replace(serviceId, appendedServiceId);
            //       // text = text;

            //    }
            //}

            //File.WriteAllText(csvpath, text);




        }



        private void FilterBTN_Click(object sender, RoutedEventArgs e)
        {

            CalculateTotals();

        }

        private async void FilterAllBTN_Click(object sender, RoutedEventArgs e)
        {
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            vm.WeeklyServerRecordCollection.Clear();
            

            Mouse.OverrideCursor = Cursors.Wait;
           
           // ProgressBar.IsIndeterminate = true;

          
            //GET DATE VALUES OVER THE YEAR TO DATE
            var dateFrom = vm.DateFrom.ToString(DateTime.Now.Year.ToString() + "-01-01");
            var dateTo = DateTime.Now.ToString("yyyy-MM-dd");

            //Set the Report Date Pickers to the filter range
            vm.DateFrom = Convert.ToDateTime(dateFrom);
            vm.DateTo = Convert.ToDateTime(dateTo);

           
            if (POOnlyCB.IsChecked == true)
            {
              
                await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE POTestingQuantity > 0 AND Day BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ORDER BY id DESC"));
            
            }
            else
            {
                await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE date(Day) BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ORDER BY id DESC"));
            }

            //ProgressBar.IsIndeterminate = false;
            Mouse.OverrideCursor = null;

            vm.CurrentFilter = "Year";

            //Close Db
            sqlite_conn.Close();
        }

        private async void FilterThursdayBTN_Click(object sender, RoutedEventArgs e)
        {
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            vm.WeeklyServerRecordCollection.Clear();

            Mouse.OverrideCursor = Cursors.Wait;

            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            //var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            //var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //MessageBox.Show(dateFrom + " To " + dateTo);

            vm.DateFrom = DateTimeExtensions.StartOfWeek(DateTime.Now, DayOfWeek.Thursday);
            vm.DateTo = DateTime.Now;


            if (POOnlyCB.IsChecked == true)
            {
                //if (vm.CurrentFilter == "Purchase Orders Only")
                //{
                await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE POTestingQuantity > 0 AND Day BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "' ORDER BY id DESC"));
                //}

            }
            else
            {
                await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE Day BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "' ORDER BY id DESC"));
            }


            Mouse.OverrideCursor = null;
            vm.CurrentFilter = "Thursday To Thursday";

            //Close Db
            sqlite_conn.Close();
        }

        private async void FilterMondayBTN_Click(object sender, RoutedEventArgs e)
        {
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            vm.WeeklyServerRecordCollection.Clear();

            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            //var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            //var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            Mouse.OverrideCursor = Cursors.Wait;

            vm.DateFrom = DateTimeExtensions.StartOfWeek(DateTime.Now, DayOfWeek.Monday);
            vm.DateTo = DateTime.Now;


            if (POOnlyCB.IsChecked == true)
            {
                //if (vm.CurrentFilter == "Purchase Orders Only")
                //{
                await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE POTestingQuantity > 0 AND Day BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "' ORDER BY id DESC"));
                //}

            }
            else
            {

                await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE Day BETWEEN '" + vm.DateFrom.ToString("yyyy-MM-dd") + "' AND '" + vm.DateTo.ToString("yyyy-MM-dd") + "' ORDER BY id DESC"));
            }

            //ReadAllData(sqlite_conn);
            Mouse.OverrideCursor = null;
            vm.CurrentFilter = "Monday To Monday";

            //Close Db
            sqlite_conn.Close();
        }

        private async void FilterOnHoldBTN_Click(object sender, RoutedEventArgs e)
        {
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            vm.WeeklyServerRecordCollection.Clear();

            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            Mouse.OverrideCursor = Cursors.Wait;

            //MessageBox.Show(dateFrom + " To " + dateTo);

            // string SQLcmd = $"SELECT * FROM sorteos WHERE DATE(fecha) BETWEEN ('{FechaInicio}') AND ('{FechaFinal}')";

            if (POOnlyCB.IsChecked == true)
            {
                //if (vm.CurrentFilter == "Purchase Orders Only")
                //{
                await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE POTestingQuantity > 0 AND Status = 'On Hold' ORDER BY id DESC"));
                //}

            }
            else
            {

                await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE Status = 'On Hold' ORDER BY id DESC"));
            }



            //ReadAllData(sqlite_conn);
            Mouse.OverrideCursor = null;
            vm.CurrentFilter = "On Hold Jobs";

            //Close Db
            sqlite_conn.Close();
        }

       

        private void DPFrom_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            vm.CurrentFilter = "";
        }

        private void DPTo_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            vm.CurrentFilter = "";
        }

        private void SearchBTN_Click(object sender, RoutedEventArgs e)
        {
            
            SearchBySalesOrderNo();
        }

        private void SearchTXT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SearchBySalesOrderNo();
            }
        }



        private async void SearchBySalesOrderNo()
        {
            vm.CurrentFilter = "";
            vm.NotificationMessage = "";

            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            vm.WeeklyServerRecordCollection.Clear();

            Mouse.OverrideCursor = Cursors.Wait;

            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            //var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            //var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //MessageBox.Show(dateFrom + " To " + dateTo);

            // string SQLcmd = $"SELECT * FROM sorteos WHERE DATE(fecha) BETWEEN ('{FechaInicio}') AND ('{FechaFinal}')";
            if (string.IsNullOrEmpty(SearchTXT.Text))
            {
                //Display Message
                vm.NotificationMessage = "Please enter a value into the Search TextBox!";
            }
            else
            {
                string search = SearchTXT.Text;

                await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE SONumber LIKE '" + search + "%' OR Technician LIKE '" + search + "%' ORDER BY id DESC")); 
            }

            

            //ReadAllData(sqlite_conn);
            Mouse.OverrideCursor = null;
            vm.CurrentFilter = "";

            //Close Db
            sqlite_conn.Close();
        }

        private void WRTabCtrl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


        private async void FilterDateResultsBTN_Click(object sender, RoutedEventArgs e)
        {
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            vm.WeeklyServerRecordCollection.Clear();

            Mouse.OverrideCursor = Cursors.Wait;

            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");


            await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE Day BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ORDER BY id DESC"));

            Mouse.OverrideCursor = null;
            vm.CurrentFilter = "";
            //Close Db
            sqlite_conn.Close();
        }

        private async void ExportCSVBTN_Click(object sender, RoutedEventArgs e)
        {

            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            vm.WeeklyServerRecordCollection.Clear();

            Mouse.OverrideCursor = Cursors.Wait;

            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");


            await System.Threading.Tasks.Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE Day BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ORDER BY id DESC"));

            Mouse.OverrideCursor = null;
            //Close Db
            sqlite_conn.Close();

            
            //Export report to csv
            StaticFunctions.CreateWeeklyServerRecordsCSV(vm.WeeklyServerRecordCollection.Where(x => x.Day >= vm.DateFrom).Where(x => x.Day <= vm.DateTo).ToList());
            Mouse.OverrideCursor = null;
        }

        private async void ExportUserBTN_Click(object sender, RoutedEventArgs e)
        {
            //Clear the collection
            vm.WeeklyServerReportCollection.Clear();

            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.AddDays(1).ToString("yyyy-MM-dd");

            //return all items in date range
            //Get Results with filter
            await Task.Run(() => ReadBackgroundData("SELECT * FROM Jobs WHERE Day BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ORDER BY  id, SONumber DESC"));

            //Create Async Task for long running process
            GenerateDriveReport(WeeklyServersLV, vm.DateFrom.ToShortDateString() + " - " + vm.DateTo.ToShortDateString());
        }


      


        public async Task ReadBackgroundData(string sqlquery)
        {
            try
            {

                //Create connection
                sqlite_conn = CreateConnection(cs);

                //Declare observable collection

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();


                //Create Flow Doc


                //Loop through results set
                while (sqlite_datareader.Read())
                {


                    WeeklyServerRecordsModel itm = new WeeklyServerRecordsModel();
                    if (sqlite_datareader[0].GetType() != typeof(DBNull))
                        itm.id = sqlite_datareader.GetInt32(0);
                    if (sqlite_datareader[1].GetType() != typeof(DBNull))
                    {

                        string date = sqlite_datareader.GetString(1);
                        itm.Day = Convert.ToDateTime(date);

                    }

                    if (sqlite_datareader[2].GetType() != typeof(DBNull))
                        itm.SONumber = sqlite_datareader.GetString(2);
                    if (sqlite_datareader[3].GetType() != typeof(DBNull))
                        itm.ServerModel = sqlite_datareader.GetString(3);
                    if (sqlite_datareader[4].GetType() != typeof(DBNull))
                        itm.Quantity = sqlite_datareader.GetInt32(4);
                    if (sqlite_datareader[5].GetType() != typeof(DBNull))
                        itm.Technician = sqlite_datareader.GetString(5);
                    if (sqlite_datareader[6].GetType() != typeof(DBNull))
                        itm.SalesRep = sqlite_datareader.GetString(6);
                    if (sqlite_datareader[7].GetType() != typeof(DBNull))
                        itm.JobType = sqlite_datareader.GetString(7);
                    if (sqlite_datareader[8].GetType() != typeof(DBNull))
                        itm.Issues = sqlite_datareader.GetString(8);
                    if (sqlite_datareader[9].GetType() != typeof(DBNull))
                        itm.ActionTaken = sqlite_datareader.GetString(9);
                    if (sqlite_datareader[10].GetType() != typeof(DBNull))
                        itm.Status = sqlite_datareader.GetString(10);
                    if (sqlite_datareader[11].GetType() != typeof(DBNull))
                        itm.BuildQuantity = sqlite_datareader.GetInt32(11);
                    if (sqlite_datareader[12].GetType() != typeof(DBNull))
                        itm.BuildFWQuantity= sqlite_datareader.GetInt32(12);
                    if (sqlite_datareader[13].GetType() != typeof(DBNull))
                        itm.BuildTestQuantity = sqlite_datareader.GetInt32(13);
                    if (sqlite_datareader[14].GetType() != typeof(DBNull))
                        itm.CPUTestQuantity = sqlite_datareader.GetInt32(14);
                    if (sqlite_datareader[15].GetType() != typeof(DBNull))
                        itm.CPUZQuantity = sqlite_datareader.GetInt32(15);
                    if (sqlite_datareader[16].GetType() != typeof(DBNull))
                        itm.CTOQuantity = sqlite_datareader.GetInt32(16);
                    if (sqlite_datareader[17].GetType() != typeof(DBNull))
                        itm.DIMMTestQuantity = sqlite_datareader.GetInt32(17);
                    if (sqlite_datareader[18].GetType() != typeof(DBNull))
                        itm.IOBoardQuantity = sqlite_datareader.GetInt32(18);
                    if (sqlite_datareader[19].GetType() != typeof(DBNull))
                        itm.MotherboardQuantity = sqlite_datareader.GetInt32(19);
                    if (sqlite_datareader[20].GetType() != typeof(DBNull))
                        itm.RAMReplaceQuantity = sqlite_datareader.GetInt32(20);
                    if (sqlite_datareader[21].GetType() != typeof(DBNull))
                        itm.RMAReplaceBuildQuantity = sqlite_datareader.GetInt32(21);
                    if (sqlite_datareader[22].GetType() != typeof(DBNull))
                        itm.PlugAndPlayQuantity = sqlite_datareader.GetInt32(22);
                    if (sqlite_datareader[23].GetType() != typeof(DBNull))
                        itm.POTestingQuantity = sqlite_datareader.GetInt32(23);
                    if (sqlite_datareader[24].GetType() != typeof(DBNull))
                        itm.PartTestingQuantity = sqlite_datareader.GetInt32(24);
                    if (sqlite_datareader[25].GetType() != typeof(DBNull))
                        itm.ClientGearQuantity = sqlite_datareader.GetInt32(25);



                    //Update Collection via dispatcher because async
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        //Add item to collection
                        vm.WeeklyServerReportCollection.Add(itm);

                        // If collection changes Count
                        vm.RecordsTotal = WeeklyServersLV.Items.Count.ToString();

                    });


                }

                //Close connection
                sqlite_conn.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //return null;

            }

            //return null;
        }



        public async void GenerateDriveReport(ListView lvValue, string daterange)
        {


            //Create new Flow Document Async
            FlowDocument ReportFD = new FlowDocument();
            Section MainSection = new Section();
            MainSection.Padding = new Thickness(2);
            MainSection.Margin = new Thickness(0);
            Section mySectionReportHeader = new Section();
            mySectionReportHeader.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#0f89bd");  //(Brush)bc.ConvertFrom("#CC119EDA");



            Paragraph ReportHeader = new Paragraph(new Run("Weekly Server Records Breakdown"));
            ReportHeader.Padding = new Thickness(0, 5, 0, 5);
            ReportHeader.FontSize = 24;
            ReportHeader.FontFamily = new FontFamily("Segoe UI");
            ReportHeader.Foreground = Brushes.White;
            ReportHeader.TextAlignment = TextAlignment.Center;
            ReportHeader.Margin = new Thickness(0, 5, 0, 5);

            //Report date or title header
            Paragraph ReportDateHeader = new Paragraph();
            ReportDateHeader = new Paragraph(new Run("Total Items " + vm.WeeklyServerReportCollection.Select(p => p.BuildQuantity + p.BuildFWQuantity + p.BuildTestQuantity + p.ClientGearQuantity + p.CPUTestQuantity + p.CPUZQuantity + p.CTOQuantity + p.DIMMTestQuantity + p.IOBoardQuantity + p.MotherboardQuantity + p.PartTestingQuantity + p.PlugAndPlayQuantity + p.POTestingQuantity + p.RAMReplaceQuantity + p.RMAReplaceBuildQuantity).Sum().ToString()));
            ReportDateHeader.Padding = new Thickness(0, 1, 0, 1);
            ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
            ReportDateHeader.FontSize = 20;
            ReportDateHeader.Foreground = Brushes.White;
            ReportDateHeader.TextAlignment = TextAlignment.Center;
            ReportDateHeader.Margin = new Thickness(0, 1, 0, 1);

            //Report date or title header
            Paragraph ReportPOHeader = new Paragraph();
            ReportPOHeader = new Paragraph(new Run("Total Orders " + vm.WeeklyServerReportCollection.Select(o => o.SONumber.Trim()).Distinct().Count().ToString()));
            ReportPOHeader.Padding = new Thickness(0, 1, 0, 1);
            ReportPOHeader.FontFamily = new FontFamily("Segoe UI");
            ReportPOHeader.FontSize = 20;
            ReportPOHeader.Foreground = Brushes.White;
            ReportPOHeader.TextAlignment = TextAlignment.Center;
            ReportPOHeader.Margin = new Thickness(0, 1, 0, 1);

            Paragraph ReportDate = new Paragraph();
            ReportDate = new Paragraph(new Run(daterange));
            ReportDate.Padding = new Thickness(0, 1, 0, 1);
            ReportDate.FontFamily = new FontFamily("Segoe UI");
            ReportDate.FontSize = 16;
            ReportDate.Foreground = Brushes.White;
            ReportDate.TextAlignment = TextAlignment.Center;
            ReportDate.Margin = new Thickness(0, 1, 0, 1);




            //IMAGE NOT WORKING ON DIFFRENT THREADS
            //Add TB Logo
            //Create Block ui container and add image as a child
            BlockUIContainer blockUIImage = new BlockUIContainer();
            blockUIImage.Padding = new Thickness(10);
            blockUIImage.Margin = new Thickness(10);
            BitmapImage bmp0 = new BitmapImage();

            bmp0.BeginInit();
            bmp0.UriSource = new Uri("pack://application:,,,/Images/TechBuyerLogoCenteredReports.png", UriKind.Absolute);
            bmp0.CacheOption = BitmapCacheOption.OnLoad;
            bmp0.EndInit();
            bmp0.Freeze();

            System.Windows.Controls.Image img0 = new System.Windows.Controls.Image();

            img0.Source = bmp0;
            img0.Width = 150;
            img0.Height = 75;

            blockUIImage.Child = img0;
            //Add image to section
            mySectionReportHeader.Blocks.Add(blockUIImage);

            //IMAGE NOT WORKING ON DIFFRENT THREADS

            //Add Paragraphs to Header
            mySectionReportHeader.Blocks.Add(ReportHeader);
            mySectionReportHeader.Blocks.Add(ReportDateHeader);
            mySectionReportHeader.Blocks.Add(ReportPOHeader);
            mySectionReportHeader.Blocks.Add(ReportDate);


            //Operative count

            List<string> opList = new List<string>();
            //List<string> poList = new List<string>();
            List<WeeklyServerRecordsModel> poList = new List<WeeklyServerRecordsModel>();
            List<string> totalpoList = new List<string>();


            //Get distinct value in column
            opList = vm.WeeklyServerRecordCollection.Select(o => o.Technician.Replace(o.Day.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim()).Distinct().ToList<string>();
            //get all ponumbers operative processed


            //New Paragraph
            Paragraph ReportOperativeTotals = new Paragraph();
            Paragraph POOperativeTotals = new Paragraph();
            Paragraph ReportPOTotals = new Paragraph();


            //Create new Section to host table
            Section mySectionRecords = new Section();
            mySectionRecords.Margin = new Thickness(0);

            //ADD POTOTALS

            //Get all distinct PO Numbers
            totalpoList = vm.WeeklyServerReportCollection.Select(p => p.SONumber).ToList<string>();


            //Operative PO Section
            string operatives = "";
            string jobtypes = "";
            string pos = "";

            foreach (var op in opList)
            {
                //Clear list before populating
                poList.Clear();
                //Clear Jobtypes
                jobtypes = "";

                //Create new Paragraph Add Operative Ref
                ReportOperativeTotals = new Paragraph(new Run(op));
                ReportOperativeTotals.Padding = new Thickness(1, 1, 1, 1);
                ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
                ReportOperativeTotals.FontSize = 15;
                ReportOperativeTotals.Foreground = Brushes.MediumPurple;
                ReportOperativeTotals.TextAlignment = TextAlignment.Center;
                ReportOperativeTotals.Margin = new Thickness(1, 1, 1, 1);
                //Add to block
                mySectionRecords.Blocks.Add(ReportOperativeTotals);

                //Pull Object to do count across multiple fields
                //IEnumerable<WeeklyServerRecordsModel> wsr = vm.WeeklyServerReportCollection.Select(p => p.SONumber).Distinct().Where(o => o.Technician.Replace("Technician Name:", "").Replace("Tested By:", "").Trim() + "" == op);
                //Count all fields to return total
                //int jobTotals = wsr.FirstOrDefault().BuildQuantity + wsr.FirstOrDefault().BuildFWQuantity + wsr.FirstOrDefault().BuildTestQuantity + wsr.FirstOrDefault().ClientGearQuantity + wsr.FirstOrDefault().CPUTestQuantity + wsr.FirstOrDefault().CPUZQuantity + wsr.FirstOrDefault().CTOQuantity + wsr.FirstOrDefault().DIMMTestQuantity + wsr.FirstOrDefault().IOBoardQuantity + wsr.FirstOrDefault().MotherboardQuantity + wsr.FirstOrDefault().PartTestingQuantity + wsr.FirstOrDefault().PlugAndPlayQuantity + wsr.FirstOrDefault().POTestingQuantity + wsr.FirstOrDefault().RAMReplaceQuantity + wsr.FirstOrDefault().RMAReplaceBuildQuantity;
               
                //Count the items an operative has processed in collection
                //operatives += " Orders: " + vm.WeeklyServerReportCollection.Where(p => p.Technician.Replace(p.Day.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.SONumber).Distinct().Count().ToString() + " Items: " + jobTotals.ToString();
                operatives += " Orders: " + vm.WeeklyServerReportCollection.Where(p => p.Technician.Replace(p.Day.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.SONumber).Distinct().Count().ToString() + " Items: " + vm.WeeklyServerReportCollection.Where(p => p.Technician.Replace(p.Day.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.BuildQuantity + p.BuildFWQuantity + p.BuildTestQuantity + p.ClientGearQuantity + p.CPUTestQuantity + p.CPUZQuantity + p.CTOQuantity + p.DIMMTestQuantity + p.IOBoardQuantity + p.MotherboardQuantity + p.PartTestingQuantity + p.PlugAndPlayQuantity + p.POTestingQuantity + p.RAMReplaceQuantity + p.RMAReplaceBuildQuantity).Sum().ToString();

                ReportOperativeTotals = new Paragraph(new Run(operatives));
                ReportOperativeTotals.Padding = new Thickness(1, 1, 1, 1);
                ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
                ReportOperativeTotals.FontSize = 15;
                ReportOperativeTotals.Foreground = Brushes.Green;
                ReportOperativeTotals.TextAlignment = TextAlignment.Center;
                ReportOperativeTotals.Margin = new Thickness(1, 1, 1, 1);
                //Add to block
                mySectionRecords.Blocks.Add(ReportOperativeTotals);



                //Count each area
                jobtypes += " Builds: " + vm.WeeklyServerReportCollection.Where(p => p.Technician.Replace(p.Day.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.BuildQuantity + p.BuildFWQuantity + p.BuildTestQuantity).Sum().ToString();
                jobtypes += " CTO: " + vm.WeeklyServerReportCollection.Where(p => p.Technician.Replace(p.Day.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.CTOQuantity).Sum().ToString();
                jobtypes += " RMA Builds: " + vm.WeeklyServerReportCollection.Where(p => p.Technician.Replace(p.Day.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.RMAReplaceBuildQuantity).Sum().ToString();
                jobtypes += " Motherboard: " + vm.WeeklyServerReportCollection.Where(p => p.Technician.Replace(p.Day.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.MotherboardQuantity).Sum().ToString();
                jobtypes += " Purchase Orders: " + vm.WeeklyServerReportCollection.Where(p => p.Technician.Replace(p.Day.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.POTestingQuantity).Sum().ToString();
                jobtypes += " Part Testing: " + vm.WeeklyServerReportCollection.Where(p => p.Technician.Replace(p.Day.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.PartTestingQuantity).Sum().ToString();
                jobtypes += " Client Gear: " + vm.WeeklyServerReportCollection.Where(p => p.Technician.Replace(p.Day.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.ClientGearQuantity).Sum().ToString();
                


                ReportOperativeTotals = new Paragraph(new Run(jobtypes));
                ReportOperativeTotals.Padding = new Thickness(1, 1, 1, 1);
                ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
                ReportOperativeTotals.FontSize = 15;
                ReportOperativeTotals.Foreground = (SolidColorBrush)new BrushConverter().ConvertFromString("#0f89bd");
                ReportOperativeTotals.TextAlignment = TextAlignment.Center;
                ReportOperativeTotals.Margin = new Thickness(1, 1, 1, 1);
                //Add to block
                mySectionRecords.Blocks.Add(ReportOperativeTotals);


                //Get breakdown of SO's per Operative  
                //poList = vm.WeeklyServerReportCollection.Where(p => p.Technician.Replace(p.Day.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.SONumber).Distinct().ToList<string>();

                //foreach (var po in poList)
                //{
                //    //Get SO Count and Individual PO Count
                //    pos += " " + po.ToUpper() + " " + " (" + vm.WeeklyServerReportCollection.Count(o => o.SONumber == po).ToString() + @"\" + vm.WeeklyServerReportCollection.Where(p => p.SONumber == po).Select(p => p.BuildQuantity + p.BuildFWQuantity + p.BuildTestQuantity + p.ClientGearQuantity + p.CPUTestQuantity + p.CPUZQuantity + p.CTOQuantity + p.DIMMTestQuantity + p.IOBoardQuantity + p.MotherboardQuantity + p.PartTestingQuantity + p.PlugAndPlayQuantity + p.POTestingQuantity + p.RAMReplaceQuantity + p.RMAReplaceBuildQuantity).Sum().ToString() + ")  ";

                //}

                //New code includes Job Type
                poList = vm.WeeklyServerReportCollection.Where(p => p.Technician.Replace(p.Day.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p).ToList<WeeklyServerRecordsModel>();

                foreach (var po in poList)
                {

                    //


                    //Get SO Count and Individual PO Count vm.WeeklyServerReportCollection.Count(o => o.SONumber == po.SONumber).ToString() + " " +  
                    pos += " " + po.SONumber.ToUpper() + " (" + po.ReturnJobTypes() + "" + @"" + vm.WeeklyServerReportCollection.Where(p => p.SONumber == po.SONumber).Select(p => p.BuildQuantity + p.BuildFWQuantity + p.BuildTestQuantity + p.ClientGearQuantity + p.CPUTestQuantity + p.CPUZQuantity + p.CTOQuantity + p.DIMMTestQuantity + p.IOBoardQuantity + p.MotherboardQuantity + p.PartTestingQuantity + p.PlugAndPlayQuantity + p.POTestingQuantity + p.RAMReplaceQuantity + p.RMAReplaceBuildQuantity).Sum().ToString() + ")  ";

                }

                //Create new Paragraph
                ReportOperativeTotals = new Paragraph(new Run(pos));
                ReportOperativeTotals.Padding = new Thickness(1, 1, 1, 1);
                ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
                ReportOperativeTotals.FontSize = 12;
                ReportOperativeTotals.Foreground = Brushes.Black;
                ReportOperativeTotals.TextAlignment = TextAlignment.Center;
                ReportOperativeTotals.Margin = new Thickness(1, 1, 1, 1);


                mySectionRecords.Blocks.Add(ReportOperativeTotals);



                operatives = "";
                pos = "";

            }




            //1 Add Header To Main Section
            MainSection.Blocks.Add(mySectionReportHeader);
            //2 Add Records to Main Section
            MainSection.Blocks.Add(mySectionRecords);


            //Add Main Section to flowdocument
            ReportFD.Blocks.Add(MainSection);


            //END OF FLOWDOCUMENT

            //Specify Filename
            String FileName = "";


            FileName = "Weekly Server Records Breakdown Report_" + vm.DateFrom.ToShortDateString().Replace("/", "-") + " To " + vm.DateTo.ToShortDateString().Replace("/", "-");


            //SAVE FLOW DOC TO PDF
            DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

            string SavePath = vm.myDocs + @"\" + FileName + @".pdf";

            //Clone the source document
            var str = XamlWriter.Save(ReportFD);
            var stringReader = new System.IO.StringReader(str);
            var xmlReader = XmlReader.Create(stringReader);
            var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

            DocumentPaginator Clonepaginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;

            //A4 Dimensions
            Clonepaginator.PageSize = new System.Windows.Size(1122, 793);

            Thickness t = new Thickness(72);  // copy.PagePadding;
            CloneDoc.PagePadding = new Thickness(
                             Math.Max(0, t.Left),
                               Math.Max(0, t.Top),
                             Math.Max(1122 - (0 + 1122), t.Right),
                             Math.Max(793 - (0 + 793), t.Bottom));



            CloneDoc.ColumnWidth = double.PositiveInfinity;

            //End A4 Dimensions

            //Convert Flow Doc to Fixed First
            var paginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;
            var package = Package.Open(new MemoryStream(), FileMode.Create, FileAccess.ReadWrite);
            var packUri = new Uri("pack://temp.xps");
            PackageStore.RemovePackage(packUri);
            PackageStore.AddPackage(packUri, package);
            var xps = new XpsDocument(package, CompressionOption.NotCompressed, packUri.ToString());
            XpsDocument.CreateXpsDocumentWriter(xps).Write(paginator);
            FixedDocument fddoc = new FixedDocument();


            //Requires Dispatcher
            Application.Current.Dispatcher.Invoke((Action)delegate
            {



                fddoc = xps.GetFixedDocumentSequence().References[0].GetDocument(true);
                // your code



                //Fixed document to PDF
                var ms = new MemoryStream();
                var package2 = Package.Open(ms, FileMode.Create);
                var doc = new XpsDocument(package2);
                var writer = XpsDocument.CreateXpsDocumentWriter(doc);
                writer.Write(fddoc.DocumentPaginator);

                //docWriter.Write(fddoc.DocumentPaginator);
                doc.Close();
                package2.Close();

                // Get XPS file bytes
                var bytes = ms.ToArray();
                ms.Dispose();

                // Print to PDF
                var outputFilePath = SavePath;
                PdfFilePrinter.PrintXpsToPdf(bytes, outputFilePath, "Document Title");


                //Launch report
                ////Launch file for printing
                if (File.Exists(outputFilePath))
                {
                    System.Diagnostics.Process.Start(outputFilePath);

                }

            });


            //create new instance
            //vm.ITADReportCollection = new ObservableCollection<ITADReporty>();



        }


        //private void OrderRefTXT_LostFocus(object sender, RoutedEventArgs e)
        //{
          

        //    vm.TextSuggestion.Add(vm.AddWeeklyRecord.SONumber);


        //    foreach(string suggest in vm.TextSuggestion)
        //    {
        //        Console.WriteLine(suggest);
        //    }
        //}





        //private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    ComboBoxItem SelectedBrand = (ComboBoxItem)SelectBrandCB.SelectedItem;
        //}
    }


    //public class SearchResult
    //{
    //    public string Title { get; set; }
    //    public string Content { get; set; }
    //    public string Url { get; set; }
    //}

    public class SearchResult
    {
        public string Location { get; set; }
        public string Name { get; set; }
        public string VersionString { get; set; }
        public string Key { get; set; }

        //public string IntelligentProvisioning { get; set; }
    }


    //private void RemCon_Click(object sender, RoutedEventArgs e)
    //{
    //    //MessageBox.Show(ServIPV4);
    //    //StaticFunctions.RunRemoteConsoleCommand(@"" + vm.myDocs + @"\HPTOOLS\HP iLO Integrated Remote Console\HPLOCONS.exe", @"-addr " + ServIPV4 + @":443 -name Administrator -password T3c#8uy3r! -lang en");
    //}

    //        private async void HPScript_Click(object sender, RoutedEventArgs e)
    //        {
    //            //Remote Console Launch
    //            // StaticFunctions.RunRemoteConsoleCommand(@"C:\Program Files (x86)\Hewlett-Packard\HP iLO Integrated Remote Console\HPLOCONS.exe", @"-addr 192.168.101.248:443 -name Administrator -password T3c#8uy3r! -lang en");
    //            //await PutTaskDelay(30000);

    //            ////RIBCL Example using HPQLOCFG
    //            // StaticFunctions.RunCLICommand(@"C:\Users\N.Myers\Desktop\HP ILO Scripts\HPCONFIGUTIL\HPQLOCFG.exe", @"-f C:\Users\N.Myers\Desktop\HP ILO Scripts\HPCONFIGUTIL\Scripts\iLo_Factory_Defaults.xml -s 192.168.101.248 -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "");
    //            //await PutTaskDelay(30000);

    //            // PutTaskDelay(5000);
    //            ////RIBCL Example using HPQLOCFG
    //            //StaticFunctions.RunCLICommand(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe", @"-f @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Scripts\iLo_Factory_Defaults.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "");
    //            // working StaticFunctions.RunCLICommand(@"C:\Users\N.Myers\Documents\HPCONFIGUTIL\HPQLOCFG.exe", @"-f C:\Users\N.Myers\Documents\HPCONFIGUTIL\iLo_Factory_Defaults.xml -s 192.168.101.248 -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "");
    //            ProcessPiper pp = new ProcessPiper(vm);
    //            ////FOR OLD XML ILO
    //            ////Requires XML Path 
    //            pp.Start(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"-f " + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL\Scripts\iLo_Factory_Defaults.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL");
    //            // StaticFunctions.RunCLICommand(@"C:\Users\N.Myers\Documents\HPCONFIGUTIL\HPQLOCFG.exe", @"-f C:\Users\N.Myers\Documents\HPCONFIGUTIL\Set_Host_PowerOn.xml -s 192.168.101.248 -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "");

    //            //StaticFunctions.RunCLICommand(@"C:\Users\N.Myers\Documents\HPCONFIGUTIL\HPQLOCFG.exe",
    //            //                              @"-f Set_Host_PowerOn.xml -s 192.168.101.248 -t user = Admin,password = pass");
    //            //StaticFunctions.RunCLICommand(@"C:\Program Files (x86)\Hewlett-Packard\HP iLO Integrated Remote Console\HPLOCONS.exe", @"-addr 192.168.101.248:443 -name Administrator -password T3c#8uy3r! -lang en");
    //        }

    //        private async void HPScript2_Click(object sender, RoutedEventArgs e)
    //        {
    //            //StaticFunctions.RunCLICommand(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe", @"-f @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Scripts\Set_Host_PowerOff.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "");
    //            //await PutTaskDelay(30000);
    //            ProcessPiper pp = new ProcessPiper(vm);
    //            ////FOR OLD XML ILO
    //            ////Requires XML Path 
    //            pp.Start(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"-f " + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL\Scripts\Set_Host_PowerOff.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL");
    //        }



    //        private async void HPScript3_Click(object sender, RoutedEventArgs e)
    //        {
    //            //StaticFunctions.RunCLICommand(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe", @"-f @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Scripts\Set_Host_PowerOn.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "");
    //            //await PutTaskDelay(30000);
    //            ProcessPiper pp = new ProcessPiper(vm);
    //            ////FOR OLD XML ILO
    //            ////Requires XML Path 
    //            pp.Start(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"-f " + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL\Scripts\Set_Host_PowerOn.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL");
    //        }

    //        private async void HPScript4_Click(object sender, RoutedEventArgs e)
    //        {

    //            //StaticFunctions.RunCLICommand(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe", @"-f @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Scripts\License.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "");
    //            //await PutTaskDelay(30000);
    //            ProcessPiper pp = new ProcessPiper(vm);
    //            ////FOR OLD XML ILO
    //            ////Requires XML Path 
    //            pp.Start(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"-f " + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL\Scripts\License.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL");
    //        }

    //        private async void HPScript5_Click(object sender, RoutedEventArgs e)
    //        {
    //            //StaticFunctions.RunCLICommand(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe", @"-f @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Scripts\Set_Server_name.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "");
    //            //await PutTaskDelay(30000);
    //            ProcessPiper pp = new ProcessPiper(vm);
    //            ////FOR OLD XML ILO
    //            ////Requires XML Path 
    //            pp.Start(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"-f " + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL\Scripts\Set_Server_name.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL");
    //        }

    //        private async void HPScript6_Click(object sender, RoutedEventArgs e)
    //        {
    //            //StaticFunctions.RunCLICommand(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe", @"-f " + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL\Scripts\Get_Embedded_Health.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "");
    //            //await PutTaskDelay(30000);
    //            ProcessPiper pp = new ProcessPiper(vm);
    //            ////FOR OLD XML ILO
    //            ////Requires XML Path 
    //            pp.Start(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"-f " + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL\Scripts\Get_Embedded_Health.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL");
    //        }

    //        private async void HPScript7_Click(object sender, RoutedEventArgs e)
    //        {
    //            //StaticFunctions.RunCLICommand(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe", @"-f @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Scripts\Add_User.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "");
    //            //await PutTaskDelay(30000);
    //            ProcessPiper pp = new ProcessPiper(vm);
    //            ////FOR OLD XML ILO
    //            ////Requires XML Path 
    //            pp.Start(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"-f " + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL\Scripts\Add_User.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL");
    //        }
    //        private async void HPScript8_Click(object sender, RoutedEventArgs e)
    //        {
    //            //StaticFunctions.RunCLICommand(@"@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe", @"-f @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Scripts\Delete_User.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "");
    //            //await PutTaskDelay(30000);
    //            ProcessPiper pp = new ProcessPiper(vm);
    //            ////FOR OLD XML ILO
    //            ////Requires XML Path 
    //            pp.Start(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\HPQLOCFG.exe"), @"-f " + @"\\pinnacle.local\tech_resources\Nebula\HP Tools\HPCONFIGUTIL\Scripts\Delete_User.xml -s " + ServIPV4 + @" -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", "", @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL");
    //        }

    //        private void IPV4TXTB_TextChanged(object sender, TextChangedEventArgs e)
    //        {
    //            //ServIPV4 = IPV4TXTB.Text;
    //        }

    //        private async void HPScript9_Click(object sender, RoutedEventArgs e)
    //        {
    //            ProcessPiper pp = new ProcessPiper(vm);

    //            //FOR ILO RESTFUL
    //            await System.Threading.Tasks.Task.Run(() => pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "factorydefaults --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));


    //        }

    //        private async void HPScript10_Click(object sender, RoutedEventArgs e)
    //        {
    //            ProcessPiper pp = new ProcessPiper(vm);

    //            //FOR ILO RESTFUL
    //            // ilo rest full path  C:\Program Files\Hewlett Packard Enterprise\RESTful Interface Tool\ilorest.exe  bootorder biosdefaults
    //            //pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "login 192.168.101.34 -u username -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool")  --reboot=ForceRestart;
    //            // pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "biosdefaults --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //            await System.Threading.Tasks.Task.Run(() => pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "biosdefaults --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
    //        }

    //        private async void HPScript11_Click(object sender, RoutedEventArgs e)
    //        {
    //            //ilolicense 332N6-VJMMM-MHTPD-L7XNR-29G8B
    //            ProcessPiper pp = new ProcessPiper(vm);

    //            //FOR ILO RESTFUL
    //            // ilo rest full path  C:\Program Files\Hewlett Packard Enterprise\RESTful Interface Tool\ilorest.exe  bootorder biosdefaults
    //            //pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "login 192.168.101.34 -u username -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //            await System.Threading.Tasks.Task.Run(() => pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "ilolicense 332N6-VJMMM-MHTPD-L7XNR-29G8B --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));
    //            //pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "ilolicense 332N6-VJMMM-MHTPD-L7XNR-29G8B --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //        }

    //        private async void HPScript12_Click(object sender, RoutedEventArgs e)
    //        {
    //            //To return the serverstate run the command without arguments. Possible values include: None, Unknown, Reset, PowerOff, InPost, InPostDiscoveryComplete, FinishedPost.
    //            //iLOrest > serverstate

    //            //ilolicense 332N6-VJMMM-MHTPD-L7XNR-29G8B
    //           ProcessPiper pp = new ProcessPiper(vm);


    //            //await StartILORest(new System.IO.FileInfo(@"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "serverstate --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");

    //            // The await causes the handler to return immediately.
    //            await System.Threading.Tasks.Task.Run(() => pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "serverstate --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

    //          //  await  Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new Action(() => StartILORest(new System.IO.FileInfo(@"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "serverstate --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool")));
    //                //FOR ILO RESTFUL
    //                // ilo rest full path  C:\Program Files\Hewlett Packard Enterprise\RESTful Interface Tool\ilorest.exe  bootorder biosdefaults
    //                //pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "login 192.168.101.34 -u username -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");

    //            // pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "serverstate --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //        }



    //        private void HPScript13_Click(object sender, RoutedEventArgs e)
    //        {
    //            ProcessPiper pp = new ProcessPiper(vm);

    //            //FOR ILO RESTFUL
    //            // ilo rest full path  C:\Program Files\Hewlett Packard Enterprise\RESTful Interface Tool\ilorest.exe  bootorder biosdefaults
    //            //pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "login 192.168.101.34 -u username -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //            pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "serverstatus --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //        }

    //        private void HPScript14_Click(object sender, RoutedEventArgs e)
    //        {
    //            ProcessPiper pp = new ProcessPiper(vm);

    //            //FOR ILO RESTFUL
    //            // ilo rest full path  C:\Program Files\Hewlett Packard Enterprise\RESTful Interface Tool\ilorest.exe  bootorder biosdefaults
    //            //pp.StartILORest(new System.IO.FileInfo(@"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "login 192.168.101.34 -u username -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //            pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "serverstatus --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //        }

    //        private void HPScript15_Click(object sender, RoutedEventArgs e)
    //        {
    //            //LOGINNAME: The account name, not used to login.
    //            //USERNAME: The account username name, used to login.
    //            //PASSWORD: The account password, used to login.
    //            //Id: The number associated with an iLO user account.
    //            //PRIVILEGES:

    //            //            1: Login
    //            //2: Remote Console
    //            //3: User Config
    //            //4: iLO Config
    //            //5: Virtual Media
    //            //6: Virtual Power and Reset

    //            //iLO 5 added privileges:
    //            //7: Host NIC Config
    //            //8: Host Bios Config
    //            //9: Host Storage Config
    //            //10: System Recovery Config
    //            //Roles:

    //            //Administrator
    //            //ReadOnly
    //            //Operator
    //            // Use with ilo 4 --addprivs 1,2,3,4,5,6
    //            // Use with ilo 5 --addprivs 1,2,3,4,5,6,7,8,9,10
    //            //pp.StartILORest(new System.IO.FileInfo(@"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "login 192.168.101.34 -u username -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");

    //            ProcessPiper pp = new ProcessPiper(vm);
    //            pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "iloaccounts add Administrator Administrator password --addprivs 1,2,3,4,5,6 --url " + ServIPV4 + " -u username -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //        }

    //        private void HPScript16_Click(object sender, RoutedEventArgs e)
    //        {
    //            //LOGINNAME: The account name, not used to login.
    //            //USERNAME: The account username name, used to login.
    //            //PASSWORD: The account password, used to login.
    //            //Id: The number associated with an iLO user account.
    //            //PRIVILEGES:

    //            //            1: Login
    //            //2: Remote Console
    //            //3: User Config
    //            //4: iLO Config
    //            //5: Virtual Media
    //            //6: Virtual Power and Reset

    //            //iLO 5 added privileges:
    //            //7: Host NIC Config
    //            //8: Host Bios Config
    //            //9: Host Storage Config
    //            //10: System Recovery Config
    //            //Roles:

    //            //Administrator
    //            //ReadOnly
    //            //Operator
    //            // Use with ilo 4 --addprivs 1,2,3,4,5,6
    //            // Use with ilo 5 --addprivs 1,2,3,4,5,6,7,8,9,10
    //            //pp.StartILORest(new System.IO.FileInfo(@"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "login 192.168.101.34 -u username -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"); --logout

    //            ProcessPiper pp = new ProcessPiper(vm);
    //            pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "iloaccounts delete Administrator --url " + ServIPV4 + " -u username -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //        }

    //        private void HPScript17_Click(object sender, RoutedEventArgs e)
    //        {
    //            ProcessPiper pp = new ProcessPiper(vm);

    //            //FOR ILO RESTFUL
    //            // ilo rest full path  C:\Program Files\Hewlett Packard Enterprise\RESTful Interface Tool\ilorest.exe  bootorder biosdefaults
    //            //pp.StartILORest(new System.IO.FileInfo(@"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "login 192.168.101.248 -u username -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //            pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "types --url " + ServIPV4 + " -u username -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //        }

    //        private void HPScript18_Click(object sender, RoutedEventArgs e)
    //        {
    //            ProcessPiper pp = new ProcessPiper(vm);

    //            //FOR ILO RESTFUL
    //            // ilo rest full path  C:\Program Files\Hewlett Packard Enterprise\RESTful Interface Tool\ilorest.exe  bootorder biosdefaults
    //            //pp.StartILORest(new System.IO.FileInfo(@"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "login 192.168.101.248 -u username -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //            pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", @"firmwareupdate E:\ilo4_273.bin --url " + ServIPV4 + " -u username -p " + vm.OverridePasswordS1.Trim() + "", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");

    //        }

    //        private async void SENDCMDBTN_Click(object sender, RoutedEventArgs e)
    //        {

    //           // ProcessPiper pp = new ProcessPiper(vm);
    //           // //MessageBox.Show(CMDTXTB.Text + " --url " + ServIPV4 + " -u username -p " + vm.OverridePasswordS1.Trim() + "");   --url 192.168.101.248 -u Administrator -p " + vm.OverridePasswordS1.Trim() + "
    //           // //FOR ILO RESTFUL
    //           // // ilo rest full path  C:\Program Files\Hewlett Packard Enterprise\RESTful Interface Tool\ilorest.exe  bootorder biosdefaults
    //           // //pp.StartILORest(new System.IO.FileInfo(@"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "login 192.168.101.34 -u username -p " + vm.OverridePasswordS1.Trim() + "", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");  --logout

    //           // //USE OPTION TO NAVIGATE TO OPTION THEN RUN COMMAND SUCH OS GET AND SET
    //           // //pp.StartILORest(new System.IO.FileInfo(@"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "" + CMDTXTB.Text + " --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + " --select computersystem", @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");

    //           // //pp.StartILORest(new System.IO.FileInfo(@"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "" + CMDTXTB.Text + " --logout --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + " --select " + RESTFULAREA, @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");
    //           // pp.StartILORest(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"),"", "" + CMDTXTB.Text + " --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + " ", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool");


    //           //// await System.Threading.Tasks.Task.Run(() => pp.StartILORest(new System.IO.FileInfo(@"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\ilorest.exe"), "", "" + CMDTXTB.Text + " --url " + ServIPV4 + " -u Administrator -p " + vm.OverridePasswordS1.Trim() + " --select" + RESTFULAREA, @"@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool"));

    //           // CMDTXTB.Text = "";
    //           // RESTFULCMD = "";

    //        }

    //        private void WEBBROWSE1_Navigating(object sender, NavigatingCancelEventArgs e)
    //        {

    //        }


    //        private async void WEBBROWSEBTN_Click(object sender, RoutedEventArgs e)
    //        {

    //            // ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
    //            // var client = new WebClient();
    //            //client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
    //            //var response = client.DownloadString("https://192.168.101.248/redfish/v1/Systems/1/FirmwareInventory");
    //            //var releases = JArray.Parse(response);

    //            //if(ServIPV4 != String.Empty)
    //            //{
    //            //    WEBBROWSE1.Navigate("https://" + ServIPV4);
    //            //    WEBBROWSE1.Focus();
    //            //    CMDTXTB.Focus();
    //            //    await PutTaskDelay(500);
    //            //    WEBBROWSE1.Focus();
    //            //    await PutTaskDelay(200);
    //            //    System.Windows.Forms.SendKeys.SendWait("{F5}");
    //            //    // await PutTaskDelay(500);

    //            //}
    //            var example1 = @"{
    //  ""@odata.context"": "" / redfish / v1 /$metadata#Systems/Members/1/FirmwareInventory$entity"",
    //  ""@odata.id"": ""/redfish/v1/Systems/1/FirmwareInventory/"",
    //  ""@odata.type"": ""#FwSwVersionInventory.1.2.0.FwSwVersionInventory"",
    //  ""Current"": {
    //                ""IntelligentProvisioning"": [
    //                  {
    //        ""Location"": ""System Board"",
    //                    ""Name"": ""Intelligent Provisioning"",
    //                    ""VersionString"": ""1.61.45""
    //      }
    //    ],
    //    ""Other"": [
    //      {
    //        ""Location"": ""Embedded"",
    //        ""Name"": ""HP Ethernet 1Gb 4-port 331FLR Adapter"",
    //        ""VersionString"": ""N/A""
    //      },
    //      {
    //        ""Location"": ""Embedded"",
    //        ""Name"": ""Smart Array P420i Controller"",
    //        ""VersionString"": ""6.68""
    //      },
    //      {
    //        ""Location"": ""PCI-E Slot 2"",
    //        ""Name"": ""Unknown"",
    //        ""VersionString"": ""N/A""
    //      },
    //      {
    //        ""Location"": ""PCI-E Slot 3"",
    //        ""Name"": ""Unknown"",
    //        ""VersionString"": ""N/A""
    //      }
    //    ],
    //    ""PlatformDefinitionTable"": [
    //      {
    //        ""Location"": ""System Board"",
    //        ""Name"": ""Intelligent Platform Abstraction Data"",
    //        ""VersionString"": ""2.10""
    //      }
    //    ],
    //    ""PowerManagementController"": [
    //      {
    //        ""Key"": ""00000000000000000000000000504d0c"",
    //        ""Location"": ""System Board"",
    //        ""Name"": ""Power Management Controller Firmware"",
    //        ""VersionString"": ""3.3""
    //      }
    //    ],
    //    ""PowerManagementControllerBootloader"": [
    //      {
    //        ""Location"": ""System Board"",
    //        ""Name"": ""Power Management Controller Firmware Bootloader"",
    //        ""VersionString"": ""2.7""
    //      }
    //    ],
    //    ""PowerSupplies"": [
    //      {
    //        ""Key"": ""058302000000000000000cf38db966ea"",
    //        ""Location"": ""Bay 1"",
    //        ""Name"": ""Power Supply"",
    //        ""VersionString"": ""2.01""
    //      },
    //      {
    //        ""Key"": ""058302000000000000000cf38db966ea"",
    //        ""Location"": ""Bay 2"",
    //        ""Name"": ""Power Supply"",
    //        ""VersionString"": ""2.01""
    //      }
    //    ],
    //    ""SASProgrammableLogicDevice"": [
    //      {
    //        ""Location"": ""System Board"",
    //        ""Name"": ""SAS Programmable Logic Device"",
    //        ""VersionString"": ""Version 0x0C""
    //      }
    //    ],
    //    ""SPSFirmwareVersionData"": [
    //      {
    //        ""Location"": ""System Board"",
    //        ""Name"": ""Server Platform Services (SPS) Firmware"",
    //        ""VersionString"": ""2.1.7.E7.4""
    //      }
    //    ],
    //    ""SystemBMC"": [
    //      {
    //        ""Key"": ""2932ecaecc69d843bd0e61dc3406f71b"",
    //        ""Location"": ""System Board"",
    //        ""Name"": ""iLO"",
    //        ""VersionString"": ""2.73 Feb 11 2020""
    //      }
    //    ],
    //    ""SystemProgrammableLogicDevice"": [
    //      {
    //        ""Key"": ""000000000000000000000000000000b3"",
    //        ""Location"": ""System Board"",
    //        ""Name"": ""System Programmable Logic Device"",
    //        ""VersionString"": ""Version 0x32""
    //      }
    //    ],
    //    ""SystemRomActive"": [
    //      {
    //        ""Key"": ""00000000000000000000000001503730"",
    //        ""Location"": ""System Board"",
    //        ""Name"": ""System ROM"",
    //        ""VersionString"": ""P70 05/24/2019""
    //      }
    //    ],
    //    ""SystemRomBackup"": [
    //      {
    //        ""Key"": ""00000000000000000000000001503730"",
    //        ""Location"": ""System Board"",
    //        ""Name"": ""Redundant System ROM"",
    //        ""VersionString"": ""P70 05/24/2019""
    //      }
    //    ],
    //    ""SystemRomBootblock"": [
    //      {
    //        ""Location"": ""System Board"",
    //        ""Name"": ""System ROM Bootblock"",
    //        ""VersionString"": ""03/05/2013""
    //      }
    //    ]
    //  },
    //  ""Id"": ""FirmwareInventory"",
    //  ""Name"": ""Firmware Version Inventory"",
    //  ""Type"": ""FwSwVersionInventory.1.2.0"",
    //  ""links"": {
    //    ""self"": {
    //      ""href"": ""/redfish/v1/Systems/1/FirmwareInventory/""
    //    }
    //  }
    //}";




    //            var JSONObj = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(example2);


    //            foreach (var item in JSONObj)
    //            {

    //               // MessageBox.Show(JSONObj["Name"]);
    //                MessageBox.Show(JSONObj["Location"]);
    //                MessageBox.Show(JSONObj["VersionString"]);
    //            }




    // get JSON result objects into a list
    // IList<JToken> results = googleSearch["Current"]["IntelligentProvisioning"].Children().ToList();
    //IList<JToken> results = googleSearch["Current"].Children().ToList();
    //IList<JToken> results = googleSearch["Current"].Children().ToList();
    //IList<JToken> results = googleSearch;

    // serialize JSON results into .NET objects
    //IList<SearchResult> searchResults = new List<SearchResult>();
    //foreach (JToken result in results)
    //{
    //    // JToken.ToObject is a helper method that uses JsonSerializer internally
    //    SearchResult searchResult = result.ToObject<SearchResult>();
    //    searchResults.Add(searchResult);
    //}

    //foreach(var item in searchResults)
    //{
    //    MessageBox.Show(item.Name + "\r" + item.Location + "\r" + item.VersionString);
    //}


    //            var example2 = @"{
    //  'Current': {
    //                'SPSFirmwareVersionData': [
    //                  {
    //        'Location': 'System Board',
    //                    'VersionString': '2.1.7.E7.4'
    //      }
    //    ],
    //    'SystemBMC': [
    //      {
    //        'Key': '2932ecaecc69d843bd0e61dc3406f71b',
    //        'Location': 'System Board',
    //        'VersionString': '2.73 Feb 11 2020'
    //      }
    //    ]

    //  }
    //}";
    //            JObject googleSearch = JObject.Parse(example1);

    //            JObject o = JObject.Parse(@"{
    //  'Stores': [
    //    'Lambton Quay',
    //    'Willis Street'
    //  ],
    //  'Manufacturers': [
    //    {
    //      'Name': 'Acme Co',
    //      'Products': [
    //        {
    //          'Name': 'Anvil',
    //          'Price': 50
    //        }
    //      ]
    //    },
    //    {
    //      'Name': 'Contoso',
    //      'Products': [
    //        {
    //          'Name': 'Elbow Grease',
    //          'Price': 99.95
    //        },
    //        {
    //          'Name': 'Headlight Fluid',
    //          'Price': 4
    //        }
    //      ]
    //    }
    //  ]
    //}");



    //JsonParser.ReadFirmwareJson(example1);




    // List<String> mylist =  new List<string>();
    // mylist.Add("" + googleSearch.SelectToken("Current.SystemBMC[0].Name".ToString() + googleSearch.SelectToken("Current.SystemBMC[0].Location").ToString()));
    // //mylist.Add("" + (string)googleSearch.SelectToken("Current.SystemBMC[0].Location"));

    //// +(string)googleSearch.SelectToken("Current.SystemBMC[0].Location")
    //// mylist.Add("" + (string)googleSearch.SelectToken("Current.SPSFirmwareVersionData[0].Name" + ": " + (string)googleSearch.SelectToken("Current.SPSFirmwareVersionData[0].VersionString")));
    ////  mylist.Add("" + (string)googleSearch.SelectToken("Current.SystemRomActive[0].Name" + ": " + (string)googleSearch.SelectToken("Current.SystemRomActive[0].VersionString")));



    // //string name = (string)googleSearch.SelectToken("Current.SystemBMC[0].Name");
    // //string location = (string)googleSearch.SelectToken("Current.SystemBMC[0].Location");
    // //string version = (string)googleSearch.SelectToken("Current.SPSFirmwareVersionData.VersionString");
    // //string location = (string)googleSearch.SelectToken("IntelligentProvisioning[0]Location");
    // //string version = (string)googleSearch.SelectToken("Manufacturers[0].VersionString"); SystemRomActive

    // foreach (var itm in mylist)
    // {
    //     MessageBox.Show(itm);
    // }


    //var grid = new Grid();

    //int timestamp = new TimeSpan(DateTime.Now.Ticks).Milliseconds;
    //const MouseButton mouseButton = MouseButton.Left;
    //var mouseDownEvent =
    //   new MouseButtonEventArgs(Mouse.PrimaryDevice, timestamp, mouseButton)
    //   {
    //       RoutedEvent = UIElement.MouseLeftButtonDownEvent,
    //       Source = WEBBROWSE1,
    //   };


    //WEBBROWSE1.Navigate("https://" + ServIPV4);
    //WEBBROWSE1.Focus();
    //CMDTXTB.Focus();
    //await PutTaskDelay(2000);
    ////WEBBROWSE1.Focus();
    //await PutTaskDelay(2000);
    //System.Windows.Forms.SendKeys.SendWait("\t\t\t");
    ////await PutTaskDelay(200);
    ////System.Windows.Forms.SendKeys.SendWait("{TAB}");
    ////await PutTaskDelay(200);
    ////System.Windows.Forms.SendKeys.SendWait("{TAB}");
    ////await PutTaskDelay(500);
    //System.Windows.Forms.SendKeys.SendWait("{ENTER}");
    //WEBBROWSE1.Focus();
    //CMDTXTB.Focus();
    //await PutTaskDelay(500);
    //WEBBROWSE1.Focus();
    //System.Windows.Forms.SendKeys.SendWait("{TAB}");
    //await PutTaskDelay(2000);
    //System.Windows.Forms.SendKeys.SendWait("{TAB}");
    //await PutTaskDelay(1000);
    //System.Windows.Forms.SendKeys.SendWait("{TAB}");
    ////await PutTaskDelay(1000);

    //}


    //public async void WEBBROWSE1_Navigated(object sender, NavigationEventArgs e)
    //{
    //    //WEBBROWSE1.Focus();
    //    //if(e.Content != null)
    //    //   {
    //    //       MessageBox.Show(e.WebResponse.Headers.ToString());
    //    //   }

    //    // CMDTXTB.Focus();
    //    // await PutTaskDelay(3000);
    //    // WEBBROWSE1.Focus();
    //    // await PutTaskDelay(3000);
    //    // System.Windows.Forms.SendKeys.SendWait("{F5}");
    //    // //WEBBROWSE1.Focus();
    //    ////await PutTaskDelay(2000);
    //    //// System.Windows.Forms.SendKeys.SendWait("{F5}");

    //    ////await PutTaskDelay(1000);
    //    //// System.Windows.Forms.SendKeys.SendWait("{F5}");
    //    //// WEBBROWSE1.Focus();
    //    // //CMDTXTB.Focus();
    //    // //await PutTaskDelay(1000);
    //    // //WEBBROWSE1.Focus();
    //    // //await PutTaskDelay(2000);
    //    // //WEBBROWSE1.Focus();
    //    // //System.Windows.Forms.SendKeys.SendWait("{TAB}");
    //    // //await PutTaskDelay(2000);
    //    // //System.Windows.Forms.SendKeys.SendWait("{TAB}");
    //    // //// WEBBROWSE1.Focus();
    //    // //System.Windows.Forms.SendKeys.SendWait("{ENTER}");
    //    // //await PutTaskDelay(1000);
    //    // ////WEBBROWSE1.Focus();
    //    // //System.Windows.Forms.SendKeys.SendWait("{TAB}");
    //    // //await PutTaskDelay(1000);
    //    // //System.Windows.Forms.SendKeys.SendWait("{TAB}");
    //    // //await PutTaskDelay(1000);
    //    // ////WEBBROWSE1.Focus();
    //    // //System.Windows.Forms.SendKeys.SendWait("{ENTER}");
    //    //System.Windows.Forms.SendKeys.SendWait("{ENTER}");
    //}

    //private async void WEBBROWSELAUNCHBTN_Click(object sender, RoutedEventArgs e)
    //{
    //    //CMDTXTB.Focus();
    //    //await PutTaskDelay(100);
    //    //WEBBROWSE1.Focus();
    //    //await PutTaskDelay(100);
    //    //System.Windows.Forms.SendKeys.SendWait("{F5}");
    //    //WEBBROWSE1.Focus();
    //    //await PutTaskDelay(1000);
    //    // System.Windows.Forms.SendKeys.SendWait("{F5}");
    //    //System.Windows.Forms.SendKeys.SendWait("\t\t");
    //    ////await PutTaskDelay(2000);
    //    ////System.Windows.Forms.SendKeys.SendWait("\t");
    //    ////// WEBBROWSE1.Focus();
    //    //System.Windows.Forms.SendKeys.SendWait("{ENTER}");
    //    //await PutTaskDelay(1000);
    //    ////WEBBROWSE1.Focus();
    //    //System.Windows.Forms.SendKeys.SendWait("{TAB}");
    //    //await PutTaskDelay(1000);
    //    //System.Windows.Forms.SendKeys.SendWait("{TAB}");
    //    //await PutTaskDelay(1000);
    //    ////WEBBROWSE1.Focus();
    //    //System.Windows.Forms.SendKeys.SendWait("{ENTER}");

    //}

    //private void RESTAREACB_SelectionChanged(object sender, SelectionChangedEventArgs e)
    //{
    //    //ComboBoxItem SelectedBrand = (ComboBoxItem)RESTAREACB.SelectedItem;

    //    //RESTFULAREA = SelectedBrand.Content.ToString();

    //    //MessageBox.Show(RESTFULAREA);
    //}

}
