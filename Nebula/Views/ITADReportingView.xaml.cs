﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SQLite;
//using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Xps.Packaging;
using System.Xml;
using Nebula.Commands;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.Paginators;
using Nebula.ViewModels;
using Nebula.Views;
using QRCoder;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for ITADReporting.xaml
    /// </summary>
    public partial class ITADReportingView : UserControl
    {
        GoodsInOutViewModel vm;
        //Connection to SQL Lite DB
        //if all criteria met create the job
        SQLiteConnection sqlite_conn;
        //SQL Lite DB LINK
        public string cs = "";
      

        public ITADReportingView()
        {
            InitializeComponent();

            //Point DB to Test Databases
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\ITAD_Reporting.db";
            }
            else
            {

                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/ITADReporting/ITAD_Reporting.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/ITADReporting/ITAD_Reporting.db";
                }
                else
                {

                    //Set connection string to Live DB
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\ITAD_Reporting.db";
                }
              
            }

        }
        public ITADReportingView(GoodsInOutViewModel PassedViewModel)
        {
            InitializeComponent();

            vm = PassedViewModel;
            this.DataContext = vm;

            //Point DB to Test Databases
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\ITAD_Reporting.db";
            }
            else
            {
                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/ITADReporting/ITAD_Reporting.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/ITADReporting/ITAD_Reporting.db";
                }
                else
                {

                    //Set connection string to Live DB
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\ITAD_Reporting.db";
                }
            }

          

           

           


            //Load Any Categories
            ReadCategory("SELECT * FROM DeviceCategories ORDER BY Category");


            //Load Any Processes
            ReadProcess("SELECT * FROM ProcessPerformed ORDER BY Process");

            //Load Any Processes
            ReadTestingProcess("SELECT * FROM TestingPerformed ORDER BY Process");

            //Load Any Verification
            ReadVerificationProcess("SELECT * FROM ErasurePerformed ORDER BY Process");

            //Set the date user property
            vm.ITADReportUserDate = "Technician Name: " + vm.FullUser + " " + DateTime.Now.ToShortDateString();

            //Set default value for verified by
            vm.ITADVerifiedBy = "D Benyon";


            ////Call initial data set async
            InitialData();

        }

        private async void InitialData()
        {
            //Read Data
           FilterDateSearch("", "");
        }



        private void SignBTN_Click(object sender, RoutedEventArgs e)
        {

            //Generate QR Code from Serial Number
            GenerateLabel();

            //Reset User Back To Current
            vm.ITADReportUserDate = "Technician Name: " + vm.FullUser + " " + DateTime.Now.ToShortDateString();

            if (string.IsNullOrWhiteSpace(vm.ITADPOSONo))
            {
                MessageBox.Show(App.Current.FindResource("ITADRepGBRequiredPOSONo").ToString(), App.Current.FindResource("ITADRepGBCreateRecord").ToString(), MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (string.IsNullOrWhiteSpace(vm.ITADAssetNo))
            {
                MessageBox.Show(App.Current.FindResource("ITADRepGBRequiredAssettNo").ToString(), App.Current.FindResource("ITADRepGBCreateRecord").ToString(), MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (string.IsNullOrWhiteSpace(vm.ITADSerialNo))
            {
                MessageBox.Show(App.Current.FindResource("ITADRepGBRequiredSerialNo").ToString(), App.Current.FindResource("ITADRepGBCreateRecord").ToString(), MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (string.IsNullOrWhiteSpace(vm.ITADDeviceCategory))
            {
                MessageBox.Show(App.Current.FindResource("ITADRepGBValidCategory").ToString(), App.Current.FindResource("ITADRepGBCreateRecord").ToString(), MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (string.IsNullOrWhiteSpace(vm.ITADProcessPerformed))
            {
                MessageBox.Show(App.Current.FindResource("ITADRepGBValidErasure").ToString(), App.Current.FindResource("ITADRepGBCreateRecord").ToString(), MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (string.IsNullOrWhiteSpace(vm.ITADTestingPerformed))
            {
                MessageBox.Show(App.Current.FindResource("ITADRepGBValidTesting").ToString(), App.Current.FindResource("ITADRepGBCreateRecord").ToString(), MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (string.IsNullOrWhiteSpace(vm.ITADVerificationPer))
            {
                MessageBox.Show(App.Current.FindResource("ITADRepGBValidVerification").ToString(), App.Current.FindResource("ITADRepGBCreateRecord").ToString(), MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {

                //Check if non data bearing device 

                if (ProcessPerCB.Text == "Non Data Bearing Device")
                {
                    //Set the Declaration properties.
                    vm.ITADDeclaration = App.Current.FindResource("ITADRepDeclarationText1").ToString() + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + App.Current.FindResource("ITADRepDeclarationText2").ToString();
                    //vm.ITADDeclaration2 = "Asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + " was sanitised using the following process (" + vm.ITADProcessPerformed.Replace("Process Performed: ", "") + @") in line with Company & Manufactures procedures.";
                    //Technician Confirmation
                    vm.ITADTechConfirm = vm.FullUser.Replace("Technician Name: ", "") + App.Current.FindResource("ITADRepTechnicianText1").ToString() + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + App.Current.FindResource("ITADRepDeclarationText2").ToString();
                    //Technician Confirmation
                    vm.ITADVerificationConfirm = App.Current.FindResource("ITADRepVerificationText1").ToString() + vm.ITADVerifiedBy + App.Current.FindResource("ITADRepVerificationText2").ToString() + vm.ITADVerificationPerformedSelItem.Process + ".";
                }
                else
                {
                    //Set the Declaration properties.
                    vm.ITADDeclaration = App.Current.FindResource("ITADRepDeclarationText1").ToString() + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + App.Current.FindResource("ITADRepDeclarationText3").ToString();
                    vm.ITADDeclaration2 = "Asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + App.Current.FindResource("ITADRepTechnicianText2").ToString() + vm.ITADProcessPerformedSelItem.Process.Replace("Process Performed: ", "") + App.Current.FindResource("ITADRepTechnicianText4").ToString();
                    //Technician Confirmation
                    vm.ITADTechConfirm = vm.FullUser.Replace("Technician Name: ", "") + App.Current.FindResource("ITADRepTechnicianText1").ToString() + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + App.Current.FindResource("ITADRepTechnicianText3").ToString();
                    //Technician Confirmation
                    vm.ITADVerificationConfirm = App.Current.FindResource("ITADRepVerificationText1").ToString() + vm.ITADVerifiedBy + App.Current.FindResource("ITADRepVerificationText2").ToString() + vm.ITADVerificationPerformedSelItem.Process + ".";
                }


                //Refresh Current User
                vm.CurrentUser = "Technician Name: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, 3).Replace(".", "").ToUpper();

                //Refresh full user
                vm.FullUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").ToUpperUserFullName();


                //ADD DB ENTRY HERE
                //CreateTable(sqlite_conn);
                ////insert all items in table to DB
                //InsertData(sqlite_conn);

                //Show the Flow Document Reader
                FDR.Visibility = Visibility.Visible;
                //Enable print button
                PrintBTN.IsEnabled = true;
                vm.ITADIsReprint = false;

               
            }


        }




        //Create Connection to DB
        static SQLiteConnection CreateConnection(string pathtodb)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            //sqlite_conn = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            //sqlite_conn = new SQLiteConnection("Data Source=" + pathtodb + "; Version = 3; New = True; Compress = True; ");
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return sqlite_conn;
        }



        public void InsertData()
        {
            try
            {

            // Create Connection
            sqlite_conn = CreateConnection(cs);

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.CommandText = "INSERT INTO ReportRecords (DateCreated, POSONumber, AssetNo, SerialNo, Manufacturer, ModelNo, DeviceCategory, ProcessPerformed, TestingPerformed, Notes, ReportBody, SavePath, TestedBy, VerifiedBy, VerificationPerformed)" + " VALUES(@datecreated,@posonumber,@assetno,@serialno,@manufacturer,@modelno,@devicecategory,@processperformed,@testingperformed,@notes,@reportbody,@savepath,@testedby,@verifiedby,@verificationperformed)";
            sqlite_cmd.CommandType = CommandType.Text;

            //LOOP THROUGH ADDED COLLECTION
            //foreach (NetsuiteInventory itm in vm.ScannedSerialCollection)
            //{

            //Date must be in this format to work with SQL Lite
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@datecreated", DateTime.Now.ToString("yyyy-MM-dd H:mm:ss")));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@posonumber", vm.ITADPOSONo));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@assetno", vm.ITADAssetNo));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", vm.ITADSerialNo));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", " "));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@modelno", " "));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@devicecategory", vm.ITADDeviceCategorySelItem.Category));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@processperformed", vm.ITADProcessPerformedSelItem.Process));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@testingperformed", vm.ITADTestingPerformedSelItem.Process));


            if (vm.ITADNotes == string.Empty)
            {
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@notes", " "));
            }
            else
            {
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@notes", vm.ITADNotes));
            }


            if (vm.ITADCLIOutput == string.Empty)
            {
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportbody", " "));
            }
            else
            {
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportbody", vm.ITADCLIOutput));
            }



            sqlite_cmd.Parameters.Add(new SQLiteParameter("@savepath", " "));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@testedby", vm.FullUser));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@verifiedby", vm.ITADVerifiedBy));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@verificationperformed", vm.ITADVerificationPerformedSelItem.Process));
            sqlite_cmd.ExecuteNonQuery();


            //Close DB connection
            sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        public void InsertCategory()
        {
            try
            {

            // Create Connection
            sqlite_conn = CreateConnection(cs);

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.CommandText = "INSERT INTO DeviceCategories (Manufacturer, Model, Category)" + " VALUES(@manufacturer,@model,@category)";
            sqlite_cmd.CommandType = CommandType.Text;


            //Date must be in this format to work with SQL Lite
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", " "));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@model", " "));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@category", vm.ITADAddCategory));
            sqlite_cmd.ExecuteNonQuery();

            //Close DB connection
            sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }


        public void ReadCategory(string sqlquery)
        {
            try
            {

                // Create Connection
                sqlite_conn = CreateConnection(cs);

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ITADDeviceCategories dc = new ITADDeviceCategories();

                    dc.Manufacturer = sqlite_datareader.GetString(1);
                    dc.Model = sqlite_datareader.GetString(2);
                    dc.Category = sqlite_datareader.GetString(3);



                    vm.ITADCategoryCollection.Add(dc);

                }

                //Close DB connection
                sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        public void InsertProcess()
        {
            try
            {

            // Create Connection
            sqlite_conn = CreateConnection(cs);

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.CommandText = "INSERT INTO ProcessPerformed (Process)" + " VALUES(@process)";
            sqlite_cmd.CommandType = CommandType.Text;


            //Date must be in this format to work with SQL Lite
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@process", vm.ITADAddProcess));
            sqlite_cmd.ExecuteNonQuery();
       
            //Close DB connection
            sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void ReadProcess(string sqlquery)
        {
            try
            {

                // Create Connection
                sqlite_conn = CreateConnection(cs);

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ITADProcessesPerformed pp = new ITADProcessesPerformed();

                    pp.Process = sqlite_datareader.GetString(1);


                    vm.ITADProcessesCollection.Add(pp);

                }

                //Close DB connection
                sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }




        public void InsertTestingProcess()
        {

            try
            {

            // Create Connection
            sqlite_conn = CreateConnection(cs);

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.CommandText = "INSERT INTO TestingPerformed (Process)" + " VALUES(@process)";
            sqlite_cmd.CommandType = CommandType.Text;


            //Date must be in this format to work with SQL Lite
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@process", vm.ITADAddTesting));
            sqlite_cmd.ExecuteNonQuery();


            //Close DB connection
            sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void ReadTestingProcess(string sqlquery)
        {
            try
            {
                // Create Connection
                sqlite_conn = CreateConnection(cs);

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ITADTestingPerformed pp = new ITADTestingPerformed();

                    pp.Process = sqlite_datareader.GetString(1);


                    vm.ITADTestingCollection.Add(pp);

                }

                //Close DB connection
                sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void ReadVerificationProcess(string sqlquery)
        {
            try
            {

                // Create Connection
                sqlite_conn = CreateConnection(cs);

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ITADVerificationPerformed pp = new ITADVerificationPerformed();

                    pp.Process = sqlite_datareader.GetString(1);


                    vm.ITADVerificationCollection.Add(pp);

                }

                //Close DB connection
                sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void InsertVerificationProcess()
        {

            try
            {

            // Create Connection
            sqlite_conn = CreateConnection(cs);

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.CommandText = "INSERT INTO ErasurePerformed (Process)" + " VALUES(@process)";
            sqlite_cmd.CommandType = CommandType.Text;


            //Date must be in this format to work with SQL Lite
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@process", vm.ITADAddVerification));
            sqlite_cmd.ExecuteNonQuery();

            //Close DB connection
            sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }



        public async void ReadData(string sqlquery)
        {

            //Console.WriteLine("Read Data");
            try
            {
                // Create Connection
                sqlite_conn = CreateConnection(cs);

                //Clear Collection
                vm.ITADReportCollection = new ObservableCollection<ITADReport>();
                vm.ITADReportCollection.Clear();



                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                ////Clear Collection need to invoke dispatcher
                //App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                //{
                //    vm.ITADReportCollection.Clear();
                //});

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ITADReport itm = new ITADReport();
                    itm.InternalID = sqlite_datareader.GetInt32(0);
                    if (sqlite_datareader[1].GetType() != typeof(DBNull))
                        if (sqlite_datareader[1].GetType() != typeof(DBNull))
                        {

                            string date = sqlite_datareader.GetString(1);
                            itm.DateCreated = Convert.ToDateTime(date);

                            //    itm.DateCreated = sqlite_datareader.GetDateTime(1);
                            // show the error message to the UI for invalid TO Date
                            // return;

                        }

                    if (sqlite_datareader[2].GetType() != typeof(DBNull))
                        itm.POSONumber = sqlite_datareader.GetString(2);
                    if (sqlite_datareader[3].GetType() != typeof(DBNull))
                        itm.AssetNo = sqlite_datareader.GetString(3);
                    if (sqlite_datareader[4].GetType() != typeof(DBNull))
                        itm.SerialNo = sqlite_datareader.GetString(4);
                    if (sqlite_datareader[5].GetType() != typeof(DBNull))
                        itm.Manufacturer = sqlite_datareader.GetString(5);
                    if (sqlite_datareader[6].GetType() != typeof(DBNull))
                        itm.ModelNo = sqlite_datareader.GetString(6);
                    if (sqlite_datareader[7].GetType() != typeof(DBNull))
                        itm.DeviceCategory = sqlite_datareader.GetString(7);
                    if (sqlite_datareader[8].GetType() != typeof(DBNull))
                        itm.ProcessPerformed = sqlite_datareader.GetString(8);
                    if (sqlite_datareader[9].GetType() != typeof(DBNull))
                        itm.TestingPerformed = sqlite_datareader.GetString(9);
                    if (sqlite_datareader[10].GetType() != typeof(DBNull))
                        itm.Notes = sqlite_datareader.GetString(10);
                    if (sqlite_datareader[11].GetType() != typeof(DBNull))
                        itm.ReportBody = sqlite_datareader.GetString(11);
                    if (sqlite_datareader[12].GetType() != typeof(DBNull))
                        itm.SavePath = sqlite_datareader.GetString(12);
                    if (sqlite_datareader[13].GetType() != typeof(DBNull))
                        itm.TestedBy = sqlite_datareader.GetString(13);
                    if (sqlite_datareader[14].GetType() != typeof(DBNull))
                        itm.VerifiedBy = sqlite_datareader.GetString(14);
                    if (sqlite_datareader[15].GetType() != typeof(DBNull))
                        itm.VerificationPerformed = sqlite_datareader.GetString(15);

                    //Add to Collection need to invoke dispatcher
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        vm.ITADReportCollection.Add(itm);

                    });


                }


                // If collection changes Count
                vm.RecordsTotal = vm.ITADReportCollection.Count.ToString();

                //Close DB connection
                sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        public void DeleteData(SQLiteConnection conn)
        {

            try
            {
            // Create Connection
            sqlite_conn = CreateConnection(cs);

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.CommandText = "DELETE FROM ReportRecords WHERE SerialNo = @serialno;";
            sqlite_cmd.CommandType = CommandType.Text;
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", vm.ITADSerialNo));

            sqlite_cmd.ExecuteNonQuery();

            //Close DB connection
            sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        private void DeleteCategoryBTN_Click(object sender, RoutedEventArgs e)
        {


            if (vm.ITADDeviceCategory != "")
            {
                try
                {
                // Create Connection
                sqlite_conn = CreateConnection(cs);

                SQLiteCommand sqlite_cmd;
                //SQLiteConnection conn = new SQLiteConnection();
                //conn.Open();
                sqlite_cmd = sqlite_conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "DELETE FROM DeviceCategories WHERE Category = @category;";
                sqlite_cmd.CommandType = CommandType.Text;
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@category", vm.ITADDeviceCategory));

                sqlite_cmd.ExecuteNonQuery();

                //Re-Read and reset ComboBoxes
                ReadAndResetComboCategories();

                //Close DB connection
                sqlite_conn.Close();

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void DeleteErasureBTN_Click(object sender, RoutedEventArgs e)
        {
            if (vm.ITADProcessPerformed != "")
            {
                try
                {
                 // Create Connection
                 sqlite_conn = CreateConnection(cs);

                SQLiteCommand sqlite_cmd;
                SQLiteConnection conn = new SQLiteConnection();

                sqlite_cmd = sqlite_conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "DELETE FROM ProcessPerformed WHERE Process = @process;";
                sqlite_cmd.CommandType = CommandType.Text;
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@process", vm.ITADProcessPerformed));

                sqlite_cmd.ExecuteNonQuery();

                //Re-Read and reset ComboBoxes
                ReadAndResetComboCategories();

                //Close DB connection
                sqlite_conn.Close();

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }



        private void DeleteTestingBTN_Click(object sender, RoutedEventArgs e)
        {
            if (vm.ITADTestingPerformed != "")
            {

                try
                {
                // Create Connection
                sqlite_conn = CreateConnection(cs);

                SQLiteCommand sqlite_cmd;
                SQLiteConnection conn = new SQLiteConnection();

                sqlite_cmd = sqlite_conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "DELETE FROM TestingPerformed WHERE Process = @process;";
                sqlite_cmd.CommandType = CommandType.Text;
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@process", vm.ITADTestingPerformed));

                sqlite_cmd.ExecuteNonQuery();

                //Re-Read and reset ComboBoxes
                ReadAndResetComboCategories();

                //Close DB connection
                sqlite_conn.Close();

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }

        }


        private void DeleteVerificationBTN_Click(object sender, RoutedEventArgs e)
        {
            if (vm.ITADVerificationPer != "")
            {

                try
                {
                // Create Connection
                sqlite_conn = CreateConnection(cs);

                SQLiteCommand sqlite_cmd;
                SQLiteConnection conn = new SQLiteConnection();

                sqlite_cmd = sqlite_conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "DELETE FROM ErasurePerformed WHERE Process = @process;";
                sqlite_cmd.CommandType = CommandType.Text;
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@process", vm.ITADVerificationPer));

                sqlite_cmd.ExecuteNonQuery();

                //Re-Read and reset ComboBoxes
                ReadAndResetComboCategories();

               //Close DB connection
               sqlite_conn.Close();

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }

        }



        private void ReadAndResetComboCategories()
        {
            vm.ITADCategoryCollection.Clear();
            vm.ITADProcessesCollection.Clear();
            vm.ITADTestingCollection.Clear();
            vm.ITADVerificationCollection.Clear();

            //Load Any Categories
            ReadCategory("SELECT * FROM DeviceCategories ORDER BY Category");


            //Load Any Processes
            ReadProcess("SELECT * FROM ProcessPerformed ORDER BY Process");

            //Load Any Processes
            ReadTestingProcess("SELECT * FROM TestingPerformed ORDER BY Process");

            //Load Any Verification
            ReadVerificationProcess("SELECT * FROM ErasurePerformed ORDER BY Process");

            DeleteDeviceCatCB.SelectedIndex = -1;
            DeleteErasureCB.SelectedIndex = -1;
            DeleteTestingCB.SelectedIndex = -1;
            DeleteVerificationCB.SelectedIndex = -1;

            DeviceCatCB.SelectedIndex = -1;
            ProcessPerCB.SelectedIndex = -1;
            TestingProCB.SelectedIndex = -1;
            VerificationCB.SelectedIndex = -1;
        }



        private async void FilterSearch(string searchFilter, string whichColumn)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.AddDays(1).ToString("yyyy-MM-dd");


            //Search both columns await Task.Run(() => 
            await Task.Run(() => ReadData("SELECT * FROM ReportRecords WHERE AssetNo LIKE '" + searchFilter + "' || '%' OR SerialNo LIKE '" + searchFilter + "'  || '%' OR POSONumber LIKE '" + searchFilter + "'  || '%' AND DateCreated BETWEEN '" + dateFrom + "' AND '" + dateTo + "'"));


            Mouse.OverrideCursor = null;
        }



        private async void FilterDateSearch(string searchFilter, string whichColumn)
        {
            Mouse.OverrideCursor = Cursors.Wait;

        
            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.AddDays(1).ToString("yyyy-MM-dd");

            await Task.Run(() => ReadData("SELECT * FROM ReportRecords WHERE DateCreated BETWEEN '" + dateFrom + "' AND '" + dateTo + "'"));

           
            Mouse.OverrideCursor = null;
        }





        private void PrintBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Clear the message text box
                ReportMSGTB.Text = "";
                //Show progress
                ProgressBar.IsIndeterminate = true;

                //Regenerate QR Label if unexpected changes added by user
                GenerateLabel();


                string savePath = "";

                if (Directory.Exists(@"M:\Technical Test Reports\Nebula Test Reports"))
                {
                    //Update Message
                    ReportMSGTB.Foreground = Brushes.Green;
                    ReportMSGTB.Text = "File Saved to >>>";
                    //Save to ITAD M Drive
                    savePath = @"M:\Technical Test Reports\Nebula Test Reports\";
                }
                else if (Directory.Exists(@"W:\FR Warehouse\ITAD\Test Reports\"))
                {
                    //Update Message
                    ReportMSGTB.Foreground = Brushes.Green;
                    ReportMSGTB.Text = "File Saved to >>>";
                    //Save to ITAD M Drive
                    savePath = @"W:\FR Warehouse\ITAD\Test Reports\";
                }
                else
                {
                    //Update Message
                    ReportMSGTB.Foreground = Brushes.Red;
                    ReportMSGTB.Text = "Network unavailable, File saved locally >>>";

                    //Save to Users Documents
                    savePath = @"" + vm.myDocs + @"\";

                }

                //Set string to combination name
                string fileURI = vm.ITADPOSONo.Replace("PO/SO No: ", "") + " - " + vm.ITADAssetNo.Replace("Asset No: ", "") + " - " + vm.ITADSerialNo.Replace("Serial No: ", "") + " - " + vm.ITADProcessPerformedSelItem.Process + " - " + vm.CurrentUserInitials.Trim();



                DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

                ////Set the serial number to text on the clipboard  Use control v
                if (!string.IsNullOrEmpty(vm.ITADSerialNo) || !string.IsNullOrEmpty(vm.ITADSerialNo) || !string.IsNullOrEmpty(vm.ITADSerialNo))
                    Clipboard.SetText(fileURI, TextDataFormat.UnicodeText);

                //Add processed item to List
                vm.ITADAssetsProcessed.Add(vm.ITADAssetNo.Replace("Asset No: ", "") + "_" + DateTime.Now.ToShortDateString() + "_" + DateTime.Now.ToShortTimeString());
                // ADD PRINT TO PDF AND COPYTING OF ANY ADDITIONAL REPORTS
                //Original
                //dpw.SaveDocAndPrintITAD(Report1);
                //Create the PDF from the Flow Document or Fixed Document
                dpw.FlowDocument2Pdf(Report1, savePath + fileURI + ".pdf");

                //Add list of string to hold additional reports
                List<string> combinePDF = new List<string>();


                //Add first flowdoc to list
                combinePDF.Add(savePath + fileURI + ".pdf");
               

                //Save Any Additional Attatchments to the same file path
                int numAtt = 1;

                foreach (string report in vm.ITADAttachedFiles)
                {
                    numAtt += 1;

                 
                    if (File.Exists(report))
                    {
                        //Get File Extension
                        string ext = System.IO.Path.GetExtension(report);

                    
                        if (ext != ".pdf")
                        {

                            //image file
                            // Convert to PDF and delete image
                            PDFHelper.Instance.SaveImageAsPdf(report, savePath + fileURI + numAtt.ToString() + ".pdf", 600, false);
                            combinePDF.Add(savePath + fileURI + numAtt.ToString() + ".pdf");
                        }
                        else if (ext == ".pdf")
                        {

                            if (File.Exists(savePath + fileURI + numAtt.ToString() + ".pdf"))
                            {
                                File.Delete(savePath + fileURI + numAtt.ToString() + ".pdf");

                                //If PDF Copy to save location
                                File.Copy(report, savePath + fileURI + numAtt.ToString() + ".pdf");
                            }
                            else
                            {
                                //If PDF Copy to save location
                                File.Copy(report, savePath + fileURI + numAtt.ToString() + ".pdf");
                            }

                            
                        
                      
                           
                            //combinePDF.Add(report);
                            combinePDF.Add(savePath + fileURI + numAtt.ToString() + ".pdf");
                        }


                        //Launch File
                        //System.Diagnostics.Process.Start(savePath + fileURI + numAtt.ToString() + ext.Trim());
                    }

                }


                //Combine all pdf's in list
                PDFHelper.CombinePDF(combinePDF, savePath + fileURI + ".pdf");


                //Delete temp files

                foreach (var tfile in combinePDF)
                {

                    if (tfile.Contains(savePath + fileURI + ".pdf"))
                    {
                        //skip first file
                    }
                    else
                    {
                        //delete file
                        if (File.Exists(tfile))
                        {
                            //delete temp file
                            File.Delete(tfile);
                           

                        }
                    }
                }



                //Launch Combined PDF File
                System.Diagnostics.Process.Start(savePath + fileURI + ".pdf");

                //insert all items in table to DB
                InsertData();



                //Hide progress
                ProgressBar.IsIndeterminate = false;



                //Remove Temp QR Code Image
                if (File.Exists(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.ITADSerialNo.Trim() + ".png"))
                {
                    File.Delete(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.ITADSerialNo.Trim() + ".png");
                }


                //Check if reprint or not. Property resetting based off this
                if (vm.ITADIsReprint == true)
                {
                    //Update Message
                    ReportMSGTB.Text += savePath + fileURI + ".pdf";
                  
                    //RePrint
                    vm.ITADReportUserDate = "Technician Name: " + vm.FullUser + " " + DateTime.Now.ToShortDateString();
                    // Reset certain fields
                    AssetNoTXT.Text = "";
                    SerialNoTXT.Text = "";
                    AssetNoTXT.Text = "";
                    //Reset the ComboBoxes
                    //DeviceCatCB.SelectedIndex = -1;
                    //ProcessPerCB.SelectedIndex = -1;
                    //TestingProCB.SelectedIndex = -1;
                    //VerificationCB.SelectedIndex = -1;
                    //Reset the ComboBoxes
                    DeleteDeviceCatCB.SelectedIndex = -1;
                    DeleteErasureCB.SelectedIndex = -1;
                    DeleteTestingCB.SelectedIndex = -1;
                    DeleteVerificationCB.SelectedIndex = -1;
                    vm.ITADAttachedFiles.Clear();
                    //Reset VM Properties
                    vm.ITADPOSONo = "";
                    vm.ITADSerialNo = "";
                    vm.ITADAssetNo = "";
                    vm.ITADNotes = "";
                    vm.ITADCLIOutput = "";
                    vm.ITADDeclaration = "";
                    vm.ITADDeclaration2 = "";
                    vm.ITADTechConfirm = "";
                    vm.ITADVerification = "";
                    vm.ITADVerificationConfirm = "";
                    vm.ITADVerifiedBy = "D Benyon";
                    vm.ITADQRCode = null;
                    //Disable print button
                    PrintBTN.IsEnabled = false;

                }
                else
                {
                    //Update Message
                    ReportMSGTB.Text += savePath + fileURI + ".pdf";
                    //Not a Reprint
                    vm.ITADReportUserDate = "Technician Name: " + vm.FullUser + " " + DateTime.Now.ToShortDateString();
                    // Reset certain fields
                    SerialNoTXT.Text = "";
                    AssetNoTXT.Text = "";

                    vm.ITADAttachedFiles.Clear();

                    //DeleteDeviceCatCB.SelectedIndex = -1;
                    //DeleteErasureCB.SelectedIndex = -1;
                    //DeleteTestingCB.SelectedIndex = -1;
                    //DeleteVerificationCB.SelectedIndex = -1;

                    //Reset VM Properties
                    vm.ITADSerialNo = "";
                    vm.ITADAssetNo = "";
                    vm.ITADNotes = "";
                    vm.ITADCLIOutput = "";
                    vm.ITADDeclaration = "";
                    vm.ITADDeclaration2 = "";
                    vm.ITADTechConfirm = "";
                    vm.ITADVerification = "";
                    vm.ITADVerificationConfirm = "";
                    vm.ITADVerifiedBy = "D Benyon";
                    vm.ITADQRCode = null;
                    //Disable print button
                    PrintBTN.IsEnabled = false;


                }

                //Show progress
                ProgressBar.IsIndeterminate = false;

           



            }
            catch (Exception ex)
            {
                //Show progress
                ProgressBar.IsIndeterminate = false;
                //Update Message
                ReportMSGTB.Text = ex.Message;
                Console.WriteLine(ex.Message);
            }

        }


        // BEST METHOD TO USE

        private String filterContent(String content)
        {
            return content.Replace("[^\\u0009\\u000a\\u000d\\u0000-\\uD7FF\\uE000-\\uFFFD]", "");
        }




        public DocumentPaginator PrintFlowDoc(FlowDocument FlowDoc)
        {
            try
            {



                //Clone the source document
                var str = XamlWriter.Save(FlowDoc);
                str.Replace("\u0000", "");
                str.Replace("\u0001", "");
                str.Replace("\u0002", "");
                //remove any invalid chars before passing
                var stringReader = new System.IO.StringReader(filterContent(str));
                var xmlReader = XmlReader.Create(stringReader);
                var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

                // get information about the dimensions of the seleted printer+media.
                System.Printing.PrintDocumentImageableArea ia = null;

                // passes in the printqueue CreateXPSDocumentWriter that calls upon a standar print dialog 
                System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);


                if (docWriter != null && ia != null)
                {
                    DocumentPaginator paginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;

                    // Change the PageSize and PagePadding for the document to match the CanvasSize for the printer device.
                    paginator.PageSize = new System.Windows.Size(ia.MediaSizeWidth, ia.MediaSizeHeight);
                    Thickness t = new Thickness(72);  // copy.PagePadding;
                    CloneDoc.PagePadding = new Thickness(
                                     Math.Max(ia.OriginWidth, t.Left),
                                       Math.Max(ia.OriginHeight, t.Top),
                                       Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
                                       Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));



                    CloneDoc.ColumnWidth = double.PositiveInfinity;

                    return paginator;

                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);

            }
            return null;
        }



        private void ToggleSwitch_Checked(object sender, RoutedEventArgs e)
        {
            if (LogoHeadBRD.Visibility == Visibility.Collapsed)
            {
                //Display The Logo
                LogoHeadBRD.Visibility = Visibility.Visible;
                FooterTB.Visibility = Visibility.Visible;
            }
        }

        private void ToggleSwitch_Unchecked(object sender, RoutedEventArgs e)
        {
            if (LogoHeadBRD.Visibility == Visibility.Visible)
            {
                //Display The Logo
                LogoHeadBRD.Visibility = Visibility.Collapsed;
                FooterTB.Visibility = Visibility.Collapsed;
            }
        }

        //Bool to detect if enter was pressed by user
        public bool enterPressed;

        private void CLITXT_TextChanged(object sender, TextChangedEventArgs e)
        {

            if (CLITXT.Text != string.Empty && e.Changes.FirstOrDefault().AddedLength > 1)
            {

                if (enterPressed == true)
                {
                    //Skip Formatter
                    //Run formatter
                    //CLITXT.Text = CleanReportFormatting(CLITXT.Text);
                    ExtractSerialNumber(CLITXT.Text);

                }
                else
                {
                    //Run formatter
                    CLITXT.Text = CleanReportFormatting(CLITXT.Text);
                    ExtractSerialNumber(CLITXT.Text);
                    //reset to false
                    enterPressed = false;
                }
            }
        }

        //if enter pressed set to true
        private void CLITXT_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                enterPressed = true;
            }

        }

        private void CLITXT_LostFocus(object sender, RoutedEventArgs e)
        {
            //Check for CISCO ILET ERROR
            if (CLITXT.Text.Contains("%ILET-1-AUTHENTICATION_FAIL") || CLITXT.Text.Contains("ILET-1-AUTHENTICATION_FAIL"))
            {
                //Set Text to show error
                CLITXT.Text = "Cisco %ILET Error detected! Please check the devices authenticity!";
                //Launch Dialog
                vm.DialogMessage("Error detected in CLI Output", "ILET Error DETECTED, Please check device authenticity!");
               

            }
        }



        public void ExtractSerialNumber(string report)
        {
            try
            {


                //check product brand is populated


                //for Cisco Capture the Serial Number from the CLI 


                if (report.Contains("Processor board ID"))
                {
                    var myint = report.IndexOf(" ID ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());

                    string serialExtract = report.Substring(myint, 16).Replace(",", "").Replace("ID", "").Trim();
                    myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;

                }

                if (report.Contains("Top Assembly Serial Number"))
                {
                    var myint = report.IndexOf("Top Assembly Serial Number           :", 0, StringComparison.OrdinalIgnoreCase);

                    // MessageBox.Show(myint.ToString());
                    string serialExtract = report.Substring(myint + 30, 22).Replace(",", "").Replace(":", "").Trim();
                    myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;

                }


                if (report.Contains("FC (1 Slot) Chassis"))
                {
                    var myint = report.IndexOf("SN: ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());
                    string serialExtract = report.Substring(myint, 16).Replace(",", "").Replace(":", "").Trim();
                    myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;

                }






                //for Mellanox Capture the Serial Number from the CLI 
                if (report.Contains("CHASSIS"))
                {
                    var myint = report.IndexOf("-", 0, StringComparison.OrdinalIgnoreCase) + 4;

                    //MessageBox.Show(myint.ToString());

                    string serialExtract = report.Substring(myint, 21).Replace(",", "").Trim();
                    //myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;

                }







                //for Cisco Capture the Serial Number from the CLI 
                if (report.Contains("Serial Number") && report.Contains("Cisco"))
                {
                    var myint = report.IndexOf(" Serial Number ", 0, StringComparison.OrdinalIgnoreCase);
                    string serialExtract = "";

                    serialExtract = report.Substring(myint, 34).Replace(":", "").Trim();

                    int startPos = serialExtract.IndexOf("Serial Number ");
                    int length = serialExtract.LastIndexOf("\n");


                    serialExtract = serialExtract.Substring(startPos, length).Replace("Serial Number ", "").Replace(":", "").Trim();


                    vm.TestSerialNo = serialExtract;


                    return;

                }


                //for HP Capture the Serial Number from the CLI  && report.Contains("HP-") || report.Contains("ProCurve")
                if (report.Contains("Serial Number") && report.Contains("HP-") || report.Contains("ProCurve"))
                {
                    var myint = report.IndexOf(" Serial Number ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());// Serial Number      : CN95FP90L4

                    string serialExtract = report.Substring(myint, 29).Replace("Serial Number", "").Replace(":", "").Trim();
                    //MessageBox.Show(myint.ToString());
                    myint = serialExtract.IndexOf(":", 0);
                    vm.TestSerialNo = serialExtract;

                    return;

                }



                if (report.Contains("DEVICE_SERIAL_NUMBER"))
                {
                    var myint = report.IndexOf("DEVICE_SERIAL_NUMBER", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());

                    string serialExtract = report.Substring(myint, 34).Replace("DEVICE_SERIAL_NUMBER", "").Replace(":", "").Trim();
                    //MessageBox.Show(myint.ToString());
                    myint = serialExtract.IndexOf(":", 0);
                    if (serialExtract.Contains("MAC_ADD"))
                    {

                        //PULL IN THE SERIAL NUMBER INSTEAD
                        serialExtract = report.Substring(myint, 34).Replace("Serial Number", "").Replace(":", "").Trim();
                        //MessageBox.Show(myint.ToString());
                        myint = serialExtract.IndexOf(":", 0);
                        vm.TestSerialNo = serialExtract;
                    }
                    else
                    {
                        vm.TestSerialNo = serialExtract;
                    }
                    return;
                }
                // Console.WriteLine("HP");





                // Console.WriteLine("Dell");
                if (report.Contains("Service Tag:"))
                {
                    var myint = report.IndexOf("Service Tag:", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());Serial number     Description

                    string serialExtract = report.Substring(myint, 20).Replace("Service Tag:", "").Trim();
                    //myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;

                    return;

                }

                // Console.WriteLine("netgear");
                if (report.Contains("Serial number") && report.Contains("Juniper"))
                {
                    var myint = report.IndexOf(" Serial number ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());Serial number     Description

                    string serialExtract = report.Substring(myint, 89).Replace(",", "").Replace("Chassis", "").Replace("Serial number", "").Replace("Description", "").Trim();
                    //myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;
                    return;
                }

                // Console.WriteLine("netgear");
                if (report.Contains("Serial number") && report.Contains("Netgear"))
                {
                    var myint = report.IndexOf(" Serial number ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());Serial number     Description

                    string serialExtract = report.Substring(myint, 89).Replace(",", "").Replace("Chassis", "").Replace("Serial number", "").Replace("Description", "").Trim();
                    //myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;
                    return;
                }

                // Console.WriteLine("netgear");
                if (report.Contains("Serial number") && report.Contains("IBM"))
                {
                    var myint = report.IndexOf(" Serial number ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());Serial number     Description

                    string serialExtract = report.Substring(myint, 89).Replace(",", "").Replace("Chassis", "").Replace("Serial number", "").Replace("Description", "").Trim();
                    //myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;
                    return;
                }

                // Console.WriteLine("netgear");
                if (report.Contains("Serial number") && report.Contains("Draytek"))
                {
                    var myint = report.IndexOf(" Serial number ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());Serial number     Description

                    string serialExtract = report.Substring(myint, 89).Replace(",", "").Replace("Chassis", "").Replace("Serial number", "").Replace("Description", "").Trim();
                    //myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;
                    return;
                }





            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);

            }
        }

        public string CleanReportFormatting(string report)
        {
            //try
            //{


            //STRING BUILDER READS THROUGH REPORT STRING AND CLEANS UP SEPARATING ONTO SEPARATE LINES
            StringBuilder sb1 = new StringBuilder();

            using (StringReader reader = new StringReader(report))
            {
                //int i = 1; 

                string line;
                while ((line = reader.ReadLine()) != null)
                {


                    if (line == String.Empty || line == "" || line.EndsWith(">") || line.EndsWith("#"))
                    {

                    }
                    else
                    {
                        //workaround for cisco reports
                        if (line.Contains("Sig#"))
                        {
                            sb1.AppendLine(line.Trim());
                        }
                        else if (line.Contains("#") && line.EndsWith("#") != true)
                        {
                            // MessageBox.Show("Cert Not Here");
                            sb1.AppendLine("\r\r" + line.Trim() + "\r");

                        }
                        else
                        {
                            sb1.AppendLine(line.Trim());
                        }

                    }


                }
            }
            return sb1.ToString();
            //}
            //catch (Exception ex)
            //{
            //    return sb1.ToString();
            //    //Console.WriteLine(ex.Message);
            //}
        }

        private void AddReportsBTN_Click(object sender, RoutedEventArgs e)
        {
            //Clear collection
            vm.ITADAttachedFiles.Clear();

            using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
            {

                //"All files (*.*)|*.*";
                openFileDialog.Filter = "PDF & Image Files|*.pdf;*.jpg;*.jpeg;*.png;*.gif;*.tif;...";


                openFileDialog.FilterIndex = 0;
                openFileDialog.Multiselect = true;
                openFileDialog.RestoreDirectory = true;


                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {

                    // Loop through how ever many items are selected and add to the list
                    foreach (var file in openFileDialog.FileNames)
                    {
                        if (File.Exists(file))
                        {

                            //int lastSlash = file.LastIndexOf(@"\");

                            vm.ITADAttachedFiles.Add(file); //file.Substring(lastSlash + 1);

                        }
                    }

                }
            }

        }

        private void AddProcessBTN_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(vm.ITADAddProcess) || vm.ITADAddProcess == "")
            {
                MessageBox.Show("Please enter a value in the Erasure Process Text Box.");
            }
            else
            {
                //insert all items in table to DB
                InsertProcess();
                vm.ITADProcessesCollection.Clear();
                //Load Any Processes
                ReadProcess("SELECT * FROM ProcessPerformed ORDER BY Process");

                vm.ITADAddProcess = "";

            }
        }

        private void AddTestingBTN_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(vm.ITADAddTesting) || vm.ITADAddTesting == "")
            {
                MessageBox.Show("Please enter a value in the Testing Process Text Box.");
            }
            else
            {
                //insert all items in table to DB
                InsertTestingProcess();
                vm.ITADTestingCollection.Clear();
                //Load Any Processes
                ReadTestingProcess("SELECT * FROM TestingPerformed ORDER BY Process");

                vm.ITADAddTesting = "";

            }
        }

        private void AddCategoryBTN_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(vm.ITADAddCategory) || vm.ITADAddCategory == "")
            {
                MessageBox.Show("Please enter a value in the Category Text Box.");
            }
            else
            {
                //insert all items in table to DB
                InsertCategory();
                vm.ITADCategoryCollection.Clear();
                //Load Any Categories
                ReadCategory("SELECT * FROM DeviceCategories ORDER BY Category");

                vm.ITADAddCategory = "";

            }

        }

        private void AddVerificationBTN_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(vm.ITADAddVerification) || vm.ITADAddVerification == "")
            {
                MessageBox.Show("Please enter a value in the Verification Process Text Box.");
            }
            else
            {
                //insert all items in table to DB
                InsertVerificationProcess();
                vm.ITADVerificationCollection.Clear();
                //Load Any Processes
                ReadVerificationProcess("SELECT * FROM ErasurePerformed ORDER BY Process");

                vm.ITADAddVerification = "";

            }
        }




        private void FindBTNS_Click(object sender, RoutedEventArgs e)
        {
          


            

            //IF SEARCH FIELD FULL USE NORMAL FILTER SEARCH, ELSE USE THE DATE RANGE SELECTED
            if (SearchTXTB.Text != string.Empty)
            {
               
                //Assign control text to viewmodel property
                vm.ReportingSearchText = SearchTXTB.Text;
                //Feed in view model search text
                FilterSearch(vm.ReportingSearchText, "");
               

            }
            else
            {
               
            FilterDateSearch("", "");
              
            }


           


        }

        private void SearchTXTB_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void SearchTXTB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                FilterSearch(SearchTXTB.Text, "");
            }
        }

        private void GenerateBTN_Click(object sender, RoutedEventArgs e)
        {
            if (ReportLV.SelectedIndex != -1)
            {

               
                vm.ITADPOSONo = vm.ITADReportSelectedItem.POSONumber;
                vm.ITADAssetNo = vm.ITADReportSelectedItem.AssetNo;
                vm.ITADDeviceCategory = vm.ITADReportSelectedItem.DeviceCategory;
                vm.ITADProcessPerformed = vm.ITADReportSelectedItem.ProcessPerformed;
                vm.ITADTestingPerformed = vm.ITADReportSelectedItem.TestingPerformed;
                vm.ITADNotes = vm.ITADReportSelectedItem.Notes;
                vm.ITADCLIOutput = vm.ITADReportSelectedItem.ReportBody;
                vm.ITADSerialNo = vm.ITADReportSelectedItem.SerialNo;
                vm.ITADReportUserDate = "Technician Name: " + vm.ITADReportSelectedItem.TestedBy.Replace("Checked By:","").Trim().Replace("Technician Name:", "").Trim() + " " + vm.ITADReportSelectedItem.DateCreated.ToShortDateString();
                //Set the Declaration properties.
                vm.ITADDeclaration = "This report serves as confirmation that the asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + @" has be sanitised of all user data in accordance to GDPR using Company/Manufactures procedures and guidelines for sanitisation.";
                vm.ITADDeclaration2 = "Asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + " was sanitised using the following process (" + vm.ITADProcessPerformed.Replace("Process Performed: ", "") + @") in line with Company & Manufactures procedures.";
                //Technician Confirmation
                vm.ITADTechConfirm = vm.ITADReportSelectedItem.TestedBy.Replace("Checked By:", "").Replace("Technician Name:", "").Trim() + " hereby confirms that Asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + @" has been sanitised of all user data and is fit for reuse.";
               

                //Check if selected item contains Validation values
                if (string.IsNullOrWhiteSpace(vm.ITADReportSelectedItem.VerifiedBy) && string.IsNullOrWhiteSpace(vm.ITADReportSelectedItem.VerificationPerformed))
                {
                    //Technician Confirmation
                    vm.ITADVerificationConfirm = "Sanitisation of this asset was verified by " + vm.ITADVerifiedBy + ".";
                }
                else
                {
                    //Technician Confirmation
                    vm.ITADVerificationConfirm = "Sanitisation of this asset was verified by " + vm.ITADReportSelectedItem.VerifiedBy + ", using the verification process " + vm.ITADReportSelectedItem.VerificationPerformed + ".";
                }

                //Generate QR
                //QRCODE Dimensions
                ITADStandardLabelZebraImage.Width = 80;
                ITADStandardLabelZebraImage.Height = 80;


                // Create QR CODE with Serial No from pasted list
                vm.ITADQRCode = CreateQRCode(vm.ITADSerialNo.Trim());

                //*** Checklist QR CODE ***
                //Call Embed QR Saves & extracts QR Code to File. This is required for the cloning of the flowdocument as it requires a full path
                vm.ITADQRCode = vm.EmbedChecklistQR(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.ITADSerialNo.Trim() + ".png", vm.ITADQRCode);



                //Enable Print Button Again
                PrintBTN.IsEnabled = true;

                //set reprint flag to true
                vm.ITADIsReprint = true;

            }


        }

        private void ExportCSVBTN_Click(object sender, RoutedEventArgs e)
        {
            List<ITADReport> reportList = new List<ITADReport>();

            if (ReportLV.Items.Count > 0)
            {
                foreach (ITADReport ns in ReportLV.Items)
                {
                    //Add to local list
                    reportList.Add(ns);
                }

            }
            //Write to CSV
            StaticFunctions.CreateITADReportCSV(reportList);
        }

        private void PITabCtrl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            if (PITabCtrl.SelectedIndex == 0 && vm.ITADIsReprint == true)
            {
                ////Reset the ComboBoxes
                DeviceCatCB.SelectedIndex = -1;
                ProcessPerCB.SelectedIndex = -1;
                TestingProCB.SelectedIndex = -1;
                VerificationCB.SelectedIndex = -1;
                DeleteDeviceCatCB.Text = "";
                DeleteErasureCB.Text = "";
                DeleteTestingCB.Text = "";
                DeleteVerificationCB.Text = "";
                DeleteDeviceCatCB.SelectedIndex = -1;
                DeleteErasureCB.SelectedIndex = -1;
                DeleteTestingCB.SelectedIndex = -1;
                DeleteVerificationCB.SelectedIndex = -1;
                vm.ITADDeviceCategory = "";
                vm.ITADProcessPerformed = "";
                vm.ITADTestingPerformed = "";
                vm.ITADVerificationPer = "";
                vm.ITADIsReprint = false;
                vm.ITADCLIOutput = "";
                vm.ITADPOSONo = "";
                vm.ITADAssetNo = "";
                vm.ITADSerialNo = "";
                vm.ITADTechConfirm = "";
                vm.ITADDeclaration = "";
                vm.ITADDeclaration2 = "";
                vm.ITADVerificationConfirm = "";
                vm.ITADNotes = "";
                vm.ITADCLIOutput = "";
                vm.ITADVerifiedBy = "D Benyon";

                //Reset User Back To Current
                vm.ITADReportUserDate = vm.FullUser.Replace("Checked By", "Technician Name") + " " + DateTime.Now.ToShortDateString();

                //vm.ITADReportCollection.Clear();

                //Disable print button
                PrintBTN.IsEnabled = false;

            }
            else if (PITabCtrl.SelectedIndex == 1)
            {
                vm.ITADIsReprint = true;

                //Disable print button
                PrintBTN.IsEnabled = false;

            }
            else if (PITabCtrl.SelectedIndex == 2)
            {
                //vm.ITADIsReprint = true;

                ////Disable print button
                //PrintBTN.IsEnabled = false;

            }

        }

        private void DateFrom_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //reset search textbox
            SearchTXTB.Text = "";
        }

        private void DateTo_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //reset search textbox
            SearchTXTB.Text = "";
            //Search date range on change
            //FilterDateSearch("", "");
        }

        private void ClearBTN_Click(object sender, RoutedEventArgs e)
        {

            //Clear Document
            vm.ITADPOSONo = "";
            vm.ITADAssetNo = "";
            vm.ITADSerialNo = "";
            vm.ITADDeviceCategory = "";
            vm.ITADProcessPerformed = "";
            vm.ITADTestingPerformed = "";
            vm.ITADVerificationPer = "";
            vm.ITADTechConfirm = "";
            vm.ITADDeclaration = "";
            vm.ITADDeclaration2 = "";
            vm.ITADVerificationConfirm = "";
            vm.ITADNotes = "";
            vm.ITADCLIOutput = "";
            vm.ITADVerifiedBy = "D Benyon";
            vm.ITADQRCode = null;

            DeleteDeviceCatCB.Text = "";
            DeleteErasureCB.Text = "";
            DeleteTestingCB.Text = "";
            DeleteVerificationCB.Text = "";
            DeleteDeviceCatCB.SelectedIndex = -1;
            DeleteErasureCB.SelectedIndex = -1;
            DeleteTestingCB.SelectedIndex = -1;
            DeleteVerificationCB.SelectedIndex = -1;

            //Reset User Back To Current
            vm.ITADReportUserDate = "Technician Name: " + vm.FullUser + " " + DateTime.Now.ToShortDateString();

            //Disable print button
            PrintBTN.IsEnabled = false;

        }

        private void BulkImportLV_DragEnter(object sender, DragEventArgs e)
        {

        }

        private void BulkImportLV_Drop(object sender, DragEventArgs e)
        {

        }

        private async void BRSignBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {

            //Reset User Back To Current
            vm.ITADReportUserDate = "Technician Name: " + vm.FullUser + " " + DateTime.Now.ToShortDateString();

            if (string.IsNullOrWhiteSpace(vm.ITADPOSONo))
            {
                MessageBox.Show("The LOT Number field is required!", "Bulk Reports", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (string.IsNullOrWhiteSpace(vm.ITADDeviceCategory))
            {
                MessageBox.Show("A valid Category needs selecting!", "Bulk Reports", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (string.IsNullOrWhiteSpace(vm.ITADProcessPerformed))
            {
                MessageBox.Show("A valid Erasure Process needs selecting!", "Bulk Reports", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (string.IsNullOrWhiteSpace(vm.ITADTestingPerformed))
            {
                MessageBox.Show("A valid Testing Process needs selecting!", "Bulk Reports", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (string.IsNullOrWhiteSpace(vm.ITADVerificationPer))
            {
                MessageBox.Show("A valid Verification Process needs selecting!", "Bulk Reports", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (string.IsNullOrWhiteSpace(BulkItems.Text))
            {
                MessageBox.Show("Please paste your list into the box provided!", "Bulk Reports", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {


                //Show progress
                ProgressBar.IsIndeterminate = true;


                //Create temporary list
                List<BulkReport> bulkList = new List<BulkReport>();

                //Split lines
                var textLines = BulkItems.Text.Split('\n');

                string assetNo = "";
                string serialNo = "";

                // Loop through pasted list and split via space
                foreach (var line in textLines)
                {
                    //Check if line empty before attempting
                    if (!string.IsNullOrEmpty(line))
                    {

                        //split on space
                        var splitLines = line.Split(' ');

                        //Add item to list with the split array
                        bulkList.Add(new BulkReport(splitLines[0].Trim(), splitLines[1].Trim()));
                    }


                }



                //Create paginator instance
                DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

                // Set save path
                string savePath = "";

                if (Directory.Exists(@"M:\Technical Test Reports\Nebula Test Reports"))
                {
                    //Save to ITAD M Drive
                    savePath = @"M:\Technical Test Reports\Nebula Test Reports\";
                }
                else
                {
                    //Save to Users Documents
                    savePath = @"" + vm.myDocs + @"\";

                }


                //Refresh Current User
                vm.CurrentUser = "Technician Name: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, 3).Replace(".", "").ToUpper();

                //Refresh full user
                vm.FullUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").ToUpperUserFullName();



                //Loop through bulk list
                foreach (var item in bulkList)
                {
                    //populate TextBoxes
                    vm.ITADAssetNo = item.assetNo;
                    vm.ITADSerialNo = item.serialNo;

                        //Generate QR
                        //QRCODE Dimensions
                        ITADStandardLabelZebraImage.Width = 80;
                        ITADStandardLabelZebraImage.Height = 80;


                        // Create QR CODE with Serial No from pasted list
                        vm.ITADQRCode = CreateQRCode(vm.ITADSerialNo.Trim());

                        //*** Checklist QR CODE ***
                        //Call Embed QR Saves & extracts QR Code to File. This is required for the cloning of the flowdocument as it requires a full path
                        vm.ITADQRCode = vm.EmbedChecklistQR(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.ITADSerialNo.Trim() + ".png", vm.ITADQRCode);


                        //Check if non data bearing device 
                        if (ProcessPerCB.Text == "Non Data Bearing Device")
                        {
                        //Set the Declaration properties.
                        vm.ITADDeclaration = "This report serves as confirmation that the asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + @" contains no personally identifiable data nor any configuration data specific to other organisations.";
                        //vm.ITADDeclaration2 = "Asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + " was sanitised using the following process (" + vm.ITADProcessPerformed.Replace("Process Performed: ", "") + @") in line with Company & Manufactures procedures.";
                        //Technician Confirmation
                        vm.ITADTechConfirm = vm.FullUser.Replace("Technician Name: ", "") + " hereby confirms that Asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + @" contains no personally identifiable data nor any configuration data specific to other organisations.";
                        //Technician Confirmation
                        vm.ITADVerificationConfirm = "Sanitisation of this asset was verified by " + vm.ITADVerifiedBy + ", using the verification process " + vm.ITADVerificationPerformedSelItem.Process + ".";
                    }
                    else
                    {
                        //Set the Declaration properties.
                        vm.ITADDeclaration = "This report serves as confirmation that the asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + @" has be sanitised of all user data in accordance to GDPR using Company/Manufactures procedures and guidelines for sanitisation.";
                        vm.ITADDeclaration2 = "Asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + " was sanitised using the following process (" + vm.ITADProcessPerformedSelItem.Process.Replace("Process Performed: ", "") + @") in line with Company & Manufactures procedures.";
                        //Technician Confirmation
                        vm.ITADTechConfirm = vm.FullUser.Replace("Technician Name: ", "") + " hereby confirms that Asset " + vm.ITADAssetNo.Replace("Asset No: ", "") + @"/" + vm.ITADSerialNo.Replace("Serial No: ", "") + @" has been sanitised of all user data and is fit for reuse.";
                        //Technician Confirmation
                        vm.ITADVerificationConfirm = "Sanitisation of this asset was verified by " + vm.ITADVerifiedBy + ", using the verification process " + vm.ITADVerificationPerformedSelItem.Process + ".";
                    }
                  

                    //ADD DB ENTRY HERE
                    InsertData();

                    //Enable print button
                    PrintBTN.IsEnabled = true;
                    vm.ITADIsReprint = false;


                    //Generate PDF
                    //Set string to combination name
                    string fileURI = vm.ITADPOSONo.Replace("PO/SO No: ", "") + " - " + vm.ITADAssetNo.Replace("Asset No: ", "") + " - " + vm.ITADSerialNo.Replace("Serial No: ", "") + " - " + vm.ITADProcessPerformedSelItem.Process + " - " + vm.CurrentUserInitials.Trim();

                    ////Set the serial number to text on the clipboard  Use control v
                    //if (!string.IsNullOrEmpty(vm.ITADSerialNo) || !string.IsNullOrEmpty(vm.ITADSerialNo) || !string.IsNullOrEmpty(vm.ITADSerialNo))
                    //    Clipboard.SetText(fileURI, TextDataFormat.UnicodeText);
                    //Add Delay
                    //await PutTaskDelay(400);
                    // Generate PDF from FlowDocument
                    dpw.FlowDocument2Pdf(Report1, savePath + fileURI + ".pdf");
                    //Add Delay
                    //await PutTaskDelay(400);

                    //Add processed item to List
                    vm.ITADAssetsProcessed.Add(vm.ITADAssetNo.Replace("Asset No: ", "") + "_" + DateTime.Now.ToShortDateString() + "_" + DateTime.Now.ToShortTimeString());
                    //Launch Combined PDF File
                    System.Diagnostics.Process.Start(savePath + fileURI + ".pdf");

                   //Remove QR Code Image
                   if (File.Exists(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.ITADSerialNo.Trim() + ".png"))
                   {
                      File.Delete(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.ITADSerialNo.Trim() + ".png");
                   }

                    } //BULKLIST LOOP



                //TIDY UP
                //Check if reprint or not. Property resetting based off this
                if (vm.ITADIsReprint == true)
                {
                    //RePrint
                    vm.CurrentUser = "Technician Name: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, 3).Replace(".", "").ToUpper();
                    // Reset certain fields
                    AssetNoTXT.Text = "";
                    SerialNoTXT.Text = "";
                    AssetNoTXT.Text = "";
                    //Reset the ComboBoxes
                    DeleteDeviceCatCB.SelectedIndex = -1;
                    DeleteErasureCB.SelectedIndex = -1;
                    DeleteTestingCB.SelectedIndex = -1;
                    DeleteVerificationCB.SelectedIndex = -1;
                    vm.ITADAttachedFiles.Clear();
                    //Reset VM Properties
                    vm.ITADPOSONo = "";
                    vm.ITADSerialNo = "";
                    vm.ITADAssetNo = "";
                    vm.ITADNotes = "";
                    vm.ITADCLIOutput = "";
                    vm.ITADDeclaration = "";
                    vm.ITADDeclaration2 = "";
                    vm.ITADTechConfirm = "";
                    vm.ITADVerification = "";
                    vm.ITADVerificationConfirm = "";
                    vm.ITADVerifiedBy = "D Benyon";
                    //Disable print button
                    PrintBTN.IsEnabled = false;

                }
                else
                {
                    //Not a Reprint
                    vm.CurrentUser = "Technician Name: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace(@"PINNACLE\", @"").Substring(0, 3).Replace(".", "").ToUpper();
                    // Reset certain fields
                    SerialNoTXT.Text = "";
                    AssetNoTXT.Text = "";
                    vm.ITADAttachedFiles.Clear();
                    DeleteDeviceCatCB.SelectedIndex = -1;
                    DeleteErasureCB.SelectedIndex = -1;
                    DeleteTestingCB.SelectedIndex = -1;
                    DeleteVerificationCB.SelectedIndex = -1;
                    //RESET COMBO BOXES
                    BRDeviceCatCB.SelectedIndex = -1;
                    BRProcessPerCB.SelectedIndex = -1;
                    BRTestingProCB.SelectedIndex = -1;
                    BRVerificationCB.SelectedIndex = -1;
                    //Clear pasted list
                    BulkItems.Text = "";
                    //Reset VM Properties
                    vm.ITADSerialNo = "";
                    vm.ITADAssetNo = "";
                    vm.ITADNotes = "";
                    vm.ITADCLIOutput = "";
                    vm.ITADDeclaration = "";
                    vm.ITADDeclaration2 = "";
                    vm.ITADTechConfirm = "";
                    vm.ITADVerification = "";
                    vm.ITADVerificationConfirm = "";
                    vm.ITADVerifiedBy = "D Benyon";
                    //Disable print button
                    PrintBTN.IsEnabled = false;


                }


                //Hide progress
                ProgressBar.IsIndeterminate = false;

            }







            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

        public void GenerateLabel()
        {
            if (vm != null)
            {

                //QRCODE Dimensions
                ITADStandardLabelZebraImage.Width = 80;
                ITADStandardLabelZebraImage.Height = 80;


                // Create QR CODE
                //vm.ChecklistQRCode = CreateQRCode(DateTime.Now.ToShortDateString() + "," + vm.CurrentUserInitials + ",1," + vm.POSONumber + "," + vm.ModelNo + "," + vm.ChassisSerialNo);
                vm.ITADQRCode = CreateQRCode(vm.ITADSerialNo);

                //*** Checklist QR CODE ***
                //Call Embed QR Saves & extracts QR Code to File. This is required for the cloning of the flowdocument as it requires a full path
                vm.ITADQRCode = vm.EmbedChecklistQR(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.ITADSerialNo.Trim() + ".png", vm.ITADQRCode);

            }
        }





        //Generate QRCODE Image
        public BitmapImage CreateQRCode(string stringData)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(stringData, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            System.Drawing.Bitmap qrCodeImage = qrCode.GetGraphic(40, System.Drawing.Color.Black, System.Drawing.Color.White, null, 1, 1, true);



            return ToBitmapImage(qrCodeImage);
        }

        //BITMAP TO BITMAP IMAGE
        public BitmapImage ToBitmapImage(System.Drawing.Bitmap bitmap)
        {
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();

                return bitmapImage;
            }
        }

        ////BITMAP IMAGE TO BITMAP
        private System.Drawing.Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            return new System.Drawing.Bitmap(bitmapImage.StreamSource);
        }




        //Delay
        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

        private void SerialNoTXT_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(SerialNoTXT.Text.Length > 0)
            {
              
            }
            else
            {
                vm.ITADQRCode = null;
            }
           
        }

        private async void GenReportBTN_Click(object sender, RoutedEventArgs e)
        {
            //Clear the collection
            vm.ITADReportCollection.Clear();

            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.AddDays(1).ToString("yyyy-MM-dd");

            //return all items in date range
            //Get Results with filter
            await Task.Run(() => ReadBackgroundData("SELECT * FROM ReportRecords WHERE DateCreated BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ORDER BY  POSONumber ASC"));

            //Create Async Task for long running process
            GenerateDriveReport(ReportLV, vm.DateFrom.ToShortDateString() + " - " + vm.DateTo.ToShortDateString());
        }



        public async Task ReadBackgroundData(string sqlquery)
        {
            try
            {


                // Create Connection
                sqlite_conn = CreateConnection(cs);
             
           
                //Declare observable collection

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();


                //Create Flow Doc


                //Loop through results set
                while (sqlite_datareader.Read())
                {


                    ITADReport itm = new ITADReport();
                    if (sqlite_datareader[0].GetType() != typeof(DBNull))
                        itm.InternalID = sqlite_datareader.GetInt32(0);
                    if (sqlite_datareader[1].GetType() != typeof(DBNull))
                    {

                        string date = sqlite_datareader.GetString(1);
                        itm.DateCreated = Convert.ToDateTime(date);

                    }

                    if (sqlite_datareader[2].GetType() != typeof(DBNull))
                        itm.POSONumber = sqlite_datareader.GetString(2);
                    if (sqlite_datareader[3].GetType() != typeof(DBNull))
                        itm.AssetNo = sqlite_datareader.GetString(3);
                    if (sqlite_datareader[4].GetType() != typeof(DBNull))
                        itm.SerialNo = sqlite_datareader.GetString(4);
                    if (sqlite_datareader[5].GetType() != typeof(DBNull))
                        itm.Manufacturer = sqlite_datareader.GetString(5);
                    if (sqlite_datareader[6].GetType() != typeof(DBNull))
                        itm.ModelNo = sqlite_datareader.GetString(6);
                    if (sqlite_datareader[7].GetType() != typeof(DBNull))
                        itm.DeviceCategory = sqlite_datareader.GetString(7);
                    if (sqlite_datareader[8].GetType() != typeof(DBNull))
                        itm.ProcessPerformed = sqlite_datareader.GetString(8);
                    if (sqlite_datareader[9].GetType() != typeof(DBNull))
                        itm.TestingPerformed = sqlite_datareader.GetString(9);
                    if (sqlite_datareader[10].GetType() != typeof(DBNull))
                        itm.Notes = sqlite_datareader.GetString(10);
                    if (sqlite_datareader[11].GetType() != typeof(DBNull))
                        itm.ReportBody = sqlite_datareader.GetString(11);
                    if (sqlite_datareader[12].GetType() != typeof(DBNull))
                        itm.SavePath = sqlite_datareader.GetString(12);
                    if (sqlite_datareader[13].GetType() != typeof(DBNull))
                        itm.TestedBy = sqlite_datareader.GetString(13);



                    //Update Collection via dispatcher because async
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        //Add item to collection
                        vm.ITADReportCollection.Add(itm);

                        // If collection changes Count
                        vm.RecordsTotal = ReportLV.Items.Count.ToString();

                    });


                }

                //Close DB connection
                sqlite_conn.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //return null;

            }

            //return null;
        }



        public async void GenerateDriveReport(ListView lvValue, string daterange)
        {


            //Create new Flow Document Async
            FlowDocument ReportFD = new FlowDocument();
            Section MainSection = new Section();
            MainSection.Padding = new Thickness(2);
            MainSection.Margin = new Thickness(0);
            Section mySectionReportHeader = new Section();
            mySectionReportHeader.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#0f89bd");  //(Brush)bc.ConvertFrom("#CC119EDA");



            Paragraph ReportHeader = new Paragraph(new Run("Serial Testing Breakdown"));
            ReportHeader.Padding = new Thickness(0, 5, 0, 5);
            ReportHeader.FontSize = 24;
            ReportHeader.FontFamily = new FontFamily("Segoe UI");
            ReportHeader.Foreground = Brushes.White;
            ReportHeader.TextAlignment = TextAlignment.Center;
            ReportHeader.Margin = new Thickness(0, 5, 0, 5);

            //Report date or title header
            Paragraph ReportDateHeader = new Paragraph();
            ReportDateHeader = new Paragraph(new Run("Total Items " + vm.ITADReportCollection.Count().ToString()));
            ReportDateHeader.Padding = new Thickness(0, 1, 0, 1);
            ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
            ReportDateHeader.FontSize = 20;
            ReportDateHeader.Foreground = Brushes.White;
            ReportDateHeader.TextAlignment = TextAlignment.Center;
            ReportDateHeader.Margin = new Thickness(0, 1, 0, 1);

            //Report date or title header
            Paragraph ReportPOHeader = new Paragraph();
            ReportPOHeader = new Paragraph(new Run("Total Orders " + vm.ITADReportCollection.Select(o => o.POSONumber.Trim()).Distinct().Count().ToString()));
            ReportPOHeader.Padding = new Thickness(0, 1, 0, 1);
            ReportPOHeader.FontFamily = new FontFamily("Segoe UI");
            ReportPOHeader.FontSize = 20;
            ReportPOHeader.Foreground = Brushes.White;
            ReportPOHeader.TextAlignment = TextAlignment.Center;
            ReportPOHeader.Margin = new Thickness(0, 1, 0, 1);

            Paragraph ReportDate = new Paragraph();
            ReportDate = new Paragraph(new Run(daterange));
            ReportDate.Padding = new Thickness(0, 1, 0, 1);
            ReportDate.FontFamily = new FontFamily("Segoe UI");
            ReportDate.FontSize = 16;
            ReportDate.Foreground = Brushes.White;
            ReportDate.TextAlignment = TextAlignment.Center;
            ReportDate.Margin = new Thickness(0, 1, 0, 1);




            //IMAGE NOT WORKING ON DIFFRENT THREADS
            //Add TB Logo
            //Create Block ui container and add image as a child
            BlockUIContainer blockUIImage = new BlockUIContainer();
            blockUIImage.Padding = new Thickness(10);
            blockUIImage.Margin = new Thickness(10);
            BitmapImage bmp0 = new BitmapImage();

            bmp0.BeginInit();
            bmp0.UriSource = new Uri("pack://application:,,,/Images/TechBuyerLogoCenteredReports.png", UriKind.Absolute);
            bmp0.CacheOption = BitmapCacheOption.OnLoad;
            bmp0.EndInit();
            bmp0.Freeze();

            System.Windows.Controls.Image img0 = new System.Windows.Controls.Image();

            img0.Source = bmp0;
            img0.Width = 150;
            img0.Height = 75;

            blockUIImage.Child = img0;
            //Add image to section
            mySectionReportHeader.Blocks.Add(blockUIImage);

            //IMAGE NOT WORKING ON DIFFRENT THREADS

            //Add Paragraphs to Header
            mySectionReportHeader.Blocks.Add(ReportHeader);
            mySectionReportHeader.Blocks.Add(ReportDateHeader);
            mySectionReportHeader.Blocks.Add(ReportPOHeader);
            mySectionReportHeader.Blocks.Add(ReportDate);


            //Operative count

            List<string> opList = new List<string>();
            List<string> poList = new List<string>();
            List<string> totalpoList = new List<string>();


            //Get distinct value in column
            opList = vm.ITADReportCollection.Select(o => o.TestedBy.Replace(o.DateCreated.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim()).Distinct().ToList<string>();
            //get all ponumbers operative processed


            //New Paragraph
            Paragraph ReportOperativeTotals = new Paragraph();
            Paragraph POOperativeTotals = new Paragraph();
            Paragraph ReportPOTotals = new Paragraph();


            //Create new Section to host table
            Section mySectionRecords = new Section();
            mySectionRecords.Margin = new Thickness(0);

            //ADD POTOTALS

            //Get all distinct PO Numbers
            totalpoList = vm.ITADReportCollection.Select(p => p.POSONumber).ToList<string>();


            //Operative PO Section
            string operatives = "";
            string pos = "";

            foreach (var op in opList)
            {
                //Clear list before populating
                poList.Clear();

                //Create new Paragraph Add Operative Ref
                ReportOperativeTotals = new Paragraph(new Run(op));
                ReportOperativeTotals.Padding = new Thickness(1, 1, 1, 1);
                ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
                ReportOperativeTotals.FontSize = 15;
                ReportOperativeTotals.Foreground = Brushes.MediumPurple;
                ReportOperativeTotals.TextAlignment = TextAlignment.Center;
                ReportOperativeTotals.Margin = new Thickness(1, 1, 1, 1);
                //Add to block
                mySectionRecords.Blocks.Add(ReportOperativeTotals);

                //Count the items an operative has processed in collection  + " " + vm.ITADReportCollection.Count(o => o.TestedBy == op).ToString()
                operatives += " Orders: " + vm.ITADReportCollection.Where(p => p.TestedBy.Replace(p.DateCreated.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.POSONumber).Distinct().Count().ToString() + " Items: " + vm.ITADReportCollection.Count(o => o.TestedBy.Replace(o.DateCreated.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).ToString() + "";

                ReportOperativeTotals = new Paragraph(new Run(operatives));
                ReportOperativeTotals.Padding = new Thickness(1, 1, 1, 1);
                ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
                ReportOperativeTotals.FontSize = 15;
                ReportOperativeTotals.Foreground = Brushes.Green;
                ReportOperativeTotals.TextAlignment = TextAlignment.Center;
                ReportOperativeTotals.Margin = new Thickness(1, 1, 1, 1);
                //Add to block
                mySectionRecords.Blocks.Add(ReportOperativeTotals);

                //Get breakdown of PO's per Operative
                poList = vm.ITADReportCollection.Where(p => p.TestedBy.Replace(p.DateCreated.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.POSONumber).Distinct().ToList<string>();

                foreach (var po in poList)
                {
                    pos += " " + po + " (" + vm.ITADReportCollection.Count(o => o.POSONumber == po).ToString() + @"\" + totalpoList.Count(p => p == po).ToString() + ")  ";

                }

                //Create new Paragraph
                ReportOperativeTotals = new Paragraph(new Run(pos));
                ReportOperativeTotals.Padding = new Thickness(1, 1, 1, 1);
                ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
                ReportOperativeTotals.FontSize = 12;
                ReportOperativeTotals.Foreground = Brushes.Black;
                ReportOperativeTotals.TextAlignment = TextAlignment.Center;
                ReportOperativeTotals.Margin = new Thickness(1, 1, 1, 1);


                mySectionRecords.Blocks.Add(ReportOperativeTotals);



                operatives = "";
                pos = "";

            }




            //1 Add Header To Main Section
            MainSection.Blocks.Add(mySectionReportHeader);
            //2 Add Records to Main Section
            MainSection.Blocks.Add(mySectionRecords);


            //Add Main Section to flowdocument
            ReportFD.Blocks.Add(MainSection);


            //END OF FLOWDOCUMENT


            String FileName = "";




            if (string.IsNullOrEmpty(SearchTXTB.Text))
            {
                //Set Clipboard Include date range


                FileName = "Serial Testing Report_" + vm.DateFrom.ToShortDateString().Replace("/", "-") + " To " + vm.DateTo.ToShortDateString().Replace("/", "-");
            }
            else
            {
                //Set Clipboard Include date range

                FileName = SearchTXTB.Text + "_Serial Testing Report_" + vm.DateFrom.ToShortDateString().Replace("/", "-") + " To " + vm.DateTo.ToShortDateString().Replace("/", "-");

            }


            //SAVE FLOW DOC TO PDF
            DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

            string SavePath = vm.myDocs + @"\" + FileName + @".pdf";

            //Clone the source document
            var str = XamlWriter.Save(ReportFD);
            var stringReader = new System.IO.StringReader(str);
            var xmlReader = XmlReader.Create(stringReader);
            var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

            DocumentPaginator Clonepaginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;

            //A4 Dimensions
            Clonepaginator.PageSize = new System.Windows.Size(1122, 793);

            Thickness t = new Thickness(72);  // copy.PagePadding;
            CloneDoc.PagePadding = new Thickness(
                             Math.Max(0, t.Left),
                               Math.Max(0, t.Top),
                             Math.Max(1122 - (0 + 1122), t.Right),
                             Math.Max(793 - (0 + 793), t.Bottom));



            CloneDoc.ColumnWidth = double.PositiveInfinity;

            //End A4 Dimensions

            //Convert Flow Doc to Fixed First
            var paginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;
            var package = Package.Open(new MemoryStream(), FileMode.Create, FileAccess.ReadWrite);
            var packUri = new Uri("pack://temp.xps");
            PackageStore.RemovePackage(packUri);
            PackageStore.AddPackage(packUri, package);
            var xps = new XpsDocument(package, CompressionOption.NotCompressed, packUri.ToString());
            XpsDocument.CreateXpsDocumentWriter(xps).Write(paginator);
            FixedDocument fddoc = new FixedDocument();


            //Requires Dispatcher
            Application.Current.Dispatcher.Invoke((Action)delegate
            {



                fddoc = xps.GetFixedDocumentSequence().References[0].GetDocument(true);
                // your code



                //Fixed document to PDF
                var ms = new MemoryStream();
                var package2 = Package.Open(ms, FileMode.Create);
                var doc = new XpsDocument(package2);
                var writer = XpsDocument.CreateXpsDocumentWriter(doc);
                writer.Write(fddoc.DocumentPaginator);

                //docWriter.Write(fddoc.DocumentPaginator);
                doc.Close();
                package2.Close();

                // Get XPS file bytes
                var bytes = ms.ToArray();
                ms.Dispose();

                // Print to PDF
                var outputFilePath = SavePath;
                PdfFilePrinter.PrintXpsToPdf(bytes, outputFilePath, "Document Title");


                //Launch report
                ////Launch file for printing
                if (File.Exists(outputFilePath))
                {
                    System.Diagnostics.Process.Start(outputFilePath);

                }

            });


            //create new instance
            //vm.ITADReportCollection = new ObservableCollection<ITADReporty>();



        }

      
    }





    public class BulkReport
        {

            public BulkReport()
            {

            }

            public BulkReport(string assetno, string serialno)
            {
                assetNo = assetno;
                serialNo = serialno;

            }

            public string assetNo { get; set; }
            public string serialNo { get; set; }

        }

    
}

