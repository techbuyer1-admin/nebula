﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Nebula.ViewModels;
using Nebula.Models;
using MahApps.Metro.Controls;
using Nebula.Commands;
using System.IO;
using Nebula.Paginators;
using System.Windows.Media;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Net.Security;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Net.Http;
using Nebula.DellServers;
using Nebula.Helpers;
using MahApps.Metro.Controls.Dialogs;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Globalization;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for DellServerView.xaml
    /// </summary>
    public partial class DellServerView : UserControl
    {


        //declare viewmodel
       MainViewModel vm = new MainViewModel(DialogCoordinator.Instance);    //
       MainViewModel vmMain;
 

        TabItem ServerTab = null;
        string ServerTabHeader = "";

        public DellServerView()
        {
            InitializeComponent();

            this.DataContext = vm;

            // vm = new MainViewModel();

            //CLEAR SERVER PROPERTIES ON LOAD
            //reset flipview to start
            //FlipView1.SelectedIndex = 0;
            //delete server object
            //vm.DellServerS1 = null;
            // tab header set back to default
            //vm.Server1TabHeader = "Server1";
            ////clear server ip
            vm.Server1IPv4 = string.Empty;


            vm.OverridePasswordS1 = "calvin";
         

        }




        public DellServerView(MainViewModel PassedViewModel, string whichTab, string PassedServerIP, bool IsBlade, string EnclosureBay)
        {
            InitializeComponent();
            //get the passed in viewmodel and assign to the local vm variable
            vmMain = PassedViewModel;
          

            this.DataContext = vm;


            // Slide to main server page & Run Get Server Info

            //MessageBox.Show("New Instance");

            //DellServerUC.Name = InstanceName;


            //CLEAR SERVER PROPERTIES ON LOAD
            //reset flipview to start
            //FlipView1.SelectedIndex = 0;
            //delete server object
            vm.DellServerS1 = null;
            // tab header set back to default
            vm.Server1TabHeader = "Server1";
            //clear server ip
            vm.Server1IPv4 = string.Empty;

        


            // Wire up event for webbrowsers
            WebBrowse1.LoadCompleted += WebBrowse1_LoadCompleted;


           // RunServerScriptsAllS1.Visibility = Visibility.Collapsed;
           


            vm.OverridePasswordS1 = "calvin";
           

            // ADD THIS TO GET SERVER INFO SCRIPT


            //PULL IN INFRO FROM CONSTRUCTOR AND LAUNCH GET SERVER INFO
            //Set Tab
            vm.WhichTab = whichTab;
            vm.Server1IPv4 = PassedServerIP;



            //Console.WriteLine(vmMain.Server1IPv4);




            //MessageBox.Show(PassedViewModel.ServerType);

            //IS IT A BLADE?
            //Check If Blade and Switch Slide and set get server info going
            if (PassedViewModel.ServerType == "DellBlade" && IsBlade == true)
            {



                // MessageBox.Show("Prior Tp WhichTab Check");
                //Set the ip in the Master ViewModel, needed for free enclosure slots
                switch (vm.WhichTab)
                {
                    case "1":
                    //MessageBox.Show("Inside WhichTAB 1");

                    ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Orange;
                        ServerTab.Header = EnclosureBay;
                    }
                                
                        vmMain.Server1IPv4 = PassedServerIP;

                        break;
                    case "2":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server2IPv4 = PassedServerIP;
                        break;
                    case "3":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server3IPv4 = PassedServerIP;
                        break;
                    case "4":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server4IPv4 = PassedServerIP;
                        break;
                    case "5":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server5IPv4 = PassedServerIP;
                        break;
                    case "6":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server6IPv4 = PassedServerIP;
                        break;
                    case "7":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server7IPv4 = PassedServerIP;
                        break;
                    case "8":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server8IPv4 = PassedServerIP;
                        break;
                    case "9":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server9IPv4 = PassedServerIP;
                        break;
                    case "10":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server10IPv4 = PassedServerIP;
                        break;
                    case "11":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server11IPv4 = PassedServerIP;
                        break;
                    case "12":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server12IPv4 = PassedServerIP;
                        break;
                    case "13":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server13IPv4 = PassedServerIP;
                        break;
                    case "14":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server14IPv4 = PassedServerIP;
                        break;
                    case "15":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server15IPv4 = PassedServerIP;
                        break;
                    case "16":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server16IPv4 = PassedServerIP;
                        break;

                }




                //Set Server Info Going
                vm.FlipView1Index = 2;
                if (vm.DellServerS1 == null)
                    vm.GetDellServerDataCommand.Execute(new String[] { "ServerInfo", EnclosureBay });




            }



        }


        public static void AddDocument(FlowDocument from, FlowDocument to)
        {
            TextRange range = new TextRange(from.ContentStart, from.ContentEnd);
            MemoryStream stream = new MemoryStream();
            System.Windows.Markup.XamlWriter.Save(range, stream);
            range.Save(stream, DataFormats.XamlPackage);
            TextRange range2 = new TextRange(to.ContentEnd, to.ContentEnd);
            range2.Load(stream, DataFormats.XamlPackage);
        }


        private void DoThePrint(System.Windows.Documents.FlowDocument document)
        {
            // Clone the source document's content into a new FlowDocument.
            // This is because the pagination for the printer needs to be
            // done differently than the pagination for the displayed page.
            // We print the copy, rather that the original FlowDocument.
            System.IO.MemoryStream s = new System.IO.MemoryStream();
            TextRange source = new TextRange(document.ContentStart, document.ContentEnd);
            source.Save(s, DataFormats.Xaml);
            FlowDocument copy = new FlowDocument();
            TextRange dest = new TextRange(copy.ContentStart, copy.ContentEnd);
            dest.Load(s, DataFormats.Xaml);

            // Create a XpsDocumentWriter object, implicitly opening a Windows common print dialog,
            // and allowing the user to select a printer.

            // get information about the dimensions of the seleted printer+media.
            System.Printing.PrintDocumentImageableArea ia = null;
            System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);

            if (docWriter != null && ia != null)
            {
                DocumentPaginator paginator = ((IDocumentPaginatorSource)copy).DocumentPaginator;

                // Change the PageSize and PagePadding for the document to match the CanvasSize for the printer device.
                paginator.PageSize = new Size(ia.MediaSizeWidth, ia.MediaSizeHeight);
                Thickness t = new Thickness(72);  // copy.PagePadding;
                copy.PagePadding = new Thickness(
                                 Math.Max(ia.OriginWidth, t.Left),
                                   Math.Max(ia.OriginHeight, t.Top),
                                   Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
                                   Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));

                copy.ColumnWidth = double.PositiveInfinity;
                //copy.PageWidth = 528; // allow the page to be the natural with of the output device

                // Send content to the printer.
                docWriter.Write(paginator);
            }

        }


        //private void PrintBTNS1_Click(object sender, RoutedEventArgs e)
        //{

        //    ////Set the serial number to text on the clipboard  Use control v
        //    if (vm.DellServerS1 != null)
        //    {
        //        if (!string.IsNullOrEmpty(vm.DellServerS1.ServiceTag))
        //            Clipboard.SetText(vm.DellServerS1.ServiceTag.Trim() + "_Report", TextDataFormat.UnicodeText);


        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(DELLServerReportS1);
        //    }



        //}










        //Previous slide to help skip Test Cert for GOT
        int previousSlide = 0;

        private void FlipView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var flipview = ((FlipView)sender);
            switch (flipview.SelectedIndex)
            {
                case 0:
                    flipview.BannerText = "Dell Procedure";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                    }

                    break;
                case 1:
                    flipview.BannerText = "Obtain the DHCP Address";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                    }
                    break;
                case 2:
                    flipview.BannerText = "Enter Server IP & Run Scripts";
                    if (vm != null)
                    {

                        //vm.ILORestOutput1 = "";

                        //Runs a check every 20 seconds for LCD and Info
                        if (string.IsNullOrWhiteSpace(vm.Server1IPv4) != true || vm.Server1IPv4 != "")
                        {
                            //await CheckLCDDisplay(vm.Server1IPv4, vm.myDocs, vm.IsScriptRunningS1, vm.IsServerThere1, "S1");
                        }
                        //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);
                        //Load Ultima URL
                        if (vm.DellServerS1 != null)
                        {
                            if (vm.DellServerS1.Manufacturer != null && vm.DellServerS1.Model != null)
                                vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.DellServerS1.Manufacturer.Replace("Inc.", "") + " " + vm.DellServerS1.Model + " Server &go=Go";
                        }


                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";

                    }
                    break;

                case 3:
                    flipview.BannerText = "Server Reports & Checklist";
                    if (vm != null)
                    {

                        if (vm.DellServerS1 != null)
                        {
                            //await PutTaskDelay(500);


                            //ADD GOT Specific items
                            if (StaticFunctions.Department == "GoodsOutTech")
                            {
                                flipview.BannerText = "Server Certificate";
                                //Create instance of Checklist view and pass in the instance of the mainview model
                                vm.ChecklistView1 = new ServerCertReportView(vm);
                                if (vm != null)
                                {
                                    vm.GenerateTestCertificate("Dell");
                                }
                                //Create instance of HP Test Report View
                                vm.ServerCertView1 = new TestCertificateView(vm);
                                //Create instance of Dell Test Report View
                                vm.ReportView1 = new DellReportsView(vm);

                            }
                            else
                            {
                                //Goods In
                                flipview.BannerText = "Server Checklist";
                                //Create instance of Checklist view and pass in the instance of the mainview model
                                vm.ChecklistView1 = new CheckListView(vm);
                                //Create instance of Dell Test Report View
                                vm.ReportView1 = new DellReportsView(vm);

                            }


                            //vm.ProgressVisibility = Visibility.Hidden;
                            //vm.ProgressIsActive = false;
                            //vm.ProgressPercentage = 0;
                            //vm.ProgressMessage = "";
                            vm.ProgressMessage1 = "";
                            //vm.ILORestOutput1 = "";
                            //Load Ultima URL
                            if (vm.DellServerS1 != null)
                            {
                                if (vm.DellServerS1.Manufacturer != null && vm.DellServerS1.Model != null)
                                    vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.DellServerS1.Manufacturer.Replace("Inc.", "") + " " + vm.DellServerS1.Model + " Server &go=Go";
                            }

                            // Clear QR bitmap image.
                            vm.ChecklistQRCode = null;



                        }
                    }
                    break;
                case 4:
                    flipview.BannerText = "Final Wipe Scripts";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                        vm.ProgressMessage1 = "";
                        vm.ILORestOutput1 = "";
                        //Set previous slide to help skip Test Cert for GOT
                        previousSlide = 4;

                    }
                    break;
                case 5:
                    flipview.BannerText = "Test Certificate";
                    if (vm != null)
                    {
                        vm.ILORestOutput1 = "";

                        //ADD GOT Specific items
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //GOT Skip the Test Cert Sliide
                            switch (previousSlide)
                            {
                                case 4:
                                    vm.CertificateView1 = null;
                                    flipview.SelectedIndex = 6;
                                    break;
                                case 6:
                                    vm.CertificateView1 = null;
                                    flipview.SelectedIndex = 4;
                                    break;
                            }

                        }
                        else
                        {
                            //Show for Goods In
                            vm.GenerateTestCertificate("Dell");
                        }
        
                    }
                    break;
                case 6:
                    flipview.BannerText = "Completion";

                    if (vm != null)
                    {
                        //Reset license properties
                        vm.GetServerInfoInitialRun = false;
                        vm.DellEmbeddedLicense = "";
                        //Set previous slide to help skip Test Cert for GOT
                        previousSlide = 6;

                    }
                    break;
            }
        }







        private void RefreshDataS1BTN_Click(object sender, RoutedEventArgs e)
        {
            if (vm.DellServerS1 != null)
            {

                vm.GetDellServerDataCommand.Execute(new String[] { "ServerInfo", "Dell" });
                // MessageBox.Show("Dropped Through");
                vm.DellServerS1.TestedBy = vm.CurrentUser.Replace("Checked By:", "");
                //vm.DELLGenerateReport(DELLServerReportS1, vm.DellServerS1);
            }
        }






        private async Task CheckLCDDisplay(string ServerIP, string myDocs, bool OtherScript, string ServerThere, string ServerNumber)
        {

            ProcessPiper pp = new ProcessPiper(vm);


            if (ServerIP != string.Empty)
                while (true)
                {

                    if (OtherScript != true)
                    {


                        if (ServerThere == "Yes")
                        {


                            switch (ServerNumber)
                            {
                                case "S1":
                                    //get LCD display
                                    //MessageBox.Show("Yep");
                                    await System.Threading.Tasks.Task.Run(() => pp.StartSensorInventoryRACDM1(new System.IO.FileInfo(@"" + myDocs + @"\HPTOOLS\Dell\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + ServerIP + @" -u root -p calvin --nocertwarn get System.LCD", @"" + myDocs + @"\HPTOOLS\Dell\SysMgt\iDRACTools\racadm"));
                                    break;
                                
                            }




                        }

                    }

                    //MessageBox.Show("Fire");
                    await Task.Delay(TimeSpan.FromSeconds(20));
                }
        }


        private async Task RefreshServerInfo(string ServerIP, string myDocs)
        {
            ProcessPiper pp = new ProcessPiper(vm);

            while (true)
            {
                if (vm.IsServerThere1 == "Yes")
                {
                    if (vm.IsScriptRunningS1 != true)
                    {
                        //MessageBox.Show("Fire1");
                        //acquire the server inventory 


                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDMGeneric1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Dell\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p calvin --nocertwarn hwinventory export -f " + vm.Server1IPv4 + @"Inventory.xml", @"" + vm.myDocs + @"\HPTOOLS\Dell\SysMgt\iDRACTools\racadm"));
                    }


                }

                //MessageBox.Show("Fire");
                await Task.Delay(TimeSpan.FromSeconds(80));
            }
        }




        //SERVER1
        private void SelectAllBtnS1_Click(object sender, RoutedEventArgs e)
        {


            //GIQ1CBS1.IsChecked = true;
            //GIQ2CBS1.IsChecked = true;
            ////GIQ3CBS1.IsChecked = true;
            //GIQ4CBS1.IsChecked = true;
            //GIQ5CBS1.IsChecked = true;
            //GIQ6CBS1.IsChecked = true;
            //GIQ7CBS1.IsChecked = true;
            //GIQ8CBS1.IsChecked = true;
            //GIQ9CBS1.IsChecked = true;
            //GIQ10CBS1.IsChecked = true;
            //GIQ11CBS1.IsChecked = true;
            //GIQ12CBS1.IsChecked = true;
            //GIQ13CBS1.IsChecked = true;
            //GIQ14CBS1.IsChecked = true;
            //GIQ15CBS1.IsChecked = true;
            ////GIQ16CBS1.IsChecked = true;
            //GIQ17CBS1.IsChecked = true;
            ////GIQ18CBS1.IsChecked = true;
            //GIQ19CBS1.IsChecked = true;
            //GIQ20CBS1.IsChecked = true;
            ////GIQ21CBS1.IsChecked = true;
            //GIQ22CBS1.IsChecked = true;
            ////GIQ23CBS1.IsChecked = true;
            //GIQ24CBS1.IsChecked = true;
            //GIQ25CBS1.IsChecked = true;
            ////MessageTB.Text = String.Empty;
        }

        private void ClearAllBtnS1_Click(object sender, RoutedEventArgs e)
        {
            //GIQ1CBS1.IsChecked = false;
            //GIQ2CBS1.IsChecked = false;
            ////GIQ3CBS1.IsChecked = false;
            //GIQ4CBS1.IsChecked = false;
            //GIQ5CBS1.IsChecked = false;
            //GIQ6CBS1.IsChecked = false;
            //GIQ7CBS1.IsChecked = false;
            //GIQ8CBS1.IsChecked = false;
            //GIQ9CBS1.IsChecked = false;
            //GIQ10CBS1.IsChecked = false;
            //GIQ11CBS1.IsChecked = false;
            //GIQ12CBS1.IsChecked = false;
            //GIQ13CBS1.IsChecked = false;
            //GIQ14CBS1.IsChecked = false;
            //GIQ15CBS1.IsChecked = false;
            ////GIQ16CBS1.IsChecked = false;
            //GIQ17CBS1.IsChecked = false;
            ////GIQ18CBS1.IsChecked = false;
            //GIQ19CBS1.IsChecked = false;
            //GIQ20CBS1.IsChecked = false;
            ////GIQ21CBS1.IsChecked = false;
            //GIQ22CBS1.IsChecked = false;
            ////GIQ23CBS1.IsChecked = false;
            //GIQ24CBS1.IsChecked = false;
            //GIQ25CBS1.IsChecked = false;
            ////MessageTB.Text = String.Empty;
        }




        private void GenCheck1_Click(object sender, RoutedEventArgs e)
        {

            //reset flipview to start
            FlipView1.SelectedIndex = 0;
            //Add to Database
            //vm.ServersProcessedCRUDCommand.Execute(vm);
            vm.DELLFrontLCDS1 = string.Empty;
            //ChassisNoTXTBS1.Text = "";
            
            //clear server ip
            //vm.Server1IPv4 = string.Empty;
            vm.BIOSUpgradePath1 = string.Empty;
            vm.CONTUpgradePath1 = string.Empty;
            vm.IDRACUpgradePath1 = string.Empty;
            vm.ILORestOutput1 = string.Empty;
            vm.ProgressIsActive1 = false;
            vm.ProgressMessage1 = string.Empty;
            vm.ProgressPercentage1 = 0;
            //AddTestMsg1.Text = string.Empty;

            //Set the ip in the Master ViewModel, needed for free enclosure slots
            switch (vm.WhichTab)
            {
                case "1":
                    vmMain.Server1IPv4 = "";
                    break;
                case "2":
                    vmMain.Server2IPv4 = "";
                    break;
                case "3":
                    vmMain.Server3IPv4 = "";
                    break;
                case "4":
                    vmMain.Server4IPv4 = "";
                    break;
                case "5":
                    vmMain.Server5IPv4 = "";
                    break;
                case "6":
                    vmMain.Server6IPv4 = "";
                    break;
                case "7":
                    vmMain.Server7IPv4 = "";
                    break;
                case "8":
                    vmMain.Server8IPv4 = "";
                    break;
                case "9":
                    vmMain.Server9IPv4 = "";
                    break;
                case "10":
                    vmMain.Server10IPv4 = "";
                    break;
                case "11":
                    vmMain.Server11IPv4 = "";
                    break;
                case "12":
                    vmMain.Server12IPv4 = "";
                    break;
                case "13":
                    vmMain.Server13IPv4 = "";
                    break;
                case "14":
                    vmMain.Server14IPv4 = "";
                    break;
                case "15":
                    vmMain.Server15IPv4 = "";
                    break;
                case "16":
                    vmMain.Server16IPv4 = "";
                    break;

            }


            //  //tab header set back to default
            //vm.Server1TabHeader = "Server" + vm.WhichTab;

            //Reset Test Certificate properties
            vm.TestCertificateS1 = null;
            vm.CertificateView1 = null;
            vm.TestCertificatePrinted1 = false;
            vm.UpdatesRun = "";

            TabItem ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + vm.WhichTab) as TabItem;
            if (ServerTab != null)
            {
                ServerTab.Header = "Server" + vm.WhichTab;
                ServerTab.Foreground = Brushes.MediumPurple;
            }
         


            vm.OverridePasswordS1 = "calvin";

        }







        private void CancelScriptS1_Click(object sender, RoutedEventArgs e)
        {
            vm.CancelScriptS1 = true;
        }




        static readonly Guid SID_SWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");


        public async void Login2ServerWebPage(string ServerIP)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            //WebClient client = new WebClient();
            //Byte[] pageData = client.DownloadData("https://192.168.101.35");
            //string pageHtml = Encoding.ASCII.GetString(pageData);
            //Console.WriteLine(pageHtml);




            //Launch Dell Webpage   + "/console"
            //WebBrowse1.Navigate("https://" + vm.Server1IPv4 + "/console");
            //WebBrowse1.Navigate("https://" + ServerIP + "");
            await PutTaskDelay(100);
           // WebBrowse1.Focus();
            await PutTaskDelay(1000);
            //WebBrowse1.Refresh();
            //WebBrowse1.Focus();
            await PutTaskDelay(4000);
            //WebBrowse1.Focus();


            // ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);


            //Need to use Microsoft HTML reference, to pass the WebBrowse Document to it
            mshtml.HTMLDocument sslpage = (mshtml.HTMLDocument)WebBrowse1.Document;

            if (sslpage.getElementById("overridelink") != null)
            {
                mshtml.IHTMLElement gohome = sslpage.getElementById("overridelink");
                //MessageBox.Show(gohome.id);
                gohome.click();
                await PutTaskDelay(4000);
            }


            //LOGIN
            //Need to use Microsoft HTML reference, to pass the WebBrowse Document to it 
            mshtml.HTMLDocument document = (mshtml.HTMLDocument)WebBrowse1.Document;

            //Get the user and password input boxes
            if (document.getElementById("user") != null)
            {
                mshtml.IHTMLElement userInput = document.getElementById("user");
                mshtml.IHTMLElement passwordInput = document.getElementById("password");
                userInput.innerText = "root";
                passwordInput.innerText = "calvin";
                mshtml.IHTMLElement OKButton = document.getElementById("btnOK");
                ////click the ok button
                OKButton.click();
            }

            await PutTaskDelay(20000);
            //////for frames embedded in main document
            // mshtml.HTMLDocument doc = this.WebBrowse1.Document as mshtml.HTMLDocument;
            //mshtml.HTMLWindow2 win = doc.frames.item(1) as mshtml.HTMLWindow2;
            //mshtml.HTMLDocument innerdoc = win.document as mshtml.HTMLDocument;
            //string str = innerdoc.body.innerText;
            //MessageBox.Show(str);
            // END OF FRAMES CAPTURE

            await PutTaskDelay(3000);

            //Need to use Microsoft HTML reference, to pass the WebBrowse Document to it 
            mshtml.HTMLDocument preview = (mshtml.HTMLDocument)WebBrowse1.Document;


            if (preview.getElementById("remoteConLaunch_link") != null)
            {
                mshtml.IHTMLElement remoteConsole = document.getElementById("remoteConLaunch_link");

                remoteConsole.click();

            }


            await PutTaskDelay(5000);


            //Get the user and password input boxes
            if (document.getElementById("user") != null)
            {
                mshtml.IHTMLElement userInput = document.getElementById("user");
                mshtml.IHTMLElement passwordInput = document.getElementById("password");
                userInput.innerText = "root";
                passwordInput.innerText = "calvin";
                mshtml.IHTMLElement OKButton = document.getElementById("btnOK");
                ////click the ok button
                OKButton.click();
            }

            await PutTaskDelay(2000);

            //Need to use Microsoft HTML reference, to pass the WebBrowse Document to it
            mshtml.HTMLDocument sslpage2 = (mshtml.HTMLDocument)WebBrowse1.Document;

            if (sslpage2.getElementById("overridelink") != null)
            {
                mshtml.IHTMLElement gohome = sslpage.getElementById("overridelink");
                //MessageBox.Show(gohome.id);
                gohome.click();
                await PutTaskDelay(4000);
            }

            //WebBrowse2.Navigate(WebBrowse1.Document);

            //Get the user and password input boxes
            if (preview.getElementById("console_preview_img_id") != null)
            {
                //MessageBox.Show("Console Preview");

                mshtml.IHTMLElement previewImg = document.getElementById("console_preview_img_id");




                var imgWidth = previewImg.getAttribute("width");
                var imgHeight = previewImg.getAttribute("height");
                imgWidth = 800;
                imgHeight = 450;


                //Path to preview image https://192.168.101.42/capconsole/scapture0.png?1607510739495
                //mshtml.IHTMLElement passwordInput = document.getElementById("password");
                //previewImg.innerHTML.Contains("width=") = "400";
                //passwordInput.innerText = "calvin";<img name="console_preview_img_id" id="console_preview_img_id" src="https://192.168.101.42/capconsole/scapture0.png?1607510739495" height="200" width="400" onerror="javascript:imageLoadError(this);" alt="">
                //mshtml.IHTMLElement OKButton = document.getElementById("btnOK");
                //////click the ok button
                //OKButton.click();
            }
        }



        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private void LaunchRemoteConsoleS1_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(@"https://" + vm.Server1IPv4.ToString() + @"/console");
            //Login to webpage
            Login2ServerWebPage(vm.Server1IPv4);
        }



        private async void LaunchVirtualConsoleS2_Click(object sender, RoutedEventArgs e)
        {
            //Need to use Microsoft HTML reference, to pass the WebBrowse Document to it 
            mshtml.HTMLDocument preview = (mshtml.HTMLDocument)WebBrowse1.Document;

            //Get the user and password input boxes
            //if (preview.getElementById("console_preview_img_id") != null)
            //{

            mshtml.IHTMLElement previewImg = preview.getElementById("da");

            //mshtml.IHTMLElement remoteConsole = preview.getElementById("remoteconlaunch_link");
            //mshtml.IHTMLElement previewImg = preview.getElementById("console_preview_img_id");
            //mshtml.IHTMLElement remoteConsole = preview.getElementById("remoteconlaunch_link");
            //MessageBox.Show(previewImg.getAttribute("class"));
            await PutTaskDelay(5000);

            //var imgWidth = previewImg.getAttribute("width");
            //var imgHeight = previewImg.getAttribute("height");
            //imgWidth = 500;
            //imgHeight = 250;


            //  MessageBox.Show(previewImg.getAttribute("width"));
            //   //remoteConsole.click();

            //Path to preview image https://192.168.101.42/capconsole/scapture0.png?1607510739495
            //mshtml.IHTMLElement passwordInput = document.getElementById("password");
            //previewImg.innerHTML.Contains("width=") = "400";
            //passwordInput.innerText = "calvin";<img name="console_preview_img_id" id="console_preview_img_id" src="https://192.168.101.42/capconsole/scapture0.png?1607510739495" height="200" width="400" onerror="javascript:imageLoadError(this);" alt="">
            //mshtml.IHTMLElement OKButton = document.getElementById("btnOK");
            //////click the ok button
            //OKButton.click();
            //}
        }


        private void SkipBTN1_Click(object sender, RoutedEventArgs e)
        {

            //reset flipview to start
            FlipView1.SelectedIndex = 2;
        }




        private void CloseAppByPid(int processID)
        {
            try
            {
                Process proc = Process.GetProcessById(processID);
                if (proc.ProcessName.Contains("HPLOCONS"))
                {
                    proc.Kill();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }



        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

        private void LaunchWebPage1_Click(object sender, RoutedEventArgs e)
        {
            //System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object sender1, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

            //System.Diagnostics.Process.Start(@"https://" + vm.Server1IPv4.ToString() + @"/console");



            //var myClient = new WebClient();
            //Stream response = myClient.OpenRead(@"https://" + vm.Server1IPv4.ToString() + @"/console");

            ////WebBrowse1.scr
            //// The stream data is used here.
            //response.Close();


            //String url = @"https://" + vm.Server1IPv4.ToString();
            //HttpWebRequest request = HttpWebRequest.CreateHttp(url);
            //request.ServerCertificateValidationCallback += (sender2, certificate, chain, sslPolicyErrors) => true;

            //MessageBox.Show(request.GetResponse().ToString());
            //client.

          



        }


        //WPF WEBBROWSER RELATED ITEMS
        private void WebBrowse1_Navigating(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            ////WORKS
            //////Clear any javacript warnings in webbrowser control
            dynamic activeX = this.WebBrowse1.GetType().InvokeMember("ActiveXInstance",
                BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null, this.WebBrowse1, new object[] { });

            activeX.Silent = true;
            //WORKS

            HideScriptErrors(WebBrowse1, true);

        }

        private void WebBrowse1_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            //TO LOAD ANY LINKS BACK INTO THE WEBBROWSER WINDOW
            //IServiceProvider serviceProvider = (IServiceProvider)WebBrowse1.Document;
            //Guid serviceGuid = SID_SWebBrowserApp;
            //Guid iid = typeof(SHDocVw.IWebBrowser2).GUID;
            //SHDocVw.IWebBrowser2 myWebBrowser2 = (SHDocVw.IWebBrowser2)serviceProvider.QueryService(ref serviceGuid, ref iid);
            //SHDocVw.DWebBrowserEvents_Event wbEvents = (SHDocVw.DWebBrowserEvents_Event)myWebBrowser2;
            //wbEvents.NewWindow += new SHDocVw.DWebBrowserEvents_NewWindowEventHandler(OnWebBrowserNewWindow);
        }



        void OnWebBrowserNewWindow(string URL, int Flags, string TargetFrameName, ref object PostData, string Headers, ref bool Processed)
        {
            Processed = true;
            WebBrowse1.Navigate(URL);
        }

        private void WebBrowse1_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            // double Zoom = 0.5;
            // mshtml.IHTMLDocument2 doc = WebBrowse1.Document as mshtml.IHTMLDocument2;
            ////doc.
            // doc.parentWindow.execScript("document.body.style.zoom=" + Zoom.ToString().Replace(",", ".") + ";");
        }

        public void HideScriptErrors(WebBrowser wb, bool Hide)
        {

            FieldInfo fiComWebBrowser = typeof(WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fiComWebBrowser == null) return;
            object objComWebBrowser = fiComWebBrowser.GetValue(wb);

            if (objComWebBrowser == null) return;

            objComWebBrowser.GetType().InvokeMember(
            "Silent", BindingFlags.SetProperty, null, objComWebBrowser, new object[] { Hide });

        }




        [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("6d5140c1-7436-11ce-8034-00aa006009fa")]
        internal interface IServiceProvider
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            object QueryService(ref Guid guidService, ref Guid riid);
        }

        private void CloneUpdatesBTN_Click(object sender, RoutedEventArgs e)
        {
            //Duplicate Updates Across The Board

            // MessageBox.Show(vm.WhichTab);

            //Clear Collection
            vmMain.UserControlCollection.Clear();

            //Load collection with all instances
            vmMain.UserControlCollection.Add(vmMain.CurrentView1);
            vmMain.UserControlCollection.Add(vmMain.CurrentView2);
            vmMain.UserControlCollection.Add(vmMain.CurrentView3);
            vmMain.UserControlCollection.Add(vmMain.CurrentView4);
            vmMain.UserControlCollection.Add(vmMain.CurrentView5);
            vmMain.UserControlCollection.Add(vmMain.CurrentView6);
            vmMain.UserControlCollection.Add(vmMain.CurrentView7);
            vmMain.UserControlCollection.Add(vmMain.CurrentView8);
            vmMain.UserControlCollection.Add(vmMain.CurrentView9);
            vmMain.UserControlCollection.Add(vmMain.CurrentView10);
            vmMain.UserControlCollection.Add(vmMain.CurrentView11);
            vmMain.UserControlCollection.Add(vmMain.CurrentView12);
            vmMain.UserControlCollection.Add(vmMain.CurrentView13);
            vmMain.UserControlCollection.Add(vmMain.CurrentView14);
            vmMain.UserControlCollection.Add(vmMain.CurrentView15);
            vmMain.UserControlCollection.Add(vmMain.CurrentView16);



            //Loop through collection
            foreach(DellServerView itm in vmMain.UserControlCollection)
            {
                if(itm.vm.DellServerS1 != null)
                {
                    //Update inner vm with select file name
                    itm.vm.BIOSUpgradePath1 = vm.BIOSUpgradePath1;
                    itm.vm.IDRACUpgradePath1 = vm.IDRACUpgradePath1;
                    itm.vm.CONTUpgradePath1 = vm.CONTUpgradePath1;
                    itm.vm.CONT2UpgradePath1 = vm.CONT2UpgradePath1;

                    //Check if update file exists and make a copy of the update on the other servers
                 
                   //BIOS
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + itm.vm.Server1IPv4 + @"BIOSUpdate.exe"))
                    {
                           //delete file before attempting a copy
                        
                            
                    }
                    else
                    {
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe"))
                            File.Copy(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + itm.vm.Server1IPv4 + @"BIOSUpdate.exe");
                    }


                    //IDRAC EXE
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + itm.vm.Server1IPv4 + @"IDRACUpdate.exe"))
                    {


                    }
                    else
                    {
                        //Copy the Update file and rename with cloned server ip
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe"))
                            File.Copy(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + itm.vm.Server1IPv4 + @"IDRACUpdate.exe");
                    }



                    //IDRAC D7
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + itm.vm.Server1IPv4 + @"IDRACUpdate.d7"))
                    {


                    }
                    else
                    {
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7"))                        //Copy the Update file and rename with cloned server ip
                            File.Copy(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + itm.vm.Server1IPv4 + @"IDRACUpdate.d7");
                    }


                    //IDRAC D9
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + itm.vm.Server1IPv4 + @"IDRACUpdate.d9"))
                    {


                    }
                    else
                    {
                        //Copy the Update file and rename with cloned server ip
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9"))
                            File.Copy(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + itm.vm.Server1IPv4 + @"IDRACUpdate.d9");
                    }

                    //CONTROLLER
                    //CONTROLLER1
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + itm.vm.Server1IPv4 + @"CONTUpdate.exe"))
                    {


                    }
                    else
                    {
                        //Copy the Update file and rename with cloned server ip
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe"))
                            File.Copy(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + itm.vm.Server1IPv4 + @"CONTUpdate.exe");
                    }

                    //CONTROLLER2

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + itm.vm.Server1IPv4 + @"CONT2Update.exe"))
                    {


                    }
                    else
                    {
                        //Copy the Update file and rename with cloned server ip
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe"))
                            File.Copy(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + itm.vm.Server1IPv4 + @"CONT2Update.exe");
                    }

                    ////RUN Update Scripts
                    //itm.vm.GetDellServerDataCommand.Execute(new String[] { "Combined Updates", "Dell" });


                }
                // MessageBox.Show(itm.vm.DellServerS1.ServiceTag);
            }
        

        }



        private void RunUpdatesBTN_Click(object sender, RoutedEventArgs e)
        {

            //Run Wipe Scripts
            RunScripts("Combined Updates", "Dell");

        }

        private void ServerIPTXTB1_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //RUN Server Info Scripts
                vm.GetDellServerDataCommand.Execute(new String[] { "ServerInfo", "Dell" });
                             
            }
        }


       public void RunScripts(string Option, string Manufacturer)
        {

            try
            {

         
            //Clear Collection
            vmMain.UserControlCollection.Clear();

            //Load collection with all instances
            vmMain.UserControlCollection.Add(vmMain.CurrentView1);
            vmMain.UserControlCollection.Add(vmMain.CurrentView2);
            vmMain.UserControlCollection.Add(vmMain.CurrentView3);
            vmMain.UserControlCollection.Add(vmMain.CurrentView4);
            vmMain.UserControlCollection.Add(vmMain.CurrentView5);
            vmMain.UserControlCollection.Add(vmMain.CurrentView6);
            vmMain.UserControlCollection.Add(vmMain.CurrentView7);
            vmMain.UserControlCollection.Add(vmMain.CurrentView8);
            vmMain.UserControlCollection.Add(vmMain.CurrentView9);
            vmMain.UserControlCollection.Add(vmMain.CurrentView10);
            vmMain.UserControlCollection.Add(vmMain.CurrentView11);
            vmMain.UserControlCollection.Add(vmMain.CurrentView12);
            vmMain.UserControlCollection.Add(vmMain.CurrentView13);
            vmMain.UserControlCollection.Add(vmMain.CurrentView14);
            vmMain.UserControlCollection.Add(vmMain.CurrentView15);
            vmMain.UserControlCollection.Add(vmMain.CurrentView16);

         
          

            //Loop through collection
            foreach (DellServerView itm in vmMain.UserControlCollection)
            {
                if (itm.vm.DellServerS1 != null)
                {

                    //RUN Update Scripts
                    itm.vm.GetDellServerDataCommand.Execute(new String[] { Option, Manufacturer });

                }
                // MessageBox.Show(itm.vm.DellServerS1.ServiceTag);
            }

            }
            catch (Exception ex)
            {
                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (vm.WarningMessage1 != "")
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Green;
                    }

                }
            }

        }

        private void RunGetServerInfo(string Option, string Manufacturer)
        {
            try
            {


                //Clear Collection
                vmMain.UserControlCollection.Clear();

                //Load collection with all instances
                vmMain.UserControlCollection.Add(vmMain.CurrentView1);
                vmMain.UserControlCollection.Add(vmMain.CurrentView2);
                vmMain.UserControlCollection.Add(vmMain.CurrentView3);
                vmMain.UserControlCollection.Add(vmMain.CurrentView4);
                vmMain.UserControlCollection.Add(vmMain.CurrentView5);
                vmMain.UserControlCollection.Add(vmMain.CurrentView6);
                vmMain.UserControlCollection.Add(vmMain.CurrentView7);
                vmMain.UserControlCollection.Add(vmMain.CurrentView8);
                vmMain.UserControlCollection.Add(vmMain.CurrentView9);
                vmMain.UserControlCollection.Add(vmMain.CurrentView10);
                vmMain.UserControlCollection.Add(vmMain.CurrentView11);
                vmMain.UserControlCollection.Add(vmMain.CurrentView12);
                vmMain.UserControlCollection.Add(vmMain.CurrentView13);
                vmMain.UserControlCollection.Add(vmMain.CurrentView14);
                vmMain.UserControlCollection.Add(vmMain.CurrentView15);
                vmMain.UserControlCollection.Add(vmMain.CurrentView16);

                //int TabID = 0;

                //Loop through collection
                foreach (DellServerView itm in vmMain.UserControlCollection)
                {

                    if (itm.vm.IsScriptRunningS1 != true && !string.IsNullOrEmpty(vm.Server1IPv4))
                    {
                 

                        //RUN Update Scripts
                        itm.vm.GetDellServerDataCommand.Execute(new String[] { Option, Manufacturer });

                    }

                    // MessageBox.Show(itm.vm.DellServerS1.ServiceTag);
                }


            }
            catch (Exception ex)
            {

                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (vm.WarningMessage1 != "")
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Green;
                    }

                }
            }
        }

       




        private void GetInfoBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
             

                //Run get server info across all active servers
                RunGetServerInfo("ServerInfo", "Dell");

                
            }
            catch (Exception ex)
            {
                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (vm.WarningMessage1 != "")
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Green;
                    }

                }
            }
        }

        private void CloseOverlayBTN_Click(object sender, RoutedEventArgs e)
        {
            //Hide overlay message
            vm.ShowOverlay = Visibility.Collapsed;

        }

        private void ServerIPTXTB1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void OPS1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void OPS1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //RUN Server Info Scripts
                vm.GetDellServerDataCommand.Execute(new String[] { "ServerInfo", "Dell" });

            }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            //RedfishRestful redFish = new RedfishRestful();
            //await redFish.RedfishActionCall("https://" + vm.Server1IPv4 + "/redfish/v1/UpdateService/Actions/UpdateService.SimpleUpdate", HttpMethod.Post, "root:calvin", vm, "{\"ImageURI\": \"http://10.3.5.10:9555/api/firmware\",\"TransferProtocol\": \"HTTP\"}");

        }

        private void RunFinalAllScripts_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Run Wipe Scripts
                RunScripts("FinalScripts", "Dell");
            }
            catch (Exception ex)
            {
                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (vm.WarningMessage1 != "")
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Green;
                    }

                }
            }
        }

        private void ChkFWBTN_Click(object sender, RoutedEventArgs e)
        {
            //Check Firmware
            FirmwareUpdateCheck();
        }

        public void FirmwareUpdateCheck()
        {
            string bios = vm.BIOSUpgradePath1?.Replace(".EXE", "").Replace(".exe", "");

            string cpld = vm.CPLDUpgradePath1?.Replace(".EXE", "").Replace(".exe", "");

            string controller1 = vm.CONTUpgradePath1?.Replace(".EXE", "").Replace(".exe", "");

            string controller2 = vm.CONT2UpgradePath1?.Replace(".EXE", "").Replace(".exe", "");

            string idrac = vm.IDRACUpgradePath1?.Replace(".d7", "").Replace(".d8", "").Replace(".d9", "").Replace(".d10", "");

            vm.WarningMessage1 = "";

            //vm.DellServerS1 = new DELLServerInfo();
            //vm.DellServerS1.ControllerVersion = "25.5.9.0001";

            //bios
            if (!string.IsNullOrEmpty(bios) && !string.IsNullOrEmpty(vm.DellServerS1.CPLDVersion))
            {
                var biosArray = bios.Split('-');
                bool biosMatch = false;

                //Loop through array and find a match
                foreach (var arrayStr in biosArray)
                {
                    Console.WriteLine(arrayStr);
                    if (vm.DellServerS1 != null)
                        if (vm.DellServerS1.BiosCurrentVersion == arrayStr)
                        {
                            //As soon as a match is found exit
                            biosMatch = true;
                            break;

                        }
                }
                //Check if match was found
                if (biosMatch == true)
                {
                    //Clear Update
                    vm.BIOSUpgradePath1 = String.Empty;

                    //CLear Flash fie
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"BIOSUpdate.exe");
                    }

                    vm.UpdatesRun += @" [BIOS] ";
                }
                else
                {
                    vm.WarningMessage1 += "[Firmware " + vm.BIOSUpgradePath1 + " failed! Please rerun the update.]";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.BIOSUpgradePath1 + " failed to apply!!! Please rerun the update.");
                }


            }
            else
            {
                if(vm.DellServerS1 != null)
                if (string.IsNullOrEmpty(bios) && !string.IsNullOrEmpty(vm.DellServerS1.BiosCurrentVersion))
                {

                }
                else
                {
                    //vm.WarningMessage1 += "[Blank Server Field detected for Bios update.]";
                }
                    
            }


            //cpld
            if (vm.DellServerS1 != null)
            if (!string.IsNullOrEmpty(cpld) && !string.IsNullOrEmpty(vm.DellServerS1.CPLDVersion))
            {
                var cpldArray = cpld.Split('-');
                bool cpldMatch = false;
                //Loop through array and find a match
                foreach (var arrayStr in cpldArray)
                {
                    Console.WriteLine(arrayStr);
                    if (vm.DellServerS1 != null)
                        if (vm.DellServerS1.CPLDVersion == arrayStr)
                        {
                            //As soon as a match is found exit
                            cpldMatch = true;
                            break;

                        }
                }
                //Check if match was found
                if (cpldMatch == true)
                {
                    //Clear Update
                    vm.CPLDUpgradePath1 = String.Empty;

                    //CLear Flash fie
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CPLDUpdate.exe"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CPLDUpdate.exe");
                    }

                    vm.UpdatesRun += @" [CPLD] ";
                }
                else
                {
                    vm.WarningMessage1 += "[Firmware " + vm.CPLDUpgradePath1 + " failed! Please rerun the update.]";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.BIOSUpgradePath1 + " failed to apply!!! Please rerun the update.");
                }


            }
            else
            {
                if (vm.DellServerS1 != null)
                if (string.IsNullOrEmpty(cpld) && !string.IsNullOrEmpty(vm.DellServerS1.CPLDVersion))
                {

                }
                else
                {
                    //vm.WarningMessage1 += "[Blank Server Field detected for CPLD update.]";
                }

                    
            }

            //Controller 1
            if (vm.DellServerS1 != null)
            if (!string.IsNullOrEmpty(controller1) && !string.IsNullOrEmpty(vm.DellServerS1.ControllerVersion))
            {
                //Split update into array
                var cont1Array = controller1.Split('-');
                bool cont1Match = false;
                //Loop through array and find a match
                foreach (var arrayStr in cont1Array)
                {

                    Console.WriteLine(arrayStr);
                    if (vm.DellServerS1 != null)
                        if (vm.DellServerS1.ControllerVersion.Contains(arrayStr))
                        {
                            //As soon as a match is found exit
                            cont1Match = true;
                            break;
                        }//Check the other way round
                        else if(arrayStr.Contains(vm.DellServerS1.ControllerVersion))
                        {
                            //As soon as a match is found exit
                            cont1Match = true;
                            break;
                        }

                }

                //Check if match was found
                if (cont1Match == true)
                {
                    //Clear Update 
                    vm.CONTUpgradePath1 = String.Empty;

                    //CLear Flash fie
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONTUpdate.exe");
                    }

                    vm.UpdatesRun += @" [CONTROLLER] ";
                }
                else
                {
                    vm.WarningMessage1 += "[Firmware " + vm.CONTUpgradePath1 + " failed! Please rerun the update.]";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.CONTUpgradePath1 + " failed to apply!!! Please rerun the update.");
                }

            }
            else
            {
               if (vm.DellServerS1 != null)
               if (string.IsNullOrEmpty(controller2) && !string.IsNullOrEmpty(vm.DellServerS1.ControllerVersion))
                {

                }
                else
                {
                    //vm.WarningMessage1 += "[Blank Server Field detected for Integrated Controller update.]";
                }
                
            }
            //Controller 2
            if (vm.DellServerS1 != null)
            if (!string.IsNullOrEmpty(controller2) && !string.IsNullOrEmpty(vm.DellServerS1.ControllerVersion2))
            {
                //Split update into array
                var cont2Array = controller2.Split('-');
                bool cont2Match = false;
                //Loop through array and find a match
                foreach (var arrayStr in cont2Array)
                {
                    Console.WriteLine(arrayStr);
                   
                        if (vm.DellServerS1 != null)
                            if (vm.DellServerS1.ControllerVersion2.Contains(arrayStr))
                            {
                                //As soon as a match is found exit
                                cont2Match = true;
                                break;
                            }//Check the other way round
                            else if (arrayStr.Contains(vm.DellServerS1.ControllerVersion2))
                            {
                                //As soon as a match is found exit
                                cont2Match = true;
                                break;
                            }



                    }

                //Check if match was found
                if (cont2Match == true)
                {
                    //Clear Update 
                    vm.CONT2UpgradePath1 = String.Empty;

                    //CLear Flash fie
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"CONT2Update.exe");
                    }
                    //Update Certificate Field
                    vm.UpdatesRun += @" [CONTROLLER] ";
                }
                else
                {
                    vm.WarningMessage1 += "[Firmware " + vm.CONT2UpgradePath1 + " failed! Please rerun the update.]";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.CONT2UpgradePath1 + " failed to apply!!! Please rerun the update.");
                }
            }
            else
            {
                if (vm.DellServerS1 != null)
                if (string.IsNullOrEmpty(controller2) && !string.IsNullOrEmpty(vm.DellServerS1.ControllerVersion2))
                {

                }
                else
                {
                    //vm.WarningMessage1 += "[Blank Server Field detected for Additional Controller update.]";
                }
                    
            }

            //Idrac
            if (vm.DellServerS1 != null)
            if (!string.IsNullOrEmpty(idrac) && !string.IsNullOrEmpty(vm.DellServerS1.iDRACVersion))
            {
                var idracArray = idrac.Split('-');
                bool idracMatch = false;
                //Loop through array and find a match
                foreach (var arrayStr in idracArray)
                {
                    Console.WriteLine(arrayStr);
                    if (vm.DellServerS1 != null)
                        if (vm.DellServerS1.iDRACVersion == arrayStr)
                        {
                            //As soon as a match is found exit
                            idracMatch = true;
                            break;

                        }
                }

                //Check if match was found
                if (idracMatch == true)
                {


                    //CLEAR Flash File INFORMATION
                    vm.IDRACUpgradePath1 = String.Empty;

                    //IDRAC
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.exe");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d7");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\" + vm.Server1IPv4 + @"IDRACUpdate.d9");
                    }
                    //for test certificate
                    vm.UpdatesRun += @" [IDRAC] ";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.IDRACUpgradePath1 + " has updated successfully!");
                }
                else
                {
                    vm.WarningMessage1 += "[Firmware " + vm.IDRACUpgradePath1 + " failed! Please rerun the update.]";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.IDRACUpgradePath1 + " failed to apply!!! Please rerun the update.");
                }
            }
            else
            {
                if (vm.DellServerS1 != null)
                if (string.IsNullOrEmpty(idrac) && !string.IsNullOrEmpty(vm.DellServerS1.iDRACVersion))
                {

                }
                else
                {
                    //vm.WarningMessage1 += "[Blank Server Field detected for iDrac update.]";
                }
                
            }

        }
    }
}
