﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.ViewModels;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for DellReportsView.xaml
    /// </summary>
    public partial class DellReportsView : UserControl
    {

        MainViewModel vm;    //

        public DellReportsView()
        {
            InitializeComponent();
        }

        public DellReportsView(MainViewModel passedviewmodel)
        {
            InitializeComponent();

            vm = passedviewmodel;

            this.DataContext = vm;


         


            //Show Goods In or Goods Out Report
            if (StaticFunctions.Department == "GoodsOutTech" || StaticFunctions.Department == "GoodsIn" || StaticFunctions.Department.Contains("TestDept") || StaticFunctions.Department.Contains("ITAD"))
            {

                //Clear if already added
                if (DELLServerReportGOTS1.Blocks.Count > 1)
                {
                    DELLServerReportGOTS1.Blocks.Remove(DELLServerReportGOTS1.Blocks.LastBlock);
                }
                vm.DellServerS1.TestedBy = vm.CurrentUser;

                FDViewerS1.Visibility = Visibility.Visible;
                vm.DELLGenerateReport2(DELLServerReportGOTS1, vm.DellServerS1);

                //Set FlowDocument for printing Test Report
                vm.FlowDocToPrint = DELLServerReportGOTS1;
            }
          

        }
    }
}
