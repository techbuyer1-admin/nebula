﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.ViewModels;
using Nebula.Properties;
using Nebula.Models;
using Nebula.Helpers;
using System.IO;


namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for GoodsOutDistributionToolsView.xaml
    /// </summary>
    public partial class GoodsOutDistributionToolsView : UserControl
    {
        public GoodsOutDistributionToolsView()
        {
            InitializeComponent();
        }

        //declare viewmodel
        MainViewModel vm;     //
      

        public GoodsOutDistributionToolsView(MainViewModel PassedViewModel)
        {
            InitializeComponent();
            //get the passed in viewmodel and assign to the local vm variable
            vm = PassedViewModel;
        }
        private void ZipDocsBTN_Click(object sender, RoutedEventArgs e)
        {
          

            //  using (System.Windows.Forms.FolderBrowserDialog openFolderDialog = new System.Windows.Forms.FolderBrowserDialog())
            //{

            //    //Requires GUID for MyComputer Or ThisPC Folder
            //    //openFileDialog.InitialDirectory = "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}"; //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
            //    //openFileDialog.Filter = "All files (*.*)|*.*";
            //    //openFileDialog.FilterIndex = 0;
            //    //openFileDialog.Multiselect = true;
            //    //openFileDialog.RestoreDirectory = true;

            //   openFolderDialog.RootFolder = Environment.SpecialFolder.MyComputer;


            //    string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");

            //    //MessageBox.Show(Environment.SpecialFolder.MyDocuments.ToString());

            //    //if (openFolderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //    //{


            //    //   // MessageBox.Show(openFolderDialog.SelectedPath);

            //    //    ZipClass.CreateZip(openFolderDialog.SelectedPath, openFolderDialog.SelectedPath);

            //    //}







            //}
        }

                

       private void EmailDocsBTN_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SendEmailBTN_Click(object sender, RoutedEventArgs e)
        {
            //Get the directory for drives
            string mydocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            //Get the root path location from current date
            // string rootpath = vm.GetFolderPath(DateTime.Now.ToShortDateString());
            //rootpath = mydocs + @"\";
            //MessageBox.Show(drives);


            EmailClass email = new EmailClass(vm);
            email.ZipFolderAndSendMail();
        }
    }
}
