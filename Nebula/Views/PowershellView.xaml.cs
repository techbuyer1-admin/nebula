﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for PowershellView.xaml
    /// </summary>
    public partial class PowershellView : UserControl
    {
        public PowershellView()
        {
            InitializeComponent();
        }

        private void RunCommand_Click(object sender, RoutedEventArgs e)
        {

            //https://www.youtube.com/watch?v=X6ClzyoxVOw

            //            Function Set-Properties($w,$h)
            //            {
            //             $textbox.height =$h
            //             $textbox.width =$h

            //             }

            //            Set - Properties 400 300


            //            $textbox.text = "hello world"

            //Runspace rs = RunspaceFactory.CreateRunspace();
            //rs.ThreadOptions = PSThreadOptions.UseCurrentThread;
            //rs.Open();
            //rs.SessionStateProxy.SetVariable("textbox", this.PowershellTXTB2);


            //PowerShell ps = PowerShell.Create();
            //ps.Runspace = rs;

            //ps.AddScript(PoweshellTXTB1.Text);
            //ps.Invoke();


            //rs.Close();

            PowershellTXTB2.Text = RunScript(PowershellTXTB1.Text);


        }



        private string RunScript(string scriptText)
        {
            // create Powershell runspace

            Runspace runspace = RunspaceFactory.CreateRunspace();

            // open it

            runspace.Open();

            runspace.SessionStateProxy.SetVariable("textbox", this.PowershellTXTB2);

            // create a pipeline and feed it the script text

            Pipeline pipeline = runspace.CreatePipeline();
            pipeline.Commands.AddScript(scriptText);

            // add an extra command to transform the script
            // output objects into nicely formatted strings

            // remove this line to get the actual objects
            // that the script returns. For example, the script

            // "Get-Process" returns a collection
            // of System.Diagnostics.Process instances.

            pipeline.Commands.Add("Out-String");

            // execute the script
            if(scriptText != string.Empty)
            {
                Collection<PSObject> results = pipeline.Invoke();

                // close the runspace

                runspace.Close();

                // convert the script result into a single string

                StringBuilder stringBuilder = new StringBuilder();
                foreach (PSObject obj in results)
                {
                    stringBuilder.AppendLine(obj.ToString());
                }
                return stringBuilder.ToString();
            }
            else
            {
                return "No Command Entered";
            }
           

         
        }

    }
}
