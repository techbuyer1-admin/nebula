﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Nebula.ViewModels;
using Nebula.Models;
using Nebula.Views;
using MahApps.Metro.Controls;
using Nebula.Commands;
using System.IO;
using Nebula.Paginators;
using System.Windows.Media;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Net.Security;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Net.Http;
using Nebula.CiscoServers;
using Nebula.Helpers;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for CiscoMultiServerView.xaml
    /// </summary>
    public partial class CiscoMultiServerView : UserControl
    {


        //declare viewmodel
        MainViewModel vm;     //

        public CiscoMultiServerView()
        {
            InitializeComponent();


        }




        public CiscoMultiServerView(MainViewModel PassedViewModel, bool isGOT)
        {
            InitializeComponent();
            //get the passed in viewmodel and assign to the local vm variable
            vm = PassedViewModel;

            if (isGOT == true)
            {
                //Goods Out Technical
                vm.CurrentView1 = new CiscoServerView(vm, "1");
                vm.CurrentView2 = new CiscoServerView(vm, "2");
                vm.CurrentView3 = new CiscoServerView(vm, "3");
                vm.CurrentView4 = new CiscoServerView(vm, "4");
                vm.CurrentView5 = new CiscoServerView(vm, "5");
                vm.CurrentView6 = new CiscoServerView(vm, "6");
                vm.CurrentView7 = new CiscoServerView(vm, "7");
                vm.CurrentView8 = new CiscoServerView(vm, "8");
                vm.CurrentView9 = new CiscoServerView(vm, "9");
                vm.CurrentView10 = new CiscoServerView(vm, "10");
                vm.CurrentView11 = new CiscoServerView(vm, "11");
                vm.CurrentView12 = new CiscoServerView(vm, "12");
                vm.CurrentView13 = new CiscoServerView(vm, "13");
                vm.CurrentView14 = new CiscoServerView(vm, "14");
                vm.CurrentView15 = new CiscoServerView(vm, "15");
                vm.CurrentView16 = new CiscoServerView(vm, "16");
            
            }
            else
            {
                //Goods in
                vm.CurrentView1 = new CiscoServerView(vm, "1");
                vm.CurrentView2 = new CiscoServerView(vm, "2");
                vm.CurrentView3 = new CiscoServerView(vm, "3");
                vm.CurrentView4 = new CiscoServerView(vm, "4");
                vm.CurrentView5 = new CiscoServerView(vm, "5");
                vm.CurrentView6 = new CiscoServerView(vm, "6");
                vm.CurrentView7 = new CiscoServerView(vm, "7");
                vm.CurrentView8 = new CiscoServerView(vm, "8");
                vm.CurrentView9 = new CiscoServerView(vm, "9");
                vm.CurrentView10 = new CiscoServerView(vm, "10");
                vm.CurrentView11 = new CiscoServerView(vm, "11");
                vm.CurrentView12 = new CiscoServerView(vm, "12");
                vm.CurrentView13 = new CiscoServerView(vm, "13");
                vm.CurrentView14 = new CiscoServerView(vm, "14");
                vm.CurrentView15 = new CiscoServerView(vm, "15");
                vm.CurrentView16 = new CiscoServerView(vm, "16");
            }




            //CLEAR SERVER PROPERTIES ON LOAD
            //reset flipview to start
            //FlipView1.SelectedIndex = 0;
            //delete server object
            //vm.CiscoServerS1 = null;
            //// tab header set back to default
            //vm.Server1TabHeader = "Server1";
            //clear server ip
            //vm.Server1IPv4 = string.Empty;





            vm.OverridePasswordS1 = "PASSW0RD";






            // ADD THIS TO GET SERVER INFO SCRIPT


        }



        public static void AddDocument(FlowDocument from, FlowDocument to)
        {
            TextRange range = new TextRange(from.ContentStart, from.ContentEnd);
            MemoryStream stream = new MemoryStream();
            System.Windows.Markup.XamlWriter.Save(range, stream);
            range.Save(stream, DataFormats.XamlPackage);
            TextRange range2 = new TextRange(to.ContentEnd, to.ContentEnd);
            range2.Load(stream, DataFormats.XamlPackage);
        }


        private void DoThePrint(System.Windows.Documents.FlowDocument document)
        {
            // Clone the source document's content into a new FlowDocument.
            // This is because the pagination for the printer needs to be
            // done differently than the pagination for the displayed page.
            // We print the copy, rather that the original FlowDocument.
            System.IO.MemoryStream s = new System.IO.MemoryStream();
            TextRange source = new TextRange(document.ContentStart, document.ContentEnd);
            source.Save(s, DataFormats.Xaml);
            FlowDocument copy = new FlowDocument();
            TextRange dest = new TextRange(copy.ContentStart, copy.ContentEnd);
            dest.Load(s, DataFormats.Xaml);

            // Create a XpsDocumentWriter object, implicitly opening a Windows common print dialog,
            // and allowing the user to select a printer.

            // get information about the dimensions of the seleted printer+media.
            System.Printing.PrintDocumentImageableArea ia = null;
            System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);

            if (docWriter != null && ia != null)
            {
                DocumentPaginator paginator = ((IDocumentPaginatorSource)copy).DocumentPaginator;

                // Change the PageSize and PagePadding for the document to match the CanvasSize for the printer device.
                paginator.PageSize = new Size(ia.MediaSizeWidth, ia.MediaSizeHeight);
                Thickness t = new Thickness(72);  // copy.PagePadding;
                copy.PagePadding = new Thickness(
                                 Math.Max(ia.OriginWidth, t.Left),
                                   Math.Max(ia.OriginHeight, t.Top),
                                   Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
                                   Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));

                copy.ColumnWidth = double.PositiveInfinity;
                //copy.PageWidth = 528; // allow the page to be the natural with of the output device

                // Send content to the printer.
                docWriter.Write(paginator);
            }

        }





        private async Task CheckLCDDisplay(string ServerIP, string myDocs, bool OtherScript, string ServerThere, string ServerNumber)
        {

            ProcessPiper pp = new ProcessPiper(vm);


            if (ServerIP != string.Empty)
                while (true)
                {

                    if (OtherScript != true)
                    {


                        if (ServerThere == "Yes")
                        {


                            switch (ServerNumber)
                            {
                                case "S1":
                                    //get LCD display
                                    //MessageBox.Show("Yep");
                                    await System.Threading.Tasks.Task.Run(() => pp.StartSensorInventoryRACDM1(new System.IO.FileInfo(@"" + myDocs + @"\HPTOOLS\Cisco\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + ServerIP + @" -u root -p calvin --nocertwarn get System.LCD", @"" + myDocs + @"\HPTOOLS\Cisco\SysMgt\iDRACTools\racadm"));
                                    break;

                            }

                        }

                    }

                    //MessageBox.Show("Fire");
                    await Task.Delay(TimeSpan.FromSeconds(20));
                }
        }


        private async Task RefreshServerInfo(string ServerIP, string myDocs)
        {
            ProcessPiper pp = new ProcessPiper(vm);

            while (true)
            {
                if (vm.IsServerThere1 == "Yes")
                {
                    if (vm.IsScriptRunningS1 != true)
                    {
                        //MessageBox.Show("Fire1");
                        //acquire the server inventory 


                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDMGeneric1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Cisco\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p calvin --nocertwarn hwinventory export -f " + vm.Server1IPv4 + @"Inventory.xml", @"" + vm.myDocs + @"\HPTOOLS\Cisco\SysMgt\iDRACTools\racadm"));
                    }


                }

                //MessageBox.Show("Fire");
                await Task.Delay(TimeSpan.FromSeconds(80));
            }
        }





        private void SkipBTN1_Click(object sender, RoutedEventArgs e)
        {

            //reset flipview to start
            //FlipView1.SelectedIndex = 2;
        }




        private void CancelScriptS1_Click(object sender, RoutedEventArgs e)
        {
            vm.CancelScriptS1 = true;
        }




        static readonly Guid SID_SWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");





        private void CloseAppByPid(int processID)
        {
            try
            {
                Process proc = Process.GetProcessById(processID);
                if (proc.ProcessName.Contains("HPLOCONS"))
                {
                    proc.Kill();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }



        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

        private void LaunchWebPage1_Click(object sender, RoutedEventArgs e)
        {

        }






        [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("6d5140c1-7436-11ce-8034-00aa006009fa")]
        internal interface IServiceProvider
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            object QueryService(ref Guid guidService, ref Guid riid);
        }

        private void CPS2_GotFocus(object sender, RoutedEventArgs e)
        {

        }
    }



}


