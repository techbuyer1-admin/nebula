﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.ViewModels;
using Nebula.Properties;
using Nebula.Models;
using Nebula.Helpers;
using MahApps.Metro.Controls;
using Nebula.Commands;
using System.IO;
using Renci.SshNet;
using Renci.SshNet.Common;
using System.Collections.ObjectModel;
using System.Net.Http;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for HPEnclosureView.xaml
    /// </summary>
    public partial class HPEnclosureView : UserControl
    {
        //List<BladeItem> ipList = new List<BladeItem>();


        //declare viewmodel
        MainViewModel vm;
        //
        public HPEnclosureView()
        {
            InitializeComponent();
        }


        public HPEnclosureView(MainViewModel PassedViewModel)
        {
            InitializeComponent();
            //get the passed in viewmodel and assign to the local vm variable
            vm = PassedViewModel;
            //Reset Message
            vm.PartCodeMessage = "";


            //new the backing properties
            vm.BladeipList = new ObservableCollection<BladeItem>();
            vm.SelectedBladeipList = new ObservableCollection<BladeItem>();

            //IF IP PRESENT RUN THE CHECK
            if (string.IsNullOrWhiteSpace(vm.HPEnclosureIP))
            {

            }
            else
            {
                //vm.BladeipList.Add(new BladeItem { ServerIP = "192.168.101.115" });
                //vm.BladeipList.Add(new BladeItem { ServerIP = "192.168.101.116" });
                //vm.BladeipList.Add(new BladeItem { ServerIP = "192.168.101.117" });
                //Call checkenclosure method
                CheckEnclosure();
            }


            //vm.BladeipList.Add(new BladeItem { Bay = "1", ServerIP = "192.168.101.118" });
        }



        // public





        private void LoadServersBTN_Click(object sender, RoutedEventArgs e)
        {

            //MessageBox.Show("Here");
            //Get all selected items from the list view
            var selectedItems = EnclosureBaysLV.SelectedItems;
            foreach (BladeItem selectedItem in selectedItems)
            {
                // MessageBox.Show(selectedItem.ServerIP); ;
                vm.SelectedBladeipList.Add(selectedItem);


                if (vm.SelectedBladeipList.Count() > 0)
                {
                    // MessageBox.Show(vm.Server1IPv4);
                    //check if slot is free
                    if (vm.Server1IPv4 == "")
                    {
                        // MessageBox.Show("Empty Slot 1");

                        vm.Server1IPv4 = selectedItem.ServerIP;
                        vm.Server1TabHeader = "Server1 " + selectedItem.Bay;

                        //MessageBox.Show(vm.Server1IPv4);

                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {

                                case "HPGen8Blade":
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView1 = new HPG8ServerView(vm, "1", vm.Server1IPv4, true, "Server1 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //MessageBox.Show("Gen9 Blade");
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView1 = new HPG9ServerView(vm, "1", vm.Server1IPv4, true, "Server1 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView1 = new HPG10ServerView(vm, "1", vm.Server1IPv4, true, "Server1 " + selectedItem.Bay);
                                    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView1 = new HPG8ServerView(vm, "1", vm.Server1IPv4, true, "Server1 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //MessageBox.Show("Gen9 Blade");
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView1 = new HPG9ServerView(vm, "1", vm.Server1IPv4, true, "Server1 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView1 = new HPG10ServerView(vm, "1", vm.Server1IPv4, true, "Server1 " + selectedItem.Bay);
                                    break;
                            }
                        }


                    }
                    else if (vm.Server2IPv4 == "")
                    {
                        // MessageBox.Show("Empty Slot 2");
                        vm.Server2IPv4 = selectedItem.ServerIP;
                        vm.Server2TabHeader = "Server2 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {

                                case "HPGen8Blade":
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView2 = new HPG8ServerView(vm, "2", vm.Server2IPv4, true, "Server2 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView2 = new HPG9ServerView(vm, "2", vm.Server2IPv4, true, "Server2 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView2 = new HPG10ServerView(vm, "2", vm.Server2IPv4, true, "Server2 " + selectedItem.Bay);
                                    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView2 = new HPG8ServerView(vm, "2", vm.Server2IPv4, true, "Server2 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView2 = new HPG9ServerView(vm, "2", vm.Server2IPv4, true, "Server2 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView2 = new HPG10ServerView(vm, "2", vm.Server2IPv4, true, "Server2 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server3IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 3");
                        vm.Server3IPv4 = selectedItem.ServerIP;
                        vm.Server3TabHeader = "Server3 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {

                                case "HPGen8Blade":
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView3 = new HPG8ServerView(vm, "3", vm.Server3IPv4, true, "Server3 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView3 = new HPG9ServerView(vm, "3", vm.Server3IPv4, true, "Server3 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView3 = new HPG10ServerView(vm, "3", vm.Server3IPv4, true, "Server3 " + selectedItem.Bay);
                                    break;

                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView3 = new HPG8ServerView(vm, "3", vm.Server3IPv4, true, "Server3 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView3 = new HPG9ServerView(vm, "3", vm.Server3IPv4, true, "Server3 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView3 = new HPG10ServerView(vm, "3", vm.Server3IPv4, true, "Server3 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server4IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 4");
                        vm.Server4IPv4 = selectedItem.ServerIP;
                        vm.Server4TabHeader = "Server4 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView4 = new HPG8ServerView(vm, "4", vm.Server4IPv4, true, "Server4 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    // if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView4 = new HPG9ServerView(vm, "4", vm.Server4IPv4, true, "Server4 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    // if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView4 = new HPG10ServerView(vm, "4", vm.Server4IPv4, true, "Server4 " + selectedItem.Bay);
                                    break;
                                    //case "HPGen8Blade":
                                    //    //if (vm.Gen8ServerInfo1 == null)
                                    //    vm.CurrentView4 = new HPG8ServerViewGOT(vm, "4", vm.Server4IPv4, true, "Server4 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen9Blade":
                                    //    //if (vm.Gen9ServerInfo1 == null)
                                    //    vm.CurrentView4 = new HPG9ServerViewGOT(vm, "4", vm.Server4IPv4, true, "Server4 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen10Blade":
                                    //    //if (vm.Gen10ServerInfo1 == null)
                                    //    vm.CurrentView4 = new HPG10ServerViewGOT(vm, "4", vm.Server4IPv4, true, "Server4 " + selectedItem.Bay);
                                    //    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    // if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView4 = new HPG8ServerView(vm, "4", vm.Server4IPv4, true, "Server4 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    // if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView4 = new HPG9ServerView(vm, "4", vm.Server4IPv4, true, "Server4 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    // if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView4 = new HPG10ServerView(vm, "4", vm.Server4IPv4, true, "Server4 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server5IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 5");
                        vm.Server5IPv4 = selectedItem.ServerIP;
                        vm.Server5TabHeader = "Server5 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    // if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView5 = new HPG8ServerView(vm, "5", vm.Server5IPv4, true, "Server5 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    // if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView5 = new HPG9ServerView(vm, "5", vm.Server5IPv4, true, "Server5 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    // if (vm.GenGenericServerInfo1 == null)
                                    vm.CurrentView5 = new HPG10ServerView(vm, "5", vm.Server5IPv4, true, "Server5 " + selectedItem.Bay);
                                    break;
                                    //case "HPGen8Blade":
                                    //    //if (vm.Gen8ServerInfo1 == null)
                                    //    vm.CurrentView5 = new HPG8ServerViewGOT(vm, "5", vm.Server5IPv4, true, "Server5 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen9Blade":
                                    //    //if (vm.Gen9ServerInfo1 == null)
                                    //    vm.CurrentView5 = new HPG9ServerViewGOT(vm, "5", vm.Server5IPv4, true, "Server5 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen10Blade":
                                    //    //if (vm.Gen10ServerInfo1 == null)
                                    //    vm.CurrentView5 = new HPG10ServerViewGOT(vm, "5", vm.Server5IPv4, true, "Server5 " + selectedItem.Bay);
                                    //    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView5 = new HPG8ServerView(vm, "5", vm.Server5IPv4, true, "Server5 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView5 = new HPG9ServerView(vm, "5", vm.Server5IPv4, true, "Server5 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView5 = new HPG10ServerView(vm, "5", vm.Server5IPv4, true, "Server5 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server6IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 6");
                        vm.Server6IPv4 = selectedItem.ServerIP;
                        vm.Server6TabHeader = "Server6 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView6 = new HPG8ServerView(vm, "6", vm.Server6IPv4, true, "Server6 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView6 = new HPG9ServerView(vm, "6", vm.Server6IPv4, true, "Server6 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView6 = new HPG10ServerView(vm, "6", vm.Server6IPv4, true, "Server6 " + selectedItem.Bay);
                                    break;
                                    //case "HPGen8Blade":
                                    //    //if (vm.Gen8ServerInfo1 == null)
                                    //    vm.CurrentView6 = new HPG8ServerViewGOT(vm, "6", vm.Server6IPv4, true, "Server6 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen9Blade":
                                    //    //if (vm.Gen9ServerInfo1 == null)
                                    //    vm.CurrentView6 = new HPG9ServerViewGOT(vm, "6", vm.Server6IPv4, true, "Server6 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen10Blade":
                                    //    //if (vm.Gen10ServerInfo1 == null)
                                    //    vm.CurrentView6 = new HPG10ServerViewGOT(vm, "6", vm.Server6IPv4, true, "Server6 " + selectedItem.Bay);
                                    //    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView6 = new HPG8ServerView(vm, "6", vm.Server6IPv4, true, "Server6 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView6 = new HPG9ServerView(vm, "6", vm.Server6IPv4, true, "Server6 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView6 = new HPG10ServerView(vm, "6", vm.Server6IPv4, true, "Server6 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server7IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 7");
                        vm.Server7IPv4 = selectedItem.ServerIP;
                        vm.Server7TabHeader = "Server7 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView7 = new HPG8ServerView(vm, "7", vm.Server7IPv4, true, "Server7 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView7 = new HPG9ServerView(vm, "7", vm.Server7IPv4, true, "Server7 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView7 = new HPG10ServerView(vm, "7", vm.Server7IPv4, true, "Server7 " + selectedItem.Bay);
                                    break;
                                    //case "HPGen8Blade":
                                    //    //if (vm.Gen8ServerInfo1 == null)
                                    //    vm.CurrentView7 = new HPG8ServerViewGOT(vm, "7", vm.Server7IPv4, true, "Server7 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen9Blade":
                                    //    //if (vm.Gen9ServerInfo1 == null)
                                    //    vm.CurrentView7 = new HPG9ServerViewGOT(vm, "7", vm.Server7IPv4, true, "Server7 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen10Blade":
                                    //    //if (vm.Gen10ServerInfo1 == null)
                                    //    vm.CurrentView7 = new HPG10ServerViewGOT(vm, "7", vm.Server7IPv4, true, "Server7 " + selectedItem.Bay);
                                    //    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView7 = new HPG8ServerView(vm, "7", vm.Server7IPv4, true, "Server7 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView7 = new HPG9ServerView(vm, "7", vm.Server7IPv4, true, "Server7 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView7 = new HPG10ServerView(vm, "7", vm.Server7IPv4, true, "Server7 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server8IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 8");
                        vm.Server8IPv4 = selectedItem.ServerIP;
                        vm.Server8TabHeader = "Server8 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView8 = new HPG8ServerView(vm, "8", vm.Server8IPv4, true, "Server8 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView8 = new HPG9ServerView(vm, "8", vm.Server8IPv4, true, "Server8 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView8 = new HPG10ServerView(vm, "8", vm.Server8IPv4, true, "Server8 " + selectedItem.Bay);
                                    break;
                                    //case "HPGen8Blade":
                                    //    //if (vm.Gen8ServerInfo1 == null)
                                    //    vm.CurrentView8 = new HPG8ServerViewGOT(vm, "8", vm.Server8IPv4, true, "Server8 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen9Blade":
                                    //    //if (vm.Gen9ServerInfo1 == null)
                                    //    vm.CurrentView8 = new HPG9ServerViewGOT(vm, "8", vm.Server8IPv4, true, "Server8 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen10Blade":
                                    //    //if (vm.Gen10ServerInfo1 == null)
                                    //    vm.CurrentView8 = new HPG10ServerViewGOT(vm, "8", vm.Server8IPv4, true, "Server8 " + selectedItem.Bay);
                                    //    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView8 = new HPG8ServerView(vm, "8", vm.Server8IPv4, true, "Server8 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView8 = new HPG9ServerView(vm, "8", vm.Server8IPv4, true, "Server8 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView8 = new HPG10ServerView(vm, "8", vm.Server8IPv4, true, "Server8 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server9IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 9");
                        vm.Server9IPv4 = selectedItem.ServerIP;
                        vm.Server9TabHeader = "Server9 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView9 = new HPG8ServerView(vm, "9", vm.Server9IPv4, true, "Server9 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView9 = new HPG9ServerView(vm, "9", vm.Server9IPv4, true, "Server9 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView9 = new HPG10ServerView(vm, "9", vm.Server9IPv4, true, "Server9 " + selectedItem.Bay);
                                    break;
                                    //case "HPGen8Blade":
                                    //    //if (vm.Gen8ServerInfo1 == null)
                                    //    vm.CurrentView9 = new HPG8ServerViewGOT(vm, "9", vm.Server9IPv4, true, "Server9 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen9Blade":
                                    //    //if (vm.Gen9ServerInfo1 == null)
                                    //    vm.CurrentView9 = new HPG9ServerViewGOT(vm, "9", vm.Server9IPv4, true, "Server9 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen10Blade":
                                    //    //if (vm.Gen10ServerInfo1 == null)
                                    //    vm.CurrentView9 = new HPG10ServerViewGOT(vm, "9", vm.Server9IPv4, true, "Server9 " + selectedItem.Bay);
                                    //    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView9 = new HPG8ServerView(vm, "9", vm.Server9IPv4, true, "Server9 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView9 = new HPG9ServerView(vm, "9", vm.Server9IPv4, true, "Server9 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView9 = new HPG10ServerView(vm, "9", vm.Server9IPv4, true, "Server9 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server10IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 10");
                        vm.Server10IPv4 = selectedItem.ServerIP;
                        vm.Server10TabHeader = "Server10 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView10 = new HPG8ServerView(vm, "10", vm.Server10IPv4, true, "Server10 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView10 = new HPG9ServerView(vm, "10", vm.Server10IPv4, true, "Server10 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView10 = new HPG10ServerView(vm, "10", vm.Server10IPv4, true, "Server10 " + selectedItem.Bay);
                                    break;
                                    //case "HPGen8Blade":
                                    //    //if (vm.Gen8ServerInfo1 == null)
                                    //    vm.CurrentView10 = new HPG8ServerViewGOT(vm, "10", vm.Server10IPv4, true, "Server10 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen9Blade":
                                    //    //if (vm.Gen9ServerInfo1 == null)
                                    //    vm.CurrentView10 = new HPG9ServerViewGOT(vm, "10", vm.Server10IPv4, true, "Server10 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen10Blade":
                                    //    //if (vm.Gen10ServerInfo1 == null)
                                    //    vm.CurrentView10 = new HPG10ServerViewGOT(vm, "10", vm.Server10IPv4, true, "Server10 " + selectedItem.Bay);
                                    //    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView10 = new HPG8ServerView(vm, "10", vm.Server10IPv4, true, "Server10 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView10 = new HPG9ServerView(vm, "10", vm.Server10IPv4, true, "Server10 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView10 = new HPG10ServerView(vm, "10", vm.Server10IPv4, true, "Server10 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server11IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 11");
                        vm.Server11IPv4 = selectedItem.ServerIP;
                        vm.Server11TabHeader = "Server11 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView11 = new HPG8ServerView(vm, "11", vm.Server11IPv4, true, "Server11 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView11 = new HPG9ServerView(vm, "11", vm.Server11IPv4, true, "Server11 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView11 = new HPG10ServerView(vm, "11", vm.Server11IPv4, true, "Server11 " + selectedItem.Bay);
                                    break;
                                    //case "HPGen8Blade":
                                    //    //if (vm.Gen8ServerInfo1 == null)
                                    //    vm.CurrentView11 = new HPG8ServerViewGOT(vm, "11", vm.Server11IPv4, true, "Server11 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen9Blade":
                                    //    //if (vm.Gen9ServerInfo1 == null)
                                    //    vm.CurrentView11 = new HPG9ServerViewGOT(vm, "11", vm.Server11IPv4, true, "Server11 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen10Blade":
                                    //    //if (vm.Gen10ServerInfo1 == null)
                                    //    vm.CurrentView11 = new HPG10ServerViewGOT(vm, "11", vm.Server11IPv4, true, "Server11 " + selectedItem.Bay);
                                    //    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView11 = new HPG8ServerView(vm, "11", vm.Server11IPv4, true, "Server11 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView11 = new HPG9ServerView(vm, "11", vm.Server11IPv4, true, "Server11 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView11 = new HPG10ServerView(vm, "11", vm.Server11IPv4, true, "Server11 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server12IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 12");
                        vm.Server12IPv4 = selectedItem.ServerIP;
                        vm.Server12TabHeader = "Server12 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView12 = new HPG8ServerView(vm, "12", vm.Server12IPv4, true, "Server12 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView12 = new HPG9ServerView(vm, "12", vm.Server12IPv4, true, "Server12 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView12 = new HPG10ServerView(vm, "12", vm.Server12IPv4, true, "Server12 " + selectedItem.Bay);
                                    break;
                                    //case "HPGen8Blade":
                                    //    //if (vm.Gen8ServerInfo1 == null)
                                    //    vm.CurrentView12 = new HPG8ServerViewGOT(vm, "12", vm.Server12IPv4, true, "Server12 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen9Blade":
                                    //    //if (vm.Gen9ServerInfo1 == null)
                                    //    vm.CurrentView12 = new HPG9ServerViewGOT(vm, "12", vm.Server12IPv4, true, "Server12 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen10Blade":
                                    //    //if (vm.Gen10ServerInfo1 == null)
                                    //    vm.CurrentView12 = new HPG10ServerViewGOT(vm, "12", vm.Server12IPv4, true, "Server12 " + selectedItem.Bay);
                                    //    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView12 = new HPG8ServerView(vm, "12", vm.Server12IPv4, true, "Server12 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView12 = new HPG9ServerView(vm, "12", vm.Server12IPv4, true, "Server12 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView12 = new HPG10ServerView(vm, "12", vm.Server12IPv4, true, "Server12 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server13IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 13");
                        vm.Server13IPv4 = selectedItem.ServerIP;
                        vm.Server13TabHeader = "Server13 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView13 = new HPG8ServerView(vm, "13", vm.Server13IPv4, true, "Server13 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView13 = new HPG9ServerView(vm, "13", vm.Server13IPv4, true, "Server13 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView13 = new HPG10ServerView(vm, "13", vm.Server13IPv4, true, "Server13 " + selectedItem.Bay);
                                    break;
                                    //case "HPGen8Blade":
                                    //    //if (vm.Gen8ServerInfo1 == null)
                                    //    vm.CurrentView13 = new HPG8ServerViewGOT(vm, "13", vm.Server13IPv4, true, "Server13 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen9Blade":
                                    //    //if (vm.Gen9ServerInfo1 == null)
                                    //    vm.CurrentView13 = new HPG9ServerViewGOT(vm, "13", vm.Server13IPv4, true, "Server13 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen10Blade":
                                    //    //if (vm.Gen10ServerInfo1 == null)
                                    //    vm.CurrentView13 = new HPG10ServerViewGOT(vm, "13", vm.Server13IPv4, true, "Server13 " + selectedItem.Bay);
                                    //    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView13 = new HPG8ServerView(vm, "13", vm.Server13IPv4, true, "Server13 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView13 = new HPG9ServerView(vm, "13", vm.Server13IPv4, true, "Server13 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView13 = new HPG10ServerView(vm, "13", vm.Server13IPv4, true, "Server13 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server14IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 14");
                        vm.Server14IPv4 = selectedItem.ServerIP;
                        vm.Server14TabHeader = "Server14 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView14 = new HPG8ServerView(vm, "14", vm.Server14IPv4, true, "Server14 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView14 = new HPG9ServerView(vm, "14", vm.Server14IPv4, true, "Server14 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView14 = new HPG10ServerView(vm, "14", vm.Server14IPv4, true, "Server14 " + selectedItem.Bay);
                                    break;
                                    //case "HPGen8Blade":
                                    //    //if (vm.Gen8ServerInfo1 == null)
                                    //    vm.CurrentView14 = new HPG8ServerViewGOT(vm, "14", vm.Server14IPv4, true, "Server14 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen9Blade":
                                    //    //if (vm.Gen9ServerInfo1 == null)
                                    //    vm.CurrentView14 = new HPG9ServerViewGOT(vm, "14", vm.Server14IPv4, true, "Server14 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen10Blade":
                                    //    //if (vm.Gen10ServerInfo1 == null)
                                    //    vm.CurrentView14 = new HPG10ServerViewGOT(vm, "14", vm.Server14IPv4, true, "Server14 " + selectedItem.Bay);
                                    //    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView14 = new HPG8ServerView(vm, "14", vm.Server14IPv4, true, "Server14 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView14 = new HPG9ServerView(vm, "14", vm.Server14IPv4, true, "Server14 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView14 = new HPG10ServerView(vm, "14", vm.Server14IPv4, true, "Server14 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server15IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 15");
                        vm.Server15IPv4 = selectedItem.ServerIP;
                        vm.Server15TabHeader = "Server15 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView15 = new HPG8ServerView(vm, "15", vm.Server15IPv4, true, "Server15 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView15 = new HPG9ServerView(vm, "15", vm.Server15IPv4, true, "Server15 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView15 = new HPG10ServerView(vm, "15", vm.Server15IPv4, true, "Server15 " + selectedItem.Bay);
                                    break;
                                    //case "HPGen8Blade":
                                    //    //if (vm.Gen8ServerInfo1 == null)
                                    //    vm.CurrentView15 = new HPG8ServerViewGOT(vm, "15", vm.Server15IPv4, true, "Server15 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen9Blade":
                                    //    //if (vm.Gen9ServerInfo1 == null)
                                    //    vm.CurrentView15 = new HPG9ServerViewGOT(vm, "15", vm.Server15IPv4, true, "Server15 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen10Blade":
                                    //    //if (vm.Gen10ServerInfo1 == null)
                                    //    vm.CurrentView15 = new HPG10ServerViewGOT(vm, "15", vm.Server15IPv4, true, "Server15 " + selectedItem.Bay);
                                    //    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView15 = new HPG8ServerView(vm, "15", vm.Server15IPv4, true, "Server15 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView15 = new HPG9ServerView(vm, "15", vm.Server15IPv4, true, "Server15 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView15 = new HPG10ServerView(vm, "15", vm.Server15IPv4, true, "Server15 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                    else if (vm.Server16IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 16");
                        vm.Server16IPv4 = selectedItem.ServerIP;
                        vm.Server16TabHeader = "Server16 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView16 = new HPG8ServerView(vm, "16", vm.Server16IPv4, true, "Server16 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView16 = new HPG9ServerView(vm, "16", vm.Server16IPv4, true, "Server16 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView16 = new HPG10ServerView(vm, "16", vm.Server16IPv4, true, "Server16 " + selectedItem.Bay);
                                    break;
                                    //case "HPGen8Blade":
                                    //    //if (vm.Gen8ServerInfo1 == null)
                                    //    vm.CurrentView16 = new HPG8ServerViewGOT(vm, "16", vm.Server16IPv4, true, "Server16 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen9Blade":
                                    //    //if (vm.Gen9ServerInfo1 == null)
                                    //    vm.CurrentView16 = new HPG9ServerViewGOT(vm, "16", vm.Server16IPv4, true, "Server16 " + selectedItem.Bay);
                                    //    break;
                                    //case "HPGen10Blade":
                                    //    //if (vm.Gen10ServerInfo1 == null)
                                    //    vm.CurrentView16 = new HPG10ServerViewGOT(vm, "16", vm.Server16IPv4, true, "Server16 " + selectedItem.Bay);
                                    //    break;
                            }

                        }
                        else
                        {
                            ///Goods In
                            switch (vm.ServerType)
                            {
                                case "HPGen8Blade":
                                    //if (vm.Gen8ServerInfo1 == null)
                                    vm.CurrentView16 = new HPG8ServerView(vm, "16", vm.Server16IPv4, true, "Server16 " + selectedItem.Bay);
                                    break;
                                case "HPGen9Blade":
                                    //if (vm.Gen9ServerInfo1 == null)
                                    vm.CurrentView16 = new HPG9ServerView(vm, "16", vm.Server16IPv4, true, "Server16 " + selectedItem.Bay);
                                    break;
                                case "HPGen10Blade":
                                    //if (vm.Gen10ServerInfo1 == null)
                                    vm.CurrentView16 = new HPG10ServerView(vm, "16", vm.Server16IPv4, true, "Server16 " + selectedItem.Bay);
                                    break;
                            }
                        }

                    }
                }

                //add
                //if (vm.SelectedBladeipList.Count() > 0)
                //{
                //    // MessageBox.Show(vm.Server1IPv4);
                //    //check if slot is free
                //    if (vm.Server1IPv4 == "")
                //    {
                //        // MessageBox.Show("Empty Slot 1");

                //        vm.Server1IPv4 = selectedItem.ServerIP;
                //        vm.Server1TabHeader = "Server1 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server2IPv4 == "")
                //    {
                //        // MessageBox.Show("Empty Slot 2");
                //        vm.Server2IPv4 = selectedItem.ServerIP;
                //        vm.Server2TabHeader = "Server2 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server3IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 3");
                //        vm.Server3IPv4 = selectedItem.ServerIP;
                //        vm.Server3TabHeader = "Server3 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server4IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 4");
                //        vm.Server4IPv4 = selectedItem.ServerIP;
                //        vm.Server4TabHeader = "Server4 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server5IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 5");
                //        vm.Server5IPv4 = selectedItem.ServerIP;
                //        vm.Server5TabHeader = "Server5 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server6IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 6");
                //        vm.Server6IPv4 = selectedItem.ServerIP;
                //        vm.Server6TabHeader = "Server6 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server7IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 7");
                //        vm.Server7IPv4 = selectedItem.ServerIP;
                //        vm.Server7TabHeader = "Server7 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server8IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 8");
                //        vm.Server8IPv4 = selectedItem.ServerIP;
                //        vm.Server8TabHeader = "Server8 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server9IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 9");
                //        vm.Server9IPv4 = selectedItem.ServerIP;
                //        vm.Server9TabHeader = "Server9 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server10IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 10");
                //        vm.Server10IPv4 = selectedItem.ServerIP;
                //        vm.Server10TabHeader = "Server10 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server11IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 11");
                //        vm.Server11IPv4 = selectedItem.ServerIP;
                //        vm.Server11TabHeader = "Server11 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server12IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 12");
                //        vm.Server12IPv4 = selectedItem.ServerIP;
                //        vm.Server12TabHeader = "Server12 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server13IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 13");
                //        vm.Server13IPv4 = selectedItem.ServerIP;
                //        vm.Server13TabHeader = "Server13 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server14IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 14");
                //        vm.Server14IPv4 = selectedItem.ServerIP;
                //        vm.Server14TabHeader = "Server14 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server15IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 15");
                //        vm.Server15IPv4 = selectedItem.ServerIP;
                //        vm.Server15TabHeader = "Server15 " + selectedItem.Bay;

                //    }
                //    else if (vm.Server16IPv4 == "")
                //    {
                //        //  MessageBox.Show("Empty Slot 16");
                //        vm.Server16IPv4 = selectedItem.ServerIP;
                //        vm.Server16TabHeader = "Server16 " + selectedItem.Bay;

                //    }
                //}

            }






            vm.FlyOutContent = new HPEnclosureView(vm);

            //if (vm.Server1IPv4 != string.Empty)
            //{
            //    vm.FlipView1Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo1 == null)
            //                vm.RunRestfulScriptsCommandGen8S1.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo1 == null)
            //                vm.RunRestfulScriptsCommandGen9S1.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo1 == null)
            //                vm.RunRestfulScriptsCommandGen10S1.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}



            //if (vm.Server2IPv4 != string.Empty)
            //{
            //    vm.FlipView2Index = 4;

            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo2 == null)
            //                vm.RunRestfulScriptsCommandGen8S2.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo2 == null)
            //                vm.RunRestfulScriptsCommandGen9S2.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo2 == null)
            //                vm.RunRestfulScriptsCommandGen10S2.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}



            //if (vm.Server3IPv4 != string.Empty)
            //{
            //    vm.FlipView3Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo3 == null)
            //                vm.RunRestfulScriptsCommandGen8S3.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo3 == null)
            //                vm.RunRestfulScriptsCommandGen9S3.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo3 == null)
            //                vm.RunRestfulScriptsCommandGen10S3.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}


            //if (vm.Server4IPv4 != string.Empty)
            //{
            //    vm.FlipView4Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo4 == null)
            //                vm.RunRestfulScriptsCommandGen8S4.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo4 == null)
            //                vm.RunRestfulScriptsCommandGen9S4.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo4 == null)
            //                vm.RunRestfulScriptsCommandGen10S4.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}


            //if (vm.Server5IPv4 != string.Empty)
            //{
            //    vm.FlipView5Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo5 == null)
            //                vm.RunRestfulScriptsCommandGen8S5.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo5 == null)
            //                vm.RunRestfulScriptsCommandGen9S5.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo5 == null)
            //                vm.RunRestfulScriptsCommandGen10S5.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}



            //if (vm.Server6IPv4 != string.Empty)
            //{
            //    vm.FlipView6Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo6 == null)
            //                vm.RunRestfulScriptsCommandGen8S6.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo6 == null)
            //                vm.RunRestfulScriptsCommandGen9S6.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo6 == null)
            //                vm.RunRestfulScriptsCommandGen10S6.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}



            //if (vm.Server7IPv4 != string.Empty)
            //{
            //    vm.FlipView7Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo7 == null)
            //                vm.RunRestfulScriptsCommandGen8S7.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo7 == null)
            //                vm.RunRestfulScriptsCommandGen9S7.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo7 == null)
            //                vm.RunRestfulScriptsCommandGen10S7.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }

            //}


            //if (vm.Server8IPv4 != string.Empty)
            //{
            //    vm.FlipView8Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo8 == null)
            //                vm.RunRestfulScriptsCommandGen8S8.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo8 == null)
            //                vm.RunRestfulScriptsCommandGen9S8.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo8 == null)
            //                vm.RunRestfulScriptsCommandGen10S8.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}



            //if (vm.Server9IPv4 != string.Empty)
            //{
            //    vm.FlipView9Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo9 == null)
            //                vm.RunRestfulScriptsCommandGen8S9.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo9 == null)
            //                vm.RunRestfulScriptsCommandGen9S9.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo9 == null)
            //                vm.RunRestfulScriptsCommandGen10S9.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}



            //if (vm.Server10IPv4 != string.Empty)
            //{
            //    vm.FlipView10Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo10 == null)
            //                vm.RunRestfulScriptsCommandGen8S10.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo10 == null)
            //                vm.RunRestfulScriptsCommandGen9S10.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo10 == null)
            //                vm.RunRestfulScriptsCommandGen10S10.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}



            //if (vm.Server11IPv4 != string.Empty)
            //{
            //    vm.FlipView11Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo11 == null)
            //                vm.RunRestfulScriptsCommandGen8S11.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo11 == null)
            //                vm.RunRestfulScriptsCommandGen9S11.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo11 == null)
            //                vm.RunRestfulScriptsCommandGen10S11.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}



            //if (vm.Server12IPv4 != string.Empty)
            //{
            //    vm.FlipView12Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo12 == null)
            //                vm.RunRestfulScriptsCommandGen8S12.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo12 == null)
            //                vm.RunRestfulScriptsCommandGen9S12.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo12 == null)
            //                vm.RunRestfulScriptsCommandGen10S12.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}



            //if (vm.Server13IPv4 != string.Empty)
            //{
            //    vm.FlipView13Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo13 == null)
            //                vm.RunRestfulScriptsCommandGen8S13.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo13 == null)
            //                vm.RunRestfulScriptsCommandGen9S13.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo13 == null)
            //                vm.RunRestfulScriptsCommandGen10S13.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}



            //if (vm.Server14IPv4 != string.Empty)
            //{
            //    vm.FlipView14Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo14 == null)
            //                vm.RunRestfulScriptsCommandGen8S14.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo14 == null)
            //                vm.RunRestfulScriptsCommandGen9S14.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo14 == null)
            //                vm.RunRestfulScriptsCommandGen10S14.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}



            //if (vm.Server15IPv4 != string.Empty)
            //{
            //    vm.FlipView15Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo15 == null)
            //                vm.RunRestfulScriptsCommandGen8S15.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo15 == null)
            //                vm.RunRestfulScriptsCommandGen9S15.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo15 == null)
            //                vm.RunRestfulScriptsCommandGen10S15.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}



            //if (vm.Server16IPv4 != string.Empty)
            //{
            //    vm.FlipView16Index = 4;
            //    switch (vm.ServerType)
            //    {
            //        case "HPGen8Blade":
            //            if (vm.Gen8ServerInfo16 == null)
            //                vm.RunRestfulScriptsCommandGen8S16.Execute(new String[] { "ServerInfo", "Gen8" });
            //            break;
            //        case "HPGen9Blade":
            //            if (vm.Gen9ServerInfo16 == null)
            //                vm.RunRestfulScriptsCommandGen9S16.Execute(new String[] { "ServerInfo", "Gen9" });
            //            break;
            //        case "HPGen10Blade":
            //            if (vm.Gen10ServerInfo16 == null)
            //                vm.RunRestfulScriptsCommandGen10S16.Execute(new String[] { "ServerInfo", "Gen10" });
            //            break;
            //    }
            //}



            vm.ShowHPFlyout = false;


        }


        //public void RunGetServerInfo()
        //{
        //    switch (vm.ServerType)
        //    {
        //        case "HPGen8Blade":
        //            vm.RunRestfulScriptsCommandGen8S1.Execute(new String[] { "ServerInfo", "Gen8" });
        //            break;
        //        case "HPGen9Blade":
        //            vm.RunRestfulScriptsCommandGen9S1.Execute(new String[] { "ServerInfo", "Gen9" });
        //            break;
        //        case "HPGen10Blade":
        //            vm.RunRestfulScriptsCommandGen10S1.Execute(new String[] { "ServerInfo", "Gen9" });
        //            break;
        //    }
        //}



        private void EnclosureBaysLV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void EnclosureBaysLV_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void FindServersBTN_Click(object sender, RoutedEventArgs e)
        {
            //Clear items before check
            vm.BladeipList.Clear();
            vm.SelectedBladeipList.Clear();

            //Call checkenclosure method
            CheckEnclosure();
        }





        public string SSHCommand(string commandString)
        {
            string output = "";

            try
            {


                //Set up the SSH connection
                using (var client = new SshClient(vm.HPEnclosureIP, "Administrator", "password"))
                {

                    //Accept Host key
                    client.HostKeyReceived += delegate (object sender, HostKeyEventArgs e)
                    {
                        e.CanTrust = true;
                    };

                    //Start the connection  var output = client.RunCommand("show device details").Result;
                    client.Connect();

                    //output = client.RunCommand("SHOW EBIPA").Result;
                    //output = client.RunCommand("SHOW SERVER LIST").Result;
                    output = client.RunCommand(commandString).Result;

                    client.Disconnect();

                    return output;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return output;
            }

        }

        public ObservableCollection<BladeItem> CheckHPEnclosure()
        {
            string aLine = "";
            string bLine = "";

            StringReader strReaderBayInfo = new StringReader(SSHCommand("SHOW SERVER LIST"));

            ObservableCollection<BladeItem> BladeipList = new ObservableCollection<BladeItem>();
            // MessageBox.Show(SSHCommand("SHOW SERVER LIST"));

            // string WhichBay = "";
            //int bayCount = 1;

            while ((aLine = strReaderBayInfo.ReadLine()) != null)
            {

                //if server there extract the info
                if (aLine.Contains("192.168.") || aLine.Contains("10.0.") || aLine.Contains("172.16.") || aLine.Contains("172.17.") || aLine.Contains("172.18.") || aLine.Contains("172.19.") || aLine.Contains("172.20.") || aLine.Contains("172.21.") || aLine.Contains("172.22.") || aLine.Contains("172.23.") || aLine.Contains("172.24.") || aLine.Contains("172.25.") || aLine.Contains("172.26.") || aLine.Contains("172.27.") || aLine.Contains("172.28.") || aLine.Contains("172.29.") || aLine.Contains("172.30.") || aLine.Contains("172.31."))
                {
                    //MessageBox.Show(aLine.Trim());
                    BladeItem blade = new BladeItem();

                    // MessageBox.Show(aLine.Substring(30, 18).Trim());

                    //Bay Number
                    blade.Bay = "Bay" + aLine.Substring(0, 4).Trim();// + aLine ;
                                                                     //Server IP
                    blade.ServerIP = aLine.Substring(30, 19).Trim();
                    //Status
                    blade.Status = aLine.Substring(50, 8).Trim();
                    //Power State
                    blade.PowerState = aLine.Substring(59, 5).Trim();



                    StringReader strReaderServerInfo = new StringReader(SSHCommand("SHOW SERVER INFO " + aLine.Substring(0, 4).Trim()));


                    while ((bLine = strReaderServerInfo.ReadLine()) != null)
                    {

                        //MessageBox.Show(bLine);
                        if (bLine != null)
                        {


                            if (bLine.Contains("Part Number:"))
                            {
                                blade.PartNumber = bLine.Replace("Part Number:", "").Trim();
                            }
                            else if (bLine.Contains("Manufacturer:"))
                            {
                                blade.Manufacturer = bLine.Replace("Manufacturer:", "").Trim();

                            }
                            else if (bLine.Contains("Product Name:"))
                            {


                                blade.ProductName = bLine.Replace("Product Name:", "").Trim();

                            }
                            else if (bLine.Contains("ROM Version:"))
                            {
                                blade.RomVersion = bLine.Replace("ROM Version:", "").Trim();

                            }
                            else if (bLine.Contains("Firmware Version:"))
                            {
                                blade.FirmwareVersion = bLine.Replace("Firmware Version:", "").Trim();

                            }
                        }



                    };

                    //Add the blade to the collection
                    BladeipList.Add(blade);
                }



                //do what you want to do with the line;
            };

            return BladeipList;

        }


        public async void CheckEnclosure()
        {


            ProgressBarHP.IsIndeterminate = true;

            //Call function to check hp enclosure
            vm.BladeipList = await System.Threading.Tasks.Task.Run(() => CheckHPEnclosure());



            ProgressBarHP.IsIndeterminate = false;
            //Don't Select Anything
            EnclosureBaysLV.SelectedIndex = -1;
        }

        private void SelectAllDELLBTN_Click(object sender, RoutedEventArgs e)
        {
            EnclosureBaysLV.SelectAll();
        }

        private void PackIconSimpleIcons_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            vm.ShowHPFlyout = false;
        }

        private void HeaderLogo_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            vm.ShowHPFlyout = false;
        }

        private void HostTxtB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                //Clear items before check
                vm.BladeipList.Clear();
                vm.SelectedBladeipList.Clear();

                //Call checkenclosure method
                CheckEnclosure();

            }
        }

        private async void FactoryResetBTN_Click(object sender, RoutedEventArgs e)
        {
            //Create instance of Redfish lib
            RedfishRestful RedfishCrawler = new RedfishRestful();

            //Test Code

            BladeItem bli = new BladeItem();
            bli.Manufacturer = "HPE";
            bli.ProductName = "BL460 Gen8";
            bli.ServerIP = "192.168.101.248";

            vm.BladeipList.Add(bli);

            // await System.Threading.Tasks.Task.Run(() => vm.TimedMessage("Factory Defaults Running, please wait...", "Info", 4000));
            vm.PartCodeMessageColour = Brushes.Green;
            vm.PartCodeMessage = App.Current.FindResource("SvrENCFactoryDefaultsMSG").ToString();

            //Add progress animation
            ProgressBarHP.IsIndeterminate = true;
            //Set Timed Message
           


            try
            {

                //Loop Through each enclosure item
                foreach (BladeItem itm in vm.BladeipList)
                {

                    if (!string.IsNullOrEmpty(itm.ServerIP))
                    {
                        //RUN Factory Default Scripts
                     
                        vm.Server1IPv4 = itm.ServerIP;

                       //REDFISH FACTORY DEFAULTS
                       await RedfishCrawler.RedfishActionCall("https://" + vm.Server1IPv4 + "/redfish/v1/Managers/1/Actions/Oem/Hp/HpiLO.ResetToFactoryDefaults/", HttpMethod.Post, "Administrator:password", vm, "{\"Action\": \"ResetToFactoryDefaults\",\"ResetType\": \"Default\"}");

                    }


                }

                //Wait 120 seconds
                await PutTaskDelay(120000);

            
                //Dispaly Message to user
              

                
                // Switch off progress
                ProgressBarHP.IsIndeterminate = false;
                vm.PartCodeMessageColour = Brushes.Green;
                // await System.Threading.Tasks.Task.Run(() => vm.TimedMessage("Factory Defaults are being applied. Please wait....", "Info", 120000));
                vm.PartCodeMessage = App.Current.FindResource("SvrENCFactoryDefaultsCompleteMSG").ToString();

            }
            catch (Exception ex)
            {

                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;


            }

     }


        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

    }


    public class BladeItem
    {
        public string Bay { get; set; }
        public string ServerIP { get; set; }
        public string Manufacturer { get; set; }
        public string ProductName { get; set; }
        public string PartNumber { get; set; }
        public string RomVersion { get; set; }
        public string FirmwareVersion { get; set; }
        public string Status { get; set; }
        public string PowerState { get; set; }
    }

}
