﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.IO.Ports;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Nebula.Helpers;
using Nebula.Properties;
using Nebula.ViewModels;
using System.Text.RegularExpressions;
using Nebula.Models;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for SerialClusterView.xaml
    /// </summary>
    public partial class SerialClusterView : UserControl
    {
        //http://jkorpela.fi/chars/c0.html FOR FULL CHART
        const char CtrlA = (char)1;
        const char CtrlB = (char)2;
        const char CtrlC = (char)3;
        const char CtrlD = (char)4;
        const char CtrlE = (char)5;
        const char CtrlF = (char)6;
        const char CtrlG = (char)7;
        const char CtrlH = (char)8;
        const char CtrlI = (char)9;
        const char CtrlJ = (char)10;
        const char CtrlK = (char)11;
        const char CtrlL = (char)12;
        const char CtrlM = (char)13;
        const char CtrlN = (char)14;
        const char CtrlO = (char)15;
        const char CtrlP = (char)16;
        const char CtrlQ = (char)17;
        const char CtrlR = (char)18;
        const char CtrlS = (char)19;
        const char CtrlT = (char)20;
        const char CtrlU = (char)21;
        const char CtrlV = (char)21;
        const char CtrlW = (char)23;
        const char CtrlX = (char)24;
        const char CtrlY = (char)25;
        const char CtrlZ = (char)26;
        const char CtrlESC = (char)27;
        const char CtrlFILESEP = (char)28;
        const char CtrlGRPSEP = (char)29;
        const char CtrlRECSEP = (char)30;
        const char CR = (char)13;
        public enum DataMode { Text, Hex };
        public enum LogMsgType { Incoming, Outgoing, Normal, Warning, Error };

        // The main control for communicating through the RS-232 port
        private SerialPort _serialPort1 = new SerialPort();
        private SerialPort _serialPort2 = new SerialPort();
        private SerialPort _serialPort3 = new SerialPort();
        private SerialPort _serialPort4 = new SerialPort();
        private SerialPort _serialPort5 = new SerialPort();
        private SerialPort _serialPort6 = new SerialPort();
        private SerialPort _serialPort7 = new SerialPort();
        private SerialPort _serialPort8 = new SerialPort();
        private SerialPort _serialPort9 = new SerialPort();
        private SerialPort _serialPort10 = new SerialPort();
        private SerialPort _serialPort11 = new SerialPort();
        private SerialPort _serialPort12 = new SerialPort();
        private SerialPort _serialPort13 = new SerialPort();
        private SerialPort _serialPort14 = new SerialPort();
        private SerialPort _serialPort15 = new SerialPort();
        private SerialPort _serialPort16 = new SerialPort();

        //public static string Output { get; set; }
        public string ConsoleIncoming1 { get; set; }
        public string ConsoleIncoming2 { get; set; }
        public string ConsoleIncoming3 { get; set; }
        public string ConsoleIncoming4 { get; set; }
        public string ConsoleIncoming5 { get; set; }
        public string ConsoleIncoming6 { get; set; }
        public string ConsoleIncoming7 { get; set; }
        public string ConsoleIncoming8 { get; set; }
        public string ConsoleIncoming9 { get; set; }
        public string ConsoleIncoming10 { get; set; }
        public string ConsoleIncoming11 { get; set; }
        public string ConsoleIncoming12 { get; set; }
        public string ConsoleIncoming13 { get; set; }
        public string ConsoleIncoming14 { get; set; }
        public string ConsoleIncoming15 { get; set; }
        public string ConsoleIncoming16 { get; set; }


        // RTB CONTENT
        public string ConsoleContent1 { get; set; }
        public string ConsoleContent2 { get; set; }
        public string ConsoleContent3 { get; set; }
        public string ConsoleContent4 { get; set; }
        public string ConsoleContent5 { get; set; }
        public string ConsoleContent6 { get; set; }
        public string ConsoleContent7 { get; set; }
        public string ConsoleContent8 { get; set; }
        public string ConsoleContent9 { get; set; }
        public string ConsoleContent10 { get; set; }
        public string ConsoleContent11 { get; set; }

        public string ConsoleContent12 { get; set; }
        public string ConsoleContent13 { get; set; }

        public string ConsoleContent14 { get; set; }
        public string ConsoleContent15 { get; set; }
        public string ConsoleContent16 { get; set; }


        ////Flow Documents for the RTB Console windows
        FlowDocument FlowDoc1 = new FlowDocument();
        FlowDocument FlowDoc2 = new FlowDocument();
        FlowDocument FlowDoc3 = new FlowDocument();
        FlowDocument FlowDoc4 = new FlowDocument();
        FlowDocument FlowDoc5 = new FlowDocument();
        FlowDocument FlowDoc6 = new FlowDocument();
        FlowDocument FlowDoc7 = new FlowDocument();
        FlowDocument FlowDoc8 = new FlowDocument();
        FlowDocument FlowDoc9 = new FlowDocument();
        FlowDocument FlowDoc10 = new FlowDocument();
        FlowDocument FlowDoc11 = new FlowDocument();
        FlowDocument FlowDoc12 = new FlowDocument();
        FlowDocument FlowDoc13 = new FlowDocument();
        FlowDocument FlowDoc14 = new FlowDocument();
        FlowDocument FlowDoc15 = new FlowDocument();
        FlowDocument FlowDoc16 = new FlowDocument();

        ////Paragraphs for the Flow Documents on the RTB Console windows
        Paragraph para1 = new Paragraph();
        Paragraph para2 = new Paragraph();
        Paragraph para3 = new Paragraph();
        Paragraph para4 = new Paragraph();
        Paragraph para5 = new Paragraph();
        Paragraph para6 = new Paragraph();
        Paragraph para7 = new Paragraph();
        Paragraph para8 = new Paragraph();
        Paragraph para9 = new Paragraph();
        Paragraph para10 = new Paragraph();
        Paragraph para11 = new Paragraph();
        Paragraph para12 = new Paragraph();
        Paragraph para13 = new Paragraph();
        Paragraph para14 = new Paragraph();
        Paragraph para15 = new Paragraph();
        Paragraph para16 = new Paragraph();

        List<SerialPort> ComCollection = new List<SerialPort>();
        List<SerialPort> prts = new List<SerialPort>();


        //declare viewmodel
        MainViewModel vm;

        public SerialClusterView(MainViewModel PassedViewModel)
        {
            InitializeComponent();
            try
            {

           
            // Load user settings
            settings.Reload();

            //get the passed in viewmodel and assign to the local vm variable
            vm = PassedViewModel;
            //set the datacontext of the view to the passed vm
            this.DataContext = vm;


            //Add all serial ports to a collection


            // Call the Get Ports and Sort and Add To Combo Box
            string[] comPorts = OrderedPortNames();
            foreach (string s in comPorts)
            {
                //cluster selection combobox
                ComPortCB.Items.Add(s);
               
            }
          
            //Populate the Com ComboBoxes
            //Baud
            BaudCB.Items.Add("4800");
            BaudCB.Items.Add("9600");
            BaudCB.Items.Add("14400");
            BaudCB.Items.Add("19200");
            BaudCB.Items.Add("38400");
            BaudCB.Items.Add("57600");
            BaudCB.Items.Add("115200");
            //Parity
            ParityCB.Items.Add("None");
            ParityCB.Items.Add("Odd");
            ParityCB.Items.Add("Even");
            ParityCB.Items.Add("Mark");
            ParityCB.Items.Add("Space");
            // Data Bits
            DataBitsCB.Items.Add("7");
            DataBitsCB.Items.Add("8");
            DataBitsCB.Items.Add("9");
            // Stop Bits
            StopBitsCB.Items.Add("None");
            StopBitsCB.Items.Add("One");
            StopBitsCB.Items.Add("Two");
            StopBitsCB.Items.Add("OnePointFive");
            //Handshake
            HandShakeCB.Items.Add("None");
            HandShakeCB.Items.Add("XOnXOff");
            HandShakeCB.Items.Add("RequestToSend");
            HandShakeCB.Items.Add("RequestToSendXOnXOff");


            ComCollection.Add(_serialPort1);
            ComCollection.Add(_serialPort2);
            ComCollection.Add(_serialPort3);
            ComCollection.Add(_serialPort4);
            ComCollection.Add(_serialPort5);
            ComCollection.Add(_serialPort6);
            ComCollection.Add(_serialPort7);
            ComCollection.Add(_serialPort8);
            ComCollection.Add(_serialPort9);
            ComCollection.Add(_serialPort10);
            ComCollection.Add(_serialPort11);
            ComCollection.Add(_serialPort12);
            ComCollection.Add(_serialPort13);
            ComCollection.Add(_serialPort14);
            ComCollection.Add(_serialPort15);
            ComCollection.Add(_serialPort16);

            // Add items to isolate combobox 
            IsolateComPortCB.Items.Add("Serial1");
            IsolateComPortCB.Items.Add("Serial2");
            IsolateComPortCB.Items.Add("Serial3");
            IsolateComPortCB.Items.Add("Serial4");
            IsolateComPortCB.Items.Add("Serial5");
            IsolateComPortCB.Items.Add("Serial6");
            IsolateComPortCB.Items.Add("Serial7");
            IsolateComPortCB.Items.Add("Serial8");
            IsolateComPortCB.Items.Add("Serial9");
            IsolateComPortCB.Items.Add("Serial10");
            IsolateComPortCB.Items.Add("Serial11");
            IsolateComPortCB.Items.Add("Serial12");
            IsolateComPortCB.Items.Add("Serial13");
            IsolateComPortCB.Items.Add("Serial14");
            IsolateComPortCB.Items.Add("Serial15");
            IsolateComPortCB.Items.Add("Serial16");

            //foreach (var serial in ComCollection)
            //{
            //    //cluster selection combobox
            //   // ComPortCB.Items.Add(s);
            //    //com isolation combobox
            //    IsolateComPortCB.Items.Add(serial.ToString());
            //}



            ConsoleRTB1.Document = FlowDoc1;
            ConsoleRTB2.Document = FlowDoc2;
            ConsoleRTB3.Document = FlowDoc3;
            ConsoleRTB4.Document = FlowDoc4;
            ConsoleRTB5.Document = FlowDoc5;
            ConsoleRTB6.Document = FlowDoc6;
            ConsoleRTB7.Document = FlowDoc7;
            ConsoleRTB8.Document = FlowDoc8;
            ConsoleRTB9.Document = FlowDoc9;
            ConsoleRTB10.Document = FlowDoc10;
            ConsoleRTB11.Document = FlowDoc11;
            ConsoleRTB12.Document = FlowDoc12;
            ConsoleRTB13.Document = FlowDoc13;
            ConsoleRTB14.Document = FlowDoc14;
            ConsoleRTB15.Document = FlowDoc15;
            ConsoleRTB16.Document = FlowDoc16;


            //Read in the XML Product files and add to the collection
            //vm.ReadProductFileCommand.Execute("");







            //Mapping of serial port to Com Port

            //Select Cluster Comport to Start
            //foreach (var sp in ComCollection)
            //{
            //    sp.PortName = "";
            //}

            //*******Serial Init
            //_serialPort1.PortName = "COM5";
            //_serialPort2.PortName = "COM6";
            //_serialPort3.PortName = "COM7";
            //_serialPort4.PortName = "COM8";
            //_serialPort5.PortName = "COM9";
            //_serialPort6.PortName = "COM10";
            //_serialPort7.PortName = "COM11";
            //_serialPort8.PortName = "COM12";
            //_serialPort9.PortName = "COM13";
            //_serialPort10.PortName = "COM14";
            //_serialPort11.PortName = "COM15";
            //_serialPort12.PortName = "COM16";
            //_serialPort13.PortName = "COM17";
            //_serialPort14.PortName = "COM18";
            //_serialPort15.PortName = "COM19";
            //_serialPort16.PortName = "COM20";

            // When data is recieved through the port, call this method
            //_serialPort1.DataReceived += new SerialDataReceivedEventHandler(_serialPort1_DataReceived);
            //_serialPort1.PinChanged += new SerialPinChangedEventHandler(_serialPort1_PinChanged);
            //_serialPort2.DataReceived += new SerialDataReceivedEventHandler(_serialPort2_DataReceived);
            //_serialPort2.PinChanged += new SerialPinChangedEventHandler(_serialPort2_PinChanged);

            //
            //_serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve);
            //_serialPort2.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve);
            //_serialPort3.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve);
            //_serialPort4.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve);
            _serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve1);
            _serialPort2.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve2);
            _serialPort3.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve3);
            _serialPort4.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve4);
            _serialPort5.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve5);
            _serialPort6.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve6);
            _serialPort7.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve7);
            _serialPort8.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve8);
            _serialPort9.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve9);
            _serialPort10.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve10);
            _serialPort11.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve11);
            _serialPort12.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve12);
            _serialPort13.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve13);
            _serialPort14.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve14);
            _serialPort15.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve15);
            _serialPort16.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve16);


            // Load user settings
            settings.Reload();
            LoadSettings();

                // set screen offset
                ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("1-4"));
                //change background of selected bank
                Bank1.Background = Brushes.MediumPurple;
                Bank2.Background = (Brush)FindResource("TechBuyerBrush1");
                Bank3.Background = (Brush)FindResource("TechBuyerBrush1");
                Bank4.Background = (Brush)FindResource("TechBuyerBrush1");

                //set console window initial font size
                ConsoleFontSizeClusterCB.SelectedIndex = 4;
            vm.ConsoleFontSize = 12;

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }



        //TODO CREATE CONTROLS DYNAMICALLY TO SUPPORT MULTIPLE SERIALPORTS
        //NEED TO DETECT NUMBER OF COM PORTS ON THE SYSTEM AND GENERATE THE CONTROLS ON THE FLY
        public void CreateConsoleWindows() 
        {



            // //Creating a flow document from code
            // vm.FlowDoc = new FlowDocument();

            // vm.FlowDoc.Name = "SingleSerialFlowDoc1";

            // BlockUIContainer Blockui = new BlockUIContainer();
            // Border Board = new Border();
            // Board.BorderBrush = Brushes.Black;
            // Board.Padding = new Thickness(2);
            // Board.HorizontalAlignment = HorizontalAlignment.Center;
            // Board.BorderThickness = new Thickness(2);
            // Image img = new Image();
            // img.Source = new BitmapImage(new Uri("pack://application:,,,/Images/TechBuyerLogo Centered.png")); //"pack://application:,,,/AssemblyName;component/Images/TechBuyerLogo Centered.png"
            // img.Width = 50;
            // img.Height = 50;
            // img.VerticalAlignment = VerticalAlignment.Center;
            // img.HorizontalAlignment = HorizontalAlignment.Center;
            // StackPanel sp = new StackPanel();
            // sp.HorizontalAlignment = HorizontalAlignment.Center;
            // sp.Orientation = Orientation.Vertical;
            // TextBlock tb = new TextBlock();
            // tb.Name = "TitleHeader1";
            // tb.Text = "Test Report";
            // tb.Foreground = Brushes.White;
            // tb.Background = (Brush)FindResource("TechBuyerBrush1");

            // tb.FontFamily = new FontFamily("Segoe UI");
            // tb.FontSize = 12;
            // tb.Padding = new Thickness(5);
            // Binding bind = new Binding("Value") { ElementName = "TitleHeader1" };
            // BindingOperations.SetBinding(tb, TextBlock.WidthProperty, bind);
            // Blockui.Child = Board;
            // Board.Child = sp;
            // sp.Children.Add(img);
            // sp.Children.Add(tb);
            // vm.FlowDoc.Blocks.Add(Blockui);

            // //Blockui.Child = 


            //Paragraph p = new Paragraph(new Run("Hello, world!"));
            // p.FontSize = 36;
            // vm.FlowDoc.Blocks.Add(p);

            // p = new Paragraph(new Run("The ultimate programming greeting!"));
            // p.FontSize = 14;
            // p.FontStyle = FontStyles.Italic;
            // p.TextAlignment = TextAlignment.Left;
            // p.Foreground = Brushes.Gray;
            // vm.FlowDoc.Blocks.Add(p);


          

            //< StackPanel Orientation = "Vertical" Margin = "6" >

            //                  < StackPanel Orientation = "Horizontal" >

            //                       < TextBlock Foreground = "{DynamicResource TechBuyerBrush1}" > Serial1 </ TextBlock >

            //                        < Rectangle x: Name = "OnOff1" Width = "10" Height = "10" Margin = "10,0,0,0" Fill = "Red" HorizontalAlignment = "Left" ></ Rectangle >


            //                                </ StackPanel >


            //                                < StackPanel Orientation = "Vertical" >

            //                                     < RichTextBox x: Name = "ConsoleRTB1" MaxWidth = "450" Height = "250" Background = "Black" Foreground = "Lime" ScrollViewer.CanContentScroll = "True"  VerticalScrollBarVisibility = "Auto" />

            //                                                   < TextBlock Foreground = "{DynamicResource TechBuyerBrush1}" HorizontalAlignment = "Left" > Flash Content </ TextBlock >

            //                                                              < Border BorderBrush = "#FFCCCCCC" BorderThickness = "1" >

            //                                                                     < ListBox x: Name = "LB1" Margin = "0,0,0,10" Height = "160" SelectionMode = "Multiple" />

            //                                                                         </ Border >

            //                                                                         < Button x: Name = "DelFlash1" Cursor = "Hand" Background = "{DynamicResource TechBuyerBrush1}" HorizontalAlignment = "Left" Foreground = "White" Height = "30" Width = "120" MinWidth = "50" > Delete Selected </ Button >

            //                                                                                         </ StackPanel >


            //                                                                                 </ StackPanel >






        }



        ////EXTENSION METHOD FOR SETTING RICHTEXT WPF CONTROL
        //public void SetText(this RichTextBox richTextBox, string text)
        //{
        //    richTextBox.Document.Blocks.Clear();
        //    richTextBox.Document.Blocks.Add(new Paragraph(new Run(text)));
        //}

        //public string GetText(this RichTextBox richTextBox)
        //{
        //    return new TextRange(richTextBox.Document.ContentStart,
        //        richTextBox.Document.ContentEnd).Text;
        //}




        private DataMode CurrentDataMode
        {
            get
            {
                //if (rbHex.Checked) return DataMode.Hex;
                /* else */
                return DataMode.Text;
            }
            set
            {
                //if (value == DataMode.Text) rbText.Checked = true;
                //else rbHex.Checked = true;
            }
        }

        // Various colors for logging info
        //private System.Windows.Media.Color[] LogMsgTypeColor = { Color.Blue, Color.Green, Color.Black, Color.Orange, Color.Red };

        // Temp holder for whether a key was pressed
        //private bool KeyHandled = false;

        private Settings settings = Settings.Default;

        private string RefreshComPortList(IEnumerable<string> PreviousPortNames, string CurrentSelection, bool PortOpen)
        {
            // Create a new return report to populate
            string selected = null;

            // Retrieve the list of ports currently mounted by the operating system (sorted by name)
            string[] ports = SerialPort.GetPortNames();

            // First determain if there was a change (any additions or removals)
            bool updated = PreviousPortNames.Except(ports).Count() > 0 || ports.Except(PreviousPortNames).Count() > 0;

            // If there was a change, then select an appropriate default port
            if (updated)
            {
                // Use the correctly ordered set of port names
                ports = OrderedPortNames();

                // Find newest port if one or more were added
                string newest = SerialPort.GetPortNames().Except(PreviousPortNames).OrderBy(a => a).LastOrDefault();

                // If the port was already open... (see logic notes and reasoning in Notes.txt)
                if (PortOpen)
                {
                    if (ports.Contains(CurrentSelection)) selected = CurrentSelection;
                    else if (!String.IsNullOrEmpty(newest)) selected = newest;
                    else selected = ports.LastOrDefault();
                }
                else
                {
                    if (!String.IsNullOrEmpty(newest)) selected = newest;
                    else if (ports.Contains(CurrentSelection)) selected = CurrentSelection;
                    else selected = ports.LastOrDefault();
                }
            }

            // If there was a change to the port list, return the recommended default selection
            return selected;
        }

        private string[] OrderedPortNames()
        {
            // Just a placeholder for a successful parsing of a string to an integer
            int num;

            // Order the serial port names in numberic order (if possible)
            return SerialPort.GetPortNames().OrderBy(a => a.Length > 3 && int.TryParse(a.Substring(3), out num) ? num : 0).ToArray();
        }





        void _serialPort1_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort2_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort3_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort4_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort5_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort6_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort7_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort8_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }
        void _serialPort9_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort10_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort11_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort12_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort13_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort14_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort15_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort16_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        private void UpdatePinState()
        {
            //this.Invoke(new ThreadStart(() => {
            //    // Show the state of the pins
            //    //chkCD.Checked = comport.CDHolding;
            //    //chkCTS.Checked = comport.CtsHolding;
            //    //chkDSR.Checked = comport.DsrHolding;
            //}));
        }

        //test delegate
        public delegate void UpdateUiTextDelegate(string recdata,string com);

        //delegate
        public delegate void UpdateUiTextDelegate1(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate2(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate3(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate4(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate5(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate6(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate7(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate8(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate9(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate10(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate11(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate12(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate13(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate14(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate15(string recdata);
        //delegate
        public delegate void UpdateUiTextDelegate16(string recdata);




//        CheckBox chbx1 = new CheckBox();
//        chbx1.Name = "chbx1";
//chbx1.CheckedChanged += checkBox_CheckedChanged;
//CheckBox chbx2 = new CheckBox();
//        chbx2.Name = "chbx2";
//chbx2.CheckedChanged += checkBox_CheckedChanged;
//CheckBox chbx3 = new CheckBox();
//        chbx3.Name = "chbx2";
//chbx3.CheckedChanged += checkBox_CheckedChanged;
//And

//private void checkBox_CheckedChanged(object sender, EventArgs e)
//        {
//            string chbxName = ((CheckBox)sender).Name;
//            //Necessary code for identifying the CheckBox and following processes ...
//            checkBox = Code which will determine what checkBox sent it.
//        if (checkBox.Checked)
//            { Box.ChangeState(checkBox, true); }
//            else { Box.ChangeState(checkBox, false); }
//        }


        
            //ConsoleRTB1.CaretPosition = ConsoleRTB1.CaretPosition.DocumentEnd;

            /* update caret position */
            //ConsoleRTB1.CaretPosition = startPtr.GetPositionAtOffset((start) + text.Length);

            /* update focus */
          
           
            //ConsoleRTB1.Focus();


            // Assign the value of the plot to the RichTextBox.


   


        static string ReplaceHexadecimalSymbols(string txt)
        {
            string r = "[\x00-\x08\x0B\x0C\x0E-\x1F\x26]";
            return Regex.Replace(txt, r, "", RegexOptions.Compiled);
        }


        //char LF = (char)10;
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        StringBuilder sb3 = new StringBuilder();
        StringBuilder sb4 = new StringBuilder();
        StringBuilder sb5 = new StringBuilder();
        StringBuilder sb6 = new StringBuilder();
        StringBuilder sb7 = new StringBuilder();
        StringBuilder sb8 = new StringBuilder();
        StringBuilder sb9 = new StringBuilder();
        StringBuilder sb10 = new StringBuilder();
        StringBuilder sb11 = new StringBuilder();
        StringBuilder sb12 = new StringBuilder();
        StringBuilder sb13 = new StringBuilder();
        StringBuilder sb14 = new StringBuilder();
        StringBuilder sb15 = new StringBuilder();
        StringBuilder sb16 = new StringBuilder();

        public string currentline1;
        public string currentline2;
        public string currentline3;
        public string currentline4;
        public string currentline5;
        public string currentline6;
        public string currentline7;
        public string currentline8;
        public string currentline9;
        public string currentline10;
        public string currentline11;
        public string currentline12;
        public string currentline13;
        public string currentline14;
        public string currentline15;
        public string currentline16;

        //Serial Ports Recieved Events
        private void Recieve1(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort1, CommandTXT1, "prompt1", para1, FlowDoc1, ConsoleRTB1, sb1, currentline1, MsgSuccess1, MsgWarning1, MsgError1,MsgNormCPU1, MsgMedCPU1, MsgHighCPU1);
          
        }
        
        private void Recieve2(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort2, CommandTXT1, "prompt2", para2, FlowDoc2, ConsoleRTB2, sb2, currentline2, MsgSuccess2, MsgWarning2, MsgError2, MsgNormCPU2, MsgMedCPU2, MsgHighCPU2);
        }

        private void Recieve3(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort3, CommandTXT1, "prompt3", para3, FlowDoc3, ConsoleRTB3, sb3, currentline3, MsgSuccess3, MsgWarning3, MsgError3, MsgNormCPU3, MsgMedCPU3, MsgHighCPU3);
           
        }


        private void Recieve4(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort4, CommandTXT1, "prompt4", para4, FlowDoc4, ConsoleRTB4, sb4, currentline4, MsgSuccess4, MsgWarning4, MsgError4, MsgNormCPU4, MsgMedCPU4, MsgHighCPU4);

        }

      
        private void Recieve5(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort5, CommandTXT1, "prompt5", para5, FlowDoc5, ConsoleRTB5, sb5, currentline5, MsgSuccess5, MsgWarning5, MsgError5, MsgNormCPU5, MsgMedCPU5, MsgHighCPU5);
        }


        private void Recieve6(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort6, CommandTXT1, "prompt6", para6, FlowDoc6, ConsoleRTB6, sb6, currentline6, MsgSuccess6, MsgWarning6, MsgError6, MsgNormCPU6, MsgMedCPU6, MsgHighCPU6);
          
        }

        private void Recieve7(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort7, CommandTXT1, "prompt7", para7, FlowDoc7, ConsoleRTB7, sb7, currentline7, MsgSuccess7, MsgWarning7, MsgError7, MsgNormCPU7, MsgMedCPU7, MsgHighCPU7);
        }


        private void Recieve8(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort8, CommandTXT1, "prompt8", para8, FlowDoc8, ConsoleRTB8, sb8, currentline8, MsgSuccess8, MsgWarning8, MsgError8, MsgNormCPU8, MsgMedCPU8, MsgHighCPU8);
       
        }

      
        private void Recieve9(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort9, CommandTXT1, "prompt9", para9, FlowDoc9, ConsoleRTB9, sb9, currentline9, MsgSuccess9, MsgWarning9, MsgError9, MsgNormCPU9, MsgMedCPU9, MsgHighCPU9);
        }

     
        private void Recieve10(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort10, CommandTXT1, "prompt10", para10, FlowDoc10, ConsoleRTB10, sb10, currentline10, MsgSuccess10, MsgWarning10, MsgError10, MsgNormCPU10, MsgMedCPU10, MsgHighCPU10);
          
        }


        private void Recieve11(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort11, CommandTXT1, "prompt11", para11, FlowDoc11, ConsoleRTB11, sb11, currentline11, MsgSuccess11, MsgWarning11, MsgError11, MsgNormCPU11, MsgMedCPU11, MsgHighCPU11);
          
        }

      

        private void Recieve12(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort12, CommandTXT1, "prompt12", para12, FlowDoc12, ConsoleRTB12, sb12, currentline12, MsgSuccess12, MsgWarning12, MsgError12, MsgNormCPU12, MsgMedCPU12, MsgHighCPU12);
           
        }

      
        private void Recieve13(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort13, CommandTXT1, "prompt13", para13, FlowDoc13, ConsoleRTB13, sb13, currentline13, MsgSuccess13, MsgWarning13, MsgError13, MsgNormCPU13, MsgMedCPU13, MsgHighCPU13);
          
        }


        private void Recieve14(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort14, CommandTXT1, "prompt14", para14, FlowDoc14, ConsoleRTB14, sb14, currentline14, MsgSuccess14, MsgWarning14, MsgError14, MsgNormCPU14, MsgMedCPU14, MsgHighCPU14);
         
        }

        private void Recieve15(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort15, CommandTXT1, "prompt15", para15, FlowDoc15, ConsoleRTB15, sb15, currentline15, MsgSuccess15, MsgWarning15, MsgError15, MsgNormCPU15, MsgMedCPU15, MsgHighCPU15);
        }

        private void Recieve16(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //Call the Centralised Recieved and Write Methods in the ViewModel
            vm.RecieveData(_serialPort16, CommandTXT1, "prompt16", para16, FlowDoc16, ConsoleRTB16, sb16, currentline16, MsgSuccess16, MsgWarning16, MsgError16, MsgNormCPU16, MsgMedCPU16, MsgHighCPU16);
         
        }
    //******END Serial Recieved Events********


        private void SendCarriageReturn()
        {
            if (CurrentDataMode == DataMode.Text)
            {
                // Send the user's text straight out the port
                if (_serialPort1.IsOpen)
                    _serialPort1.Write("\r");
                if (_serialPort2.IsOpen)
                    _serialPort2.Write("\r");
                if (_serialPort3.IsOpen)
                    _serialPort3.Write("\r");
                if (_serialPort4.IsOpen)
                    _serialPort4.Write("\r");
                if (_serialPort5.IsOpen)
                    _serialPort5.Write("\r");
                if (_serialPort6.IsOpen)
                    _serialPort6.Write("\r");
                if (_serialPort7.IsOpen)
                    _serialPort7.Write("\r");
                if (_serialPort8.IsOpen)
                    _serialPort8.Write("\r");
                if (_serialPort9.IsOpen)
                    _serialPort9.Write("\r");
                if (_serialPort10.IsOpen)
                    _serialPort10.Write("\r");
                if (_serialPort11.IsOpen)
                    _serialPort11.Write("\r");
                if (_serialPort12.IsOpen)
                    _serialPort12.Write("\r");
                if (_serialPort13.IsOpen)
                    _serialPort13.Write("\r");
                if (_serialPort14.IsOpen)
                    _serialPort14.Write("\r");
                if (_serialPort15.IsOpen)
                    _serialPort15.Write("\r");
                if (_serialPort16.IsOpen)
                    _serialPort16.Write("\r");
                // Show in the terminal window the user's text
                // Log(LogMsgType.Outgoing, "\n");
            }
            else
            {
      
            }
            //textBox1.SelectAll();
        }


        private void SendDataSingle(string type, SerialPort serial, TextBox command)
        {


            //Check the Brand
            if (vm.ProductBrand == "Cisco" || vm.ProductBrand == "Mellanox")
            {
                switch (type)
                {
                    case "Normal":
                        //Check for just  a Carriage Return
                        if (string.IsNullOrEmpty(command.Text))
                        {
                            MainViewModel.CurrentCommand = "";
                            if (serial.IsOpen)
                                serial.Write("\r");
                        }
                        else
                        {
                            MainViewModel.CurrentCommand = command.Text;
                            if (command.Text.Contains("exit"))
                            {
                                //if exit command clear terminal before sending
                                //ClearTerminal(ConsoleRTB1);
                            }
                            if (serial.IsOpen)
                                serial.Write(command.Text + "\r");
                            //serial.Write(command.Text + "\r\r");
                        }
                        break;
                    //case "Space":
                    //    MainViewModel.CurrentCommand = "";

                    //    if (serial.IsOpen)
                    //        serial.WriteLine(string.Format("{0}", SPACE));
                    //    break;
                    case "CTRL]":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 29 }, 0, 1);
                        break;
                    case "CTRLB":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial .Write(new byte[] { 2 }, 0, 1);
                        break;
                    case "CTRLC":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial .Write(new byte[] { 3 }, 0, 1);
                        break;
                    case "CTRLD":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial .Write(new byte[] { 4 }, 0, 1);
                        break;
                    case "CTRLF":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial .Write(new byte[] { 6 }, 0, 1);
                        break;
                    case "CTRLP":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial .Write(new byte[] { 16 }, 0, 1);
                        break;
                    case "CTRLS":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 19 }, 0, 1);
                        break;
                    case "CTRLX":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.WriteLine(string.Format("{0}{1}", CtrlX, CR));
                        break;
                    case "CTRLZ":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial .Write(new byte[] { 26 }, 0, 1);
                        break;
                    case "SHIFTS":
                        MainViewModel.CurrentCommand = "";
                        //MessageBox.Show("Here");
                        if (serial.IsOpen)
                            serial.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        //_serialPort1.WriteLine(string.Format("{0}{1}", ModifierKeys.Shift, Key.S));
                        break;
                }

            }
            else if (vm.ProductBrand == "HP" || vm.ProductBrand == "Aruba")
            {
                //needs to go through the appendstring parser to strip out characters


                switch (type)
                {
                    case "Normal":
                        //Check for just  a Carriage Return
                        if (string.IsNullOrEmpty(command.Text))
                        {
                            MainViewModel.CurrentCommand = "";
                            // MessageBox.Show("HP Blank Return");
                            if (serial.IsOpen)
                                serial.Write("\r");

                        }
                        else
                        {
                            MainViewModel.CurrentCommand = command.Text;
                            if (serial.IsOpen)
                                serial.Write(command.Text.Trim() + "\r"); // + "\r");

                        }
                        break;
                    case "CTRL]":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 29 }, 0, 1);
                        break;
                    case "CTRLB":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 2 }, 0, 1);
                        break;
                    case "CTRLC":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 3 }, 0, 1);
                        break;
                    case "CTRLD":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 4 }, 0, 1);
                        break;
                    case "CTRLF":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 6 }, 0, 1);
                        break;
                    case "CTRLP":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 16 }, 0, 1);
                        break;
                    case "CTRLS":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 19 }, 0, 1);
                        break;
                    case "CTRLX":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 24 }, 0, 1);
                        break;
                    case "CTRLZ":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 26 }, 0, 1);
                        break;
                    case "SHIFTS":
                        MainViewModel.CurrentCommand = "";
                        //MessageBox.Show("Here");
                        if (serial.IsOpen)
                            serial.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        //_serialPort1.WriteLine(string.Format("{0}{1}", ModifierKeys.Shift, Key.S));
                        break;

                }//end switch
            }//end if
            else if (vm.ProductBrand == "Juniper" || vm.ProductBrand == "Netgear" || vm.ProductBrand == "Dell" || vm.ProductBrand == "Draytek" || vm.ProductBrand == "IBM")
            {
                //needs to go through the appendstring parser to strip out characters


                switch (type)
                {
                    case "Normal":
                        //Check for just  a Carriage Return
                        if (string.IsNullOrEmpty(command.Text))
                        {
                            MainViewModel.CurrentCommand = "";
                            // MessageBox.Show("HP Blank Return");
                            if (serial.IsOpen)
                                serial.Write("\r\r");

                        }
                        else
                        {
                            MainViewModel.CurrentCommand = command.Text;
                            if (serial.IsOpen)
                                serial.Write(command.Text.Trim() + "\r\r"); // + "\r");

                        }
                        break;
                    case "CTRL]":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 29 }, 0, 1);
                        break;
                    case "CTRLB":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 2 }, 0, 1);
                        break;
                    case "CTRLC":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 3 }, 0, 1);
                        break;
                    case "CTRLD":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 4 }, 0, 1);
                        break;
                    case "CTRLF":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 6 }, 0, 1);
                        break;
                    case "CTRLP":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 16 }, 0, 1);
                        break;
                    case "CTRLS":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 19 }, 0, 1);
                        break;
                    case "CTRLX":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 24 }, 0, 1);
                        break;
                    case "CTRLZ":
                        MainViewModel.CurrentCommand = "";

                        if (serial.IsOpen)
                            serial.Write(new byte[] { 26 }, 0, 1);
                        break;
                    case "SHIFTS":
                        MainViewModel.CurrentCommand = "";
                        //MessageBox.Show("Here");
                        if (serial.IsOpen)
                            serial.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        //_serialPort1.WriteLine(string.Format("{0}{1}", ModifierKeys.Shift, Key.S));
                        break;

                }//end switch
            }//end if

        }




        private void SendData(string type)
        {

            //Check the Brand
            if (vm.ProductBrand == "Cisco" || vm.ProductBrand == "Mellanox")
            {
                switch (type)
                {
                    case "Normal":
                        //Check for just  a Carriage Return "\r"
                        if (string.IsNullOrEmpty(CommandTXT1.Text))
                        {
                            MainViewModel.CurrentCommand = "";

                            if (_serialPort1.IsOpen)
                                _serialPort1.Write("\r");
                            if (_serialPort2.IsOpen)
                                _serialPort2.Write("\r");

                            if (_serialPort3.IsOpen)
                                _serialPort3.Write("\r");

                            if (_serialPort4.IsOpen)
                                _serialPort4.Write("\r");

                            if (_serialPort5.IsOpen)
                                _serialPort5.Write("\r");

                            if (_serialPort6.IsOpen)
                                _serialPort6.Write("\r");

                            if (_serialPort7.IsOpen)
                                _serialPort7.Write("\r");

                            if (_serialPort8.IsOpen)
                                _serialPort8.Write("\r");

                            if (_serialPort9.IsOpen)
                                _serialPort9.Write("\r");

                            if (_serialPort10.IsOpen)
                                _serialPort10.Write("\r");

                            if (_serialPort11.IsOpen)
                                _serialPort11.Write("\r");

                            if (_serialPort12.IsOpen)
                                _serialPort12.Write("\r");

                            if (_serialPort13.IsOpen)
                                _serialPort13.Write("\r");

                            if (_serialPort14.IsOpen)
                                _serialPort14.Write("\r");

                            if (_serialPort15.IsOpen)
                                _serialPort15.Write("\r");

                            if (_serialPort16.IsOpen)
                                _serialPort16.Write("\r");


                            CommandTXT1.Text = "";
                        }
                        else
                        {
                            MainViewModel.CurrentCommand = CommandTXT1.Text;

                            //if (CommandTXT1.Text.Contains("exit"))
                            //{
                            //    //if exit command clear terminal before sending
                            //    ClearTerminal();

                            //}

                            if (MainViewModel.CurrentCommand == "en" || MainViewModel.CurrentCommand == "enable" || MainViewModel.CurrentCommand == "exit")
                            {
                                //MessageBox.Show(CurrentCommand);
                                if (_serialPort1.IsOpen)
                                    _serialPort1.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort2.IsOpen)
                                    _serialPort2.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort3.IsOpen)
                                    _serialPort3.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort4.IsOpen)
                                    _serialPort4.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort5.IsOpen)
                                    _serialPort5.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort6.IsOpen)
                                    _serialPort6.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort7.IsOpen)
                                    _serialPort7.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort8.IsOpen)
                                    _serialPort8.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort9.IsOpen)
                                    _serialPort9.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort10.IsOpen)
                                    _serialPort10.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort11.IsOpen)
                                    _serialPort11.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort12.IsOpen)
                                    _serialPort12.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort13.IsOpen)
                                    _serialPort13.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort14.IsOpen)
                                    _serialPort14.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort15.IsOpen)
                                    _serialPort15.Write(CommandTXT1.Text + "\r\r");

                                if (_serialPort16.IsOpen)
                                    _serialPort16.Write(CommandTXT1.Text + "\r\r");
                            }
                            else
                            {
                                if (_serialPort1.IsOpen)
                                    _serialPort1.Write(CommandTXT1.Text + "\r");

                                if (_serialPort2.IsOpen)
                                    _serialPort2.Write(CommandTXT1.Text + "\r");

                                if (_serialPort3.IsOpen)
                                    _serialPort3.Write(CommandTXT1.Text + "\r");

                                if (_serialPort4.IsOpen)
                                    _serialPort4.Write(CommandTXT1.Text + "\r");

                                if (_serialPort5.IsOpen)
                                    _serialPort5.Write(CommandTXT1.Text + "\r");

                                if (_serialPort6.IsOpen)
                                    _serialPort6.Write(CommandTXT1.Text + "\r");

                                if (_serialPort7.IsOpen)
                                    _serialPort7.Write(CommandTXT1.Text + "\r");

                                if (_serialPort8.IsOpen)
                                    _serialPort8.Write(CommandTXT1.Text + "\r");

                                if (_serialPort9.IsOpen)
                                    _serialPort9.Write(CommandTXT1.Text + "\r");

                                if (_serialPort10.IsOpen)
                                    _serialPort10.Write(CommandTXT1.Text + "\r");

                                if (_serialPort11.IsOpen)
                                    _serialPort11.Write(CommandTXT1.Text + "\r");

                                if (_serialPort12.IsOpen)
                                    _serialPort12.Write(CommandTXT1.Text + "\r");

                                if (_serialPort13.IsOpen)
                                    _serialPort13.Write(CommandTXT1.Text + "\r");

                                if (_serialPort14.IsOpen)
                                    _serialPort14.Write(CommandTXT1.Text + "\r");

                                if (_serialPort15.IsOpen)
                                    _serialPort15.Write(CommandTXT1.Text + "\r");

                                if (_serialPort16.IsOpen)
                                    _serialPort16.Write(CommandTXT1.Text + "\r");
                            }




                            CommandTXT1.Text = "";
                            CommandTXT1.Clear();
                        }
                        break;
                        case "CTRL]":
                        MainViewModel.CurrentCommand = "";//

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 29 }, 0, 1);
                        break;
                    case "CTRLB":
                        MainViewModel.CurrentCommand = "";//

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 2 }, 0, 1);
                        break;
                    case "CTRLC":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 3 }, 0, 1);
                        break;
                    case "CTRLD":
                        MainViewModel.CurrentCommand = "";

                        
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 4 }, 0, 1);
                        break;
                    case "CTRLF":
                        MainViewModel.CurrentCommand = "";


                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 6 }, 0, 1);
                        break;
                    case "CTRLP":
                        MainViewModel.CurrentCommand = "";


                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 16 }, 0, 1);
                        break;
                    case "CTRLS":
                        MainViewModel.CurrentCommand = "";

                       

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 19 }, 0, 1);
                        break;
                    case "CTRLZ":
                        MainViewModel.CurrentCommand = "";

                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 26 }, 0, 1);
                        break;
                    case "SHIFTS":
                        MainViewModel.CurrentCommand = "";
                        if (_serialPort1.IsOpen)
                            _serialPort1.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort2.IsOpen)
                            _serialPort2.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort3.IsOpen)
                            _serialPort3.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort4.IsOpen)
                            _serialPort4.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort5.IsOpen)
                            _serialPort5.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort6.IsOpen)
                            _serialPort6.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort7.IsOpen)
                            _serialPort7.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort8.IsOpen)
                            _serialPort8.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort9.IsOpen)
                            _serialPort9.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort10.IsOpen)
                            _serialPort10.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort11.IsOpen)
                            _serialPort11.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort12.IsOpen)
                            _serialPort12.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort13.IsOpen)
                            _serialPort13.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort14.IsOpen)
                            _serialPort14.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort15.IsOpen)
                            _serialPort15.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort16.IsOpen)
                            _serialPort16.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        break;

                }
             
            }
            else if (vm.ProductBrand == "HP" || vm.ProductBrand == "Aruba")
            {
                //needs to go through the appendstring parser to strip out characters
                //SendData();
                //SendData();
                switch (type)
                {
                    case "Normal":
                        //Check for just  a Carriage Return
                        if (string.IsNullOrEmpty(CommandTXT1.Text))
                        {
                            MainViewModel.CurrentCommand = "";
                            // MessageBox.Show("HP Blank Return");"\r"
                            if (_serialPort1.IsOpen)
                                _serialPort1.Write("\r");

                            if (_serialPort2.IsOpen)
                                _serialPort2.Write("\r");

                            if (_serialPort3.IsOpen)
                                _serialPort3.Write("\r");

                            if (_serialPort4.IsOpen)
                                _serialPort4.Write("\r");

                            if (_serialPort5.IsOpen)
                                _serialPort5.Write("\r");

                            if (_serialPort6.IsOpen)
                                _serialPort6.Write("\r");

                            if (_serialPort7.IsOpen)
                                _serialPort7.Write("\r");

                            if (_serialPort8.IsOpen)
                                _serialPort8.Write("\r");

                            if (_serialPort9.IsOpen)
                                _serialPort9.Write("\r");

                            if (_serialPort10.IsOpen)
                                _serialPort10.Write("\r");

                            if (_serialPort11.IsOpen)
                                _serialPort11.Write("\r");

                            if (_serialPort12.IsOpen)
                                _serialPort12.Write("\r");

                            if (_serialPort13.IsOpen)
                                _serialPort13.Write("\r");

                            if (_serialPort14.IsOpen)
                                _serialPort14.Write("\r");

                            if (_serialPort15.IsOpen)
                                _serialPort15.Write("\r");

                            if (_serialPort16.IsOpen)
                                _serialPort16.Write("\r");

                        }
                        else
                        {
                            MainViewModel.CurrentCommand = CommandTXT1.Text;

                            if (_serialPort1.IsOpen)
                                _serialPort1.Write(CommandTXT1.Text + "\r"); // + "\r");

                            if (_serialPort2.IsOpen)
                                _serialPort2.Write(CommandTXT1.Text + "\r");

                            if (_serialPort3.IsOpen)
                                _serialPort3.Write(CommandTXT1.Text + "\r");

                            if (_serialPort4.IsOpen)
                                _serialPort4.Write(CommandTXT1.Text + "\r");

                            if (_serialPort5.IsOpen)
                                _serialPort5.Write(CommandTXT1.Text + "\r");

                            if (_serialPort6.IsOpen)
                                _serialPort6.Write(CommandTXT1.Text + "\r");

                            if (_serialPort7.IsOpen)
                                _serialPort7.Write(CommandTXT1.Text + "\r");

                            if (_serialPort8.IsOpen)
                                _serialPort8.Write(CommandTXT1.Text + "\r");

                            if (_serialPort9.IsOpen)
                                _serialPort9.Write(CommandTXT1.Text + "\r");

                            if (_serialPort10.IsOpen)
                                _serialPort10.Write(CommandTXT1.Text + "\r");

                            if (_serialPort11.IsOpen)
                                _serialPort11.Write(CommandTXT1.Text + "\r");

                            if (_serialPort12.IsOpen)
                                _serialPort12.Write(CommandTXT1.Text + "\r");

                            if (_serialPort13.IsOpen)
                                _serialPort13.Write(CommandTXT1.Text + "\r");

                            if (_serialPort14.IsOpen)
                                _serialPort14.Write(CommandTXT1.Text + "\r");

                            if (_serialPort15.IsOpen)
                                _serialPort15.Write(CommandTXT1.Text + "\r");

                            if (_serialPort16.IsOpen)
                                _serialPort16.Write(CommandTXT1.Text + "\r");


                            CommandTXT1.Text = "";
                            CommandTXT1.Clear();
                        }
                        break;
                    case "CTRL]":
                        MainViewModel.CurrentCommand = "";//

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 29 }, 0, 1);
                        break;
                    case "CTRLB":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 2 }, 0, 1);

                     
                        break;
                    case "CTRLC":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 3 }, 0, 1);
                        break;
                    case "CTRLD":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 4 }, 0, 1);
                        break;
                    case "CTRLF":
                        MainViewModel.CurrentCommand = "";
                   

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 6 }, 0, 1);
                        break;
                    case "CTRLP":
                        MainViewModel.CurrentCommand = "";


                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 16 }, 0, 1);
                        break;
                    case "CTRLS":
                        MainViewModel.CurrentCommand = "";



                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 19 }, 0, 1);
                        break;
                    case "CTRLZ":
                        MainViewModel.CurrentCommand = "";
                     
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 26 }, 0, 1);
                        break;
                    case "SHIFTS":
                        MainViewModel.CurrentCommand = "";
                        if (_serialPort1.IsOpen)
                            _serialPort1.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort2.IsOpen)
                            _serialPort2.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort3.IsOpen)
                            _serialPort3.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort4.IsOpen)
                            _serialPort4.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort5.IsOpen)
                            _serialPort5.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort6.IsOpen)
                            _serialPort6.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort7.IsOpen)
                            _serialPort7.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort8.IsOpen)
                            _serialPort8.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort9.IsOpen)
                            _serialPort9.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort10.IsOpen)
                            _serialPort10.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort11.IsOpen)
                            _serialPort11.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort12.IsOpen)
                            _serialPort12.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort13.IsOpen)
                            _serialPort13.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort14.IsOpen)
                            _serialPort14.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort15.IsOpen)
                            _serialPort15.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort16.IsOpen)
                            _serialPort16.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        break;

                }//end switch

            }//end if
            else if (vm.ProductBrand == "Juniper" || vm.ProductBrand == "Netgear" || vm.ProductBrand == "Dell" || vm.ProductBrand == "Draytek" || vm.ProductBrand == "IBM")
            {
                //needs to go through the appendstring parser to strip out characters
                //SendData();
                //SendData();
                switch (type)
                {
                    case "Normal":
                        //Check for just  a Carriage Return
                        if (string.IsNullOrEmpty(CommandTXT1.Text))
                        {
                            MainViewModel.CurrentCommand = "";
                            // MessageBox.Show("HP Blank Return");
                            if (_serialPort1.IsOpen)
                                _serialPort1.Write("\r");

                            if (_serialPort2.IsOpen)
                                _serialPort2.Write("\r");

                            if (_serialPort3.IsOpen)
                                _serialPort3.Write("\r");

                            if (_serialPort4.IsOpen)
                                _serialPort4.Write("\r");

                            if (_serialPort5.IsOpen)
                                _serialPort5.Write("\r");

                            if (_serialPort6.IsOpen)
                                _serialPort6.Write("\r");

                            if (_serialPort7.IsOpen)
                                _serialPort7.Write("\r");

                            if (_serialPort8.IsOpen)
                                _serialPort8.Write("\r");

                            if (_serialPort9.IsOpen)
                                _serialPort9.Write("\r");

                            if (_serialPort10.IsOpen)
                                _serialPort10.Write("\r");

                            if (_serialPort11.IsOpen)
                                _serialPort11.Write("\r");

                            if (_serialPort12.IsOpen)
                                _serialPort12.Write("\r");

                            if (_serialPort13.IsOpen)
                                _serialPort13.Write("\r");

                            if (_serialPort14.IsOpen)
                                _serialPort14.Write("\r");

                            if (_serialPort15.IsOpen)
                                _serialPort15.Write("\r");

                            if (_serialPort16.IsOpen)
                                _serialPort16.Write("\r");

                        }
                        else
                        {
                            MainViewModel.CurrentCommand = CommandTXT1.Text;

                            if (_serialPort1.IsOpen)
                                _serialPort1.Write(CommandTXT1.Text + "\r"); // + "\r");

                            if (_serialPort2.IsOpen)
                                _serialPort2.Write(CommandTXT1.Text + "\r");

                            if (_serialPort3.IsOpen)
                                _serialPort3.Write(CommandTXT1.Text + "\r");

                            if (_serialPort4.IsOpen)
                                _serialPort4.Write(CommandTXT1.Text + "\r");

                            if (_serialPort5.IsOpen)
                                _serialPort5.Write(CommandTXT1.Text + "\r");

                            if (_serialPort6.IsOpen)
                                _serialPort6.Write(CommandTXT1.Text + "\r");

                            if (_serialPort7.IsOpen)
                                _serialPort7.Write(CommandTXT1.Text + "\r");

                            if (_serialPort8.IsOpen)
                                _serialPort8.Write(CommandTXT1.Text + "\r");

                            if (_serialPort9.IsOpen)
                                _serialPort9.Write(CommandTXT1.Text + "\r");

                            if (_serialPort10.IsOpen)
                                _serialPort10.Write(CommandTXT1.Text + "\r");

                            if (_serialPort11.IsOpen)
                                _serialPort11.Write(CommandTXT1.Text + "\r");

                            if (_serialPort12.IsOpen)
                                _serialPort12.Write(CommandTXT1.Text + "\r");

                            if (_serialPort13.IsOpen)
                                _serialPort13.Write(CommandTXT1.Text + "\r");

                            if (_serialPort14.IsOpen)
                                _serialPort14.Write(CommandTXT1.Text + "\r");

                            if (_serialPort15.IsOpen)
                                _serialPort15.Write(CommandTXT1.Text + "\r");

                            if (_serialPort16.IsOpen)
                                _serialPort16.Write(CommandTXT1.Text + "\r");

                            CommandTXT1.Text = "";
                            CommandTXT1.Clear();

                        }
                        break;
                    case "CTRL]":
                        MainViewModel.CurrentCommand = "";//

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 29 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 29 }, 0, 1);
                        break;
                    case "CTRLB":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 2 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 2 }, 0, 1);


                        break;
                    case "CTRLC":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 3 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 3 }, 0, 1);
                        break;
                    case "CTRLD":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 4 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 4 }, 0, 1);
                        break;
                    case "CTRLF":
                        MainViewModel.CurrentCommand = "";


                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 6 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 6 }, 0, 1);
                        break;
                    case "CTRLP":
                        MainViewModel.CurrentCommand = "";


                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 16 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 16 }, 0, 1);
                        break;
                    case "CTRLS":
                        MainViewModel.CurrentCommand = "";



                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 19 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 19 }, 0, 1);
                        break;
                    case "CTRLZ":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort2.IsOpen)
                            _serialPort2.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort3.IsOpen)
                            _serialPort3.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort4.IsOpen)
                            _serialPort4.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort5.IsOpen)
                            _serialPort5.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort6.IsOpen)
                            _serialPort6.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort7.IsOpen)
                            _serialPort7.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort8.IsOpen)
                            _serialPort8.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort9.IsOpen)
                            _serialPort9.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort10.IsOpen)
                            _serialPort10.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort11.IsOpen)
                            _serialPort11.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort12.IsOpen)
                            _serialPort12.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort13.IsOpen)
                            _serialPort13.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort14.IsOpen)
                            _serialPort14.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort15.IsOpen)
                            _serialPort15.Write(new byte[] { 26 }, 0, 1);
                        if (_serialPort16.IsOpen)
                            _serialPort16.Write(new byte[] { 26 }, 0, 1);
                        break;
                    case "SHIFTS":
                        MainViewModel.CurrentCommand = "";
                        if (_serialPort1.IsOpen)
                           
                        if (_serialPort2.IsOpen)
                            _serialPort2.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort3.IsOpen)
                            _serialPort3.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort4.IsOpen)
                            _serialPort4.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort5.IsOpen)
                            _serialPort5.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort6.IsOpen)
                            _serialPort6.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort7.IsOpen)
                            _serialPort7.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort8.IsOpen)
                            _serialPort8.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort9.IsOpen)
                            _serialPort9.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort10.IsOpen)
                            _serialPort10.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort11.IsOpen)
                            _serialPort11.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort12.IsOpen)
                            _serialPort12.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort13.IsOpen)
                            _serialPort13.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort14.IsOpen)
                            _serialPort14.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort15.IsOpen)
                            _serialPort15.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        if (_serialPort16.IsOpen)
                            _serialPort16.WriteLine(string.Format("{0}{1}", "\x10", "\x53"));
                        break;

                }//end switch

            }//end if



        }


       



        public void OpenSinglePort(List<SerialPort> comlst, String portname, String baud1, String parity1, String databit1, String stopbit1, String handshake1)
        {

            bool error = false;

            //MessageBox.Show(portname + " " + baud1 + " " + parity1 + " " + databit1 + " " + stopbit1 + " " + handshake1);

            foreach (var cp in comlst)
            {
                // If the port is open, close it.
                if (cp.IsOpen)
                    cp.Close();
                else
                {
                    //Apply Selection Settings To Each Serial
                    cp.PortName = portname;
                    cp.BaudRate = Convert.ToInt32(baud1);
                    cp.Parity = (Parity)Enum.Parse(typeof(Parity), parity1);
                    cp.DataBits = Convert.ToInt32(databit1);
                    cp.StopBits = (StopBits)Enum.Parse(typeof(StopBits), stopbit1);
                    cp.Handshake = (Handshake)Enum.Parse(typeof(Handshake), handshake1);


                    try
                    {
                        // Open the port
                        cp.Open();
                    }
                    catch (UnauthorizedAccessException) { error = true; Console.WriteLine(error.ToString()); }
                    catch (IOException) { error = true; Console.WriteLine(error.ToString()); }
                    catch (ArgumentException) { error = true; Console.WriteLine(error.ToString()); }

                    //if (error) MessageBox.Show(this, "Could not open the COM port.  Most likely it is already in use, has been removed, or is unavailable.", "COM Port Unavalible", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //else
                    //{
                    //    // Show the initial pin states
                    //    UpdatePinState();
                    //    //chkDTR.Checked = comport.DtrEnable;
                    //    //chkRTS.Checked = comport.RtsEnable;
                    //}
                }


                // If the port is open, send focus to the send data box
                if (cp.IsOpen)
                {
                    // textBox1.Focus();
                    //if (chkClearOnOpen.Checked) ClearTerminal();
                    ClearTerminal();
                }
            }



        }




        //Open Multiple Ports at Once
        public void OpenMultiPorts(List<SerialPort> comlst, string portname, string baud, string parity, string databit, string stopbit, string handshake)
        {

            bool error = false;

            foreach (var cp in comlst)
            {
                // If the port is open, close it.
                if (cp.IsOpen)
                    cp.Close();
                else
                {
                    // Set the port's settings
                    //cp.PortName = comboBox1.Text;
                    cp.BaudRate = int.Parse(baud);
                    cp.Parity = (Parity)Enum.Parse(typeof(Parity), parity);
                    cp.DataBits = int.Parse(databit);
                    cp.StopBits = (StopBits)Enum.Parse(typeof(StopBits), stopbit);



                    try
                    {
                        // Open the port
                        cp.Open();
                    
                    }
                    catch (UnauthorizedAccessException) { error = true; Console.WriteLine(error.ToString()); }
                    catch (IOException) { error = true; Console.WriteLine(error.ToString()); }
                    catch (ArgumentException) { error = true; Console.WriteLine(error.ToString()); }

                    //if (error) MessageBox.Show(this, "Could not open the COM port.  Most likely it is already in use, has been removed, or is unavailable.", "COM Port Unavalible", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //else
                    //{
                    //    // Show the initial pin states
                    //    UpdatePinState();
                    //    //chkDTR.Checked = comport.DtrEnable;
                    //    //chkRTS.Checked = comport.RtsEnable;
                    //}
                }


                // If the port is open, send focus to the send data box
                if (cp.IsOpen)
                {
                    cp.Write("\r");
                    // textBox1.Focus();
                    //if (chkClearOnOpen.Checked) ClearTerminal();
                    ClearTerminal();
                }
            }

                //just send a quick enter
                SendData("Normal");
           
        }


        private void ClearTerminal()
        {
            //Clear the RichText Console Window
            ConsoleRTB1.SelectAll();
            ConsoleRTB1.Cut();
            ConsoleRTB2.SelectAll();
            ConsoleRTB2.Cut();
            ConsoleRTB3.SelectAll();
            ConsoleRTB3.Cut();
            ConsoleRTB4.SelectAll();
            ConsoleRTB4.Cut();
            ConsoleRTB5.SelectAll();
            ConsoleRTB5.Cut();
            ConsoleRTB6.SelectAll();
            ConsoleRTB6.Cut();
            ConsoleRTB7.SelectAll();
            ConsoleRTB7.Cut();
            ConsoleRTB8.SelectAll();
            ConsoleRTB8.Cut();
            ConsoleRTB9.SelectAll();
            ConsoleRTB9.Cut();
            ConsoleRTB10.SelectAll();
            ConsoleRTB10.Cut();
            ConsoleRTB11.SelectAll();
            ConsoleRTB11.Cut();
            ConsoleRTB12.SelectAll();
            ConsoleRTB12.Cut();
            ConsoleRTB13.SelectAll();
            ConsoleRTB13.Cut();
            ConsoleRTB14.SelectAll();
            ConsoleRTB14.Cut();
            ConsoleRTB15.SelectAll();
            ConsoleRTB15.Cut();
            ConsoleRTB16.SelectAll();
            ConsoleRTB16.Cut();

            //Clear the RichText Console Window
            ConsoleRTB1.Document.Blocks.Clear();
            ConsoleRTB2.Document.Blocks.Clear();
            ConsoleRTB3.Document.Blocks.Clear();
            ConsoleRTB4.Document.Blocks.Clear();
            ConsoleRTB5.Document.Blocks.Clear();
            ConsoleRTB6.Document.Blocks.Clear();
            ConsoleRTB7.Document.Blocks.Clear();
            ConsoleRTB8.Document.Blocks.Clear();
            ConsoleRTB9.Document.Blocks.Clear();
            ConsoleRTB10.Document.Blocks.Clear();
            ConsoleRTB11.Document.Blocks.Clear();
            ConsoleRTB12.Document.Blocks.Clear();
            ConsoleRTB13.Document.Blocks.Clear();
            ConsoleRTB14.Document.Blocks.Clear();
            ConsoleRTB15.Document.Blocks.Clear();
            ConsoleRTB16.Document.Blocks.Clear();

            FlowDoc1.Blocks.Clear();
            FlowDoc2.Blocks.Clear();
            FlowDoc3.Blocks.Clear();
            FlowDoc4.Blocks.Clear();
            FlowDoc5.Blocks.Clear();
            FlowDoc6.Blocks.Clear();
            FlowDoc7.Blocks.Clear();
            FlowDoc8.Blocks.Clear();
            FlowDoc9.Blocks.Clear();
            FlowDoc10.Blocks.Clear();
            FlowDoc11.Blocks.Clear();
            FlowDoc12.Blocks.Clear();
            FlowDoc13.Blocks.Clear();
            FlowDoc14.Blocks.Clear();
            FlowDoc15.Blocks.Clear();
            FlowDoc16.Blocks.Clear();

            ConsoleIncoming1 = string.Empty;
            ConsoleIncoming2 = string.Empty;
            ConsoleIncoming3 = string.Empty;
            ConsoleIncoming4 = string.Empty;
            ConsoleIncoming5 = string.Empty;
            ConsoleIncoming6 = string.Empty;
            ConsoleIncoming7 = string.Empty;
            ConsoleIncoming8 = string.Empty;
            ConsoleIncoming9 = string.Empty;
            ConsoleIncoming10 = string.Empty;
            ConsoleIncoming11 = string.Empty;
            ConsoleIncoming12 = string.Empty;
            ConsoleIncoming13 = string.Empty;
            ConsoleIncoming14 = string.Empty;
            ConsoleIncoming15 = string.Empty;
            ConsoleIncoming16 = string.Empty;

            //Clear the flash files
            LB1.Items.Clear();
            LB2.Items.Clear();
            LB3.Items.Clear();
            LB4.Items.Clear();
            LB5.Items.Clear();
            LB6.Items.Clear();
            LB7.Items.Clear();
            LB8.Items.Clear();
            LB9.Items.Clear();
            LB10.Items.Clear();
            LB11.Items.Clear();
            LB12.Items.Clear();
            LB13.Items.Clear();
            LB14.Items.Clear();
            LB15.Items.Clear();
            LB16.Items.Clear();

            //RESET MESSAGE TEXTBLOCKS
            MsgSuccess1.Text = "";
            MsgSuccess1.Background = Brushes.Transparent;
            MsgSuccess1.Padding = new Thickness(2);
            MsgSuccess2.Text = "";
            MsgSuccess2.Background = Brushes.Transparent;
            MsgSuccess2.Padding = new Thickness(2);
            MsgSuccess3.Text = "";
            MsgSuccess3.Background = Brushes.Transparent;
            MsgSuccess3.Padding = new Thickness(2);
            MsgSuccess4.Text = "";
            MsgSuccess4.Background = Brushes.Transparent;
            MsgSuccess4.Padding = new Thickness(2);
            MsgSuccess5.Text = "";
            MsgSuccess5.Background = Brushes.Transparent;
            MsgSuccess5.Padding = new Thickness(2);
            MsgSuccess6.Text = "";
            MsgSuccess6.Background = Brushes.Transparent;
            MsgSuccess6.Padding = new Thickness(2);
            MsgSuccess7.Text = "";
            MsgSuccess7.Background = Brushes.Transparent;
            MsgSuccess7.Padding = new Thickness(2);
            MsgSuccess8.Text = "";
            MsgSuccess8.Background = Brushes.Transparent;
            MsgSuccess8.Padding = new Thickness(2);
            MsgSuccess9.Text = "";
            MsgSuccess9.Background = Brushes.Transparent;
            MsgSuccess9.Padding = new Thickness(2);
            MsgSuccess10.Text = "";
            MsgSuccess10.Background = Brushes.Transparent;
            MsgSuccess10.Padding = new Thickness(2);
            MsgSuccess11.Text = "";
            MsgSuccess11.Background = Brushes.Transparent;
            MsgSuccess11.Padding = new Thickness(2);
            MsgSuccess12.Text = "";
            MsgSuccess12.Background = Brushes.Transparent;
            MsgSuccess12.Padding = new Thickness(2);
            MsgSuccess13.Text = "";
            MsgSuccess13.Background = Brushes.Transparent;
            MsgSuccess13.Padding = new Thickness(2);
            MsgSuccess14.Text = "";
            MsgSuccess14.Background = Brushes.Transparent;
            MsgSuccess14.Padding = new Thickness(2);
            MsgSuccess15.Text = "";
            MsgSuccess15.Background = Brushes.Transparent;
            MsgSuccess15.Padding = new Thickness(2);
            MsgSuccess16.Text = "";
            MsgSuccess16.Background = Brushes.Transparent;
            MsgSuccess16.Padding = new Thickness(2);
            //WARNINGS
            MsgWarning1.Text = "";
            MsgWarning1.Background = Brushes.Transparent;
            MsgWarning1.Padding = new Thickness(2);
            MsgWarning2.Text = "";
            MsgWarning2.Background = Brushes.Transparent;
            MsgWarning2.Padding = new Thickness(2);
            MsgWarning3.Text = "";
            MsgWarning3.Background = Brushes.Transparent;
            MsgWarning3.Padding = new Thickness(2);
            MsgWarning4.Text = "";
            MsgWarning4.Background = Brushes.Transparent;
            MsgWarning4.Padding = new Thickness(2);
            MsgWarning5.Text = "";
            MsgWarning5.Background = Brushes.Transparent;
            MsgWarning5.Padding = new Thickness(2);
            MsgWarning6.Text = "";
            MsgWarning6.Background = Brushes.Transparent;
            MsgWarning6.Padding = new Thickness(2);
            MsgWarning7.Text = "";
            MsgWarning7.Background = Brushes.Transparent;
            MsgWarning7.Padding = new Thickness(2);
            MsgWarning8.Text = "";
            MsgWarning8.Background = Brushes.Transparent;
            MsgWarning8.Padding = new Thickness(2);
            MsgWarning9.Text = "";
            MsgWarning9.Background = Brushes.Transparent;
            MsgWarning9.Padding = new Thickness(2);
            MsgWarning10.Text = "";
            MsgWarning10.Background = Brushes.Transparent;
            MsgWarning10.Padding = new Thickness(2);
            MsgWarning11.Text = "";
            MsgWarning11.Background = Brushes.Transparent;
            MsgWarning11.Padding = new Thickness(2);
            MsgWarning12.Text = "";
            MsgWarning12.Background = Brushes.Transparent;
            MsgWarning12.Padding = new Thickness(2);
            MsgWarning13.Text = "";
            MsgWarning13.Background = Brushes.Transparent;
            MsgWarning13.Padding = new Thickness(2);
            MsgWarning14.Text = "";
            MsgWarning14.Background = Brushes.Transparent;
            MsgWarning14.Padding = new Thickness(2);
            MsgWarning15.Text = "";
            MsgWarning15.Background = Brushes.Transparent;
            MsgWarning15.Padding = new Thickness(2);
            MsgWarning16.Text = "";
            MsgWarning16.Background = Brushes.Transparent;
            MsgWarning16.Padding = new Thickness(2);
            //ERRORS
            MsgError1.Text = "";
            MsgError1.Background = Brushes.Transparent;
            MsgError1.Padding = new Thickness(2);
            MsgError2.Text = "";
            MsgError2.Background = Brushes.Transparent;
            MsgError2.Padding = new Thickness(2);
            MsgError3.Text = "";
            MsgError3.Background = Brushes.Transparent;
            MsgError3.Padding = new Thickness(2);
            MsgError4.Text = "";
            MsgError4.Background = Brushes.Transparent;
            MsgError4.Padding = new Thickness(2);
            MsgError5.Text = "";
            MsgError5.Background = Brushes.Transparent;
            MsgError5.Padding = new Thickness(2);
            MsgError6.Text = "";
            MsgError6.Background = Brushes.Transparent;
            MsgError6.Padding = new Thickness(2);
            MsgError7.Text = "";
            MsgError7.Background = Brushes.Transparent;
            MsgError7.Padding = new Thickness(2);
            MsgError8.Text = "";
            MsgError8.Background = Brushes.Transparent;
            MsgError8.Padding = new Thickness(2);
            MsgError9.Text = "";
            MsgError9.Background = Brushes.Transparent;
            MsgError9.Padding = new Thickness(2);
            MsgError10.Text = "";
            MsgError10.Background = Brushes.Transparent;
            MsgError10.Padding = new Thickness(2);
            MsgError11.Text = "";
            MsgError11.Background = Brushes.Transparent;
            MsgError11.Padding = new Thickness(2);
            MsgError12.Text = "";
            MsgError12.Background = Brushes.Transparent;
            MsgError12.Padding = new Thickness(2);
            MsgError13.Text = "";
            MsgError13.Background = Brushes.Transparent;
            MsgError13.Padding = new Thickness(2);
            MsgError14.Text = "";
            MsgError14.Background = Brushes.Transparent;
            MsgError14.Padding = new Thickness(2);
            MsgError15.Text = "";
            MsgError15.Background = Brushes.Transparent;
            MsgError15.Padding = new Thickness(2);
            MsgError16.Text = "";
            MsgError16.Background = Brushes.Transparent;
            MsgError16.Padding = new Thickness(2);



            MsgNormCPU1.Text = "";
            MsgNormCPU2.Text = "";
            MsgNormCPU3.Text = "";
            MsgNormCPU4.Text = "";
            MsgNormCPU5.Text = "";
            MsgNormCPU6.Text = "";
            MsgNormCPU7.Text = "";
            MsgNormCPU8.Text = "";
            MsgNormCPU9.Text = "";
            MsgNormCPU10.Text = "";
            MsgNormCPU11.Text = "";
            MsgNormCPU12.Text = "";
            MsgNormCPU13.Text = "";
            MsgNormCPU14.Text = "";
            MsgNormCPU15.Text = "";
            MsgNormCPU16.Text = "";

            MsgMedCPU1.Text = "";
            MsgMedCPU2.Text = "";
            MsgMedCPU3.Text = "";
            MsgMedCPU4.Text = "";
            MsgMedCPU5.Text = "";
            MsgMedCPU6.Text = "";
            MsgMedCPU7.Text = "";
            MsgMedCPU8.Text = "";
            MsgMedCPU9.Text = "";
            MsgMedCPU10.Text = "";
            MsgMedCPU11.Text = "";
            MsgMedCPU12.Text = "";
            MsgMedCPU13.Text = "";
            MsgMedCPU14.Text = "";
            MsgMedCPU15.Text = "";
            MsgMedCPU16.Text = "";


            MsgHighCPU1.Text = "";
            MsgHighCPU2.Text = "";
            MsgHighCPU3.Text = "";
            MsgHighCPU4.Text = "";
            MsgHighCPU5.Text = "";
            MsgHighCPU6.Text = "";
            MsgHighCPU7.Text = "";
            MsgHighCPU8.Text = "";
            MsgHighCPU9.Text = "";
            MsgHighCPU10.Text = "";
            MsgHighCPU11.Text = "";
            MsgHighCPU12.Text = "";
            MsgHighCPU13.Text = "";
            MsgHighCPU14.Text = "";
            MsgHighCPU15.Text = "";
            MsgHighCPU16.Text = "";

            //clear the serial completeline properties
            vm.SerialCompleteLine1 = "";
            vm.SerialCompleteLine2 = "";
            vm.SerialCompleteLine3 = "";
            vm.SerialCompleteLine4 = "";
            vm.SerialCompleteLine5 = "";
            vm.SerialCompleteLine6 = "";
            vm.SerialCompleteLine7 = "";
            vm.SerialCompleteLine8 = "";
            vm.SerialCompleteLine9 = "";
            vm.SerialCompleteLine10 = "";
            vm.SerialCompleteLine11 = "";
            vm.SerialCompleteLine12 = "";
            vm.SerialCompleteLine13 = "";
            vm.SerialCompleteLine14 = "";
            vm.SerialCompleteLine15 = "";
            vm.SerialCompleteLine16 = "";

            //CommandTXT1.Focus();
            ConsoleRTB1.Focus();

            //set isolation selection
            //IsolateComPortCB.SelectedIndex = -1;
            CommandTXT1.Text = "";
            //vm.CMDHistory.Clear();
        }

           

        private void CommandTXT1_TextChanged(object sender, TextChangedEventArgs e)
        {
            //MessageBox.Show(sender.ToString());
        }

        private void CommandTXT1_PreviewKeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Up)
            {
                //Send Data To Serial Port
                //add command to list

                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            //MessageBox.Show(historicCMD);
                            CommandTXT1.Text = historicCMD;
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            //MessageBox.Show(historicCMD);
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }
            }
        }

        private void CommandTXT1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if(CommandTXT1.Text != "")
                    {
                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }
                   
                }


                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if(IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                           
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                  
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                 
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                  
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                   
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                  
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Text = "";
                            CommandTXT1.Clear();
                        }
                  
                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Text = "";
                        CommandTXT1.Clear();
                    }

             
                  

                }
                else
                {
                    CommandTXT1.Clear();
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }
            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
                //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }
            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }

        private void OpenPortBtn_Click(object sender, RoutedEventArgs e)
        {
            //Clear ports before adding again
            prts.Clear();
            //Add ports
            prts.Add(_serialPort1);
            prts.Add(_serialPort2);
            prts.Add(_serialPort3);
            prts.Add(_serialPort4);
            prts.Add(_serialPort5);
            prts.Add(_serialPort6);
            prts.Add(_serialPort7);
            prts.Add(_serialPort8);
            prts.Add(_serialPort9);
            prts.Add(_serialPort10);
            prts.Add(_serialPort11);
            prts.Add(_serialPort12);
            prts.Add(_serialPort13);
            prts.Add(_serialPort14);
            prts.Add(_serialPort15);
            prts.Add(_serialPort16);


            if (ComPortCB.SelectedItem == null && BaudCB.SelectedItem == null && ParityCB.SelectedItem == null && DataBitsCB.SelectedItem == null && StopBitsCB.SelectedItem == null && HandShakeCB.SelectedItem == null)
            {
                MessageBox.Show("Please select a COM Port Settings!");
            }
            else if (ComPortCB.SelectedItem == null)
            {
                MessageBox.Show("Please select a COM Port!");
            }
            else
            {
                //OpenSinglePort(prts, ComPortCB.Text, BaudCB.Text, ParityCB.Text, DataBitsCB.Text, StopBitsCB.Text, HandShakeCB.Text);
                //Set on\off indicator color

                //OnOff.Fill = Brushes.Green;
                //MultiPort Version
                OpenMultiPorts(prts, ComPortCB.Text, BaudCB.Text, ParityCB.Text, DataBitsCB.Text, StopBitsCB.Text, HandShakeCB.Text);
            }


            //Single
            //OpenSinglePort(prts, ComPortCB.Text, BaudCB.Text, ParityCB.Text, DataBitsCB.Text, StopBitsCB.Text, HandShakeCB.Text);
            //Set on\off indicator color
            if (_serialPort1.IsOpen == true)
                OnOff1.Fill = Brushes.Green;
            if (_serialPort2.IsOpen == true)
                OnOff2.Fill = Brushes.Green;
            if (_serialPort3.IsOpen == true)
                OnOff3.Fill = Brushes.Green;
            if (_serialPort4.IsOpen == true)
                OnOff4.Fill = Brushes.Green;
            if (_serialPort5.IsOpen == true)
                OnOff5.Fill = Brushes.Green;
            if (_serialPort6.IsOpen == true)
                OnOff6.Fill = Brushes.Green;
            if (_serialPort7.IsOpen == true)
                OnOff7.Fill = Brushes.Green;
            if (_serialPort8.IsOpen == true)
                OnOff8.Fill = Brushes.Green;
            if (_serialPort9.IsOpen == true)
                OnOff9.Fill = Brushes.Green;
            if (_serialPort10.IsOpen == true)
                OnOff10.Fill = Brushes.Green;
            if (_serialPort11.IsOpen == true)
                OnOff11.Fill = Brushes.Green;
            if (_serialPort12.IsOpen == true)
                OnOff12.Fill = Brushes.Green;
            if (_serialPort13.IsOpen == true)
                OnOff13.Fill = Brushes.Green;
            if (_serialPort14.IsOpen == true)
                OnOff14.Fill = Brushes.Green;
            if (_serialPort15.IsOpen == true)
                OnOff15.Fill = Brushes.Green;
            if (_serialPort16.IsOpen == true)
                OnOff16.Fill = Brushes.Green;


            // set screen offset
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("1-4"));
            //change background of selected bank
            Bank1.Background = Brushes.MediumPurple;
            Bank2.Background = (Brush)FindResource("TechBuyerBrush1");
            Bank3.Background = (Brush)FindResource("TechBuyerBrush1");
            Bank4.Background = (Brush)FindResource("TechBuyerBrush1");



        }




        private void LoadSettings()
        {
        //Load application settings
            BaudCB.SelectedIndex = BaudCB.Items.IndexOf("9600");
            ParityCB.SelectedIndex = ParityCB.Items.IndexOf("None");
            DataBitsCB.SelectedIndex = DataBitsCB.Items.IndexOf("8");
            StopBitsCB.SelectedIndex = StopBitsCB.Items.IndexOf("One");
            HandShakeCB.SelectedIndex = HandShakeCB.Items.IndexOf("None");
        }

        private void SaveSettings()
        {

            //settings.PortName = "COM5";
            settings.BaudRate = "9600";
            settings.Parity = "None";
            settings.DataBits = "8";
            settings.StopBits = "One";
            settings.HandShake = "None";

            //settings.ClearOnOpen = chkClearOnOpen.Checked;
            //settings.ClearWithDTR = chkClearWithDTR.Checked;

            settings.Save();
        }

        private void ClosePortBtn_Click(object sender, RoutedEventArgs e)
        {

            //Close all serial ports added to the collection
            CloseAllSerialPorts();

        }

        public void CloseAllSerialPorts()
        {
            //Loop through ports list and close if open
            foreach (var sp in prts)
            {
                try
                {
                    if (sp.IsOpen)
                    {
                        sp.Close();
                        //ConsoleContent1 = string.Empty;

                     
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }

            ClearTerminal();
            CommandTXT1.Clear();
            CommandTXT1.Focus();

            //    //Set on\off indicator color
            if (_serialPort1.IsOpen == false)
                OnOff1.Fill = Brushes.Red;
            if (_serialPort2.IsOpen == false)
                OnOff2.Fill = Brushes.Red;
            if (_serialPort3.IsOpen == false)
                OnOff3.Fill = Brushes.Red;
            if (_serialPort4.IsOpen == false)
                OnOff4.Fill = Brushes.Red;
            if (_serialPort5.IsOpen == false)
                OnOff5.Fill = Brushes.Red;
            if (_serialPort6.IsOpen == false)
                OnOff6.Fill = Brushes.Red;
            if (_serialPort7.IsOpen == false)
                OnOff7.Fill = Brushes.Red;
            if (_serialPort8.IsOpen == false)
                OnOff8.Fill = Brushes.Red;
            if (_serialPort9.IsOpen == false)
                OnOff9.Fill = Brushes.Red;
            if (_serialPort10.IsOpen == false)
                OnOff10.Fill = Brushes.Red;
            if (_serialPort11.IsOpen == false)
                OnOff11.Fill = Brushes.Red;
            if (_serialPort12.IsOpen == false)
                OnOff12.Fill = Brushes.Red;
            if (_serialPort13.IsOpen == false)
                OnOff13.Fill = Brushes.Red;
            if (_serialPort14.IsOpen == false)
                OnOff14.Fill = Brushes.Red;
            if (_serialPort15.IsOpen == false)
                OnOff15.Fill = Brushes.Red;
            if (_serialPort16.IsOpen == false)
                OnOff16.Fill = Brushes.Red;

            //clear the port list
            prts.Clear();
            //set the combobox back to selecting nothing
            SelectBrandCB.SelectedIndex = 0;
            ProductListCB.SelectedIndex = -1;
        }

        private void Serial2ComMapping(string startComNumber)
        {
            //MessageBox.Show(ComPortCB.SelectedItem.ToString());

            //Strip Out COM Leaving only the start number
            string ComStart = ComPortCB.SelectedValue.ToString().Replace("COM", "");
            int ComNo = 0;


            //MessageBox.Show(ComStart.GetType().ToString());
            //Loop through incrementing the start number by 1 for all serial ports in the collection
            if (ComPortCB.SelectedValue != null)
            {

                ComNo = Convert.ToInt32(ComStart);

            }



            //MessageBox.Show(ComNo.ToString());


            foreach (var sp in ComCollection)
            {
                sp.PortName = "COM" + ComNo;

                ComNo = ComNo + 1;
            }

        }

        private void IsolateSerial2ComMapping(string startComNumber)
        {
            //MessageBox.Show(ComPortCB.SelectedItem.ToString());

            //Strip Out COM Leaving only the start number
            string ComStart = IsolateComPortCB.SelectedValue.ToString().Replace("COM", "");
            int ComNo = 0;


            //MessageBox.Show(ComStart.GetType().ToString());
            //Loop through incrementing the start number by 1 for all serial ports in the collection
            if (ComPortCB.SelectedValue != null)
            {

                ComNo = Convert.ToInt32(ComStart);

            }


            

        //   MessageBox.Show(ComNo.ToString());


            //foreach (var sp in ComCollection)
            //{
            //    sp.PortName = "COM" + ComNo;

            //    ComNo = ComNo + 1;
            //}

        }





        private void ComPortCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            // call function and pass in value from comport selection
            Serial2ComMapping(ComPortCB.SelectedValue.ToString().Replace("COM", ""));

        }

        private void IsolateComPortCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            // call function and pass in value from comport selection
            IsolateSerial2ComMapping(IsolateComPortCB.SelectedValue.ToString().Replace("COM", ""));

        }



        private void ReportBtn_Click(object sender, RoutedEventArgs e)
        {
            //if (vm.ReportCollection != null)
            //    vm.ReportCollection.Clear();

            //how do i pull in the reports from all richtextboxes then cycle through the results through the left and right button clicks
            vm.ReportCollection = new List<string>();

            foreach (RichTextBox rtb in Helper.FindControlsOfType<RichTextBox>(this))
            {
                // MessageBox.Show(rtb.Name);
               //if the richtextbox is not empty then add to the collection
                // Add the found richtextbox to the collection
                if(vm.ConvertRichTextBoxContentsToString(rtb) != string.Empty)
                vm.ReportCollection.Add(vm.GenerateReportCluster(rtb));
            }


            vm.LoadReport = new TestReportFlowDocument(vm);

            vm.LoadWindow = new ReportViewWindow(vm);

            vm.LoadWindow.ShowDialog();
        }


      

        private void AutoCommands_Click(object sender, RoutedEventArgs e)
        {
            //reset cancel
            vm.CancelAutoScript = false;
            //Stop outer scrollviewer from scrolling and set bank to 1-4
            ClusterSV.CanContentScroll = true;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("1-4"));

            //If both comboboxes have a selection then allow
            if (SelectBrandCB.Text != "Select Brand" && ProductListCB.SelectedItem != null)
            {
                if (ProductListCB.Text.Contains(SelectBrandCB.Text))
                {

                    //Call the automated commands
                    if (_serialPort1.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort1, CommandTXT1, ConsoleRTB1, vm.SerialCompleteLine1, MsgScriptInProgress1, MsgSuccess1, MsgWarning1, MsgError1, vm.PasswordUnlock1);
                    if (_serialPort2.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort2, CommandTXT1, ConsoleRTB2, vm.SerialCompleteLine2, MsgScriptInProgress2, MsgSuccess2, MsgWarning2, MsgError2, vm.PasswordUnlock2);
                    if (_serialPort3.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort3, CommandTXT1, ConsoleRTB3, vm.SerialCompleteLine3, MsgScriptInProgress3, MsgSuccess3, MsgWarning3, MsgError3, vm.PasswordUnlock3);
                    if (_serialPort4.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort4, CommandTXT1, ConsoleRTB4, vm.SerialCompleteLine4, MsgScriptInProgress4, MsgSuccess4, MsgWarning4, MsgError4, vm.PasswordUnlock4);
                    if (_serialPort5.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort5, CommandTXT1, ConsoleRTB5, vm.SerialCompleteLine5, MsgScriptInProgress5, MsgSuccess5, MsgWarning5, MsgError5, vm.PasswordUnlock5);
                    if (_serialPort6.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort6, CommandTXT1, ConsoleRTB6, vm.SerialCompleteLine6, MsgScriptInProgress6, MsgSuccess6, MsgWarning6, MsgError6, vm.PasswordUnlock6);
                    if (_serialPort7.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort7, CommandTXT1, ConsoleRTB7, vm.SerialCompleteLine7, MsgScriptInProgress7, MsgSuccess7, MsgWarning7, MsgError7, vm.PasswordUnlock7);
                    if (_serialPort8.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort8, CommandTXT1, ConsoleRTB8, vm.SerialCompleteLine8, MsgScriptInProgress8, MsgSuccess8, MsgWarning8, MsgError8, vm.PasswordUnlock8);
                    if (_serialPort9.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort9, CommandTXT1, ConsoleRTB9, vm.SerialCompleteLine9, MsgScriptInProgress9, MsgSuccess9, MsgWarning9, MsgError9, vm.PasswordUnlock9);
                    if (_serialPort10.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort10, CommandTXT1, ConsoleRTB10, vm.SerialCompleteLine10, MsgScriptInProgress10, MsgSuccess10, MsgWarning10, MsgError10, vm.PasswordUnlock10);
                    if (_serialPort11.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort11, CommandTXT1, ConsoleRTB11, vm.SerialCompleteLine11, MsgScriptInProgress11, MsgSuccess11, MsgWarning11, MsgError11, vm.PasswordUnlock11);
                    if (_serialPort12.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort12, CommandTXT1, ConsoleRTB12, vm.SerialCompleteLine12, MsgScriptInProgress12, MsgSuccess12, MsgWarning12, MsgError12, vm.PasswordUnlock12);
                    if (_serialPort13.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort13, CommandTXT1, ConsoleRTB13, vm.SerialCompleteLine13, MsgScriptInProgress13, MsgSuccess13, MsgWarning13, MsgError13, vm.PasswordUnlock13);
                    if (_serialPort14.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort14, CommandTXT1, ConsoleRTB14, vm.SerialCompleteLine14, MsgScriptInProgress14, MsgSuccess14, MsgWarning14, MsgError14, vm.PasswordUnlock14);
                    if (_serialPort15.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort15, CommandTXT1, ConsoleRTB15, vm.SerialCompleteLine15, MsgScriptInProgress15, MsgSuccess15, MsgWarning15, MsgError15, vm.PasswordUnlock15);
                    if (_serialPort16.IsOpen)
                        vm.AutomatedWipeCommands(_serialPort16, CommandTXT1, ConsoleRTB16, vm.SerialCompleteLine16, MsgScriptInProgress16, MsgSuccess16, MsgWarning16, MsgError16, vm.PasswordUnlock16);
                    //Clear the terminals of previous commands
                    ClearTerminal();

                    //Call the script end dialog
                    //await PutTaskDelay(3000);
                    //vm.DialogMessage("App Message", "Auto Script Complete");

                }
                else
                {
                    vm.DialogMessage("Message", "Selected automation file does not match the selected brand.");
                }


            }
            else if (SelectBrandCB.Text == "Select Brand" && ProductListCB.SelectedItem != null || SelectBrandCB.Text != "Select Brand" && ProductListCB.SelectedItem != null)
            {
                vm.DialogMessage("Message", "Please select the brand and corresponding automation script.");
                //MessageBox.Show("Please select a product.");
            }


          
        }

        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

        //IMPLEMENT  A FOR EACH VERSION
        private void DelFlash1_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB1, _serialPort1, ConsoleRTB1, MsgSuccess1, MsgWarning1, MsgError1);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB2, _serialPort2, ConsoleRTB2, MsgSuccess2, MsgWarning2, MsgError2);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB3, _serialPort3, ConsoleRTB3, MsgSuccess3, MsgWarning3, MsgError3);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB4, _serialPort4, ConsoleRTB4, MsgSuccess4, MsgWarning4, MsgError4);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB5, _serialPort5, ConsoleRTB5, MsgSuccess5, MsgWarning5, MsgError5);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB6, _serialPort6, ConsoleRTB6, MsgSuccess6, MsgWarning6, MsgError6);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB7, _serialPort7, ConsoleRTB7, MsgSuccess7, MsgWarning7, MsgError7);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB8, _serialPort8, ConsoleRTB8, MsgSuccess8, MsgWarning8, MsgError8);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB9, _serialPort9, ConsoleRTB9, MsgSuccess9, MsgWarning9, MsgError9);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB10, _serialPort10, ConsoleRTB10, MsgSuccess10, MsgWarning10, MsgError10);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB11, _serialPort11, ConsoleRTB11, MsgSuccess11, MsgWarning11, MsgError11);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB12, _serialPort12, ConsoleRTB12, MsgSuccess12, MsgWarning12, MsgError12);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB13, _serialPort13, ConsoleRTB13, MsgSuccess13, MsgWarning13, MsgError13);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB14, _serialPort14, ConsoleRTB14, MsgSuccess14, MsgWarning14, MsgError14);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB15, _serialPort15, ConsoleRTB15, MsgSuccess15, MsgWarning15, MsgError15);
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB16, _serialPort16, ConsoleRTB16, MsgSuccess16, MsgWarning16, MsgError16);
        }

                          
        private void DelFlash2_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB2, _serialPort2, ConsoleRTB2, MsgSuccess2, MsgWarning2, MsgError2);
        }

        private void DelFlash3_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB3, _serialPort3, ConsoleRTB3, MsgSuccess3, MsgWarning3, MsgError3);
        }

        private void DelFlash4_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB4, _serialPort4, ConsoleRTB4, MsgSuccess4, MsgWarning4, MsgError4);
        }

        private void DelFlash5_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB5, _serialPort5, ConsoleRTB5, MsgSuccess5, MsgWarning5, MsgError5);
        }

        private void DelFlash6_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB6, _serialPort6, ConsoleRTB6, MsgSuccess6, MsgWarning6, MsgError6);
        }

        private void DelFlash7_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB7, _serialPort7, ConsoleRTB7, MsgSuccess7, MsgWarning7, MsgError7);
        }

        private void DelFlash8_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB8, _serialPort8, ConsoleRTB8, MsgSuccess8, MsgWarning8, MsgError8);
        }

        private void DelFlash9_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB9, _serialPort9, ConsoleRTB9, MsgSuccess9, MsgWarning9, MsgError9);
        }

        private void DelFlash10_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB10, _serialPort10, ConsoleRTB10, MsgSuccess10, MsgWarning10, MsgError10);
        }

        private void DelFlash11_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB11, _serialPort11, ConsoleRTB11, MsgSuccess11, MsgWarning11, MsgError11);
        }

        private void DelFlash12_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB12, _serialPort12, ConsoleRTB12, MsgSuccess12, MsgWarning12, MsgError12);
        }

        private void DelFlash13_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB13, _serialPort13, ConsoleRTB13, MsgSuccess13, MsgWarning13, MsgError13);
        }

        private void DelFlash14_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB14, _serialPort14, ConsoleRTB14, MsgSuccess14, MsgWarning14, MsgError14);
        }

        private void DelFlash15_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB15, _serialPort15, ConsoleRTB15, MsgSuccess15, MsgWarning15, MsgError15);
        }

        private void DelFlash16_Click(object sender, RoutedEventArgs e)
        {
            //Loop through detected flash files and delete
            vm.DeleteFlash(LB16, _serialPort16, ConsoleRTB16, MsgSuccess16, MsgWarning16, MsgError16);
        }


        private void ConsoleRTB1_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB1")
            {
               
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB1.CaretPosition.GetLineStartPosition(0), ConsoleRTB1.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB1.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") && myText.Contains("<") == false  || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB1.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                        ConsoleRTB1.UpdateLayout();
                        ConsoleRTB1.Focus();
                    }
                  

                  
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent1, LB1, ConsoleRTB1, _serialPort1);
            }
        }


     
        private void ConsoleRTB1_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB1.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB1.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                  
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                //MessageBox.Show("NowHere");
                TextRange txtRange1 = new TextRange(ConsoleRTB1.Selection.Start, ConsoleRTB1.Selection.End);
              
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB1.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB1.IsReadOnly = false;

                            }
                        }
                    }

                }
                        // CommandTXT1.Text += e.Key;
            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }

                        MainViewModel.CurrentCommand = CommandTXT1.Text;
                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }

               }
               

                if (CommandTXT1.Text != "")
                {
                    //add write to other consoles here

                    //CREATE METHOD AND ADD A NEW RUN
                    //var newRun = new Run(string.Empty, ConsoleRTB1.CaretPosition.GetInsertionPosition(LogicalDirection.Forward)) { Foreground = Brushes.MediumPurple};
                    //ConsoleRTB1.CaretPosition.Paragraph.Inlines.Add(newRun);
                    //ConsoleRTB1.CaretPosition = newRun.ContentEnd;
                    //MyRichTextBox.Focus();// Capture what was typed in direct to the console RTB || myText.Contains(">")
                    //ConsoleRTB1.CaretPosition.InsertTextInRun(CommandTXT1.Text + "\r");




                    //if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                    //{
                    //    TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                    //    txtRange.Text = CommandTXT1.Text;
                    //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    //}
                    if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                      

                }


                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Text ="";
                            CommandTXT1.Clear();
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                      //MessageBox.Show("HI");
                        SendData("Normal");
                      
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }

            //if (!(e.Key == Key.Left | e.Key == Key.Right | e.Key == Key.Up | e.Key == Key.Down | e.Key == Key.LeftShift))
            //{
            //    var trange = new TextRange(ConsoleRTB1.Selection.Start, ConsoleRTB1.Selection.End);
            //    bool isProtected = false;
            //    switch (trange.Text.Length)
            //    {
            //        case 0:
            //            {
            //                // Zero length selection
            //                if (e.Key == Key.Back)
            //                    trange = new TextRange(trange.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), trange.Start);
            //                else if (e.Key == Key.Delete)
            //                    trange = new TextRange(trange.End.GetInsertionPosition(LogicalDirection.Forward), trange.End.GetNextInsertionPosition(LogicalDirection.Forward));
            //                isProtected = trange.GetPropertyValue(FontStretchProperty) == FontStretches.UltraCondensed;
            //                break;
            //            }

            //        default:
            //            {
            //                //RichTextBox selection has length
            //                isProtected = trange.GetPropertyValue(FontStretchProperty) == DependencyProperty.UnsetValue || trange.GetPropertyValue(FontStretchProperty) == FontStretches.UltraCondensed;
            //                break;
            //            }
            //    }

            //    if (isProtected)
            //    {
            //        e.Handled = true; return;
            //    }
            //}



            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB1.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB1 != null && ConsoleRTB1.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB1.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB1.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB1.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB1.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }



            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
            }
            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                //MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }


        private void ConsoleRTB1_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Back)
            {
                MessageBox.Show("Here");
                //TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);

                //if (txtRange.Text.Length >= 1)
                //{
                //    if (txtRange.Text.Contains(">") || txtRange.Text.Contains("#"))
                //    {
                //        txtRange.Text.Replace("-", "");
                //    }
                //}

            }

        }



        //private void ConsoleRTB2_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming2 != null)
        //    {
        //        // Check for flash and questions on initial boot
        //        vm.FlashCheck(ConsoleContent2, LB2, ConsoleRTB2, _serialPort2);
        //    }
        //}

        private void ConsoleRTB2_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB2")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB2.CaretPosition.GetLineStartPosition(0), ConsoleRTB2.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB2.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB2.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent2, LB2, ConsoleRTB2, _serialPort2);
            }
        }



        private void ConsoleRTB2_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB2.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB2.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB2_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

          
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB2.Selection.Start, ConsoleRTB2.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB2.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB2.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }

                       

                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }
                  


                }

                if (CommandTXT1.Text != "")
                {
                    //add write to other consoles here
                    if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    //if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                    //{
                    //    TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                    //    txtRange.Text = CommandTXT1.Text;
                    //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    //}
                    if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                }

                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB2.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB2 != null && ConsoleRTB2.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB2.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB2.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB2.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB2.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }


        private void ConsoleRTB2_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }

        //private void ConsoleRTB3_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming3 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent3, LB3, ConsoleRTB3, _serialPort3);
        //    }
        //}


        private void ConsoleRTB3_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB3")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB3.CaretPosition.GetLineStartPosition(0), ConsoleRTB3.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB3.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB3.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent3, LB3, ConsoleRTB3, _serialPort3);
            }
        }



        private void ConsoleRTB3_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB3.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB3.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB3_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

         
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB3.Selection.Start, ConsoleRTB3.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB3.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB3.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }

                        


                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }

                   

                }

                if (CommandTXT1.Text != "")
                {
                    //add write to other consoles here
                    if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    //if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                    //{
                    //    TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                    //    txtRange.Text = CommandTXT1.Text;
                    //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    //}
                    if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                }

                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }



                CommandTXT1.Clear();
                CommandTXT1.Text = "";
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB3.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB3 != null && ConsoleRTB3.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB3.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB3.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB3.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB3.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }


        private void ConsoleRTB3_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }



        //private void ConsoleRTB4_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming4 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent4, LB4, ConsoleRTB4, _serialPort4);
        //    }
        //}


        private void ConsoleRTB4_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB4")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB4.CaretPosition.GetLineStartPosition(0), ConsoleRTB4.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB4.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB4.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent4, LB4, ConsoleRTB4, _serialPort4);
            }
        }



        private void ConsoleRTB4_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB4.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB4.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB4_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

          
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB4.Selection.Start, ConsoleRTB4.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB4.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB4.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }


                       

                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }

                    

                }


                if (CommandTXT1.Text != "")
                {
                    //add write to other consoles here
                    if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    //if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                    //{
                    //    TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                    //    txtRange.Text = CommandTXT1.Text;
                    //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    //}
                    if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                }



                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }

                CommandTXT1.Clear();
                CommandTXT1.Text = "";
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB4.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB4 != null && ConsoleRTB4.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB4.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB4.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB4.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB4.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }


        private void ConsoleRTB4_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }




        //private void ConsoleRTB5_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming5 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent5, LB5, ConsoleRTB5, _serialPort5);
        //    }
        //}

        private void ConsoleRTB5_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB5")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB5.CaretPosition.GetLineStartPosition(0), ConsoleRTB5.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB5.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB5.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent5, LB5, ConsoleRTB5, _serialPort5);
            }
        }



        private void ConsoleRTB5_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB5.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB5.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB5_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

         
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    TextRange txtRange1 = new TextRange(ConsoleRTB5.Selection.Start, ConsoleRTB5.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB5.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB5.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }

                        

                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }
                   

                }


                if (CommandTXT1.Text != "")
                {
                    //add write to other consoles here
                    if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    //if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                    //{
                    //    TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                    //    txtRange.Text = CommandTXT1.Text;
                    //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    //}
                    if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                    if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                    {
                        TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                        txtRange.Text = CommandTXT1.Text;
                        txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                    }
                }

                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB5.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB5 != null && ConsoleRTB5.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB5.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB5.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB5.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB5.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }


        private void ConsoleRTB5_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }



        //private void ConsoleRTB6_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming6 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent6, LB6, ConsoleRTB6, _serialPort6);
        //    }
        //}


        private void ConsoleRTB6_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB6")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB6.CaretPosition.GetLineStartPosition(0), ConsoleRTB6.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB6.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB6.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent6, LB6, ConsoleRTB6, _serialPort6);
            }
        }



        private void ConsoleRTB6_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB6.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB6.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB6_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

         
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB6.Selection.Start, ConsoleRTB6.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB6.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB6.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }

                        


                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }
                   
                }

                //add write to other consoles here
                if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                //if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                //{
                //    TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                //    txtRange.Text = CommandTXT1.Text;
                //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                //}
                if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }


                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB6.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB6 != null && ConsoleRTB6.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB6.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB6.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB6.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB6.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }


        private void ConsoleRTB6_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }

        //private void ConsoleRTB7_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming7 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent7, LB7, ConsoleRTB7, _serialPort7);
        //    }
        //}


        private void ConsoleRTB7_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB7")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB7.CaretPosition.GetLineStartPosition(0), ConsoleRTB7.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB7.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB7.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent7, LB7, ConsoleRTB7, _serialPort7);
            }
        }



        private void ConsoleRTB7_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB7.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB7.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB7_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

        
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB7.Selection.Start, ConsoleRTB7.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB7.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB7.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }


                        


                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }

                   
                }

                //add write to other consoles here
                if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                //if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                //{
                //    TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                //    txtRange.Text = CommandTXT1.Text;
                //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                //}
                if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }


                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB7.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB7 != null && ConsoleRTB7.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB7.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB7.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB7.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB7.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }


        private void ConsoleRTB7_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }

        //private void ConsoleRTB8_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming8 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent8, LB8, ConsoleRTB8, _serialPort8);
        //    }
        //}


        private void ConsoleRTB8_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB8")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB8.CaretPosition.GetLineStartPosition(0), ConsoleRTB8.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB8.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB8.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent8, LB8, ConsoleRTB8, _serialPort8);
            }
        }



        private void ConsoleRTB8_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB8.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB8.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB8_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

           
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB8.Selection.Start, ConsoleRTB8.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB8.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB8.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }

                        

                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }

                    

                }



                //add write to other consoles here
                if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                //if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                //{
                //    TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                //    txtRange.Text = CommandTXT1.Text;
                //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                //}
                if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }


                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB8.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB8 != null && ConsoleRTB8.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB8.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB8.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB8.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB8.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }


        private void ConsoleRTB8_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }



        //private void ConsoleRTB9_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming9 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent9, LB9, ConsoleRTB9, _serialPort9);
        //    }
        //}


        private void ConsoleRTB9_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB9")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB9.CaretPosition.GetLineStartPosition(0), ConsoleRTB9.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB9.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB9.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent9, LB9, ConsoleRTB9, _serialPort9);
            }
        }



        private void ConsoleRTB9_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB9.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB9.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB9_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

         
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB9.Selection.Start, ConsoleRTB9.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB9.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB9.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }
                       

                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }
                    

                }

                //add write to other consoles here
                if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                //if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                //{
                //    TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                //    txtRange.Text = CommandTXT1.Text;
                //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                //}
                if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }


                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB9.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB9 != null && ConsoleRTB9.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB9.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB9.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB9.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB9.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }


        private void ConsoleRTB9_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }

        //private void ConsoleRTB10_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming10 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent10, LB10, ConsoleRTB10, _serialPort10);
        //    }
        //}

        private void ConsoleRTB10_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB10")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB10.CaretPosition.GetLineStartPosition(0), ConsoleRTB10.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB10.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB10.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                       
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent10, LB10, ConsoleRTB10, _serialPort10);
            }
        }



        private void ConsoleRTB10_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB10.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB10.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB10_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

          
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB10.Selection.Start, ConsoleRTB10.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB10.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB10.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }



                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }



                }


                //add write to other consoles here
                if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                //if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                //{
                //    TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                //    txtRange.Text = CommandTXT1.Text;
                //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                //}
                if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }


                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB10.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB10 != null && ConsoleRTB10.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB10.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB10.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB10.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB10.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }


        private void ConsoleRTB10_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }

        //private void ConsoleRTB11_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming11 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent11, LB11, ConsoleRTB11, _serialPort11);
        //    }
        //}


        private void ConsoleRTB11_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB11")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB11.CaretPosition.GetLineStartPosition(0), ConsoleRTB11.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB11.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB11.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent11, LB11, ConsoleRTB11, _serialPort11);
            }
        }



        private void ConsoleRTB11_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB11.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB11.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB11_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

          
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB11.Selection.Start, ConsoleRTB11.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB11.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB11.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }

                       

                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }

                    

                }

                //add write to other consoles here
                if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                //if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                //{
                //    TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                //    txtRange.Text = CommandTXT1.Text;
                //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                //}
                if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }


                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB11.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB11 != null && ConsoleRTB11.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB11.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB11.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB11.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB11.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }


        private void ConsoleRTB11_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }

        //private void ConsoleRTB12_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming12 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent12, LB12, ConsoleRTB12, _serialPort12);
        //    }
        //}

        private void ConsoleRTB12_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (ConsoleIncoming12 != null)
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB12.CaretPosition.GetLineStartPosition(0), ConsoleRTB12.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB12.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB12.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent12, LB12, ConsoleRTB12, _serialPort12);
            }
        }



        private void ConsoleRTB12_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB12.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB12.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB12_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

          
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB12.Selection.Start, ConsoleRTB12.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB12.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB12.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }

                        

                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }

                  
                }

                //add write to other consoles here
                if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                //if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                //{
                //    TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                //    txtRange.Text = CommandTXT1.Text;
                //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                //}
                if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }



                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB12.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB12 != null && ConsoleRTB12.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB12.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB12.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB12.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB12.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }


        private void ConsoleRTB12_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }

        //private void ConsoleRTB13_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming13 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent13, LB13, ConsoleRTB13, _serialPort13);
        //    }
        //}

        private void ConsoleRTB13_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB13")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB13.CaretPosition.GetLineStartPosition(0), ConsoleRTB13.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB13.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB13.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent13, LB13, ConsoleRTB13, _serialPort13);
            }
        }



        private void ConsoleRTB13_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB13.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB13.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB13_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

           
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB13.Selection.Start, ConsoleRTB13.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB13.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB13.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }

                        


                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }

                   
                }

                //add write to other consoles here
                if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                //if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                //{
                //    TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                //    txtRange.Text = CommandTXT1.Text;
                //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                //}
                if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }



                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB13.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB13 != null && ConsoleRTB13.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB13.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB13.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB13.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB13.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }


        private void ConsoleRTB13_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }

        //private void ConsoleRTB14_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming14 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent14, LB14, ConsoleRTB14, _serialPort14);
        //    }
        //}

        private void ConsoleRTB14_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB14")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB14.CaretPosition.GetLineStartPosition(0), ConsoleRTB14.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB14.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB14.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent14, LB14, ConsoleRTB14, _serialPort14);
            }
        }



        private void ConsoleRTB14_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB14.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB14.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB14_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

         
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB14.Selection.Start, ConsoleRTB14.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB14.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB14.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }

                       

                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }

                   

                }


                if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                //if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                //{
                //    TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                //    txtRange.Text = CommandTXT1.Text;
                //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                //}
                if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }


                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB14.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB14 != null && ConsoleRTB14.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB14.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB14.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB14.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB14.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }


        private void ConsoleRTB14_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }

        //private void ConsoleRTB15_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming15 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent15, LB15, ConsoleRTB15, _serialPort15);
        //    }
        //}

        private void ConsoleRTB15_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB15")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB15.CaretPosition.GetLineStartPosition(0), ConsoleRTB15.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB15.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB15.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent15, LB15, ConsoleRTB15, _serialPort15);
            }
        }



        private void ConsoleRTB15_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB15.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB15.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB15_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

           
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB15.Selection.Start, ConsoleRTB15.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB15.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB15.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }

                       

                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }


                }


                if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                //if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                //{
                //    TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                //    txtRange.Text = CommandTXT1.Text;
                //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                //}
                if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }


                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB15.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB15 != null && ConsoleRTB15.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB15.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB15.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB15.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB15.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }


        private void ConsoleRTB15_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }

        //private void ConsoleRTB16_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    //Check only when recieving new serial content
        //    if (ConsoleIncoming16 != null)
        //    {
        //        //Check for flash and questions on initial boot

        //        vm.FlashCheck(ConsoleContent16, LB16, ConsoleRTB16, _serialPort16);
        //    }
        //}

        private void ConsoleRTB16_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check only when recieving new serial content
            if (((RichTextBox)sender).Name == "ConsoleRTB16")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">")
                var myText = new TextRange(ConsoleRTB16.CaretPosition.GetLineStartPosition(0), ConsoleRTB16.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB16.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');


                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB16.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }



                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent1, LB1, ConsoleRTB16, _serialPort1);
            }
        }



        private void ConsoleRTB16_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Get the Start Claret Position
            TextPointer start = ConsoleRTB16.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);

            //Set the typed text to Purple
            ConsoleRTB16.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


        }


        private void ConsoleRTB16_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {

         
            // CommandTXT1.Text += e.Key;
            //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");
                    TextRange txtRange1 = new TextRange(ConsoleRTB16.Selection.Start, ConsoleRTB16.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                        TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                        if (txtRange2.Text.Length >= 1)
                        {
                            if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                            {
                                // MessageBox.Show(txtRange2.Text.Last().ToString());
                                //Stop delete
                                ConsoleRTB16.IsReadOnly = true;
                                e.Handled = true;
                                ConsoleRTB16.IsReadOnly = false;

                            }
                        }
                    }

                }

            if (e.Key == Key.Enter)
            {

                //add command to history list
                if (vm.CMDHistory.Contains(CommandTXT1.Text))
                {
                    //do nothing
                }
                else
                {
                    //add to list
                    if (CommandTXT1.Text != "")
                    {
                        if (CommandTXT1.Text == "going a cold start")
                        {
                            CommandTXT1.Text = "";
                        }

                        

                        vm.CMDHistory.Add(CommandTXT1.Text);
                    }

                   

                }


                if (_serialPort1.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB1) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB1.Document.ContentEnd, ConsoleRTB1.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort2.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB2) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB2.Document.ContentEnd, ConsoleRTB2.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort3.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB3) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB3.Document.ContentEnd, ConsoleRTB3.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort4.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB4) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB4.Document.ContentEnd, ConsoleRTB4.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort5.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB5) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB5.Document.ContentEnd, ConsoleRTB5.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort6.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB6) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB6.Document.ContentEnd, ConsoleRTB6.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort7.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB7) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB7.Document.ContentEnd, ConsoleRTB7.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort8.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB8) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB8.Document.ContentEnd, ConsoleRTB8.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort9.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB9) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB9.Document.ContentEnd, ConsoleRTB9.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort10.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB10) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB10.Document.ContentEnd, ConsoleRTB10.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort11.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB11) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB11.Document.ContentEnd, ConsoleRTB11.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort12.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB12) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB12.Document.ContentEnd, ConsoleRTB12.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort13.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB13) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB13.Document.ContentEnd, ConsoleRTB13.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort14.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB14) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB14.Document.ContentEnd, ConsoleRTB14.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                if (_serialPort15.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB15) != string.Empty)
                {
                    TextRange txtRange = new TextRange(ConsoleRTB15.Document.ContentEnd, ConsoleRTB15.Document.ContentEnd);
                    txtRange.Text = CommandTXT1.Text;
                    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                }
                //if (_serialPort16.IsOpen && vm.ConvertRichTextBoxContentsToString(ConsoleRTB16) != string.Empty)
                //{
                //    TextRange txtRange = new TextRange(ConsoleRTB16.Document.ContentEnd, ConsoleRTB16.Document.ContentEnd);
                //    txtRange.Text = CommandTXT1.Text;
                //    txtRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);
                //}

                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //To enable isolation on the select serial port
                    if (IsolateComPortCB.SelectedIndex > -1)
                    {
                        if (IsolateComPortCB.SelectedItem.ToString() != string.Empty)
                        {
                            // Send Command to isolated serial port
                            //MessageBox.Show(IsolateComPortCB.SelectedItem.ToString());
                            switch (IsolateComPortCB.SelectedItem.ToString())
                            {

                                case "Serial1":
                                    SendDataSingle("Normal", _serialPort1, CommandTXT1);
                                    break;
                                case "Serial2":
                                    SendDataSingle("Normal", _serialPort2, CommandTXT1);
                                    break;
                                case "Serial3":
                                    SendDataSingle("Normal", _serialPort3, CommandTXT1);
                                    break;
                                case "Serial4":
                                    SendDataSingle("Normal", _serialPort4, CommandTXT1);
                                    break;
                                case "Serial5":
                                    SendDataSingle("Normal", _serialPort5, CommandTXT1);
                                    break;
                                case "Serial6":
                                    SendDataSingle("Normal", _serialPort6, CommandTXT1);
                                    break;
                                case "Serial7":
                                    SendDataSingle("Normal", _serialPort7, CommandTXT1);
                                    break;
                                case "Serial8":
                                    SendDataSingle("Normal", _serialPort8, CommandTXT1);
                                    break;
                                case "Serial9":
                                    SendDataSingle("Normal", _serialPort9, CommandTXT1);
                                    break;
                                case "Serial10":
                                    SendDataSingle("Normal", _serialPort10, CommandTXT1);
                                    break;
                                case "Serial11":
                                    SendDataSingle("Normal", _serialPort11, CommandTXT1);
                                    break;
                                case "Serial12":
                                    SendDataSingle("Normal", _serialPort12, CommandTXT1);
                                    break;
                                case "Serial13":
                                    SendDataSingle("Normal", _serialPort13, CommandTXT1);
                                    break;
                                case "Serial14":
                                    SendDataSingle("Normal", _serialPort14, CommandTXT1);
                                    break;
                                case "Serial15":
                                    SendDataSingle("Normal", _serialPort15, CommandTXT1);
                                    break;
                                case "Serial16":
                                    SendDataSingle("Normal", _serialPort16, CommandTXT1);
                                    break;
                            }

                            //SendDataSingle("Isolate");
                            CommandTXT1.Clear();
                            CommandTXT1.Text = "";
                        }

                    }
                    else
                    {


                        // Send Command to serial port
                        //MessageBox.Show("HI");
                        SendData("Normal");
                        CommandTXT1.Clear();
                        CommandTXT1.Text = "";
                    }




                }
                else
                {
                    CommandTXT1.Clear();
                    CommandTXT1.Text = "";
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
            {
                string historicCMD = "";


                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {


                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            // MessageBox.Show(historicCMD);
                            // ConsoleRTB16.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB16 != null && ConsoleRTB16.Document != null)
                            {
                                // Inserts the text at current selection. 

                                TextPointer start = ConsoleRTB16.CaretPosition;

                                // start.GetLineStartPosition
                                // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                                //colour selected text
                                ConsoleRTB16.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                                // start.InsertTextInRun(historicCMD);
                                ConsoleRTB16.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB16.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }


            }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }


        private void ConsoleRTB16_KeyDown(object sender, KeyEventArgs e)
        {

            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
                // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

        }

        private void ClearConsoleBtn_Click(object sender, RoutedEventArgs e)
        {
            ClearTerminal();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //close serial ports on exit
            CloseAllSerialPorts();
        }

        private void SelectBrandCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //MessageBox.Show(SelectBrandCB.SelectedValue);
            if (vm != null)
            {
                vm.CMDHistory.Clear();

                //Get the selected item and cast to a ComboBoxItem and get the string through the content property
                ComboBoxItem SelectedBrand = (ComboBoxItem)SelectBrandCB.SelectedItem;
                //Read in the XML Product files and add to the collection based on the selected brand
                vm.ReadProductFileCommand.Execute(SelectedBrand.Content);

                //check if anything other is selected, then return focus to the Command Textbox
                CommandTXT1.Focus();
                //vm.ProductBrand = SelectBrandCB.Text;
                //MessageBox.Show(vm.ProductBrand);

            }
        }

        private void SelAll1_Click(object sender, RoutedEventArgs e)
        {

          


            if (LB1.Items != null)
            {
                if (LB1.SelectedItems.Count == LB1.Items.Count)
                {
                    LB1.SelectedItems.Clear();
                    //LB1.SelectedItems.Clear();
                    //LB1.SelectedItems.Clear();
                    //LB1.SelectedItems.Clear();
                    //LB1.SelectedItems.Clear();
                    //LB1.SelectedItems.Clear();
                    //LB1.SelectedItems.Clear();
                    //LB1.SelectedItems.Clear();
                    //LB1.SelectedItems.Clear();
                    //LB1.SelectedItems.Clear();
                    //LB1.SelectedItems.Clear();
                    //LB1.SelectedItems.Clear();
                    //LB1.SelectedItems.Clear();
                    //LB1.SelectedItems.Clear();
                }
                else
                {
                    LB1.SelectAll();
                    LB2.SelectAll();
                    LB3.SelectAll();
                    LB4.SelectAll();
                    LB5.SelectAll();
                    LB6.SelectAll();
                    LB7.SelectAll();
                    LB8.SelectAll();
                    LB9.SelectAll();
                    LB10.SelectAll();
                    LB11.SelectAll();
                    LB12.SelectAll();
                    LB13.SelectAll();
                    LB14.SelectAll();
                    LB15.SelectAll();
                    LB16.SelectAll();
                }
            }
            LB1.SelectAll();
            LB2.SelectAll();
            LB3.SelectAll();
            LB4.SelectAll();
            LB5.SelectAll();
            LB6.SelectAll();
            LB7.SelectAll();
            LB8.SelectAll();
            LB9.SelectAll();
            LB10.SelectAll();
            LB11.SelectAll();
            LB12.SelectAll();
            LB13.SelectAll();
            LB14.SelectAll();
            LB15.SelectAll();
            LB16.SelectAll();

        }
        private void SelAll2_Click(object sender, RoutedEventArgs e)
        {
            if (LB2.Items != null)
            {
                if (LB2.SelectedItems.Count == LB2.Items.Count)
                {
                    LB2.SelectedItems.Clear();
                }
                else
                {
                    LB2.SelectAll();
                }
            }
        }
        private void SelAll3_Click(object sender, RoutedEventArgs e)
        {
            if (LB3.Items != null)
            {
                if (LB3.SelectedItems.Count == LB3.Items.Count)
                {
                    LB3.SelectedItems.Clear();
                }
                else
                {
                    LB3.SelectAll();
                }
            }
        }
        private void SelAll4_Click(object sender, RoutedEventArgs e)
        {
            if (LB4.Items != null)
            {
                if (LB4.SelectedItems.Count == LB4.Items.Count)
                {
                    LB4.SelectedItems.Clear();
                }
                else
                {
                    LB4.SelectAll();
                }
            }
        }
        private void SelAll5_Click(object sender, RoutedEventArgs e)
        {
            if (LB5.Items != null)
            {
                if (LB5.SelectedItems.Count == LB5.Items.Count)
                {
                    LB5.SelectedItems.Clear();
                }
                else
                {
                    LB5.SelectAll();
                }
            }
        }
        private void SelAll6_Click(object sender, RoutedEventArgs e)
        {
            if (LB6.Items != null)
            {
                if (LB6.SelectedItems.Count == LB6.Items.Count)
                {
                    LB6.SelectedItems.Clear();
                }
                else
                {
                    LB6.SelectAll();
                }
            }
        }
        private void SelAll7_Click(object sender, RoutedEventArgs e)
        {
            if (LB7.Items != null)
            {
                if (LB7.SelectedItems.Count == LB7.Items.Count)
                {
                    LB7.SelectedItems.Clear();
                }
                else
                {
                    LB7.SelectAll();
                }
            }
        }
        private void SelAll8_Click(object sender, RoutedEventArgs e)
        {
            if (LB8.Items != null)
            {
                if (LB8.SelectedItems.Count == LB8.Items.Count)
                {
                    LB8.SelectedItems.Clear();
                }
                else
                {
                    LB8.SelectAll();
                }
            }
        }
        private void SelAll9_Click(object sender, RoutedEventArgs e)
        {
            if (LB9.Items != null)
            {
                if (LB9.SelectedItems.Count == LB9.Items.Count)
                {
                    LB9.SelectedItems.Clear();
                }
                else
                {
                    LB9.SelectAll();
                }
            }
        }
        private void SelAll10_Click(object sender, RoutedEventArgs e)
        {
            if (LB10.Items != null)
            {
                if (LB10.SelectedItems.Count == LB10.Items.Count)
                {
                    LB10.SelectedItems.Clear();
                }
                else
                {
                    LB10.SelectAll();
                }
            }
        }
        private void SelAll11_Click(object sender, RoutedEventArgs e)
        {
            if (LB11.Items != null)
            {
                if (LB11.SelectedItems.Count == LB11.Items.Count)
                {
                    LB11.SelectedItems.Clear();
                }
                else
                {
                    LB11.SelectAll();
                }
            }
        }
        private void SelAll12_Click(object sender, RoutedEventArgs e)
        {
            if (LB12.Items != null)
            {
                if (LB12.SelectedItems.Count == LB12.Items.Count)
                {
                    LB12.SelectedItems.Clear();
                }
                else
                {
                    LB12.SelectAll();
                }
            }
        }
        private void SelAll13_Click(object sender, RoutedEventArgs e)
        {
            if (LB13.Items != null)
            {
                if (LB13.SelectedItems.Count == LB13.Items.Count)
                {
                    LB13.SelectedItems.Clear();
                }
                else
                {
                    LB13.SelectAll();
                }
            }
        }
        private void SelAll14_Click(object sender, RoutedEventArgs e)
        {
            if (LB14.Items != null)
            {
                if (LB14.SelectedItems.Count == LB14.Items.Count)
                {
                    LB14.SelectedItems.Clear();
                }
                else
                {
                    LB14.SelectAll();
                }
            }
        }
        private void SelAll15_Click(object sender, RoutedEventArgs e)
        {
            if (LB15.Items != null)
            {
                if (LB15.SelectedItems.Count == LB15.Items.Count)
                {
                    LB15.SelectedItems.Clear();
                }
                else
                {
                    LB15.SelectAll();
                }
            }
        }
        private void SelAll16_Click(object sender, RoutedEventArgs e)
        {
            if (LB16.Items != null)
            {
                if (LB16.SelectedItems.Count == LB16.Items.Count)
                {
                    LB16.SelectedItems.Clear();
                }
                else
                {
                    LB16.SelectAll();
                }
            }
        }

        private void EnterBTN_Click(object sender, RoutedEventArgs e)
        {
            if (_serialPort1.IsOpen)
            _serialPort1.Write("\r\r");
            if (_serialPort2.IsOpen)
                _serialPort2.Write("\r\r");
            if (_serialPort3.IsOpen)
                _serialPort3.Write("\r\r");
            if (_serialPort4.IsOpen)
                _serialPort4.Write("\r\r");
            if (_serialPort5.IsOpen)
                _serialPort5.Write("\r\r");
            if (_serialPort6.IsOpen)
                _serialPort6.Write("\r\r");
            if (_serialPort7.IsOpen)
                _serialPort7.Write("\r\r");
            if (_serialPort8.IsOpen)
                _serialPort8.Write("\r\r");
            if (_serialPort9.IsOpen)
                _serialPort9.Write("\r\r");
            if (_serialPort10.IsOpen)
                _serialPort10.Write("\r\r");
            if (_serialPort11.IsOpen)
                _serialPort11.Write("\r\r");
            if (_serialPort12.IsOpen)
                _serialPort12.Write("\r\r");
            if (_serialPort13.IsOpen)
                _serialPort13.Write("\r\r");
            if (_serialPort14.IsOpen)
                _serialPort14.Write("\r\r");
            if (_serialPort15.IsOpen)
                _serialPort15.Write("\r\r");
            if (_serialPort16.IsOpen)
                _serialPort16.Write("\r\r");
        }

        private void ClearSelectionBTN_Click(object sender, RoutedEventArgs e)
        {
            //set isolation selection
            IsolateComPortCB.SelectedIndex = -1;
        }

        private void ConsoleFontSizeClusterCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem != null)
            {
                //SelectedUser = (sender as ComboBox).SelectedItem.ToString();
                // double cb1 = ConsoleFontSizeCB.SelectedItem.ToString();
                //MessageBox.Show(cb1.ToString()));



                if (ConsoleRTB1 != null)
                {
                    ////method 1
                    ComboBoxItem typeItem = (ComboBoxItem)ConsoleFontSizeClusterCB.SelectedItem;
                    string value = typeItem.Content.ToString();
                    ConsoleRTB1.FontSize = Convert.ToInt32(value);
                   // PromptTB1.FontSize = Convert.ToInt32(value);
                    ConsoleRTB2.FontSize = Convert.ToInt32(value);
                   // PromptTB2.FontSize = Convert.ToInt32(value);
                    ConsoleRTB3.FontSize = Convert.ToInt32(value);
                   // PromptTB3.FontSize = Convert.ToInt32(value);
                    ConsoleRTB4.FontSize = Convert.ToInt32(value);
                   // PromptTB4.FontSize = Convert.ToInt32(value);
                    ConsoleRTB5.FontSize = Convert.ToInt32(value);
                   // PromptTB5.FontSize = Convert.ToInt32(value);
                    ConsoleRTB6.FontSize = Convert.ToInt32(value);
                   // PromptTB6.FontSize = Convert.ToInt32(value);
                    ConsoleRTB7.FontSize = Convert.ToInt32(value);
                   // PromptTB7.FontSize = Convert.ToInt32(value);
                    ConsoleRTB8.FontSize = Convert.ToInt32(value);
                   // PromptTB8.FontSize = Convert.ToInt32(value);
                    ConsoleRTB9.FontSize = Convert.ToInt32(value);
                    //PromptTB9.FontSize = Convert.ToInt32(value);
                    ConsoleRTB10.FontSize = Convert.ToInt32(value);
                    //PromptTB10.FontSize = Convert.ToInt32(value);
                    ConsoleRTB11.FontSize = Convert.ToInt32(value);
                   // PromptTB11.FontSize = Convert.ToInt32(value);
                    ConsoleRTB12.FontSize = Convert.ToInt32(value);
                   // PromptTB12.FontSize = Convert.ToInt32(value);
                    ConsoleRTB13.FontSize = Convert.ToInt32(value);
                    //PromptTB13.FontSize = Convert.ToInt32(value);
                    ConsoleRTB14.FontSize = Convert.ToInt32(value);
                    //PromptTB14.FontSize = Convert.ToInt32(value);
                    ConsoleRTB15.FontSize = Convert.ToInt32(value);
                    //PromptTB15.FontSize = Convert.ToInt32(value);
                    ConsoleRTB16.FontSize = Convert.ToInt32(value);
                    //PromptTB16.FontSize = Convert.ToInt32(value);
                    //method2
                    //ConsoleRTB1.FontSize = Convert.ToInt32(ConsoleFontSizeCB.Text);
                    //double rtbfontsz = (ConsoleFontSizeCB.SelectedItem.ToString());
                    vm.ConsoleFontSize = ConsoleRTB1.FontSize;
                }


                //if (SP1.IsOpen)
                //    SP1.PortName = ComPortCB.SelectedItem.ToString();
            }
        }


        private void ConsoleColourCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem != null)
            {

                if (ConsoleRTB1 != null)
                {
                    ////method 1
                    //ComboBoxItem typeItem = (ComboBoxItem)ConsoleColourCB.SelectedIndex;

                    switch (ConsoleColourCB.SelectedIndex)
                    {
                        case 0:


                            vm.ConsoleColour = Brushes.White;
                            //MessageBox.Show("yup white");
                            break;
                        case 1:
                            //ConsoleRTB1.Foreground = Brushes.LimeGreen;
                            vm.ConsoleColour = Brushes.LimeGreen;
                            break;
                        case 2:
                            //ConsoleRTB1.Foreground = Brushes.PowderBlue;
                            vm.ConsoleColour = Brushes.PowderBlue;
                            break;
                        case 3:
                            //ConsoleRTB1.Foreground = Brushes.Orange;
                            vm.ConsoleColour = Brushes.Orange;
                            break;
                        case 4:
                            //ConsoleRTB1.Foreground = Brushes.Thistle;
                            vm.ConsoleColour = Brushes.Thistle;
                            break;
                    }


                    ConsoleRTB1.Foreground = vm.ConsoleColour;
                    ConsoleRTB2.Foreground = vm.ConsoleColour;
                    ConsoleRTB3.Foreground = vm.ConsoleColour;
                    ConsoleRTB4.Foreground = vm.ConsoleColour;
                    ConsoleRTB5.Foreground = vm.ConsoleColour;
                    ConsoleRTB6.Foreground = vm.ConsoleColour;
                    ConsoleRTB7.Foreground = vm.ConsoleColour;
                    ConsoleRTB8.Foreground = vm.ConsoleColour;
                    ConsoleRTB9.Foreground = vm.ConsoleColour;
                    ConsoleRTB10.Foreground = vm.ConsoleColour;
                    ConsoleRTB11.Foreground = vm.ConsoleColour;
                    ConsoleRTB12.Foreground = vm.ConsoleColour;
                    ConsoleRTB13.Foreground = vm.ConsoleColour;
                    ConsoleRTB14.Foreground = vm.ConsoleColour;
                    ConsoleRTB15.Foreground = vm.ConsoleColour;
                    ConsoleRTB16.Foreground = vm.ConsoleColour;



                    //ConsoleRTB1.Foreground = 
                    //*PromptTB1.FontSize = Convert.ToInt32(value);
                    //method2
                    //ConsoleRTB1.FontSize = Convert.ToInt32(ConsoleFontSizeCB.Text);


                    //double rtbfontsz = (ConsoleFontSizeCB.SelectedItem.ToString());

                }


                //if (SP1.IsOpen)
                //    SP1.PortName = ComPortCB.SelectedItem.ToString();
            }

        }


        public async void GroupBreakState()
        {
            if (_serialPort1.IsOpen)
                _serialPort1.BreakState = true;
            if (_serialPort2.IsOpen)
                _serialPort2.BreakState = true;
            if (_serialPort3.IsOpen)
                _serialPort3.BreakState = true;
            if (_serialPort4.IsOpen)
                _serialPort4.BreakState = true;
            if (_serialPort5.IsOpen)
                _serialPort5.BreakState = true;
            if (_serialPort6.IsOpen)
                _serialPort6.BreakState = true;
            if (_serialPort7.IsOpen)
                _serialPort7.BreakState = true;
            if (_serialPort8.IsOpen)
                _serialPort8.BreakState = true;
            if (_serialPort9.IsOpen)
                _serialPort9.BreakState = true;
            if (_serialPort10.IsOpen)
                _serialPort10.BreakState = true;
            if (_serialPort11.IsOpen)
                _serialPort11.BreakState = true;
            if (_serialPort12.IsOpen)
                _serialPort12.BreakState = true;
            if (_serialPort13.IsOpen)
                _serialPort13.BreakState = true;
            if (_serialPort14.IsOpen)
                _serialPort14.BreakState = true;
            if (_serialPort15.IsOpen)
                _serialPort15.BreakState = true;
            if (_serialPort16.IsOpen)
                _serialPort16.BreakState = true;

            BreakModeRect1.Fill = Brushes.Green;
            BreakModeRect2.Fill = Brushes.Green;
            BreakModeRect3.Fill = Brushes.Green;
            BreakModeRect4.Fill = Brushes.Green;
            BreakModeRect5.Fill = Brushes.Green;
            BreakModeRect6.Fill = Brushes.Green;
            BreakModeRect7.Fill = Brushes.Green;
            BreakModeRect8.Fill = Brushes.Green;
            BreakModeRect9.Fill = Brushes.Green;
            BreakModeRect10.Fill = Brushes.Green;
            BreakModeRect11.Fill = Brushes.Green;
            BreakModeRect12.Fill = Brushes.Green;
            BreakModeRect13.Fill = Brushes.Green;
            BreakModeRect14.Fill = Brushes.Green;
            BreakModeRect15.Fill = Brushes.Green;
            BreakModeRect16.Fill = Brushes.Green;
            await PutTaskDelay(400);
            if (_serialPort1.IsOpen)
                _serialPort1.BreakState = false;
            if (_serialPort2.IsOpen)
                _serialPort2.BreakState = false;
            if (_serialPort3.IsOpen)
                _serialPort3.BreakState = false;
            if (_serialPort4.IsOpen)
                _serialPort4.BreakState = false;
            if (_serialPort5.IsOpen)
                _serialPort5.BreakState = false;
            if (_serialPort6.IsOpen)
                _serialPort6.BreakState = false;
            if (_serialPort7.IsOpen)
                _serialPort7.BreakState = false;
            if (_serialPort8.IsOpen)
                _serialPort8.BreakState = false;
            if (_serialPort9.IsOpen)
                _serialPort9.BreakState = false;
            if (_serialPort10.IsOpen)
                _serialPort10.BreakState = false;
            if (_serialPort11.IsOpen)
                _serialPort11.BreakState = false;
            if (_serialPort12.IsOpen)
                _serialPort12.BreakState = false;
            if (_serialPort13.IsOpen)
                _serialPort13.BreakState = false;
            if (_serialPort14.IsOpen)
                _serialPort14.BreakState = false;
            if (_serialPort15.IsOpen)
                _serialPort15.BreakState = false;
            if (_serialPort16.IsOpen)
                _serialPort16.BreakState = false;


        }
    

        private async void BreakONBTN1_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            GroupBreakState();
            await PutTaskDelay(200);
            GroupBreakState();
            await PutTaskDelay(200);
            GroupBreakState();
            await PutTaskDelay(200);
            GroupBreakState();
            //if (_serialPort1.IsOpen)
            //    _serialPort1.BreakState = true;

            if (_serialPort1.IsOpen)
                _serialPort1.BreakState = false;
            if (_serialPort2.IsOpen)
                _serialPort2.BreakState = false;
            if (_serialPort3.IsOpen)
                _serialPort3.BreakState = false;
            if (_serialPort4.IsOpen)
                _serialPort4.BreakState = false;
            if (_serialPort5.IsOpen)
                _serialPort5.BreakState = false;
            if (_serialPort6.IsOpen)
                _serialPort6.BreakState = false;
            if (_serialPort7.IsOpen)
                _serialPort7.BreakState = false;
            if (_serialPort8.IsOpen)
                _serialPort8.BreakState = false;
            if (_serialPort9.IsOpen)
                _serialPort9.BreakState = false;
            if (_serialPort10.IsOpen)
                _serialPort10.BreakState = false;
            if (_serialPort11.IsOpen)
                _serialPort11.BreakState = false;
            if (_serialPort12.IsOpen)
                _serialPort12.BreakState = false;
            if (_serialPort13.IsOpen)
                _serialPort13.BreakState = false;
            if (_serialPort14.IsOpen)
                _serialPort14.BreakState = false;
            if (_serialPort15.IsOpen)
                _serialPort15.BreakState = false;
            if (_serialPort16.IsOpen)
                _serialPort16.BreakState = false;

            BreakModeRect1.Fill = Brushes.Red;
            BreakModeRect2.Fill = Brushes.Red;
            BreakModeRect3.Fill = Brushes.Red;
            BreakModeRect4.Fill = Brushes.Red;
            BreakModeRect5.Fill = Brushes.Red;
            BreakModeRect6.Fill = Brushes.Red;
            BreakModeRect7.Fill = Brushes.Red;
            BreakModeRect8.Fill = Brushes.Red;
            BreakModeRect9.Fill = Brushes.Red;
            BreakModeRect10.Fill = Brushes.Red;
            BreakModeRect11.Fill = Brushes.Red;
            BreakModeRect12.Fill = Brushes.Red;
            BreakModeRect13.Fill = Brushes.Red;
            BreakModeRect14.Fill = Brushes.Red;
            BreakModeRect15.Fill = Brushes.Red;
            BreakModeRect16.Fill = Brushes.Red;

            //if (_serialPort1.IsOpen)
            //    _serialPort1.BreakState = false;



        }

        private void BreakOFFBTN1_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort1.IsOpen)
                _serialPort1.BreakState = false;
        }

        private async void BreakONBTN2_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort2.IsOpen)
            {
                _serialPort2.BreakState = true;

                BreakModeRect2.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort2.BreakState = false;
                BreakModeRect2.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN2_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort2.IsOpen)
                _serialPort2.BreakState = false;
        }

        private async void BreakONBTN3_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort3.IsOpen)
            {
                _serialPort3.BreakState = true;
                BreakModeRect3.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort3.BreakState = false;
                BreakModeRect3.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN3_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort3.IsOpen)
                _serialPort3.BreakState = false;
        }

        private async void BreakONBTN4_Click(object sender, RoutedEventArgs e)
        {
            if (_serialPort4.IsOpen)
            {
                _serialPort4.BreakState = true;
                BreakModeRect4.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort4.BreakState = false;
                BreakModeRect4.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN4_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort4.IsOpen)
                _serialPort4.BreakState = false;
        }

        private async void BreakONBTN5_Click(object sender, RoutedEventArgs e)
        {
            if (_serialPort5.IsOpen)
            {
                _serialPort5.BreakState = true;
                BreakModeRect5.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort5.BreakState = false;
                BreakModeRect5.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN5_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort5.IsOpen)
                _serialPort5.BreakState = false;
        }

        private async void BreakONBTN6_Click(object sender, RoutedEventArgs e)
        {
            if (_serialPort6.IsOpen)
            {
                _serialPort6.BreakState = true;
                BreakModeRect6.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort6.BreakState = false;
                BreakModeRect6.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN6_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort6.IsOpen)
                _serialPort6.BreakState = false;
        }

        private async void BreakONBTN7_Click(object sender, RoutedEventArgs e)
        {
            if (_serialPort7.IsOpen)
            {
                _serialPort7.BreakState = true;
                BreakModeRect7.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort7.BreakState = false;
                BreakModeRect7.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN7_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort7.IsOpen)
                _serialPort7.BreakState = false;
        }

        private async void BreakONBTN8_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort8.IsOpen)
            {
                _serialPort8.BreakState = true;
                BreakModeRect8.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort8.BreakState = false;
                BreakModeRect8.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN8_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort8.IsOpen)
                _serialPort8.BreakState = false;
        }

        private async void BreakONBTN9_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort9.IsOpen)
            {
                _serialPort9.BreakState = true;
                BreakModeRect9.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort9.BreakState = false;
                BreakModeRect9.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN9_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort9.IsOpen)
                _serialPort9.BreakState = false;
        }

        private async void BreakONBTN10_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort10.IsOpen)
            {
                _serialPort10.BreakState = true;
                BreakModeRect10.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort10.BreakState = false;
                BreakModeRect10.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN10_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort10.IsOpen)
                _serialPort10.BreakState = false;
        }

        private async void BreakONBTN11_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort11.IsOpen)
            {
                _serialPort11.BreakState = true;
                BreakModeRect11.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort11.BreakState = false;
                BreakModeRect11.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN11_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort11.IsOpen)
                _serialPort11.BreakState = false;
        }

        private async void BreakONBTN12_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort12.IsOpen)
            {
                _serialPort12.BreakState = true;
                BreakModeRect12.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort12.BreakState = false;
                BreakModeRect12.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN12_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort12.IsOpen)
                _serialPort12.BreakState = false;
        }

        private async void BreakONBTN13_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort13.IsOpen)
            {
                _serialPort13.BreakState = true;
                BreakModeRect13.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort13.BreakState = false;
                BreakModeRect13.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN13_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort13.IsOpen)
                _serialPort13.BreakState = false;
        }

        private async void BreakONBTN14_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort14.IsOpen)
            {
                _serialPort14.BreakState = true;
                BreakModeRect14.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort14.BreakState = false;
                BreakModeRect14.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN14_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort14.IsOpen)
                _serialPort14.BreakState = false;
        }

     
        private async void BreakONBTN15_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort15.IsOpen)
            {
                _serialPort15.BreakState = true;
                BreakModeRect15.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort15.BreakState = false;
                BreakModeRect15.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN15_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort15.IsOpen)
                _serialPort15.BreakState = false;
        }

        private async void BreakONBTN16_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort16.IsOpen)
            {
                _serialPort16.BreakState = true;
                BreakModeRect16.Fill = Brushes.Green;
                await PutTaskDelay(300);
                _serialPort16.BreakState = false;
                BreakModeRect16.Fill = Brushes.Red;
            }
        }

        private void BreakOFFBTN16_Click(object sender, RoutedEventArgs e)
        {
            //Set the serial into a break state
            if (_serialPort16.IsOpen)
                _serialPort16.BreakState = false;
        }

        private void ESCBTN1_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort1.IsOpen)
                _serialPort1.Write("\x1b");

        }

        private void ESCBTN2_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort2.IsOpen)
                _serialPort2.Write("\x1b");

        }
        private void ESCBTN3_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort3.IsOpen)
                _serialPort3.Write("\x1b");

        }

        private void ESCBTN4_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort4.IsOpen)
                _serialPort4.Write("\x1b");

        }

        private void ESCBTN5_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort5.IsOpen)
                _serialPort5.Write("\x1b");

        }


        private void ESCBTN6_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort6.IsOpen)
                _serialPort6.Write("\x1b");

        }

        private void ESCBTN7_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort7.IsOpen)
                _serialPort7.Write("\x1b");

        }

        private void ESCBTN8_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort8.IsOpen)
                _serialPort8.Write("\x1b");

        }

        private void ESCBTN9_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort9.IsOpen)
                _serialPort9.Write("\x1b");

        }

        private void ESCBTN10_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort10.IsOpen)
                _serialPort10.Write("\x1b");

        }

        private void ESCBTN11_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort11.IsOpen)
                _serialPort11.Write("\x1b");

        }

        private void ESCBTN12_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort12.IsOpen)
                _serialPort12.Write("\x1b");

        }

        private void ESCBTN13_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort13.IsOpen)
                _serialPort13.Write("\x1b");

        }

        private void ESCBTN14_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort14.IsOpen)
                _serialPort14.Write("\x1b");

        }

        private void ESCBTN15_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort15.IsOpen)
                _serialPort15.Write("\x1b");

        }

        private void ESCBTN16_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort16.IsOpen)
                _serialPort16.Write("\x1b");

        }

        private void CancelAutoCommands_Click(object sender, RoutedEventArgs e)
        {
            MsgScriptInProgress1.Text = "";
            vm.AutoCLIVisibility = Visibility.Collapsed;
            vm.AutoCLIRunning = false;
            vm.AutoCLIRunning = false;
        }

      

        private void Search_Click(object sender, RoutedEventArgs e)
        {
          
        }

        private void ClusterSV_MouseEnter(object sender, MouseEventArgs e)
        {
            //Set to physical units scroll
           ClusterSV.CanContentScroll = false;
        }

        private void Bank1_Click(object sender, RoutedEventArgs e)
        {
            ClusterSV.CanContentScroll = false;
            //ClusterSV.Height = 600;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("1-4"));
            //change background of selected bank
            //Bank1.Background = Brushes.MediumPurple;
            //Bank2.Background = (Brush)FindResource("TechBuyerBrush1"); 
            //Bank3.Background = (Brush)FindResource("TechBuyerBrush1");
            //Bank4.Background = (Brush)FindResource("TechBuyerBrush1");
            //await PutTaskDelay(500);
            //ClusterSV.CanContentScroll = true;
        }

        private void Bank2_Click(object sender, RoutedEventArgs e)
        {
            ClusterSV.CanContentScroll = false;
            //ClusterSV.Height = 600;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("5-8"));
            //change background of selected bank
            //Bank1.Background = (Brush)FindResource("TechBuyerBrush1");
            //Bank2.Background = Brushes.MediumPurple;
            //Bank3.Background = (Brush)FindResource("TechBuyerBrush1");
            //Bank4.Background = (Brush)FindResource("TechBuyerBrush1");
            //await PutTaskDelay(500);
            //ClusterSV.CanContentScroll = true;
        }

        private void Bank3_Click(object sender, RoutedEventArgs e)
        {
            ClusterSV.CanContentScroll = false;
            //ClusterSV.Height = 600;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("9-12"));
            //change background of selected bank
            //Bank1.Background = (Brush)FindResource("TechBuyerBrush1");
            //Bank2.Background = (Brush)FindResource("TechBuyerBrush1");
            //Bank3.Background = Brushes.MediumPurple;
            //Bank4.Background = (Brush)FindResource("TechBuyerBrush1");
            //await PutTaskDelay(500);
            //ClusterSV.CanContentScroll = true;
        }

        private void Bank4_Click(object sender, RoutedEventArgs e)
        {
            ClusterSV.CanContentScroll = false;
            //ClusterSV.Height = 600;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("13-16"));
            //change background of selected bank
            //Bank1.Background = (Brush)FindResource("TechBuyerBrush1");
            //Bank2.Background = (Brush)FindResource("TechBuyerBrush1");
            //Bank3.Background = (Brush)FindResource("TechBuyerBrush1"); 
            //Bank4.Background = Brushes.MediumPurple;
            //await PutTaskDelay(500);
            //ClusterSV.CanContentScroll = true;
        }


        //function to apply adjustment to outer scrollviewer of screens
        private double ScreenPlacementOffset(string screenLayout)
        {
            double bank = 0;

            //check which bank the item has come from
            switch (screenLayout)
            {
                case "1-4":
                    bank = 0;
                    Bank1.Background = Brushes.MediumPurple;
                    Bank2.Background = (Brush)FindResource("TechBuyerBrush1");
                    Bank3.Background = (Brush)FindResource("TechBuyerBrush1");
                    Bank4.Background = (Brush)FindResource("TechBuyerBrush1");
                    break;
                case "5-8":
                    bank = 774;
                    //bank = ClusterSV.ActualHeight + 2 * 1;
                    Bank1.Background = (Brush)FindResource("TechBuyerBrush1");
                    Bank2.Background = Brushes.MediumPurple;
                    Bank3.Background = (Brush)FindResource("TechBuyerBrush1");
                    Bank4.Background = (Brush)FindResource("TechBuyerBrush1");
                    break;
                case "9-12":
                    bank = 1548;
                   // bank = ClusterSV.ActualHeight + 4 * 2;
                    Bank1.Background = (Brush)FindResource("TechBuyerBrush1");
                    Bank2.Background = (Brush)FindResource("TechBuyerBrush1");
                    Bank3.Background = Brushes.MediumPurple;
                    Bank4.Background = (Brush)FindResource("TechBuyerBrush1");
                    break;
                case "13-16":
                    //bank = ClusterSV.ActualHeight * 4;
                    bank = 2322;
                    Bank1.Background = (Brush)FindResource("TechBuyerBrush1");
                    Bank2.Background = (Brush)FindResource("TechBuyerBrush1");
                    Bank3.Background = (Brush)FindResource("TechBuyerBrush1");
                    Bank4.Background = Brushes.MediumPurple;
                    break;
            }
            //ClusterSV.CanContentScroll = true;
            return bank;
        }

      
        //If mouse scroll wheel used prevent the outer scroll viewer from moving
        private void ConsoleRTB1_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("1-4"));
            //ClusterSV.CanContentScroll = true;
        }

        private void ConsoleRTB2_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("1-4"));
            //ClusterSV.CanContentScroll = true;
        }

        private void ConsoleRTB3_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("1-4"));
            //ClusterSV.CanContentScroll = true;
        }

        private void ConsoleRTB4_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("1-4"));
           // ClusterSV.CanContentScroll = true;
        }

        private void ConsoleRTB5_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            //MessageBox.Show("Scrolled");
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("5-8"));
           // ClusterSV.CanContentScroll = true;

        }

        private void ConsoleRTB6_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("5-8"));
            //ClusterSV.CanContentScroll = true;
        }

        private void ConsoleRTB7_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("5-8"));
            //ClusterSV.CanContentScroll = true;
        }

        private void ConsoleRTB8_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("5-8"));
            //ClusterSV.CanContentScroll = true;
        }

        private void ConsoleRTB9_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("9-12"));
            //ClusterSV.CanContentScroll = true;
        }

        private void ConsoleRTB10_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("9-12"));
           // ClusterSV.CanContentScroll = true;
        }

        private void ConsoleRTB11_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("9-12"));
            //ClusterSV.CanContentScroll = true;
        }

        private void ConsoleRTB12_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("9-12"));
            //ClusterSV.CanContentScroll = true;
        }

        private void ConsoleRTB13_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("13-16"));
            //ClusterSV.CanContentScroll = true;
        }
        private void ConsoleRTB14_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("13-16"));
            //ClusterSV.CanContentScroll = true;
        }
        private void ConsoleRTB15_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("13-16"));
            //ClusterSV.CanContentScroll = true;
        }

        private void ConsoleRTB16_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Set to logical units scroll
            ClusterSV.CanContentScroll = false;
            ClusterSV.ScrollToVerticalOffset(ScreenPlacementOffset("13-16"));
            //ClusterSV.CanContentScroll = true;
        }
    }
}




