﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;
using MsOutlook = Microsoft.Office.Interop.Outlook;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for ReportDriveTest.xaml
    /// </summary>
    public partial class ReportDriveTest : Window
    {
        DriveTestViewModel vm;

        //Create Collection to hold discrepancy report
        ObservableCollection<NetsuiteDiscrepancyReport> dReportCollection = new ObservableCollection<NetsuiteDiscrepancyReport>();
        //Create Collection to hold failure report
        ObservableCollection<NetsuiteInventory> fReportCollection = new ObservableCollection<NetsuiteInventory>();

        EmailClass email;

        public ReportDriveTest()
        {
            InitializeComponent();

        }
        //FAILURE REPORT CONSTRUCTOR
        public ReportDriveTest(DriveTestViewModel passedViewModel, ObservableCollection<NetsuiteInventory> FailureCollection)
        {
            InitializeComponent();
            vm = passedViewModel;

            //Add in email class
            email = new EmailClass(vm);

            //Hide Controls
            Discrepancy.Visibility = Visibility.Collapsed;
            EmailDiscrepancyBTN.Visibility = Visibility.Collapsed;


            fReportCollection.Clear();

            //Set 1 for all quantites
            foreach (NetsuiteInventory nsi in FailureCollection)
            {
                nsi.POQuantity = "1";
            }

            fReportCollection = FailureCollection;
            //add passed report
            //dReportCollection.Add(dReport);

            //Clear ListView
            ReportLV2.Items.Clear();
            //read the reports
            ReportLV2.ItemsSource = fReportCollection;

            //DESELECT ITEMS
            ReportLV2.SelectedIndex = -1;


        }

        //DISCREPANCY REPORT CONSTRUCTOR
        public ReportDriveTest(DriveTestViewModel passedViewModel, ObservableCollection<NetsuiteDiscrepancyReport> ReportCollection)
        {
            InitializeComponent();

            vm = passedViewModel;

            //Add in email class
            email = new EmailClass(vm);

            //Hide Controls
            Failures.Visibility = Visibility.Collapsed;
            EmailFailureBTN.Visibility = Visibility.Collapsed;


            dReportCollection.Clear();

            dReportCollection = ReportCollection;
            //add passed report
            //dReportCollection.Add(dReport);
            
            //Clear ListView
            ReportLV1.Items.Clear();
            //read the reports
            ReportLV1.ItemsSource = dReportCollection;

            //DESELECT ITEMS
            ReportLV1.SelectedIndex = -1;


            // Send both documents to the print functions
            //PrintHelper.ShowPrintPreview(PrintHelper.GetFixedDocument2(TB1, null, new PrintDialog(), this));

        }


        public void GenerateXHTML()
        {
            //XHTML XMLWriter method

            //const string XHTMLNS = "http://www.w3.org/1999/xhtml";
            //MemoryStream ms = new MemoryStream();
            //StringWriter sw = new StringWriter();
            //XmlWriterSettings settings = new XmlWriterSettings();
            //settings.CloseOutput = false;

            //using (XmlWriter writer = XmlWriter.Create(sw, settings))
            //{
            //    writer.WriteStartDocument();
            //    writer.WriteDocType("html", "-//W3C//DTD XHTML 1.0 Transitional//EN", "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd", null);
            //    writer.WriteStartElement("html", XHTMLNS);
            //    writer.WriteStartElement("head", XHTMLNS);
            //    writer.WriteElementString("title", "Test Page");
            //    writer.WriteEndElement(); // End head
            //    writer.WriteStartElement("body", XHTMLNS);
            //    //First Table
            //    writer.WriteStartElement("table", XHTMLNS);
            //    writer.WriteStartElement("tr", XHTMLNS);
            //    writer.WriteStartElement("th", XHTMLNS);
            //    writer.WriteString("Col1");
            //    writer.WriteEndElement(); // End td
            //    writer.WriteStartElement("th", XHTMLNS);
            //    writer.WriteString("Col2");
            //    writer.WriteEndElement(); // End td
            //    writer.WriteStartElement("th", XHTMLNS);
            //    writer.WriteString("Col3");
            //    writer.WriteEndElement(); // End td
            //    writer.WriteStartElement("th", XHTMLNS);
            //    writer.WriteString("Col4");
            //    writer.WriteEndElement(); // End td
            //    writer.WriteEndElement(); // End tr
            //    writer.WriteEndElement(); // End u
            //    //End Table
            //    writer.WriteString("table");
            //    writer.WriteStartElement("u", XHTMLNS);
            //    writer.WriteString("test");
            //    writer.WriteEndElement(); // End u
            //    writer.WriteString(" content.");
            //    writer.WriteEndElement(); // End body
            //    writer.WriteEndElement(); // End html
            //    writer.WriteEndDocument();
            //}

            // ms.Position = 0;
            // this.webBrowser1.DocumentStream = ms.ToString;







            //foreach (NetsuiteDiscrepancyReport element in dReportCollection)
            //{
            //    MessageBox.Show(element.Comments);
            //}
        }



        //Discrepancy email
        public bool SendMailWithOutlookOld(string subject, string htmlBody, string recipients, string[] filePaths, ObservableCollection<NetsuiteDiscrepancyReport> dReportCollection)
        {
            try
            {
                // create the outlook application.
                MsOutlook.Application outlookApp = new MsOutlook.Application();
                if (outlookApp == null)
                    return false;

                // create a new mail item.
                MsOutlook.MailItem mail = (MsOutlook.MailItem)outlookApp.CreateItem(MsOutlook.OlItemType.olMailItem);

                //Create HTML For Discrepancies only         
                string outlookBody = "<!DOCTYPE html>" +
                    "<html>" +
                    "<head></head>" +
                    "<style>" +
                    "body {font-size:12px;}" +
                    "table { font-family: Arial, Helvetica, sans-serif; width:800px; text-align: center;}" +
                    "table, caption, th, td{border: 1px solid black; text-align: center;}" +
                    "th { width:200px padding: 0px; text-align: center;}" +
                    "</style><body>";

              //Create HTML Page with full info
                string textBody = "<!DOCTYPE html>" +
                    "<html>" +
                    "<head></head>" +
                    "<style>" +
                    "body {font-size:12px;}"  +
                    "table { font-family: Arial, Helvetica, sans-serif; width:800px; text-align: center;}" +
                    "table, caption, th, td{border: 1px solid black; text-align: center;}" +
                    "th { width:200px padding: 0px; text-align: center;}" +
                    "</style>";

                //Add Table and Headers title=Discrepancy Report "+ vm.PONumber + " <h1 align='center'>Drive Test</h1>
                textBody += "<body>" +
                    "<div align='left'>" +
                    "<table style='margin-top:20px; '>" +
                    //"<tr style='border: 1px solid black; color:white; background-color:#778899; '><td style='height:40px; font-size:20px;'>Totals</td></tr></table></tr>"

                    //Add Blank Extras table  Fields Product Code/Discrepancy/
                    //"<tr style='border: 1px solid black; color:white; background-color:#778899; '>" +
                    //"</caption>" +
                    "<tr style='border: 1px solid black; color:white; background-color:#778899; '>" +
                    "<th style='font-size:24px; height:24px;'><b>Total Quantity on PO</b></td>" +
                    "<th style='font-size:24px; height:24px;'><b>Total Number Serials Recieved</b></td>" +
                    //"<th  style='height:20px;'><b>Discrepancy</b></td>" +
                    //"<th width = 300><b>Comments</b></td>" +
                    "</tr>" +
                    "<tr>" +
                    //"<td style='font-size:24px; color:#99cc00; background-color:white; height:24px;'>" + vm.POImportTotal + " </td>" +
                    "<td style='font-size:24px; color:#99cc00; background-color:white; height:24px;'>" + vm.TotalOnImport + " </td>" +
                    "<td style='font-size:24px; color:#99cc00; background-color:white; height:24px;'>" + vm.ScannedSerialCollection.Count().ToString() + "</td>" +
                    "</tr></table>" +
                    //MAIN TABLE
                    "<table style='margin-top:20px; '><tr style='border: 1px solid black; color:white; background-color:#778899; '><td style='height:40px; font-size:20px;'>Purchase Order</td></tr></table>" +
                    "<tr>" +
                    "<table style='margin-top:20px border: 1px solid black;'>" +
                    //"<caption style=' color:#0099ff; background-color:white; ' >Drive Test</caption>" +
                    "<tr style=' border:1px solid black; color:black; background-color:#c5ccd3;'>" +
                    "<th><b>Type</b></th>" +
                    "<th><b>New Part Code</b></th>" +
                    "<th><b>Product Code</b></th>" +
                    "<th><b>Product Description</b></th>" +
                    "<th><b>PO Price €/£</b></th>" +
                    "<th><b>PO Quantity</b></th>" +
                    "<th><b>Received</b></th>" +
                    "<th><b>Discrepancy</b></th>" +
                    "<th width = 300><b>Comments</b></th>" +
                    "</tr>";


                outlookBody += "<p style='font-size:14px;'>Hi,&nbsp;</p><p style='font-size:14px;'>Please find the full discrepancy report attached.&nbsp;</p>" +
                "<p style = 'font-size:14px;' >Discrepancies Detected: &nbsp;&nbsp;</p>" +
                //Containing Table
                "<table style='margin-top:20px border: 0px solid black;'>" +
                "<table style='margin-top:20px border: 1px solid black;'>" +
                //"<caption style=' color:#0099ff; background-color:white; ' >Drive Test</caption>" +
                "<tr style=' border:1px solid black; color:black; background-color:#c5ccd3;'>" +
                "<th><b>Type</b></th>" +
                "<th><b>New Part Code</b></th>" +
                "<th><b>Product Code</b></th>" +
                "<th><b>Product Description</b></th>" +
                "<th><b>PO Price €/£</b></th>" +
                "<th><b>PO Quantity</b></th>" +
                "<th><b>Received</b></th>" +
                "<th><b>Discrepancy</b></th>" +
                "<th width = 300><b>Comments</b></th>" +
                "</tr>";

                //Create HTML Element and loop through the collection

                foreach (NetsuiteDiscrepancyReport element in dReportCollection)
                {
                    // 
                    if (element.LineType != null && element.LineType.Contains("(E)"))
                    {
                      
                    }
                    else
                    {
                        if (Convert.ToInt32(element.Discrepancy) == 0)
                        {
                            textBody += "<tr>" +
                                "<td  style='height:20px;'>" + element.LineType + "</td>" +
                                "<td  style='height:20px;'>" + element.NewPartCode + "</td>" +
                                "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                                "<td  style='height:20px;'> " + element.ProductDescription + "</td>" +
                                "<td  style='height:20px;'> " + element.POLineUnitPrice + "</td>" +
                                "<td  style='height:20px;'> " + element.ExpectedQuantity + "</td>" +
                                "<td  style='height:20px;'> " + element.ReceivedQuantity + "</td>" +
                                "<td  style=' color:black; background-color:#99cc00;'> " + element.Discrepancy + "</td>" +
                                "<td  style='height:20px;'> " + element.Comments + "</td></tr>";
                        }
                        else if (Convert.ToInt32(element.Discrepancy) < 0)
                        {
                            textBody += "<tr>" +
                                "<td  style='height:20px;'>" + element.LineType + "</td>" +
                                "<td  style='height:20px;'>" + element.NewPartCode + "</td>" +
                                "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                                "<td  style='height:20px;'> " + element.ProductDescription + "</td>" +
                                "<td  style='height:20px;'> " + element.POLineUnitPrice + "</td>" +
                                "<td  style='height:20px;'> " + element.ExpectedQuantity + "</td>" +
                                "<td  style='height:20px;'> " + element.ReceivedQuantity + "</td>" +
                                "<td  style=' color:black; background-color:#ff6347;'> " + element.Discrepancy + "</td>" +
                                "<td  style='height:20px;'> " + element.Comments + "</td></tr>";

                            outlookBody += "<tr>" +
                              "<td  style='height:20px;'>" + element.LineType + "</td>" +
                              "<td  style='height:20px;'>" + element.NewPartCode + "</td>" +
                              "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                              "<td  style='height:20px;'> " + element.ProductDescription + "</td>" +
                              "<td  style='height:20px;'> " + element.POLineUnitPrice + "</td>" +
                              "<td  style='height:20px;'> " + element.ExpectedQuantity + "</td>" +
                              "<td  style='height:20px;'> " + element.ReceivedQuantity + "</td>" +
                              "<td  style=' color:black; background-color:#ff6347;'> " + element.Discrepancy + "</td>" +
                              "<td  style='height:20px;'> " + element.Comments + "</td></tr>";
                        }
                        else if (Convert.ToInt32(element.Discrepancy) > 0)
                        {
                            textBody += "<tr>" +
                                "<td  style='height:20px;'>" + element.LineType + "</td>" +
                                "<td  style='height:20px;'>" + element.NewPartCode + "</td>" +
                                "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                                "<td  style='height:20px;'> " + element.ProductDescription + "</td>" +
                                "<td  style='height:20px;'> " + element.POLineUnitPrice + "</td>" +
                                "<td  style='height:20px;'> " + element.ExpectedQuantity + "</td>" +
                                "<td  style='height:20px;'> " + element.ReceivedQuantity + "</td>" +
                                "<td  style=' color:black; background-color:#ffd700;'> " + element.Discrepancy + "</td>" +
                                "<td  style='height:20px;'> " + element.Comments + "</td></tr>";

                            outlookBody += "<tr>" +
                            "<td  style='height:20px;'>" + element.LineType + "</td>" +
                            "<td  style='height:20px;'>" + element.NewPartCode + "</td>" +
                            "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                            "<td  style='height:20px;'> " + element.ProductDescription + "</td>" +
                            "<td  style='height:20px;'> " + element.POLineUnitPrice + "</td>" +
                            "<td  style='height:20px;'> " + element.ExpectedQuantity + "</td>" +
                            "<td  style='height:20px;'> " + element.ReceivedQuantity + "</td>" +
                            "<td  style=' color:black; background-color:#ffd700;'> " + element.Discrepancy + "</td>" +
                            "<td  style='height:20px;'> " + element.Comments + "</td></tr>";

                        }
                        else
                        {
                            textBody += "<tr>" +
                               "<td  style='height:20px;'>" + element.LineType + "</td>" +
                               "<td  style='height:20px;'>" + element.NewPartCode + "</td>" +
                               "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                               "<td  style='height:20px;'> " + element.ProductDescription + "</td>" +
                               "<td  style='height:20px;'> " + element.POLineUnitPrice + "</td>" +
                               "<td  style='height:20px;'> " + element.ExpectedQuantity + "</td>" +
                               "<td  style='height:20px;'> " + element.ReceivedQuantity + "</td>" +
                               "<td  style=' color:black; background-color:#ffffff;'> " + element.Discrepancy + "</td>" +
                               "<td  style='height:20px;'> " + element.Comments + "</td></tr>";
                        }
                    }


            }

                //Close the discrepancy table in Email Body
                outlookBody += "</table>";



                textBody += "</table>" +
                            "<tr>" +
                            "<table style='margin-top:20px border: 1px solid black;'><tr style='border: 1px solid black; color:white; background-color:#778899; '><td style='height:40px; font-size:20px;'>Extras</td></tr></table></tr>";
                //"<tr style='border: 0px solid black; color:#778899; background-color:white; '><td style='height:40px; font-size:20px;'>Extras</td></tr>";
                //EXTRAS IN EMAIL BODY
                outlookBody += "<table style='margin-top:20px border: 1px solid black;'>" +
                    "<tr style='color:black; background-color:#c5ccd3;'>" +
                    "<th  style='height:20px;'><b>Product Code</b></td>" +
                    "<th  style='height:20px;'><b>Extra Not On PO</b></td>" +
                    "<th  style='height:20px;'><b>Discrepancy</b></td>" +
                    "<th width = 300><b>Comments</b></td>" +
                    "</tr>";


                //End of Discrepancy Table
                //Extras Table
                textBody += "<tr style='border: 1px solid black;'><table style='border: 1px solid black;'> " +
                    //"<caption style=' color:#4da6ff; background-color:white; '>Extras</caption>" +
                    "<tr style='color:black; background-color:#c5ccd3;'>" +
                    "<th  style='height:20px;'><b>Product Code</b></td>" +
                    "<th  style='height:20px;'><b>Extra Not On PO</b></td>" +
                    "<th  style='height:20px;'><b>Discrepancy</b></td>" +
                    "<th width = 300><b>Comments</b></td>" +
                    "</tr>";               
                
                //loop through import collection
                foreach (NetsuiteDiscrepancyReport element in dReportCollection)
                {
                    //Check if item is an extra
                    if(element.LineType != null)
                    {
                        if (element.LineType.Contains("(E)"))
                        {
                            outlookBody += "<tr>" +
                                        "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                                        "<td  style='height:20px;'>Extras Not On PO</td>" +
                                        "<td  style=' color:black; background-color:#ffd700; '> " + element.ReceivedQuantity + "</td>" +
                                        "<td  style='height:20px;'> " + element.Comments + "</td>" +
                                        "</tr>";

                            textBody += "<tr>" +
                                    "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                                    "<td  style='height:20px;'>Extras Not On PO</td>" +
                                    "<td  style=' color:black; background-color:#ffd700; '> " + element.ReceivedQuantity + "</td>" +
                                    "<td  style='height:20px;'> " + element.Comments + "</td>" +
                                    "</tr>";

                        }
                    }
                                 
                    
                }

                //Close the extras table in Email Body
                outlookBody += "</table>";


                textBody += "</table>" +
                            "</tr>" +
                            "<tr>" +
                            "<table style='margin-top:20px border: 1px solid black;'><tr style='border: 1px solid black; color:white; background-color:#778899; '><td style='height:40px; font-size:20px;'>Issues</td></tr></table></tr>" +
                            //ISSUE HEADING
                            "<table style = 'margin-top:20px border: 1px solid black;' >" +
                            "<tr style=' border:1px solid black; color:black; background-color:#c5ccd3;'>" +
                            "<th><b>Type</b></th>" +
                            "<th><b>New Part Code</b></th>" +
                            "<th><b>Product Code</b></th>" +
                            "<th><b>Product Description</b></th>" +
                            "<th><b>PO Price €/£</b></th>" +
                            "<th><b>PO Quantity</b></th>" +
                            "<th><b>Received</b></th>" +
                            "<th><b>Discrepancy</b></th>" +
                            "<th width = 300><b>Comments</b></th>" +
                            "</tr>";
                //ISSUES TABLE

                //PURCHASING SIDE



                foreach (NetsuiteDiscrepancyReport element in dReportCollection)
                {
                    if (element.LineType != null && element.LineType.Contains("(E)"))
                    {

                    }
                    else
                    {
                        if (Convert.ToInt32(element.Discrepancy) < 0)
                        {
                            textBody += "<tr>" +
                                "<td  style='height:20px;'>" + element.LineType + "</td>" +
                                "<td  style='height:20px;'>" + element.NewPartCode + "</td>" +
                                "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                                "<td  style='height:20px;'> " + element.ProductDescription + "</td>" +
                                "<td  style='height:20px;'> " + element.POLineUnitPrice + "</td>" +
                                "<td  style='height:20px;'> " + element.ExpectedQuantity + "</td>" +
                                "<td  style='height:20px;'> " + element.ReceivedQuantity + "</td>" +
                                "<td  style=' color:black; background-color:#ff6347;'> " + element.Discrepancy + "</td>" +
                                "<td  style='height:20px;'> " + element.Comments + "</td></tr>";


                        }

                    }


                }

              //  outlookBody += "</table>";


                textBody += "</table>" +
                            "</tr>" +
                            "<tr>" +
                            "<table style='margin-top:20px border: 1px solid black;'><tr style='border: 1px solid black; color:white; background-color:#778899; '><td style='height:40px; font-size:20px;'>Serials</td></tr></table></tr>";

                //ADD SERIAL TO OUTLOOK EMAIL
                outlookBody += "<table style='margin-top:20px border: 1px solid black;'>" +
               "<tr style='color:black; background-color:#c5ccd3;'>" +
               "<th  style='height:20px;'><b>Serial No</b></td>" +
               "<th  style='height:20px;'><b>Product Code</b></td>" +
               //"<th  style='height:20px;'><b>            </b></td>" +
               //"<th  style='height:20px; width = 300><b>            </b></td>" +
               "</tr>";


                //Add Blank Extras table  Fields Product Code/Discrepancy/
                textBody += "<tr style='border: 1px solid black;'>" +
                    "<table style='margin-top:20px border: 1px solid black;'>" +
                    //"<caption style=' color:#4da6ff; background-color:white;'>" +
                    //"Extras" +
                    //"</caption>" +
                    "<tr style='color:black; background-color:#c5ccd3;'>" +
                    "<th  style='height:20px;'><b>Serial No</b></td>" +
                    "<th  style='height:20px;'><b>Product</b></td>" +
                    //"<th  style='height:20px;'><b>Discrepancy</b></td>" +
                    //"<th width = 300><b>Comments</b></td>" +
                    "</tr>"; ;

                //Add some blank fields
                foreach (NetsuiteInventory itm in vm.ScannedSerialCollection)
                {

                    outlookBody += "<tr>" +
                                 "<td style='height:20px;'>" + itm.SerialNumber + " </td>" +
                                 "<td style='height:20px;'>" + itm.ProductCode + "</td>" +
                                 //"<td style='height:20px;'>" + "" + "</td>" +
                                 //"<td style='height:20px;'>" + "" + "</td>" +
                                 "</tr></tr>";

                    textBody += "<tr>" +
                                  "<td style='height:20px;'>" + itm.SerialNumber + " </td>" +
                                  "<td style='height:20px;'>" + itm.ProductCode + "</td>" +
                                  //"<td style='height:20px;'>" + "" + "</td>" +
                                  //"<td style='height:20px;'>" + "" + "</td>" +
                                  "</tr></tr>";
                }
                //            "<tr><table><tr style='border: 1px solid black; color:white; background-color:#778899; '><td style='height:40px; font-size:20px;'>Extras</td></tr></table></tr>";

                //Close the extras table in Email Body
                outlookBody += "</table>";

                //end of purchasing tables 
                textBody += "</table>" +
                            "</div>" +
                            "</body>" +
                            "</html>";


                outlookBody += "</table>" +
                        "</div>" +
                        "</body>" +
                        "</html>";





                //************WRITE TO HTML FILE************
                File.WriteAllText(vm.myDocs + @"\" + vm.PONumber + "_DiscrepancyList.html", textBody.ToString());

                //Create new string array
                filePaths = new string[3];

                filePaths[0] = vm.myDocs + @"\" + vm.PONumber + "_DiscrepancyList.html";


                //Add attachments.
                if (filePaths != null)
                {
                    foreach (string file in filePaths)
                    {
                        //attach the file
                        // MessageBox.Show(file);
                        if (File.Exists(file))
                        {
                            MsOutlook.Attachment oAttach = mail.Attachments.Add(file);
                        }



                    }
                }



                //Serials Table Single Column

                // end of document
                // textBody += "</div>";

                mail.Subject = subject;
                mail.To = recipients;
                //mail.HTMLBody = mail.HTMLBody + string.Format("{0} <br/> {1}", infoMessage, mail.HTMLBody);
                // mail.HTMLBody = mail.HTMLBody + string.Format("{0} <br/> {1}", textBody, mail.HTMLBody);

                //mail.HTMLBody = mail.HTMLBody + string.Format("{0} <br/> {1}", textBody, mail.HTMLBody);
                mail.HTMLBody = mail.HTMLBody + string.Format("{0} <br/> {1}", outlookBody, mail.HTMLBody);


                // For XML xhtml Method
                //mail.HTMLBody = mail.HTMLBody + string.Format("{0} <br/> {1}", sw, mail.HTMLBody);

                //if (sendType == MailSendType.SendDirect)
                //    mail.Send();
                //else if (sendType == MailSendType.ShowModal)
                mail.Display(true);
                //else if (sendType == MailSendType.ShowModeless)
                //    mail.Display(false);


              

                // MessageBox.Show(infoMessage.ToString());

                // set html body. 
                // add the body of the email
                // mail.HTMLBody = mail.HTMLBody;
                
                // mail.HTMLBody = 

                mail = null;
                outlookApp = null;
                return true;
            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                return false;
            }




        }

        public bool SendMailWithOutlook(string subject, string htmlBody, string recipients, string[] filePaths, ObservableCollection<NetsuiteDiscrepancyReport> dReportCollection)
        {
            try
            {
                // create the outlook application.
                MsOutlook.Application outlookApp = new MsOutlook.Application();
                if (outlookApp == null)
                    return false;

                // create a new mail item.
                MsOutlook.MailItem mail = (MsOutlook.MailItem)outlookApp.CreateItem(MsOutlook.OlItemType.olMailItem);

                //Create HTML For Discrepancies only         
                string outlookBody = "<!DOCTYPE html>" +
                    "<html>" +
                    "<head></head>" +
                    "<style>" +
                    "body {font-size:12px;}" +
                    "table { font-family: Arial, Helvetica, sans-serif; width:1300px; text-align: center;}" +
                    "table, caption, th, td{border: 0px solid white; text-align: center;}" +
                    "th { width:200px padding: 0px; text-align: center;}" +
                    "</style><body>";

                //Create HTML Page with full info
                string textBody = "<!DOCTYPE html>" +
                    "<html>" +
                    "<head></head>" +
                    "<style>" +
                    "body {font-size:12px;}" +
                    "table { font-family: Arial, Helvetica, sans-serif; width:1300px; text-align: center;}" +
                    "table, caption, th, td{border: 0px solid white; text-align: center;}" +
                    "th { width:200px padding: 0px; text-align: center;}" +
                    "</style>";

                //Add Table and Headers title=Discrepancy Report "+ vm.PONumber + " <h1 align='center'>Drive Test</h1>
                textBody += "<body>" +
                    "<div align='left'>" +
                    "<table style='margin-top:20px margin-bottom:20px; '>" +
                    //"<tr style='border: 1px solid black; color:white; background-color:#778899; '><td style='height:40px; font-size:20px;'>Totals</td></tr></table></tr>"

                    //Add Blank Extras table  Fields Product Code/Discrepancy/
                    //"<tr style='border: 1px solid black; color:white; background-color:#778899; '>" +
                    //"</caption>" +
                    "<tr style='border: 1px solid black; color:white; background-color:#778899; '>" +
                    "<th style='font-size:24px; height:24px;'><b>Total Quantity on PO</b></td>" +
                    "<th style='font-size:24px; height:24px;'><b>Total Number Serials Recieved</b></td>" +
                    //"<th  style='height:20px;'><b>Discrepancy</b></td>" +
                    //"<th width = 300><b>Comments</b></td>" +
                    "</tr>" +
                    "<tr>" +
                    //"<td style='font-size:24px; color:#99cc00; background-color:white; height:24px;'>" + vm.POImportTotal + " </td>" +
                    "<td style='font-size:24px; color:black; background-color:white; height:24px;'>" + vm.TotalOnImport + " </td>" +
                    "<td style='font-size:24px; color:black; background-color:white; height:24px;'>" + vm.ScannedSerialCollection.Count().ToString() + "</td>" +
                    "</tr></table>" +
                    //MAIN TABLE
                    "<table style='margin-top:20px; margin-bottom:20px;'><tr style='border: 0px solid white; color:white; background-color:#778899; '><td style='height:40px; font-size:20px;'>Purchase Order</td></tr></table>" +
                    "<tr>" +
                    "<table style='margin-top:20px; margin-bottom:20px; border: 0px solid white;'>" +
                    //"<caption style=' color:#0099ff; background-color:white; ' >Drive Test</caption>" +
                    "<tr style=' border:1px solid black; color:black; background-color:#c5ccd3;'>" +
                    "<th><b>Type</b></th>" +
                    "<th><b>New Part Code</b></th>" +
                    "<th><b>Product Code</b></th>" +
                    "<th><b>Product Description</b></th>" +
                    "<th><b>PO Price €/£</b></th>" +
                    "<th><b>PO Quantity</b></th>" +
                    "<th><b>Received</b></th>" +
                    "<th><b>Discrepancy</b></th>" +
                    "<th width = 300><b>Comments</b></th>" +
                    "</tr>";


                outlookBody += "<p style='font-size:14px;'>Hi,&nbsp;</p><p style='font-size:14px;'>Please find the full discrepancy report attached.&nbsp;</p>" +
                "<p style = 'font-size:14px;' >Discrepancies Detected: &nbsp;&nbsp;</p>" +
                //Containing Table
                "<table style='margin-top:10px; margin-bottom:10px;'><tr style='border: 0px solid white; color:white; background-color:#778899; '><td style='height:20px; font-size:20px;'>Discrepancies</td></tr></table>" +
                "<table style='margin-top:10px; margin-bottom:10px; border: 0px solid white;'>" +
                //"<caption style=' color:#0099ff; background-color:white; ' >Drive Test</caption>" +
                "<tr style=' border:0px solid white; color:black; background-color:#c5ccd3;'>" +
                "<th><b>Type</b></th>" +
                "<th><b>New Part Code</b></th>" +
                "<th><b>Product Code</b></th>" +
                "<th><b>Product Description</b></th>" +
                "<th><b>PO Price €/£</b></th>" +
                "<th><b>PO Quantity</b></th>" +
                "<th><b>Received</b></th>" +
                "<th><b>Discrepancy</b></th>" +
                "<th width = 300><b>Comments</b></th>" +
                "</tr>";

                //Create HTML Element and loop through the collection

                foreach (NetsuiteDiscrepancyReport element in dReportCollection)
                {
                    // 
                    if (element.LineType != null && element.LineType.Contains("(E)"))
                    {

                    }
                    else
                    {
                        if (Convert.ToInt32(element.Discrepancy) == 0)
                        {
                            textBody += "<tr>" +
                                "<td  style='height:20px;'>" + element.LineType + "</td>" +
                                "<td  style='height:20px;'>" + element.NewPartCode + "</td>" +
                                "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                                "<td  style='height:20px;'> " + element.ProductDescription + "</td>" +
                                "<td  style='height:20px;'> " + element.POLineUnitPrice + "</td>" +
                                "<td  style='height:20px;'> " + element.ExpectedQuantity + "</td>" +
                                "<td  style='height:20px;'> " + element.ReceivedQuantity + "</td>" +
                                "<td  style=' color:black; background-color:#99cc00;'> " + element.Discrepancy + "</td>" +
                                "<td  style='height:20px;'> " + element.Comments + "</td></tr>";
                        }
                        else if (Convert.ToInt32(element.Discrepancy) < 0)
                        {
                            textBody += "<tr>" +
                                "<td  style='height:20px;'>" + element.LineType + "</td>" +
                                "<td  style='height:20px;'>" + element.NewPartCode + "</td>" +
                                "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                                "<td  style='height:20px;'> " + element.ProductDescription + "</td>" +
                                "<td  style='height:20px;'> " + element.POLineUnitPrice + "</td>" +
                                "<td  style='height:20px;'> " + element.ExpectedQuantity + "</td>" +
                                "<td  style='height:20px;'> " + element.ReceivedQuantity + "</td>" +
                                "<td  style=' color:black; background-color:#ff6347;'> " + element.Discrepancy + "</td>" +
                                "<td  style='height:20px;'> " + element.Comments + "</td></tr>";

                            outlookBody += "<tr>" +
                              "<td  style='height:10px;'>" + element.LineType + "</td>" +
                              "<td  style='height:10px;'>" + element.NewPartCode + "</td>" +
                              "<td  style='height:10px;'>" + element.ProductCode + "</td>" +
                              "<td  style='height:10px;'> " + element.ProductDescription + "</td>" +
                              "<td  style='height:10px;'> " + element.POLineUnitPrice + "</td>" +
                              "<td  style='height:10px;'> " + element.ExpectedQuantity + "</td>" +
                              "<td  style='height:10px;'> " + element.ReceivedQuantity + "</td>" +
                              "<td  style=' color:black; background-color:#ff6347;'> " + element.Discrepancy + "</td>" +
                              "<td  style='height:10px;'> " + element.Comments + "</td></tr>";
                        }
                        else if (Convert.ToInt32(element.Discrepancy) > 0)
                        {
                            textBody += "<tr>" +
                                "<td  style='height:20px;'>" + element.LineType + "</td>" +
                                "<td  style='height:20px;'>" + element.NewPartCode + "</td>" +
                                "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                                "<td  style='height:20px;'> " + element.ProductDescription + "</td>" +
                                "<td  style='height:20px;'> " + element.POLineUnitPrice + "</td>" +
                                "<td  style='height:20px;'> " + element.ExpectedQuantity + "</td>" +
                                "<td  style='height:20px;'> " + element.ReceivedQuantity + "</td>" +
                                "<td  style=' color:black; background-color:#ffd700;'> " + element.Discrepancy + "</td>" +
                                "<td  style='height:20px;'> " + element.Comments + "</td></tr>";

                            outlookBody += "<tr>" +
                            "<td  style='height:10px;'>" + element.LineType + "</td>" +
                            "<td  style='height:10px;'>" + element.NewPartCode + "</td>" +
                            "<td  style='height:10px;'>" + element.ProductCode + "</td>" +
                            "<td  style='height:10px;'> " + element.ProductDescription + "</td>" +
                            "<td  style='height:10px;'> " + element.POLineUnitPrice + "</td>" +
                            "<td  style='height:10px;'> " + element.ExpectedQuantity + "</td>" +
                            "<td  style='height:10px;'> " + element.ReceivedQuantity + "</td>" +
                            "<td  style=' color:black; background-color:#ffd700;'> " + element.Discrepancy + "</td>" +
                            "<td  style='height:10px;'> " + element.Comments + "</td></tr>";

                        }
                        else
                        {
                            textBody += "<tr>" +
                               "<td  style='height:20px;'>" + element.LineType + "</td>" +
                               "<td  style='height:20px;'>" + element.NewPartCode + "</td>" +
                               "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                               "<td  style='height:20px;'> " + element.ProductDescription + "</td>" +
                               "<td  style='height:20px;'> " + element.POLineUnitPrice + "</td>" +
                               "<td  style='height:20px;'> " + element.ExpectedQuantity + "</td>" +
                               "<td  style='height:20px;'> " + element.ReceivedQuantity + "</td>" +
                               "<td  style=' color:black; background-color:#ffffff;'> " + element.Discrepancy + "</td>" +
                               "<td  style='height:20px;'> " + element.Comments + "</td></tr>";
                        }
                    }


                }

                //Close the discrepancy table in Email Body
                outlookBody += "</table><p style = 'font-size:14px;' >&nbsp;&nbsp;</p>";



                textBody += "</table>" +
                            "<tr>" +
                            "<table style='margin-top:20px; margin-bottom:20px; border: 0px solid white;'><tr style='border: 0px solid white; color:white; background-color:#778899; '><td style='height:40px; font-size:20px;'>Extras</td></tr></table></tr>";
                //"<tr style='border: 0px solid black; color:#778899; background-color:white; '><td style='height:40px; font-size:20px;'>Extras</td></tr>";
                //EXTRAS IN EMAIL BODY
                outlookBody += "<table style='margin-top:10px; margin-bottom:10px;'><tr style='border: 0px solid white; color:white; background-color:#778899; '><td style='height:20px; font-size:20px;'>Extras</td></tr></table>" +
                "<table style='margin-top:10px; margin-bottom:10px; border: 0px solid white;'>" +
                    "<tr style='color:black; background-color:#c5ccd3;'>" +
                    "<th  style='height:6px;'><b>Product Code</b></td>" +
                    "<th  style='height:6px;'><b>Extra Not On PO</b></td>" +
                    "<th  style='height:6px;'><b>Discrepancy</b></td>" +
                    "<th width = 300><b>Comments</b></td>" +
                    "</tr>";


                //End of Discrepancy Table
                //Extras Table
                textBody += "<tr style='border: 1px solid black;'><table style='border: 0px solid white;'> " +
                    //"<caption style=' color:#4da6ff; background-color:white; '>Extras</caption>" +
                    "<tr style='color:black; background-color:#c5ccd3;'>" +
                    "<th  style='height:20px;'><b>Product Code</b></td>" +
                    "<th  style='height:20px;'><b>Extra Not On PO</b></td>" +
                    "<th  style='height:20px;'><b>Discrepancy</b></td>" +
                    "<th width = 300><b>Comments</b></td>" +
                    "</tr>";

                //loop through import collection
                foreach (NetsuiteDiscrepancyReport element in dReportCollection)
                {
                    //Check if item is an extra
                    if (element.LineType != null)
                    {
                        if (element.LineType.Contains("(E)"))
                        {
                            outlookBody += "<tr>" +
                                        "<td  style='height:6px;'>" + element.ProductCode + "</td>" +
                                        "<td  style='height:6px;'>Extras Not On PO</td>" +
                                        "<td  style='height:6px; color:black; background-color:#ffd700; '> " + element.ReceivedQuantity + "</td>" +
                                        "<td  style='height:6px;'> " + element.Comments + "</td>" +
                                        "</tr>";

                            textBody += "<tr>" +
                                    "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                                    "<td  style='height:20px;'>Extras Not On PO</td>" +
                                    "<td  style=' color:black; background-color:#ffd700; '> " + element.ReceivedQuantity + "</td>" +
                                    "<td  style='height:20px;'> " + element.Comments + "</td>" +
                                    "</tr>";

                        }
                    }


                }

                //Close the extras table in Email Body
                outlookBody += "</table><p style = 'font-size:14px;' >&nbsp;&nbsp;</p>";


                textBody += "</table>" +
                            "</tr>" +
                            "<tr>" +
                            "<table style='margin-top:20px; margin-bottom:20px; border: 0px solid white;'><tr style='border: 0px solid white; color:white; background-color:#778899; '><td style='height:40px; font-size:20px;'>Issues</td></tr></table></tr>" +
                            //ISSUE HEADING
                            "<table style = 'margin-top:20px; margin-bottom:20px; border: 0px solid white;' >" +
                            "<tr style=' border:1px solid black; color:black; background-color:#c5ccd3;'>" +
                            "<th><b>Type</b></th>" +
                            "<th><b>New Part Code</b></th>" +
                            "<th><b>Product Code</b></th>" +
                            "<th><b>Product Description</b></th>" +
                            "<th><b>PO Price €/£</b></th>" +
                            "<th><b>PO Quantity</b></th>" +
                            "<th><b>Received</b></th>" +
                            "<th><b>Discrepancy</b></th>" +
                            "<th width = 300><b>Comments</b></th>" +
                            "</tr>";
                //ISSUES TABLE

                //PURCHASING SIDE



                foreach (NetsuiteDiscrepancyReport element in dReportCollection)
                {
                    if (element.LineType != null && element.LineType.Contains("(E)"))
                    {

                    }
                    else
                    {
                        if (Convert.ToInt32(element.Discrepancy) < 0)
                        {
                            textBody += "<tr>" +
                                "<td  style='height:20px;'>" + element.LineType + "</td>" +
                                "<td  style='height:20px;'>" + element.NewPartCode + "</td>" +
                                "<td  style='height:20px;'>" + element.ProductCode + "</td>" +
                                "<td  style='height:20px;'> " + element.ProductDescription + "</td>" +
                                "<td  style='height:20px;'> " + element.POLineUnitPrice + "</td>" +
                                "<td  style='height:20px;'> " + element.ExpectedQuantity + "</td>" +
                                "<td  style='height:20px;'> " + element.ReceivedQuantity + "</td>" +
                                "<td  style=' color:black; background-color:#ff6347;'> " + element.Discrepancy + "</td>" +
                                "<td  style='height:20px;'> " + element.Comments + "</td></tr>";


                        }

                    }


                }

                //  outlookBody += "</table>";


                textBody += "</table>" +
                            "</tr>" +
                            "<tr>" +
                            "<table style='margin-top:20px; margin-bottom:20px; border: 0px solid white;'><tr style='border: 0px solid white; color:white; background-color:#778899; '><td style='height:40px; font-size:20px;'>Serials</td></tr></table></tr>";

                //ADD SERIAL TO OUTLOOK EMAIL
                outlookBody += "<table style='margin-top:10px; margin-bottom:10px;'><tr style='border: 0px solid white; color:white; background-color:#778899; '><td style='height:20px; font-size:20px;'>Serials</td></tr></table>" +
               "<table style='margin-top:10px; margin-bottom:10px; border: 0px solid white;'>" +
               "<tr style='color:black; background-color:#c5ccd3;'>" +
               "<th  style='height:6px;'><b>Serial No</b></td>" +
               "<th  style='height:6px;'><b>Product Code</b></td>" +
               //"<th  style='height:20px;'><b>            </b></td>" +
               //"<th  style='height:20px; width = 300><b>            </b></td>" +
               "</tr>";


                //Add Blank Extras table  Fields Product Code/Discrepancy/
                textBody += "<tr style='border: 0px solid white;'>" +
                    "<table style='margin-top:20px; margin-bottom:20px; border: 0px solid white;'>" +
                    //"<caption style=' color:#4da6ff; background-color:white;'>" +
                    //"Extras" +
                    //"</caption>" +
                    "<tr style='color:black; background-color:#c5ccd3;'>" +
                    "<th  style='height:20px;'><b>Serial No</b></td>" +
                    "<th  style='height:20px;'><b>Product</b></td>" +
                    //"<th  style='height:20px;'><b>Discrepancy</b></td>" +
                    //"<th width = 300><b>Comments</b></td>" +
                    "</tr>"; ;

                //Add some blank fields
                foreach (NetsuiteInventory itm in vm.ScannedSerialCollection)
                {

                    outlookBody += "<tr>" +
                                 "<td style='height:6px;'>" + itm.SerialNumber + " </td>" +
                                 "<td style='height:6px;'>" + itm.ProductCode + "</td>" +
                                 //"<td style='height:20px;'>" + "" + "</td>" +
                                 //"<td style='height:20px;'>" + "" + "</td>" +
                                 "</tr></tr>";

                    textBody += "<tr>" +
                                  "<td style='height:10px;'>" + itm.SerialNumber + " </td>" +
                                  "<td style='height:10px;'>" + itm.ProductCode + "</td>" +
                                  //"<td style='height:20px;'>" + "" + "</td>" +
                                  //"<td style='height:20px;'>" + "" + "</td>" +
                                  "</tr></tr>";
                }
                //            "<tr><table><tr style='border: 1px solid black; color:white; background-color:#778899; '><td style='height:40px; font-size:20px;'>Extras</td></tr></table></tr>";

                //Close the extras table in Email Body
                outlookBody += "</table><p style = 'font-size:14px;' >&nbsp;&nbsp;</p>";

                //end of purchasing tables 
                textBody += "</table>" +
                            "</div>" +
                            "</body>" +
                            "</html>";


                outlookBody += "</table>" +
                        "</div>" +
                        "</body>" +
                        "</html>";





                //************WRITE TO HTML FILE************
                File.WriteAllText(vm.myDocs + @"\" + vm.PONumber + "_DiscrepancyList.html", textBody.ToString());

                //Create new string array
                filePaths = new string[3];

                filePaths[0] = vm.myDocs + @"\" + vm.PONumber + "_DiscrepancyList.html";


                //Add attachments.
                if (filePaths != null)
                {
                    foreach (string file in filePaths)
                    {
                        //attach the file
                        // MessageBox.Show(file);
                        if (File.Exists(file))
                        {
                            MsOutlook.Attachment oAttach = mail.Attachments.Add(file);
                        }



                    }
                }



                //Serials Table Single Column

                // end of document
                // textBody += "</div>";

                mail.Subject = subject;
                mail.To = recipients;
                //mail.HTMLBody = mail.HTMLBody + string.Format("{0} <br/> {1}", infoMessage, mail.HTMLBody);
                // mail.HTMLBody = mail.HTMLBody + string.Format("{0} <br/> {1}", textBody, mail.HTMLBody);

                //mail.HTMLBody = mail.HTMLBody + string.Format("{0} <br/> {1}", textBody, mail.HTMLBody);
                mail.HTMLBody = mail.HTMLBody + string.Format("{0} <br/> {1}", outlookBody, mail.HTMLBody);


                // For XML xhtml Method
                //mail.HTMLBody = mail.HTMLBody + string.Format("{0} <br/> {1}", sw, mail.HTMLBody);

                //if (sendType == MailSendType.SendDirect)
                //    mail.Send();
                //else if (sendType == MailSendType.ShowModal)
                mail.Display(true);
                //else if (sendType == MailSendType.ShowModeless)
                //    mail.Display(false);




                // MessageBox.Show(infoMessage.ToString());

                // set html body. 
                // add the body of the email
                // mail.HTMLBody = mail.HTMLBody;

                // mail.HTMLBody = 

                mail = null;
                outlookApp = null;
                return true;
            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                return false;
            }




        }

        //Failure email
        public void SendMailWithOutlook(string subject, string htmlBody, string recipients, string[] filePaths, ObservableCollection<NetsuiteInventory> fReportCollection)
        {



            try
            {
                //Sort Collection
               // fReportCollection.OrderByDescending(a => a.ProductCode);

                //Work Out Cost
                decimal TotalCost = 0.00M;
                int TotalCount = 0;
                string emailCurrency = "";

            //Check selected currency
            if (PoundCurrency.IsChecked == true)
            {
                emailCurrency = "£";
            }
            else if (EuroCurrency.IsChecked == true)
            {
                emailCurrency = "€";
            }
            else if (DollarCurrency.IsChecked == true)
            {
                emailCurrency = "$";
            }

            //Create HTML       
            string outlookBody = "<!DOCTYPE html>" +
                    "<html>" +
                    "<head></head>" +
                    "<style>" +
                    "body {font-size:12px;}" +
                    "div{text-align: center;}" +
                    "table { font-family: Arial, Helvetica, sans-serif; width:1200px; text-align: center;}" +
                    "table, caption, th, td{border: 0px solid White; text-align: center;}" +
                    "th { width:200px padding: 0px; text-align: center;}" +
                    "</style><body>" +
                    "<p style='font-size:14px;'>Hi,&nbsp;</p><p style='font-size:14px;'>Fail(s) for " + fReportCollection.FirstOrDefault().PONumber + " are as follows;&nbsp;</p>" +

                    //Containing Table
                    //"<table style='margin-top:20px border: 0px solid black;'>" +
                    "<table style='margin-top:20px border: 0px solid black;'>" +
                    //"<caption style=' color:#0099ff; background-color:white; ' >Drive Test</caption>" +
                    "<tr style=' border:1px solid black; color:black; background-color:#c5ccd3;'>" +
                    "<th><b>Part Number</b></th>" +
                    "<th><b>Quantity</b></th>" +
                    "<th><b>Cost Each (" + emailCurrency + ")</b></th>" +
                    "<th><b>Reason For Fail</b></th>" +
                    "<th><b>Drive Serial</b></th>" +
                    "<th><b>HP Serial</b></th>" +
                    "<th><b>PO</b></th>" +
                    "<th><b>Initial</b></th>" +
                    "<th><b>ATF</b></th>" +
                    "<th><b>7 Digit</b></th>" +
                    "</tr>";


                foreach (NetsuiteInventory nsi in fReportCollection)
                {
               

                //Add HTML Elements
                outlookBody += "<tr>";
                //Too split lines to make more readable
                if (outlookBody.Contains(nsi.ProductCode))
                {
                    outlookBody += "<td  style='height:20px;'></td>";
                    outlookBody += "<td  style='height:20px;'></td>"; //Quantity
                }
                else
                {
                    //Console.WriteLine(nsi.POQuantity);

                    //outlookBody += "<td  style='height:20px;'> " + nsi.ProductCode + "</td>";
                    //outlookBody += "<td  style='height:20px;'>" + fReportCollection.Where(fl => fl.ProductCode == nsi.ProductCode).Count().ToString() + "</td>"; //Quantity
                    //Required for readability of the email
                    if (nsi.POQuantity == "1")
                    {
                        outlookBody += "<td  style='height:20px;'> " + nsi.ProductCode + "</td>";
                        outlookBody += "<td  style='height:20px;'>" + fReportCollection.Where(fl => fl.ProductCode == nsi.ProductCode).Count().ToString() + "</td>"; //Quantity
                    }
                    else
                    {
                        outlookBody += "<td  style='height:20px;'> " + nsi.ProductCode + "</td>";
                        outlookBody += "<td  style='height:20px;'>" + nsi.POQuantity + "</td>"; //Quantity
                    }

                }
               
                
                outlookBody += "<td  style='height:20px;'> " + nsi.POLineUnitPrice + "</td>";
                outlookBody += "<td  style='height:20px;'> " + nsi.DriveFailedReason + " " + nsi.DriveFailedReasonSpecify + "</td>";
                outlookBody += "<td  style='height:20px;'>" + nsi.SerialNumber + "</td>";
                outlookBody += "<td  style='height:20px;'>" + nsi.HPSerialNumber + "</td>";
                outlookBody += "<td  style='height:20px;'>" + nsi.PONumber + "</td>";
                outlookBody += "<td  style='height:20px;'>" + nsi.TestedByOperative + "</td>";
                outlookBody += "<td  style='height:20px;'>" + nsi.ATF + "</td>";
                outlookBody += "<td  style='height:20px;'>" + nsi.SevenDigit + "</td>";
                //"<td  style='height:20px; background-color:#ff6347;'> " + nsi.DriveResult + "</td>" +
                outlookBody += "</tr>";


                TotalCost += Convert.ToDecimal(nsi.POLineUnitPrice) * Convert.ToInt32(nsi.POQuantity);
               
                //Required to handle any additional lines added that were not recieved.
                if (nsi.POQuantity == "1")
                {
                    TotalCount += 1;
                }
                else
                {
                    TotalCount += Convert.ToInt32(nsi.POQuantity);
                }
                    

            }





            //Close the discrepancy table in Email Body width:1200px;
            outlookBody += "</table>" +
                               "</table>" +
                               "<table style='width:1200px;'>" +
                               "<p style='font-size:18px; background-color:#ff6347; color:#ffffff;'>Total Failures  " + TotalCount + "&nbsp;</p>" +
                               "<p style='font-size:18px; background-color:#ff6347; color:#ffffff;'>Total Cost  " + emailCurrency + TotalCost + "&nbsp;</p>" +
                               "</table>" +
                               "</body>" +
                               "</html>";
                //Close HTML


                //MessageBox.Show(files[0]);
                //email.AttachFilesAndSendMail(files, vm.TestResultsLVSelectedItem.SerialNumber);
                email.DrivetestSendMailWithOutlook("Fail Report " + fReportCollection.FirstOrDefault().PONumber, outlookBody, "", null, EmailClass.MailSendType.ShowModal);


            }
            catch (Exception ex)
            {
                string exception = ex.Message;

            }




        }


        public ReportDriveTest(DriveTestViewModel passedViewModel, NetsuiteDiscrepancyReport dReport)
        {
            InitializeComponent();

            //Create Collection to hold discrepancy report
            ObservableCollection<NetsuiteDiscrepancyReport> dReportCollection = new ObservableCollection<NetsuiteDiscrepancyReport>();
            //add passed report
            dReportCollection.Add(dReport);

            //read the reports
            ReportLV1.ItemsSource = dReportCollection;
            //ReportLV2.ItemsSource = diagReport.testLogRecord;


            //wait until data populated
            //await PutTaskDelay(500);

            ReportLV1.Height = ReportLV1.ActualHeight;
            //ReportLV2.Height = ReportLV2.ActualHeight;

            // Send both documents to the print functions
            PrintHelper.ShowPrintPreview(PrintHelper.GetFixedDocument(ReportLV1, null, new PrintDialog(), this));
        }


     


        public void PrintFixedDocument()
        {
            //RESET SCROLL
            //ReportLV1.ScrollIntoView(ReportLV1.Items[0]);
            //ReportLV2.ScrollIntoView(ReportLV2.Items[0]);

            //ReportLV1.UpdateLayout();
            //ReportLV2.UpdateLayout();

            //ReportLV1.ScrollIntoView(ReportLV1.View);
            //ReportLV2.ScrollIntoView(ReportLV2.View);

            ResetListViewScroll(ReportLV1);
            //ResetListViewScroll(ReportLV2);

            //VisualTreeHelperEx.FindDescendantByType<ScrollViewer>(ReportLV1)?.ScrollToTop();

            //ReportLV2.ScrollIntoView(ReportLV1.SelectedItem);

            ReportLV1.Height = ReportLV1.ActualHeight;
           // ReportLV2.Height = ReportLV2.ActualHeight;

            // Send both documents to the print functions
            PrintHelper.ShowPrintPreview(PrintHelper.GetFixedDocument(ReportLV1, null, new PrintDialog(), this));



        }


        public void ResetListViewScroll(ListView LV)
        {
            //int childrenCount = VisualTreeHelper.GetChildrenCount(LV);
            //if (childrenCount > 0)
            //{
            //    DependencyObject depobj = VisualTreeHelper.GetChild(LV, 0);
            //    if (depobj is Decorator)
            //    {
            //        Decorator border = depobj as Decorator;
            //        ScrollViewer scroll = border.Child as ScrollViewer;
            //        scroll.ScrollToTop();
            //        scroll.ScrollToLeftEnd();
            //    }
            //}

            //try
            //{
            //    Decorator border = VisualTreeHelper.GetChild(LV, 0) as Decorator;
            //    ScrollViewer scroll = border.Child as ScrollViewer;
            //    scroll.ScrollToTop();
            //}
            //catch (Exception) { }
        }


       


        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }


        private void PrintBTN_Click(object sender, RoutedEventArgs e)
        {

          

            PrintHelper.ShowPrintPreview(PrintHelper.GetFixedDocument2(ContainerSP, null, new PrintDialog(), this));


            //PrintFixedDocument();
        }

        private async void EmailBTN_Click(object sender, RoutedEventArgs e)
        {
            //update the collection with modified version
            //vm.NetsuiteDiscrepancyCollection = dReportCollection;

            //Create Outlook Object
           await System.Threading.Tasks.Task.Run(() => SendMailWithOutlook("" + vm.PONumber + " Discrepancy Report", "", "", null, dReportCollection));

            //await PutTaskDelay(2000);
            ////Close the discrepancy itermediate window
            //this.Close();
        }

        private void EmailFailureBTN_Click(object sender, RoutedEventArgs e)
        {
            //Create Outlook Object
            SendMailWithOutlook("" + vm.PONumber + " Failure Report", "", "", null, fReportCollection);
        }

        private void AddLineBTN_Click(object sender, RoutedEventArgs e)
        {
            NetsuiteInventory nsi = new NetsuiteInventory();
            nsi.PONumber = fReportCollection.FirstOrDefault().PONumber;
            nsi.POQuantity = "";
            nsi.TestedByOperative = vm.CurrentUser;// fReportCollection.FirstOrDefault().TestedByOperative;
            //Add a new line
            fReportCollection.Add(nsi);
        }

        private void DeleteLineBTN_Click(object sender, RoutedEventArgs e)
        {

            fReportCollection.Remove((NetsuiteInventory)ReportLV2.SelectedItem);
        }
    }
}

