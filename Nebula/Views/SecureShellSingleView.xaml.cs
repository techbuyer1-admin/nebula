﻿using Renci.SshNet;
using Renci.SshNet.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Models;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for SecureShellSingleView.xaml
    /// </summary>
    public partial class SecureShellSingleView : UserControl
    {


        //Console.WriteLine("Hello World!");
        //Connection information
        //string user = "";
        //string pass = "";
        //string host = ""; //127.0.0.1 for local host or use netbios name NJM-PCW7  UbuntuVM

        public SecureShellSingleView()
        {
            InitializeComponent();

            //the contents of the private key generated on the machine that makes the connection to the server

            ////Create an instance of the PrivateKeyInfo Class 
            //PrivateKeyInfo pki = new PrivateKeyInfo();







            //Console.WriteLine("Please Enter Hostname to connect to:");
            //host = Console.ReadLine();
            //Console.WriteLine("Please Enter Your UserName:");
            //user = Console.ReadLine();
            //Console.WriteLine("Please Enter Your Password:");
            //pass = ReadPassword();




            ////Set up the SSH connection

            ////using privatekey method, add in the property ThePrivateKey from the PrivateKeyInfo Class
            //using (var client = new SshClient(host, user, pki.ThePrivateKey))
            //{
            //    //Start the connection
            //    client.Connect();
            //    var output = client.RunCommand("echo test");
            //    //var output = client.RunCommand("mkdir TraceyMyers");
            //    //client.RunCommand("sudo gedit /etc/ssh/sshd_config");
            //    //Console.ReadLine();
            //    client.Disconnect();
            //    MessageBox.Show(output.ToString());
            //    Console.WriteLine(output.Result);
            //}


        }


        



       


public string ExecuteCommandSsh(string host, int port, string username, string password, string commands)
        {
            var returnMessage = string.Empty;

            try
            {
                if(CommandTXTB.Text == string.Empty)
                {
                    MessageBox.Show("Please enter a Command in the Command Textbox");
                }
                else
                {
                    using (var client = new SshClient(host, port, username, password))
                    {
                        //Create the command string
                        var fullCommand = string.Empty;
                        foreach (var command in commands)
                        {
                            fullCommand += command + "\n";
                        }

                        client.Connect();
                        var sshCommand = client.CreateCommand(fullCommand);
                        sshCommand.CommandTimeout = new TimeSpan(0, 0, 10);

                        try
                        {
                            sshCommand.Execute();
                           // MessageBox.Show(sshCommand.Result);
                            returnMessage = sshCommand.Result;
                        }
                        catch (SshOperationTimeoutException)
                        {
                          returnMessage = sshCommand.Result;
                        }


                        client.Disconnect();
                    }
                }


               
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return returnMessage;
        }



        public void SendSSHCommand(string host, string username, string password, string command)
        {

            if (command == "")
            {
                MessageBox.Show("Please enter a Command in the Command Textbox");
            }
            else if (host == string.Empty)
            {
                MessageBox.Show("Please enter a HostName or IP");
            }
            else if (username == string.Empty)
            {
                MessageBox.Show("Please enter a Username");
            }
            else if (password == string.Empty)
            {
                MessageBox.Show("Please enter a Password");
            }
            else
            {

                try
                {

              
                //Set up the SSH connection
                using (var client = new SshClient(host, username, password))
                {

                    //Accept Host key
                    client.HostKeyReceived += delegate (object sender, HostKeyEventArgs e)
                    {
                        e.CanTrust = true;
                    };

                   
                    client.Connect();


                        //   MessageBox.Show(client.RunCommand(command).Result);

              
                  
                     
                    OutputTXTB.Text += client.RunCommand(command).Result;


                    OutputTXTB.ScrollToEnd();

                     client.Disconnect();
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }

          
        }


        public static string ReadPassword()
        {
            string password = "";
            ConsoleKeyInfo info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter)
            {
                if (info.Key != ConsoleKey.Backspace)
                {
                    Console.Write("*");
                    password += info.KeyChar;
                }
                else if (info.Key == ConsoleKey.Backspace)
                {
                    if (!string.IsNullOrEmpty(password))
                    {
                        // remove one character from the list of password characters
                        password = password.Substring(0, password.Length - 1);
                        // get the location of the cursor
                        int pos = Console.CursorLeft;
                        // move the cursor to the left by one character
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                        // replace it with space
                        Console.Write(" ");
                        // move the cursor to the left by one character again
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                    }
                }
                info = Console.ReadKey(true);
            }

            // add a new line because user pressed enter at the end of their password
            Console.WriteLine();
            return password;
        }

       

        private void CommandTXTB_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void CommandTXTB_KeyDown(object sender, KeyEventArgs e)
        {
           


            }

        private void ClearBTN_Click(object sender, RoutedEventArgs e)
        {
            OutputTXTB.Text = "";
        }

        private void HostTxtB_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void CommandTXTB_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                string host = HostTxtB.Text;
                string username = UserTxtB.Text;
                string password = PasswordTxtB.Password;
                string command = CommandTXTB.Text;


                //  //MessageBox.Show("Enter Hit");
                // if(HostTxtB.Text == "" || UserTxtB.Text == "" || PasswordTxtB.Password == "")
                // await System.Threading.Tasks.Task.Run(() => SendSSHCommand(host, username, password, command));


                //SendSSHCommand(host, username, password, command);

                using (var client = new SshClient(host, username, password))
                {
                    try
                    {
                        //Accept Host key
                        //client.HostKeyReceived += delegate (object sender, HostKeyEventArgs e)
                        //{
                        //    e.CanTrust = true;
                        //};


                        client.Connect();


                        //   MessageBox.Show(client.RunCommand(command).Result);




                        OutputTXTB.Text += client.RunCommand(command).Result;


                        OutputTXTB.ScrollToEnd();

                        client.Disconnect();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                        client.Disconnect();
                    }
                  
                }

                    //  CommandTXTB.Text = "";
                    //
                    //using (var client = new SshClient(host, username, password))
                    //  {
                    //   client.Connect();

                    //     using (var command2Send = client.CreateCommand("command1 && command2"))
                    //     {
                    //        Console.Write(command2Send.Execute()); //Don't forget to Execute the command
                    //     }

                    //    client.Disconnect();
                    // }



                }
        }

        private void SendBTN_Click(object sender, RoutedEventArgs e)
        {
            SendSSHCommand(HostTxtB.Text, UserTxtB.Text, PasswordTxtB.Password, CommandTXTB.Text);
          
            //await System.Threading.Tasks.Task.Run(() => SendSSHCommand(HostTxtB.Text, UserTxtB.Text, PasswordTxtB.Password, CommandTXTB.Text));
        }
    }
 }

