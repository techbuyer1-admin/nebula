﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;
using System.Diagnostics;
using System.IO;
using System.Web.Script.Serialization;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Net.Security;
using Newtonsoft.Json;
using System.Xml;
using System.Data;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using Microsoft.VisualBasic.FileIO;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for QualityControlView.xaml
    /// </summary>
    public partial class QualityControlView : UserControl
    {

        //declare viewmodel
        GoodsInOutViewModel vm;
        int FilterYearSpan = -12;


        //DB Connection String
        public string cs = "";



        public QualityControlView()
        {
            InitializeComponent();



            //Check if Test Mode Checked
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\QCLogs.db";
            }
            else
            {

                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/QCLogs/QCLogs.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/QCLogs/QCLogs.db";
                }
                else
                {

                    //Set connection string to Live DB
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\QCLogs.db";
                }

            }
        }


        public QualityControlView(GoodsInOutViewModel PassedViewModel)
        {
            InitializeComponent();
            vm = PassedViewModel;
            this.DataContext = vm;
            vm.CurrentFilter = "Please Wait...";

            //Check if Test Mode Checked
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\QCLogs.db";
            }
            else
            {
                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/QCLogs/QCLogs.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/QCLogs/QCLogs.db";
                }
                else
                {

                    //Set connection string to Live DB
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\QCLogs.db";
                }



            }


            //Set all ComboBoxes to Select Nothing
            OperativeCB.SelectedIndex = -1;
            AddBookedInCB.SelectedIndex = -1;


         
            //Get starting data
            GetInitialData();

            // Load the Product Codes only if inventory file exists
            if (File.Exists(@"" + vm.myDocs + @"\Inventory.csv"))
            {
                //Will Create a new file for each year
                string newFileName = @"" + vm.myDocs + @"\Inventory.csv";
                vm.ProductCodes.Clear();
                //Uses a VB class to process the file instead of streamreader
                using (TextFieldParser parser = new TextFieldParser(newFileName))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        string currentLine = parser.ReadLine();

                        //Add 
                        //PartCodeSearch.AutoSuggestionList.Add(FromCsv(currentLine));
                        vm.ProductCodes.Add(FromCsv(currentLine));

                    }
                }
            }

            //Count
            CalculateTotals();

            //foreach(string str in vm.ProductCodes)
            //{
            //    MessageBox.Show(str);
            //}


        }

        //To check the presence of a column in a table
        private bool CheckColumnExists(SQLiteConnection conn, string tableName, string columnName)
        {

            var cmd = conn.CreateCommand();
            cmd.CommandText = string.Format("PRAGMA table_info({0})", tableName);

            var reader = cmd.ExecuteReader();
            int nameIndex = reader.GetOrdinal("Name");
            while (reader.Read())
            {
                if (reader.GetString(nameIndex).Equals(columnName))
                {

                    return true;
                }
            }

            return false;
        }


        public async void GetInitialData()
        {
            vm.CurrentFilter = "Please Wait...";
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            vm.QCItems.Clear();

            //Set Initial Dates for Weekly Records
            vm.DateFrom = DateTimeExtensions.StartOfWeek(DateTime.Now, DayOfWeek.Monday);
            vm.DateTo = DateTime.Now;

            //Check for new columns pressence, if not present add
            if (CheckColumnExists(sqlite_conn, "QualityControl", "TestedBy") != true)
            {
                //Check for existense of columns, if not add
                sqlite_cmd.CommandText = "ALTER TABLE QualityControl ADD COLUMN TestedBy TEXT Default '';";
                sqlite_cmd.ExecuteNonQuery();
            }




            //MessageBox.Show(@"\\pinnacle.local\tech_resources\Nebula\Repository\Data\" + System.Globalization.RegionInfo.CurrentRegion.DisplayName + @"\QCLogs.db");

            //Load Any Operatives
            //Query DB
            await Task.Run(() => ReadOperativeData(sqlite_conn, "SELECT * FROM Operatives ORDER BY Operative"));

            //Monday To Monday
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + vm.DateFrom.ToString("yyyy-MM-dd") + "') AND date('" + vm.DateTo.ToString("yyyy-MM-dd") + "') ORDER BY Date DESC"));
            //Set current filter
            vm.CurrentFilter = "This Week " + vm.QCItems.Count.ToString() + " Total Items";

            //All Data
            //ReadAllData(sqlite_conn);
            sqlite_conn.Close();



        }



        //Extract the Product Codes from the file
        public string FromCsv(string csvLine)
        {
            string code = "";
            if (csvLine != null)
            {
                string[] values = csvLine.Split(',');


                if (values[0] != string.Empty)
                    code = Convert.ToString(values[0]);
            }

            return code;
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime dt = DateTime.Now.StartOfWeek(DayOfWeek.Monday);

            if (dt.ToShortDateString() == DateTime.Now.ToShortDateString())
            {
                // MessageBox.Show("Check if file already exists, if not Create a new XML File for the week " + dt.ToShortDateString());
            }
            else
            {
                // MessageBox.Show("Days Not Matched  " + dt.ToShortDateString());
            }


        }




        private void QCLV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {



            int ItemIndex;
            ItemIndex = QCLV.SelectedIndex;
            //MessageBox.Show(ItemIndex.ToString());
            if (ItemIndex != -1)
            {
                QCRecordsModel RecordRow = QCLV.Items.GetItemAt(ItemIndex) as QCRecordsModel;
                vm.UpdateQCRecord = RecordRow;
            }



            //  MessageBox.Show(vm.WeeklyServerRecord.SONumber);

        }


        public void CalculateTotals()
        {
            //Zero each of the properties
            vm.BentPinsTotals = 0;
            vm.WrongPNTotals = 0;
            vm.DirtyTotals = 0;
            vm.MissingPaperWorkTotals = 0;
            vm.MissingPartsTotals = 0;
            vm.ExtraPartsTotals = 0;
            vm.ILOSwitchTotals = 0;
            vm.DataHolderTotals = 0;
            vm.DamagedTotals = 0;
            vm.WrongCablingTotals = 0;
            vm.StickersTotals = 0;
            vm.FirmwareTotals = 0;
            vm.OtherTotals = 0;



            foreach (QCRecordsModel itm in QCLV.Items)
            {

                switch (itm.Issue)
                {
                    case "Bent pin(s)":
                        vm.BentPinsTotals += 1;
                        // MessageBox.Show(vm.WeekComm.BuildTotals.ToString());
                        break;
                    case "Wrong PN":
                        vm.WrongPNTotals += 1;
                        // MessageBox.Show(vm.WeekComm.BuildTotals.ToString());
                        break;
                    case "Dirty":
                        vm.DirtyTotals += 1;
                        // MessageBox.Show(vm.WeekComm.BuildTotals.ToString());
                        break;
                    case "Missing/wrong paperwork":
                        vm.MissingPaperWorkTotals += 1;
                        // MessageBox.Show(vm.WeekComm.BuildTotals.ToString());
                        break;
                    case "Missing parts":
                        vm.MissingPartsTotals += 1;
                        //MessageBox.Show(itm.Quantity.ToString());
                        break;
                    case "Extra parts":
                        vm.ExtraPartsTotals += 1;
                        //MessageBox.Show(itm.Quantity.ToString());
                        break;
                    case "ILO switch":
                        vm.ILOSwitchTotals += 1;
                        //MessageBox.Show(itm.Quantity.ToString());
                        break;
                    case "Data holder left inside":
                        vm.DataHolderTotals += 1;
                        //MessageBox.Show(itm.Quantity.ToString());
                        break;
                    case "Damaged":
                        vm.DamagedTotals += 1;
                        //MessageBox.Show(itm.Quantity.ToString());
                        break;
                    case "Not Booked In":
                        vm.NotBookedInTotals += 1;
                        //MessageBox.Show(itm.Quantity.ToString());
                        break;
                    case "Wrong cabling":
                        vm.WrongCablingTotals += 1;
                        //MessageBox.Show(itm.Quantity.ToString());
                        break;
                    case "Stickers":
                        vm.StickersTotals += 1;
                        //MessageBox.Show(itm.Quantity.ToString());
                        break;
                    case "Firmware":
                        vm.FirmwareTotals += 1;
                        //MessageBox.Show(itm.Quantity.ToString());
                        break;
                    default:
                        if (itm.Issue != "")
                        {
                            vm.OtherTotals += 1;
                        }

                        break;
                }


                ////new method to use for separation totals
                //if (itm.BuildQuantity > 0 || itm.BuildFWQuantity > 0 || itm.BuildTestQuantity > 0 || itm.RMAReplaceBuildQuantity > 0)
                //    {
                //        vm.BuildTotals += itm.BuildQuantity + itm.BuildFWQuantity + itm.BuildTestQuantity + itm.RMAReplaceBuildQuantity;
                //    }

                //if (itm.CTOQuantity > 0)
                //{
                //    vm.CTOTotals += itm.CTOQuantity;
                //}

                //if (itm.MotherboardQuantity > 0)
                //{
                //    //MessageBox.Show(itm.MotherboardQuantity.ToString());
                //    vm.MotherboardTotals += itm.MotherboardQuantity;
                //}




            }

        }



        private void AddPersonBTN_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(vm.Operative);

            //Check form has been filled out
            //if(vm != null)
            if (string.IsNullOrWhiteSpace(vm.Operative))
            {
                MessageBox.Show("Please Enter A Value for Full Name", "Add Operative", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                //if all criteria met create the job
                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);




                //CreateTable(sqlite_conn);
                InsertOperativeData(sqlite_conn);


                vm.OperativeCollection.Clear();

                //GET DATE VALUES SELECTED ON THE DATEPICKERS
                //var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                //var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

                //Read partial data
                ReadOperativeData(sqlite_conn, "SELECT * FROM Operatives ORDER BY Operative");


                sqlite_conn.Close();

                //reset object to default values
                vm.Operative = string.Empty;
                vm.IsQC = false;
                OperativeCB.SelectedIndex = -1;

                //SortDescription sd = new SortDescription("Name", ListSortDirection.Ascending);



                //cb1.Items.SortDescriptions.Add(sd);



            }
        }


        private void AddJobBTN_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(PartCodeSearch.TextBoxText); 
            //Check form has been filled out
            //if (string.IsNullOrWhiteSpace(vm.AddQCRecord.PONumber))
            //{
            //    MessageBox.Show("PO Number is required!", "Create Record", MessageBoxButton.OK, MessageBoxImage.Warning);
            //}
            if (string.IsNullOrWhiteSpace(vm.AddQCRecord.PartNumber))
            {
                MessageBox.Show("The Part Number field is required!", "Create Record", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (string.IsNullOrWhiteSpace(vm.AddQCRecord.Issue))
            {
                MessageBox.Show("The Issue field is required!", "Create Record", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                //if all criteria met create the job
                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);




                //CreateTable(sqlite_conn);
                InsertData(sqlite_conn);


                vm.QCItems.Clear();

                //GET DATE VALUES SELECTED ON THE DATEPICKERS
                var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

                //Read partial data
                ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') ORDER BY Date DESC");


                sqlite_conn.Close();

                //reset object to default values
                vm.AddQCRecord.PONumber = string.Empty;
                vm.AddQCRecord.PartNumber = string.Empty;
                vm.AddQCRecord.Quantity = string.Empty;
                vm.AddQCRecord.SerialNo = string.Empty;
                vm.AddQCRecord.BookedInBy = string.Empty;
                vm.AddQCRecord.TestedBy = string.Empty;
                //vm.AddQCRecord.Date = string.Empty;
                vm.AddQCRecord.Status = string.Empty;
                vm.AddQCRecord.Comments = string.Empty;
                vm.AddQCRecord.Issue = string.Empty;
                vm.AddQCRecord.Specify = string.Empty;
                vm.AddQCRecord.QCCheckedBy = string.Empty;
                AddBookedInCB.SelectedIndex = -1;
                AddTestedByCB.SelectedIndex = -1;
                PartCodeSearch.autoTextBox.Text = string.Empty;
            }





        }




        private void UpdateJobBTN_Click(object sender, RoutedEventArgs e)
        {

            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            //CreateTable(sqlite_conn);
            //InsertData(sqlite_conn);


            //vm.WeeklyServerRecordCollection.Clear();
            //ReadData(sqlite_conn);
            //MessageBox.Show("Now Update");


            //Update first selected item
            UpdateData(sqlite_conn);

            //Multi-Status update
            foreach (QCRecordsModel selectedItem in QCLV.SelectedItems)
            {
                //Assign to the update record
                //Requires ID
                vm.UpdateQCRecord.Id = selectedItem.Id;
                //Update ListView Selection
                selectedItem.Status = vm.UpdateQCRecord.Status;
                //selectedItem.Issue = vm.UpdateQCRecord.Issue;
                //selectedItem.Specify = vm.UpdateQCRecord.Specify;
                //Update Status only
                UpdateStatus(sqlite_conn);
            }




            // MessageBox.Show("Update Complete");
            vm.QCItems.Clear();
            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //MessageBox.Show(dateFrom + " To " + dateTo);

            // string SQLcmd = $"SELECT * FROM sorteos WHERE DATE(fecha) BETWEEN ('{FechaInicio}') AND ('{FechaFinal}')";

            //ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE date(Day) BETWEEN date('2020-10-05') AND date('2020-10-05')");


            ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') ORDER BY Date DESC");


            sqlite_conn.Close();

            //reset object to default values
            //reset object to default values
            vm.AddQCRecord.PONumber = string.Empty;
            vm.AddQCRecord.PartNumber = string.Empty;
            vm.AddQCRecord.BookedInBy = string.Empty;
            vm.AddQCRecord.TestedBy = string.Empty;
            vm.AddQCRecord.Quantity = string.Empty;
            vm.AddQCRecord.SerialNo = string.Empty;
            //vm.AddQCRecord.Date = string.Empty;
            vm.AddQCRecord.Status = string.Empty;
            vm.AddQCRecord.Comments = string.Empty;
            vm.AddQCRecord.Issue = string.Empty;
            vm.AddQCRecord.Specify = string.Empty;
            vm.AddQCRecord.QCCheckedBy = string.Empty;

            //UpdateBookedInCB.SelectedIndex = -1;
            //UpdateCheckedByCB.SelectedItem = -1;


        }

        private void DeleteJobBTN_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("Are you sure you want to delete this item?", "Delete Entry", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (mbr == MessageBoxResult.Yes)
            {
                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);
                //CreateTable(sqlite_conn);
                DeleteData(sqlite_conn);


                vm.QCItems.Clear();
                //GET DATE VALUES SELECTED ON THE DATEPICKERS
                var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                var dateTo = vm.DateTo.ToString("yyyy-MM-dd");



                ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') ORDER BY Date DESC");



                sqlite_conn.Close();

                //reset object to default values
                vm.UpdateQCRecord.PONumber = string.Empty;
                vm.UpdateQCRecord.PartNumber = string.Empty;
                vm.UpdateQCRecord.BookedInBy = string.Empty;
                vm.UpdateQCRecord.TestedBy = string.Empty;
                vm.UpdateQCRecord.Quantity = string.Empty;
                vm.UpdateQCRecord.SerialNo = string.Empty;
                //vm.UpdateQCRecord.Date = string.Empty;
                vm.UpdateQCRecord.Status = string.Empty;
                vm.UpdateQCRecord.Comments = string.Empty;
                vm.UpdateQCRecord.Issue = string.Empty;
                vm.UpdateQCRecord.Specify = string.Empty;
                vm.UpdateQCRecord.QCCheckedBy = string.Empty;


                //UpdateBookedInCB.SelectedIndex = -1;
                //UpdateCheckedByCB.SelectedItem = -1;



            }
            else if (mbr == MessageBoxResult.No)
            {
                //do something else
            }









        }



        private void DeletePersonBTN_Click(object sender, RoutedEventArgs e)
        {

            // MessageBox.Show(vm.OperativeSelected);

            MessageBoxResult mbr = MessageBox.Show("Are you sure you want to delete this item?", "Delete Entry", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (mbr == MessageBoxResult.Yes)
            {
                SQLiteConnection sqlite_conn;
                sqlite_conn = CreateConnection(cs);
                //CreateTable(sqlite_conn);
                DeleteOperativeData(sqlite_conn);


                //vm.WeeklyServerRecordCollection.Clear();
                //GET DATE VALUES SELECTED ON THE DATEPICKERS
                var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

                vm.OperativeCollection.Clear();

                //GET DATE VALUES SELECTED ON THE DATEPICKERS
                //var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
                //var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

                //Read partial data
                ReadOperativeData(sqlite_conn, "SELECT * FROM Operatives ORDER BY Operative");

                sqlite_conn.Close();

                //reset object to default values
                vm.AddQCRecord.PONumber = string.Empty;
                vm.AddQCRecord.PartNumber = string.Empty;
                vm.AddQCRecord.Quantity = string.Empty;
                vm.AddQCRecord.SerialNo = string.Empty;
                vm.AddQCRecord.BookedInBy = string.Empty;
                vm.AddQCRecord.TestedBy = string.Empty;
                //vm.AddQCRecord.Date = string.Empty;
                vm.AddQCRecord.Status = string.Empty;
                vm.AddQCRecord.Comments = string.Empty;
                vm.AddQCRecord.Issue = string.Empty;
                vm.AddQCRecord.Specify = string.Empty;
                vm.AddQCRecord.QCCheckedBy = string.Empty;

                OperativeCB.SelectedIndex = -1;



            }
            else if (mbr == MessageBoxResult.No)
            {
                //do something else
            }
        }


        private async void QueryJobBTN_Click(object sender, RoutedEventArgs e)
        {
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            vm.QCItems.Clear();

            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //MessageBox.Show(dateFrom + " To " + dateTo);

            // string SQLcmd = $"SELECT * FROM sorteos WHERE DATE(fecha) BETWEEN ('{FechaInicio}') AND ('{FechaFinal}')";

            //ReadData(sqlite_conn, "SELECT * FROM Jobs WHERE date(Day) BETWEEN date('2020-10-05') AND date('2020-10-05')");

            if (vm.CurrentFilter == "On Hold Jobs")
            {
                ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE Status = 'On Hold' ORDER BY Date DESC");
            }
            else
            {
                ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') ORDER BY Date DESC");
            }



            //Close Db
            sqlite_conn.Close();



            //Create Flow Document

            FlowDocument ReportFD = new FlowDocument();
            Section MainSection = new Section();
            MainSection.Padding = new Thickness(4);
            Section mySectionReportHeader = new Section();
            mySectionReportHeader.Background = Brushes.MediumPurple; //(Brush)bc.ConvertFrom("#CC119EDA");
            Paragraph ReportHeader = new Paragraph(new Run("Quality Control Report"));
            ReportHeader.Padding = new Thickness(10);
            ReportHeader.FontSize = 30;
            ReportHeader.FontFamily = new FontFamily("Segoe UI");
            ReportHeader.Foreground = Brushes.White;
            ReportHeader.TextAlignment = TextAlignment.Center;

            //Report date or title header
            Paragraph ReportDateHeader = new Paragraph();
            if (vm.CurrentFilter == "On Hold Jobs")
            {
                ReportDateHeader = new Paragraph(new Run("Jobs On Hold"));
                ReportDateHeader.Padding = new Thickness(4);
                ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
                ReportDateHeader.FontSize = 20;
                ReportDateHeader.Foreground = Brushes.White;
                ReportDateHeader.TextAlignment = TextAlignment.Center;
            }
            else
            {
                ReportDateHeader = new Paragraph(new Run("" + vm.DateFrom.ToShortDateString() + " - " + vm.DateTo.ToShortDateString() + ""));
                ReportDateHeader.Padding = new Thickness(10);
                ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
                ReportDateHeader.FontSize = 20;
                ReportDateHeader.Foreground = Brushes.White;
                ReportDateHeader.TextAlignment = TextAlignment.Center;
            }



            //Add Paragraph to Header
            mySectionReportHeader.Blocks.Add(ReportHeader);
            mySectionReportHeader.Blocks.Add(ReportDateHeader);

            //Totals 
            Section mySectionTotals = new Section();
            mySectionTotals.Background = Brushes.MediumPurple; //(Brush)bc.ConvertFrom("#CC119EDA");
            Paragraph ReportTotalsHeader = new Paragraph(new Run("Report Totals"));
            ReportTotalsHeader.Padding = new Thickness(10);
            ReportTotalsHeader.FontFamily = new FontFamily("Segoe UI");
            ReportTotalsHeader.FontSize = 20;
            ReportTotalsHeader.Foreground = Brushes.White;
            ReportTotalsHeader.TextAlignment = TextAlignment.Center;

            CalculateTotals();

            //MessageBox.Show("Builds=" + vm.BuildTotals + "Bulds=" + vm.CTOTotals);

            Paragraph ReportTotals = new Paragraph(new Run("Bent Pin(s) " + vm.BentPinsTotals + " | Wrong PN " + vm.WrongPNTotals + " | Dirty " + vm.DirtyTotals + " | Paperwork Issue " + vm.MissingPaperWorkTotals +
                " | Missing Parts " + vm.MissingPartsTotals + " | Extra Parts " + vm.ExtraPartsTotals + " | iLO Switch " + vm.ILOSwitchTotals + " | Data Holder Present " + vm.DataHolderTotals +
                " | Damaged " + vm.DamagedTotals + " | Wrong Cabling " + vm.WrongCablingTotals + " | Stickers " + vm.StickersTotals + " | Firmware " + vm.FirmwareTotals + "" + " | Other " + vm.OtherTotals + ""));

            ReportTotals.Padding = new Thickness(4);
            ReportTotals.FontFamily = new FontFamily("Segoe UI");
            ReportTotals.FontSize = 15;
            ReportTotals.Foreground = Brushes.White;
            ReportTotals.TextAlignment = TextAlignment.Center;

            //Add Paragraph to Header
            mySectionTotals.Blocks.Add(ReportTotalsHeader);
            mySectionTotals.Blocks.Add(ReportTotals);

            //Records
            Section mySectionRecordsHeader = new Section();
            Section mySectionRecords = new Section();

            Table RecordTable = new Table();
            RecordTable.CellSpacing = 2;
            RecordTable.TextAlignment = TextAlignment.Center;
            //RecordTable.BorderBrush = Brushes.Black;
            //RecordTable.BorderThickness = new Thickness(.5);

            // Create 6 columns and add them to the table's Columns collection.
            //int numberOfColumns = 22;
            int numberOfColumns = 11;
            for (int x = 0; x < numberOfColumns; x++)
            {
                RecordTable.Columns.Add(new TableColumn());

                //ALTER THE COLUMN WIDTHS
                if (x == 0)
                {
                    RecordTable.Columns[x].Width = new GridLength(8, GridUnitType.Star);
                }
                else if (x == 1 || x == 2 || x == 4 || x == 5 || x == 6 || x == 7 || x == 8 || x == 9 || x == 10)
                {
                    RecordTable.Columns[x].Width = new GridLength(10, GridUnitType.Star);
                }
                else if (x == 3)
                {
                    RecordTable.Columns[x].Width = new GridLength(6, GridUnitType.Star);
                }
                else
                {
                    RecordTable.Columns[x].Width = new GridLength(5, GridUnitType.Star);
                }


            }

            // Create and add an empty TableRowGroup to hold the table's Rows.
            RecordTable.RowGroups.Add(new TableRowGroup());

            // Add the first (title) row.
            RecordTable.RowGroups[0].Rows.Add(new TableRow());

            // Alias the current working row for easy reference.
            TableRow currentRow = RecordTable.RowGroups[0].Rows[0];



            //// Add the header row with content,
            //currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Records"))));
            //// and set the row to span all 6 columns.
            //currentRow.Cells[0].ColumnSpan = 10;

            //// Add the second (header) row.
            //RecordTable.RowGroups[0].Rows.Add(new TableRow());
            currentRow = RecordTable.RowGroups[0].Rows[0];
            // Global formatting for the title row.
            currentRow.Background = Brushes.MediumPurple;
            currentRow.Foreground = Brushes.White;
            currentRow.FontSize = 20;

            currentRow.FontWeight = System.Windows.FontWeights.Bold;

            // Global formatting for the header row.
            currentRow.FontSize = 9;
            currentRow.FontWeight = FontWeights.Normal;
            currentRow.FontFamily = new FontFamily("Segoe UI");
            // Add cells with content to the second row.
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Date"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Part No"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("PO Number"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Qty"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Serial"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Booked In By"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Tested By"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Checked"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Issue"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Status"))));
            currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Comments"))));




            // Add Headers to table
            mySectionRecords.Blocks.Add(RecordTable);

            //Start of Row To start adding collection
            int RowIndex = 1;

            //Add Loop Through Records

            foreach (QCRecordsModel itm in QCLV.Items)
            {

                RecordTable.RowGroups[0].Rows.Add(new TableRow());
                currentRow = RecordTable.RowGroups[0].Rows[RowIndex];
                currentRow.FontWeight = FontWeights.Normal;
                currentRow.FontSize = 8;
                currentRow.Foreground = Brushes.Black;
                currentRow.FontFamily = new FontFamily("Segoe UI");

                //Add Table
                //Paragraph ReportRecord = new Paragraph(new Run("" + itm.Day + "|" + itm.SONumber + "|" + itm.ServerModel + "|" + itm.JobType + "|" + itm.Quantity 
                //+ "|" + itm.SalesRep + "|" + itm.Technician + "|" + itm.Issues + "|" + itm.ActionTaken + "|" + itm.Status));
                //ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
                //ReportDateHeader.FontSize = 10;
                //ReportDateHeader.Foreground = Brushes.MediumPurple;
                //ReportDateHeader.TextAlignment = TextAlignment.Left;

                // Add cells with content to the third row.
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.Date.ToShortDateString().Replace("//", "-")))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.PartNumber))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.PONumber))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.Quantity))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.SerialNo))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.BookedInBy))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.TestedBy))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.QCCheckedBy))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.Issue))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.Status))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(itm.Comments))));




                //for (int n = 0; n < currentRow.Cells.Count(); n++)
                //{
                //    //for border
                //    //currentRow.Cells[n].BorderThickness = new Thickness(0,1,0,1);
                //    //currentRow.Cells[n].BorderBrush = Brushes.Black;
                //    if (n % 2 == 0)
                //        currentRow.Cells[n].Background = Brushes.AliceBlue;

                //    else
                //        currentRow.Cells[n].Background = Brushes.White;
                //}


                //Increment By 1  RecordTable.RowGroups[0]
                RowIndex++;
            }


            //To be used to create row colouring, needs to target the row groups
            for (int n = 1; n < RecordTable.RowGroups[0].Rows.Count(); n++)
            {
                //for border
                //currentRow.Cells[n].BorderThickness = new Thickness(0,1,0,1);
                //currentRow.Cells[n].BorderBrush = Brushes.Black;
                if (n % 2 == 0)
                    RecordTable.RowGroups[0].Rows[n].Background = Brushes.AliceBlue;

                else
                    RecordTable.RowGroups[0].Rows[n].Background = Brushes.White;
            }

            //Add Table To Section
            mySectionRecords.Blocks.Add(RecordTable);



            //1 Add Header To Main Section
            MainSection.Blocks.Add(mySectionReportHeader);
            //2 Add Totals Section To Main Section
            MainSection.Blocks.Add(mySectionTotals);
            //3 Add Records to Main Section
            MainSection.Blocks.Add(mySectionRecords);


            //Add Main Section to flowdocument
            ReportFD.Blocks.Add(MainSection);


            await PutTaskDelay(500);

            //set clipboard name
            Clipboard.SetText("Quality_Control_Report " + vm.DateFrom.ToShortDateString().Replace("/", "-") + " To " + vm.DateTo.ToShortDateString().Replace("/", "-"), TextDataFormat.UnicodeText);


            //Call print to pdf function
            vm.PrintSaveFlowDocumentCommand.Execute(ReportFD);

        }






        static SQLiteConnection CreateConnection(string pathtodb)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            //sqlite_conn = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            //sqlite_conn = new SQLiteConnection("Data Source=" + pathtodb + "; Version = 3; New = True; Compress = True; ");
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sqlite_conn;
        }




        public void CreateTable(SQLiteConnection conn)
        {

            SQLiteCommand sqlite_cmd;
            string Createsql = "CREATE TABLE SampleTable(Col1 VARCHAR(20), Col2 INT)";
            string Createsql1 = "CREATE TABLE SampleTable1(Col1 VARCHAR(20), Col2 INT)";
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = Createsql;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_cmd.CommandText = Createsql1;
            sqlite_cmd.ExecuteNonQuery();

        }

        public void InsertData(SQLiteConnection conn)
        {
            try
            {
                //MessageBox.Show(vm.AddQCRecord.PartNumber);

                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "INSERT INTO QualityControl (Date, PONumber, PartNumber, Quantity, SerialNo, QCCheckedBy, BookedInBy, TestedBy, Issue, Specify, Status, Comments" +
                    ") VALUES(@date, @ponumber, @partnumber, @quantity, @serialno, @qccheckedby, @bookedinby, @testedby,  @issue, @specify, @status, @comments)";
                sqlite_cmd.CommandType = CommandType.Text;
                //Date must be in this format
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@date", DateTime.Now.ToString("yyyy-MM-dd")));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@ponumber", vm.AddQCRecord.PONumber + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@quantity", vm.AddQCRecord.Quantity + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", vm.AddQCRecord.SerialNo + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@partnumber", vm.AddQCRecord.PartNumber + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@qccheckedby", vm.FullUser + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@bookedinby", vm.AddQCRecord.BookedInBy + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@testedby", vm.AddQCRecord.TestedBy + ""));
                if (SpecifyTXT.Text != string.Empty)
                {
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@issue", vm.AddQCRecord.Specify + ""));
                }
                else
                {
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@issue", vm.AddQCRecord.Issue + ""));
                }
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@specify", vm.AddQCRecord.Specify + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@status", vm.AddQCRecord.Status + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@comments", vm.AddQCRecord.Comments + ""));

                sqlite_cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        public void InsertOperativeData(SQLiteConnection conn)
        {
            try
            {


                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "INSERT INTO Operatives (Operative, IsQC) VALUES(@operative, @isqc)";
                sqlite_cmd.CommandType = CommandType.Text;
                //Date must be in this format
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@operative", vm.Operative + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@isqc", vm.IsQC.ToString() + ""));

                sqlite_cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void DeleteOperativeData(SQLiteConnection conn)
        {
            try
            {


                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "DELETE FROM Operatives WHERE id = @id;";
                sqlite_cmd.CommandType = CommandType.Text;
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@id", vm.OperativeSelected.Id));

                sqlite_cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void DeleteData(SQLiteConnection conn)
        {
            try
            {


                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "DELETE FROM QualityControl WHERE id = @id;";
                sqlite_cmd.CommandType = CommandType.Text;
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@id", vm.UpdateQCRecord.Id));

                sqlite_cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        public void UpdateData(SQLiteConnection conn)
        {

            try
            {


                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                //sqlite_cmd.CommandText = "UPDATE Jobs SET Technician = 'Nick', Status = 'Complete' WHERE id = '1'; ";
                sqlite_cmd.CommandText = "UPDATE QualityControl SET PartNumber = @partnumber, PONumber = @ponumber, Quantity = @quantity, SerialNo = @serialno, QCCheckedBy = @qccheckedby, BookedInBy = @bookedinby, TestedBy = @testedby, Issue = @issue, Specify = @specify, Status = @status, Comments = @comments WHERE id = @id;";

                sqlite_cmd.CommandType = CommandType.Text;
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@id", vm.UpdateQCRecord.Id));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@partnumber", vm.UpdateQCRecord.PartNumber + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@ponumber", vm.UpdateQCRecord.PONumber + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@quantity", vm.UpdateQCRecord.Quantity + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", vm.UpdateQCRecord.SerialNo + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@qccheckedby", vm.UpdateQCRecord.QCCheckedBy));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@bookedinby", vm.UpdateQCRecord.BookedInBy + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@testedby", vm.UpdateQCRecord.TestedBy + ""));
                if (SpecifyTXT.Text != string.Empty)
                {
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@issue", vm.UpdateQCRecord.Specify + ""));
                }
                else
                {
                    sqlite_cmd.Parameters.Add(new SQLiteParameter("@issue", vm.UpdateQCRecord.Issue + ""));
                }

                sqlite_cmd.Parameters.Add(new SQLiteParameter("@specify", vm.UpdateQCRecord.Specify + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@status", vm.UpdateQCRecord.Status + ""));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@comments", vm.UpdateQCRecord.Comments + ""));

                sqlite_cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        public void UpdateStatus(SQLiteConnection conn)
        {

            try
            {

                //Update common shared values Issue = @issue, Specify = @specify, 
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();

                sqlite_cmd.CommandText = "UPDATE QualityControl SET Status = @status WHERE id = @id;";

                sqlite_cmd.CommandType = CommandType.Text;
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@id", vm.UpdateQCRecord.Id));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@status", vm.UpdateQCRecord.Status + ""));
                //sqlite_cmd.Parameters.Add(new SQLiteParameter("@issue", vm.UpdateQCRecord.Issue + ""));
                //sqlite_cmd.Parameters.Add(new SQLiteParameter("@specify", vm.UpdateQCRecord.Specify + ""));



                sqlite_cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public async void ReadOperativeData(SQLiteConnection conn, string sqlquery)
        {

            try
            {



                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    Operatives op = new Operatives();
                    op.Id = sqlite_datareader.GetInt32(0);
                    op.OperativeName = sqlite_datareader.GetString(1);
                    op.isQC = Boolean.Parse(sqlite_datareader.GetString(2));



                    vm.OperativeCollection.Add(op);
                    // string myreader = sqlite_datareader.GetString(0);
                    //  MessageBox.Show(sqlite_datareader.GetString(1).ToString());
                }


                foreach (var op in vm.OperativeCollection)
                {
                    if (op.isQC == true)
                    {
                        vm.QCOperativeCollection.Add(op);
                    }
                    else
                    {

                    }
                }

                await PutTaskDelay(400);

                CalculateTotals();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            // conn.Close();
        }


        public async Task ReadData(SQLiteConnection conn, string sqlquery)
        {

            try
            {

                vm.QCItems = new ObservableCollection<QCRecordsModel>();

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();


                while (sqlite_datareader.Read())
                {


                    QCRecordsModel job = new QCRecordsModel();

                    job.Id = sqlite_datareader.GetInt32(0);
                    job.Date = sqlite_datareader.GetDateTime(1);
                    if (sqlite_datareader.GetString(2) != null)
                        job.PONumber = sqlite_datareader.GetString(2);
                    if (sqlite_datareader.GetString(3) != null)
                        job.PartNumber = sqlite_datareader.GetString(3);
                    if (sqlite_datareader.GetString(4) != null)
                        job.Quantity = sqlite_datareader.GetString(4);
                    if (sqlite_datareader.GetString(5) != null)
                        job.SerialNo = sqlite_datareader.GetString(5);
                    if (sqlite_datareader.GetString(6) != null)
                        job.QCCheckedBy = sqlite_datareader.GetString(6);
                    if (sqlite_datareader.GetString(7) != null)
                        job.BookedInBy = sqlite_datareader.GetString(7);
                    if (sqlite_datareader.GetString(8) != null)
                        job.Issue = sqlite_datareader.GetString(8);
                    if (sqlite_datareader.GetString(9) != null)
                        job.Specify = sqlite_datareader.GetString(9);
                    if (sqlite_datareader.GetString(10) != null)
                        job.Status = sqlite_datareader.GetString(10);
                    if (sqlite_datareader.GetString(11) != null)
                        job.Comments = sqlite_datareader.GetString(11);
                    if (sqlite_datareader.GetString(12) != null)
                        job.TestedBy = sqlite_datareader.GetString(12);




                    App.Current.Dispatcher.Invoke((System.Action)delegate
                    {
                        vm.QCItems.Add(job);
                    });


                }

                //await PutTaskDelay(1000);

                CalculateTotals();



            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }





        //public static void ListViewToCSV(ListView listView, string filePath, bool includeHidden)
        //{
        //    //make header string
        //    StringBuilder result = new StringBuilder();
        //    WriteCSVRow(result, listView.Columns.Count, i => includeHidden || listView.Columns[i].Width > 0, i => listView.Columns[i].Text);

        //    //export data rows
        //    foreach (ListViewItem listItem in listView.Items)
        //        WriteCSVRow(result, listView.Columns.Count, i => includeHidden || listView.Columns[i].Width > 0, i => listItem.SubItems[i].Text);

        //    File.WriteAllText(filePath, result.ToString());
        //}

        //private static void WriteCSVRow(StringBuilder result, int itemsCount, Func<int, bool> isColumnNeeded, Func<int, string> columnValue)
        //{
        //    bool isFirstTime = true;
        //    for (int i = 0; i < itemsCount; i++)
        //    {
        //        if (!isColumnNeeded(i))
        //            continue;

        //        if (!isFirstTime)
        //            result.Append(",");
        //        isFirstTime = false;

        //        result.Append(String.Format("\"{0}\"", columnValue(i)));
        //    }
        //    result.AppendLine();
        //}



        public static void ReadReplace(string csvpath)
        {

            //string text = File.ReadAllText(csvpath);

            //using (StreamReader sr = new StreamReader(csvpath))
            //{
            //    String line;
            //    while ((line = sr.ReadLine()) != null)
            //    {
            //        string[] Column = line.Split(',');

            //        foreach(var itm in Column)
            //        {
            //           // MessageBox.Show(itm.ToString());
            //        }

            //       // string serviceId = Column[4].Trim('"');
            //        //string appendedServiceId = serviceId + "a";

            //       // text = text.Replace(serviceId, appendedServiceId);
            //       // text = text;

            //    }
            //}

            //File.WriteAllText(csvpath, text);




        }



        private void FilterBTN_Click(object sender, RoutedEventArgs e)
        {

            CalculateTotals();

        }

        private async void FilterAllBTN_Click(object sender, RoutedEventArgs e)
        {
            vm.CurrentFilter = "Please Wait...";
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            vm.QCItems = new ObservableCollection<QCRecordsModel>();


            Mouse.OverrideCursor = Cursors.Wait;


            //GET DATE VALUES OVER THE YEAR TO DATE
            //var dateFrom = vm.DateFrom.ToString(DateTime.Now.Year.ToString() + "-01-01");
            var dateFrom = Convert.ToDateTime(DateTime.Now.AddMonths(-12)).ToString("yyyy-MM-dd");
            var dateTo = DateTime.Now.ToString("yyyy-MM-dd");

            //Set the Report Date Pickers to the filter range
            vm.DateFrom = Convert.ToDateTime(dateFrom);
            vm.DateTo = Convert.ToDateTime(dateTo);

            //Query DB
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') ORDER BY  Date DESC"));

            vm.CurrentFilter = "Year " + vm.QCItems.Count.ToString() + " Total Items";
            Mouse.OverrideCursor = null;

            //Close Db
            sqlite_conn.Close();
        }

        private async void FilterCurrentMonthBTN_Click(object sender, RoutedEventArgs e)
        {
            vm.CurrentFilter = "Please Wait...";
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            vm.QCItems = new ObservableCollection<QCRecordsModel>();
            Mouse.OverrideCursor = Cursors.Wait;

            // Filter on Month So Far
            vm.DateFrom = DateTime.Parse(@"1/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + "");
            vm.DateTo = DateTime.Now;


            //Query DB
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + vm.DateFrom.ToString("yyyy-MM-dd") + "') AND date('" + vm.DateTo.ToString("yyyy-MM-dd") + "') ORDER BY  Date DESC"));


            vm.CurrentFilter = "This Month " + vm.QCItems.Count.ToString() + " Total Items";
            Mouse.OverrideCursor = null;

            //Close Db
            sqlite_conn.Close();
        }

        private async void FilterMondayBTN_Click(object sender, RoutedEventArgs e)
        {
            vm.CurrentFilter = "Please Wait...";
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            Mouse.OverrideCursor = Cursors.Wait;
            vm.QCItems = new ObservableCollection<QCRecordsModel>();


            vm.DateFrom = DateTimeExtensions.StartOfWeek(DateTime.Now, DayOfWeek.Monday);
            vm.DateTo = DateTime.Now;

            //Query DB
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + vm.DateFrom.ToString("yyyy-MM-dd") + "') AND date('" + vm.DateTo.ToString("yyyy-MM-dd") + "') ORDER BY  Date DESC"));


            vm.CurrentFilter = "This Week " + vm.QCItems.Count.ToString() + " Total Items";

            Mouse.OverrideCursor = null;
            //Close Db
            sqlite_conn.Close();
        }

        private async void FilterOnHoldBTN_Click(object sender, RoutedEventArgs e)
        {
            vm.CurrentFilter = "Please Wait...";
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            Mouse.OverrideCursor = Cursors.Wait;
            vm.QCItems = new ObservableCollection<QCRecordsModel>();

            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            //var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateFrom = Convert.ToDateTime(DateTime.Now.AddMonths(FilterYearSpan)).ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //Query DB
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + vm.DateFrom.ToString("yyyy-MM-dd") + "') AND date('" + vm.DateTo.ToString("yyyy-MM-dd") + "') AND Status = 'On Hold' ORDER BY  Date DESC"));

            vm.CurrentFilter = "On Hold Jobs " + vm.QCItems.Count.ToString() + " Total Items";

            Mouse.OverrideCursor = null;

            //Close Db
            sqlite_conn.Close();
        }


        private void DPFrom_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            vm.CurrentFilter = "";
        }

        private void DPTo_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            vm.CurrentFilter = "";
        }

        private void SearchBTN_Click(object sender, RoutedEventArgs e)
        {
            vm.QCItems = new ObservableCollection<QCRecordsModel>();

            //Run Query Async
            SearchBySalesOrderNo();
        }

        private void SearchTXT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                vm.QCItems = new ObservableCollection<QCRecordsModel>();

                //Run Query Async
                SearchBySalesOrderNo();

            }
        }



        private async void SearchBySalesOrderNo()
        {
            vm.CurrentFilter = "Please Wait...";

            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            Mouse.OverrideCursor = Cursors.Wait;
            vm.QCItems = new ObservableCollection<QCRecordsModel>();

            //Query DB Async
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE PONumber LIKE '" + vm.QCSearch + "%' OR PartNumber LIKE '" + vm.QCSearch + "%' OR SerialNo LIKE'" + vm.QCSearch + "%'"));


            vm.CurrentFilter = "";
            Mouse.OverrideCursor = null;
            //Close Db
            sqlite_conn.Close();
        }

        private void OperativeCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            Operatives typeItem = (Operatives)OperativeCB.SelectedItem;
            if (typeItem != null)
                vm.OperativeSelected = typeItem;

        }

        private void AddBookedInCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            Operatives typeItem = (Operatives)AddBookedInCB.SelectedItem;
            if (typeItem != null)
                vm.AddQCRecord.BookedInBy = typeItem.OperativeName;

        }

        private async void NeedToUnbookBTN_Click(object sender, RoutedEventArgs e)
        {
            vm.CurrentFilter = "Please Wait...";
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            Mouse.OverrideCursor = Cursors.Wait;
            vm.QCItems = new ObservableCollection<QCRecordsModel>();

            //Set Dates
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //Query DB
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE Status = 'To Unbook' ORDER BY  Date DESC"));

            //Set property
            vm.CurrentFilter = "To Unbook " + vm.QCItems.Count.ToString() + " Total Items";
            Mouse.OverrideCursor = null;

            //Close Db
            sqlite_conn.Close();
        }

        private async void WithITADBTN_Click(object sender, RoutedEventArgs e)
        {
            vm.CurrentFilter = "Please Wait...";
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            Mouse.OverrideCursor = Cursors.Wait;
            vm.QCItems = new ObservableCollection<QCRecordsModel>();

            //Set Dates, Last 2 Years as start date
            //var dateFrom = Convert.ToDateTime("01/01/" + (DateTime.Now.Year - 1)).ToString("yyyy-MM-dd");
            var dateFrom = Convert.ToDateTime(DateTime.Now.AddMonths(FilterYearSpan)).ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //Query DB
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') AND Status = 'With ITAD' ORDER BY  Date DESC"));

            //Set property
            vm.CurrentFilter = "With ITAD " + vm.QCItems.Count.ToString() + " Total Items";

            Mouse.OverrideCursor = null;

            //Close Db
            sqlite_conn.Close();
        }

        private async void WithGIBTN_Click(object sender, RoutedEventArgs e)
        {
            vm.CurrentFilter = "Please Wait...";
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            Mouse.OverrideCursor = Cursors.Wait;
            vm.QCItems = new ObservableCollection<QCRecordsModel>();

            //Set Dates, Last 2 Years as start date
            //var dateFrom = Convert.ToDateTime("01/01/" + (DateTime.Now.Year - 1)).ToString("yyyy-MM-dd");
            var dateFrom = Convert.ToDateTime(DateTime.Now.AddMonths(FilterYearSpan)).ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //Query DB
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') AND Status = 'With GI' ORDER BY  Date DESC"));

            //Set property
            vm.CurrentFilter = "With GI " + vm.QCItems.Count.ToString() + " Total Items";

            Mouse.OverrideCursor = null;

            //Close Db
            sqlite_conn.Close();
        }


        private async void FilterNWithGIMailBTN_Click(object sender, RoutedEventArgs e)
        {
            vm.CurrentFilter = "Please Wait...";
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            Mouse.OverrideCursor = Cursors.Wait;
            vm.QCItems = new ObservableCollection<QCRecordsModel>();

            //Set Dates, Last 2 Years as start date
            //var dateFrom = Convert.ToDateTime("01/01/" + (DateTime.Now.Year - 1)).ToString("yyyy-MM-dd");
            var dateFrom = Convert.ToDateTime(DateTime.Now.AddMonths(FilterYearSpan)).ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //Query DB
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') AND Status = 'With GI Mail' ORDER BY  Date DESC"));

            //Set property
            vm.CurrentFilter = "With GI Mail " + vm.QCItems.Count.ToString() + " Total Items";

            Mouse.OverrideCursor = null;

            //Close Db
            sqlite_conn.Close();
        }

        private async void FilterNWithGIServersBTN_Click(object sender, RoutedEventArgs e)
        {
            vm.CurrentFilter = "Please Wait...";
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            Mouse.OverrideCursor = Cursors.Wait;
            vm.QCItems = new ObservableCollection<QCRecordsModel>();

            switch (OneYearRB)
            {
                default:
                    break;
            }

            //Set Dates, Last 2 Years as start date
            //var dateFrom = Convert.ToDateTime("01/01/" + (DateTime.Now.Year - 1)).ToString("yyyy-MM-dd");
            var dateFrom = Convert.ToDateTime(DateTime.Now.AddMonths(FilterYearSpan)).ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //Query DB
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') AND Status = 'With GI Servers' ORDER BY  Date DESC"));

            //Set property
            vm.CurrentFilter = "With GI Servers " + vm.QCItems.Count.ToString() + " Total Items";

            Mouse.OverrideCursor = null;

            //Close Db
            sqlite_conn.Close();
        }

        private async void WithRepairBTN_Click(object sender, RoutedEventArgs e)
        {
            vm.CurrentFilter = "Please Wait...";
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            Mouse.OverrideCursor = Cursors.Wait;
            vm.QCItems = new ObservableCollection<QCRecordsModel>();

            //Set Dates, Last 2 Years as start date
            //var dateFrom = Convert.ToDateTime("01/01/" + (DateTime.Now.Year - 1)).ToString("yyyy-MM-dd");
            var dateFrom = Convert.ToDateTime(DateTime.Now.AddMonths(FilterYearSpan)).ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //Query DB
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') AND Status = 'With Repair' ORDER BY  Date DESC"));

            //Set property
            vm.CurrentFilter = "With Repair " + vm.QCItems.Count.ToString() + " Total Items";

            Mouse.OverrideCursor = null;

            //Close Db
            sqlite_conn.Close();
        }

        private async void FilterWithRMABTN_Click(object sender, RoutedEventArgs e)
        {
            vm.CurrentFilter = "Please Wait...";
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);
            Mouse.OverrideCursor = Cursors.Wait;
            vm.QCItems = new ObservableCollection<QCRecordsModel>();

            //Set Dates, Last 2 Years as start date
            //var dateFrom = Convert.ToDateTime("01/01/" + (DateTime.Now.Year - 1)).ToString("yyyy-MM-dd");
            var dateFrom = Convert.ToDateTime(DateTime.Now.AddMonths(FilterYearSpan)).ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //Query DB
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') AND Status = 'With RMA' ORDER BY  Date DESC"));

            //Set property
            vm.CurrentFilter = "With RMA " + vm.QCItems.Count.ToString() + " Total Items";

            Mouse.OverrideCursor = null;

            //Close Db
            sqlite_conn.Close();
        }

        private async void FilterWithDriveTestBTN_Click(object sender, RoutedEventArgs e)
        {
            vm.CurrentFilter = "Please Wait...";
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(cs);

            Mouse.OverrideCursor = Cursors.Wait;
            vm.QCItems = new ObservableCollection<QCRecordsModel>();

            //Set Dates, Last 2 Years as start date
            //var dateFrom = Convert.ToDateTime("01/01/" + (DateTime.Now.Year - 1)).ToString("yyyy-MM-dd");
            var dateFrom = Convert.ToDateTime(DateTime.Now.AddMonths(FilterYearSpan)).ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.ToString("yyyy-MM-dd");

            //Query DB
            await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM QualityControl WHERE date(Date) BETWEEN date('" + dateFrom + "') AND date('" + dateTo + "') AND Status = 'With Drive Test' ORDER BY  Date DESC"));

            //Set property
            vm.CurrentFilter = "With Drive Test " + vm.QCItems.Count.ToString() + " Total Items";

            Mouse.OverrideCursor = null;

            //Close Db
            sqlite_conn.Close();
        }

        private void ExportCSVBTN_Click(object sender, RoutedEventArgs e)
        {
            List<QCRecordsModel> qcList = new List<QCRecordsModel>();

            if (QCLV.Items.Count > 0)
            {
                foreach (QCRecordsModel ns in QCLV.Items)
                {
                    //Add to local list
                    qcList.Add(ns);
                }

            }
            //Write to CSV
            StaticFunctions.CreateQCCSV(qcList);

        }


        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

        private async void ScanQR_TextChanged(object sender, TextChangedEventArgs e)
        {

            try
            {

                //Check if string contains more than 5 commas, if so process
                if (ScanQR.Text.Count(a => a == ',') == 5)
                {

                    //string result = ScanQR.Text.Substring(0, ScanQR.Text.IndexOf("-")).Trim();
                    if (ScanQR.Text.Length > 0 && ScanQR.Text.Contains(","))
                    {
                        //Add comma to start to split string correctly
                        string qrString = "," + ScanQR.Text;
                        //Split the QR Result to array
                        string[] splitInfo = qrString.Split(',');


                        await PutTaskDelay(1100);

                        //MessageBox.Show(splitInfo.Length.ToString());

                        if (string.IsNullOrEmpty(splitInfo[5]))
                        {

                            //SERVER CHECKLIST QR
                            //vm.AddQCRecord.BookedInBy = splitInfo[2];
                            //populate the combobox
                            //AddBookedInCB.Text = vm.AddQCRecord.BookedInBy;
                            vm.AddQCRecord.Quantity = splitInfo[3];
                            //Order Ref
                            vm.AddQCRecord.PONumber = splitInfo[4];
                            //Serial Number
                            //if (!string.IsNullOrEmpty(splitInfo[6]))
                            vm.AddQCRecord.SerialNo = splitInfo[6];

                        }
                        else
                        {
                            //LABEL QR
                            vm.AddQCRecord.BookedInBy = splitInfo[2];
                            //populate the combobox
                            AddBookedInCB.Text = splitInfo[2];
                            //AddTestedByCB.Text = splitInfo[2];
                            vm.AddQCRecord.Quantity = splitInfo[3];
                            //if (!string.IsNullOrEmpty(splitInfo[4]))
                            vm.AddQCRecord.PONumber = splitInfo[4];
                            if (!string.IsNullOrEmpty(splitInfo[5]))
                            {
                                vm.AddQCRecord.PartNumber = splitInfo[5];
                                //PartCode load Model Number into search section
                                //PartCodeSearch.autoTextBox.Focus();
                                PartCodeSearch.autoTextBox.Text = splitInfo[5].Replace("PowerEdge", "").Replace("ProLiant", "").Replace("Lenovo", "").Replace("System", "").Replace("Cisco", "").Trim();
                                PartCodeSearch.autoListPopup.IsOpen = false;
                            }

                            //Serial Number
                            //if (!string.IsNullOrEmpty(splitInfo[6]))
                            vm.AddQCRecord.SerialNo = splitInfo[6];
                        }
                    }

                    //Clear and set focus to textbox
                    ScanQR.Text = "";
                    ScanQR.Focus();

                }


            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }

        private void ThreeMonthRB_Checked(object sender, RoutedEventArgs e)
        {
            if (ThreeMonthRB.IsChecked == true)
            {
                FilterYearSpan = -3;
            }
        }

        private void SixMonthRB_Checked(object sender, RoutedEventArgs e)
        {
            if (SixMonthRB.IsChecked == true)
            {
                FilterYearSpan = -6;
            }
        }


        private void OneYearRB_Checked(object sender, RoutedEventArgs e)
        {
            if (OneYearRB.IsChecked == true)
            {
                FilterYearSpan = -12;
            }
        }

        private void TwoYearRB_Checked(object sender, RoutedEventArgs e)
        {
            if (TwoYearRB.IsChecked == true)
            {
                FilterYearSpan = -24;
            }
        }

        private void ThreeYearRB_Checked(object sender, RoutedEventArgs e)
        {
            if (ThreeYearRB.IsChecked == true)
            {
                FilterYearSpan = -36;
            }
        }

        private void FaultCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ////Change status of selected items.
            //if (QCLV != null)
            //    foreach (QCRecordsModel selectedItem in QCLV.SelectedItems)
            //    {

            //        //Update ListView Selection
            //        selectedItem.Issue = vm.UpdateQCRecord.Issue;

            //    }
        }

        private void StatusCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Change status of selected items.
            if (QCLV != null)
                foreach (QCRecordsModel selectedItem in QCLV.SelectedItems)
                {

                    //Update ListView Selection
                    selectedItem.Status = vm.UpdateQCRecord.Status;
                }
        }



        private void SpecifyIssueTXT_TextChanged(object sender, TextChangedEventArgs e)
        {
            ////Change status of selected items.
            //if (QCLV != null)
            //    foreach (QCRecordsModel selectedItem in QCLV.SelectedItems)
            //    {

            //        //Update ListView Selection
            //        selectedItem.Specify = vm.UpdateQCRecord.Specify;
            //    }
        }

        private void AddTestedByCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            Operatives typeItem = (Operatives)AddTestedByCB.SelectedItem;
            if (typeItem != null)
                vm.AddQCRecord.TestedBy = typeItem.OperativeName;
        }
    }
}
