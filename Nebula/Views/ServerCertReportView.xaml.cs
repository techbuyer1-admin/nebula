﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.Models;
using Nebula.Paginators;
using Nebula.ViewModels;
using QRCoder;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for ServerCertReportView.xaml
    /// </summary>
    public partial class ServerCertReportView : UserControl
    {

        MainViewModel vm;

        public ServerCertReportView()
        {
            InitializeComponent();
        }

        public ServerCertReportView(MainViewModel passedviewmodel, bool Independent)
        {
            InitializeComponent();

            vm = passedviewmodel;

            //Hide as not required by GOT
            PrintBTNS1.Visibility = Visibility.Collapsed;


        }

        public ServerCertReportView(MainViewModel passedviewmodel)
        {
            InitializeComponent();

            vm = passedviewmodel;

       
            // Fill property with blank bitmap image
            vm.ChecklistQRCode = new BitmapImage();

            //Reset Message
            vm.ChecklistMessage = "";

            //Show Save button, depending on path
            SaveDocsBTN.Visibility = Visibility.Visible;
            //Hide as not required by GOT
            PrintBTNS1.Visibility = Visibility.Collapsed;
            //Hide out of region checkbox
            OutOfRegionVizCKB.Visibility = Visibility.Hidden;


            //Check department user and which PO SO Textbox to display
            DepartmentUserCheck();

            //DELL
            if (vm.DellServerS1 != null)
            {
                // ChassisNoTXTBS1.Text = vm.DellServerS1.ServiceTag;
                vm.ChassisSerialNo = vm.DellServerS1.ServiceTag;
                vm.ModelNo = vm.DellServerS1.Model;
                vm.ProcessedDate = DateTime.Now.ToShortDateString();
                //BoardTXTB.Text = vm.DellServerS1.BoardPartNumber;
                //Add First available controller to textbox
                if (!string.IsNullOrEmpty(vm.DellServerS1.ControllerType))
                {
                    //ControllerTXTB.Text = vm.DellServerS1.ControllerType;
                }
                else if (!string.IsNullOrEmpty(vm.DellServerS1.ControllerType2))
                {
                    //ControllerTXTB.Text = vm.DellServerS1.ControllerType2;
                }
                else
                {
                    //ControllerTXTB.Text = vm.DellServerS1.ControllerType3;
                }



           
            
                //Check license information is present
                if (string.IsNullOrEmpty(vm.DellServerS1.iDRACLicense))
                {
                    //Check if empty, show warning if true
                    vm.ChecklistWarningMessage = "Dell License information missing! Please run Get Server Info again.";
                }
            }

            //HP
            if (vm.GenGenericServerInfo1 != null || vm.GenGenericServerInfo1 != null || vm.GenGenericServerInfo1 != null)
            {
                //Show out of region checkbox
                OutOfRegionVizCKB.Visibility = Visibility.Visible;
          
                if (vm.GenGenericServerInfo1 != null)
                    vm.ChassisSerialNo = vm.GenGenericServerInfo1.ChassisSerial;
                if (vm.GenGenericServerInfo1 != null)
                    vm.ModelNo = vm.GenGenericServerInfo1.Model;
            
                vm.ProcessedDate = DateTime.Now.ToShortDateString();

            }
            //Lenovo
            if (vm.LenovoServerS1 != null)
            {
                // ChassisNoTXTBS1.Text = vm.DellServerS1.ServiceTag;
                vm.ChassisSerialNo = vm.LenovoServerS1.SystemSerialNumber;
                vm.ModelNo = vm.LenovoServerS1.Model;
                vm.ProcessedDate = DateTime.Now.ToShortDateString();
                //if (!string.IsNullOrEmpty(vm.LenovoServerS1.BoardSerialNumber))
                //    BoardTXTB.Text = vm.LenovoServerS1.BoardSerialNumber;
                //if (!string.IsNullOrEmpty(vm.LenovoServerS1.PartNumber))
                //    ControllerTXTB.Text = vm.LenovoServerS1.PartNumber;
            }
            //Cisco
            //if (vm.CiscoServerS1 != null)
            //{
            //    // ChassisNoTXTBS1.Text = vm.DellServerS1.ServiceTag;
            //    vm.ChassisSerialNo = vm.CiscoServerS1.SystemSerialNumber;
            //    vm.ModelNo = vm.CiscoServerS1.Model;
            //    vm.ProcessedDate = DateTime.Now.ToShortDateString();
            //}

            //if ITAD Switch ref info required
            if (StaticFunctions.Department == "ITAD")
            {
                PurchaseOrderTB.Text = "Enter LOT - Asset Number";
                PORefTB.Text = "(Please include Lot - Asset prefix)";
            }
            else
            {
                PurchaseOrderTB.Text = "Enter PO Number";
                PORefTB.Text = "(Please include PO prefix)";
            }



        }


        private void PurchaseOrderTBX_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (PurchaseOrderTBX.Text == "")
            {
                PrintBTNS1.IsEnabled = false;
            }
            else
            {
                PrintBTNS1.IsEnabled = true;
            }

            vm.POSONumber = PurchaseOrderTBX.Text;
            vm.PONumber = PurchaseOrderTBX.Text;

                    
        }

        private void SalesOrderTBX_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SalesOrderTBX.Text == "")
            {
                PrintBTNS1.IsEnabled = false;
            }
            else
            {
                PrintBTNS1.IsEnabled = true;
            }

            vm.POSONumber = SalesOrderTBX.Text;
            vm.SONumber = SalesOrderTBX.Text;

        }

     

        private void DepartmentUserCheck()
        {
            if (StaticFunctions.Department.Contains("GoodsIn") || StaticFunctions.Department.Contains("TestDept") || StaticFunctions.Department.Contains("ITAD"))
            {
                //Show PO Textbox
                PONumberSP.Visibility = Visibility.Visible;
                SONumberSP.Visibility = Visibility.Collapsed;
                //Show Hide Save & Print Buttons SHOW BOTH
                SaveDocsBTN.Visibility = Visibility.Visible;
                PrintBTNS1.Visibility = Visibility.Visible;
                //Show Brand Toggle Switch
                //BrandChecklistTS.Visibility = Visibility.Hidden;
                //Goods In user
                vm.GoodsInRep = vm.CurrentUserInitials;
            }
            else if (StaticFunctions.Department.Contains("GoodsOutTech"))
            {
                //Show sO Textbox
                SONumberSP.Visibility = Visibility.Visible;
                PONumberSP.Visibility = Visibility.Collapsed;

                //Show Hide Save & Print Buttons
                SaveDocsBTN.Visibility = Visibility.Visible;
                

                //Goods Out user
                vm.GoodsOutRep = vm.CurrentUserInitials;
            }
        }


        private void PrintBTNS1_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                //Clear Messsage
                vm.ChecklistMessage = "";

                //Check license information is present
                if (vm.DellServerS1 != null)
                    if (vm.DellServerS1.iDRACLicense == string.Empty || vm.DellServerS1.iDRACLicense == "")
                    {
                        //Clear Warning Messsage
                        vm.ChecklistWarningMessage = "";
                    }


                if (vm.ChassisSerialNo != "" && vm.PONumber != "" || vm.SONumber != "")
                {

                    //// Check the tickboxes have been ticked
                    //if (GIBOX1.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2 || GOTBOX1.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2)
                    //{

                        //*** Checklist QR CODE ***
                        //Call Embed QR Saves & extracts QR Code to File. This is required for the cloning of the flowdocument as it requires a full path
                        vm.ChecklistQRCode = vm.EmbedChecklistQR(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.ChassisSerialNo.Trim() + ".png", vm.ChecklistQRCode);




                        ////Set the serial number to text on the clipboard  Use control v
                        if (!string.IsNullOrEmpty(ChassisNoTXTB.Text))
                            Clipboard.SetText(ChassisNoTXTB.Text.Trim() + "_Checklist", TextDataFormat.UnicodeText);



                        //OLD METHOD

                        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


                        ////Get the text from the clipboard
                        //Clipboard.GetText(TextDataFormat.UnicodeText);

                      //***  dpw.SaveDocPDF(ServCheckLstFDoc);

                        //Clear values
                        //vm.ChecklistQRCode = null;
                        // bi.UriSource = new Uri("", UriKind.RelativeOrAbsolute); ;

                        //vm.ChassisSerialNo = "";
                        //vm.PONumber = "";
                        //vm.SONumber = "";

                        //Clear GI OR GOT Checkboxes
                        //ClearCheckBoxes();
                    }
                    else
                    {
                        vm.ChecklistMessage = "Please select all the Check Boxes!";
                    }


                //}


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }


        private void OutOfRegionVizCKB_Checked(object sender, RoutedEventArgs e)
        {

            //hide Out Of Region TextBlock
            if (vm.GenGenericServerInfo1 != null)
                vm.GenGenericServerInfo1.OutOfRegionViz = Visibility.Hidden;
            //if (vm.GenGenericServerInfo1 != null)
            //    vm.GenGenericServerInfo1.OutOfRegionViz = Visibility.Hidden;
            //if (vm.GenGenericServerInfo1 != null)
            //    vm.GenGenericServerInfo1.OutOfRegionViz = Visibility.Hidden;


        }

        private void OutOfRegionVizCKB_Unchecked(object sender, RoutedEventArgs e)
        {

            //Show Out Of Region TextBlock
            if (vm.GenGenericServerInfo1 != null)
                vm.GenGenericServerInfo1.OutOfRegionViz = Visibility.Visible;
            //if (vm.GenGenericServerInfo1 != null)
            //    vm.GenGenericServerInfo1.OutOfRegionViz = Visibility.Visible;
            //if (vm.GenGenericServerInfo1 != null)
            //    vm.GenGenericServerInfo1.OutOfRegionViz = Visibility.Visible;

        }
    }
}
