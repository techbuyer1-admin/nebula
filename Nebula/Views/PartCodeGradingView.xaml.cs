﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using Nebula.Commands;
using Nebula.Models;
using Nebula.ViewModels;


namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for PartCodeGradingView.xaml
    /// </summary>
    public partial class PartCodeGradingView : UserControl
    {

        //Set String to DB Path
        public string cs = "";
        // Sql lite connection
        SQLiteConnection sqlite_conn;

        //declare viewmodel
        MainViewModel vm;     //

        public PartCodeGradingView()
        {
            InitializeComponent();
        }

        public PartCodeGradingView(MainViewModel PassedViewModel, string GradingType)
        {
            InitializeComponent();

            vm = PassedViewModel;


            //Point DB to Test Databases
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\PartCodeGeneratorDB.db";
            }
            else
            {
                //Set connection string to Live DB
                //Use DFS Hosted DB for multiple site access
                cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/PartCodeGeneratorDB.db";
            }

            //ToolTips

            //Create collection
            ObservableCollection<AestheticTooltip> tooltipsOC = new ObservableCollection<AestheticTooltip>();

            //Add tooltips to collection
            tooltipsOC.Add(new AestheticTooltip("Factory Sealed", "A brand new product which is factory sealed in original packaging."));
            tooltipsOC.Add(new AestheticTooltip("Open Retail", "A brand new product in original packaging that has had outer packaging opened."));
            tooltipsOC.Add(new AestheticTooltip("Grade A Refurbished", "A refurbished product that looks and operates 'as good as' new, without original packaging."));
            tooltipsOC.Add(new AestheticTooltip("Grade B Refubished", "A refurbished product that operates as good as new, with minor aesthetic markings. May have\n been re-vinyled and/or had a new keyboard."));
            tooltipsOC.Add(new AestheticTooltip("Grade C1 Refubished", "A refurbished product that operates as good as new, with aesthetic markings larger on casing\n or screen than Grade B. May have been re-vinyled and/or had a new keyboard."));
            tooltipsOC.Add(new AestheticTooltip("Grade C2 Refubished", "A refurbished product that operates as good as new, with aesthetic markings larger on casing\n or screen than Grade C1, and/or have further signs of wear on the palm rest, keyboard and trackpad.\n May have been re-vinyled and/or had a new keyboard."));
            tooltipsOC.Add(new AestheticTooltip("Grade D Refubished", "A tested product that is not 100% functional and/or below C2 Grade aesthetically. May also\n have a faulty, low % or no battery."));
            //tooltipsOC.Add(new AestheticTooltip(App.Current.FindResource("PCGradeTTipT1").ToString(), App.Current.FindResource("PCGradeTTip1").ToString()));
            //tooltipsOC.Add(new AestheticTooltip(App.Current.FindResource("PCGradeTTipT2").ToString(), App.Current.FindResource("PCGradeTTip2").ToString()));
            //tooltipsOC.Add(new AestheticTooltip(App.Current.FindResource("PCGradeTTipT3").ToString(), App.Current.FindResource("PCGradeTTip3").ToString()));
            //tooltipsOC.Add(new AestheticTooltip(App.Current.FindResource("PCGradeTTipT4").ToString(), App.Current.FindResource("PCGradeTTip4").ToString()));
            //tooltipsOC.Add(new AestheticTooltip(App.Current.FindResource("PCGradeTTipT5").ToString(), App.Current.FindResource("PCGradeTTip5").ToString()));
            //tooltipsOC.Add(new AestheticTooltip(App.Current.FindResource("PCGradeTTipT6").ToString(), App.Current.FindResource("PCGradeTTip6").ToString()));
            //tooltipsOC.Add(new AestheticTooltip(App.Current.FindResource("PCGradeTTipT7").ToString(), App.Current.FindResource("PCGradeTTip7").ToString()));

            //create listview and set required colums and add gridview
            ListView lv = new ListView();

            double fontSize = 12;
            double listWidth = 740;
            //Create style for centered items
            ListViewItem lvi = new ListViewItem();
            Style lvstyle = new Style(typeof(ListViewItem));
            lvstyle.Setters.Add(new Setter(ListViewItem.HorizontalContentAlignmentProperty, HorizontalAlignment.Left));
            lvstyle.Setters.Add(new Setter(ListViewItem.FontSizeProperty, fontSize));
            lvstyle.Setters.Add(new Setter(ListViewItem.ForegroundProperty, Brushes.White));
            lvstyle.Setters.Add(new Setter(ListViewItem.BackgroundProperty, Brushes.DimGray));
            lvstyle.Setters.Add(new Setter(ListViewItem.MaxWidthProperty, listWidth));
            lv.ItemContainerStyle = lvstyle;
            DataTemplate dt = new DataTemplate();
            TextBlock tb1 = new TextBlock();
            TextBlock tb2 = new TextBlock();
            //dt.
            //lv.ItemTemplate = dt;

            //Set Grid Column Property
            lv.SetValue(Grid.ColumnProperty, 2);
            //Grid view columns
            GridView gv = new GridView();
            GridViewColumn gvc1 = new GridViewColumn();
            gvc1.DisplayMemberBinding = new Binding("Grading");
            gvc1.Header = App.Current.FindResource("PCGradeTTipHeader1").ToString();
            gv.Columns.Add(gvc1);

            GridViewColumn gvc2 = new GridViewColumn();
            gvc2.DisplayMemberBinding = new Binding("Description");
            gvc2.Header = App.Current.FindResource("PCGradeTTipHeader2").ToString();



            //DataTemplate template = new DataTemplate();
            //FrameworkElementFactory factory = new FrameworkElementFactory(typeof(TextBlock));
            //factory.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Right);
            //factory.SetBinding(TextBlock.TextProperty, new Binding("Description"));
            //template.VisualTree = factory;
            //gvc2.SetValue(TextBlock.TextProperty, TextWrapping.Wrap);


            gv.Columns.Add(gvc2);
            //Assign gridview to listview
            lv.View = gv;
            //Assign Collection to itemsource
            lv.ItemsSource = tooltipsOC;

            //Functionality options
            GFuncScreenCB.ToolTip = lv;
            GFuncKBCB.ToolTip = lv;
            GFuncTrackpadCB.ToolTip = lv;
            GFuncWebcamCB.ToolTip = lv;
            GradingFuncWifiCB.ToolTip = lv;
            GradingFuncSpeakersCB.ToolTip = lv;
            GFuncMicrophoneCB.ToolTip = lv;
            GFuncPortsCB.ToolTip = lv;
            GAFuncBatteryCB.ToolTip = lv;

            //Add tooltips to comboboxes
            GAestheticOuterCasingLidCB.ToolTip = lv;
            GAestheticOuterCasingPanelCB.ToolTip = lv;
            GAestheticFrontBezelCB.ToolTip = lv;
            GAestheticScreenCB.ToolTip = lv;
            GradingAestheticKBCB.ToolTip = lv;
            GradingAestheticTrackpadCB.ToolTip = lv;
            GAestheticMicrophoneCB.ToolTip = lv;
            GAestheticPortsCB.ToolTip = lv;

            //Show hide options on load
            switch (GradingType)
            {


                case "AllInOne":
                    FuncKeyboardTile.Visibility = Visibility.Collapsed;
                    FuncTrackpadTile.Visibility = Visibility.Collapsed;
                    FuncBatteryTile.Visibility = Visibility.Collapsed;
                    AesthTrackpadTile.Visibility = Visibility.Collapsed;
                    AesthPalmRestTile.Visibility = Visibility.Collapsed;
                    AesthKeyboardTile.Visibility = Visibility.Collapsed;

                    break;
                case "Apple":

                    break;
                case "Desktops":
                    FuncScreenTile.Visibility = Visibility.Collapsed;
                    FuncKeyboardTile.Visibility = Visibility.Collapsed;
                    FuncTrackpadTile.Visibility = Visibility.Collapsed;
                    FuncBatteryTile.Visibility = Visibility.Collapsed;
                    AesthTrackpadTile.Visibility = Visibility.Collapsed;
                    AesthPalmRestTile.Visibility = Visibility.Collapsed;
                    AesthKeyboardTile.Visibility = Visibility.Collapsed;
                    AesthScreenTile.Visibility = Visibility.Collapsed;

                    break;
                case "Laptops":
                    AesthFrontBezelTile.Visibility = Visibility.Collapsed;
                    break;
            }


        }

        private void GFuncScreenCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }

        }

        private void GFuncKBCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GFuncTrackpadCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GFuncWebcamCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GradingFuncWifiCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GradingFuncSpeakersCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GFuncMicrophoneCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GFuncPortsCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GAFuncBatteryCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GAestheticOuterCasingLidCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GAestheticOuterCasingPanelCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GAestheticFrontBezelCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GAestheticScreenCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GradingAestheticKBCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GradingAestheticTrackpadCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GAestheticMicrophoneCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void GAestheticPortsCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (vm != null)
            {
                //Grade item based off selections
                vm.GradePart();
            }
        }

        private void LaunchGuideBTN_Click(object sender, RoutedEventArgs e)
        {
            ProcessPiper pp = new ProcessPiper();

            pp.StartExeProcess(@"" + @"\\pinnacle.local\tech_resources\Nebula\Documentation\Client Equipment Grading.xlsx", "", "");
        }

        private void AllNewFSBTN_Click(object sender, RoutedEventArgs e)
        {

            //Functionality
            vm.GradingFuncScreen = "New FS";
            vm.GradingFuncKeyboard = "New FS";
            vm.GradingFuncTrackpad = "New FS";
            vm.GradingFuncWebcam = "New FS";
            vm.GradingFuncWifi = "New FS";
            vm.GradingFuncSpeakers = "New FS";
            vm.GradingFuncMicrophone = "New FS";
            vm.GradingFuncPorts = "New FS";
            vm.GradingBattery = "80%+";
            //Aesthetics
            vm.GradingAestOuterCasingLid = "New FS";
            vm.GradingAestOuterCasingBase = "New FS";
            vm.GradingAestScreen = "New FS";
            vm.GradingAestPalmRest = "New FS";
            vm.GradingAestPorts = "New FS";
            vm.GradingAestTrackpad = "New FS";
            vm.GradingAestKeyboard = "New FS";
            vm.GradingAestFrontBezel = "New FS";
            vm.GradingGrade = "FS";


        }

        private void AllNewORBTN_Click(object sender, RoutedEventArgs e)
        {
            //Functionality
            vm.GradingFuncScreen = "New OR";
            vm.GradingFuncKeyboard = "New OR";
            vm.GradingFuncTrackpad = "New OR";
            vm.GradingFuncWebcam = "New OR";
            vm.GradingFuncWifi = "New OR";
            vm.GradingFuncSpeakers = "New OR";
            vm.GradingFuncMicrophone = "New OR";
            vm.GradingFuncPorts = "New OR";
            vm.GradingBattery = "80%+";
            //Aesthetics
            vm.GradingAestOuterCasingLid = "New OR";
            vm.GradingAestOuterCasingBase = "New OR";
            vm.GradingAestScreen = "New OR";
            vm.GradingAestPalmRest = "New OR";
            vm.GradingAestPorts = "New OR";
            vm.GradingAestTrackpad = "New OR";
            vm.GradingAestKeyboard = "New OR";
            vm.GradingAestFrontBezel = "New OR";
            vm.GradingGrade = "OR";

        }

        private void AllRefABTN_Click(object sender, RoutedEventArgs e)
        {
            //Functionality
            vm.GradingFuncScreen = "A";
            vm.GradingFuncKeyboard = "A";
            vm.GradingFuncTrackpad = "A";
            vm.GradingFuncWebcam = "A";
            vm.GradingFuncWifi = "A";
            vm.GradingFuncSpeakers = "A";
            vm.GradingFuncMicrophone = "A";
            vm.GradingFuncPorts = "A";
            vm.GradingBattery = "80%+";
            //Aesthetics
            vm.GradingAestOuterCasingLid = "A";
            vm.GradingAestOuterCasingBase = "A";
            vm.GradingAestScreen = "A";
            vm.GradingAestPalmRest = "A";
            vm.GradingAestPorts = "A";
            vm.GradingAestTrackpad = "A";
            vm.GradingAestKeyboard = "A";
            vm.GradingAestFrontBezel = "A";
            vm.GradingGrade = "A";
        }

        private void AllRefBBTN_Click(object sender, RoutedEventArgs e)
        {
            //Functionality
            vm.GradingFuncScreen = "B";
            vm.GradingFuncKeyboard = "B";
            vm.GradingFuncTrackpad = "B";
            vm.GradingFuncWebcam = "B";
            vm.GradingFuncWifi = "B";
            vm.GradingFuncSpeakers = "B";
            vm.GradingFuncMicrophone = "B";
            vm.GradingFuncPorts = "B";
            vm.GradingBattery = "80%+";
            //Aesthetics
            vm.GradingAestOuterCasingLid = "B";
            vm.GradingAestOuterCasingBase = "B";
            vm.GradingAestScreen = "B";
            vm.GradingAestPalmRest = "B";
            vm.GradingAestPorts = "B";
            vm.GradingAestTrackpad = "B";
            vm.GradingAestKeyboard = "B";
            vm.GradingAestFrontBezel = "B";
            vm.GradingGrade = "B";
        }

        private void AllRefC1BTN_Click(object sender, RoutedEventArgs e)
        {
            //Functionality
            vm.GradingFuncScreen = "C1";
            vm.GradingFuncKeyboard = "C1";
            vm.GradingFuncTrackpad = "C1";
            vm.GradingFuncWebcam = "C1";
            vm.GradingFuncWifi = "C1";
            vm.GradingFuncSpeakers = "C1";
            vm.GradingFuncMicrophone = "C1";
            vm.GradingFuncPorts = "C1";
            vm.GradingBattery = "80%+";
            //Aesthetics
            vm.GradingAestOuterCasingLid = "C1";
            vm.GradingAestOuterCasingBase = "C1";
            vm.GradingAestScreen = "C1";
            vm.GradingAestPalmRest = "C1";
            vm.GradingAestPorts = "C1";
            vm.GradingAestTrackpad = "C1";
            vm.GradingAestKeyboard = "C1";
            vm.GradingAestFrontBezel = "C1";
            vm.GradingGrade = "C1";
        }

        private void AllRefC2BTN_Click(object sender, RoutedEventArgs e)
        {
            //Functionality
            vm.GradingFuncScreen = "C2";
            vm.GradingFuncKeyboard = "C2";
            vm.GradingFuncTrackpad = "C2";
            vm.GradingFuncWebcam = "C2";
            vm.GradingFuncWifi = "C2";
            vm.GradingFuncSpeakers = "C2";
            vm.GradingFuncMicrophone = "C2";
            vm.GradingFuncPorts = "C2";
            vm.GradingBattery = "50%+";
            //Aesthetics
            vm.GradingAestOuterCasingLid = "C2";
            vm.GradingAestOuterCasingBase = "C2";
            vm.GradingAestScreen = "C2";
            vm.GradingAestPalmRest = "C2";
            vm.GradingAestPorts = "C2";
            vm.GradingAestTrackpad = "C2";
            vm.GradingAestKeyboard = "C2";
            vm.GradingAestFrontBezel = "C2";
            vm.GradingGrade = "C2";
        }

        private void ResetBTN_Click(object sender, RoutedEventArgs e)
        {
            //Functionality
            vm.GradingFuncScreen = "";
            vm.GradingFuncKeyboard = "";
            vm.GradingFuncTrackpad = "";
            vm.GradingFuncWebcam = "";
            vm.GradingFuncWifi = "";
            vm.GradingFuncSpeakers = "";
            vm.GradingFuncMicrophone = "";
            vm.GradingFuncPorts = "";
            vm.GradingBattery = "";
            //Aesthetics
            vm.GradingAestOuterCasingLid = "";
            vm.GradingAestOuterCasingBase = "";
            vm.GradingAestScreen = "";
            vm.GradingAestPalmRest = "";
            vm.GradingAestPorts = "";
            vm.GradingAestTrackpad = "";
            vm.GradingAestKeyboard = "";
            vm.GradingAestFrontBezel = "";
            vm.GradingGrade = "";
        }





        static SQLiteConnection CreateConnection(string pathtodb)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            //sqlite_conn = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            //sqlite_conn = new SQLiteConnection("Data Source=" + pathtodb + "; Version = 3; New = True; Compress = True; ");
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sqlite_conn;
        }



        private void AddModelBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {


                //Add to DB
                if (!string.IsNullOrEmpty(UnitTypeCB.Text) && !string.IsNullOrEmpty(ManufacturerCB.Text) && !string.IsNullOrEmpty(ModelTB.Text) && !string.IsNullOrEmpty(AbbTB.Text))
                {


                    //Check if already present
                    LaptopDesktopModel lapdesk = vm.LaptopDesktopCollection.Where(l => l.Model == ModelTB.Text && l.Manufacturer == ManufacturerCB.Text && l.Abbreviations == AbbTB.Text && l.Abbreviations == AbbTB.Text).FirstOrDefault();

                    if (lapdesk != null)
                    {
                        //Dispaly Message to user
                        vm.TimedMessage(App.Current.FindResource("PCGradePartPresent").ToString(), "Warning", 7000);
                    }
                    else
                    {

                        //Add to DB

                        //if all criteria met create the job
                        SQLiteConnection sqlite_conn;

                        sqlite_conn = CreateConnection(cs);


                        //CreateTable(sqlite_conn);
                        InsertData(sqlite_conn);



                        switch (vm.SelectedType)
                        {
                            case "All In Ones":
                                //Call DB for Values
                                ReadLaptopDesktopData(sqlite_conn, "SELECT * FROM GeneratorTypes WHERE Type = 'Desktop' ORDER BY Type");

                                break;
                            case "Desktops":

                                //Call DB for Values
                                ReadLaptopDesktopData(sqlite_conn, "SELECT * FROM GeneratorTypes WHERE Type = 'Desktop' ORDER BY Type");

                                break;
                            case "Laptops":

                                //Call DB for Values
                                ReadLaptopDesktopData(sqlite_conn, "SELECT * FROM GeneratorTypes WHERE Type = 'Laptop' ORDER BY Type");

                                break;
                        }



                        //Clear properties
                        vm.AddModelUnitType = "";
                        vm.AddModelManufacturer = "";
                        vm.AddModelModel = "";
                        vm.AddModelAbbreviation = "";




                        //Dispaly Message to user
                        vm.TimedMessage(App.Current.FindResource("PCGradeNewModelAdded").ToString(), "Info", 7000);
                    }



                }
                else
                {
                    //Dispaly Message to user
                    vm.TimedMessage(App.Current.FindResource("PCGradeFillInAllFields").ToString(), "Warning", 7000);

                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }



        //Laptop Desktop
        public void ReadLaptopDesktopData(SQLiteConnection conn, string sqlquery)
        {

            try
            {
                //Clear Collection and reload
                vm.LaptopDesktopCombinedCollection.Clear();


                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {

                    //Create Instance of part numbers class
                    LaptopDesktopModel op = new LaptopDesktopModel();

                    op.Type = sqlite_datareader.GetString(0);
                    op.Manufacturer = sqlite_datareader.GetString(1);
                    op.Model = sqlite_datareader.GetString(2);
                    op.Abbreviations = sqlite_datareader.GetString(3);

                    //Add to Main Collection
                    vm.LaptopDesktopCollection.Add(op);
                    //Add to Combined String collection for Combo Box Selection, on selection Abbreviation needs to be pulled
                    vm.LaptopDesktopCombinedCollection.Add(op.Manufacturer + "_" + op.Model);

                }

                // sqlite_conn.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message);
            }

        }



        public void InsertData(SQLiteConnection conn)
        {

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();

            sqlite_cmd.CommandText = "INSERT INTO GeneratorTypes (Type, Manufacturer, Model, Abbreviations) VALUES(@type,@manufacturer,@model,@abbreviation)";
            sqlite_cmd.CommandType = CommandType.Text;

            sqlite_cmd.Parameters.Add(new SQLiteParameter("@type", vm.AddModelUnitType));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", vm.AddModelManufacturer));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@model", vm.AddModelModel));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@abbreviation", vm.AddModelAbbreviation));
            sqlite_cmd.ExecuteNonQuery();
        }



        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }



    }

    public class AestheticTooltip
    {
        public AestheticTooltip()
        {

        }

        public AestheticTooltip(string grading, string description)
        {
            Grading = grading;
            Description = description;
        }

        public string Grading { get; set; }
        public string Description { get; set; }

    }


}
