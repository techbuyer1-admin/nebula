﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.ViewModels;
using Nebula.Properties;
using Nebula.Models;
using Nebula.Helpers;
using MahApps.Metro.Controls;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for ServerProcessingView.xaml
    /// </summary>
    public partial class ServerProcessingView : UserControl
    {
        //declare viewmodel
        MainViewModel vm;

        public ServerProcessingView()
        {
            InitializeComponent();
        }

        public ServerProcessingView(MainViewModel PassedViewModel)
        {
            InitializeComponent();
            //get the passed in viewmodel and assign to the local vm variable
            vm = PassedViewModel;

        }


        //https://stackoverflow.com/questions/12088848/programmatically-flip-through-metro-flipview
        //
        private void FlipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var flipview = ((FlipView)sender);
            switch (flipview.SelectedIndex)
            {
                case 0:
                    flipview.BannerText = "Set Dip Switch 1 to the On Position";
                    break;
                case 1:
                    flipview.BannerText = "Xbox!";
                    break;
                case 2:
                    flipview.BannerText = "Chess!";
                    break;
            }
        }


       
    }
}
