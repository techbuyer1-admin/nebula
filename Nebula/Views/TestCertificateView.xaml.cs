﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Helpers;
using Nebula.Paginators;
using Nebula.ViewModels;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for TestCertificateView.xaml
    /// </summary>
    public partial class TestCertificateView : UserControl
    {

        MainViewModel vm;

        public TestCertificateView()
        {

        }

        public TestCertificateView(MainViewModel PassedViewModel)
        {
            InitializeComponent();

            vm = PassedViewModel;
            this.DataContext = vm;

            //Show Verified Symbols
            if(vm.TestCertificatePrinted1 == true)
            {
                TestedBySP.Visibility = Visibility.Visible;
                ServerHealthLB.Visibility = Visibility.Visible;
            }
            else
            {
                vm.TestCertificatePrinted1 = false;
                TestedBySP.Visibility = Visibility.Hidden;
                ServerHealthLB.Visibility = Visibility.Hidden;
            }

           

            //Set FlowDocument for printing Test Report
            vm.FlowDocToPrint2 = TestCertificate1;

        }


        private void PrintBTNS1_Click(object sender, RoutedEventArgs e)
        {

            if(TestedBySP.Visibility == Visibility.Visible)
            {
                //// Set clipboard to certificate name
                //Dell
                if (vm.DellServerS1 != null)
                {
                    if (!string.IsNullOrEmpty(vm.DellServerS1.ServiceTag))
                        Clipboard.SetText("Test_Certificate_" + vm.DellServerS1.ServiceTag.Trim(), TextDataFormat.UnicodeText);
                }

                ////Lenovo X-Syatem
                if (vm.LenovoServerS1 != null)
                {
                    if (!string.IsNullOrEmpty(vm.LenovoServerS1.SystemSerialNumber))
                        Clipboard.SetText("Test_Certificate_" + vm.LenovoServerS1.SystemSerialNumber.Trim(), TextDataFormat.UnicodeText);
                }

                //HP Gen8 Gen9 Gen10
                if (vm.GenGenericServerInfo1 != null)
                {
                    if (!string.IsNullOrEmpty(vm.GenGenericServerInfo1.ChassisSerial))
                        Clipboard.SetText("Test_Certificate_" + vm.GenGenericServerInfo1.ChassisSerial.Trim(), TextDataFormat.UnicodeText);
                }

                ////HP Gen 9
                //if (vm.GenGenericServerInfo1 != null)
                //{
                //    if (!string.IsNullOrEmpty(vm.GenGenericServerInfo1.ChassisSerial))
                //        Clipboard.SetText("Test_Certificate_" + vm.GenGenericServerInfo1.ChassisSerial.Trim(), TextDataFormat.UnicodeText);
                //}

                ////HP Gen 10
                //if (vm.GenGenericServerInfo1 != null)
                //{
                //    if (!string.IsNullOrEmpty(vm.GenGenericServerInfo1.ChassisSerial))
                //        Clipboard.SetText("Test_Certificate_" + vm.GenGenericServerInfo1.ChassisSerial.Trim(), TextDataFormat.UnicodeText);
                //}


                // Clipboard.SetText("Test_Certificate_Report", TextDataFormat.UnicodeText);
                //OLD METHOD

                //DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

                //dpw.SaveCollectionDocPDF(TestCertificate1);


                //Call function
                //DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();
                //Set to A4
                //dpw.PrintDocs(System.Printing.PageMediaSizeName.ISOA4);

                //Get the current Default Printer
                string DefPrinter = DocumentPaginatorWrapper.GetDefaultPrinter();


                //Set Default temp to the Microsoft PDF Virtual printer
               // DocumentPaginatorWrapper.SetDefaultPrinterQueue("Microsoft Print to PDF");

                //Create Print Dialog
                PrintDialog printDlg = new PrintDialog();

                //Arrange Size
                TestCertificate1.PagePadding = new Thickness(40);
                TestCertificate1.PageWidth = 793;// a4 width in px
                TestCertificate1.PageHeight = 1100;
                //IMPORTANT For document to fit PDF
                TestCertificate1.ColumnWidth = TestCertificate1.PageWidth;

                //Set Flowdocument as Paginator sOURCE
                IDocumentPaginatorSource idpSource = TestCertificate1;

                //Set Verified Property
                vm.TestCertificatePrinted1 = true;

                //Set Document
                vm.CertificateView1 = new TestCertificateView(vm);



                //Show Print Dialog
                if (printDlg.ShowDialog() == true)
                {
                    printDlg.PrintDocument(idpSource.DocumentPaginator, "Test Certificate");
                }


                //Set default printer back to Original
                DocumentPaginatorWrapper.SetDefaultPrinterQueue(DefPrinter);

            }
            else
            {
                MessageBox.Show("Please Verify the Server First.");
            }




        }

        private void VerifyBTNS1_Click(object sender, RoutedEventArgs e)
        {
            //Show Verified Symbols
            TestedBySP.Visibility = Visibility.Visible;
            ServerHealthLB.Visibility = Visibility.Visible;
            //To show button in PrintSaveDocCommand
            vm.ChecklistTicked = true;
        }

        private void AddJobBTN_Click(object sender, RoutedEventArgs e)
        {
            if(UpdateTitleTXTB.Text == "" || UpdateDescriptionTXTB.Text == "")
            {

            }
            else
            { //Add to Update List
                vm.TestCertificateS1.UpdatesTestingPerformedCollection.Add(new UpdatesTestingPerformed(UpdateTitleTXTB.Text, UpdateDescriptionTXTB.Text));

                //Clear TextBoxes
                UpdateTitleTXTB.Text = "";
                UpdateDescriptionTXTB.Text = "";

            }
           
        }
    }


    public class TestCert : HasPropertyChanged
    {

        public TestCert()
        {

            ServerInfoTest = new ObservableCollection<string>();

            //Create Collections to use
            ServerAdditionalInfoCollection = new ObservableCollection<ServerAdditionalInfo>();
            CPUMemInfoCollection = new ObservableCollection<CPUMemoryInfo>();
            HealthStatusCollection = new ObservableCollection<HealthStatus>();
            FirmwareLevelsCollection = new ObservableCollection<FirmwareLevels>();
            UpdatesTestingPerformedCollection = new ObservableCollection<UpdatesTestingPerformed>();
        }


        //To Hold vm.CurrentUser and Date object at top of the document
        private string _Tester;

        public string Tester
        {
            get { return _Tester; }
            set
            {
                _Tester = value;



                OnPropertyChanged("Tester");
            }
        }

        private string _Manufacturer;

        public string Manufacturer
        {
            get { return _Manufacturer; }
            set
            {
                _Manufacturer = value;



                OnPropertyChanged("Manufacturer");
            }
        }


        private string _Model;

        public string Model
        {
            get { return _Model; }
            set
            {
                _Model = value;



                OnPropertyChanged("Model");
            }
        }

        private string _Serial;

        public string Serial
        {
            get { return _Serial; }
            set
            {
                _Serial = value;



                OnPropertyChanged("Serial");
            }
        }

        //To set the brand to display at top of the document i.e Hp Dell Lenovo Cisco
        private string _Brand;

        public string Brand
        {
            get { return _Brand; }
            set
            {
                _Brand = value;



                OnPropertyChanged("Brand");
            }
        }

        //Test Certificate Image Property  
        private Image _ProductImage;

        public Image ProductImage
        {
            get { return _ProductImage; }
            set
            {
                _ProductImage = value;


                OnPropertyChanged("ProductImage");
            }
        }


   
        //Holds the path to the image which is bound to the source property Source="../Images/TestCertificate/HP-Proliant-DL380-Gen9.jpg" 
        private BitmapImage _ProductImageBitmap;

        public BitmapImage ProductImageBitmap
        {
            get { return _ProductImageBitmap; }
            set
            {
                _ProductImageBitmap = value;


                OnPropertyChanged("ProductImageBitmap");
            }
        }


        private string _ProductImagePath;

        public string ProductImagePath
        {
            get { return _ProductImagePath; }
            set
            {
                _ProductImagePath = value;


                OnPropertyChanged("ProductImagePath");
            }
        }

        //Icon at top of page displaying reult
        private string _ResultIcon;

        public string ResultIcon
        {
            get { return _ResultIcon; }
            set
            {
                _ResultIcon = value;


                OnPropertyChanged("ResultIcon");
            }
        }

        //Text at top of page displaying reult Passed Failed etc
        private string _Result;

        public string Result
        {
            get { return _Result; }
            set
            {
                _Result = value;


                OnPropertyChanged("Result");
            }
        }

        //Server Additioal Info
        private ObservableCollection<ServerAdditionalInfo> _ServerAdditionalInfoCollection;

        public ObservableCollection<ServerAdditionalInfo> ServerAdditionalInfoCollection
        {
            get { return _ServerAdditionalInfoCollection; }
            set
            {
                _ServerAdditionalInfoCollection = value;



                OnPropertyChanged("ServerAdditionalInfoCollection");
            }
        }


        //CPU Memory Info
        private ObservableCollection<CPUMemoryInfo> _CPUMemInfoCollection;

        public ObservableCollection<CPUMemoryInfo> CPUMemInfoCollection
        {
            get { return _CPUMemInfoCollection; }
            set
            {
                _CPUMemInfoCollection = value;



                OnPropertyChanged("CPUMemInfo");
            }
        }

        //Health Status
        private ObservableCollection<HealthStatus> _HealthStatusCollection;

        public ObservableCollection<HealthStatus> HealthStatusCollection
        {
            get { return _HealthStatusCollection; }
            set
            {
                _HealthStatusCollection = value;



                OnPropertyChanged("HealthStatusCollection");
            }
        }


        //Firmware Levels
        private ObservableCollection<FirmwareLevels> _FirmwareLevelsCollection;

        public ObservableCollection<FirmwareLevels> FirmwareLevelsCollection
        {
            get { return _FirmwareLevelsCollection; }
            set
            {
                _FirmwareLevelsCollection = value;



                OnPropertyChanged("FirmwareLevelsCollection");
            }
        }

        //Updates &  Levels
        private ObservableCollection<UpdatesTestingPerformed> _UpdatesTestingPerformedCollection;

        public ObservableCollection<UpdatesTestingPerformed> UpdatesTestingPerformedCollection
        {
            get { return _UpdatesTestingPerformedCollection; }
            set
            {
                _UpdatesTestingPerformedCollection = value;



                OnPropertyChanged("UpdatesTestingPerformedCollection");
            }
        }

        //Storage
        //private ObservableCollection<FirmwareLevels> _FirmwareLevelsCollection;

        //public ObservableCollection<FirmwareLevels> FirmwareLevelsCollection
        //{
        //    get { return _FirmwareLevelsCollection; }
        //    set
        //    {
        //        _FirmwareLevelsCollection = value;



        //        OnPropertyChanged("FirmwareLevelsCollection");
        //    }
        //}



        //CPU Details
        private string _CPUManufacturer;

        public string CPUManufacturer
        {
            get { return _CPUManufacturer; }
            set
            {
                _CPUManufacturer = value;



                OnPropertyChanged("CPUManufacturer");
            }
        }

        private string _CPUModel;

        public string CPUModel
        {
            get { return _CPUModel; }
            set
            {
                _CPUModel = value;



                OnPropertyChanged("CPUModel");
            }
        }

        private string _CPUCount;

        public string CPUCount
        {
            get { return _CPUCount; }
            set
            {
                _CPUCount = value;



                OnPropertyChanged("CPUCount");
            }
        }


        //Memory Details

        private string _MemoryManufacturer;

        public string MemoryManufacturer
        {
            get { return _MemoryManufacturer; }
            set
            {
                _MemoryManufacturer = value;



                OnPropertyChanged("MemoryManufacturer");
            }
        }

        private string _MemoryModel;

        public string MemoryModel
        {
            get { return _MemoryModel; }
            set
            {
                _MemoryModel = value;



                OnPropertyChanged("MemoryModel");
            }
        }

        private string _MemoryCount;

        public string MemoryCount
        {
            get { return _MemoryCount; }
            set
            {
                _MemoryCount = value;



                OnPropertyChanged("MemoryCount");
            }
        }

        //Health

        private string _CPUHealth;

        public string CPUHealth
        {
            get { return _CPUHealth; }
            set
            {
                _CPUHealth = value;



                OnPropertyChanged("CPUHealth");
            }
        }

        private string _MemoryHealth;

        public string MemoryHealth
        {
            get { return _MemoryHealth; }
            set
            {
                _MemoryHealth = value;



                OnPropertyChanged("MemoryHealth");
            }
        }


        private string _FansHealth;

        public string FansHealth
        {
            get { return _FansHealth; }
            set
            {
                _FansHealth = value;



                OnPropertyChanged("FansHealth");
            }
        }


        private string _PowerHealth;

        public string PowerHealth
        {
            get { return _PowerHealth; }
            set
            {
                _PowerHealth = value;



                OnPropertyChanged("PowerHealth");
            }
        }

        private string _StorageHealth;

        public string StorageHealth
        {
            get { return _StorageHealth; }
            set
            {
                _StorageHealth = value;



                OnPropertyChanged("StorageHealth");
            }
        }

        private string _TempratureHealth;

        public string TempratureHealth
        {
            get { return _TempratureHealth; }
            set
            {
                _TempratureHealth = value;



                OnPropertyChanged("TempratureHealth");
            }
        }

        private string _NetworkHealth;

        public string NetworkHealth
        {
            get { return _NetworkHealth; }
            set
            {
                _NetworkHealth = value;



                OnPropertyChanged("NetworkHealth");
            }
        }

        private string _BiosHealth;

        public string BiosHealth
        {
            get { return _BiosHealth; }
            set
            {
                _BiosHealth = value;



                OnPropertyChanged("BiosHealth");
            }
        }


        //Firmware Updated
        private string _Firmware1;

        public string Firmware1
        {
            get { return _Firmware1; }
            set
            {
                _Firmware1 = value;



                OnPropertyChanged("Firmware1");
            }
        }

        private string _Firmware2;

        public string Firmware2
        {
            get { return _Firmware2; }
            set
            {
                _Firmware2 = value;



                OnPropertyChanged("Firmware2");
            }
        }

        private string _Firmware3;

        public string Firmware3
        {
            get { return _Firmware3; }
            set
            {
                _Firmware3 = value;



                OnPropertyChanged("Firmware3");
            }
        }

        private string _Firmware4;

        public string Firmware4
        {
            get { return _Firmware4; }
            set
            {
                _Firmware4 = value;



                OnPropertyChanged("Firmware4");
            }
        }

        private string _Firmware5;

        public string Firmware5
        {
            get { return _Firmware5; }
            set
            {
                _Firmware5 = value;



                OnPropertyChanged("Firmware5");
            }
        }


        //Diagnostics Used

        private string _TestingProcedure1;

        public string TestingProcedure1
        {
            get { return _TestingProcedure1; }
            set
            {
                _TestingProcedure1 = value;



                OnPropertyChanged("TestingProcedure1");
            }
        }

        private string _TestingProcedure2;

        public string TestingProcedure2
        {
            get { return _TestingProcedure2; }
            set
            {
                _TestingProcedure2 = value;



                OnPropertyChanged("TestingProcedure2");
            }
        }

        private string _TestingProcedure3;

        public string TestingProcedure3
        {
            get { return _TestingProcedure3; }
            set
            {
                _TestingProcedure3 = value;



                OnPropertyChanged("TestingProcedure3");
            }
        }

        private string _TestingProcedure4;

        public string TestingProcedure4
        {
            get { return _TestingProcedure4; }
            set
            {
                _TestingProcedure4 = value;



                OnPropertyChanged("TestingProcedure4");
            }
        }


        private string _TestingProcedure5;

        public string TestingProcedure5
        {
            get { return _TestingProcedure5; }
            set
            {
                _TestingProcedure5 = value;



                OnPropertyChanged("TestingProcedure5");
            }
        }


        //LOGO SIZE
        private double _LogoWidth;

        public double LogoWidth
        {
            get { return _LogoWidth; }
            set
            {
                _LogoWidth = value;



                OnPropertyChanged("LogoWidth");
            }
        }


        private double _LogoHeight;

        public double LogoHeight
        {
            get { return _LogoHeight; }
            set
            {
                _LogoHeight = value;



                OnPropertyChanged("LogoHeight");
            }
        }



        private ObservableCollection<string> _ServerInfoTest;

        public ObservableCollection<string> ServerInfoTest
        {
            get { return _ServerInfoTest; }
            set
            {
                _ServerInfoTest = value;



                OnPropertyChanged("ServerInfoTest");
            }
        }

    }


    public class ServerAdditionalInfo
    {
        public ServerAdditionalInfo()
        {

        }
        public ServerAdditionalInfo(string nm, string val)
        {
            Name = nm;
            Value = val;
        }
        public string Name { get; set; }
        public string Value { get; set; }

    }

   

    public class CPUMemoryInfo
    {
        public CPUMemoryInfo()
        {

        }

        public CPUMemoryInfo(string nm,string val)
        {
            Name = nm;
            Value = val;
        }
        public string Name { get; set; }
        public string Value { get; set; }

    }

    public class HealthStatus
    {
        public HealthStatus()
        {

        }

        public HealthStatus(string nm, string val)
        {
            Name = nm;
            Value = val;
        }

        public string Name { get; set; }
        public string Value { get; set; }

    }

    public class FirmwareLevels
    {
        public FirmwareLevels()
        {

        }

        public FirmwareLevels(string nm, string val)
        {
            Name = nm;
            Value = val;
        }
        public string Name { get; set; }
        public string Value { get; set; }

    }


    public class UpdatesTestingPerformed
    {
        public UpdatesTestingPerformed()
        {

        }

        public UpdatesTestingPerformed(string nm,string val)
        {
            Name = nm;
            Value = val;
        }
        public string Name { get; set; }
        public string Value { get; set; }

    }


}
