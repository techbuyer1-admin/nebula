﻿using Microsoft.VisualBasic.FileIO;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Printing;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.Models;
using Nebula.Paginators;
using Nebula.Properties;
using Nebula.ViewModels;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for BarcodeListOnlyView.xaml
    /// </summary>
    public partial class BarcodeListOnlyView : UserControl
    {
      
        //declare viewmodel
        MainViewModel vm;     //
        //string CodeType = "Barcode";
        string CodeType = "QRCode";
        //Application settings
        private Settings settings = Settings.Default;


        public BarcodeListOnlyView()
        {
            InitializeComponent();
        }

        public BarcodeListOnlyView(MainViewModel PassedViewModel, ObservableCollection<NetsuiteInventory> itemsToPrint)
        {
            InitializeComponent();

            vm = PassedViewModel;
            this.DataContext = vm;

            //NetsuiteInventory nets = new NetsuiteInventory();
            //nets.InternalID = "11111";
            //nets.ProductCode = "WS-C750X";
            //nets.ProductDescription = "24 port switch";


            //netsuite observable collections
            //NetsuiteInventoryData = new ObservableCollection<NetsuiteInventory>();
            //NetsuiteQueryData = new ObservableCollection<NetsuiteInventory>();

            //vm.NetsuiteMultiPrint = new ObservableCollection<NetsuiteInventory>();
            if(itemsToPrint != null)
            {

                vm.NetsuiteMultiPrint = itemsToPrint;
            }

            // vm.NetsuiteQueryData.Add(nets);

            //Colapse all labels
            StandardLabelZebra.Visibility = Visibility.Visible;
            MediumLabelZebra.Visibility = Visibility.Collapsed;
            LargeLabelZebra.Visibility = Visibility.Collapsed;
            StandardLabelDYMO.Visibility = Visibility.Collapsed;

            vm.SelectedBarcode = "Zebra - Standard Label";
            //Clear Serial Number Text Box
            SerialNoTXTB.Text = "";

            //set default values for barcode height and barcode bar width
            vm.BarcodeHeight = 40;
            vm.BarcodeBarWidth = 1;

            if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\Inventory.csv"))
            {

            }
            else
            {
                File.Copy(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\Inventory.csv", vm.myDocs + @"\Inventory.csv");
            }

            //Copy the excel with query to netsuite local to user
            if (File.Exists(@"" + vm.myDocs + @"\NS Item WQ.xlsx"))
            {





                //call async function
                GetExcelData();

            }
            else
            {


                if (File.Exists(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\NS Item WQ.xlsx"))
                {
                    File.Copy(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\NS Item WQ.xlsx", @"" + vm.myDocs + @"\NS Item WQ.xlsx");
                    //call async function
                    GetExcelData();



                }
                else
                {
                    File.Copy(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\NS Item WQ.xlsx", @"" + vm.myDocs + @"\NS Item WQ.xlsx");



                    //call async function
                    GetExcelData();

                }




            }


            //Readin Printers
            PopulateInstalledPrintersCombo();


            //Load Settings
            LoadSettings();


            //ADD 600 TO THE NUMBER OF COPIES
            for (int i = 1; i < 601; i++)
            {
                NumberOfCopiesCB.Items.Add(i);
            }



        }




        private void LoadSettings()
        {

            if (DefaultPrinterCB.Items.Count > 0)
            {

                //DefaultPrinterCB.Text = settings.DefaultLabelPrinter;
                DefaultPrinterCB.SelectedIndex = DefaultPrinterCB.Items.IndexOf(settings.DefaultLabelPrinter);
            }


        }

        private void SaveSettings()
        {


            settings.DefaultLabelPrinter = DefaultPrinterCB.SelectedItem.ToString();


            settings.Save();
        }

        private void DefaultPrinterCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void SetPrinterBTN_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(DefaultPrinterCB.Text);
            SaveSettings();

            // MessageBox.Show(settings.DefaultLabelPrinter);
        }

        private void PopulateInstalledPrintersCombo()
        {
            // Add list of installed printers found to the combo box.
            // The pkInstalledPrinters string will be used to provide the display string.
            String pkInstalledPrinters;
            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                pkInstalledPrinters = PrinterSettings.InstalledPrinters[i];

                if (pkInstalledPrinters.Contains("Zebra") || pkInstalledPrinters.Contains("ZT") || pkInstalledPrinters.Contains("ZDesigner") || pkInstalledPrinters.Contains("DYMO") || pkInstalledPrinters.Contains("PDF"))
                {
                    //check if network printer
                    //if (pkInstalledPrinters.Contains(@"\"))
                    //{
                    //    //Network printer
                    //    DefaultPrinterCB.Items.Add(pkInstalledPrinters.Substring(pkInstalledPrinters.LastIndexOf('\\') + 1));
                    //}
                    //else
                    //{
                    //    //Local Printer
                    //    DefaultPrinterCB.Items.Add(pkInstalledPrinters);
                    //}

                    DefaultPrinterCB.Items.Add(pkInstalledPrinters);

                }

            }

            //Check for Local DYMO Printer
            foreach (string prt in DefaultPrinterCB.Items)
            {
                if (prt.Contains("DYMO"))
                {
                    //Select the correct label
                    LabelSizeCB.SelectedIndex = 1;
                    vm.SelectedBarcode = "DYMO - Standard Label";
                    QRBARToggle.IsChecked = true;
                    CodeType = "QRCode";
                    StandardLabelDYMOImage.Width = 60;
                    StandardLabelDYMOImage.Height = 60;
                    StandardLabelZebraImage.Width = 50;
                    StandardLabelZebraImage.Height = 50;
                    MediumLabelZebraImage.Width = 50;
                    MediumLabelZebraImage.Height = 50;
                    LargeLabelZebraImage.Width = 60;
                    LargeLabelZebraImage.Height = 60;
                    //Recreate Label
                    // LabelsToGenerate();
                    //Select Default Printer
                    DefaultPrinterCB.SelectedIndex = 0;

                    //Save Settings
                    //MessageBox.Show(DefaultPrinterCB.Text);
                    SaveSettings();
                    //MessageBox.Show("Local DYMO Present");
                }

                if (prt.Contains("BROTHER"))
                {
                    //Select the correct label
                    LabelSizeCB.SelectedIndex = 1;
                    vm.SelectedBarcode = "BROTHER - Standard Label";
                    QRBARToggle.IsChecked = true;
                    CodeType = "QRCode";
                    StandardLabelDYMOImage.Width = 60;
                    StandardLabelDYMOImage.Height = 60;
                    StandardLabelZebraImage.Width = 50;
                    StandardLabelZebraImage.Height = 50;
                    MediumLabelZebraImage.Width = 50;
                    MediumLabelZebraImage.Height = 50;
                    LargeLabelZebraImage.Width = 60;
                    LargeLabelZebraImage.Height = 60;
                    //Recreate Label
                    // LabelsToGenerate();
                    //Select Default Printer
                    DefaultPrinterCB.SelectedIndex = 0;

                    //Save Settings
                    //MessageBox.Show(DefaultPrinterCB.Text);
                    SaveSettings();
                    //MessageBox.Show("Local DYMO Present");
                }

            }


        }


        private void ToggleSwitch_Checked(object sender, RoutedEventArgs e)
        {
            CodeType = "Barcode";
            //Recreate Label
            LabelsToGenerate();
            //CodeType = "QRCode";
        }

        private void ToggleSwitch_Unchecked(object sender, RoutedEventArgs e)
        {

            CodeType = "QRCode";
            //Recreate Label
            LabelsToGenerate();

            //   QueryResultsLV.SelectionMode = SelectionMode.Multiple;

            //foreach(NetsuiteInventory record in QueryResultsLV.SelectedItems)
            //{
            //       MessageBox.Show(record.PrintQuantity.ToString()); 
            //}

            //NetsuiteInventory RecordRow = QueryResultsLV.Items.GetItemAt(ItemIndex) as NetsuiteInventory;
            //vm.BarcodeDescription = RecordRow.ProductDescription;
            //vm.BarcodeData = RecordRow.ProductCode;

        }



        private async void GetExcelData()
        {

            try
            {
                //Refresh the data
                //RefreshBTN.IsEnabled = false;


                //Clear Collections and current search
                vm.NetsuiteInventoryData.Clear();
                vm.NetsuiteQueryData.Clear();
                vm.NetsuiteInventoryCount = 0;
                vm.BarcodeData = "";
                vm.BarcodeDescription = "";
                vm.BarcodeSearch = "";

                //Read in the data from excel direct await 
                //ProgressBar.IsIndeterminate = true;
                await System.Threading.Tasks.Task.Run(() => ExcelToCSV(@"" + vm.myDocs + @"\NS Item WQ.xlsx", @"" + vm.myDocs + @"\Inventory.csv"));


                if (File.Exists(@"" + vm.myDocs + @"\Inventory.csv"))
                {
                    //File.Delete(@"" + vm.myDocs + @"\Inventory.csv");
                    //await PutTaskDelay(20000);
                }
                else
                {
                    //wait 10 seconds for query to run
                    await PutTaskDelay(20000);

                }

                // await PutTaskDelay(20000);

                //ExcelToCSV(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\NS Item WQ.xlsx", @"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\Inventory.csv");


                if (File.Exists(@"" + vm.myDocs + @"\NS Item WQ.xlsx"))
                {
                    //Read in the data from csv
                    await System.Threading.Tasks.Task.Run(() => vm.ReadInventoryDataCommand.Execute(@"" + vm.myDocs + @"\Inventory.csv"));
                    // vm.ReadInventoryDataCommand.Execute(@"" + @"\\pinnacle.local\tech_resources\Nebula\Barcodes\Inventory.csv");
                }
                else
                {
                    MessageBox.Show("File is missing, please contact support");
                }



                // await ReadExcel(@"C:\Users\N.Myers\Desktop\Excel Test\NS Item WQ.xlsx");

               // ProgressBar.IsIndeterminate = false;

                //set focus to search field
                //SearchTXTB.Focus();

               // RefreshBTN.IsEnabled = true;

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }



        public void GenerateLabel()
        {
            if (vm != null)
                if (vm.BarcodeData != null || vm.BarcodeData != "" || vm.BarcodeData != string.Empty)
                {


                    switch (CodeType)
                    {
                        case "Barcode":
                            //StandardLabelZebraImage.Width = 220;
                            AlterBarWidthHeight();
                            StandardLabelZebraImage.Width = double.NaN; //252

                            MediumLabelZebraImage.Width = double.NaN;

                            LargeLabelZebraImage.Width = double.NaN;

                            StandardLabelZebraImage.Height = 30;
                            MediumLabelZebraImage.Height = 30;
                            LargeLabelZebraImage.Height = 30;
                            //StandardLabelZebraImage.Width = 246; //252

                            //MediumLabelZebraImage.Width = 280;

                            //LargeLabelZebraImage.Width = 320;

                            switch (LabelSizeCB.Text)
                            {
                                case "Zebra - Standard Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        StandardLabelZebraImage.Source = CreateBarcode();

                                    }
                                    else
                                    {
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }

                                    break;
                                case "Zebra - Medium Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        MediumLabelZebraImage.Source = CreateBarcode();

                                    }
                                    else
                                    {
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }

                                    break;
                                case "Zebra - Large Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        LargeLabelZebraImage.Source = CreateBarcode();

                                    }
                                    else
                                    {
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }

                                    break;
                                case "DYMO - Standard Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        StandardLabelDYMOImage.Source = CreateBarcode();

                                    }
                                    else
                                    {
                                        StandardLabelDYMOImage.Source = null; //CreateQRCode("");
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }

                                    break;
                               
                            }




                            break;

                        case "QRCode":
                            //QRCODE
                            //BarWidthHeightCB.SelectedIndex = 3;
                            StandardLabelZebraImage.Width = 50;
                            StandardLabelZebraImage.Height = 50;
                            StandardLabelDYMOImage.Width = 50;
                            StandardLabelDYMOImage.Height = 50;
                            MediumLabelZebraImage.Width = 50;
                            MediumLabelZebraImage.Height = 50;
                            LargeLabelZebraImage.Width = 60;
                            LargeLabelZebraImage.Height = 60;

                            switch (LabelSizeCB.Text)
                            {
                                case "Zebra - Standard Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        StandardLabelZebraImage.Source = CreateQRCode(vm.BarcodeData);

                                    }
                                    else
                                    {
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }


                                    break;
                                case "Zebra - Medium Label":

                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        MediumLabelZebraImage.Source = CreateQRCode(vm.BarcodeData);

                                    }
                                    else
                                    {
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }

                                    break;
                                case "Zebra - Large Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        LargeLabelZebraImage.Source = CreateQRCode(vm.BarcodeData);

                                    }
                                    else
                                    {

                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }


                                    break;
                                case "DYMO - Standard Label":
                                    if (String.IsNullOrWhiteSpace(vm.BarcodeData) != true)
                                    {
                                        StandardLabelDYMOImage.Source = CreateQRCode(vm.BarcodeData);

                                    }
                                    else
                                    {
                                        StandardLabelDYMOImage.Source = null; //CreateQRCode("");
                                        StandardLabelZebraImage.Source = null; //CreateQRCode("");
                                        MediumLabelZebraImage.Source = null; //CreateQRCode("");
                                        LargeLabelZebraImage.Source = null;// CreateQRCode("");
                                    }


                                    break;
                            }

                            break;
                    }


                    //QRCODE
                    // StandardLabelZebraImage.Source = CreateQRCode(vm.BarcodeData);
                    // //Update the image 
                    //// StandardLabelZebraImage.Source = CreateBarcode();
                    //MediumLabelZebraImage.Source = CreateBarcode();
                    //LargeLabelZebraImage.Source = CreateBarcode();
                    //switch (vm.SelectedBarcode)
                    //{
                    //    case "Standard Label":
                    //        dpw.PrintStandardVisual(StandardLabelZebraEntireBarcode);
                    //        break;
                    //    case "Large Label":
                    //        dpw.PrintStandardVisual(LargeLabelEntireBarcode);
                    //        break;
                    //}

                    // vm.BarcodeImage.Source = CreateBarcode();
                }
        }





        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            //generate a new label
            GenerateLabel();


        }



        private void SerialNoTXTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(vm.BarcodeSerialNo) != true)
            {
                vm.QRSubTitle = "Serial No";
                vm.ShowSerialTitle = Visibility.Visible;
                StandardSerialLabelZebraImage.Source = CreateQRCode(vm.BarcodeSerialNo);
                MediumSerialLabelZebraImage.Source = CreateQRCode(vm.BarcodeSerialNo);
                LargeSerialLabelZebraImage.Source = CreateQRCode(vm.BarcodeSerialNo);
                StandardSerialLabelDYMOImage.Source = CreateQRCode(vm.BarcodeSerialNo);
            }
            else
            {
                vm.QRSubTitle = "";
                vm.ShowSerialTitle = Visibility.Hidden;
                StandardSerialLabelZebraImage.Source = null; //CreateQRCode("");
                MediumSerialLabelZebraImage.Source = null; //CreateQRCode("");
                LargeSerialLabelZebraImage.Source = null;// CreateQRCode("");
                StandardSerialLabelDYMOImage.Source = null; //CreateQRCode("");
            }
        }




        //public void GeneratePng(Visual mapGrid, string pngURI)
        //{
        //    double dpi = App.SelectedDPI;
        //    double scale = dpi / 96;
        //    Rect bounds = VisualTreeHelper.GetDescendantBounds(mapGrid);
        //    RenderTargetBitmap rt = new RenderTargetBitmap((int)(bounds.Width * scale),
        //                                                   (int)(bounds.Height * scale),
        //                                                    dpi,
        //                                                    dpi,
        //                                                    PixelFormats.Pbgra32);
        //    DrawingVisual dv = new DrawingVisual();
        //    using (DrawingContext ctx = dv.RenderOpen())
        //    {
        //        VisualBrush vb = new VisualBrush(mapGrid);
        //        ctx.DrawRectangle(vb, null, new Rect(new Point(), bounds.Size));
        //    }
        //    rt.Render(dv);
        //    PngBitmapEncoder png = new PngBitmapEncoder();
        //    png.Frames.Add(BitmapFrame.Create(rt));
        //    using (Stream streamPng = File.Create(pngURI))
        //    {
        //        png.Save(streamPng);
        //    }
        //}

        private static Bitmap ResizeImage(Bitmap mg, System.Drawing.Size newSize)
        {
            double ratio = 0d;
            double myThumbWidth = 0d;
            double myThumbHeight = 0d;
            int x = 0;
            int y = 0;

            Bitmap bp;


            if ((mg.Width / Convert.ToDouble(newSize.Width)) > (mg.Height /
            Convert.ToDouble(newSize.Height)))
                ratio = Convert.ToDouble(mg.Width) / Convert.ToDouble(newSize.Width);
            else
                ratio = Convert.ToDouble(mg.Height) / Convert.ToDouble(newSize.Height);
            myThumbHeight = Math.Ceiling(mg.Height / ratio);
            myThumbWidth = Math.Ceiling(mg.Width / ratio);

            System.Drawing.Size thumbSize = new System.Drawing.Size((int)myThumbWidth, (int)myThumbHeight);
            bp = new Bitmap(newSize.Width, newSize.Height);
            x = (newSize.Width - thumbSize.Width) / 2;
            y = (newSize.Height - thumbSize.Height);

            System.Drawing.Graphics g = Graphics.FromImage(bp);
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            System.Drawing.Rectangle rect = new System.Drawing.Rectangle(x, y, thumbSize.Width, thumbSize.Height);
            g.DrawImage(mg, rect, 0, 0, mg.Width, mg.Height, GraphicsUnit.Pixel);

            return bp;
        }

        public BitmapImage CreateBarcode()
        {

            BitmapImage bi = new BitmapImage();

            //AlterBarWidthHeight();




            ////Set the serial number to text on the clipboard  Use control v

            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
            b.BarWidth = vm.BarcodeBarWidth;

            //b.Alignment = BarcodeLib.AlignmentPositions.LEFT;
            b.StandardizeLabel = false; // (bool)StandardizeLabelCHK.IsChecked;
            //System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.CODE128, "UCS-MR-1X162RY-AB-REF", System.Drawing.Color.Black, System.Drawing.Color.White, 290, 120);

            // if (vm.BarcodeData != null || vm.BarcodeData != "" || vm.BarcodeData != string.Empty)
            if (string.IsNullOrWhiteSpace(vm.BarcodeData))
            {


            }
            else
            {
                // System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.CODE128, vm.BarcodeData, System.Drawing.Color.Black, System.Drawing.Color.White, 680, 50);



                //Create an image object from the barcode
                //MessageBox.Show(vm.BarcodeHeight.ToString());

                //WORKING
                //System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.CODE128, vm.BarcodeData, System.Drawing.Color.Black, System.Drawing.Color.White, 680, vm.BarcodeHeight);


                System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.CODE128, vm.BarcodeData, System.Drawing.Color.Black, System.Drawing.Color.White, 600, 30);
                //System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.CODE128, vm.BarcodeData, System.Drawing.Color.Black, System.Drawing.Color.White, 4000, vm.BarcodeHeight);

                // Winforms Image we want to get the WPF Image from...
                //from file
                // System.Drawing.Image imgWinForms = System.Drawing.Image.FromFile("test.png");

                System.Drawing.Image imgWinForms = img;

                // ImageSource ...

                // MessageBox.Show(img.Width + " || " + img.Height);

                bi.BeginInit();

                MemoryStream ms = new MemoryStream();

                // Save to a memory stream...

                imgWinForms.Save(ms, ImageFormat.Bmp);

                // Rewind the stream...

                ms.Seek(0, SeekOrigin.Begin);

                // Tell the WPF image to use this stream...

                bi.StreamSource = ms;
                bi.DecodePixelWidth = img.Width;
                //bi.DecodePixelWidth = 200;
                //bi.DecodePixelHeight = 30;
                //bi.DecodePixelWidth = img.Height;
                //       // bi.DecodePixelHeight = 40;

                bi.EndInit();


                //CreatePNG(bi);

                return bi;
            }



            return bi;



        }




        //Generate QRCODE Image
        public BitmapImage CreateQRCode(string stringData)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(stringData, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(40, System.Drawing.Color.Black, System.Drawing.Color.White, null, 1, 1, true);



            return ToBitmapImage(qrCodeImage);
        }

        //BITMAP TO BITMAP IMAGE
        public BitmapImage ToBitmapImage(Bitmap bitmap)
        {
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();

                return bitmapImage;
            }
        }

        ////BITMAP IMAGE TO BITMAP
        private Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            return new Bitmap(bitmapImage.StreamSource);
        }



        public BitmapSource ConvertToBitmapSource(UIElement element)
        {
            var target = new RenderTargetBitmap((int)(element.RenderSize.Width), (int)(element.RenderSize.Height), 96, 96, PixelFormats.Pbgra32);
            var brush = new VisualBrush(element);

            var visual = new DrawingVisual();
            var drawingContext = visual.RenderOpen();


            drawingContext.DrawRectangle(brush, null, new Rect(new System.Windows.Point(0, 0),
                new System.Windows.Point(element.RenderSize.Width, element.RenderSize.Height)));

            drawingContext.PushOpacityMask(brush);

            drawingContext.Close();

            target.Render(visual);

            return target;
        }


        public void CreatePNG(BitmapImage bi)
        {

            //FormattedText text = new FormattedText("Barcode Test",
            //new CultureInfo("en-us"),
            //FlowDirection.LeftToRight,
            //new Typeface(this.FontFamily, FontStyles.Normal, FontWeights.Normal, new FontStretch()),
            //this.FontSize, System.Windows.Media.Brushes.White);

            // The Visual to use as the source of the RenderTargetBitmap.
            DrawingVisual drawingVisual = new DrawingVisual();
            DrawingContext drawingContext = drawingVisual.RenderOpen();
            drawingContext.DrawImage(bi, new Rect(0, 0, bi.Width, bi.Height));
            //drawingContext.DrawText(text, new System.Windows.Point(bi.Height / 2, 0));
            drawingContext.Close();

            // The BitmapSource that is rendered with a Visual.
            RenderTargetBitmap rtb = new RenderTargetBitmap(bi.PixelWidth, bi.PixelHeight, 96, 96, PixelFormats.Pbgra32);
            rtb.Render(drawingVisual);

            // Encoding the RenderBitmapTarget as a PNG file.
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(rtb));
            using (Stream stm = File.Create(@"C:\Users\N.Myers\Desktop\For USA\new.png"))
            {
                png.Save(stm);
            }
        }

        private async void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            ComboBox cmb = (ComboBox)sender;
            //MessageBox.Show(cmb.SelectedItem.ToString());
            if (StandardLabelZebra != null)
            {
                StandardLabelZebra.Visibility = Visibility.Collapsed;
                MediumLabelZebra.Visibility = Visibility.Collapsed;
                LargeLabelZebra.Visibility = Visibility.Collapsed;
                StandardLabelDYMO.Visibility = Visibility.Collapsed;

                //MessageBox.Show(((ComboBoxItem)cmb.SelectedItem).Content.ToString());
                //generate a new label
                //GenerateLabel();
                string selected = ((ComboBoxItem)cmb.SelectedItem).Content.ToString();

                switch (selected)
                {
                    case "Zebra - Standard Label":
                        // Show Label
                        StandardLabelZebra.Visibility = Visibility.Visible;
                        vm.SelectedBarcode = "Zebra - Standard Label";

                        break;
                    case "Zebra - Medium Label":
                        // Show Label
                        MediumLabelZebra.Visibility = Visibility.Visible;
                        vm.SelectedBarcode = "Zebra - Medium Label";

                        break;

                    case "Zebra - Large Label":
                        // MessageBox.Show(cmb.SelectedItem.ToString());
                        // Show Label
                        LargeLabelZebra.Visibility = Visibility.Visible;
                        vm.SelectedBarcode = "Zebra - Large Label";

                        break;

                    case "DYMO - Standard Label":
                        // Show Label
                        StandardLabelDYMO.Visibility = Visibility.Visible;
                        vm.SelectedBarcode = "DYMO - Standard Label";

                        break;
                }
                await PutTaskDelay(300);

                //generate a new label
                GenerateLabel();
            }

        }

        private void SearchBTN_Click(object sender, RoutedEventArgs e)
        {




            //clear the listview collection items
            vm.NetsuiteQueryData.Clear();


            //Search Inventory
            SearchInventory();
        }

        private void SearchTXTB_KeyDown(object sender, KeyEventArgs e)
        {
            //Check if enter key was hit in textbox
            if (e.Key == Key.Enter)
            {
                //clear the listview collection items
                vm.NetsuiteQueryData.Clear();
                // set progress bar to true
                //Search Inventory
                SearchInventory();
            }
        }



        public void SearchInventory()
        {
            try
            {



                if (String.IsNullOrWhiteSpace(vm.BarcodeSearch))
                {

                }
                else
                {
                   // ProgressBar.IsIndeterminate = true;
                    //await PutTaskDelay(1000);

                    // MessageBox.Show("Came in"); vm.BarcodeSearch != null || vm.BarcodeSearch != "" || vm.BarcodeSearch != string.Empty ||

                    //IEnumerable<NetsuiteInventory> foos1 = vm.NetsuiteInventoryData.Where(c => c.ProductCode == vm.BarcodeData);
                    IEnumerable<NetsuiteInventory> results = from c in vm.NetsuiteInventoryData
                                                             where c.ProductCode.StartsWith(vm.BarcodeSearch.ToUpper())
                                                             select c;

                    //find results and add to query collection
                    foreach (var itm in results)
                    {
                        vm.NetsuiteQueryData.Add(itm);
                        //MessageBox.Show(itm.ProductDescription);

                    }

                    // set progress bar to false
                   // ProgressBar.IsIndeterminate = false;
                }

                // return "";

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }



        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

        private void QueryResultsLV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           

            NumberOfCopiesCB.SelectedIndex = 0;

            //ItemIndex = QueryResultsLV.SelectedIndex;
            //MessageBox.Show(ItemIndex.ToString());
            //if (ItemIndex != -1)
            //{
            //   // NetsuiteInventory RecordRow = QueryResultsLV.Items.GetItemAt(ItemIndex) as NetsuiteInventory;
            //    //vm.BarcodeDescription = RecordRow.ProductDescription;
            //    //vm.BarcodeData = RecordRow.ProductCode;







            //    //Change Bar Height Based on product code length
            //    switch (CodeType)
            //    {
            //        case "Barcode":
            //            //BarWidthHeightCB.SelectedIndex = 2;
            //            //BarWidthCB.SelectedIndex = 0;
            //            AlterBarWidthHeight();
            //            break;

            //        case "QRCode":
            //            //QRCODE

            //            break;
   
        }

        private void QueryResultsLV_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            //Change Bar Height Based on product code length
            //AlterBarWidthHeight();
        }


        private void AlterBarWidthHeight()
        {



            if (vm.BarcodeData.Count() >= 28)
            {
                //MessageBox.Show(vm.BarcodeData.Count().ToString());
                BarWidthHeightCB.SelectedIndex = 6; //40
                                                    // vm.BarcodeHeight = 45;
            }
            else if (vm.BarcodeData.Count() >= 25 && vm.BarcodeData.Count() < 28)
            {
                // MessageBox.Show(vm.BarcodeData.Count().ToString());
                //MessageBox.Show("Here");
                BarWidthHeightCB.SelectedIndex = 5; //35
                                                    // vm.BarcodeHeight = 40;
            }
            else if (vm.BarcodeData.Count() >= 21 && vm.BarcodeData.Count() < 25)
            {
                //MessageBox.Show(vm.BarcodeData.Count().ToString());
                //MessageBox.Show("Here");
                BarWidthHeightCB.SelectedIndex = 4; //40
                                                    // vm.BarcodeHeight = 35;
            }
            else if (vm.BarcodeData.Count() >= 15 && vm.BarcodeData.Count() < 21)
            {
                //MessageBox.Show(vm.BarcodeData.Count().ToString());
                //MessageBox.Show("Here");
                BarWidthHeightCB.SelectedIndex = 3; //40
                                                    // vm.BarcodeHeight = 35;
            }
            else if (vm.BarcodeData.Count() >= 12 && vm.BarcodeData.Count() < 15)
            {
                //MessageBox.Show(vm.BarcodeData.Count().ToString());
                //MessageBox.Show("Here");
                BarWidthHeightCB.SelectedIndex = 3; //30
                                                    // vm.BarcodeHeight = 30;
            }
            else if (vm.BarcodeData.Count() >= 6 && vm.BarcodeData.Count() < 12)
            {
                //MessageBox.Show(vm.BarcodeData.Count().ToString());
                //MessageBox.Show("Here");
                BarWidthHeightCB.SelectedIndex = 1; //20
                                                    // vm.BarcodeHeight = 15;
            }
            else if (vm.BarcodeData.Count() <= 5)
            {
                //MessageBox.Show(vm.BarcodeData.Count().ToString());
                //MessageBox.Show("Here");
                BarWidthHeightCB.SelectedIndex = 1; //10
                //vm.BarcodeHeight = 10;
            }


            vm.BarcodeProductIDCount = vm.BarcodeData.Count().ToString();



            //else
            //{

            //    BarWidthHeightCB.SelectedIndex = 2; //25
            //    MessageBox.Show(vm.BarcodeData.Count().ToString());
            //    // vm.BarcodeHeight = 25;
            //}


        }





        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            //clear the listview collection items
            vm.NetsuiteQueryData.Clear();
            vm.BarcodeSearch = "";
            vm.BarcodeData = "";
            vm.BarcodeDescription = "";
            vm.BarcodeSerialNo = "";
        }



        //Excel file needs trusting to run the query
        //


        public Task ReadExcel(string excelPath)
        {

            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            if (excelApp != null)
            {
                Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excelApp.Workbooks.Open(excelPath, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                Microsoft.Office.Interop.Excel.Worksheet excelWorksheet = (Microsoft.Office.Interop.Excel.Worksheet)excelWorkbook.Sheets[1];
                excelApp.DisplayAlerts = false;
                Microsoft.Office.Interop.Excel.Range excelRange = excelWorksheet.UsedRange;
                int rowCount = excelRange.Rows.Count;
                int colCount = excelRange.Columns.Count;

                for (int i = 1; i <= rowCount; i++)
                {

                    //pull in the worksheet range and assign to system array
                    Microsoft.Office.Interop.Excel.Range range = excelWorksheet.get_Range("A" + i.ToString(), "B" + i.ToString());
                    System.Array myvalues = (System.Array)range.Cells.Value;

                    //Convert to a string array                   
                    string[] strArray = ConvertToStringArray(myvalues);

                    //create new instance of netsuite item and assign array value
                    NetsuiteInventory nitm = new NetsuiteInventory();
                    nitm.ProductCode = strArray[0];
                    nitm.ProductDescription = strArray[1];

                    //add row to collection
                    vm.NetsuiteInventoryData.Add(nitm);

                    //for (int j = 1; j <= colCount; j++)
                    //{
                    ////Microsoft.Office.Interop.Excel.Range range = (excelWorksheet.Cells[i, 1] as Microsoft.Office.Interop.Excel.Range);




                    //string cellValue1 = range.Value.ToString();
                    //    string cellValue2 = range.Value.ToString();
                    //MessageBox.Show(cellValue);
                    //    //do anything
                    //}
                }

                excelWorkbook.Close();
                excelApp.Quit();
            }



            // excelApp.Quit();

            return Task.CompletedTask;
        }




        string[] ConvertToStringArray(System.Array values)
        {

            // create a new string array
            string[] theArray = new string[values.Length];

            // loop through the 2-D System.Array and populate the 1-D String Array
            for (int i = 1; i <= values.Length; i++)
            {
                if (values.GetValue(1, i) == null)
                    theArray[i - 1] = "";
                else
                    theArray[i - 1] = (string)values.GetValue(1, i).ToString();
            }

            return theArray;
        }


        //exporting excel into csv file
        public async void ExcelToCSV(string excelPath, string exportPath)
        {

            //this Thread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Microsoft.Office.Interop.Excel.Application app = null;
            //InteropExcel.Workbooks wkbks = null;
            Microsoft.Office.Interop.Excel.Workbook wb = null;


            try
            {


                String fromFile = excelPath;
                String toFile = exportPath;

                app = new Microsoft.Office.Interop.Excel.Application();
                wb = app.Workbooks.Open(fromFile, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                app.DisplayAlerts = false;
                // refresh the excel data
                wb.RefreshAll();
                //System.Threading.Thread.Sleep(5000);

                await PutTaskDelay(12000);

                //wb.Save();

                //await PutTaskDelay(5000);
                // this does not throw exception if file doesnt exist
                //File.Delete(toFile);
                //await PutTaskDelay(12000);
                wb.SaveAs(toFile, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSVWindows, Type.Missing, Type.Missing, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, false, Type.Missing, Type.Missing, Type.Missing);
                //await PutTaskDelay(8000);
                //wb.Close(true, Type.Missing, Type.Missing);

                //app.Quit();
                if (wb != null)
                {
                    //app.DisplayAlerts = false;
                    // wb.Close(false);
                    wb.Close(true, Type.Missing, Type.Missing);
                    // Marshal.FinalReleaseComObject(wb);
                    wb = null;
                }

                //if (wkbks != null)
                //{
                //    wkbks.Close();
                //    Marshal.FinalReleaseComObject(wkbks);
                //    wkbks = null;
                //}

                if (app != null)
                {
                    // Close Excel.
                    app.Quit();
                    // Marshal.ReleaseComObject(app);
                    // Marshal.FinalReleaseComObject(app);
                    await PutTaskDelay(1000);
                    //Terminate PID Instance
                    TerminateExcelProcess(app);

                    app = null;


                }




            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            // return Task.CompletedTask;
        }


        //CAPTURE PID FOR EXCEL PROCESS

        [DllImport("user32.dll")]
        static extern int GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);

        Process GetExcelProcess(Microsoft.Office.Interop.Excel.Application excelApp)
        {
            int id;
            GetWindowThreadProcessId(excelApp.Hwnd, out id);
            return Process.GetProcessById(id);
        }

        void TerminateExcelProcess(Microsoft.Office.Interop.Excel.Application excelApp)
        {
            var process = GetExcelProcess(excelApp);
            if (process != null)
            {
                process.Kill();
            }
        }



        private void RefreshBTN_Click(object sender, RoutedEventArgs e)
        {

            //call async function
            GetExcelData();
        }

        private void BarWidthHeightCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            //If Barcode height change readjust
            if (vm != null)
            {
                //get the combobox handle
                ComboBox cmb = (ComboBox)sender;
                //set barcode height property
                vm.BarcodeHeight = Convert.ToInt32(((ComboBoxItem)cmb.SelectedItem).Content.ToString());

                //call method to generate for all labels
                LabelsToGenerate();
            }

        }

        private void BarWidthCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //If Barcode height change readjust
            if (vm != null)
            {
                //get the combobox handle
                ComboBox cmb = (ComboBox)sender;
                //set barcode height property
                vm.BarcodeBarWidth = Convert.ToInt32(((ComboBoxItem)cmb.SelectedItem).Content.ToString());

                //call method to generate for all labels
                LabelsToGenerate();


            }
        }



        private void LabelsToGenerate()
        {

            //generate a new label
            GenerateLabel();

        }

        private void ProductCodeFontCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //MessageBox.Show(ProductCodeFontCB.SelectedValue.ToString());
            //Recreate Label

            LabelsToGenerate();
        }

        //private void AddBTN_Click(object sender, RoutedEventArgs e)
        //{
        //    foreach (NetsuiteInventory selItm in QueryResultsLV.SelectedItems)
        //    {
        //        //set the default quantity value
        //        selItm.PrintQuantity = "1";
        //        //add to the collection
        //        vm.NetsuiteMultiPrint.Add(selItm);
        //    }
        //}



        private async void PrintMultiBTNS1_Click(object sender, RoutedEventArgs e)
        {
            if (DefaultPrinterCB.Text != "")
            {

                ProgressMsg.Visibility = Visibility.Visible;
                ProgressRing.IsActive = true;
                //Loop Through all items in the multi print LV And Generate and print
                foreach (NetsuiteInventory selItm in MultiPrintLV.Items)
                {
                    //MessageBox.Show(selItm.ProductDescription + " || " + selItm.ProductCode + " || " + selItm.PrintQuantity);
                    //Populate The Field Data
                    vm.BarcodeDescription = selItm.ProductDescription;
                    vm.BarcodeData = selItm.ProductCode;
                    vm.BarcodeSerialNo = selItm.SerialNumber;
                    //ProgressMsg.Text = "Now Printing " + selItm.ProductCode;
                    //Generate Labels
                    LabelsToGenerate();
                    await PutTaskDelay(1500);
                    //print number of copies
                    PrintLabels(selItm.PrintQuantity.ToString());

                    await PutTaskDelay(1000);
                }

                ProgressMsg.Text = "Printing Complete!";
                await PutTaskDelay(2000);


                //Hide progress
                ProgressMsg.Visibility = Visibility.Collapsed;
                ProgressRing.IsActive = false;
            }
            else
            {
                MessageBox.Show("Please Select Your Default Label Printer!", "Printer Error");
            }

        }


        private async void PrintLabels(string numberOfCopies)
        {
            if (DefaultPrinterCB.Text != "")
            {
                PrintingProgressBar.IsIndeterminate = true;

                DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();
                //dpw.PrintStandardVisual(StandardLabelZebraEntireBarcode);

                // MessageBox.Show(vm.SelectedBarcode);

                switch (vm.SelectedBarcode)
                {
                    case "Zebra - Standard Label":

                        switch (CodeType)
                        {
                            case "Barcode":
                                //OFFSET FOR ZEBRA PRINTER
                                // MessageBox.Show(vm.SelectedBarcode);
                                //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                                // MessageBox.Show(vm.SelectedBarcode);
                                //dpw.PrintStandardVisual(StandardLabelZebraEntireBarcode);

                                // StandardLabelZebraImage.Margin = new Thickness(0, 0, 20, 0);
                                //StandardSerialLabelZebraImage.Margin = new Thickness(0, 0, 14, 0);
                                await PutTaskDelay(800);

                                dpw.PrintDirectVisualZEBRA(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);


                                //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //StandardSerialLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                // StandardLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                break;

                            case "QRCode":
                                //QRCODE
                                //  dpw.PrintStandardVisual(StandardLabelZebraEntireBarcode);
                                //dpw.PrintDirectVisual(StandardLabelZebraEntireBarcode, new PrintQueue(new PrintServer(((System.Printing.PrintQueue)DefaultPrinterCB.SelectedItem).HostingPrintServer.Name), (string)DefaultPrinterCB.SelectedValue), (int)NumberOfCopiesCB.SelectedValue, PageOrientation.Portrait);
                                //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                await PutTaskDelay(800);
                                dpw.PrintDirectVisualZEBRA(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);

                                //StandardLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);

                                break;
                        }

                        break;
                    case "Zebra - Medium Label":
                        switch (CodeType)
                        {
                            case "Barcode":
                                //OFFSET FOR ZEBRA PRINTER
                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //MediumLabelZebraImage.Margin = new Thickness(0, 0, 20, 0);
                                //MediumSerialLabelZebraImage.Margin = new Thickness(0, 0, 14, 0);
                                await PutTaskDelay(800);
                                //dpw.PrintStandardVisual(MediumLabelZebraEntireBarcode);
                                dpw.PrintDirectVisualZEBRA(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);
                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //MediumSerialLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                //MediumLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                break;

                            case "QRCode":
                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                await PutTaskDelay(800);
                                //QRCODE
                                dpw.PrintDirectVisualZEBRA(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);

                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //dpw.PrintStandardVisual(MediumLabelZebraEntireBarcode);
                                break;
                        }

                        break;
                    case "Zebra - Large Label":
                        switch (CodeType)
                        {
                            case "Barcode":
                                //OFFSET FOR ZEBRA PRINTER
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //LargeLabelZebraImage.Margin = new Thickness(0, 0, 20, 0);
                                //LargeSerialLabelZebraImage.Margin = new Thickness(0, 0, 14, 0);
                                await PutTaskDelay(800);
                                dpw.PrintDirectVisualZEBRA(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);
                                //dpw.PrintStandardVisual(LargeLabelZebraEntireBarcode);
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //LargeSerialLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                //LargeLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                break;

                            case "QRCode":

                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                await PutTaskDelay(800);
                                //QRCODE
                                // dpw.PrintStandardVisual(LargeLabelZebraEntireBarcode);
                                dpw.PrintDirectVisualZEBRA(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Portrait, LabelSizeCB.Text);
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                break;
                        }

                        break;
                    case "DYMO - Standard Label":

                        //if (DefaultPrinterCB.Text.Contains("DYMO"))
                        //{
                        //    dpw.PrintDirectVisual(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                        //}
                        //else
                        //{
                        //}
                        switch (CodeType)
                        {
                            case "Barcode":
                                //OFFSET FOR ZEBRA PRINTER
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 14, 0);
                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 14, 0);
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                dpw.PrintDirectVisualDYMOBROTHER(StandardLabelDYMOEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //dpw.PrintStandardVisual(LargeLabelZebraEntireBarcode);
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //LargeSerialLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                //LargeLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                break;

                            case "QRCode":
                                //QRCODE
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                dpw.PrintDirectVisualDYMOBROTHER(StandardLabelDYMOEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                break;
                        }

                        break;

                    case "BROTHER - Standard Label":

                        //if (DefaultPrinterCB.Text.Contains("DYMO"))
                        //{
                        //    dpw.PrintDirectVisual(StandardLabelZebraEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                        //}
                        //else
                        //{
                        //}
                        switch (CodeType)
                        {
                            case "Barcode":
                                //OFFSET FOR ZEBRA PRINTER
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 14, 0);
                                //MediumLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 14, 0);
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                dpw.PrintDirectVisualDYMOBROTHER(StandardLabelDYMOEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //dpw.PrintStandardVisual(LargeLabelZebraEntireBarcode);
                                //LargeLabelZebraEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                //LargeSerialLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                //LargeLabelZebraImage.Margin = new Thickness(0, 0, 0, 0);
                                break;

                            case "QRCode":
                                //QRCODE
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                dpw.PrintDirectVisualDYMOBROTHER(StandardLabelDYMOEntireBarcode, DefaultPrinterCB.Text, numberOfCopies, PageOrientation.Landscape);
                                //StandardLabelDYMOEntireBarcode.Margin = new Thickness(0, 0, 0, 0);
                                break;
                        }

                        break;
                }

                ////Get the text from the clipboard
                //Clipboard.GetText(TextDataFormat.UnicodeText);
                // dpw.PrintStandardVisual(vm.SelectedBarcode);

                //Clear Serial Number Text Box
                SerialNoTXTB.Text = "";

                PrintingProgressBar.IsIndeterminate = false;
                //dpw.PrintStandardVisual(TheBarcode);
            }
            else
            {
                MessageBox.Show("Please Select Your Default Label Printer!", "Printer Error");
            }
        }



        private void ClearMultiBTNS1_Click(object sender, RoutedEventArgs e)
        {
            vm.NetsuiteMultiPrint.Clear();
        }

        private void ImportMultiBTNS1_Click(object sender, RoutedEventArgs e)
        {

            //IMPORT CSV 
            //LOOP THROUGH LIST AND QUERY BARCODE using System.Windows.Forms;

            using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
            {

                //Requires GUID for MyComputer Or ThisPC Folder
                openFileDialog.InitialDirectory = vm.downld; //drives; //@"" + @"\\pinnacle.local\tech_resources\Nebula\XML ServerChecklists"
                openFileDialog.Filter = "All files (*.*)|*.*";
                openFileDialog.FilterIndex = 0;
                openFileDialog.Multiselect = true;
                openFileDialog.RestoreDirectory = true;

                // string currentDateTime = DateTime.Now.ToShortDateString().Replace(@"/", "-").Replace(":", "_");

                //MessageBox.Show(rootpath + vm.ChassisSerialNo + " Test Reports " + currentDateTime);

                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    foreach (var file in openFileDialog.FileNames)
                    {

                        // MessageBox.Show(file);
                        if (File.Exists(file))
                        {
                            //Will Create a new file for each year

                            //Uses a VB class to process the file instead of streamreader
                            using (TextFieldParser parser = new TextFieldParser(file))
                            {
                                parser.TextFieldType = FieldType.Delimited;
                                parser.SetDelimiters(",");
                                while (!parser.EndOfData)
                                {
                                    //Processing row
                                    string currentLine = parser.ReadLine();
                                    if (currentLine.Contains("Quantity") || currentLine.Contains("Server Build CTO") || currentLine.Contains("End of Group") || currentLine.ToUpper().Contains("VARIOUS") || currentLine.ToUpper().Contains("FAILED PART"))
                                    {
                                        //Dont include headers
                                    }
                                    else
                                    {
                                        vm.NetsuiteMultiPrint.Add(FromCsv(currentLine));
                                    }



                                }
                            }
                        }

                    }


                }

            }

        }

        public NetsuiteInventory FromCsv(string csvLine)
        {
            try
            {


                if (csvLine != null)
                {

                    Regex CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                    string[] values = CSVParser.Split(csvLine);
                    //string[] values = csvLine.Split(',');


                    NetsuiteInventory sproc = new NetsuiteInventory();

                    switch (values.Count().ToString())
                    {
                        case "5":
                            if (values[0] != string.Empty)
                                sproc.ProductCode = Convert.ToString(values[0]);
                            if (values[1] != string.Empty)
                                sproc.ProductDescription = Convert.ToString(values[1]);
                            //Replace any negative vaules
                            if (values[3] != string.Empty)
                            {
                                if (sproc.ProductDescription != null)
                                {
                                    //Check if Memory and reduce quantity to 1, to reduce
                                    if (sproc.ProductDescription.Contains(" MEMORY ") || sproc.ProductDescription.Contains(" Memory ") || sproc.ProductDescription.Contains(" memory ") || sproc.ProductDescription.Contains(" MEM ") || sproc.ProductDescription.Contains(" mem ")
                                        && sproc.ProductDescription.Contains("DIMM ") || sproc.ProductDescription.Contains(" Dimm") || sproc.ProductDescription.Contains(" KIT") || sproc.ProductDescription.Contains(" Kit") || sproc.ProductDescription.Contains(" kit")
                                        || sproc.ProductDescription.Contains(" ECC ") || sproc.ProductDescription.Contains(" Ecc ") || sproc.ProductDescription.Contains(" ecc ") || sproc.ProductDescription.Contains(" DDR") || sproc.ProductDescription.Contains(" Ddr") || sproc.ProductDescription.Contains(" ddr"))
                                    {
                                        sproc.PrintQuantity = "1";

                                    }
                                    else
                                    {
                                        sproc.PrintQuantity = Convert.ToString(values[3].Replace("-", ""));
                                    }
                                }
                                else
                                {
                                    sproc.PrintQuantity = Convert.ToString(values[3].Replace("-", ""));
                                }

                            }
                            break;
                        case "7":
                            if (values[1] != string.Empty)
                                sproc.ProductCode = Convert.ToString(values[1]);
                            if (values[2] != string.Empty)
                                sproc.ProductDescription = Convert.ToString(values[2]);
                            //Replace any negative vaules
                            if (values[4] != string.Empty)
                            {

                                if (sproc.ProductDescription != null)
                                {
                                    //Check if Memory and reduce quantity to 1, to reduce
                                    if (sproc.ProductDescription.Contains(" MEMORY ") || sproc.ProductDescription.Contains(" Memory ") || sproc.ProductDescription.Contains(" memory ") || sproc.ProductDescription.Contains(" MEM ") || sproc.ProductDescription.Contains(" mem ")
                                        && sproc.ProductDescription.Contains("DIMM ") || sproc.ProductDescription.Contains(" Dimm") || sproc.ProductDescription.Contains(" KIT") || sproc.ProductDescription.Contains(" Kit") || sproc.ProductDescription.Contains(" kit")
                                        || sproc.ProductDescription.Contains(" ECC ") || sproc.ProductDescription.Contains(" Ecc ") || sproc.ProductDescription.Contains(" ecc ") || sproc.ProductDescription.Contains(" DDR") || sproc.ProductDescription.Contains(" Ddr") || sproc.ProductDescription.Contains(" ddr"))
                                    {
                                        sproc.PrintQuantity = "1";

                                    }
                                    else
                                    {
                                        sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                    }
                                }
                                else
                                {
                                    sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                }
                                //}
                            }
                            break;
                        case "8":
                            if (values[1] != string.Empty)
                                sproc.ProductCode = Convert.ToString(values[1]);
                            if (values[2] != string.Empty)
                                sproc.ProductDescription = Convert.ToString(values[2]);
                            //Replace any negative vaules
                            if (values[4] != string.Empty)
                            {
                                if (sproc.ProductDescription != null)
                                {
                                    //Check if Memory and reduce quantity to 1, to reduce
                                    if (sproc.ProductDescription.Contains(" MEMORY ") || sproc.ProductDescription.Contains(" Memory ") || sproc.ProductDescription.Contains(" memory ") || sproc.ProductDescription.Contains(" MEM ") || sproc.ProductDescription.Contains(" mem ")
                                        && sproc.ProductDescription.Contains("DIMM ") || sproc.ProductDescription.Contains(" Dimm") || sproc.ProductDescription.Contains(" KIT") || sproc.ProductDescription.Contains(" Kit") || sproc.ProductDescription.Contains(" kit")
                                        || sproc.ProductDescription.Contains(" ECC ") || sproc.ProductDescription.Contains(" Ecc ") || sproc.ProductDescription.Contains(" ecc ") || sproc.ProductDescription.Contains(" DDR") || sproc.ProductDescription.Contains(" Ddr") || sproc.ProductDescription.Contains(" ddr"))
                                    {
                                        sproc.PrintQuantity = "1";

                                    }
                                    else
                                    {
                                        sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                    }
                                }
                                else
                                {
                                    sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                }
                            }
                            break;
                        //Extra Column with PO\SO\RMA Number
                        case "10":
                            if (values[1] != string.Empty)
                                sproc.ProductCode = Convert.ToString(values[1]);
                            if (values[2] != string.Empty)
                                sproc.ProductDescription = Convert.ToString(values[2]);
                            //Replace any negative vaules
                            if (values[4] != string.Empty)
                            {
                                //Check if Memory and reduce quantity to 1, to reduce
                                if (sproc.ProductDescription != null)
                                {
                                    //Check if Memory and reduce quantity to 1, to reduce
                                    if (sproc.ProductDescription.Contains(" MEMORY ") || sproc.ProductDescription.Contains(" Memory ") || sproc.ProductDescription.Contains(" memory ") || sproc.ProductDescription.Contains(" MEM ") || sproc.ProductDescription.Contains(" mem ")
                                        && sproc.ProductDescription.Contains("DIMM ") || sproc.ProductDescription.Contains(" Dimm") || sproc.ProductDescription.Contains(" KIT") || sproc.ProductDescription.Contains(" Kit") || sproc.ProductDescription.Contains(" kit")
                                        || sproc.ProductDescription.Contains(" ECC ") || sproc.ProductDescription.Contains(" Ecc ") || sproc.ProductDescription.Contains(" ecc ") || sproc.ProductDescription.Contains(" DDR") || sproc.ProductDescription.Contains(" Ddr") || sproc.ProductDescription.Contains(" ddr"))
                                    {
                                        sproc.PrintQuantity = "1";

                                    }
                                    else
                                    {
                                        sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                    }
                                }
                                else
                                {
                                    sproc.PrintQuantity = Convert.ToString(values[4].Replace("-", ""));
                                }
                            }
                            break;
                    }




                    return sproc;



                }

                return null;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

        }

        private void BarcodeListUC_Unloaded(object sender, RoutedEventArgs e)
        {
            //clear collections
            vm.NetsuiteQueryData.Clear();
            vm.NetsuiteMultiPrint.Clear();
        }

        private void DeleteSelectedBTNS1_Click(object sender, RoutedEventArgs e)
        {
            try
            {


                List<NetsuiteInventory> DeleteList = new List<NetsuiteInventory>();

                foreach (NetsuiteInventory itm in MultiPrintLV.SelectedItems)
                {

                    DeleteList.Add(itm);
                    // MessageBox.Show(itm.ProductCode);
                }

                foreach (NetsuiteInventory itm in DeleteList)
                {


                    vm.NetsuiteMultiPrint.Remove(itm);
                    // MessageBox.Show(itm.ProductCode);
                }


                DeleteList.Clear();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }


    }
}
