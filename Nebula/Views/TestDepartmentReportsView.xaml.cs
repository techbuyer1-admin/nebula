﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Xps.Packaging;
using System.Xml;
using Nebula.Commands;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.Paginators;
using Nebula.ViewModels;
using Nebula.Views;
using QRCoder;
using Brushes = System.Windows.Media.Brushes;
using FontFamily = System.Windows.Media.FontFamily;


namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for TestDepartmentReportsView.xaml
    /// </summary>
    public partial class TestDepartmentReportsView : UserControl
    {
        GoodsInOutViewModel vm;
        //Connection to SQL Lite DB
        //if all criteria met create the job
        SQLiteConnection sqlite_conn;
        //SQL Lite DB LINK

        // SET Database Path
        public string cs = "";
        

        public TestDepartmentReportsView()
        {
            InitializeComponent();
          
            this.DataContext = vm;

            //Point DB to Test Databases
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\TestDept_Reporting.db";
            }
            else
            {
                if (vm.CurrentUserLocation == "United Kingdom")
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/TestReporting/TestDept_Reporting.db";
                }
                else
                {

                    //Set connection string to Live DB
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\TestDept_Reporting.db";
                }
            }


        }
        public TestDepartmentReportsView(GoodsInOutViewModel PassedViewModel)
        {
            InitializeComponent();
            vm = PassedViewModel;
            this.DataContext = vm;

            //Point DB to Test Databases
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\TestDept_Reporting.db";
                //DFS FAST ACCESS
                //cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/TestDept_Reporting.db";
            }
            else
            {


                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/TestReporting/TestDept_Reporting.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/TestReporting/TestDept_Reporting.db";
                }
                else
                {

                    //Set connection string to Live DB
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\TestDept_Reporting.db";
                }
             
              
            }


          

          
            //vm.CurrentUser = "";
            vm.TestSerialNo = "";
            vm.TestPOSONo = "";
            vm.TestCLIOutput = "";
            ReportMSGTB.Text = "";

            //////Call initial data set async
            //InitialData();
        

            // If collection changes Count
            vm.RecordsTotal = ReportLV.Items.Count.ToString();


            ////Set message with uncpath & location. To help debunk the US Writing to our DB
            UserLocTB.Text = "DataPath: (" + StaticFunctions.UncPathToUse + ") Location: (" + StaticFunctions.UserLocation + ")";
        }


        private async void InitialData()
        {
            //Read Data
            FilterDateSearch("", "");
        }


        private void SignBTN_Click(object sender, RoutedEventArgs e)
        {


            if (vm.TestSerialNo == "") 
            {
                ReportMSGTB.Text = App.Current.FindResource("TestRepGBEnterSerialNo").ToString(); //"Please enter a Serial Number!!!";
            }
            else
            {
                // return;
                if (string.IsNullOrWhiteSpace(vm.TestPOSONo))
                {
                    MessageBox.Show(App.Current.FindResource("TestRepGBRequiredPOSONo").ToString(), App.Current.FindResource("TestRepGBCreateRecord").ToString(), MessageBoxButton.OK, MessageBoxImage.Warning); //"The PO/SO Number field is required!", "Create Record"
                }

                else if (string.IsNullOrWhiteSpace(vm.TestSerialNo))
                {
                    MessageBox.Show(App.Current.FindResource("TestRepGBRequiredSerialNo").ToString(), App.Current.FindResource("TestRepGBCreateRecord").ToString(), MessageBoxButton.OK, MessageBoxImage.Warning); //"The Serial Number field is required!", "Create Record"
                }

                else
                {
                    //Generate QR's
                    GenerateLabel();

                    //Refresh Current User
                    vm.ReportUser = "Technician Name: " + vm.CurrentUserInitials + " " + DateTime.Now.ToShortDateString();
                    //ADD DB ENTRY HERE
                    //CreateTable(sqlite_conn);
                    //insert all items in table to DB
                    //InsertData(sqlite_conn);

                    //Add preview images to front page
                    CreatePreviews();

                    //Show the Flow Document Reader
                    FDR.Visibility = Visibility.Visible;
                    //Enable print button
                    PrintBTN.IsEnabled = true;
                    vm.ITADIsReprint = false;
                }

            }
        }




        //Create Connection to DB
        static SQLiteConnection CreateConnection(string pathtodb)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            //sqlite_conn = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            //sqlite_conn = new SQLiteConnection("Data Source=" + pathtodb + "; Version = 3; New = True; Compress = True; ");
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sqlite_conn;
        }



        public void InsertData()
        {

            try
            {

            //Create connection
            sqlite_conn = CreateConnection(cs);

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.CommandText = "INSERT INTO ReportRecords (DateCreated, POSONumber, AssetNo, SerialNo, Manufacturer, ModelNo, DeviceCategory, ProcessPerformed, TestingPerformed, Notes, ReportBody, SavePath, TestedBy)" + " VALUES(@datecreated,@posonumber,@assetno,@serialno,@manufacturer,@modelno,@devicecategory,@processperformed,@testingperformed,@notes,@reportbody,@savepath,@testedby)";
            sqlite_cmd.CommandType = CommandType.Text;

            //LOOP THROUGH ADDED COLLECTION
            //foreach (NetsuiteInventory itm in vm.ScannedSerialCollection)
            //{

            //Date must be in this format to work with SQL Lite
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@datecreated", DateTime.Now.ToString("yyyy-MM-dd H:mm:ss")));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@posonumber", vm.TestPOSONo));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@assetno", @"n\a"));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", vm.TestSerialNo.Replace("Serial No:","").Trim()));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", @"n\a"));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@modelno", @"n\a"));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@devicecategory", @"n\a"));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@processperformed", @"n\a"));

            // adds options if selected by end user. This is written to a single field in the db
            string optionsSelected = "";

            //HARD RESET
           if (HardResetCB.IsChecked == true)
           {
              // add to string
              optionsSelected += "|Hard Reset|";
           }
           //GREEN LIGHT
           if (GreenLightCB.IsChecked == true)
           {
              // add to string
              optionsSelected += "|Green Light Tested|";
             
           }
           //FAULTY
           if (FaultyCB.IsChecked == true)
           {
            // add to string
            optionsSelected += "|Item Faulty|";

           }


            //Write string to db
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@testingperformed", optionsSelected));



            if (vm.ITADNotes == string.Empty || vm.ITADNotes == "")
            {
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@notes", " "));
            }
            else
            {
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@notes", vm.ITADNotes));
            }


            if (vm.TestCLIOutput == string.Empty || vm.TestCLIOutput == "")
            {
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportbody", " "));
            }
            else
            {
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportbody", vm.TestCLIOutput));
            }



            sqlite_cmd.Parameters.Add(new SQLiteParameter("@savepath", " "));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@testedby", "Technician Name: " + vm.CurrentUserInitials + " " + DateTime.Now.ToShortDateString())); // added later
            sqlite_cmd.ExecuteNonQuery();

             //Close DB connection
             sqlite_conn.Close();


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }



        }

        public void InsertCategory()
        {

            try
            {
            //Create connection
            sqlite_conn = CreateConnection(cs);
          

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.CommandText = "INSERT INTO DeviceCategories (Manufacturer, Model, Category)" + " VALUES(@manufacturer,@model,@category)";
            sqlite_cmd.CommandType = CommandType.Text;


            //Date must be in this format to work with SQL Lite
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", " "));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@model", " "));
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@category", vm.ITADAddCategory));
            sqlite_cmd.ExecuteNonQuery();


            //Close DB connection
            sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }


        public void ReadCategory(string sqlquery)
        {
            try
            {

                //Create connection
                sqlite_conn = CreateConnection(cs);

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ITADDeviceCategories dc = new ITADDeviceCategories();

                    dc.Manufacturer = sqlite_datareader.GetString(1);
                    dc.Model = sqlite_datareader.GetString(2);
                    dc.Category = sqlite_datareader.GetString(3);



                    vm.ITADCategoryCollection.Add(dc);

                    //Close DB connection
                    sqlite_conn.Close();
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        public void InsertProcess()
        {
            try
            {
                //Create connection
                sqlite_conn = CreateConnection(cs);

                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "INSERT INTO ProcessPerformed (Process)" + " VALUES(@process)";
                sqlite_cmd.CommandType = CommandType.Text;


                //Date must be in this format to work with SQL Lite
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@process", vm.ITADAddProcess));
                sqlite_cmd.ExecuteNonQuery();

                //Close DB connection
                sqlite_conn.Close();


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void ReadProcess(string sqlquery)
        {
            try
            {

                //Create connection
                sqlite_conn = CreateConnection(cs);

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ITADProcessesPerformed pp = new ITADProcessesPerformed();

                    pp.Process = sqlite_datareader.GetString(1);


                    vm.ITADProcessesCollection.Add(pp);

                }

                // If collection changes Count
                vm.RecordsTotal = ReportLV.Items.Count.ToString();

                //Close DB connection
                sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void InsertTestingProcess()
        {
            try
            {

            //Create connection
            sqlite_conn = CreateConnection(cs);

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.CommandText = "INSERT INTO TestingPerformed (Process)" + " VALUES(@process)";
            sqlite_cmd.CommandType = CommandType.Text;


            //Date must be in this format to work with SQL Lite
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@process", vm.ITADAddTesting));
            sqlite_cmd.ExecuteNonQuery();

            //Close DB connection
            sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void ReadTestingProcess(string sqlquery)
        {
            try
            {

                //Create connection
                sqlite_conn = CreateConnection(cs);

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {


                    ITADTestingPerformed pp = new ITADTestingPerformed();

                    pp.Process = sqlite_datareader.GetString(1);


                    vm.ITADTestingCollection.Add(pp);

                }

                // If collection changes Count
                vm.RecordsTotal = ReportLV.Items.Count.ToString();

                //Close DB connection
                sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        public async void ReadData(string sqlquery)
        {

            try
            {

                //Create connection
                sqlite_conn = CreateConnection(cs);

                //Clear Collection
                vm.ITADReportCollection = new ObservableCollection<ITADReport>();
                vm.ITADReportCollection.Clear();

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;


                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {
                    //// Console.WriteLine(sqlite_datareader.GetString(1).ToString());

                    ITADReport itm = new ITADReport();
                    if (sqlite_datareader[0].GetType() != typeof(DBNull))
                        itm.InternalID = sqlite_datareader.GetInt32(0);
                    if (sqlite_datareader[1].GetType() != typeof(DBNull))
                    {

                        string date = sqlite_datareader.GetString(1);
                        itm.DateCreated = Convert.ToDateTime(date);
                    }

                    if (sqlite_datareader[2].GetType() != typeof(DBNull))
                        itm.POSONumber = sqlite_datareader.GetString(2);
                    if (sqlite_datareader[3].GetType() != typeof(DBNull))
                        itm.AssetNo = sqlite_datareader.GetString(3);
                    if (sqlite_datareader[4].GetType() != typeof(DBNull))
                        itm.SerialNo = sqlite_datareader.GetString(4);
                    if (sqlite_datareader[5].GetType() != typeof(DBNull))
                        itm.Manufacturer = sqlite_datareader.GetString(5);
                    if (sqlite_datareader[6].GetType() != typeof(DBNull))
                        itm.ModelNo = sqlite_datareader.GetString(6);
                    if (sqlite_datareader[7].GetType() != typeof(DBNull))
                        itm.DeviceCategory = sqlite_datareader.GetString(7);
                    if (sqlite_datareader[8].GetType() != typeof(DBNull))
                        itm.ProcessPerformed = sqlite_datareader.GetString(8);
                    if (sqlite_datareader[9].GetType() != typeof(DBNull))
                        itm.TestingPerformed = sqlite_datareader.GetString(9);
                    if (sqlite_datareader[10].GetType() != typeof(DBNull))
                        itm.Notes = sqlite_datareader.GetString(10);
                    if (sqlite_datareader[11].GetType() != typeof(DBNull))
                        itm.ReportBody = sqlite_datareader.GetString(11);
                    if (sqlite_datareader[12].GetType() != typeof(DBNull))
                        itm.SavePath = sqlite_datareader.GetString(12);
                    if (sqlite_datareader[13].GetType() != typeof(DBNull))
                        itm.TestedBy = sqlite_datareader.GetString(13);


                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {

                        vm.ITADReportCollection.Add(itm);
                    });




                }

                // If collection changes Count
                vm.RecordsTotal = vm.ITADReportCollection.Count.ToString();

                //Close DB connection
                sqlite_conn.Close();


            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            //CalculateTotals();
            // conn.Close();
        }

    

        public void ReadDataSessionList(string sqlquery)
        {

            try
            {


             //create new instance
             vm.ITADReportSelectedItem = new ITADReport();


        

            //Generate QR
            GenerateLabel();

            //Enable Print Button Again
            PrintBTN.IsEnabled = true;

            //set reprint flag to true
            vm.ITADIsReprint = true;

            //Create connection
            sqlite_conn = CreateConnection(cs);

            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = sqlquery;

            //Clear Collection
            //vm.ITADReportCollection.Clear();

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            while (sqlite_datareader.Read())
            {
                // Console.WriteLine(sqlite_datareader.GetString(1).ToString());

                ITADReport itm = new ITADReport();
                if (sqlite_datareader[0].GetType() != typeof(DBNull))
                    itm.InternalID = sqlite_datareader.GetInt32(0);
                if (sqlite_datareader[1].GetType() != typeof(DBNull))
                {

                    string date = sqlite_datareader.GetString(1);
                    itm.DateCreated = Convert.ToDateTime(date);

                    //    itm.DateCreated = sqlite_datareader.GetDateTime(1);
                    // show the error message to the UI for invalid TO Date
                    // return;

                }

                if (sqlite_datareader[2].GetType() != typeof(DBNull))
                    vm.ITADReportSelectedItem.POSONumber = sqlite_datareader.GetString(2);
                if (sqlite_datareader[3].GetType() != typeof(DBNull))
                    vm.ITADReportSelectedItem.AssetNo = sqlite_datareader.GetString(3);
                if (sqlite_datareader[4].GetType() != typeof(DBNull))
                    vm.ITADReportSelectedItem.SerialNo = sqlite_datareader.GetString(4);
                if (sqlite_datareader[5].GetType() != typeof(DBNull))
                    vm.ITADReportSelectedItem.Manufacturer = sqlite_datareader.GetString(5);
                if (sqlite_datareader[6].GetType() != typeof(DBNull))
                    vm.ITADReportSelectedItem.ModelNo = sqlite_datareader.GetString(6);
                if (sqlite_datareader[7].GetType() != typeof(DBNull))
                    vm.ITADReportSelectedItem.DeviceCategory = sqlite_datareader.GetString(7);
                if (sqlite_datareader[8].GetType() != typeof(DBNull))
                    vm.ITADReportSelectedItem.ProcessPerformed = sqlite_datareader.GetString(8);
                if (sqlite_datareader[9].GetType() != typeof(DBNull))
                    vm.ITADReportSelectedItem.TestingPerformed = sqlite_datareader.GetString(9);
                if (sqlite_datareader[10].GetType() != typeof(DBNull))
                    vm.ITADReportSelectedItem.Notes = sqlite_datareader.GetString(10);
                if (sqlite_datareader[11].GetType() != typeof(DBNull))
                    vm.ITADReportSelectedItem.ReportBody = sqlite_datareader.GetString(11);
                if (sqlite_datareader[12].GetType() != typeof(DBNull))
                    vm.ITADReportSelectedItem.SavePath = sqlite_datareader.GetString(12);
                if (sqlite_datareader[13].GetType() != typeof(DBNull))
                    vm.ITADReportSelectedItem.TestedBy = sqlite_datareader.GetString(13);

            }

                //Close DB connection
                sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message); ;
            }

            //Check if green light tested
            if (vm.ITADReportSelectedItem.TestingPerformed != null)
                if (vm.ITADReportSelectedItem.TestingPerformed.Contains("Green Light Tested"))
                {
                    GreenLightCB.IsChecked = true;
                    vm.ITADTestingPerformed = "Green Light Tested";
                }



        }

        public void DeleteData()
        {
            try
            {

            //Create connection
            sqlite_conn = CreateConnection(cs);
           
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.CommandText = "DELETE FROM ReportRecords WHERE SerialNo = @serialno;";
            sqlite_cmd.CommandType = CommandType.Text;
            sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", vm.TestSerialNo));

            sqlite_cmd.ExecuteNonQuery();

            //Close DB connection
            sqlite_conn.Close();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message); ;
            }

        }


        private void DeleteCategoryBTN_Click(object sender, RoutedEventArgs e)
        {

            if (vm.ITADDeviceCategory != "")
            {
                try
                {

             
                //Create connection
                sqlite_conn = CreateConnection(cs);

                SQLiteCommand sqlite_cmd;
                //SQLiteConnection conn = new SQLiteConnection();
                //conn.Open();
                sqlite_cmd = sqlite_conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "DELETE FROM DeviceCategories WHERE Category = @category;";
                sqlite_cmd.CommandType = CommandType.Text;
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@category", vm.ITADDeviceCategory));

                sqlite_cmd.ExecuteNonQuery();

                //Re-Read and reset ComboBoxes
                ReadAndResetComboCategories();

                //Close DB connection
                sqlite_conn.Close();

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message); ;
                }

            }
        }

        private void DeleteErasureBTN_Click(object sender, RoutedEventArgs e)
        {
            if (vm.ITADProcessPerformed != "")
            {

                try
                {


                //Create connection
                sqlite_conn = CreateConnection(cs);

                SQLiteCommand sqlite_cmd;
                SQLiteConnection conn = new SQLiteConnection();

                sqlite_cmd = sqlite_conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "DELETE FROM ProcessPerformed WHERE Process = @process;";
                sqlite_cmd.CommandType = CommandType.Text;
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@process", vm.ITADProcessPerformed));

                sqlite_cmd.ExecuteNonQuery();

                //Re-Read and reset ComboBoxes
                ReadAndResetComboCategories();

                //Close DB connection
                sqlite_conn.Close();

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message); ;
                }
            }
        }



        private void DeleteTestingBTN_Click(object sender, RoutedEventArgs e)
        {
            if (vm.ITADTestingPerformed != "")
            {

                try
                {


                //Create connection
                sqlite_conn = CreateConnection(cs);

                SQLiteCommand sqlite_cmd;
                SQLiteConnection conn = new SQLiteConnection();

                sqlite_cmd = sqlite_conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "DELETE FROM TestingPerformed WHERE Process = @process;";
                sqlite_cmd.CommandType = CommandType.Text;
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@process", vm.ITADTestingPerformed));

                sqlite_cmd.ExecuteNonQuery();

                //Re-Read and reset ComboBoxes
                ReadAndResetComboCategories();

                //Close DB connection
                sqlite_conn.Close();

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message); ;
                }
            }

        }

        private void ReadAndResetComboCategories()
        {
            vm.ITADCategoryCollection.Clear();
            vm.ITADProcessesCollection.Clear();
            vm.ITADTestingCollection.Clear();

            //Load Any Categories
            ReadCategory("SELECT * FROM DeviceCategories ORDER BY Category");


            //Load Any Processes
            ReadProcess("SELECT * FROM ProcessPerformed ORDER BY Process");

            //Load Any Processes
            ReadTestingProcess("SELECT * FROM TestingPerformed ORDER BY Process");

            //DeleteDeviceCatCB.SelectedIndex = -1;
            //DeleteErasureCB.SelectedIndex = -1;
            //DeleteTestingCB.SelectedIndex = -1;

            //DeviceCatCB.SelectedIndex = -1;
            //ProcessPerCB.SelectedIndex = -1;
            //TestingProCB.SelectedIndex = -1;
        }



        private async void FilterSearch(string searchFilter, string whichColumn)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            try
            {

       
            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.AddDays(1).ToString("yyyy-MM-dd");

            //old query
            //await Task.Run(() => ReadData(sqlite_conn, "SELECT * FROM ReportRecords WHERE DateCreated BETWEEN '" + dateFrom + "' AND '" + dateTo + "' AND TestedBy LIKE '" + "Tested By: " + searchFilter.ToUpper() + "' || '%' OR TestedBy LIKE '" + "Technician Name: " + searchFilter.ToUpper() + "' || '%' OR SerialNo LIKE '" + searchFilter.ToUpper() + "' || '%' OR POSONumber LIKE '" + searchFilter.ToUpper() + "' || '%'"));
            //Run task
            await Task.Run(() => ReadData("SELECT * FROM ReportRecords WHERE TestedBy LIKE '" + "Tested By: " + searchFilter.ToUpper() + "' || '%' OR TestedBy LIKE '" + "Technician Name: " + searchFilter.ToUpper() + "' || '%' OR SerialNo LIKE '" + searchFilter.ToUpper() + "' || '%' OR POSONumber LIKE '" + searchFilter.ToUpper() + "' AND DateCreated BETWEEN '" + dateFrom + "' AND '" + dateTo + "'"));


            Mouse.OverrideCursor = null;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message); ;
            }
        }



        private async void FilterDateSearch(string searchFilter, string whichColumn)
        {
            try
            {

            Mouse.OverrideCursor = Cursors.Wait;

            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.AddDays(1).ToString("yyyy-MM-dd");

            await Task.Run(() => ReadData("SELECT * FROM ReportRecords WHERE DateCreated BETWEEN '" + dateFrom + "' AND '" + dateTo + "'"));

            Mouse.OverrideCursor = null;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message); ;
            }
        }


        //INSERT Custom Labels into DB
        public void InsertCustomLabelsDB(ObservableCollection<NetsuiteInventory> LabelList)
        {
            
            //Set String to DB Path
            string cs = "";


            //Point DB to Test Databases
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\LabelDetails.db";
            }
            else
            {
                if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/LabelDetails/LabelDetails.db"))
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/LabelDetails/LabelDetails.db";
                }
                else
                {
                    //Set connection string to Live DB
                    //Use DFS Hosted DB for multiple site access
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula/Repository/Data/LabelDetails.db";
                }

              
            }

            //Connection to SQL Lite DB
            //if all criteria met create the job
            SQLiteConnection conn;
            conn = CreateConnection(cs);

            //Create Query List
            List<string> myQueries = new List<string>();



            //ADD TO DB

            try
            {

                //LOOP THROUGH ADDED COLLECTION
                foreach (NetsuiteInventory itm in LabelList)
                {
                    //Console.WriteLine(itm.PONumber  + " " + itm.SerialNumber + " " + itm.TestedByOperative);
                    //Add query to list
                    myQueries.Add("INSERT INTO LabelRecords (DateCreated, POSONumber, AssetNo, SerialNo, Manufacturer, ModelNo, DeviceCategory, ProcessPerformed, TestingPerformed, Notes, ReportBody, SavePath, TestedBy)" +
                        " VALUES('" + DateTime.Now.ToString("yyyy-MM-dd") + "','" + itm.PONumber + "','" + "" + "','" + itm.SerialNumber + "','" + "" + "','" + "" + "','" + "" + "','" + "" + "','" + "" + "','" + "" + "','" + "" + "','" + itm.ReportPath + "','" + itm.ReceivedByOperative + "')");

                }




                //Create a transaction
                using (var tra = conn.BeginTransaction())
                {
                    try
                    {
                        //Console.WriteLine(myQueries.Count().ToString() + "\n");

                        foreach (var myQuery in myQueries)
                        {
                            //Console.WriteLine(myQuery.ToString());

                            using (var cd = new SQLiteCommand(myQuery, conn, tra))
                            {
                                cd.ExecuteNonQuery();
                            }
                        }

                        tra.Commit();
                    }
                    catch (Exception ex)
                    {
                        tra.Rollback();
                        //Console.Error.Writeline("I did nothing, because something wrong happened: {0}", ex);
                        throw;
                    }
                }


                //Close DB
                conn.Close();


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }


        private async void PrintBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Clear the message text box
                ReportMSGTB.Text = "";

                string dateTimeFN = DateTime.Now.ToString().Replace(@"/", "-").Replace(":", "_");
                //Create collection to send to the db
                ObservableCollection<NetsuiteInventory> customLabels = new ObservableCollection<NetsuiteInventory>();

                if (vm.TestSerialNo == "") //Serial No: 
                {
                    ReportMSGTB.Text = App.Current.FindResource("TestRepGBEnterSerialNo").ToString(); //"Please enter a Serial Number!!!";
                }
                else
                {

                string savePath = "";


                //Generate QR
                    GenerateLabel();

            
                    //Check User location & Department
                    switch (StaticFunctions.UserLocation)
                    {
                        case "Australia":
                            switch (StaticFunctions.Department)
                            {
                                case "GoodsIn":
                                    if (Directory.Exists(@"W:\AU Warehouse\Reports"))
                                    {
                                        savePath = @"W:\AU Warehouse\Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else if (Directory.Exists(@"W:\Reports"))
                                    {
                                        savePath = @"W:\Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                   
                                    break;
                                case "TestDept":
                                    if (Directory.Exists(@"W:\AU Warehouse\Nebula\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"W:\AU Warehouse\Nebula\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else if (Directory.Exists(@"W:\Nebula\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"W:\Nebula\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                                case "GoodsOutTech":
                                    if (Directory.Exists(@"W:\AU Warehouse\Nebula\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"W:\AU Warehouse\Nebula\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else if (Directory.Exists(@"W:\Nebula\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"W:\Nebula\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                                case "ITAD":
                                    if (Directory.Exists(@"W:\AU Warehouse\Nebula\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"W:\AU Warehouse\Nebula\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else if (Directory.Exists(@"W:\Nebula\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"W:\Nebula\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                            }
                            break;
                        case "United States":
                            switch (StaticFunctions.Department)
                            {
                                case "GoodsIn":
                                    if (Directory.Exists(@"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else if (Directory.Exists(@"T:\Technical Resources\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"T:\Technical Resources\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }

                                    break;
                                case "TestDept":
                                    if (Directory.Exists(@"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else if (Directory.Exists(@"T:\Technical Resources\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"T:\Technical Resources\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                                case "GoodsOutTech":
                                    if (Directory.Exists(@"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else if (Directory.Exists(@"T:\Technical Resources\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"T:\Technical Resources\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                                case "ITAD":
                                    if (Directory.Exists(@"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"T:\NJ Warehouse\Technical Resources\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else if (Directory.Exists(@"T:\Technical Resources\TB Tools App\Test Reports"))
                                    {
                                        savePath = @"T:\Technical Resources\TB Tools App\Test Reports\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                            }
                            break;
                        case "United Kingdom":
                            switch (StaticFunctions.Department)
                            {
                                case "GoodsIn":

                                  
                                    savePath = vm.GetFolderPath(DateTime.Now.ToShortDateString());

                                    if (Directory.Exists(savePath))
                                    {
                                        //Exists, just drop through
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                                case "TestDept":
                                    savePath = vm.GetFolderPath(DateTime.Now.ToShortDateString());

                                    if (Directory.Exists(savePath))
                                    {
                                        //Exists, just drop through
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                                case "GoodsOutTech":
                                    savePath = vm.GetFolderPath(DateTime.Now.ToShortDateString());

                                    if (Directory.Exists(savePath))
                                    {
                                        //Exists, just drop through
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                                case "ITAD":
                                    savePath = @"M:\Test Reports\";

                                    if (Directory.Exists(savePath))
                                    {
                                        //Exists, just drop through
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                            }
                            break;
                        case "France":
                            switch (StaticFunctions.Department)
                            {
                                case "GoodsIn":
                                    savePath = @"W:\FR Warehouse\Goods In\Test Reports\";

                                    if (Directory.Exists(savePath))
                                    {
                                        //Exists, just drop through
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                                case "TestDept":
                                    savePath = @"W:\FR Warehouse\Goods In\Test Reports\";

                                    if (Directory.Exists(savePath))
                                    {
                                        //Exists, just drop through
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                                case "GoodsOutTech":
                                    savePath = @"W:\FR Warehouse\Goods In\Test Reports\";

                                    if (Directory.Exists(savePath))
                                    {
                                        //Exists, just drop through
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    //savePath = vm.GetGOTFolderPath(DateTime.Now.ToShortDateString(), vm.SONumber);
                                    break;
                                case "ITAD":
                                    savePath = @"W:\FR Warehouse\Goods In\Test Reports\";

                                    if (Directory.Exists(savePath))
                                    {
                                        //Exists, just drop through
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                            }
                            break;
                        case "Germany":
                            switch (StaticFunctions.Department)
                            {
                                case "GoodsIn":
                                    savePath = @"W:\Nebula\TB Tools App\Test Reports\";

                                    if (Directory.Exists(savePath))
                                    {
                                        //Exists, just drop through
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                                case "TestDept":
                                    savePath = @"W:\Nebula\TB Tools App\Test Reports\";

                                    if (Directory.Exists(savePath))
                                    {
                                        //Exists, just drop through
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                                case "GoodsOutTech":
                                    savePath = @"W:\Nebula\TB Tools App\Test Reports\";

                                    if (Directory.Exists(savePath))
                                    {
                                        //Exists, just drop through
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    //savePath = vm.GetGOTFolderPath(DateTime.Now.ToShortDateString(), vm.SONumber);
                                    break;
                                case "ITAD":
                                    savePath = @"W:\Nebula\TB Tools App\Test Reports\";

                                    if (Directory.Exists(savePath))
                                    {
                                        //Exists, just drop through
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Green;
                                    }
                                    else
                                    {
                                        //Save to Users Documents
                                        savePath = @"" + vm.myDocs + @"\";
                                        //Update Message
                                        ReportMSGTB.Foreground = Brushes.Red;
                                        //Update Message
                                        ReportMSGTB.Text += "Network unavailable, File saved locally >>>";
                                    }
                                    break;
                            }
                            break;

                    }

              

                if (vm.SaveToMyDocs == true)
                {
                    //Save to Users Documents
                    savePath = @"" + vm.myDocs + @"\";
                }

             

                DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

                ////Set the serial number to text on the clipboard  Use control v
                if (!string.IsNullOrEmpty(vm.TestSerialNo))
                    Clipboard.SetText(vm.TestSerialNo.Trim(), TextDataFormat.UnicodeText); // "Serial No: " + "-" + vm.ITADDeviceCategory.Replace("Category: ", "") + "-" + vm.ITADProcessPerformed.Replace("Process Performed: ", "").Trim(), TextDataFormat.UnicodeText);

                //Add processed item to List
                //vm.ITADAssetsProcessed.Add(vm.ITADPOSONo.Replace("PO/SO No: ", "") + " - " + vm.ITADAssetNo.Replace("Asset No: ", "") + " - " + vm.ITADSerialNo.Replace("Serial No: ", "") + " - " + DateTime.Now.ToShortDateString() + "_" + DateTime.Now.ToShortTimeString());
                vm.ITADAssetsProcessed.Add(vm.TestSerialNo + "_" + vm.TestPOSONo + "_" + DateTime.Now.ToShortTimeString());
                // ADD PRINT TO PDF AND COPYTING OF ANY ADDITIONAL REPORTS
                //Original
                //dpw.SaveDocAndPrintITAD(Report1);

              


                    //Create a Faulty folder if one doesn't exist
                    if (Directory.Exists(savePath + @"Faulty"))
                    {

                    }
                    else
                    {
                        //If not there create directory 
                        Directory.CreateDirectory(savePath + @"Faulty");
                     
                    }

                    //Set string to combination name
                    string fileURI = vm.TestSerialNo.Trim();

                    //Check if Green Light only and other fields are empty
                    if (GreenLightCB.IsChecked == true && string.IsNullOrWhiteSpace(CLITXT.Text) && vm.ITADAttachedFiles.Count() == 0 || GreenLightCB.IsChecked == true && PrintLabelCKB.IsChecked == true)
                    {
                        //ADD LABEL
                        //Create a Label on the Multi-Print Queue
                        NetsuiteInventory nsi = new NetsuiteInventory();
                        nsi.PrintQuantity = "1";
                        nsi.SerialNumber = vm.TestSerialNo;
                        nsi.PONumber = vm.TestPOSONo;
                        nsi.ReceivedByOperative = vm.ReportUser.Replace("Technician Name:", "").Trim();
                        nsi.TestedByOperative = nsi.ReceivedByOperative;
                        nsi.ProductCode = "Custom";
                        nsi.ProductDescription = "Test Report Label";

                        //If saved to Documents folder add reference
                        if (SaveToMyDocsCKB.IsChecked == true)
                        {

                            //Add the report path to the label object
                            nsi.ReportPath = savePath + fileURI + "   " + dateTimeFN + ".pdf";

                        }
                        else
                        {
                            //Add the report path to the label object
                            nsi.ReportPath = savePath + fileURI + "   " + dateTimeFN + ".pdf";
                        }

                        //ADD A LABEL TO THE QUEUE
                        try
                        {
                            
                            // Find the Listview on separate view model and tap into its data context
                            ListView multiprintqueue = Helper.FindChild(Application.Current.MainWindow, "MultiPrintLV") as ListView;

                            if (multiprintqueue != null)
                            {
                                // pull in the current datacontext
                                MainViewModel mvm = (MainViewModel)multiprintqueue.DataContext;
                                // add to the referenced main view model property
                                mvm.NetsuiteMultiPrint.Add(nsi);
                                //END ADD LABEL
                                //Add to Observable Collection to be used to add to database
                                customLabels.Add(nsi);
                                //LABEL DB INSERT
                                //Insert to Labels DB using Transaction
                                await System.Threading.Tasks.Task.Run(() => InsertCustomLabelsDB(customLabels));
                            }
                         
                        }
                        catch (Exception ex)
                        {
                            //Launch custom dialog Standard, YesNo, YesNoCancel for dialogtype
                            vm.SystemMessageDialog("Test Reporting", ex.Message);
                        }


                        //*** Create a PDF document ***

                        //Create the PDF from the Flow Document and save to a fixed path.
                        dpw.FlowDocument2Pdf(Report1, savePath + fileURI + "   " + dateTimeFN + ".pdf");

                       
                    }
                    else
                    {

                        //ADD LABEL
                        //Create a Label on the Multi-Print Queue
                        NetsuiteInventory nsi = new NetsuiteInventory();
                        nsi.PrintQuantity = "1";
                        nsi.SerialNumber = vm.TestSerialNo;
                        nsi.PONumber = vm.TestPOSONo;
                        nsi.ReceivedByOperative = vm.CurrentUser.Replace("Technician Name:", "").Trim();
                        nsi.ProductCode = "Custom";
                        nsi.ProductDescription = "Test Report Label";

                        if (FaultyCB.IsChecked == true)
                        {

                            //Add the report path to the label object
                            nsi.ReportPath = savePath + @"Faulty\" + fileURI + "   " + dateTimeFN + ".pdf";
                            
                        }
                        else
                        {
                            //Add the report path to the label object
                            nsi.ReportPath = savePath + fileURI + "   " + dateTimeFN + ".pdf";
                        }


                        ////ADD A LABEL TO THE QUEUE
                        //try
                        //{
                        //    // Find the Listview on separate view model and tap into its data context
                        //    ListView multiprintqueue = Helper.FindChild(Application.Current.MainWindow, "MultiPrintLV") as ListView;
                        //    // pull in the current datacontext
                        //    MainViewModel mvm = (MainViewModel)multiprintqueue.DataContext;
                        //    // add to the referenced main view model property
                        //    mvm.NetsuiteMultiPrint.Add(nsi);
                        //    //END ADD LABEL
                        //}
                        //catch (Exception ex)
                        //{
                        //    //Launch custom dialog
                        //    vm.SystemMessageDialog("Test Reporting", ex.Message);
                        //}




                        //*** Create a PDF document ***

                        //Create the PDF from the Flow Document and save to a fixed path.
                        dpw.FlowDocument2Pdf(Report1, vm.myDocs + vm.TestSerialNo.Trim() + ".pdf");

                     


                        //Add list of string to hold additional reports
                        List<string> combinePDF = new List<string>();


                        //Add first flowdoc to list
                        combinePDF.Add(vm.myDocs + fileURI + ".pdf");


                        //Save Any Additional Attatchments to the same file path
                        int numAtt = 1;

                        foreach (string report in vm.ITADAttachedFiles)
                        {
                            numAtt += 1;


                            if (File.Exists(report))
                            {
                                //Get File Extension
                                string ext = System.IO.Path.GetExtension(report);


                                if (ext != ".pdf")
                                {

                                    //image file
                                    // Convert to PDF and delete image
                                    PDFHelper.Instance.SaveImageAsPdf(report, vm.myDocs + fileURI + numAtt.ToString() + ".pdf", 600, false);
                                    combinePDF.Add(vm.myDocs + fileURI + numAtt.ToString() + ".pdf");
                                }
                                else if (ext == ".pdf")
                                {

                                    if (File.Exists(vm.myDocs + fileURI + numAtt.ToString() + ".pdf"))
                                    {
                                        File.Delete(vm.myDocs + fileURI + numAtt.ToString() + ".pdf");

                                        //If PDF Copy to save location
                                        File.Copy(report, vm.myDocs + fileURI + numAtt.ToString() + ".pdf");
                                    }
                                    else
                                    {
                                        //If PDF Copy to save location
                                        File.Copy(report, vm.myDocs + fileURI + numAtt.ToString() + ".pdf");
                                    }



                                    //combinePDF.Add(report);
                                    combinePDF.Add(vm.myDocs + fileURI + numAtt.ToString() + ".pdf");
                                }


                                //Launch File
                                //System.Diagnostics.Process.Start(savePath + fileURI + numAtt.ToString() + ext.Trim());
                            }

                        }

                        //Check if item faulty, if so save into a separate folder
                        if (FaultyCB.IsChecked == true)
                        {

                            //Combine all pdf's in list
                            PDFHelper.CombinePDF(combinePDF, savePath + @"Faulty\" + fileURI + "   " + dateTimeFN + ".pdf");
                        }
                        else
                        {
                            //Combine all pdf's in list
                            PDFHelper.CombinePDF(combinePDF, savePath + fileURI + "   " + dateTimeFN + ".pdf");
                        }



                        //Delete temp files

                        foreach (var tfile in combinePDF)
                        {

                            if (tfile.Contains(vm.myDocs + fileURI + "   " + dateTimeFN + ".pdf"))
                            {
                                //skip first file
                            }
                            else
                            {
                                //delete file
                                if (File.Exists(tfile))
                                {
                                    //delete temp file
                                    File.Delete(tfile);


                                }
                            }
                        }


                        //Check if item faulty, if so save into a separate folder
                        if (FaultyCB.IsChecked == true)
                        {
                            //MessageBox.Show();

                            //Launch Combined PDF File
                            System.Diagnostics.Process.Start(savePath + @"Faulty\" + fileURI + "   " + dateTimeFN + ".pdf");
                        }
                        else
                        {
                            //Launch Combined PDF File
                            System.Diagnostics.Process.Start(savePath + fileURI + "   " + dateTimeFN + ".pdf");
                        }

                    } // end green light label check


      



                    //Refresh Current User
                    vm.ReportUser = "Technician Name: " + vm.CurrentUserInitials + " " + DateTime.Now.ToShortDateString();
                    //ADD DB ENTRY HERE
                    //CreateTable(sqlite_conn);
                    //insert all items in table to DB
                    InsertData();

                    //Hide progress
                    //  ProgressBar.IsIndeterminate = false;





                    //Check if reprint or not. Property resetting based off this
                if (vm.ITADIsReprint == true)
                {

                        //Check if item faulty, if so save into a separate folder
                        if (FaultyCB.IsChecked == true)
                        {
                            if (GreenLightCB.IsChecked == true && string.IsNullOrWhiteSpace(CLITXT.Text) && vm.ITADAttachedFiles.Count() == 0 || GreenLightCB.IsChecked == true && PrintLabelCKB.IsChecked == true)
                            {
                                //Update Message "Label Created and added to the Multi Print option!\n Please open Label Printing from the main menu."
                                ReportMSGTB.Text = App.Current.FindResource("TestRepGBLabelAddedMessage").ToString();
                            }
                            else
                            {
                                //Update Message
                                //ReportMSGTB.Text = "File Saved >>>" + savePath + @"Faulty\" + vm.TestSerialNo + "   " + dateTimeFN + ".pdf";
                            }

                            
                        }
                        else
                        {

                            if (GreenLightCB.IsChecked == true && string.IsNullOrWhiteSpace(CLITXT.Text) && vm.ITADAttachedFiles.Count() == 0 || GreenLightCB.IsChecked == true && PrintLabelCKB.IsChecked == true)
                            {
                                //Update Message "Label Created and added to the Multi Print option!\n Please open Label Printing from the main menu."
                                ReportMSGTB.Text = App.Current.FindResource("TestRepGBLabelAddedMessage").ToString();
                            }
                            else
                            {
                                //Update Message
                                //ReportMSGTB.Text = "File Saved >>>" + savePath + vm.TestSerialNo + "   " + dateTimeFN + ".pdf";
                            }
                          
                        }

                    //RePrint
                    vm.CurrentUser = "";
                    // Reset certain fields
                    //AssetNoTXT.Text = "";
                    SerialNoTXT.Text = "";
                    SerialOverrrideTXTB.Text = "";
                    vm.ITADAttachedFiles.Clear();
                    //Reset VM Properties
                    vm.TestSerialNo = "";
                    vm.ITADAssetNo = "";
                    vm.ITADNotes = "";
                    vm.TestPOSONo = "";
                    vm.TestCLIOutput = "";
                    vm.SaveToMyDocs = false;
                    PrintLabelCKB.IsChecked = false;
                    GreenLightCB.IsChecked = false;
                    FaultyCB.IsChecked = false;
                    HardResetCB.IsChecked = false;
                    }
                else
                {

                        //Check if item faulty, if so save into a separate folder
                        if (FaultyCB.IsChecked == true)
                        {
                            if (GreenLightCB.IsChecked == true && string.IsNullOrWhiteSpace(CLITXT.Text) && vm.ITADAttachedFiles.Count() == 0 || GreenLightCB.IsChecked == true && PrintLabelCKB.IsChecked == true)
                            {
                                //Update Message
                                ReportMSGTB.Text = "Label Created and added to the Multi Print option! Please open the Label Printing to access.";
                            }
                            else
                            {
                                //Update Message
                                ReportMSGTB.Text += savePath + @"Faulty\" + vm.TestSerialNo + "   " + dateTimeFN + ".pdf";
                            }


                        }
                        else
                        {

                            if (GreenLightCB.IsChecked == true && string.IsNullOrWhiteSpace(CLITXT.Text) && vm.ITADAttachedFiles.Count() == 0 || GreenLightCB.IsChecked == true && PrintLabelCKB.IsChecked == true)
                            {
                                //Update Message
                                ReportMSGTB.Text = "Label Created and added to the Multi Print option! Please open the Label Printing to access.";
                            }
                            else
                            {
                                //Update Message
                                ReportMSGTB.Text += savePath + vm.TestSerialNo + "   " + dateTimeFN + ".pdf";
                            }

                        }
                     
                   
                    //Not a Reprint
                    vm.CurrentUser = "";
                    // Reset certain fields
                    SerialNoTXT.Text = "";
                    //AssetNoTXT.Text = "";
                    SerialOverrrideTXTB.Text = "";
                    vm.ITADAttachedFiles.Clear();
                    //Reset VM Properties
                    vm.TestSerialNo = "";
                    vm.ITADAssetNo = "";
                    vm.ITADNotes = "";
                    //vm.TestPOSONo = "";
                    vm.TestCLIOutput = "";
                    vm.SaveToMyDocs = false;
                    PrintLabelCKB.IsChecked = false;
                    GreenLightCB.IsChecked = false;
                    FaultyCB.IsChecked = false;
                    HardResetCB.IsChecked = false;
                    }

              }

                //Reset image previews
                ImagePreviews.Children.Clear();
                //Set back to disabled
                PrintBTN.IsEnabled = false;

                //Generate QR
                GenerateLabel();

            }
            catch (Exception ex)
            {
                ReportMSGTB.Text = ex.Message;
                Console.WriteLine(ex.Message);
            }

        }


        // BEST METHOD TO USE

        private String filterContent(String content)
        {
            return content.Replace("[^\\u0009\\u000a\\u000d\\u0000-\\uD7FF\\uE000-\\uFFFD]", "");
        }


    

        public DocumentPaginator PrintFlowDoc(FlowDocument FlowDoc)
        {
            try
            {



                //Clone the source document
                var str = XamlWriter.Save(FlowDoc);
                str.Replace("\u0000", "");
                str.Replace("\u0001", "");
                str.Replace("\u0002", "");
                //remove any invalid chars before passing
                var stringReader = new System.IO.StringReader(filterContent(str));
                var xmlReader = XmlReader.Create(stringReader);
                var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

                // get information about the dimensions of the seleted printer+media.
                System.Printing.PrintDocumentImageableArea ia = null;

                // passes in the printqueue CreateXPSDocumentWriter that calls upon a standar print dialog 
                System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);


                if (docWriter != null && ia != null)
                {
                    DocumentPaginator paginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;

                    // Change the PageSize and PagePadding for the document to match the CanvasSize for the printer device.
                    paginator.PageSize = new System.Windows.Size(ia.MediaSizeWidth, ia.MediaSizeHeight);
                    Thickness t = new Thickness(72);  // copy.PagePadding;
                    CloneDoc.PagePadding = new Thickness(
                                     Math.Max(ia.OriginWidth, t.Left),
                                       Math.Max(ia.OriginHeight, t.Top),
                                       Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
                                       Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));



                    CloneDoc.ColumnWidth = double.PositiveInfinity;

                    return paginator;

                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);

            }
            return null;
        }



        private void ToggleSwitch_Checked(object sender, RoutedEventArgs e)
        {
            if (LogoHeadBRD.Visibility == Visibility.Collapsed)
            {
                //Display The Logo
                LogoHeadBRD.Visibility = Visibility.Visible;
                FooterTB.Visibility = Visibility.Visible;
            }
        }

        private void ToggleSwitch_Unchecked(object sender, RoutedEventArgs e)
        {
            if (LogoHeadBRD.Visibility == Visibility.Visible)
            {
                //Display The Logo
                LogoHeadBRD.Visibility = Visibility.Collapsed;
                FooterTB.Visibility = Visibility.Collapsed;
            }
        }

        //Bool to detect if enter was pressed by user
        public bool enterPressed;

        private void CLITXT_TextChanged(object sender, TextChangedEventArgs e)
        {
           

            if (CLITXT.Text != string.Empty && e.Changes.FirstOrDefault().AddedLength > 1)
            {
                               

                if (enterPressed == true)
                {
                    //Skip Formatter
                    //Run formatter
                    //CLITXT.Text = CleanReportFormatting(CLITXT.Text);
                    ExtractSerialNumber(CLITXT.Text);

                }
                else
                {
                    //Run formatter
                    CLITXT.Text = CleanReportFormatting(CLITXT.Text);
                    ExtractSerialNumber(CLITXT.Text);
                    //reset to false
                    enterPressed = false;
                }
            }

            //Set 
            if (!string.IsNullOrEmpty(vm.TestCLIOutput) && vm.ITADAttachedFiles.Count > 0)
            {
                //Set Page break to separate Image Previews & cli in the created pdf. See the paragraph prior to the cli field in the flowdocument
                PageBreakParagraph.BreakPageBefore = true;
            }
            else
            {
                //Set Page break to separate Image Previews & cli in the created pdf. See the paragraph prior to the cli field in the flowdocument
                PageBreakParagraph.BreakPageBefore = false;
            }


        }

        //if enter pressed set to true
        private void CLITXT_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                enterPressed = true;
            }
           
        }

        private void CLITXT_LostFocus(object sender, RoutedEventArgs e)
        {
            //Check for CISCO ILET ERROR
            if (CLITXT.Text.Contains("%ILET-1-AUTHENTICATION_FAIL") || CLITXT.Text.Contains("ILET-1-AUTHENTICATION_FAIL"))
            {
                //Set Faulty to checked
                FaultyCB.IsChecked = true;
                //Set Text to show error
                CLITXT.Text = "Cisco %ILET Error detected! Please check the devices authenticity!";
                //Launch Dialog
                vm.DialogMessage("Error detected in CLI Output", "ILET Error DETECTED, Please check device authenticity!");
              

            }

        }


        public void ExtractSerialNumber(string report)
        {
            try
            {


                //check product brand is populated


                //for Cisco Capture the Serial Number from the CLI 


                if (report.Contains("Processor board ID"))
                {
                    var myint = report.IndexOf(" ID ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());

                    string serialExtract = report.Substring(myint, 16).Replace(",", "").Replace("ID", "").Trim();
                    myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;

                }

                if (report.Contains("Top Assembly Serial Number"))
                {
                    var myint = report.IndexOf("Top Assembly Serial Number           :", 0, StringComparison.OrdinalIgnoreCase);

                    // MessageBox.Show(myint.ToString());
                    string serialExtract = report.Substring(myint + 30, 22).Replace(",", "").Replace(":", "").Trim();
                    myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;

                }


                if (report.Contains("FC (1 Slot) Chassis"))
                {
                    var myint = report.IndexOf("SN: ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());
                    string serialExtract = report.Substring(myint, 16).Replace(",", "").Replace(":", "").Trim();
                    myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;

                }






                //for Mellanox Capture the Serial Number from the CLI 
                if (report.Contains("CHASSIS"))
                {
                    var myint = report.IndexOf("-", 0, StringComparison.OrdinalIgnoreCase) + 4;

                    //MessageBox.Show(myint.ToString());

                    string serialExtract = report.Substring(myint, 21).Replace(",", "").Trim();
                    //myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;

                }







                //for Cisco Capture the Serial Number from the CLI 
                if (report.Contains("Serial Number") && report.Contains("Cisco"))
                {
                    var myint = report.IndexOf(" Serial Number ", 0, StringComparison.OrdinalIgnoreCase);
                    string serialExtract = "";

                    serialExtract = report.Substring(myint, 34).Replace(":", "").Trim();

                    int startPos = serialExtract.IndexOf("Serial Number ");
                    int length = serialExtract.LastIndexOf("\n");


                    serialExtract = serialExtract.Substring(startPos, length).Replace("Serial Number ", "").Replace(":", "").Trim();


                    vm.TestSerialNo = serialExtract;


                    return;

                }


                //for HP Capture the Serial Number from the CLI  && report.Contains("HP-") || report.Contains("ProCurve")
                if (report.Contains("Serial Number") && report.Contains("HP-") || report.Contains("ProCurve"))
                {
                    var myint = report.IndexOf(" Serial Number ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());// Serial Number      : CN95FP90L4

                    string serialExtract = report.Substring(myint, 29).Replace("Serial Number", "").Replace(":", "").Trim();
                    //MessageBox.Show(myint.ToString());
                    myint = serialExtract.IndexOf(":", 0);
                    vm.TestSerialNo = serialExtract;

                    return;

                }



                if (report.Contains("DEVICE_SERIAL_NUMBER"))
                {
                    var myint = report.IndexOf("DEVICE_SERIAL_NUMBER", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());

                    string serialExtract = report.Substring(myint, 34).Replace("DEVICE_SERIAL_NUMBER", "").Replace(":", "").Trim();
                    //MessageBox.Show(myint.ToString());
                    myint = serialExtract.IndexOf(":", 0);
                    if (serialExtract.Contains("MAC_ADD"))
                    {

                        //PULL IN THE SERIAL NUMBER INSTEAD
                        serialExtract = report.Substring(myint, 34).Replace("Serial Number", "").Replace(":", "").Trim();
                        //MessageBox.Show(myint.ToString());
                        myint = serialExtract.IndexOf(":", 0);
                        vm.TestSerialNo = serialExtract;
                    }
                    else
                    {
                        vm.TestSerialNo = serialExtract;
                    }
                    return;
                }
                // Console.WriteLine("HP");


               


                // Console.WriteLine("Dell");
                if (report.Contains("Service Tag:"))
                {
                    var myint = report.IndexOf("Service Tag:", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());Serial number     Description

                    string serialExtract = report.Substring(myint, 20).Replace("Service Tag:", "").Trim();
                    //myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;

                    return;

                }

                // Console.WriteLine("netgear");
                if (report.Contains("Serial number") && report.Contains("Juniper"))
                {
                    var myint = report.IndexOf(" Serial number ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());Serial number     Description

                    string serialExtract = report.Substring(myint, 89).Replace(",", "").Replace("Chassis", "").Replace("Serial number", "").Replace("Description", "").Trim();
                    //myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;
                    return;
                }

                // Console.WriteLine("netgear");
                if (report.Contains("Serial number") && report.Contains("Netgear"))
                {
                    var myint = report.IndexOf(" Serial number ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());Serial number     Description

                    string serialExtract = report.Substring(myint, 89).Replace(",", "").Replace("Chassis", "").Replace("Serial number", "").Replace("Description", "").Trim();
                    //myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;
                    return;
                }

                // Console.WriteLine("netgear");
                if (report.Contains("Serial number") && report.Contains("IBM"))
                {
                    var myint = report.IndexOf(" Serial number ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());Serial number     Description

                    string serialExtract = report.Substring(myint, 89).Replace(",", "").Replace("Chassis", "").Replace("Serial number", "").Replace("Description", "").Trim();
                    //myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;
                    return;
                }

                // Console.WriteLine("netgear");
                if (report.Contains("Serial number") && report.Contains("Draytek"))
                {
                    var myint = report.IndexOf(" Serial number ", 0, StringComparison.OrdinalIgnoreCase);

                    //MessageBox.Show(myint.ToString());Serial number     Description

                    string serialExtract = report.Substring(myint, 89).Replace(",", "").Replace("Chassis", "").Replace("Serial number", "").Replace("Description", "").Trim();
                    //myint = serialExtract.IndexOf(",", 0);
                    vm.TestSerialNo = serialExtract;
                    return;
                }




            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);

            }
        }

        public string CleanReportFormatting(string report)
        {
            //try
            //{


            //STRING BUILDER READS THROUGH REPORT STRING AND CLEANS UP SEPARATING ONTO SEPARATE LINES
            StringBuilder sb1 = new StringBuilder();

            using (StringReader reader = new StringReader(report))
            {
                //int i = 1; 

                string line;
                while ((line = reader.ReadLine()) != null)
                {


                    if (line == String.Empty || line == "" || line.EndsWith(">") || line.EndsWith("#"))
                    {

                    }
                    else
                    {
                        //workaround for cisco reports
                        if (line.Contains("Sig#"))
                        {
                            sb1.AppendLine(line.Trim());
                        }
                        else if (line.Contains("#") && line.EndsWith("#") != true)
                        {
                            // MessageBox.Show("Cert Not Here");
                            sb1.AppendLine("\r\r" + line.Trim() + "\r");

                        }
                        else
                        {
                            sb1.AppendLine(line.Trim());
                        }

                    }


                }
            }
            return sb1.ToString();
            //}
            //catch (Exception ex)
            //{
            //    return sb1.ToString();
            //    //Console.WriteLine(ex.Message);
            //}
        }

        private void AddReportsBTN_Click(object sender, RoutedEventArgs e)
        {
            //Clear collection
            vm.ITADAttachedFiles.Clear();
            //Reset image previews
            ImagePreviews.Children.Clear();

            //Set Page break to separate Image Previews & cli in the created pdf. See the paragraph prior to the cli field in the flowdocument
            PageBreakParagraph.BreakPageBefore = false;

            //Set back to disabled
            //PrintBTN.IsEnabled = false;



            using (System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog())
            {


                openFileDialog.Filter = "PDF & Image Files|*.pdf;*.jpg;*.jpeg;*.png;*.gif;*.tif;...";


                openFileDialog.FilterIndex = 0;
                openFileDialog.Multiselect = true;
                openFileDialog.RestoreDirectory = true;


                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {

                    // Loop through how ever many items are selected and add to the list
                    foreach (var file in openFileDialog.FileNames)
                    {
                        if (File.Exists(file))
                        {

                            //int lastSlash = file.LastIndexOf(@"\");

                            vm.ITADAttachedFiles.Add(file); //file.Substring(lastSlash + 1);

                        }
                    }

                }

                //Generate QR's
                GenerateLabel();

                //Refresh Current User
                vm.ReportUser = "Technician Name: " + vm.CurrentUserInitials + " " + DateTime.Now.ToShortDateString();

                //Add preview images to front page
                CreatePreviews();

                if (!string.IsNullOrEmpty(vm.TestCLIOutput) && vm.ITADAttachedFiles.Count > 0)
                {
                    //Set Page break to separate Image Previews & cli in the created pdf. See the paragraph prior to the cli field in the flowdocument
                    PageBreakParagraph.BreakPageBefore = true;
                }
                else
                {
                    //Set Page break to separate Image Previews & cli in the created pdf. See the paragraph prior to the cli field in the flowdocument
                    PageBreakParagraph.BreakPageBefore = false;
                }
           

            }

        }



        public void CreatePreviews()
        {
            //Update Previews string.IsNullOrWhiteSpace(CLITXT.Text) && 
            if (vm.ITADAttachedFiles.Count > 0)
            {


                if (vm.ITADAttachedFiles.FirstOrDefault().ToLower().Contains(".png") || vm.ITADAttachedFiles.FirstOrDefault().ToLower().Contains(".jpg") || vm.ITADAttachedFiles.FirstOrDefault().ToLower().Contains(".bmp") || vm.ITADAttachedFiles.FirstOrDefault().ToLower().Contains(".jpeg"))
                {
                    ImagePreviews.Children.Clear();

                    WrapPanel wp = new WrapPanel();
                    wp.HorizontalAlignment = HorizontalAlignment.Center;
                    wp.VerticalAlignment = VerticalAlignment.Center;
                    wp.Children.Clear();

                    int attachCount = 0;
                    //Loop through attached list 
                    foreach (var attachment in vm.ITADAttachedFiles)
                    {
                        //Image file add C:\Users\n.myers\Pictures\SplashScreenV2.png
                        BitmapImage bmi = new BitmapImage();
                        bmi.BeginInit();
                        bmi.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                        //vm.ImageAttach1.Rotation = Rotation.Rotate90;
                        //*** Required not to lock file ***
                        bmi.CacheOption = BitmapCacheOption.OnLoad;
                        //bmi.Palette.Colors = ColorMode.
                        bmi.UriSource = new Uri(attachment, UriKind.Absolute);
                        bmi.EndInit();

                        System.Windows.Controls.Image img = new System.Windows.Controls.Image();

                        //Add Size adjustment to preview images

                        switch (vm.ITADAttachedFiles.Count)
                        {
                            case 1:
                                img.Width = 500;
                                img.Height = 250;
                                break;
                            case 2:
                                img.Width = 300;
                                img.Height = 150;
                                break;
                            case 3:
                                img.Width = 200;
                                img.Height = 100;
                                break;
                            case 4:
                                img.Width = 200;
                                img.Height = 100;
                                break;
                            default:
                                img.Width = 200;
                                img.Height = 100;
                                break;
                        }

                        //Increase the count
                        attachCount += 1;

                        StackPanel sp = new StackPanel();

                        TextBlock tbpage = new TextBlock();
                        tbpage.HorizontalAlignment = HorizontalAlignment.Center;
                        tbpage.Foreground = Brushes.White;
                        tbpage.Background = Brushes.Green;
                        tbpage.Padding = new Thickness(4);
                        tbpage.FontFamily = new FontFamily("Calibri");
                        tbpage.Text = "Preview Page " + attachCount.ToString();
                        tbpage.FontSize = 12;

                        //Add page description
                        sp.Children.Add(tbpage);
                        img.Source = bmi;
                        //Set Rendering Attached Property options
                        RenderOptions.SetBitmapScalingMode(img, BitmapScalingMode.Fant);
                        RenderOptions.SetClearTypeHint(img, ClearTypeHint.Enabled);
                        img.Margin = new Thickness(10);
                        img.HorizontalAlignment = HorizontalAlignment.Center;
                        img.Margin = new Thickness(20);
                        //Add Image as 2nd child
                        sp.Children.Add(img);

                        //Add Stack Panel to Wrap Panel
                        wp.Children.Add(sp);
                    }

                    //add textblock to top
                    TextBlock tb = new TextBlock();
                    tb.HorizontalAlignment = HorizontalAlignment.Center;
                    tb.VerticalAlignment = VerticalAlignment.Center;
                    tb.TextAlignment = TextAlignment.Center;
                    tb.TextWrapping = TextWrapping.Wrap;
                    tb.Foreground = Brushes.Green;
                    //tb.Background = Brushes.Green;
                    tb.FontWeight = FontWeights.Bold;
                    tb.FontFamily = new FontFamily("Calibri");
                    tb.Text = App.Current.FindResource("TestRepGBImagePreviewText").ToString(); //  "To reduce paper waste, this report contains screenshot previews.\n The full digital report is available on request.";
                    tb.FontSize = 16;
                    tb.Margin = new Thickness(0, 20, 0, 20);
                    tb.Padding = new Thickness(4);

                    //Add Wrap panel and Textblock
                    ImagePreviews.Children.Add(tb);
                    ImagePreviews.Children.Add(wp);


                    ////Image file add C:\Users\n.myers\Pictures\SplashScreenV2.png
                    //vm.ImageAttach1 = new BitmapImage();

                    //vm.ImageAttach1.BeginInit();
                    //vm.ImageAttach1.Rotation = Rotation.Rotate90;
                    ////*** Required not to lock file ***
                    //vm.ImageAttach1.CacheOption = BitmapCacheOption.OnLoad;
                    //vm.ImageAttach1.UriSource = new Uri(vm.ITADAttachedFiles.FirstOrDefault().ToString(), UriKind.Absolute);
                    //vm.ImageAttach1.EndInit();

                    // vm.ITADAttachedFiles.Remove(vm.ITADAttachedFiles.FirstOrDefault().ToString());
                }


            }
        }



        private void AddProcessBTN_Click(object sender, RoutedEventArgs e)
        {
            if (vm.ITADAddProcess == "")
            {
                MessageBox.Show("Please enter a value in the Erasure Process Text Box.");
            }
            else
            {
                //insert all items in table to DB
                InsertProcess();
                vm.ITADAddProcess = "";
                vm.ITADProcessesCollection.Clear();
                //Load Any Processes
                ReadProcess("SELECT * FROM ProcessPerformed ORDER BY Process");
            }
        }

        private void AddTestingBTN_Click(object sender, RoutedEventArgs e)
        {
            if (vm.ITADAddTesting == "")
            {
                MessageBox.Show("Please enter a value in the Testing Process Text Box.");
            }
            else
            {
                //insert all items in table to DB
                InsertTestingProcess();
                vm.ITADAddTesting = "";
                vm.ITADTestingCollection.Clear();
                //Load Any Processes
                ReadTestingProcess("SELECT * FROM TestingPerformed ORDER BY Process");
            }
        }

        private void AddCategoryBTN_Click(object sender, RoutedEventArgs e)
        {
            if (vm.ITADAddCategory == "")
            {
                MessageBox.Show("Please enter a value in the Category Text Box.");
            }
            else
            {
                //insert all items in table to DB
                InsertCategory();
                vm.ITADAddCategory = "";
                vm.ITADCategoryCollection.Clear();
                //Load Any Categories
                ReadCategory("SELECT * FROM DeviceCategories ORDER BY Category");
            }

        }



        private void FindBTNS_Click(object sender, RoutedEventArgs e)
        {





            //IF SEARCH FIELD FULL USE NORMAL FILTER SEARCH, ELSE USE THE DATE RANGE SELECTED
            if (SearchTXTB.Text != string.Empty)
            {

                //Assign control text to viewmodel property
                vm.ReportingSearchText = SearchTXTB.Text;
                //Feed in view model search text
                FilterSearch(vm.ReportingSearchText, "");


            }
            else
            {

                FilterDateSearch("", "");

            }





        }


        private async void GenReportBTN_Click(object sender, RoutedEventArgs e)
        {
            //Clear the collection
            vm.ITADReportCollection.Clear();

            //GET DATE VALUES SELECTED ON THE DATEPICKERS
            var dateFrom = vm.DateFrom.ToString("yyyy-MM-dd");
            var dateTo = vm.DateTo.AddDays(1).ToString("yyyy-MM-dd");

            //return all items in date range
            //Get Results with filter
            await Task.Run(() => ReadBackgroundData("SELECT * FROM ReportRecords WHERE DateCreated BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ORDER BY  POSONumber ASC"));

            //Create Async Task for long running process
            GenerateDriveReport(ReportLV, vm.DateFrom.ToShortDateString() + " - " + vm.DateTo.ToShortDateString());

           
        }







        public async Task ReadBackgroundData(string sqlquery)
        {
            try
            {

                //Create connection
                sqlite_conn = CreateConnection(cs);

                //Declare observable collection

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();


                //Create Flow Doc


                //Loop through results set
                while (sqlite_datareader.Read())
                {
               

                    ITADReport itm = new ITADReport();
                    if (sqlite_datareader[0].GetType() != typeof(DBNull))
                        itm.InternalID = sqlite_datareader.GetInt32(0);
                    if (sqlite_datareader[1].GetType() != typeof(DBNull))
                    {

                        string date = sqlite_datareader.GetString(1);
                        itm.DateCreated = Convert.ToDateTime(date);

                    }

                    if (sqlite_datareader[2].GetType() != typeof(DBNull))
                        itm.POSONumber = sqlite_datareader.GetString(2);
                    if (sqlite_datareader[3].GetType() != typeof(DBNull))
                        itm.AssetNo = sqlite_datareader.GetString(3);
                    if (sqlite_datareader[4].GetType() != typeof(DBNull))
                        itm.SerialNo = sqlite_datareader.GetString(4);
                    if (sqlite_datareader[5].GetType() != typeof(DBNull))
                        itm.Manufacturer = sqlite_datareader.GetString(5);
                    if (sqlite_datareader[6].GetType() != typeof(DBNull))
                        itm.ModelNo = sqlite_datareader.GetString(6);
                    if (sqlite_datareader[7].GetType() != typeof(DBNull))
                        itm.DeviceCategory = sqlite_datareader.GetString(7);
                    if (sqlite_datareader[8].GetType() != typeof(DBNull))
                        itm.ProcessPerformed = sqlite_datareader.GetString(8);
                    if (sqlite_datareader[9].GetType() != typeof(DBNull))
                        itm.TestingPerformed = sqlite_datareader.GetString(9);
                    if (sqlite_datareader[10].GetType() != typeof(DBNull))
                        itm.Notes = sqlite_datareader.GetString(10);
                    if (sqlite_datareader[11].GetType() != typeof(DBNull))
                        itm.ReportBody = sqlite_datareader.GetString(11);
                    if (sqlite_datareader[12].GetType() != typeof(DBNull))
                        itm.SavePath = sqlite_datareader.GetString(12);
                    if (sqlite_datareader[13].GetType() != typeof(DBNull))
                        itm.TestedBy = sqlite_datareader.GetString(13);



                    //Update Collection via dispatcher because async
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        //Add item to collection
                        vm.ITADReportCollection.Add(itm);

                        // If collection changes Count
                        vm.RecordsTotal = ReportLV.Items.Count.ToString();

                    });

                 
                }

                //Close connection
                sqlite_conn.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //return null;

            }

            //return null;
        }



        public async void GenerateDriveReport(ListView lvValue, string daterange)
        {


            //Create new Flow Document Async
            FlowDocument ReportFD = new FlowDocument();
            Section MainSection = new Section();
            MainSection.Padding = new Thickness(2);
            MainSection.Margin = new Thickness(0);
            Section mySectionReportHeader = new Section();
            mySectionReportHeader.Background = (SolidColorBrush)new BrushConverter().ConvertFromString("#0f89bd");  //(Brush)bc.ConvertFrom("#CC119EDA");



            Paragraph ReportHeader = new Paragraph(new Run("Serial Testing Breakdown"));
            ReportHeader.Padding = new Thickness(0, 5, 0, 5);
            ReportHeader.FontSize = 24;
            ReportHeader.FontFamily = new FontFamily("Segoe UI");
            ReportHeader.Foreground = Brushes.White;
            ReportHeader.TextAlignment = TextAlignment.Center;
            ReportHeader.Margin = new Thickness(0, 5, 0, 5);

            //Report date or title header
            Paragraph ReportDateHeader = new Paragraph();
            ReportDateHeader = new Paragraph(new Run("Total Items " + vm.ITADReportCollection.Count().ToString()));
            ReportDateHeader.Padding = new Thickness(0, 1, 0, 1);
            ReportDateHeader.FontFamily = new FontFamily("Segoe UI");
            ReportDateHeader.FontSize = 20;
            ReportDateHeader.Foreground = Brushes.White;
            ReportDateHeader.TextAlignment = TextAlignment.Center;
            ReportDateHeader.Margin = new Thickness(0, 1, 0, 1);

            //Report date or title header
            Paragraph ReportPOHeader = new Paragraph();
            ReportPOHeader = new Paragraph(new Run("Total Orders " + vm.ITADReportCollection.Select(o => o.POSONumber.Trim()).Distinct().Count().ToString()));
            ReportPOHeader.Padding = new Thickness(0, 1, 0, 1);
            ReportPOHeader.FontFamily = new FontFamily("Segoe UI");
            ReportPOHeader.FontSize = 20;
            ReportPOHeader.Foreground = Brushes.White;
            ReportPOHeader.TextAlignment = TextAlignment.Center;
            ReportPOHeader.Margin = new Thickness(0, 1, 0, 1);

            Paragraph ReportDate = new Paragraph();
            ReportDate = new Paragraph(new Run(daterange));
            ReportDate.Padding = new Thickness(0, 1, 0, 1);
            ReportDate.FontFamily = new FontFamily("Segoe UI");
            ReportDate.FontSize = 16;
            ReportDate.Foreground = Brushes.White;
            ReportDate.TextAlignment = TextAlignment.Center;
            ReportDate.Margin = new Thickness(0, 1, 0, 1);




            //IMAGE NOT WORKING ON DIFFRENT THREADS
            //Add TB Logo
            //Create Block ui container and add image as a child
            BlockUIContainer blockUIImage = new BlockUIContainer();
            blockUIImage.Padding = new Thickness(10);
            blockUIImage.Margin = new Thickness(10);
            BitmapImage bmp0 = new BitmapImage();

            bmp0.BeginInit();
            bmp0.UriSource = new Uri("pack://application:,,,/Images/TechBuyerLogoCenteredReports.png", UriKind.Absolute);
            bmp0.CacheOption = BitmapCacheOption.OnLoad;
            bmp0.EndInit();
            bmp0.Freeze();

            System.Windows.Controls.Image img0 = new System.Windows.Controls.Image();

            img0.Source = bmp0;
            img0.Width = 150;
            img0.Height = 75;

            blockUIImage.Child = img0;
            //Add image to section
            mySectionReportHeader.Blocks.Add(blockUIImage);

            //IMAGE NOT WORKING ON DIFFRENT THREADS

            //Add Paragraphs to Header
            mySectionReportHeader.Blocks.Add(ReportHeader);
            mySectionReportHeader.Blocks.Add(ReportDateHeader);
            mySectionReportHeader.Blocks.Add(ReportPOHeader);
            mySectionReportHeader.Blocks.Add(ReportDate);


            //Operative count

            List<string> opList = new List<string>();
            List<string> poList = new List<string>();
            List<string> totalpoList = new List<string>();
          

            //Get distinct value in column
            opList = vm.ITADReportCollection.Select(o => o.TestedBy.Replace(o.DateCreated.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim()).Distinct().ToList<string>();
            //get all ponumbers operative processed


            //New Paragraph
            Paragraph ReportOperativeTotals = new Paragraph();
            Paragraph POOperativeTotals = new Paragraph();
            Paragraph ReportPOTotals = new Paragraph();


            //Create new Section to host table
            Section mySectionRecords = new Section();
            mySectionRecords.Margin = new Thickness(0);

            //ADD POTOTALS

            //Get all distinct PO Numbers
            totalpoList = vm.ITADReportCollection.Select(p => p.POSONumber).ToList<string>();

        
            //Operative PO Section
            string operatives = "";
            string pos = "";

            foreach (var op in opList)
            {
                //Clear list before populating
                poList.Clear();

                //Create new Paragraph Add Operative Ref
                ReportOperativeTotals = new Paragraph(new Run(op));
                ReportOperativeTotals.Padding = new Thickness(1, 1, 1, 1);
                ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
                ReportOperativeTotals.FontSize = 15;
                ReportOperativeTotals.Foreground = Brushes.MediumPurple;
                ReportOperativeTotals.TextAlignment = TextAlignment.Center;
                ReportOperativeTotals.Margin = new Thickness(1, 1, 1, 1);
                //Add to block
                mySectionRecords.Blocks.Add(ReportOperativeTotals);

                //Count the items an operative has processed in collection  + " " + vm.ITADReportCollection.Count(o => o.TestedBy == op).ToString()
                operatives += " Orders: " + vm.ITADReportCollection.Where(p => p.TestedBy.Replace(p.DateCreated.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.POSONumber).Distinct().Count().ToString() + " Items: " + vm.ITADReportCollection.Count(o => o.TestedBy.Replace(o.DateCreated.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).ToString() + "";

                ReportOperativeTotals = new Paragraph(new Run(operatives));
                ReportOperativeTotals.Padding = new Thickness(1, 1, 1, 1);
                ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
                ReportOperativeTotals.FontSize = 15;
                ReportOperativeTotals.Foreground = Brushes.Green;
                ReportOperativeTotals.TextAlignment = TextAlignment.Center;
                ReportOperativeTotals.Margin = new Thickness(1, 1, 1, 1);
                //Add to block
                mySectionRecords.Blocks.Add(ReportOperativeTotals);

                //Get breakdown of PO's per Operative
                poList = vm.ITADReportCollection.Where(p => p.TestedBy.Replace(p.DateCreated.ToShortDateString(), "").Replace("Technician Name:", "").Replace("Tested By:", "").Trim() == op).Select(p => p.POSONumber).Distinct().ToList<string>();

                foreach (var po in poList)
                {
                    pos += " " + po + " (" + vm.ITADReportCollection.Count(o => o.POSONumber == po).ToString() + @"\" + totalpoList.Count(p => p == po).ToString() + ")  ";

                }

                //Create new Paragraph
                ReportOperativeTotals = new Paragraph(new Run(pos));
                ReportOperativeTotals.Padding = new Thickness(1, 1, 1, 1);
                ReportOperativeTotals.FontFamily = new FontFamily("Segoe UI");
                ReportOperativeTotals.FontSize = 12;
                ReportOperativeTotals.Foreground = Brushes.Black;
                ReportOperativeTotals.TextAlignment = TextAlignment.Center;
                ReportOperativeTotals.Margin = new Thickness(1, 1, 1, 1);


                mySectionRecords.Blocks.Add(ReportOperativeTotals);



                operatives = "";
                pos = "";

            }




            //1 Add Header To Main Section
            MainSection.Blocks.Add(mySectionReportHeader);
            //2 Add Records to Main Section
            MainSection.Blocks.Add(mySectionRecords);


            //Add Main Section to flowdocument
            ReportFD.Blocks.Add(MainSection);


            //END OF FLOWDOCUMENT


            String FileName = "";




            if (string.IsNullOrEmpty(SearchTXTB.Text))
            {
                //Set Clipboard Include date range


                FileName = "Serial Testing Report_" + vm.DateFrom.ToShortDateString().Replace("/", "-") + " To " + vm.DateTo.ToShortDateString().Replace("/", "-");
            }
            else
            {
                //Set Clipboard Include date range

                FileName = SearchTXTB.Text + "_Serial Testing Report_" + vm.DateFrom.ToShortDateString().Replace("/", "-") + " To " + vm.DateTo.ToShortDateString().Replace("/", "-");

            }


            //SAVE FLOW DOC TO PDF
            DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

            string SavePath = vm.myDocs + @"\" + FileName + @".pdf";

            //Clone the source document
            var str = XamlWriter.Save(ReportFD);
            var stringReader = new System.IO.StringReader(str);
            var xmlReader = XmlReader.Create(stringReader);
            var CloneDoc = XamlReader.Load(xmlReader) as FlowDocument;

            DocumentPaginator Clonepaginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;

            //A4 Dimensions
            Clonepaginator.PageSize = new System.Windows.Size(1122, 793);

            Thickness t = new Thickness(72);  // copy.PagePadding;
            CloneDoc.PagePadding = new Thickness(
                             Math.Max(0, t.Left),
                               Math.Max(0, t.Top),
                             Math.Max(1122 - (0 + 1122), t.Right),
                             Math.Max(793 - (0 + 793), t.Bottom));



            CloneDoc.ColumnWidth = double.PositiveInfinity;

            //End A4 Dimensions

            //Convert Flow Doc to Fixed First
            var paginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;
            var package = Package.Open(new MemoryStream(), FileMode.Create, FileAccess.ReadWrite);
            var packUri = new Uri("pack://temp.xps");
            PackageStore.RemovePackage(packUri);
            PackageStore.AddPackage(packUri, package);
            var xps = new XpsDocument(package, CompressionOption.NotCompressed, packUri.ToString());
            XpsDocument.CreateXpsDocumentWriter(xps).Write(paginator);
            FixedDocument fddoc = new FixedDocument();


            //Requires Dispatcher
            Application.Current.Dispatcher.Invoke((Action)delegate
            {



                fddoc = xps.GetFixedDocumentSequence().References[0].GetDocument(true);
                // your code



                //Fixed document to PDF
                var ms = new MemoryStream();
                var package2 = Package.Open(ms, FileMode.Create);
                var doc = new XpsDocument(package2);
                var writer = XpsDocument.CreateXpsDocumentWriter(doc);
                writer.Write(fddoc.DocumentPaginator);

                //docWriter.Write(fddoc.DocumentPaginator);
                doc.Close();
                package2.Close();

                // Get XPS file bytes
                var bytes = ms.ToArray();
                ms.Dispose();

                // Print to PDF
                var outputFilePath = SavePath;
                PdfFilePrinter.PrintXpsToPdf(bytes, outputFilePath, "Document Title");


                //Launch report
                ////Launch file for printing
                if (File.Exists(outputFilePath))
                {
                    System.Diagnostics.Process.Start(outputFilePath);

                }

            });


            //create new instance
            //vm.ITADReportCollection = new ObservableCollection<ITADReporty>();

           

        }





        private void SearchTXTB_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void SearchTXTB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //Check if search empty. If so default to Date range only
                if (SearchTXTB.Text != string.Empty)
                {
                    FilterSearch(SearchTXTB.Text, "");
                }
                else
                {

                    FilterDateSearch("", "");
                }
            }
        }

        private void GenerateBTN_Click(object sender, RoutedEventArgs e)
        {
            //Load selected report to viewer
            LoadReport("LoadReport", null);

        }


        //Load selection to Viewer
        private void LoadReport(string Mode, ITADReport selectedItem)
        {
            switch (Mode)
            {
                case "LoadReport":

                    if (ReportLV.SelectedIndex != -1 || AssetsProcessesLB.SelectedIndex != -1)
                    {
                        //Reset checkboxes
                        GreenLightCB.IsChecked = false;
                        FaultyCB.IsChecked = false;

                        if (AssetsProcessesLB.SelectedIndex != -1)
                        {

                        }

                        if (ReportLV.SelectedIndex != -1)
                        {
                            AssetsProcessesLB.SelectedIndex = -1;
                            //AssetsProcessesLB.SelectedItems.select.;
                        }




                        vm.TestCLIOutput = vm.ITADReportSelectedItem.ReportBody;
                        vm.ITADDeviceCategory = vm.ITADReportSelectedItem.DeviceCategory;
                        vm.ITADProcessPerformed = vm.ITADReportSelectedItem.ProcessPerformed;
                        vm.ITADTestingPerformed = vm.ITADReportSelectedItem.TestingPerformed;
                        vm.ITADNotes = vm.ITADReportSelectedItem.Notes;
                        vm.ReportUser = vm.ITADReportSelectedItem.TestedBy;
                        if (vm.ITADReportSelectedItem.POSONumber != null)
                            vm.TestPOSONo = vm.ITADReportSelectedItem.POSONumber.Replace("\\", "").Replace("/", "");
                        vm.ITADAssetNo = vm.ITADReportSelectedItem.AssetNo;
                        if (vm.ITADReportSelectedItem.SerialNo != null)
                            vm.TestSerialNo = vm.ITADReportSelectedItem.SerialNo.Replace("\\", "").Replace("/", "");

                        //Check if green light tested
                        if (vm.ITADReportSelectedItem.TestingPerformed != null)
                            if (vm.ITADReportSelectedItem.TestingPerformed.Contains("Green Light Tested"))
                            {
                                GreenLightCB.IsChecked = true;
                                vm.ITADTestingPerformed += "|Green Light Tested|";
                            }

                        //Check if faulty tested
                        if (vm.ITADReportSelectedItem.TestingPerformed != null)
                            if (vm.ITADReportSelectedItem.TestingPerformed.Contains("Item Faulty"))
                            {
                                FaultyCB.IsChecked = true;
                                vm.ITADTestingPerformed += "|Item Faulty|";
                            }

                        //Check if faulty tested
                        if (vm.ITADReportSelectedItem.TestingPerformed != null)
                            if (vm.ITADReportSelectedItem.TestingPerformed.Contains("Hard Reset"))
                            {
                                HardResetCB.IsChecked = true;
                                vm.ITADTestingPerformed += "|Hard Reset|";
                            }


                        //Generate QR
                        GenerateLabel();

                        //Enable Print Button Again
                        PrintBTN.IsEnabled = true;

                        //set reprint flag to true
                        vm.ITADIsReprint = true;
                      

                    }

                    break;

                case "ExportPDF":

                    //Reset checkboxes
                    GreenLightCB.IsChecked = false;
                    FaultyCB.IsChecked = false;

                    vm.TestCLIOutput = selectedItem.ReportBody;
                    vm.ITADDeviceCategory = selectedItem.DeviceCategory;
                    vm.ITADProcessPerformed = selectedItem.ProcessPerformed;
                    vm.ITADTestingPerformed = selectedItem.TestingPerformed;
                    vm.ITADNotes = selectedItem.Notes;
                    vm.ReportUser = selectedItem.TestedBy;
                    if (selectedItem.POSONumber != null)
                        vm.TestPOSONo = selectedItem.POSONumber.Replace("\\", "").Replace("/", "");
                    vm.ITADAssetNo = selectedItem.AssetNo;
                    if (selectedItem.SerialNo != null)
                        vm.TestSerialNo = selectedItem.SerialNo.Replace("\\", "").Replace("/", "");

                    //Check if green light tested
                    if (selectedItem.TestingPerformed != null)
                        if (selectedItem.TestingPerformed.Contains("Green Light Tested"))
                        {
                            GreenLightCB.IsChecked = true;
                            vm.ITADTestingPerformed += "|Green Light Tested|";
                        }

                    //Check if faulty tested
                    if (selectedItem.TestingPerformed != null)
                        if (selectedItem.TestingPerformed.Contains("Item Faulty"))
                        {
                            FaultyCB.IsChecked = true;
                            vm.ITADTestingPerformed += "|Item Faulty|";
                        }

                    //Check if faulty tested
                    if (selectedItem.TestingPerformed != null)
                        if (selectedItem.TestingPerformed.Contains("Hard Reset"))
                        {
                            HardResetCB.IsChecked = true;
                            vm.ITADTestingPerformed += "|Hard Reset|";
                        }


                    //Generate QR
                    GenerateLabel();

                    //Enable Print Button Again
                    PrintBTN.IsEnabled = true;

                    //set reprint flag to true
                    vm.ITADIsReprint = true;
           

            break;
            }

    

        

        }



        private void ExportCSVBTN_Click(object sender, RoutedEventArgs e)
        {
            List<ITADReport> reportList = new List<ITADReport>();

            if (ReportLV.Items.Count > 0)
            {
                foreach (ITADReport ns in ReportLV.Items)
                {
                    //Add to local list
                    reportList.Add(ns);
                }

            }
            //Write to CSV
            StaticFunctions.CreateTestReportCSV(reportList);
        }

        private void PITabCtrl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (PITabCtrl.SelectedIndex == 0 && vm.ITADIsReprint == true)
            {
              
                //PrintBTN.IsEnabled = false;
                //vm.ITADIsReprint = false;
                //vm.ITADReportCollection.Clear();
                //ReportMSGTB.Text = "";
                //// If collection changes Count
                //vm.RecordsTotal = ReportLV.Items.Count.ToString();

            }
            else if (PITabCtrl.SelectedIndex == 1)
            {
                vm.ITADIsReprint = true;
                ReportMSGTB.Text = "";

                // If collection changes Count
                vm.RecordsTotal = ReportLV.Items.Count.ToString();
            }

        }

        private void DateFrom_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //reset search textbox
            //SearchTXTB.Text = "";
        }

        private void DateTo_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //reset search textbox
            //SearchTXTB.Text = "";
            //Search date range on change
            //FilterDateSearch("", "");
        }


        private void SerialOverrrideTXTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            //MessageBox.Show("Boo");
            //Send Data To Serial Port
            if (!string.IsNullOrWhiteSpace(SerialOverrrideTXTB.Text))
            {
                SerialOverrrideTXTB.Text.ToUpper();
                PrintBTN.IsEnabled = true;
            }
            

            if(!string.IsNullOrEmpty(vm.TestSerialNo))
            {
                // Create QR CODE
                vm.TESTQRCodeGI = CreateQRCode(vm.TestSerialNo.Trim()); 
            }

            if (!string.IsNullOrEmpty(vm.TestReportPOSONo))
            {
                // Create QR CODE
                vm.TESTQRCodeGO = CreateQRCode(vm.TestReportPOSONo.Replace(@"PO\SO: ", "").Trim() + "-" + vm.TestSerialNo.Trim()); 
            }
              

        }

        private void SerialOverrrideTXTB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //MessageBox.Show("Boo");
                //Send Data To Serial Port
                SerialOverrrideTXTB.Text.ToUpper();

                if (!string.IsNullOrEmpty(vm.TestSerialNo))
                {
                    // Create QR CODE
                    vm.TESTQRCodeGI = CreateQRCode(vm.TestSerialNo.Trim()); 
                }

                if (!string.IsNullOrEmpty(vm.TestReportPOSONo))
                {
                    // Create QR CODE
                    vm.TESTQRCodeGO = CreateQRCode(vm.TestReportPOSONo.Replace(@"PO\SO: ", "").Trim() + "-" + vm.TestSerialNo.Trim()); 
                }
            }
        }

        private void ChangeSerialBUT_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Boo");
            //Send Data To Serial Port
            vm.TestSerialNo = SerialOverrrideTXTB.Text;

            if (!string.IsNullOrEmpty(vm.TestSerialNo))
            {
                // Create QR CODE
                vm.TESTQRCodeGI = CreateQRCode(vm.TestSerialNo.Trim());
            }

            if (!string.IsNullOrEmpty(vm.TestReportPOSONo))
            {
                // Create QR CODE
                vm.TESTQRCodeGO = CreateQRCode(vm.TestReportPOSONo.Replace(@"PO\SO: ", "").Trim() + "-" + vm.TestSerialNo.Trim());
            }
        }

        private void POSONoTXT_TextChanged(object sender, TextChangedEventArgs e)
        {
            vm.TestReportPOSONo = @"PO\SO: " + vm.TestPOSONo;
           

            if (!string.IsNullOrEmpty(vm.TestSerialNo))
            {
                // Create QR CODE
                vm.TESTQRCodeGI = CreateQRCode(vm.TestSerialNo.Trim());
            }

            if (!string.IsNullOrEmpty(vm.TestReportPOSONo))
            {
                // Create QR CODE
                vm.TESTQRCodeGO = CreateQRCode(vm.TestReportPOSONo.Replace(@"PO\SO: ", "").Trim() + "-" + vm.TestSerialNo.Trim());
            }
        }

        private void ClearBTN_Click(object sender, RoutedEventArgs e)
        {
            vm.TestPOSONo = "";
            vm.ITADAssetNo = "";
            vm.TestSerialNo = "";
            vm.ITADDeviceCategory = "";
            vm.ITADProcessPerformed = "";
            vm.ITADTestingPerformed = "";
            vm.ITADNotes = "";
            vm.TestCLIOutput = "";
            GreenLightCB.IsChecked = false;
            FaultyCB.IsChecked = false;
            HardResetCB.IsChecked = false;


            //vm.CurrentUser = vm.ITADReportSelectedItem.TestedBy;
        }

        private void ReportLV_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            // If collection changes Count
            // If collection changes Count
            vm.RecordsTotal = ReportLV.Items.Count.ToString();
        }




        public void GenerateLabel()
        {
            if (vm != null)
            {

                //QRCODE Dimensions
                TESTStandardLabelZebraImage1.Width = 80;
                TESTStandardLabelZebraImage1.Height = 80;
                TESTStandardLabelZebraImage2.Width = 80;
                TESTStandardLabelZebraImage2.Height = 80;

                if(string.IsNullOrWhiteSpace(vm.TestReportPOSONo) || string.IsNullOrWhiteSpace(vm.TestSerialNo))
                {

                }
                else
                {
                    // Create QR CODE
                    vm.TESTQRCodeGI = CreateQRCode(vm.TestSerialNo.Replace("\\", "").Replace("/", "").Trim());

                    // Create QR CODE
                    vm.TESTQRCodeGO = CreateQRCode(vm.TestReportPOSONo.Replace(@"PO\SO: ", "").Replace("\\", "").Replace("/", "").Trim() + "-" + vm.TestSerialNo.Replace("\\","-").Replace("/","-").Trim());

                    //MessageBox.Show(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.SerialNo.Trim() + ".png");

                    //*** Checklist QR CODE ***
                    //Call Embed QR Saves & extracts QR Code to File. This is required for the cloning of the flowdocument as it requires a full path
                    vm.TESTQRCodeGI = vm.EmbedChecklistQR(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.TestSerialNo.Replace("\\", "-").Replace("/", "-").Trim() + ".png", vm.TESTQRCodeGI);
                    //GOODS OUT ORDER REF + 
                    //Call Embed QR Saves & extracts QR Code to File. This is required for the cloning of the flowdocument as it requires a full path
                    vm.TESTQRCodeGO = vm.EmbedChecklistQR(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.TestReportPOSONo.Replace(@"PO\SO: ", "").Replace("\\", "").Replace("/", "").Trim() + "_" + vm.TestSerialNo.Replace("\\", "").Replace("/", "").Trim() + ".png", vm.TESTQRCodeGO);
                }
            
            }
        }


        public BitmapImage EmbedChecklistQR(string UriPath, BitmapImage QRProperty)
        {
            try
            {

        
            ////IMPORTANT AS QR THROWS EXCEPTION IF NOT A URI PATH
            ////Generate a file of the bitmap
            BitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(QRProperty));

            using (FileStream fileStream = new FileStream(UriPath, System.IO.FileMode.Create, FileAccess.Write, FileShare.Read))
            {

                encoder.Save(fileStream);
                fileStream.Flush();
                fileStream.Close();
            }


            // Create source.
            BitmapImage bi = new BitmapImage();
            // BitmapImage.UriSource must be in a BeginInit/EndInit block.
            bi.BeginInit();
            //*** Required not to lock file ***
            bi.CacheOption = BitmapCacheOption.OnLoad;
            bi.UriSource = new Uri(UriPath, UriKind.Absolute);
            bi.EndInit();

            //Return Bitmap Image with full absolute path
            return bi;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return null;
            }
        }


        //Generate QRCODE Image
        public BitmapImage CreateQRCode(string stringData)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(stringData, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(40, System.Drawing.Color.Black, System.Drawing.Color.White, null, 1, 1, true);



            return ToBitmapImage(qrCodeImage);
        }

        //BITMAP TO BITMAP IMAGE
        public BitmapImage ToBitmapImage(Bitmap bitmap)
        {
            try
            {

          
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();

                return bitmapImage;
            }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return null;
            }
        }

        ////BITMAP IMAGE TO BITMAP
        private Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            return new Bitmap(bitmapImage.StreamSource);
        }

        private void TestReportingUC_Unloaded(object sender, RoutedEventArgs e)
        {
            //On exit clear the temp QR Code images
            if (Directory.Exists(vm.myDocs + @"\Nebula Logs\QR"))
            {
                //Get list of all items in folder
                List<string> QRimages = Directory.GetFiles(vm.myDocs + @"\Nebula Logs\QR").ToList<string>();

                foreach (var qrimage in QRimages)
                {
                    if (File.Exists(qrimage))
                    {
                        //Delete
                        File.Delete(qrimage);
                    }


                }
            }
        }

      

        private void AssetsProcessesLB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (AssetsProcessesLB.SelectedIndex > -1)
                {
                    int firstHyphen = AssetsProcessesLB.SelectedItem.ToString().IndexOf('_');
                    string serialNoRef = AssetsProcessesLB.SelectedItem.ToString().Substring(0, firstHyphen).Trim();


                    ReadDataSessionList("SELECT * FROM ReportRecords WHERE SerialNo = '" + serialNoRef + "'");


                }
              
            
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

           
           
        }

        private void SerialNoTXT_TextChanged(object sender, TextChangedEventArgs e)
        {

            //Send Data To Serial Port
            if (!string.IsNullOrWhiteSpace(SerialNoTXT.Text))
            {

                //vm.TestSerialNo.ToUpper();
                PrintBTN.IsEnabled = true;
            }


            if (!string.IsNullOrEmpty(vm.TestSerialNo))
            {
                // Create QR CODE
                vm.TESTQRCodeGI = CreateQRCode(vm.TestSerialNo.Replace(":", "").Replace(",", "").Replace("#", "").Trim());
            }

            if (!string.IsNullOrEmpty(vm.TestReportPOSONo))
            {
                // Create QR CODE
                vm.TESTQRCodeGO = CreateQRCode(vm.TestReportPOSONo.Replace(@"PO\SO: ", "").Trim() + "-" + vm.TestSerialNo.Replace(":", "").Replace(",", "").Replace("#", "").Trim());
            }
        }

        private void SerialNoTXT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                //Send Data To Serial Port
                //vm.TestSerialNo.ToUpper();

                if (!string.IsNullOrEmpty(vm.TestSerialNo))
                {
                    // Create QR CODE
                    vm.TESTQRCodeGI = CreateQRCode(vm.TestSerialNo.Replace(":", "").Replace(",", "").Replace("#", "").Trim());
                }

                if (!string.IsNullOrEmpty(vm.TestReportPOSONo))
                {
                    // Create QR CODE
                    vm.TESTQRCodeGO = CreateQRCode(vm.TestReportPOSONo.Replace(@"PO\SO: ", "").Trim() + "-" + vm.TestSerialNo.Replace(":", "").Replace(",", "").Replace("#", "").Trim());
                }
            }
        }


        private void GreenLightCB_Checked(object sender, RoutedEventArgs e)
        {
            if (GreenLightCB.IsChecked == true)
            {
                //SHOW
                GreenLightTB.Visibility = Visibility.Visible;
            }


        }

        private void GreenLightCB_Unchecked(object sender, RoutedEventArgs e)
        {
            if (GreenLightCB.IsChecked == false)
            {
                //HIDE
                GreenLightTB.Visibility = Visibility.Collapsed;
            }
        }


        private void FaultyCB_Checked(object sender, RoutedEventArgs e)
        {
            if (FaultyCB.IsChecked == true)
            {
                //SHOW
                FaultyTB.Visibility = Visibility.Visible;
            }
        }

        private void FaultyCB_Unchecked(object sender, RoutedEventArgs e)
        {
            if (FaultyCB.IsChecked == false)
            {
                //SHOW
                FaultyTB.Visibility = Visibility.Collapsed;
            }
        }

        private void HardResetCB_Checked(object sender, RoutedEventArgs e)
        {
            if (HardResetCB.IsChecked == true)
            {
                //SHOW
                HardResetTB.Visibility = Visibility.Visible;
            }
        }

        private void HardResetCB_Unchecked(object sender, RoutedEventArgs e)
        {

            if (HardResetCB.IsChecked == false)
            {
                //SHOW
                HardResetTB.Visibility = Visibility.Collapsed;
            }
        }

        private void ExportPDFBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {

  
            //Create paginator instance
            DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

            //Loop through selected items
            foreach (ITADReport itm in ReportLV.SelectedItems)
            {
                //Set view and generate QR
                //Load selected report to viewer
                LoadReport("ExportPDF", itm);


                ////Set the serial number to text on the clipboard  Use control v
                if (!string.IsNullOrEmpty(vm.TestSerialNo))
                    Clipboard.SetText(vm.TestSerialNo.Trim(), TextDataFormat.UnicodeText);
                //MessageBox.Show(vm.myDocs + @"\" + vm.TestSerialNo.Replace("Serial No: ", "").Trim() + itm.DateCreated.ToShortDateString().Replace(@"\","") + ".pdf");

                //Create the PDF from the Flow Document and save to a fixed path.
                dpw.FlowDocument2Pdf(Report1, vm.myDocs + @"\" + vm.TestSerialNo.Trim() + "_" + itm.DateCreated.ToShortDateString().Replace(@"/", "-") + ".pdf");

                //Launch File in default reader
                System.Diagnostics.Process.Start(vm.myDocs + @"\" + vm.TestSerialNo.Trim() + "_" + itm.DateCreated.ToShortDateString().Replace(@"/", "-") + ".pdf");

            }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
