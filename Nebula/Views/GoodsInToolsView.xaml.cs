﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.ViewModels;
using Nebula.Views;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for GoodsInToolsView.xaml
    /// </summary>
    public partial class GoodsInToolsView : UserControl
    {
        public GoodsInToolsView()
        {
            //set static variable Department for use when select server processing views for goods in goods out tech
            StaticFunctions.Department = "GoodsIn";
            InitializeComponent();
        }
    }
}
