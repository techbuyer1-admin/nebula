﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Nebula.Properties;
using Nebula.Models;
using Nebula.ViewModels;
using Nebula.Helpers;
using System.Windows.Threading;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for SingleSerialView.xaml
    /// </summary>
    /// 



    public partial class SingleSerialView : UserControl
    {
        //http://jkorpela.fi/chars/c0.html FOR FULL CHART
        const char CtrlA = (char)1;
        const char CtrlB = (char)2;
        const char CtrlC = (char)3;
        const char CtrlD = (char)4;
        const char CtrlE = (char)5;
        const char CtrlF = (char)6;
        const char CtrlG = (char)7;
        const char CtrlH = (char)8;
        const char CtrlI = (char)9;
        const char CtrlJ = (char)10;
        const char CtrlK = (char)11;
        const char CtrlL = (char)12;
        //const char CtrlM = (char)13;
        const char CtrlN = (char)14;
        const char CtrlO = (char)15;
        const char CtrlP = (char)16;
        const char CtrlQ = (char)17;
        const char CtrlR = (char)18;
        const char CtrlS = (char)19;
        const char CtrlT = (char)20;
        const char CtrlU = (char)21;
        const char CtrlV = (char)21;
        const char CtrlW = (char)23;
        const char CtrlX = (char)24;
        const char CtrlY = (char)25;
        const char CtrlZ = (char)26;
        const char CtrlESC = (char)27;
        const char CtrlFILESEP = (char)28;
        const char CtrlGRPSEP = (char)29;
        const char CtrlRECSEP = (char)30;
        const char CR = (char)13;
        const char SPACE = (char)32;
        // The main control for communicating through the RS-232 port
        private SerialPort _serialPort1 = new SerialPort();
        //private SerialPort _serialPort2 = new SerialPort();

        public enum DataMode { Text, Hex }
        //public enum LogMsgType { Incoming, Outgoing, Normal, Warning, Error };

        
        public string ConsoleIncoming { get; set; }

        public FlowDocument FlowDoc { get; set; }

        public RichTextBox RTB { get; set; }

        public SerialPort SP1 { get; set; }

        //Holds the console window content, is used for reporting instead of the richtextbox
        public string ConsoleContent1 { get; set; }

        //
        FlowDocument FlowDoc1 = new FlowDocument();

        Paragraph para1 = new Paragraph();

        //Holds the collection of detected com ports on the system
        List<SerialPort> ComCollection = new List<SerialPort>();
        //Holds a collection of serial ports to be used with single and multiple open port functions
        List<SerialPort> prts = new List<SerialPort>();
        //Contains a collection of flowdocuments for reporting purposes
        ObservableCollection<FlowDocument> flowDocCollection = new ObservableCollection<FlowDocument>();
        //Contains the read in xml files that the instructions will be stored as.
        ObservableCollection<AutomatedCliModel> productCollection = new ObservableCollection<AutomatedCliModel>();

        //declare viewmodel
        MainViewModel vm;

      
        public SingleSerialView(MainViewModel PassedViewModel)
        {
            InitializeComponent();

            // _serialPort1.DiscardNull = true;
           _serialPort1.Encoding = Encoding.UTF8;
           _serialPort1.NewLine = "\r";
            // _serialPort1.ReadBufferSize = 2000000;

            //get the passed in viewmodel and assign to the local vm variable
            vm = PassedViewModel;
            //set the datacontext of the view to the passed vm

            //FlowDoc = new FlowDocument();
            ConsoleRTB1.Document = FlowDoc1;

            // Call the Get Ports and Sort and Add To Combo Box
            string[] comPorts = OrderedPortNames();
            foreach (string s in comPorts)
            {
                ComPortCB.Items.Add(s);
            }

            //Populate the Com ComboBoxes
            //Baud
            BaudCB.Items.Add("4800");
            BaudCB.Items.Add("9600");
            BaudCB.Items.Add("14400");
            BaudCB.Items.Add("19200");
            BaudCB.Items.Add("38400");
            BaudCB.Items.Add("57600");
            BaudCB.Items.Add("115200");
            //Parity
            ParityCB.Items.Add("None");
            ParityCB.Items.Add("Odd");
            ParityCB.Items.Add("Even");
            ParityCB.Items.Add("Mark");
            ParityCB.Items.Add("Space");
            // Data Bits
            DataBitsCB.Items.Add("7");
            DataBitsCB.Items.Add("8");
            DataBitsCB.Items.Add("9");
            // Stop Bits
            StopBitsCB.Items.Add("None");
            StopBitsCB.Items.Add("One");
            StopBitsCB.Items.Add("Two");
            StopBitsCB.Items.Add("OnePointFive");
            //Handshake
            HandShakeCB.Items.Add("None");
            HandShakeCB.Items.Add("XOnXOff");
            HandShakeCB.Items.Add("RequestToSend");
            HandShakeCB.Items.Add("RequestToSendXOnXOff");


            //_serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve1);
            _serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve1);

           // Load user settings
            settings.Reload();
            LoadSettings();
            //set console window initial font size
            ConsoleFontSizeCB.SelectedIndex = 5;
            vm.ConsoleFontSize = 13;

           
        }

      
        //private async void Recieve1(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        //{
        //    _responseFromHerma = new StringBuilder(100);
        //    _responseFromHerma.Length = 0;

        //    //vm.RecieveData(_serialPort1, para1, FlowDoc1, ConsoleRTB1, sb1, currentline1, MsgSuccess1, MsgWarning1, MsgError1);
        //    System.IO.Ports.SerialPort port = (System.IO.Ports.SerialPort)sender;
        //    int count = port.BytesToRead;
        //    byte[] data = new byte[count];
        //    port.Read(data, 0, data.Length);
        //    // use your logger here and write  BitConverter.ToString(data)
        //    var dataAsString = System.Text.Encoding.Default.GetString(data);
           
        //    _responseFromHerma.Append(dataAsString);
        //    MessageBox.Show(dataAsString);
        //    if (_responseFromHerma.Length == statusResponseExpectedLength)
        //    {
        //        CompleteAnswerReceived();
        //    }
               
        //}

        //private void CompleteAnswerReceived()
        //{
        //    MessageBox.Show(_responseFromHerma.ToString());
        //    _callback.Invoke(_responseFromHerma.ToString());
        //}


        StringBuilder sb1 = new StringBuilder();
        //char LF = (char)10;
        public string currentline1;

        //delegate
        //public delegate void UpdateUiTextDelegate(string recdata);

        //Event for serailport recieved data
        //NEW RECIEVE
        private void Recieve1(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            vm.RecieveData(_serialPort1, CommandTXT1, "prompt1", para1, FlowDoc1, ConsoleRTB1, sb1, currentline1, MsgSuccess1, MsgWarning1, MsgError1, MsgNormCPU, MsgMedCPU, MsgHighCPU);
        }




        

        ////EXTENSION METHOD FOR SETTING RICHTEXT WPF CONTROL
        //public void SetText(this RichTextBox richTextBox, string text)
        //{
        //    richTextBox.Document.Blocks.Clear();
        //    richTextBox.Document.Blocks.Add(new Paragraph(new Run(text)));
        //}

        //public string GetText(this RichTextBox richTextBox)
        //{
        //    return new TextRange(richTextBox.Document.ContentStart,
        //        richTextBox.Document.ContentEnd).Text;
        //}


        private DataMode CurrentDataMode
        {
            get
            {
                //if (rbHex.Checked) return DataMode.Hex;
                /* else */
                return DataMode.Text;
            }
            set
            {
                //if (value == DataMode.Text) rbText.Checked = true;
                //else rbHex.Checked = true;
            }
        }

        // Various colors for logging info
        //private System.Windows.Media.Color[] LogMsgTypeColor = { Color.Blue, Color.Green, Color.Black, Color.Orange, Color.Red };

        // Temp holder for whether a key was pressed
        //private bool KeyHandled = false;

        private Settings settings = Settings.Default;

        private string RefreshComPortList(IEnumerable<string> PreviousPortNames, string CurrentSelection, bool PortOpen)
        {
            // Create a new return report to populate
            string selected = null;

            // Retrieve the list of ports currently mounted by the operating system (sorted by name)
            string[] ports = SerialPort.GetPortNames();

            // First determain if there was a change (any additions or removals)
            bool updated = PreviousPortNames.Except(ports).Count() > 0 || ports.Except(PreviousPortNames).Count() > 0;

            // If there was a change, then select an appropriate default port
            if (updated)
            {
                // Use the correctly ordered set of port names
                ports = OrderedPortNames();

                // Find newest port if one or more were added
                string newest = SerialPort.GetPortNames().Except(PreviousPortNames).OrderBy(a => a).LastOrDefault();

                // If the port was already open... (see logic notes and reasoning in Notes.txt)
                if (PortOpen)
                {
                    if (ports.Contains(CurrentSelection)) selected = CurrentSelection;
                    else if (!String.IsNullOrEmpty(newest)) selected = newest;
                    else selected = ports.LastOrDefault();
                }
                else
                {
                    if (!String.IsNullOrEmpty(newest)) selected = newest;
                    else if (ports.Contains(CurrentSelection)) selected = CurrentSelection;
                    else selected = ports.LastOrDefault();
                }
            }

            // If there was a change to the port list, return the recommended default selection
            return selected;
        }

        private string[] OrderedPortNames()
        {
            // Just a placeholder for a successful parsing of a string to an integer
            int num;

            // Order the serial port names in numberic order (if possible)
            return SerialPort.GetPortNames().OrderBy(a => a.Length > 3 && int.TryParse(a.Substring(3), out num) ? num : 0).ToArray();
        }

                                   

        void _serialPort1_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        void _serialPort2_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            // Show the state of the pins
            UpdatePinState();
        }

        private void UpdatePinState()
        {
            //this.Invoke(new ThreadStart(() => {
            //    // Show the state of the pins
            //    //chkCD.Checked = comport.CDHolding;
            //    //chkCTS.Checked = comport.CtsHolding;
            //    //chkDSR.Checked = comport.DsrHolding;
            //}));
        }

       

        public string Transform(string data)
        {
            string result = data;
            char cr = (char)13;
            char lf = (char)10;
            char tab = (char)9;

            result = result.Replace("\\r", cr.ToString());
            result = result.Replace("\\n", lf.ToString());
            result = result.Replace("\\t", tab.ToString());

            return result;
        }


        static string ReplaceHexadecimalSymbols(string txt)
        {
            //STRIPS OUT THE ESCAPE SEQUENCE x1b
            //string r = "[\x00-\x08\x0B\x0C\x0E-\x1F\x26\x1b]";
            string r = "[\x00-\x08\x0B\x0C\x0E-\x1F\x26]";
            return Regex.Replace(txt, r, "", RegexOptions.Compiled);
        }


        public String GetRtfText(RichTextBox box)
        {
            // Create a TextRange to retrieve the contents of the RichTextBox
            TextRange TextRange = new TextRange(box.Document.ContentStart, box.Document.ContentEnd);

            // Create a new MemoryStream
            MemoryStream MemoryStream = new MemoryStream();

            // Saves the RTF text to the MemoryStream
            TextRange.Save(MemoryStream, DataFormats.Rtf);

            // Return the plain RTF text
            return Encoding.UTF8.GetString(MemoryStream.ToArray());
        }

        // Sets the contents of a RichTextBox with plain text RTF
        public void SetRtfText(RichTextBox box, String RtfText)
        {
            // Create a TextRange to retrieve the contents of the RichTextBox
            TextRange TextRange = new TextRange(box.Document.ContentStart, box.Document.ContentEnd);

            // Create a new MemoryStream from the RtfText
            MemoryStream MemoryStream = new MemoryStream(Encoding.UTF8.GetBytes(RtfText));

            // Load the RTF text into the RichTextBox from the MemoryStream
            TextRange.Load(MemoryStream, DataFormats.Rtf);
        }
                               

        private void CheckInputType(string type)
        {

        }


        private void SendData(string type)
        {

            byte[] bytes = Encoding.UTF8.GetBytes(CommandTXT1.Text + "\r");
            //Check the Brand
            if (vm.ProductBrand == "Cisco" || vm.ProductBrand == "Mellanox")
            {
                switch (type)
                {
                    case "Normal":
                        //Check for just  a Carriage Return
                        if (string.IsNullOrEmpty(CommandTXT1.Text))
                        {

                            MainViewModel.CurrentCommand = "";
                            if (_serialPort1.IsOpen)
                            if (_serialPort1.BreakState == false)
                            {
                                    //_serialPort1.Write(bytes, 0, bytes.Length);
                                    //if (MainViewModel.CurrentCommand == "en" || MainViewModel.CurrentCommand == "enable" || MainViewModel.CurrentCommand == "exit")
                                    //{
                                    //    //MessageBox.Show(CurrentCommand);
                                    //    _serialPort1.Write(CommandTXT1.Text + "\r\r");
                                    //}
                                    //else
                                    //{
                                    //    _serialPort1.Write(CommandTXT1.Text + "\r");
                                    //}
                                   // MessageBox.Show(MainViewModel.CurrentCommand);

                                    _serialPort1.Write("\r");
                                }
                            else
                            {
                                    // _serialPort1.BreakState = false;
                                    // _serialPort1.Write(CommandTXT1.Text + "\r");
                                    MessageBox.Show("Please switch off Break State and enter the command again.");

                                
                            }
                            //_serialPort1.Write("\r");
                        }
                        else
                        {
                            MainViewModel.CurrentCommand = CommandTXT1.Text;
                            //if (CommandTXT1.Text.Contains("exit"))
                            //{
                            //    //if exit command clear terminal before sending
                            //    ClearTerminal(ConsoleRTB1);
                            //}
                            if (_serialPort1.IsOpen)
                            if (_serialPort1.BreakState == false)
                            {
                                    if (MainViewModel.CurrentCommand == "en" || MainViewModel.CurrentCommand == "enable" || MainViewModel.CurrentCommand == "exit")
                                    {
                                        //MessageBox.Show(CurrentCommand);
                                        _serialPort1.Write(CommandTXT1.Text + "\r\r");
                                    }
                                    else
                                    {
                                        _serialPort1.Write(CommandTXT1.Text + "\r");
                                    }

                                  
                                    
                            }
                            else
                            {
                               // _serialPort1.BreakState = false;
                                MessageBox.Show("Please switch off Break State and run the command again.");

                               // _serialPort1.Write(CommandTXT1.Text + "\r");
                            }
                          
                            //_serialPort1.Write(CommandTXT1.Text + "\r\r");
                        }
                        break;
                    case "Space":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            if (_serialPort1.BreakState == false)
                            {
                                _serialPort1.WriteLine(string.Format("{0}", SPACE));
                            }
                            else
                            {
                                // _serialPort1.BreakState = false;
                                MessageBox.Show("Please switch off Break State and run the command again.");

                                // _serialPort1.Write(CommandTXT1.Text + "\r");
                            }
                       
                        break;
                    case "CTRL]":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            if (_serialPort1.BreakState == false)
                            {
                                //ctrlb
                                if (_serialPort1.IsOpen)
                                    _serialPort1.Write(new byte[] { 29 }, 0, 1);

                            }
                            else
                            {
                                // _serialPort1.BreakState = false;
                                MessageBox.Show("Please switch off Break State and run the command again.");

                                // _serialPort1.Write(CommandTXT1.Text + "\r");
                            }

                        break;
                    case "CTRLB":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            if (_serialPort1.BreakState == false)
                            {
                                //ctrlb
                                if (_serialPort1.IsOpen)
                                    _serialPort1.Write(new byte[] { 2 }, 0, 1);
                                
                            }
                            else
                            {
                                // _serialPort1.BreakState = false;
                                MessageBox.Show("Please switch off Break State and run the command again.");

                                // _serialPort1.Write(CommandTXT1.Text + "\r");
                            }
                        
                        break;
                    case "CTRLC":
                        MainViewModel.CurrentCommand = "";
                        //ctrlc
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 3 }, 0, 1);

                        break;
                    case "CTRLD":
                        MainViewModel.CurrentCommand = "";

                        //ctrld
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 4 }, 0, 1);
                        break;
                    case "CTRLE":
                        MainViewModel.CurrentCommand = "";

                        //ctrld
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 5 }, 0, 1);
                        break;
                    case "CTRLF":
                        MainViewModel.CurrentCommand = "";

                        //ctrlf
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 6 }, 0, 1);
                        break;
                
                    case "CTRLP":
                        MainViewModel.CurrentCommand = "";

                        //ctrlP
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 16 }, 0, 1);
                        break;
                    case "CTRLS":
                        MainViewModel.CurrentCommand = "";

                        //ctrlS
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 19 }, 0, 1);
                        break;
                    case "CTRLX":
                        MainViewModel.CurrentCommand = "";

                        //ctrlX
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 24 }, 0, 1);
                        break;
                    case "CTRLZ":
                        MainViewModel.CurrentCommand = "";

                        //ctrlX
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 26 }, 0, 1);
                        break;
                    case "SHIFTS":
                        MainViewModel.CurrentCommand = "";
                        //MessageBox.Show("Here");
                        if (_serialPort1.IsOpen)
                            _serialPort1.WriteLine(string.Format("{0}{1}", ModifierKeys.Shift, Key.S));
                       // _serialPort1.WriteLine(string.Format("{0}{1}", "\x10\x53", CR));
                        //
                        break;


                }

            }
            else if (vm.ProductBrand == "HP" || vm.ProductBrand == "Aruba" )
            {
                //needs to go through the appendstring parser to strip out characters


                switch (type)
                {
                    case "Normal":
                        //Check for just  a Carriage Return
                        if (string.IsNullOrEmpty(CommandTXT1.Text))
                        {
                            MainViewModel.CurrentCommand = "";
                             //MessageBox.Show(CommandTXT1.Text);
                            if (_serialPort1.IsOpen)
                                _serialPort1.Write("\r");

                            // _serialPort1.Write(bytes, 0, bytes.Length);

                        }
                        else
                        {
                            MainViewModel.CurrentCommand = CommandTXT1.Text;
                            if (_serialPort1.IsOpen)
                            _serialPort1.Write(CommandTXT1.Text + "\r"); // + "\r");

                        }
                        break;
                    case "CTRL]":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            if (_serialPort1.BreakState == false)
                            {
                                //ctrlb
                                if (_serialPort1.IsOpen)
                                    _serialPort1.Write(new byte[] { 29 }, 0, 1);

                            }
                            else
                            {
                                // _serialPort1.BreakState = false;
                                MessageBox.Show("Please switch off Break State and run the command again.");

                                // _serialPort1.Write(CommandTXT1.Text + "\r");
                            }

                        break;
                    case "CTRLB":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            if (_serialPort1.BreakState == false)
                            {
                                //ctrlb
                                if (_serialPort1.IsOpen)
                                    _serialPort1.Write(new byte[] { 2 }, 0, 1);

                            }
                            else
                            {
                                // _serialPort1.BreakState = false;
                                MessageBox.Show("Please switch off Break State and run the command again.");

                                // _serialPort1.Write(CommandTXT1.Text + "\r");
                            }

                        break;
                    case "CTRLC":
                        MainViewModel.CurrentCommand = "";
                        //ctrlc
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 3 }, 0, 1);

                        break;
                    case "CTRLD":
                        MainViewModel.CurrentCommand = "";

                        //ctrld
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 4 }, 0, 1);
                        break;
                    case "CTRLE":
                        MainViewModel.CurrentCommand = "";

                        //ctrld
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 5 }, 0, 1);
                        break;
                    case "CTRLF":
                        MainViewModel.CurrentCommand = "";

                        //ctrlf
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 6 }, 0, 1);
                        break;

                    case "CTRLP":
                        MainViewModel.CurrentCommand = "";

                        //ctrlP
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 16 }, 0, 1);
                        break;
                    case "CTRLS":
                        MainViewModel.CurrentCommand = "";

                        //ctrlS
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 19 }, 0, 1);
                        break;
                    case "CTRLX":
                        MainViewModel.CurrentCommand = "";

                        //ctrlX
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 24 }, 0, 1);
                        break;
                    case "CTRLZ":
                        MainViewModel.CurrentCommand = "";

                        //ctrlX
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 26 }, 0, 1);
                        break;
                    case "SHIFTS":
                        MainViewModel.CurrentCommand = "";
                        //MessageBox.Show("Here");
                        if (_serialPort1.IsOpen)
                            _serialPort1.WriteLine(string.Format("{0}{1}", ModifierKeys.Shift, Key.S));
                        // _serialPort1.WriteLine(string.Format("{0}{1}", "\x10\x53", CR));
                        //
                        break;

                }//end switch
            }//end if
            else if (vm.ProductBrand == "Juniper" || vm.ProductBrand == "Netgear" || vm.ProductBrand == "Mellanox" || vm.ProductBrand == "Dell" || vm.ProductBrand == "Draytek" || vm.ProductBrand == "IBM")
            {
                //needs to go through the appendstring parser to strip out characters


                switch (type)
                {
                    case "Normal":
                        //Check for just  a Carriage Return
                        if (string.IsNullOrEmpty(CommandTXT1.Text))
                        {
                            MainViewModel.CurrentCommand = "";
                            // MessageBox.Show("HP Blank Return");
                            if (_serialPort1.IsOpen)
                                _serialPort1.Write("\r");

                        }
                        else
                        {
                            MainViewModel.CurrentCommand = CommandTXT1.Text;
                            if (_serialPort1.IsOpen)
                                _serialPort1.Write(CommandTXT1.Text.Trim() + "\r"); // + "\r");

                        }
                        break;
                    case "CTRL]":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            if (_serialPort1.BreakState == false)
                            {
                                //ctrlb
                                if (_serialPort1.IsOpen)
                                    _serialPort1.Write(new byte[] { 29 }, 0, 1);

                            }
                            else
                            {
                                // _serialPort1.BreakState = false;
                                MessageBox.Show("Please switch off Break State and run the command again.");

                                // _serialPort1.Write(CommandTXT1.Text + "\r");
                            }

                        break;
                    case "CTRLB":
                        MainViewModel.CurrentCommand = "";

                        if (_serialPort1.IsOpen)
                            if (_serialPort1.BreakState == false)
                            {
                                //ctrlb
                                if (_serialPort1.IsOpen)
                                    _serialPort1.Write(new byte[] { 2 }, 0, 1);

                            }
                            else
                            {
                                // _serialPort1.BreakState = false;
                                MessageBox.Show("Please switch off Break State and run the command again.");

                                // _serialPort1.Write(CommandTXT1.Text + "\r");
                            }

                        break;
                    case "CTRLC":
                        MainViewModel.CurrentCommand = "";
                        //ctrlc
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 3 }, 0, 1);

                        break;
                    case "CTRLD":
                        MainViewModel.CurrentCommand = "";

                        //ctrld
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 4 }, 0, 1);
                        break;
                    case "CTRLE":
                        MainViewModel.CurrentCommand = "";

                        //ctrld
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 5 }, 0, 1);
                        break;
                    case "CTRLF":
                        MainViewModel.CurrentCommand = "";

                        //ctrlf
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 6 }, 0, 1);
                        break;

                    case "CTRLP":
                        MainViewModel.CurrentCommand = "";

                        //ctrlP
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 16 }, 0, 1);
                        break;
                    case "CTRLS":
                        MainViewModel.CurrentCommand = "";

                        //ctrlS
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 19 }, 0, 1);
                        break;
                    case "CTRLX":
                        MainViewModel.CurrentCommand = "";

                        //ctrlX
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 24 }, 0, 1);
                        break;
                    case "CTRLZ":
                        MainViewModel.CurrentCommand = "";

                        //ctrlX
                        if (_serialPort1.IsOpen)
                            _serialPort1.Write(new byte[] { 26 }, 0, 1);
                        break;
                    case "SHIFTS":
                        MainViewModel.CurrentCommand = "";
                        //MessageBox.Show("Here");
                        if (_serialPort1.IsOpen)
                            _serialPort1.WriteLine(string.Format("{0}{1}", ModifierKeys.Shift, Key.S));
                        // _serialPort1.WriteLine(string.Format("{0}{1}", "\x10\x53", CR));
                        //
                        break;

                }//end switch
              }//end if

            }




        public void OpenSinglePort(List<SerialPort> comlst, String portname, String baud1, String parity1, String databit1, String stopbit1, String handshake1)
        {

           bool error = false;

            // MessageBox.Show(portname + " " + baud1 + " " + parity1 + " " + databit1 + " " + stopbit1 + " " + handshake1);

            foreach (var cp in comlst)
            {
                //If the port is open, close it.
                if (cp.IsOpen)
                    cp.Close();
                else
                {
                    // Set the port's settings
                    //cp.PortName = portname;
                    //cp.BaudRate = 9600; //Convert.ToInt32(baud);
                    //cp.Parity =  (Parity)Enum.Parse(typeof(Parity), "None");
                    //cp.DataBits = int.Parse("8");
                    //cp.StopBits = (StopBits)Enum.Parse(typeof(StopBits), "1");
                    //cp.Handshake = (Handshake)Enum.Parse(typeof(Handshake), "None");

                    cp.PortName = portname;
                    cp.BaudRate = Convert.ToInt32(baud1);
                    cp.Parity = (Parity)Enum.Parse(typeof(Parity), parity1);
                    cp.DataBits = Convert.ToInt32(databit1);
                    cp.StopBits = (StopBits)Enum.Parse(typeof(StopBits), stopbit1);
                    cp.Handshake = (Handshake)Enum.Parse(typeof(Handshake), handshake1);


                    try
                    {
                        // Open the port
                        cp.Open();
                    }
                    catch (UnauthorizedAccessException) { error = true; Console.WriteLine(error.ToString()); }
                    catch (IOException) { error = true; Console.WriteLine(error.ToString()); }
                    catch (ArgumentException) { error = true; Console.WriteLine(error.ToString()); }

                    //if (error) MessageBox.Show(this, "Could not open the COM port.  Most likely it is already in use, has been removed, or is unavailable.", "COM Port Unavalible", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //else
                    //{
                    //    // Show the initial pin states
                    //    UpdatePinState();
                    //    //chkDTR.Checked = comport.DtrEnable;
                    //    //chkRTS.Checked = comport.RtsEnable;
                    //}
                    // If the port is open, send focus to the send data box
                    if (cp.IsOpen)
                    {
                        CommandTXT1.Focus();
                        //if (chkClearOnOpen.Checked) ClearTerminal();
                        ConsoleIncoming = "";
                        ClearTerminal(ConsoleRTB1);
                    }
                }
            }

            //Send a return to show prompt
            SendData("Normal");
            //switch (vm.ProductBrand)
            //{
            //    case "Cisco":
            //        SendData("Normal");
            //        break;
            //    case "HP":
            //        SendData("Normal");
            //        break;
            //    case "Juniper":
            //        SendData("Normal");
            //        break;
            //    case "Dell":
            //        SendData("Normal");
            //        break;
            //    case "IBM":
            //        SendData("Normal");
            //        break;
            //    case "Netgear":
            //        SendData("Normal");
            //        break;
            //    case "Draytek":
            //        SendData("Normal");
            //        break;
            //}



            //if (vm.ProductBrand == "Cisco")
            //{
            //    //just send a quick enter
            //    if(_serialPort1.IsOpen)
            //    _serialPort1.Write("\r\r");
            //}
            //else if (vm.ProductBrand == "HP")
            //{
            //    if (_serialPort1.IsOpen)
            //        _serialPort1.Write("\r");
            //}
           



            // Change the state of the form's controls
            //EnableControls();


        }

    

    





        public void OpenMultiPorts(List<SerialPort> comlst, string portname, string baud, string parity, string databit, string stopbit, string handshake)
        {

            bool error = false;

            foreach (var cp in comlst)
            {
                // If the port is open, close it.
                if (cp.IsOpen)
                    cp.Close();
                else
                {
                    // Set the port's settings
                    //cp.PortName = comboBox1.Text;
                    cp.BaudRate = int.Parse(baud);
                    cp.Parity = (Parity)Enum.Parse(typeof(Parity), parity);
                    cp.DataBits = int.Parse(databit);
                    cp.StopBits = (StopBits)Enum.Parse(typeof(StopBits), stopbit);



                    try
                    {
                        // Open the port
                        cp.Open();
                    }
                    catch (UnauthorizedAccessException) { error = true; Console.WriteLine(error.ToString()); }
                    catch (IOException) { error = true; }
                    catch (ArgumentException) { error = true; }

                    //if (error) MessageBox.Show(this, "Could not open the COM port.  Most likely it is already in use, has been removed, or is unavailable.", "COM Port Unavalible", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //else
                    //{
                    //    // Show the initial pin states
                    //    UpdatePinState();
                    //    //chkDTR.Checked = comport.DtrEnable;
                    //    //chkRTS.Checked = comport.RtsEnable;
                    //}
                }






                // Change the state of the form's controls
                //EnableControls();

                // If the port is open, send focus to the send data box
                if (cp.IsOpen)
                {
                    // textBox1.Focus();
                    //if (chkClearOnOpen.Checked) ClearTerminal();
                    ClearTerminal(ConsoleRTB1);
                }
            }



        }


        private void ClearTerminal(RichTextBox rtb)
        {
            //Clear the RichText Console Window
            ConsoleRTB1.SelectAll();
            ConsoleRTB1.Cut();
            //reset the indicator textblocks
            MsgSuccess1.Text = "";
            //MsgSuccess1.Background = Brushes.Transparent;
            //MsgSuccess1.Padding = new Thickness(0);
            MsgWarning1.Text = "";
            //MsgWarning1.Background = Brushes.Transparent;
            //MsgWarning1.Padding = new Thickness(0);
            MsgError1.Text = "";
          
            //MsgSuccess1.Background = Brushes.Transparent;
            //MsgSuccess1.Padding = new Thickness(0);
            MsgWarning1.Text = "";
            //MsgWarning1.Background = Brushes.Transparent;
            //MsgWarning1.Padding = new Thickness(0);
            MsgError1.Text = "";

            MsgNormCPU.Text = "";
            MsgMedCPU.Text = "";
            MsgHighCPU.Text = "";

            //MsgError1.Background = Brushes.Transparent;
            //MsgError1.Padding = new Thickness(0);
            FlowDoc1.Blocks.Clear();
            //Clear Backing Properties
            ConsoleIncoming = String.Empty;
            ConsoleContent1 = String.Empty;
            ConsoleRTB1.Document.Blocks.Clear();
            CommandTXT1.Text = "";
            //Clear the Awaiting Prompt backing property
            vm.SerialCompleteLine1 = "";
            LB1.Items.Clear();
            //vm.CMDHistory.Clear();
        }

      

        private void CommandTXT1_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (String.IsNullOrEmpty(CommandTXT1.Text))
            {

            }
            else
            {
                vm.CommandInstruction = CommandTXT1.Text.Trim();

            }
            
        }

        //ARROWS ONLY FIRE IN THE PREVIEW KEYDOWN
        private void CommandTXT1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            //int HistoryItem = 0;


            if (e.Key == Key.Up)
            {
                //Send Data To Serial Port
                //add command to list
              
                string historicCMD = "";
                

                if (vm.CMDHistory != null)
                    if (vm.CMDHistory.Count > 0)
                    {
                       

                        //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                        if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                        {
                            vm.CMDHistoryIndex = 0;
                            //MessageBox.Show("Zero");
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            //MessageBox.Show(historicCMD);
                            CommandTXT1.Text = historicCMD;
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);
                                                       
                            //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                            //MessageBox.Show(historicCMD);
                            CommandTXT1.Text = historicCMD;
                        }

                        //increase index by one
                        vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                        //MessageBox.Show(vm.CMDHistory.Count().ToString());

                    }

            }

            //if (e.Key == Key.Down)
            //{
            //    //Send Data To Serial Port
            //    //add command to list

            //    string historicCMD = "";


            //    if (vm.CMDHistory != null)
            //        if (vm.CMDHistory.Count > 0)
            //        {


            //            if (vm.CMDHistoryIndex < 0)
            //            {
            //                vm.CMDHistoryIndex = 0;
            //                //MessageBox.Show("Zero");
            //                historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

            //                //MessageBox.Show(vm.CMDHistoryIndex.ToString());
            //                //MessageBox.Show(historicCMD);
            //                CommandTXT1.Text = historicCMD;
            //            }
            //            else
            //            {
            //                historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

            //                //MessageBox.Show(vm.CMDHistoryIndex.ToString());
            //                //MessageBox.Show(historicCMD);
            //                CommandTXT1.Text = historicCMD;
            //            }

            //            //increase index by one
            //            vm.CMDHistoryIndex = vm.CMDHistoryIndex -1;

            //            //   MessageBox.Show(vm.CMDHistory.Count().ToString());

            //        }

            //    }
                //if (e.Key == Key.Up)
                //{
                //    MessageBox.Show("YEP");
                //}
            }

        private void CommandTXT1_KeyDown(object sender, KeyEventArgs e)
        {
         
            if(e.Key == Key.Enter)
            {
                //Send Data To Serial Port
                if(SelectBrandCB.Text != "Select Brand")
                {

                    //add command to history list
                    if(vm.CMDHistory.Contains(CommandTXT1.Text))
                    {
                        //do nothing
                    }
                    else
                    {
                        if (CommandTXT1.Text != "")
                        {
                            vm.CMDHistory.Add(CommandTXT1.Text);
                            
                        }
                    }

                    // Send Command to serial port
                    SendData("Normal");
                   CommandTXT1.Clear();
                }
                else
                {
                  CommandTXT1.Clear();
                  vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            //if (e.Key == Key.Tab)
            //{
            //    MessageBox.Show("TAB");
            //}

            //if (e.Key == Key.Right)
            //{
            //    MessageBox.Show("Right Arrow");
            //}







            //if (e.Key == Key.Space)
            //{
            //    //Send Data To Serial Port
            //    if (SelectBrandCB.Text != "Select Brand")
            //    {
            //        // Send Command to serial port
            //        SendData("Space");
            //        CommandTXT1.Clear();
            //    }
            //    else
            //    {
            //        CommandTXT1.Clear();
            //        vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
            //    }
            //}
            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
               // MessageBox.Show("Break");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
               // MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
               // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
               // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }
       
            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }

            ////Check if  Pressed
            //if (e.Key == ConsoleSpecialKey.ControlBreak)
            //{
            //    //MessageBox.Show("CTRLZ");
            //    // Send Command to serial port
            //    SendData("SHIFTS");
            //    CommandTXT1.Clear();
            //}

        }

      
    //TO CYCLE THROUGH ADDED ITEMS TO CMDHISTORY LIST
    public string GetNextElement(List<string> list, int index)
        {
           
            if ((index > list.Count - 1) || (index < 0))
                throw new Exception("Invalid index");

            else if (index == list.Count - 1)
                index = 0;

            else
                index++;

            return list[index];
        }

        private void OpenPortBtn_Click(object sender, RoutedEventArgs e)
        {


            //Save Serial Options to settings file
            SaveSettings();
            //Clear the ports list
            prts.Clear();
            //Add the serialport 
            prts.Add(_serialPort1);
           // prts.Add(SP1);
            //prts.Add(_serialPort2);
            //OpenSinglePort(prts, ComPortCB.Text, BaudCB.Text, ParityCB.Text, DataBitsCB.Text, StopBitsCB.Text, HandShakeCB.Text);
            ////Set on\off indicator color

            //OnOff.Fill = Brushes.Green;


            //Single
            if (ComPortCB.SelectedItem == null && BaudCB.SelectedItem == null && ParityCB.SelectedItem == null && DataBitsCB.SelectedItem == null && StopBitsCB.SelectedItem == null && HandShakeCB.SelectedItem == null)
            {
                MessageBox.Show("Please select a COM Port!");
            }
            else if (ComPortCB.SelectedItem == null)
            {
                MessageBox.Show("Please select a COM Port!");
            }
            else
            {
                OpenSinglePort(prts, ComPortCB.Text, BaudCB.Text, ParityCB.Text, DataBitsCB.Text, StopBitsCB.Text, HandShakeCB.Text);
                //Set on\off indicator color
                if(_serialPort1.IsOpen)
                OnOff.Fill = Brushes.Green;
                //MultiPort Version
                //OpenMultiPorts(prts, ComPortCB.SelectedItem.ToString(), BaudCB.SelectedItem.ToString(), ParityCB.SelectedItem.ToString(), DataBitsCB.SelectedItem.ToString(), StopBitsCB.SelectedItem.ToString(), HandShakeCB.SelectedItem.ToString());
            }

            if (_serialPort1.IsOpen)
                _serialPort1.Write("\r");
            ConsoleRTB1.Focus();
            //CommandTXT1.Focus();
        }

        private void ClosePortBtn_Click(object sender, RoutedEventArgs e)
        {
            //Close all ports added to the collection
            CloseAllSerialPorts();
            
        }

        //Close all serial ports
        public void CloseAllSerialPorts()
        {
            //Loop through ports list and close if open
            foreach (var sp in prts)
            {
                try
                {
                    if (sp.IsOpen)
                    {
                        //MessageBox.Show("Port Open Close Port");
                        //FlowDoc1 = new FlowDocument();
                        //ConsoleContent = string.Empty;
                        //ConsoleIncoming = string.Empty;
                        ClearTerminal(ConsoleRTB1);
                        //Set on\off indicator color
                        OnOff.Fill = Brushes.Red;
                        CommandTXT1.Focus();
                        sp.Close();
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }

            //clear the ports list
            prts.Clear();

            //set the combobox back to selecting nothing
            SelectBrandCB.SelectedIndex = 0;
            ProductListCB.SelectedIndex = -1;
           
        }


        private void LoadSettings()
        {
           //Load Local Settings from Application
            ComPortCB.SelectedIndex = ComPortCB.Items.IndexOf("COM5");
            BaudCB.SelectedIndex = BaudCB.Items.IndexOf("9600");
            ParityCB.SelectedIndex = ParityCB.Items.IndexOf("None");
            DataBitsCB.SelectedIndex = DataBitsCB.Items.IndexOf("8");
            StopBitsCB.SelectedIndex = StopBitsCB.Items.IndexOf("One");
            HandShakeCB.SelectedIndex = HandShakeCB.Items.IndexOf("None");


        }

        private void SaveSettings()
        {

            //MessageBox.Show(BaudCB.SelectedValue.ToString());
            if(ComPortCB.SelectedValue != null)
            {
                settings.PortName = ComPortCB.SelectedValue.ToString();
            }
            else
            {
                //MessageBox.Show("Please Select a Com Port");
            }

            settings.BaudRate = BaudCB.SelectedItem.ToString();
            settings.Parity = ParityCB.SelectedItem.ToString();
            settings.DataBits = DataBitsCB.SelectedItem.ToString();
            settings.StopBits = StopBitsCB.SelectedItem.ToString();
            settings.HandShake = HandShakeCB.SelectedItem.ToString();


            //settings.PortName = "COM5";
            //settings.BaudRate = "9600";
            //settings.Parity = "None";
            //settings.DataBits = "8";
            //settings.StopBits = "One";
            //settings.HandShake = "None";

            //settings.ClearOnOpen = chkClearOnOpen.Checked;
            //settings.ClearWithDTR = chkClearWithDTR.Checked;

            settings.Save();
        }

       
               
        private void SendBtn_Click(object sender, RoutedEventArgs e)
        {
            //Send Data To Serial Port
            SendData("Normal");
            CommandTXT1.Clear();
        }

                      

        private void ReportBtn_Click(object sender, RoutedEventArgs e)
        {

            //CopyRTBBlocks(ConsoleRTB1);

            //Call on function and pass in the richtextbox to crawl through
            //vm.GenerateReport(ConsoleRTB1);

            //how do i pull in the reports from all richtextboxes then cycle through the results through the left and right button clicks
            //if(vm.ReportCollection != null)
            //vm.ReportCollection.Clear();


            vm.ReportCollection = new List<string>();
            foreach (RichTextBox rtb in Helper.FindControlsOfType<RichTextBox>(this))
            {
                // MessageBox.Show(rtb.Name);
                //if the richtextbox is not empty then add to the collection
                // Add the found richtextbox to the collection
                if (vm.ConvertRichTextBoxContentsToString(rtb) != string.Empty)
                    vm.ReportCollection.Add(vm.GenerateReportCluster(rtb));
            }

            //ListView lv = Helper.GetDescendantFromName(Application.Current.MainWindow, "StepsLV") as ListView;

            vm.LoadReport = new TestReportFlowDocument(vm);

            vm.LoadWindow = new ReportViewWindow(vm);

            vm.LoadWindow.ShowDialog();


          //  ClearTerminal(ConsoleRTB1);


        }

        public void RTBHighlightText(string word) 
        {
            Paragraph p = (Paragraph)ConsoleRTB1.Document.Blocks.FirstBlock;
            String originalRunText = ((Run)p.Inlines.FirstInline).Text;
            //word = "Cisco";

            var textSearchRange = new TextRange(p.ContentStart, p.ContentEnd);
            Int32 position = textSearchRange.Text.IndexOf(word, StringComparison.OrdinalIgnoreCase);
        }

        public void CopyRTBBlocks(RichTextBox rtb)
        {
     
            Paragraph p = (Paragraph)rtb.Document.Blocks.FirstBlock;

            foreach(Block blk in ConsoleRTB1.Document.Blocks)
            {
                vm.ParaCollection.Add((Paragraph)blk);
            }

        }

        string ConvertRichTextBoxContentsToString(RichTextBox rtb)
        {
            TextRange textRange = new TextRange(rtb.Document.ContentStart, rtb.Document.ContentEnd);
            
            return textRange.Text;
        }

        private void ConsoleRTB1_SelectionChanged(object sender, RoutedEventArgs e)
        {
           
        }


        private void ConsoleRTB1_TextChanged(object sender, TextChangedEventArgs e)
        {
            // look into object sender
            // if sender is this console then

            
           // if (ConsoleIncoming != null)
            if (((RichTextBox)sender).Name == "ConsoleRTB1")
            {
                // Capture what was typed in direct to the console RTB || myText.Contains(">") y
                var myText = new TextRange(ConsoleRTB1.CaretPosition.GetLineStartPosition(0), ConsoleRTB1.CaretPosition.GetLineStartPosition(1) ?? ConsoleRTB1.CaretPosition.DocumentEnd).Text;


                //int prompt = myText.LastIndexOf('#');
               //if(myText.Last() == '#')
               // {
                   
               // }
               // MessageBox.Show(myText.Last().ToString());
                if (myText.Count() > 0 && myText.Contains("#") || myText.Contains(">") || myText.Contains("]:") || myText.Contains("]?") || myText.Contains("?") || myText.Contains(":"))                
                //if (myText.Last() == '#' || myText.Last() == '>' || myText.Contains("]:") || myText.Contains("]?") || myText.Last() == '?' || myText.Last() == ':')
                {
                    // needs work
                    if (ConsoleRTB1.IsFocused)
                    {
                        vm.CaptureRTBInput(CommandTXT1, myText);
                    }
                }




                // Check for flash and questions on initial boot
                vm.FlashCheck(ConsoleContent1, LB1, ConsoleRTB1, _serialPort1);

            }

        }
        private void ConsoleRTB1_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

           //Get the Start Claret Position
            TextPointer start = ConsoleRTB1.CaretPosition;
            //Get the end position
            TextPointer end = start.GetNextContextPosition(LogicalDirection.Forward);
                   
            //Set the typed text to Purple
            ConsoleRTB1.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);

           
        }

        private void ConsoleRTB1_TextInput(object sender, TextCompositionEventArgs e)
        {
           
        }

    

        private void ConsoleRTB1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {


                // CommandTXT1.Text += e.Key;
                //prevent deletion of a prompt
            if (e.Key == Key.Back)
            {
                    //MessageBox.Show("NowHere");

                    TextRange txtRange1 = new TextRange(ConsoleRTB1.Selection.Start, ConsoleRTB1.Selection.End);
                    if (txtRange1.Text.Count() < 0)
                    {
                    TextRange txtRange2 = new TextRange(txtRange1.Start.GetNextInsertionPosition(LogicalDirection.Backward).GetInsertionPosition(LogicalDirection.Forward), txtRange1.Start);


                    if (txtRange2.Text.Length >= 1)
                    {
                        if (txtRange2.Text.Last() == '>' || txtRange2.Text.Last() == '#')
                        {
                            // MessageBox.Show(txtRange2.Text.Last().ToString());
                            //Stop delete
                            ConsoleRTB1.IsReadOnly = true;
                            e.Handled = true;
                            ConsoleRTB1.IsReadOnly = false;

                        }
                    }
                }
            }
            

           if (e.Key == Key.Enter)
            {

                // MessageBox.Show("Enter Pressed");
                //Send Data To Serial Port
                if (SelectBrandCB.Text != "Select Brand")
                {

                    //add command to history list
                    if (vm.CMDHistory.Contains(CommandTXT1.Text))
                    {
                        //do nothing
                    }
                    else
                    {
                        if (CommandTXT1.Text != "")
                        {
                            if(CommandTXT1.Text == "going a cold start")
                            {
                                CommandTXT1.Text = "";
                            }

                            vm.CMDHistory.Add(CommandTXT1.Text);
                        }
                    }

                    // Send Command to serial port
                    SendData("Normal");
                    CommandTXT1.Clear();



                }
                else
                {
                    CommandTXT1.Clear();
                    vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
                }
            }


            if (e.Key == Key.RightCtrl)
                {
                string historicCMD = "";


                    if (vm.CMDHistory != null)
                        if (vm.CMDHistory.Count > 0)
                        {


                            //historicCMD = GetNextElement(vm.CMDHistory,HistoryItem);
                            if (vm.CMDHistoryIndex > vm.CMDHistory.Count - 1)
                            {
                                vm.CMDHistoryIndex = 0;
                                //MessageBox.Show("Zero");
                                historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                                //MessageBox.Show(vm.CMDHistoryIndex.ToString());
                               // MessageBox.Show(historicCMD);
                               // ConsoleRTB1.Document.Blocks.Add(new Run(historicCMD));
                            if (ConsoleRTB1 != null && ConsoleRTB1.Document != null)
                            {
                                // Inserts the text at current selection. 
                              
                                TextPointer start = ConsoleRTB1.CaretPosition;

                                // start.GetLineStartPosition
                               // TextPointer end = start.DeleteTextInRun();

                                start.DeleteTextInRun(historicCMD.Count());

                               //colour selected text
                                ConsoleRTB1.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.MediumPurple);


                               // start.InsertTextInRun(historicCMD);
                                ConsoleRTB1.Selection.Text = historicCMD;// InsertText("Hello World!!!");
                                CommandTXT1.Text = historicCMD;

                            }
                        }
                        else
                        {
                            historicCMD = vm.CMDHistory.ElementAt(vm.CMDHistoryIndex);

                            ConsoleRTB1.Selection.Text = historicCMD;
                            CommandTXT1.Text = historicCMD;
                        }

                            //increase index by one
                            vm.CMDHistoryIndex = vm.CMDHistoryIndex + 1;

                            //MessageBox.Show(vm.CMDHistory.Count().ToString());

                        }

               
            }




            //SPECIAL KEYS
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Oem6)
            {
                // MessageBox.Show("]");
                // Send Command to serial port
                SendData("CTRL]");
                CommandTXT1.Clear();
            }
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Cancel)
            {
             
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }

            //Check if CTRL AND B Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.B)
            {
                //MessageBox.Show("CTRLB");
                // Send Command to serial port
                SendData("CTRLB");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND C Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.C)
            {
                //MessageBox.Show("CTRLC");
                // Send Command to serial port
                SendData("CTRLC");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND D  Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.D)
            {
                //MessageBox.Show("CTRLD");
                // Send Command to serial port
                SendData("CTRLD");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND F Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.F)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLF");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND P Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.P)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLP");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.S)
            {
                // MessageBox.Show("CTRLF");
                // Send Command to serial port
                SendData("CTRLS");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND X Pressed  CANCEL
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.X)
            {
                // MessageBox.Show("CTRLX");
                // Send Command to serial port
                SendData("CTRLX");
                CommandTXT1.Clear();
            }
            //Check if CTRL AND Z Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.Z)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("CTRLZ");
                CommandTXT1.Clear();
            }

            //Check if SHIFT S Pressed
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.Key == Key.S)
            {
                //MessageBox.Show("CTRLZ");
                // Send Command to serial port
                SendData("SHIFTS");
                CommandTXT1.Clear();
            }
               }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }


        private void ConsoleRTB1_KeyDown(object sender, KeyEventArgs e)
        {

         

        }


        // Returns a string containing the text content between two specified TextPointers.
        public string GetTextBetweenTextPointers(TextPointer start, TextPointer end)
        {
            StringBuilder buffer = new StringBuilder();

            while (start != null && start.CompareTo(end) < 0)
            {
                if (start.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text)
                    buffer.Append(start.GetTextInRun(LogicalDirection.Forward));

                // Note that when the TextPointer points into a text run, this skips over the entire
                // run, not just the current character in the run.
                start = start.GetNextContextPosition(LogicalDirection.Forward);
            }
            return buffer.ToString();
        } // End GetTextBetweenPointers.

        private void ReplaceWordAtPointer(TextPointer textPointer, string replacementWord)
        {
            textPointer.DeleteTextInRun(-GetWordCharactersBefore(textPointer).Count());
            textPointer.DeleteTextInRun(GetWordCharactersAfter(textPointer).Count());

            textPointer.InsertTextInRun(replacementWord);
        }

        private string GetWordCharactersBefore(TextPointer textPointer)
        {
            var backwards = textPointer.GetTextInRun(LogicalDirection.Backward);
            var wordCharactersBeforePointer = new string(backwards.Reverse().TakeWhile(c => !char.IsSeparator(c) && !char.IsPunctuation(c)).Reverse().ToArray());

            return wordCharactersBeforePointer;
        }

        private string GetWordCharactersAfter(TextPointer textPointer)
        {
            var fowards = textPointer.GetTextInRun(LogicalDirection.Forward);
            var wordCharactersAfterPointer = new string(fowards.TakeWhile(c => !char.IsSeparator(c) && !char.IsPunctuation(c)).ToArray());

            return wordCharactersAfterPointer;
        }



        private void DelFlash1_Click(object sender, RoutedEventArgs e)
        {


            vm.DeleteFlash(LB1, _serialPort1, ConsoleRTB1, MsgSuccess1, MsgWarning1, MsgError1);

        }

     
        private void AutoCommands_Click(object sender, RoutedEventArgs e)
        {
            //reset cancel
            vm.CancelAutoScript = false;

            //If both comboboxes have a selection then allow
            if (SelectBrandCB.Text != "Select Brand" && ProductListCB.SelectedItem != null )
            {
                if (ProductListCB.Text.Contains(SelectBrandCB.Text))
                {
                    //Clear the terminal of previous commands
                    ClearTerminal(ConsoleRTB1);
                    //Call the automated commands 
                    vm.AutomatedWipeCommands(_serialPort1, CommandTXT1, ConsoleRTB1, vm.SerialCompleteLine1, MsgScriptInProgress1, MsgSuccess1, MsgWarning1, MsgError1, vm.PasswordUnlock1);

                 
                }
                else
                {
                    vm.DialogMessage("Message", "Selected automation file does not match the selected brand.");
                }
               
               
            }
            else if(SelectBrandCB.Text == "Select Brand" && ProductListCB.SelectedItem != null || SelectBrandCB.Text != "Select Brand" && ProductListCB.SelectedItem != null)
            {
                vm.DialogMessage("Message", "Please select the brand and corresponding automation script.");
                //MessageBox.Show("Please select a product.");
            }


            

        }



        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

        private void ComPortCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem != null)
            {
                //SelectedUser = (sender as ComboBox).SelectedItem.ToString();
                if (_serialPort1.IsOpen)
                    _serialPort1.Close();
                    _serialPort1.PortName = ComPortCB.SelectedItem.ToString();

                //if (SP1.IsOpen)
                //    SP1.PortName = ComPortCB.SelectedItem.ToString();
            }


        
           
        }

        private void ClearBtn_Click(object sender, RoutedEventArgs e)
        {
            //Clear Terminal Output
            ClearTerminal(ConsoleRTB1);
          
        }

        private void ClearConsoleBtn_Click(object sender, RoutedEventArgs e)
        {
            ClearTerminal(ConsoleRTB1);
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //close serial ports on exit
            CloseAllSerialPorts();
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            //var msg = System.Text.Encoding.UTF8.GetBytes("\x1b[24;0HE\x1b[24;1H\x1b[24;18H\x1b[24;1H\x1b[2K\x1b[24;1H\x1b[?25h\x1b[24;1H\x1b[1;24r\x1b[24;1H\x1b[1;24r\x1b[24;1H\x1b[24;1H\x1b[2K\x1b[24;1H\x1b[?25h\x1b[24;1H\x1b[24;1HHP-2620-48-PoEP# \x1b[24;1H\x1b[24;18H\x1b[24;1H\x1b[?25h\x1b[24;18H\r\n");
            //string text = "\x1b[24;0HE\x1b[24;1H\x1b[24;18H\x1b[24;1H\x1b[2K\x1b[24;1H\x1b[?25h\x1b[24;1H\x1b[1;24r\x1b[24;1H\x1b[1;24r\x1b[24;1H\x1b[24;1H\x1b[2K\x1b[24;1H\x1b[?25h\x1b[24;1H\x1b[24;1HHP-2620-48-PoEP# \x1b[24;1H\x1b[24;18H\x1b[24;1H\x1b[?25h\x1b[24;18H\r\n";
            //MessageBox.Show("Event Fired");
            ////IDEA Send the byte in the var msg section to the sequence parser of the other program to ignore the escape characters and not append to the overall character set. Then use this to send to the write data function
            //vm.ProgressDialogProgress("Auto Script Running", "Running");
            //WriteData1(text);

            //XTerminalWindow xterm = new XTerminalWindow();
            //xterm.Show();
            // if(_serialPort1.IsOpen)
            //_serialPort1.Write("show version");

            MainViewModel.CurrentCommand = "";

            //byte byt = new byte();
            //byt

            //ReportWindow rp1 = new ReportWindow(vm);

            //rp1.Show();
            //ctrld
            //if (_serialPort1.IsOpen)
            //    _serialPort1.Write(new byte[] { 4 }, 0, 1);

            ////_serialPort1.WriteLine(string.Format("{0}{1}", CtrlD, CR));

            //SendData("CTRLD");
            //CommandTXT1.Clear();

            // await PutTaskDelay(7000);

            //Search for Text in console
            // RTBHighlightText(SearchTBX.Text);
           // double x = Convert.ToDouble(SearchTBX.Text);

                     
          //  MessageBox.Show(MyExtensions.ToSize(x, MyExtensions.SizeUnits.MB));

          // MessageBox.Show(MyExtensions.ToFileSize(x));
        }
    

        private void ProductListCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

   

        private void SelectBrandCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //MessageBox.Show(SelectBrandCB.SelectedValue);
            if (vm != null)
            {
                vm.CMDHistory.Clear();

                //Get the selected item and cast to a ComboBoxItem and get the string through the content property
                ComboBoxItem SelectedBrand = (ComboBoxItem)SelectBrandCB.SelectedItem;
                //Read in the XML Product files and add to the collection based on the selected brand
                vm.ReadProductFileCommand.Execute(SelectedBrand.Content);

                //check if anything other is selected, then return focus to the Command Textbox
                CommandTXT1.Focus();
                //vm.ProductBrand = SelectBrandCB.Text;
                //MessageBox.Show(vm.ProductBrand);

            }
              
        }

        private void EnterBTN_Click(object sender, RoutedEventArgs e)
        {
            if (_serialPort1.IsOpen)
            {
                _serialPort1.Write("\r");
                _serialPort1.Write("\r");
            }
        }


        //Select All Flash Items in Listbox
        private void SelAll1_Click(object sender, RoutedEventArgs e)
        {
            if(LB1.Items != null)
            {
                if(LB1.SelectedItems.Count == LB1.Items.Count)
                {
                    LB1.SelectedItems.Clear();
                }
                else
                {
                    LB1.SelectAll();
                }
            }
        
        }

        private void ConsoleFontSizeCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if ((sender as ComboBox).SelectedItem != null)
            {
                //SelectedUser = (sender as ComboBox).SelectedItem.ToString();
                // double cb1 = ConsoleFontSizeCB.SelectedItem.ToString();
                //MessageBox.Show(cb1.ToString()));

               

               if (ConsoleRTB1 != null)
                {
                    ////method 1
                    ComboBoxItem typeItem = (ComboBoxItem)ConsoleFontSizeCB.SelectedItem;
                    string value = typeItem.Content.ToString();
                    ConsoleRTB1.FontSize = Convert.ToInt32(value);
                   
                    vm.ConsoleFontSize = ConsoleRTB1.FontSize;
                    //*PromptTB1.FontSize = Convert.ToInt32(value);
                    //method2
                    //ConsoleRTB1.FontSize = Convert.ToInt32(ConsoleFontSizeCB.Text);

                  
                    //double rtbfontsz = (ConsoleFontSizeCB.SelectedItem.ToString());

                }
                

                //if (SP1.IsOpen)
                //    SP1.PortName = ComPortCB.SelectedItem.ToString();
            }
          
        }

        private void ConsoleColourCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem != null)
            {
            
                if (ConsoleRTB1 != null)
                {
                    ////method 1
                    //ComboBoxItem typeItem = (ComboBoxItem)ConsoleColourCB.SelectedIndex;
                   
                    switch (ConsoleColourCB.SelectedIndex)
                    {
                        case 0:
                            ConsoleRTB1.Foreground = Brushes.White;
                            vm.ConsoleColour = Brushes.White;
                            //MessageBox.Show("yup white");
                            break;
                        case 1 :
                            ConsoleRTB1.Foreground = Brushes.LimeGreen;
                            vm.ConsoleColour = Brushes.LimeGreen;
                            break;
                        case 2:
                            ConsoleRTB1.Foreground = Brushes.PowderBlue;
                            vm.ConsoleColour = Brushes.PowderBlue;
                            break;
                        case 3:
                            ConsoleRTB1.Foreground = Brushes.Orange;
                            vm.ConsoleColour = Brushes.Orange;
                            break;
                        case 4:
                            ConsoleRTB1.Foreground = Brushes.Thistle;
                            vm.ConsoleColour = Brushes.Thistle;
                            break;
                    }
                   
                    //ConsoleRTB1.Foreground = 
                    //*PromptTB1.FontSize = Convert.ToInt32(value);
                    //method2
                    //ConsoleRTB1.FontSize = Convert.ToInt32(ConsoleFontSizeCB.Text);


                    //double rtbfontsz = (ConsoleFontSizeCB.SelectedItem.ToString());

                }


                //if (SP1.IsOpen)
                //    SP1.PortName = ComPortCB.SelectedItem.ToString();
            }
        }

        private async void BreakONBTN_Click(object sender, RoutedEventArgs e)
        {
            if (_serialPort1.IsOpen)
            {
                //Set the serial into a break state
                if (_serialPort1.IsOpen)
                {
                    _serialPort1.BreakState = true;
                    BreakModeRect1.Fill = Brushes.Green;
                    await PutTaskDelay(400);
                    _serialPort1.BreakState = false;
                    BreakModeRect1.Fill = Brushes.Red;
                    _serialPort1.BreakState = true;
                    BreakModeRect1.Fill = Brushes.Green;
                    await PutTaskDelay(400);
                    _serialPort1.BreakState = false;
                    BreakModeRect1.Fill = Brushes.Red;
                    _serialPort1.BreakState = true;
                    BreakModeRect1.Fill = Brushes.Green;
                    await PutTaskDelay(400);
                    _serialPort1.BreakState = false;
                    BreakModeRect1.Fill = Brushes.Red;
                    _serialPort1.BreakState = true;
                    BreakModeRect1.Fill = Brushes.Green;
                    await PutTaskDelay(400);
                    _serialPort1.BreakState = false;
                    BreakModeRect1.Fill = Brushes.Red;
                    _serialPort1.BreakState = true;
                    BreakModeRect1.Fill = Brushes.Green;
                    await PutTaskDelay(400);
                    _serialPort1.BreakState = false;
                    BreakModeRect1.Fill = Brushes.Red;
                    _serialPort1.BreakState = true;
                    BreakModeRect1.Fill = Brushes.Green;
                    await PutTaskDelay(400);
                    _serialPort1.BreakState = false;
                    BreakModeRect1.Fill = Brushes.Red;
                   
                }
            }
          
        }

        private void BreakOFFBTN_Click(object sender, RoutedEventArgs e)
        {
            //Set back the serial to a none break state
            if (_serialPort1.IsOpen)
                _serialPort1.BreakState = false;
            BreakModeRect1.Fill = Brushes.Red;
        }

        private void ESCBTN_Click(object sender, RoutedEventArgs e)
        {


            //Add escape character Pause `
            if (_serialPort1.IsOpen)
            _serialPort1.Write("\x1b");
            //_serialPort1.Write("\x00");
            //_serialPort1.Write("\x13");
            //_serialPort1.WriteLine(string.Format("{0}{1}", CtrlESC, CR));0x03
            //;

        }

        private void CancelAutoCommands_Click(object sender, RoutedEventArgs e)
        {
            MsgScriptInProgress1.Text = "";
            vm.AutoCLIVisibility = Visibility.Collapsed;
            vm.AutoCLIRunning = false;
            vm.CancelAutoScript = true;
        }

        private void CTRLBREAKBTN_Click(object sender, RoutedEventArgs e)
        {
            if (_serialPort1.IsOpen)
            {
                _serialPort1.WriteLine(string.Format("{0}{1}", Key.Pause, Key.Pause));
               
                //_serialPort1.BreakState = true;
                //_serialPort1.WriteLine(string.Format("{0}{1}", ModifierKeys.Control, Key.Pause));
            }
           
        }

        //private void DisplayPopup(object sender, RoutedEventArgs e)
        //{
        //    if(myPopup.IsOpen == false)
        //    {
        //        myPopup.IsOpen = true;
        //    }
        //    else
        //    {
        //        myPopup.IsOpen = false;
        //    }
           
        //}
    }
}
