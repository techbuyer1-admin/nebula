﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.ViewModels;
using Nebula.Properties;
using Nebula.Models;
using Nebula.Helpers;
using MahApps.Metro.Controls;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for ProcessCommandsView.xaml
    /// </summary>
    public partial class ProcessCommandsView : UserControl
    {

        //declare viewmodel
        MainViewModel vm;

        public ProcessCommandsView()
        {
            InitializeComponent();
            
        }

        public ProcessCommandsView(MainViewModel PassedViewModel)
        {
            InitializeComponent();
           
            //get the passed in viewmodel and assign to the local vm variable
            vm = PassedViewModel;
           
        }
     




        //private async void BackButton1_Click(object sender, RoutedEventArgs e)
        //{
        //    ProcessPiper pp = new ProcessPiper(vm);

        //    //FOR COMMAND LINE EXE
        //    //pp.Start(new System.IO.FileInfo(@"C:\Windows\System32\cmd.exe"), "", "ping 192.168.0.12 -n 4", @"C:\Windows\System32");

        //    await System.Threading.Tasks.Task.Run(() => pp.Start(new System.IO.FileInfo(@"C:\Windows\System32\cmd.exe"), "", vm.CommandInstruction, @"C:\Windows\System32"));
        //}


        //private void BackButton2_Click(object sender, RoutedEventArgs e)
        //{

        //    ProcessPiper pp = new ProcessPiper(vm);

        //    //FOR ILO RESTFUL
        //    // ilo rest full path  C:\Program Files\Hewlett Packard Enterprise\RESTful Interface Tool\ilorest.exe
        //    //pp.StartILORest(new System.IO.FileInfo(@"C:\Program Files\Hewlett Packard Enterprise\RESTful Interface Tool\ilorest.exe"), "", "", @"C:\Program Files\Hewlett Packard Enterprise\RESTful Interface Tool");

        //}

        //private void BackButton3_Click(object sender, RoutedEventArgs e)
        //{
        //    ProcessPiper pp = new ProcessPiper(vm);
        //    //FOR OLD XML ILO
        //    //Requires XML Path 
        //    ///pp.Start(new System.IO.FileInfo(@"C:\Program Files (x86)\Hewlett Packard Enterprise\HP Lights-Out Configuration Utility\hpqlocfg"), "", "help", @"C:\Program Files (x86)\Hewlett Packard Enterprise\HP Lights-Out Configuration Utility");
        //    pp.Start(new System.IO.FileInfo(@"C:\Program Files (x86)\Hewlett Packard Enterprise\HP Lights-Out Configuration Utility\hpqlocfg"), "", @" -f @"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\Scripts\Get_Embedded_Health.xml -s 192.168.101.34 -t user=Admin,password=" + vm.OverridePasswordS1.Trim() + "", @"C:\Program Files (x86)\Hewlett Packard Enterprise\HP Lights-Out Configuration Utility");
        //}
    }
}
