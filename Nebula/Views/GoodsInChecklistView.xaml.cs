﻿using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.ViewModels;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for GoodsInChecklistView.xaml
    /// </summary>
    public partial class GoodsInChecklistView : UserControl
    {
        // Here we create the viewmodel with the current DialogCoordinator instance 
      //GoodsInOutViewModel vm = new GoodsInOutViewModel(DialogCoordinator.Instance);

        public GoodsInChecklistView()
        {
            InitializeComponent();


            //this.DataContext = vm;
        }

        private void SelectAllBtn_Click(object sender, RoutedEventArgs e)
        {

          
            GIQ1CB.IsChecked = true;
            GIQ2CB.IsChecked = true;
            //GIQ3CB.IsChecked = true;
            GIQ4CB.IsChecked = true;
            GIQ5CB.IsChecked = true;
            GIQ6CB.IsChecked = true;
            GIQ7CB.IsChecked = true;
            GIQ8CB.IsChecked = true;
            GIQ9CB.IsChecked = true;
            GIQ10CB.IsChecked = true;
            GIQ11CB.IsChecked = true;
            GIQ12CB.IsChecked = true;
            GIQ13CB.IsChecked = true;
            GIQ14CB.IsChecked = true;
            GIQ15CB.IsChecked = true;
            //GIQ16CB.IsChecked = true;
            GIQ17CB.IsChecked = true;
            //GIQ18CB.IsChecked = true;
            GIQ19CB.IsChecked = true;
            GIQ20CB.IsChecked = true;
            GIQ21CB.IsChecked = true;
            GIQ22CB.IsChecked = true;
            //GIQ23CB.IsChecked = true;
            GIQ24CB.IsChecked = true;
            GIQ25CB.IsChecked = true;
            //MessageTB.Text = String.Empty;
        }

        private void ClearAllBtn_Click(object sender, RoutedEventArgs e)
        {
            GIQ1CB.IsChecked = false;
            GIQ2CB.IsChecked = false;
            //GIQ3CB.IsChecked = false;
            GIQ4CB.IsChecked = false;
            GIQ5CB.IsChecked = false;
            GIQ6CB.IsChecked = false;
            GIQ7CB.IsChecked = false;
            GIQ8CB.IsChecked = false;
            GIQ9CB.IsChecked = false;
            GIQ10CB.IsChecked = false;
            GIQ11CB.IsChecked = false;
            GIQ12CB.IsChecked = false;
            GIQ13CB.IsChecked = false;
            GIQ14CB.IsChecked = false;
            GIQ15CB.IsChecked = false;
            //GIQ16CB.IsChecked = false;
            GIQ17CB.IsChecked = false;
            //GIQ18CB.IsChecked = false;
            GIQ19CB.IsChecked = false;
            GIQ20CB.IsChecked = false;
            GIQ21CB.IsChecked = false;
            GIQ22CB.IsChecked = false;
            //GIQ23CB.IsChecked = false;
            GIQ24CB.IsChecked = false;
            GIQ25CB.IsChecked = false;
            //MessageTB.Text = String.Empty;
        }

        private void ChassisNoTXTB_TextChanged(object sender, TextChangedEventArgs e)
        {
           
        }

        //private void BackButton_Click(object sender, RoutedEventArgs e)
        //{

        //    //MessageBox.Show("Clicked");
        //    //MainViewModel mvm = new MainViewModel();

        //    //mvm.LoadContentCommand.Execute("GoodsInToolsView");
        //}
    }
}
