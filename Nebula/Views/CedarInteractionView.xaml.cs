﻿using Nebula.Helpers.Netsuite;
using Nebula.Models;
using Nebula.Models.WipeDrive;
using Nebula.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for CedarInteractionView.xaml
    /// </summary>
    public partial class CedarInteractionView : UserControl
    {

        MainViewModel vm;

        public CedarInteractionView()
        {
            InitializeComponent();
        }

        public CedarInteractionView(MainViewModel passedvm)
        {
            InitializeComponent();
            //Assign view model
            vm = passedvm;
        }

        private void CedarWipeDriveBTN_Click(object sender, RoutedEventArgs e)
        {
            //Set counts to zero
            int operationCount = 0;
            int jobCount = 0;

            //Clear Textbox
            ReturnedRespTXT.Text = "";

            // read file into a string and deserialize JSON to a type
            WipeDriveModel wd = JsonConvert.DeserializeObject<WipeDriveModel>(File.ReadAllText(@"C:\Users\n.myers\Documents\WipeDrive.json"));

            //Get top end properties
            ReturnedRespTXT.Text += "Product Name: " + wd.Report.ProductName + "\n";
            ReturnedRespTXT.Text += "Product Version: " + wd.Report.ProductVersion + "\n";
            ReturnedRespTXT.Text += "Group UUID: " + wd.Report.GroupUUID + "\n";
            ReturnedRespTXT.Text += "Kernel Version: " + wd.Report.KernelVersion + "\n";
            ReturnedRespTXT.Text += "Cloud Code: " + wd.CloudCode + "\n\n\n";


            ReturnedRespTXT.Text += "**** Wipe Drive Jobs ****" + "\n\n";

            //Loop through the job collection
            foreach (var wdJob in wd.Report.Jobs)
            {
                //increment
                jobCount += 1;

                //MessageBox.Show(wdJob.Name + "\n");
                ReturnedRespTXT.Text += "[Job " + jobCount.ToString() + " Start]" + "\n";
                ReturnedRespTXT.Text += "Name: " + wdJob.Name + "\n";
                ReturnedRespTXT.Text += "ComputerID: " + wdJob.ComputerID + "\n";
                ReturnedRespTXT.Text += "UserName: " + wdJob.UserName + "\n";
                ReturnedRespTXT.Text += "Number: " + wdJob.Number + "\n";
                ReturnedRespTXT.Text += "Options: " + wdJob.Options + "\n";
                ReturnedRespTXT.Text += "Pattern Index: " + wdJob.PatternIndex + "\n";
                ReturnedRespTXT.Text += "Pattern Name: " + wdJob.PatternName + "\n";
                ReturnedRespTXT.Text += "Pattern Short Name: " + wdJob.PatternShortName + "\n";
                ReturnedRespTXT.Text += "Tpm Action: " + wdJob.TpmAction + "\n";
                ReturnedRespTXT.Text += "Tpm Action: " + wdJob.TpmAction + "\n\n\n";



                //Loop through operations in each job
                foreach (var jobOperation in wdJob.Operations)
                {
                    //increment
                    operationCount += 1;

                    ReturnedRespTXT.Text += "[Operation " + operationCount.ToString() + " Start]" + "\n";
                    //Write out all operation properties
                    //ReturnedRespTXT.Text += "[Pre Test]" + "\n";
                    ReturnedRespTXT.Text += "Pre Test Result: " + jobOperation.PreTest.PreTestResult + "\n";
                    ReturnedRespTXT.Text += "Pre Test Seconds: " + jobOperation.PreTest.PreTestSeconds + "\n";
                    ReturnedRespTXT.Text += "Pre Test Sectors Read: " + jobOperation.PreTest.PreTestSectorsRead + "\n";
                    ReturnedRespTXT.Text += "Action Result: " + jobOperation.ActionResult + "\n";
                    ReturnedRespTXT.Text += "AMAX Found: " + jobOperation.AMAXFound + "\n";
                    ReturnedRespTXT.Text += "AMAX Removed: " + jobOperation.AMAXRemoved + "\n";
                    ReturnedRespTXT.Text += "DCO Found: " + jobOperation.DCOFound + "\n";
                    ReturnedRespTXT.Text += "DCO Lock Found: " + jobOperation.DCOLockFound + "\n";
                    ReturnedRespTXT.Text += "DCO Lock Removed: " + jobOperation.DCOLockRemoved + "\n";
                    ReturnedRespTXT.Text += "DcoLockWarning: " + jobOperation.DcoLockWarning + "\n";
                    ReturnedRespTXT.Text += "DCO Removed: " + jobOperation.DCORemoved + "\n";
                    ReturnedRespTXT.Text += "Dirty Sectors: " + jobOperation.DirtySectors + "\n";
                    ReturnedRespTXT.Text += "Drive Errors: " + jobOperation.DriveErrors + "\n";
                    ReturnedRespTXT.Text += "Enhanced Secure Erase Passes: " + jobOperation.EnhancedSecureErasePasses + "\n";
                    ReturnedRespTXT.Text += "Failure Reason: " + jobOperation.FailureReason + "\n";
                    ReturnedRespTXT.Text += "HPA Found: " + jobOperation.HPAFound + "\n";
                    ReturnedRespTXT.Text += "HPA Removed: " + jobOperation.HPARemoved + "\n";
                    ReturnedRespTXT.Text += "Nist Level Reason: " + jobOperation.NistLevelReason + "\n";
                    ReturnedRespTXT.Text += "Nist Wipe Level: " + jobOperation.NistWipeLevel + "\n";
                    ReturnedRespTXT.Text += "Opal Crypto Erase Passes: " + jobOperation.OpalCryptoErasePasses + "\n";
                    ReturnedRespTXT.Text += "Passes: " + jobOperation.Passes + "\n";
                    ReturnedRespTXT.Text += "Required Nist Wipe Level: " + jobOperation.RequiredNistWipeLevel + "\n";
                    ReturnedRespTXT.Text += "Sanitize Block Erase Passes: " + jobOperation.SanitizeBlockErasePasses + "\n";
                    ReturnedRespTXT.Text += "SanitizeCryptoErasePasses: " + jobOperation.SanitizeCryptoErasePasses + "\n";
                    ReturnedRespTXT.Text += "Sanitize Overwrite Passes: " + jobOperation.SanitizeOverwritePasses + "\n";
                    ReturnedRespTXT.Text += "SecurityCode: " + jobOperation.SecurityCode + "\n";
                    ReturnedRespTXT.Text += "Security Frozen Found: " + jobOperation.SecurityFrozenFound + "\n";
                    ReturnedRespTXT.Text += "Security Frozen Removed: " + jobOperation.SecurityFrozenRemoved + "\n";
                    ReturnedRespTXT.Text += "Security Locked: " + jobOperation.SecurityLocked + "\n";
                    ReturnedRespTXT.Text += "Storage Device UUID: " + jobOperation.StorageDeviceUUID + "\n";
                    ReturnedRespTXT.Text += "Time Ended : " + jobOperation.TimeEnded + "\n";
                    ReturnedRespTXT.Text += "Time Started: " + jobOperation.TimeStarted + "\n";
                    ReturnedRespTXT.Text += "Total Sectors: " + jobOperation.TotalSectors + "\n";
                    ReturnedRespTXT.Text += "Total Sectors Overwritten: " + jobOperation.TotalSectorsOverwritten + "\n";
                    ReturnedRespTXT.Text += "Total Sectors Verified: " + jobOperation.TotalSectorsVerified + "\n";
                    ReturnedRespTXT.Text += "Trim Passes: " + jobOperation.TrimPasses + "\n";
                    ReturnedRespTXT.Text += "UUID: " + jobOperation.UUID + "\n";
                    ReturnedRespTXT.Text += "Zero Passes Added: " + jobOperation.ZeroPassesAdded + "\n";
                    ReturnedRespTXT.Text += "[Operation End]" + "\n\n";


                    //inner collection

                }


                ReturnedRespTXT.Text += "**** Job End ****" + "\n\n";

            }
        }

        private async void CedarConnectBTN_Click(object sender, RoutedEventArgs e)
        {

            //Cedar
            try
            {

                CedarInteropModel newCeder = new CedarInteropModel();

                string returned = await Task.Run(() => newCeder.GetCedarRequest());

                // MessageBox.Show(returned);

                ReturnedRespTXT.Text = returned;
                //vm.ResponseData
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private async void CedarPOCreateBTN_Click(object sender, RoutedEventArgs e)
        {
            //Cedar
            try
            {
                CedarInteropModel newCeder = new CedarInteropModel();

                string returned = await Task.Run(() => newCeder.PostPOCedarRequest());

                //string returned = await Task.Run(() => newShip.GetCedarRequest());

                //MessageBox.Show(returned);

                ReturnedRespTXT.Text = returned;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }




        private async void NetsuiteAPITestBTN_Click(object sender, RoutedEventArgs e)
        {
            //var oauth1 = CreateOAuth1HeaderGenerator();
            //var baseString = oauth1.GenerateSignatureBaseString(_nonce, _timestamp);

            ////MessageBox.Show(oauth1.ToString());

            //MessageBox.Show(baseString.ToString());

            NetSuiteApiClient_UsingHttpClient ns = new NetSuiteApiClient_UsingHttpClient();
            //Netsuite Call by Int ID
            // ReturnedRespTXT.Text += await System.Threading.Tasks.Task.Run(() => ns.GetCustomer(6357)) + "\n\n";
            //await System.Threading.Tasks.Task.Run(() => ns.GetCustomer(5158));


            //Using SuiteQL Query language
            ReturnedRespTXT.Text += await System.Threading.Tasks.Task.Run(() => ns.FindEmployeeIdsUsingSuiteQL(0)) + "\n\n";


            //Netsuite Call by Int ID
            ReturnedRespTXT.Text += await System.Threading.Tasks.Task.Run(() => ns.GetCustomer(6357)) + "\n\n";


            //Get Inventory Items
            //await System.Threading.Tasks.Task.Run(() => ns.GetPartCodes("?limit=100"));

            //await System.Threading.Tasks.Task.Run(() => ns.GetPartCodes("33407?fields=itemId,purchaseDescription,weight"));

            //await System.Threading.Tasks.Task.Run(() => ns.GetPartCodes("?limit=1000&offset=0"));

            //int j = 0;

            //for (int i = 0; i < 700; i++)
            //{
            //    await System.Threading.Tasks.Task.Run(() => ns.GetPartCodes("?offset=" + j.ToString() + ""));

            //    j += 1000;
            //    Console.WriteLine(i.ToString());
            //}

            Console.WriteLine("FINISHED!");
            //?limit=10&offset=0   // $expand=* Doesn't Work

            //Netsuite Call by String ID
            //await System.Threading.Tasks.Task.Run(() => ns.GetCustomer("C1069 [TB UK]"));
        }


        private string _nonce = "fjaLirsIcCGVZWzBX0pg";
        private string _timestamp = "1508242306";
        private string _requestUrl = "https://123456.suitetalk.api.netsuite.com/services/rest/record/v1/employee/40";

        private OAuth1HeaderGenerator CreateOAuth1HeaderGenerator()
        {
            var requestUrl = _requestUrl;

            return new OAuth1HeaderGenerator(new NetSuiteApiConfig(), HttpMethod.Get, requestUrl);
        }

        private async void KaliAPITestBTN_Click(object sender, RoutedEventArgs e)
        {



            //    //Add to lists
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri("https://a.klaviyo.com/api/profiles/"),
                Headers =
    {
        { "accept", "application/json" },
        { "revision", "2023-08-15" },
        { "Authorization", "Klaviyo-API-Key pk_069e9249a0882870282f5d32ba8a973a6e" },
    },
                Content = new StringContent("{\"data\":{\"type\":\"profile\",\"attributes\":{\"email\":\"sarah.mason@klaviyo-demo.com\",\"phone_number\":\"+15005550006\",\"external_id\":\"63f64a2b-c6bf-40c7-b81f-bed08162edbe\",\"first_name\":\"Sarah\",\"last_name\":\"Mason\",\"organization\":\"Klaviyo\",\"title\":\"Engineer\",\"image\":\"https://images.pexels.com/photos/3760854/pexels-photo-3760854.jpeg\",\"location\":{\"address1\":\"89 E 42nd St\",\"address2\":\"1st floor\",\"city\":\"New York\",\"country\":\"United States\",\"region\":\"NY\",\"zip\":\"10017\",\"timezone\":\"America/New_York\"},\"properties\":{\"subsidiary\":\"Consolidated Parent : Techbuyer Group Holdings : Techbuyer Limited\",\"newKey-1\":\"New Value\"}}}}")
                {
                    Headers =
        {
            ContentType = new MediaTypeHeaderValue("application/json")
        }
                }
            };
            using (var response = await client.SendAsync(request))
            {
                try
                {

            
                response.EnsureSuccessStatusCode();
                var body = await response.Content.ReadAsStringAsync();
                Console.WriteLine(body);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(response.ToString() + ex.Message);
                }
            }
        }
        //    // end add to lists
        //Add to Profiles
        //var client = new HttpClient();
        //        var request2 = new HttpRequestMessage
        //        {
        //            Method = HttpMethod.Post,
        //            RequestUri = new Uri("https://a.klaviyo.com/api/profiles/"),
        //            Headers =
        //{
        //    { "accept", "application/json" },
        //    { "revision", "2023-08-15" },
        //    { "Authorization", "Klaviyo-API-Key pk_069e9249a0882870282f5d32ba8a973a6e" },
        //},
        //            Content = new StringContent("{\"data\": {\"type\": \"profile\",\"attributes\": {\"email\": \"n.myers@techbuyer.com\",\"phone_number\": \"01423888888\",\"external_id\": \"\",\"first_name\": \"Nick\",\"last_name\": \"Myers\",\"organization\": \"Techbuyer\",\"title\": \"Dev\",\"image\": \"empty\",\"location\": {\"address1\": \" Unit C1\",\"address2\": \"Crimple Ct\",\"city\": \"Harrogate\",\"country\": \"United Kingdom\",\"latitude\": \"\",\"longitude\": \"\",\"region\": \"North Yorkshire\",\"zip\": \"HG1\",\"timezone\": \"GMT\"},\"properties\": { }}}}")
        //            {
        //                Headers =
        //    {
        //        ContentType = new MediaTypeHeaderValue("application/json")
        //    }
        //            }
        //        };
        //        try
        //        {

        //            using (var response = await client.SendAsync(request2))
        //            {
        //                response.EnsureSuccessStatusCode();
        //                var body = await response.Content.ReadAsStringAsync();
        //                Console.WriteLine(body);
        //            }

        //        }

        //        catch (Exception ex)
        //        {

        //            MessageBox.Show(ex.Message);
        //        }

        //    }



        // end add to Profiles

        //    //Add to lists
    //    var client = new HttpClient();
    //    var request = new HttpRequestMessage
    //    {
    //        Method = HttpMethod.Post,
    //        RequestUri = new Uri("https://a.klaviyo.com/api/lists/"),
    //        Headers =
    //{
    //    { "accept", "application/json" },
    //    { "revision", "2023-08-15" },
    //    { "Authorization", "Klaviyo-API-Key pk_069e9249a0882870282f5d32ba8a973a6e" },
    //},
    //        Content = new StringContent("{\"data\":{\"type\":\"list\",\"attributes\":{\"name\":\"Test List1\"}}}")
    //        {
    //            Headers =
    //    {
    //        ContentType = new MediaTypeHeaderValue("application/json")
    //    }
    //        }
    //    };
    //        using (var response = await client.SendAsync(request))
    //        {
    //            response.EnsureSuccessStatusCode();
    //            var body = await response.Content.ReadAsStringAsync();
    //Console.WriteLine(body);
    //        }
    //    }

    //public void GenerateSignatureBaseString_FromNetSuiteDocExample_Works()
    //{
    //    //From NetSuite documentation: https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_1534941088.html

    //    var oauth1 = CreateOAuth1HeaderGenerator();
    //    var baseString = oauth1.GenerateSignatureBaseString(_nonce, _timestamp);

    //    Assert.AreEqual("GET&https%3A%2F%2F123456.suitetalk.api.netsuite.com%2Fservices%2Frest%2Frecord%2Fv1%2Femployee%2F40&oauth_consumer_key%3Def40afdd8abaac111b13825dd5e5e2ddddb44f86d5a0dd6dcf38c20aae6b67e4%26oauth_nonce%3DfjaLirsIcCGVZWzBX0pg%26oauth_signature_method%3DHMAC-SHA256%26oauth_timestamp%3D1508242306%26oauth_token%3D2b0ce516420110bcbd36b69e99196d1b7f6de3c6234c5afb799b73d87569f5cc%26oauth_version%3D1.0", baseString);
    //}


    //public void GenerateSignatureKey_FromNetSuiteDocExample_Works()
    //{
    //    //From NetSuite documentation: https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_1534941088.html

    //    var oauth1 = CreateOAuth1HeaderGenerator();
    //    var key = oauth1.GenerateSignatureKey();

    //    Assert.AreEqual("d26ad321a4b2f23b0741c8d38392ce01c3e23e109df6c96eac6d099e9ab9e8b5&c29a677df7d5439a458c063654187e3d678d73aca8e3c9d8bea1478a3eb0d295", key);
    //}



    //public void GenerateSignature_FromNetSuiteDocExample_Works()
    //{
    //    //From NetSuite documentation: https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_1534941088.html

    //    var oauth1 = CreateOAuth1HeaderGenerator();
    //    var signature = oauth1.GenerateSignature(_nonce, _timestamp);

    //    //for some reason oracle shows Url Encoded signature in their example
    //    //https://www.screencast.com/t/yXu08Brr
    //    signature = Uri.EscapeDataString(signature);
    //    Assert.AreEqual("B5OIWznZ2YP0OB7VrJrGkYsTh%2B8H%2B5T9Hag%2Bo92q0zY%3D", signature);
    //}


    //public void GetAuthenticationHeaderValueParameter_FromNetSuiteDocExample_Works()
    //{
    //    //From NetSuite documentation: https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_1534941295.html

    //    var oauth1 = CreateOAuth1HeaderGenerator();
    //    var parameter = oauth1.GetAuthenticationHeaderValueParameter(_nonce, _timestamp);

    //    string expected = "realm=\"123456\", oauth_token=\"2b0ce516420110bcbd36b69e99196d1b7f6de3c6234c5afb799b73d87569f5cc\", oauth_consumer_key=\"ef40afdd8abaac111b13825dd5e5e2ddddb44f86d5a0dd6dcf38c20aae6b67e4\", oauth_nonce=\"fjaLirsIcCGVZWzBX0pg\", oauth_timestamp=\"1508242306\", oauth_signature_method=\"HMAC-SHA256\", oauth_version=\"1.0\", oauth_signature=\"B5OIWznZ2YP0OB7VrJrGkYsTh%2B8H%2B5T9Hag%2Bo92q0zY%3D\"";
    //    Assert.AreEqual(expected, parameter);
    //}

}
}
