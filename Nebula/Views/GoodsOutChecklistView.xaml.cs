﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using Nebula.ViewModels;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for GoodsOutChecklistView.xaml
    /// </summary>
    public partial class GoodsOutChecklistView : UserControl
    {
        // Here we create the viewmodel with the current DialogCoordinator instance 

        //GoodsInOutViewModel vm = null;


        public GoodsOutChecklistView()
        {
            InitializeComponent();
        }


        //public GoodsOutChecklistView(GoodsInOutViewModel PassedViewModel)
        //{
        //    InitializeComponent();
        //    vm = PassedViewModel;
        //   //this.DataContext = vm;
        //}

        private void SelectAllBtn_Click(object sender, RoutedEventArgs e)
        {
            GOQ1CB.IsChecked = true;
            GOQ2CB.IsChecked = true;
            GOQ3CB.IsChecked = true;
            GOQ4CB.IsChecked = true;
            GOQ5CB.IsChecked = true;
            GOQ6CB.IsChecked = true;
            GOQ7CB.IsChecked = true;
            GOQ8CB.IsChecked = true;
            GOQ9CB.IsChecked = true;
            GOQ10CB.IsChecked = true;
            GOQ11CB.IsChecked = true;
            GOQ12CB.IsChecked = true;
            GOQ13CB.IsChecked = true;
            GOQ14CB.IsChecked = true;
            GOQ15CB.IsChecked = true;
            GOQ16CB.IsChecked = true;
            GOQ17CB.IsChecked = true;
            GOQ18CB.IsChecked = true;
            GOQ19CB.IsChecked = true;
            GOQ20CB.IsChecked = true;
            GOQ21CB.IsChecked = true;
            GOQ22CB.IsChecked = true;
            GOQ23CB.IsChecked = true;
            GOQ24CB.IsChecked = true;
            GOQ25CB.IsChecked = true;
            SignFormTB.Focus();
        }

        private void ClearAllBtn_Click(object sender, RoutedEventArgs e)
        {
            GOQ1CB.IsChecked = false;
            GOQ2CB.IsChecked = false;
            GOQ3CB.IsChecked = false;
            GOQ4CB.IsChecked = false;
            GOQ5CB.IsChecked = false;
            GOQ6CB.IsChecked = false;
            GOQ7CB.IsChecked = false;
            GOQ8CB.IsChecked = false;
            GOQ9CB.IsChecked = false;
            GOQ10CB.IsChecked = false;
            GOQ11CB.IsChecked = false;
            GOQ12CB.IsChecked = false;
            GOQ13CB.IsChecked = false;
            GOQ14CB.IsChecked = false;
            GOQ15CB.IsChecked = false;
            GOQ16CB.IsChecked = false;
            GOQ17CB.IsChecked = false;
            GOQ18CB.IsChecked = false;
            GOQ19CB.IsChecked = false;
            GOQ20CB.IsChecked = false;
            GOQ21CB.IsChecked = false;
            GOQ22CB.IsChecked = false;
            GOQ23CB.IsChecked = false;
            GOQ24CB.IsChecked = false;
            GOQ25CB.IsChecked = false;
            //SalesOrderNoTBX.Text = string.Empty;
           // SignFormTB.Text = string.Empty;
            SignFormTB.Focus();
        }
    }
}
