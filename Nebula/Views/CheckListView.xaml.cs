﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.Models;
using Nebula.Paginators;
using Nebula.ViewModels;
using QRCoder;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for CheckListView.xaml
    /// </summary>
    public partial class CheckListView : UserControl
    {

        MainViewModel vm;

        public CheckListView()
        {
            InitializeComponent();
        }

        public CheckListView(MainViewModel passedviewmodel, bool Independent)
        {
            InitializeComponent();

            vm = passedviewmodel;

            // Fill property with blank bitmap image
            vm.ChecklistQRCode = new BitmapImage();

            //Reset Message
            vm.ChecklistMessage = "";

            vm.ProcessedDate = DateTime.Now.ToShortDateString();

            //Check department user and which PO SO Textbox to display
            DepartmentUserCheck();

            //Show Hide Save & Print Buttons
            SaveDocsBTN.Visibility = Visibility.Collapsed;
            PrintBTNS1.Visibility = Visibility.Visible;



            SaveToMyDocsCKB.Visibility = Visibility.Hidden;

            ChassisNoTXTB.Focus();

            //if ITAD Switch ref info required
            if (StaticFunctions.Department == "ITAD")
            {
                PurchaseOrderTB.Text = "Enter LOT - Asset Number";
                PORefTB.Text = "(Please include Lot - Asset prefix)";
            }
            else
            {
                PurchaseOrderTB.Text = "Enter PO Number";
                PORefTB.Text = "(Please include PO prefix)";
            }


     
        }

        public CheckListView(MainViewModel passedviewmodel)
        {
            InitializeComponent();

            vm = passedviewmodel;


            // Fill property with blank bitmap image
            vm.ChecklistQRCode = new BitmapImage();

            //Reset Message
            vm.ChecklistMessage = "";

            //Show Save button, depending on path
            SaveDocsBTN.Visibility = Visibility.Visible;
            PrintBTNS1.Visibility = Visibility.Collapsed;
            //Hide out of region checkbox
            OutOfRegionVizCKB.Visibility = Visibility.Hidden;


            ////******** BUG FIX ********
            ////Gen 10 Workaround for error when creating reports on checklist as physical drive members maybe blank
            //if (vm.GenGenericServerInfo1.StorageInfo.LogicalDrives.Count > 0)
            //{
            //    foreach (var logical in vm.GenGenericServerInfo1.StorageInfo.LogicalDrives)
            //    {
            //        PhysicalDrivesHP phys = new PhysicalDrivesHP();
            //        phys.Label = "";
            //        phys.Location = "";
            //        phys.Health = "";
            //        logical.PhysicalDrives.Add(phys);
            //    }
            //}




            //Check department user and which PO SO Textbox to display
            DepartmentUserCheck();

            //DELL
            if (vm.DellServerS1 != null)
            {
                // ChassisNoTXTBS1.Text = vm.DellServerS1.ServiceTag;
                vm.ChassisSerialNo = vm.DellServerS1.ServiceTag;
                vm.ModelNo = vm.DellServerS1.Model;
                vm.ProcessedDate = DateTime.Now.ToShortDateString();
                BoardTXTB.Text = vm.DellServerS1.BoardPartNumber;
                //Add First available controller to textbox
                if (!string.IsNullOrEmpty(vm.DellServerS1.ControllerType))
                {
                    ControllerTXTB.Text = vm.DellServerS1.ControllerType;
                }
                else if(!string.IsNullOrEmpty(vm.DellServerS1.ControllerType2))
                {
                    ControllerTXTB.Text = vm.DellServerS1.ControllerType2;
                }
                else
                {
                    ControllerTXTB.Text = vm.DellServerS1.ControllerType3;
                }
               


                //Fill out License section on Checklist
                if (vm.DellServerS1.iDRACLicense != null)
                if (vm.DellServerS1.iDRACLicense.Contains("Ent"))
                {
                        if (vm.DellServerS1.iDRACLicense.Contains("(Eval)"))
                        {
                            LicenseTXTB.Text = "ENT(Eval)";
                            //clear any warning message
                            vm.ChecklistWarningMessage = "";
                        }
                        else
                        {
                            LicenseTXTB.Text = "ENT";
                            //clear any warning message
                            vm.ChecklistWarningMessage = "";
                        }
                     
                }

                //Server license check
                if(vm.DellServerS1.iDRACLicense != null)
                {
                    if (vm.DellServerS1.iDRACLicense.Contains("Exp"))
                    {
                        LicenseTXTB.Text = "EXP";
                        //clear any warning message
                        vm.ChecklistWarningMessage = "";
                    }
                    else if (vm.DellServerS1.iDRACLicense.Contains("Bas"))
                    {
                        LicenseTXTB.Text = "BAS";
                        //clear any warning message
                        vm.ChecklistWarningMessage = "";
                    }
                    //else
                    //{
                    //    LicenseTXTB.Text = "NA";
                    //}
                  
                }

                //Check license information is present
                if (string.IsNullOrEmpty(vm.DellServerS1.iDRACLicense))
                {
                    //Check if empty, show warning if true
                    vm.ChecklistWarningMessage = "Dell License information missing! Please run Get Server Info again.";
                }
            }

            //HP
            if (vm.GenGenericServerInfo1 != null || vm.GenGenericServerInfo1 != null|| vm.GenGenericServerInfo1 != null)
            {
                //Show out of region checkbox
                OutOfRegionVizCKB.Visibility = Visibility.Visible;
                //if (!string.IsNullOrEmpty(vm.GenGenericServerInfo1.iLOVersion))
                    //BoardTXTB.Text = vm.GenGenericServerInfo1.iLOVersion;
                //if (!string.IsNullOrEmpty(vm.GenGenericServerInfo1.SKU))
                    //ControllerTXTB.Text = vm.GenGenericServerInfo1.SKU;


                // ChassisNoTXTBS1.Text = vm.DellServerS1.ServiceTag;
                if (vm.GenGenericServerInfo1 != null)
                    vm.ChassisSerialNo = vm.GenGenericServerInfo1.ChassisSerial;
                if (vm.GenGenericServerInfo1 != null)
                    vm.ModelNo = vm.GenGenericServerInfo1.Model;
                //if (vm.GenGenericServerInfo1 != null)
                //    vm.ChassisSerialNo = vm.GenGenericServerInfo1.ChassisSerial;
                //if (vm.GenGenericServerInfo1 != null)
                //    vm.ModelNo = vm.GenGenericServerInfo1.Model;
                //if (vm.GenGenericServerInfo1 != null)
                //    vm.ChassisSerialNo = vm.GenGenericServerInfo1.ChassisSerial;
                //if (vm.GenGenericServerInfo1 != null)
                //    vm.ModelNo = vm.GenGenericServerInfo1.Model;
                vm.ProcessedDate = DateTime.Now.ToShortDateString();

                ////remove only testing
                //if (vm.GenGenericServerInfo1 != null)
                //    vm.GenGenericServerInfo1.OutOfRegionMessage = "OutOfRegion";
                //if (vm.GenGenericServerInfo1 != null)
                //    vm.GenGenericServerInfo1.OutOfRegionMessage = "OutOfRegion";
                //if (vm.GenGenericServerInfo1 != null)
                //    vm.GenGenericServerInfo1.OutOfRegionMessage = "OutOfRegion";
            }
            //Lenovo
            if (vm.LenovoServerS1 != null)
            {
                // ChassisNoTXTBS1.Text = vm.DellServerS1.ServiceTag;
                vm.ChassisSerialNo = vm.LenovoServerS1.SystemSerialNumber;
                vm.ModelNo = vm.LenovoServerS1.Model;
                vm.ProcessedDate = DateTime.Now.ToShortDateString();
                if(!string.IsNullOrEmpty(vm.LenovoServerS1.BoardSerialNumber))
                    BoardTXTB.Text = vm.LenovoServerS1.BoardSerialNumber;
                if (!string.IsNullOrEmpty(vm.LenovoServerS1.PartNumber))
                    ControllerTXTB.Text = vm.LenovoServerS1.PartNumber; 
            }
            //Cisco
            //if (vm.CiscoServerS1 != null)
            //{
            //    // ChassisNoTXTBS1.Text = vm.DellServerS1.ServiceTag;
            //    vm.ChassisSerialNo = vm.CiscoServerS1.SystemSerialNumber;
            //    vm.ModelNo = vm.CiscoServerS1.Model;
            //    vm.ProcessedDate = DateTime.Now.ToShortDateString();
            //}

            //if ITAD Switch ref info required
            if(StaticFunctions.Department == "ITAD")
            {
                PurchaseOrderTB.Text = "Enter LOT - Asset Number";
                PORefTB.Text = "(Please include Lot - Asset prefix)";
            }
            else
            {
                PurchaseOrderTB.Text = "Enter PO Number";
                PORefTB.Text = "(Please include PO prefix)";
            }
          


        }


        public void GenerateLabel()
        {
            if (vm != null)
            {
             
            //QRCODE Dimensions
            StandardLabelZebraImage.Width = 60;
            StandardLabelZebraImage.Height = 60;


            // Create QR CODE
            //vm.ChecklistQRCode = CreateQRCode(DateTime.Now.ToShortDateString() + "," + vm.CurrentUserInitials + ",1," + vm.POSONumber + "," + vm.ModelNo + "," + vm.ChassisSerialNo);
            vm.ChecklistQRCode = CreateQRCode(DateTime.Now.ToShortDateString() + "," + vm.CurrentUserInitials + ",1," + vm.POSONumber + ",," + vm.ChassisSerialNo);

            }
        }


        public void GenerateITADLabel()
        {
            if (vm != null)
            {

                //QRCODE Dimensions
                ITADStandardLabelZebraImage.Width = 80;
                ITADStandardLabelZebraImage.Height = 80;

                // Create QR CODE
                //vm.ChecklistQRCode = CreateQRCode(DateTime.Now.ToShortDateString() + "," + vm.CurrentUserInitials + ",1," + vm.POSONumber + "," + vm.ModelNo + "," + vm.ChassisSerialNo);
                vm.ITADQRCode = CreateQRCode(vm.ChassisSerialNo);

                //*** Checklist QR CODE ***
                //Call Embed QR Saves & extracts QR Code to File. This is required for the cloning of the flowdocument as it requires a full path
                vm.ITADQRCode = vm.EmbedChecklistQR(vm.myDocs + @"\Nebula Logs\QR\ITADQR_" + vm.ChassisSerialNo.Trim() + ".png", vm.ITADQRCode);


            }
        }




        //Generate QRCODE Image
        public BitmapImage CreateQRCode(string stringData)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(stringData, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(40, System.Drawing.Color.Black, System.Drawing.Color.White, null, 1, 1, true);



            return ToBitmapImage(qrCodeImage);
        }

        //BITMAP TO BITMAP IMAGE
        public BitmapImage ToBitmapImage(Bitmap bitmap)
        {
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();

                return bitmapImage;
            }
        }

        ////BITMAP IMAGE TO BITMAP
        private Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            return new Bitmap(bitmapImage.StreamSource);
        }




        private void DepartmentUserCheck()
        {
            if (StaticFunctions.Department.Contains("GoodsIn") || StaticFunctions.Department.Contains("TestDept") || StaticFunctions.Department.Contains("ITAD"))
            {
                //Show PO Textbox
                PONumberSP.Visibility = Visibility.Visible;
                SONumberSP.Visibility = Visibility.Collapsed;
                //Show Hide Save & Print Buttons SHOW BOTH
                SaveDocsBTN.Visibility = Visibility.Visible;
                PrintBTNS1.Visibility = Visibility.Visible;
                //Show Brand Toggle Switch
                BrandChecklistTS.Visibility = Visibility.Hidden;
                //Goods In user
                vm.GoodsInRep = vm.CurrentUserInitials;
            }
            else if (StaticFunctions.Department.Contains("GoodsOutTech"))
            {
                //Show sO Textbox
                SONumberSP.Visibility = Visibility.Visible;
                PONumberSP.Visibility = Visibility.Collapsed;

                //Show Hide Save & Print Buttons
                SaveDocsBTN.Visibility = Visibility.Visible;
                PrintBTNS1.Visibility = Visibility.Visible;

                //Show Brand Toggle Switch

                BrandChecklistTS.Visibility = Visibility.Visible;

                //GoodsInSelAllSP.Visibility = Visibility.Collapsed;

                //Goods Out user
                vm.GoodsOutRep = vm.CurrentUserInitials;
            }
        }



        private void ToggleSwitch_Checked(object sender, RoutedEventArgs e)
        {
            //Display The Logo
            if (ChecklistLogo.Visibility == Visibility.Collapsed)
            {
                //Display The Logo
                ChecklistLogo.Visibility = Visibility.Visible;
            }



        }

        private void ToggleSwitch_Unchecked(object sender, RoutedEventArgs e)
        {
            if (ChecklistLogo.Visibility == Visibility.Visible)
            {
                //Display The Logo
                ChecklistLogo.Visibility = Visibility.Collapsed;
            }
        }

        private void ToggleSwitch_IsCheckedChanged(object sender, EventArgs e)
        {


        }

        private void GISelectAllBTN_Click(object sender, RoutedEventArgs e)
        {

            if (StaticFunctions.Department.Contains("GoodsIn") || StaticFunctions.Department.Contains("TestDept") || StaticFunctions.Department.Contains("ITAD"))
            {
                GIBOX1.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX2.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                //GIBOX3.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX4.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX5.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX6.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX7.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX8.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX9.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX10.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX11.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX12.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX13.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX14.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX15.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                //GIBOX16.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX17.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                //GIBOX18.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX19.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX20.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX21.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX22.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                //GIBOX23.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX24.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GIBOX25.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else if (StaticFunctions.Department.Contains("GoodsOutTech"))
            {
                GOTBOX1.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX2.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX3.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX4.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX5.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX6.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX7.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX8.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX9.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX10.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX11.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX12.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX13.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX14.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX15.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX16.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX17.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX18.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX19.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX20.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX21.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX22.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX23.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX24.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
                GOTBOX25.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }

            //Required Used to check when saving Documents PrintSaveDocCommand
            //Set Checklist Ticked property
            vm.ChecklistTicked = true;

        }




        private void ClearCheckBoxes()
        {
            if (StaticFunctions.Department.Contains("GoodsIn") || StaticFunctions.Department.Contains("TestDept") || StaticFunctions.Department.Contains("ITAD"))
            {
                GIBOX1.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX2.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                //GIBOX3.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX4.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX5.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX6.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX7.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX8.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX9.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX10.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX11.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX12.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX13.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX14.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX15.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                //GIBOX16.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX17.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                //GIBOX18.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX19.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX20.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX21.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX22.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                //GIBOX23.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX24.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GIBOX25.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
            else if (StaticFunctions.Department.Contains("GoodsOutTech"))
            {
                GOTBOX1.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX2.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX3.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX4.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX5.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX6.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX7.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX8.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX9.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX10.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX11.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX12.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX13.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX14.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX15.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX16.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX17.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX18.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX19.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX20.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX21.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX22.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX23.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX24.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
                GOTBOX25.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }





        private void GIClearAllBTN_Click(object sender, RoutedEventArgs e)
        {
            //Clear GI OR GOT Checkboxes
            ClearCheckBoxes();

            //Required Used to check when saving Documents PrintSaveDocCommand
            //Set Checklist Ticked property
            vm.ChecklistTicked = false;
        }

 

        private void BUIGOT1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX1.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX1.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX1.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX1.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX1.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX1.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGOT2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX2.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX2.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX2.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX2.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX2.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX2.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }


        private void BUIGOT3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX3.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX3.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX3.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        //private void BUIGI3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    if (GIBOX3.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
        //    {
        //        GIBOX3.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
        //    }
        //    else
        //    {
        //        GIBOX3.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
        //    }
        //}

        private void BUIGOT4_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX4.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX4.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX4.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI4_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX4.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX4.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX4.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }


        private void BUIGOT5_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX5.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX5.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX5.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI5_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX5.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX5.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX5.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }



        private void BUIGOT6_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX6.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX6.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX6.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI6_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX6.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX6.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX6.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }


        private void BUIGOT7_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX7.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX7.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX7.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI7_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX7.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX7.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX7.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }


        private void BUIGOT8_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX8.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX8.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX8.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI8_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX8.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX8.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX8.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }


        private void BUIGOT9_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX9.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX9.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX9.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI9_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX9.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX9.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX9.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }


        private void BUIGOT10_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX10.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX10.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX10.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI10_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX10.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX10.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX10.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }


        private void BUIGOT11_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX11.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX11.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX11.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI11_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX11.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX11.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX11.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }



        private void BUIGOT12_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX12.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX12.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX12.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI12_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX12.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX12.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX12.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }


        private void BUIGOT13_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX13.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX13.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX13.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI13_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX13.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX13.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX13.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }


        private void BUIGOT14_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX14.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX14.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX14.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI14_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX14.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX14.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX14.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGOT15_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX15.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX15.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX15.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI15_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX15.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX15.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX15.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }


        private void BUIGOT16_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX16.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX16.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX16.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        //private void BUIGI16_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    if (GIBOX16.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
        //    {
        //        GIBOX16.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
        //    }
        //    else
        //    {
        //        GIBOX16.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
        //    }
        //}

        private void BUIGOT17_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX17.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX17.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX17.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI17_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX17.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX17.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX17.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGOT18_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX18.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX18.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX18.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        //private void BUIGI18_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    if (GIBOX18.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
        //    {
        //        GIBOX18.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
        //    }
        //    else
        //    {
        //        GIBOX18.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
        //    }
        //}

        private void BUIGOT19_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX19.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX19.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX19.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI19_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX19.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX19.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX19.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGOT20_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX20.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX20.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX20.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI20_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX20.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX20.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX20.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGOT21_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX21.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX21.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX21.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI21_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX21.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX21.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX21.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGOT22_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX22.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX22.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX22.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI22_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX22.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX22.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX22.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGOT23_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX23.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX23.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX23.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        //private void BUIGI23_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    if (GIBOX23.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
        //    {
        //        GIBOX23.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
        //    }
        //    else
        //    {
        //        GIBOX23.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
        //    }
        //}


        private void BUIGOT24_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX24.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX24.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX24.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI24_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX24.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX24.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX24.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGOT25_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GOTBOX25.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GOTBOX25.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GOTBOX25.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void BUIGI25_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (GIBOX25.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square)
            {
                GIBOX25.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2;
            }
            else
            {
                GIBOX25.Kind = MahApps.Metro.IconPacks.PackIconEvaIconsKind.Square;
            }
        }

        private void PurchaseOrderTBX_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (PurchaseOrderTBX.Text == "")
            {
                PrintBTNS1.IsEnabled = false;
            }
            else
            {
                PrintBTNS1.IsEnabled = true;
            }

            vm.POSONumber = PurchaseOrderTBX.Text;
            vm.PONumber = PurchaseOrderTBX.Text;

            //Generate QR Code from info
            GenerateLabel();

           


            // IF ITAD Processing generate data for Flow Document
            if (StaticFunctions.Department == "ITAD")
            {

              
                if (vm.POSONumber.Contains("LOT") && vm.POSONumber.Contains(" - "))
                {
                    //Generate ITAD QR Code from info
                    GenerateITADLabel();


                    //Populate ITAD Report with data
                    string lotNumber = "";
                    string assetNumber = "";
                    //split on hyphen
                    var splitLines = vm.POSONumber.Split('-');
                    //separate into lot & asset
                    lotNumber = splitLines[0].Trim();
                    assetNumber = splitLines[1].Trim();
                    //Fill runs with data
                    LotAssetSerialRun.Text = vm.POSONumber + " - " + vm.ChassisSerialNo;
                    UserDateRun.Text = "Technician Name: " + vm.FullUser + " " + DateTime.Now.ToShortDateString();
                    categoryRun.Text = "Server";
                    processRun.Text = "Recommended GUI Erasure Commands";
                    testingRun.Text = "Diagnostic";
                    notesRun.Text = "";
                    cliRun.Text = "";
                    declareRun.Text = "This report serves as confirmation that the asset " + assetNumber.Replace("Asset No: ", "") + @"/" + vm.ChassisSerialNo.Replace("Serial No: ", "") + @" has be sanitised of all user data in accordance to GDPR using Company/Manufactures procedures and guidelines for sanitisation.";
                    declare2Run.Text = "Asset " + assetNumber.Replace("Asset No: ", "") + @"/" + vm.ChassisSerialNo.Replace("Serial No: ", "") + " was sanitised using the following process (Recommended GUI Erasure Commands) in line with Company & Manufactures procedures."; ;
                    techconfirmRun.Text = vm.FullUser.Replace("Technician Name: ", "") + " hereby confirms that Asset " + assetNumber.Replace("Asset No: ", "") + @"/" + vm.ChassisSerialNo.Replace("Serial No: ", "") + @" has been sanitised of all user data and is fit for reuse."; ;
                    verificationRun.Text = "Sanitisation of this asset was verified by D Benyon, using the verification process GUI Commands confirm factory reset.";
                }
               
            }
            }

        private void SalesOrderTBX_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SalesOrderTBX.Text == "")
            {
                PrintBTNS1.IsEnabled = false;
            }
            else
            {
                PrintBTNS1.IsEnabled = true;
            }

            vm.POSONumber = SalesOrderTBX.Text;
            vm.SONumber = SalesOrderTBX.Text;

            //Generate QR Code from info
            GenerateLabel();
        }

        private void SaveDocsBTN_Click(object sender, RoutedEventArgs e)
        {

         
        }

        private void PrintBTNS1_Click(object sender, RoutedEventArgs e)
        {
            try
            {

            //Clear Messsage
            vm.ChecklistMessage = "";

            //Check license information is present
            if(vm.DellServerS1 != null)
            if (vm.DellServerS1.iDRACLicense == string.Empty || vm.DellServerS1.iDRACLicense == "")
            {
              //Clear Warning Messsage
              vm.ChecklistWarningMessage = "";
            }
            

            if (vm.ChassisSerialNo != "" && vm.PONumber != "" || vm.SONumber != "")
            {

             // Check the tickboxes have been ticked
            if (GIBOX1.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2 || GOTBOX1.Kind == MahApps.Metro.IconPacks.PackIconEvaIconsKind.CheckmarkSquare2)
            {

                        //*** Checklist QR CODE ***
                        //Call Embed QR Saves & extracts QR Code to File. This is required for the cloning of the flowdocument as it requires a full path
                        vm.ChecklistQRCode = vm.EmbedChecklistQR(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.ChassisSerialNo.Trim() + ".png", vm.ChecklistQRCode);




                        ////Set the serial number to text on the clipboard  Use control v
                        if (!string.IsNullOrEmpty(ChassisNoTXTB.Text))
                            Clipboard.SetText(ChassisNoTXTB.Text.Trim() + "_Checklist", TextDataFormat.UnicodeText);



                        //OLD METHOD

                        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


                        ////Get the text from the clipboard
                        //Clipboard.GetText(TextDataFormat.UnicodeText);

                        dpw.SaveDocPDF(ServCheckLstFDoc);

                        //Clear values
                        //vm.ChecklistQRCode = null;
                        // bi.UriSource = new Uri("", UriKind.RelativeOrAbsolute); ;

                        //vm.ChassisSerialNo = "";
                        //vm.PONumber = "";
                        //vm.SONumber = "";

                        //Clear GI OR GOT Checkboxes
                        //ClearCheckBoxes();
                    }
                    else
                    {
                        vm.ChecklistMessage = "Please select all the Check Boxes!";
                    }


                }
              

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void OutOfRegionVizCKB_Checked(object sender, RoutedEventArgs e)
        {

            //hide Out Of Region TextBlock
               if (vm.GenGenericServerInfo1 != null)
                   vm.GenGenericServerInfo1.OutOfRegionViz = Visibility.Hidden;
                //if (vm.GenGenericServerInfo1 != null)
                //    vm.GenGenericServerInfo1.OutOfRegionViz = Visibility.Hidden;
                //if (vm.GenGenericServerInfo1 != null)
                //    vm.GenGenericServerInfo1.OutOfRegionViz = Visibility.Hidden;
          

        }

        private void OutOfRegionVizCKB_Unchecked(object sender, RoutedEventArgs e)
        {

                //Show Out Of Region TextBlock
                if (vm.GenGenericServerInfo1 != null)
                    vm.GenGenericServerInfo1.OutOfRegionViz = Visibility.Visible;
                //if (vm.GenGenericServerInfo1 != null)
                //    vm.GenGenericServerInfo1.OutOfRegionViz = Visibility.Visible;
                //if (vm.GenGenericServerInfo1 != null)
                //    vm.GenGenericServerInfo1.OutOfRegionViz = Visibility.Visible;
           
        }
    }
}
