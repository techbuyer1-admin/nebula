﻿using System;
using System.ComponentModel;
using System.IO.Ports;
using System.Security;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Nebula.Models;
using Nebula.Commands;
using Nebula.Commands.XTerminal;
using Microsoft.Win32;
using System.Text;
using System.Windows.Documents;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for TerminalWindowView.xaml
    /// </summary>
    public partial class TerminalWindowView : UserControl
    {
      
        SerialPort _serialPort1 = new SerialPort();

       

        public TerminalWindowView()
        {
            InitializeComponent();
            this.Loaded += this.OnLoaded;

            string mytext = "[24;0HE[24;1H[24;18H[24;1H[2K[24;1H[?25h[24;1H[1;24r[24;1H[1;24r[24;1H[24;1H[2K[24;1H[?25h[24;1H[24;1HHP-2620-48-PoEP# [24;1H[24;18H[24;1H[?25h[24;18H";

            //string test = Regex.Replace(vm.ReportBody, @"\e\[\d *;?\d + m", "");
            //string test = Regex.Replace(vm.ReportBody, @"\e\[\d *;?\d + m", "");
            // string test = Regex.Replace(vm.ReportBody, @"\e\[(\d+;)*(\d+)?[ABCDHJKfmsu]", "");
            //string source1 = "\x1b[2JThis text \x1b[3;20Hcontains several ANSI escapes\x1b[1;2;30;43m to format the text\x1b[K";
            //string source = "[2JThis text [3;20Hcontains several ANSI escapes[1;2;30;43m to format the text[K";
            //string result = Regex.Replace(source, @"\e\[(\d+;)*(\d+)?[ABCDHJKfmsu]", "");
            string result = Regex.Replace(mytext, @"\[(\d\d;\d\w\w)", ""); //(\[\d\w)(\[\d\d;\d\d\w)(\[\d\d;\d\w) (\d\d;\d\w)(\d\d;\d\d\w)
            result = Regex.Replace(result, @"\[(\d\d;\d\w)", "");
            result = Regex.Replace(result, @"\[(\d\d;\d\d\w)", "");
            result = Regex.Replace(result, @"\[(\w\d\d\d\w)", "");
            result = Regex.Replace(result, @"\[(\d;\d\d\w)", "");
            result = Regex.Replace(result, @"\[(\d\w)", "");
            result = Regex.Replace(result, @"\[(\W\d\d\w)", "");
            MessageBox.Show(result);

        }


        private void OnLoaded(object sender, RoutedEventArgs e)
        {
          
                var msg = System.Text.Encoding.UTF8.GetBytes(
                    "\x1b[2J\x1b[H" +
                    "ConPTY APIs required for this application are not available.\r\n" +
                    $"Please update to Windows 10 1809 (build Windows 10 1809) or higher.\r\n" +
                    "Press any key to exit.\r\n");
               // this.screen.Buffer.Append(msg, msg.Length);
                //this.KeyDown += (_, args) => this.Close();
                return;
           

          
        }

        //https://www.ascii-code.com/

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            //var msg = System.Text.Encoding.UTF8.GetBytes(
            //      "\x1b[2J\x1b[H" +
            //      "ConPTY APIs required for this application are not available.\r\n" +
            //      $"Please update to Windows 10 1809 (build {MinimumWindowsVersion}) or higher.\r\n" +
            //      "Press any key to exit.\r\n");
            //string text = "\x1b[24;0HE\x1b[24;1H\x1b[24;18H\x1b[24;1H\x1b[2K\x1b[24;1H\x1b[?25h\x1b[24;1H\x1b[1;24r\x1b[24;1H\x1b[1;24r\x1b[24;1H\x1b[24;1H\x1b[2K\x1b[24;1H\x1b[?25h\x1b[24;1HHP-2620-48-PoEP# \x1b[24;1H\x1b[24;18H\x1b[24;1H\x1b[?25h\x1b[24;18H";
            string text2 = "\x1b[24;1H HP-2620-48-PoEP#";
            var msg = System.Text.Encoding.UTF8.GetBytes(text2);
            //this.screen.Buffer.Append(msg, msg.Length);

            //Commands.XTerminal.Buffer buff = new Commands.XTerminal.Buffer(2,2);

            //buff.AppendString("Nick");

            //AppendString("[24;0HE[24;1HHP-2620-48-PoEP#");

            AppendString("\x1b[24;0H\x1b[24;1H\x1b[24;18H\x1b[24;1H\x1b[2K\x1b[24;1H\x1b[?25h\x1b[24;1H\x1b[1;24r\x1b[24;1H\x1b[1;24r\x1b[24;1H\x1b[24;1H\x1b[2K\x1b[24;1H\x1b[?25h\x1b[24;1HHP-2620-48-PoEP# \x1b[24;1H\x1b[24;18H\x1b[24;1H\x1b[?25h\x1b[24;18H");

            //var msg = System.Text.Encoding.UTF8.GetBytes(text + "\r\n");
            //this.screen.Buffer.Append(msg, msg.Length);
            //this.screen.Buffer.AppendString("Ni" + "\r\n");
            //this.screen.Buffer.
        }



        //1
        /// <summary>
        // Append the given string to the buffer.The string will be converted to UTF8 and then written to the buffer as usual.
        // </summary>
        public void AppendString(string str)
        {
            var strBytes = Encoding.UTF8.GetBytes(str);
            Append(strBytes, strBytes.Length);
        }


        private readonly SequenceParser parser = new SequenceParser();
        private int currentChar;
        private long receivedCharacters;

        //char[] tooutput;

        //string final = "";

        StringBuilder sb = new StringBuilder();

        //2  NEEDS using TBTOOLS.XTerminal to work
        public void Append(byte[] bytes, int length)
        {
            //lock (this.renderLock)
            //{
            for (var i = 0; i < length; ++i)
            {
                if (!this.AppendChar(bytes[i])) continue;

                ++this.receivedCharacters;
                switch (this.parser.Append(this.currentChar))
                {
                    case ParserAppendResult.Render:
                        // MessageBox.Show(currentChar + " To be rendered");
                        
                        //cast the int to a char and add to stringbuilder
                        sb.Append((char)currentChar);
                        //this.PrintAtCursor(this.currentChar);
                        break;
                    case ParserAppendResult.Complete:
                        //this.ExecuteParserCommand();
                      //  MessageBox.Show("Complete Execute Parser Command");
                        break;
                    case ParserAppendResult.Pending:
                       // MessageBox.Show(currentChar + "Pending");
                        break;
                    case ParserAppendResult.Invalid:
                        // XXX: we should keep a trailing history of received bytes or something so we can actually log meaningful data.
                       // MessageBox.Show("Invalid");
                        //Commands.XTerminal.Logger.Warning($"Invalid command sequence in parser.");
                        break;
                    default:
                        throw new InvalidOperationException("unexpected parser result");
                }
            }

            //************
            //SHOWS THE CHRACTER NUMBERS AS INTS IN THE ASCII TABLE, JUST NEED TO CONVERT THESE BACK
            //NEED TO CHECK THAT THE STREAM RECIEVED FROM THE SERIAL PORT INCLUDES THE ESCAPES \x1b OTHERWISE THESE WOULD NEED INSERTING BEFORE EACH [ IN THE SEQUENCE
            //WHILST IT STRIPS THE CONTROL CHARACTERS, NEED TO CHECK THE COMMAND FORMATTING ISN'T SCREWED UP

            Paragraph para = new Paragraph();
            para.Inlines.Add(ReplaceHexadecimalSymbols(sb.ToString().Trim()));

            //ConsoleRTB1.LineDown();
            FlowDoc.Blocks.Add(para);

            sb.Clear();
           // MessageBox.Show(sb.ToString());
        }


        
        //3
        /// <summary>
        /// Append a single byte to the current character.
        /// </summary>
        /// <returns>true if the current character represents a completed Unicode character</returns>
        private bool AppendChar(byte b)
        {
            // TODO: actual utf-8 parsing.
            this.currentChar = b;
            return true;
        }


        //USED IN 
        static string ReplaceHexadecimalSymbols(string txt)
        {
            //STRIPS OUT THE ESCAPE SEQUENCE x1b
            //string r = "[\x00-\x08\x0B\x0C\x0E-\x1F\x26\x1b]";
            string r = "[\x00-\x08\x0B\x0C\x0E-\x1F\x26]";
            return Regex.Replace(txt, r, "", RegexOptions.Compiled);
        }

        //4
        //public ParserAppendResult Append(int character)
        //{
        //    if (this.state == State.Reset)
        //    {
        //        this.state = State.None;
        //        this.Command = null;
        //        this.buffer.Clear();
        //    }

        //    switch (this.state)
        //    {
        //        case State.None:
        //            return this.AppendNone(character);
        //        case State.Basic:
        //            return this.AppendBasic(character);
        //        case State.ControlSequence:
        //            return this.AppendControlSequence(character);
        //        case State.OSCommand:
        //            return this.AppendOSCommand(character);
        //        case State.PrivacyMessage: // these are identical for now as we do not support them.
        //        case State.ApplicationProgramCommand:
        //            return this.AppendUntilST(character);
        //        default:
        //            throw new InvalidOperationException("Invalid parser state.");
        //    }
        //}

        //5
        //private ParserAppendResult AppendNone(int character)
        //{
        //    switch (character)
        //    {
        //        case '\0':
        //            return this.CompleteCommand(new Commands.XTerminal.ControlCharacter(Commands.XTerminal.ControlCharacter.ControlCode.NUL));
        //        case '\a':
        //            return this.CompleteCommand(new Commands.XTerminal.ControlCharacter(Commands.XTerminal.ControlCharacter.ControlCode.BEL));
        //        case '\b':
        //            return this.CompleteCommand(new Commands.XTerminal.ControlCharacter(Commands.XTerminal.ControlCharacter.ControlCode.BS));
        //        case '\f':
        //            return this.CompleteCommand(new Commands.XTerminal.ControlCharacter(Commands.XTerminal.ControlCharacter.ControlCode.FF));
        //        case '\n':
        //            return this.CompleteCommand(new Commands.XTerminal.ControlCharacter(Commands.XTerminal.ControlCharacter.ControlCode.LF));
        //        case '\r':
        //            return this.CompleteCommand(new Commands.XTerminal.ControlCharacter(Commands.XTerminal.ControlCharacter.ControlCode.CR));
        //        case '\t':
        //            return this.CompleteCommand(new Commands.XTerminal.ControlCharacter(Commands.XTerminal.ControlCharacter.ControlCode.TAB));
        //        case '\v':
        //            // NB: old timey tech sure was funny. vertical tabs. ha ha ha.
        //            return this.CompleteCommand(new Commands.XTerminal.ControlCharacter(Commands.XTerminal.ControlCharacter.ControlCode.LF));
        //        case 0x1b: // ^[ / escape
        //            this.state = State.Basic;
        //            return ParserAppendResult.Pending;
        //        default:
        //            return ParserAppendResult.Render;
        //    }
        //}


        //6
        //private ParserAppendResult AppendBasic(int character)
        //{
        //    switch (character)
        //    {
        //        case 'A':
        //        case 'B':
        //        case 'C':
        //        case 'D':
        //            return this.CompleteCommand(new Commands.XTerminal.CursorMove(string.Empty, (char)character));
        //        case '[':
        //            this.state = State.ControlSequence;
        //            return ParserAppendResult.Pending;
        //        case ']':
        //            this.state = State.OSCommand;
        //            return ParserAppendResult.Pending;
        //        case '^':
        //            this.state = State.PrivacyMessage;
        //            return ParserAppendResult.Pending;
        //        case '_':
        //            this.state = State.ApplicationProgramCommand;
        //            return ParserAppendResult.Pending;
        //        default:
        //            return this.CompleteCommand(new Commands.XTerminal.Unsupported($"^[{(char)character}"), ParserAppendResult.Invalid);
        //    }
        //}

        //7


    }//end class
}//end ns
