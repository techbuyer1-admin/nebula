﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.ViewModels;
using Nebula.Properties;
using Nebula.Models;
using Nebula.Helpers;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Nebula.Commands;
using System.Net;
using System.Net.Security;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for HPG10AMDServerView.xaml
    /// </summary>
    public partial class HPG10AMDServerView : UserControl
    {
        //declare viewmodel
        MainViewModel vm = new MainViewModel(DialogCoordinator.Instance);    //
        MainViewModel vmMain;
        TabItem ServerTab = null;
        string ServerTabHeader = "";

        public HPG10AMDServerView(MainViewModel PassedViewModel, string whichTab, string PassedServerIP, bool IsBlade, string EnclosureBay)
        {

            //get the passed in viewmodel and assign to the local vm variable

            InitializeComponent();

            this.DataContext = vm;
            vmMain = PassedViewModel;

            //CLEAR SERVER PROPERTIES ON LOAD
            //reset flipview to start
            FlipView1.SelectedIndex = 0;
            //delete server object
            vm.GenGenericServerInfo1 = null;
            //tab header set back to default
            vm.Server1TabHeader = "Server1";
            //clear server ip
            vm.Server1IPv4 = string.Empty;
            //reset flipview to start
            //Set Default password
            vm.OverridePasswordS1 = "password";

            //IMPORTANT TO PASS THE CORRECT TAB IN

            //Set Tab
            vm.WhichTab = whichTab;
            //Set IP
            vm.Server1IPv4 = PassedServerIP;
            //Get Server Type Selected
            vm.ServerType = PassedViewModel.ServerType;


            //Check If Blade and Switch Slide and set get server info going
            if (PassedViewModel.ServerType == "HPGen10Blade" && IsBlade == true)
            {

                // MessageBox.Show("Prior Tp WhichTab Check");
                //Set the ip in the Master ViewModel, needed for free enclosure slots
                switch (vm.WhichTab)
                {
                    case "1":
                        //MessageBox.Show("Inside WhichTAB 1");

                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server1IPv4 = PassedServerIP;

                        break;
                    case "2":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server2IPv4 = PassedServerIP;
                        break;
                    case "3":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server3IPv4 = PassedServerIP;
                        break;
                    case "4":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server4IPv4 = PassedServerIP;
                        break;
                    case "5":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server5IPv4 = PassedServerIP;
                        break;
                    case "6":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server6IPv4 = PassedServerIP;
                        break;
                    case "7":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server7IPv4 = PassedServerIP;
                        break;
                    case "8":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server8IPv4 = PassedServerIP;
                        break;
                    case "9":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server9IPv4 = PassedServerIP;
                        break;
                    case "10":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server10IPv4 = PassedServerIP;
                        break;
                    case "11":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server11IPv4 = PassedServerIP;
                        break;
                    case "12":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server12IPv4 = PassedServerIP;
                        break;
                    case "13":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server13IPv4 = PassedServerIP;
                        break;
                    case "14":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server14IPv4 = PassedServerIP;
                        break;
                    case "15":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server15IPv4 = PassedServerIP;
                        break;
                    case "16":
                        ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + whichTab) as TabItem;
                        if (ServerTab != null)
                        {
                            ServerTab.Foreground = Brushes.Orange;
                            ServerTab.Header = EnclosureBay;
                        }

                        vmMain.Server16IPv4 = PassedServerIP;
                        break;









                }

                //Set Server Info Going
                vm.FlipView1Index = 4;

                if (vm.GenGenericServerInfo1 == null)
                    vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen10" });



            }





        }

        //Previous slide to help skip Test Cert for GOT
        int previousSlide = 0;

        private void FlipView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var flipview = ((FlipView)sender);
            switch (flipview.SelectedIndex)
            {
                case 0:
                    flipview.BannerText = "HP Gen10 Procedure";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                    }

                    break;
                case 1:
                    flipview.BannerText = "Remove Server cover & Locate dip switches";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                    }
                    break;
                case 2:
                    flipview.BannerText = "Set Dip Switch 1 to the On Position";
                    if (vm != null)
                    {
                        //    vm.ProgressVisibility = Visibility.Hidden;
                        //    vm.ProgressIsActive = false;
                        //    vm.ProgressPercentage = 0;
                        //    vm.ProgressMessage = "";
                    }
                    break;
                case 3:
                    flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                    }
                    break;
                case 4:
                    flipview.BannerText = "Enter Server IP & Run Scripts";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                        //vm.ILORestOutput1 = "";
                        //Load Ultima URL
                        if (vm.GenGenericServerInfo1 != null)
                        {
                            if (vm.GenGenericServerInfo1.Manufacturer != null && vm.GenGenericServerInfo1.Model != null)
                                vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.GenGenericServerInfo1.Manufacturer.Replace("Inc.", "") + " " + vm.GenGenericServerInfo1.Model + " Server &go=Go";
                        }

                    }
                    break;
                case 5:
                    flipview.BannerText = "Reports & Diagnotics";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                        //vm.ILORestOutput1 = "";
                        //Load Ultima URL
                        if (vm.GenGenericServerInfo1 != null)
                        {
                            if (vm.GenGenericServerInfo1.Manufacturer != null && vm.GenGenericServerInfo1.Model != null)
                                vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.GenGenericServerInfo1.Manufacturer.Replace("Inc.", "") + " " + vm.GenGenericServerInfo1.Model + " Server &go=Go";
                        }
                    }
                    break;
                case 6:

                    if (vm != null)
                    {

                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                        //vm.ILORestOutput1 = "";
                        //Load Ultima URL
                        if (vm.GenGenericServerInfo1 != null)
                        {
                            //ADD GOT Specific items
                            if (StaticFunctions.Department == "GoodsOutTech")
                            {
                                flipview.BannerText = "Server Certificate";
                                //Create instance of Checklist view and pass in the instance of the mainview model
                                vm.ChecklistView1 = new ServerCertReportView(vm);
                                if (vm != null)
                                {
                                    vm.GenerateTestCertificate("HPGen8");
                                }
                                //Create instance of HP Test Report View
                                vm.ServerCertView1 = new TestCertificateView(vm);
                                //Create instance of HP Test Report View
                                vm.ReportView1 = new HPReportsView(vm);

                            }
                            else
                            {
                                flipview.BannerText = "Server Checklist";
                                //Create instance of Checklist view and pass in the instance of the mainview model
                                vm.ChecklistView1 = new CheckListView(vm);
                                //Create instance of HP Test Report View
                                vm.ReportView1 = new HPReportsView(vm);

                            }


                            if (vm.GenGenericServerInfo1.Manufacturer != null && vm.GenGenericServerInfo1.Model != null)
                                vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.GenGenericServerInfo1.Manufacturer.Replace("Inc.", "") + " " + vm.GenGenericServerInfo1.Model + " Server &go=Go";
                            // Clear QR bitmap image.
                            vm.ChecklistQRCode = null;



                        }
                    }
                    break;
                case 7:
                    flipview.BannerText = "Final Wipe Scripts";
                    if (vm != null)
                    {
                        vm.ILORestOutput1 = "";
                        //Set previous slide to help skip Test Cert for GOT
                        previousSlide = 7;

                    }
                    break;
                case 8:
                    flipview.BannerText = "Test Certificate";
                    if (vm != null)
                    {
                        //ADD GOT Specific items
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //GOT Skip the Test Cert Sliide
                            switch (previousSlide)
                            {
                                case 7:
                                    vm.CertificateView1 = null;
                                    flipview.SelectedIndex = 9;
                                    break;
                                case 9:
                                    vm.CertificateView1 = null;
                                    flipview.SelectedIndex = 7;
                                    break;
                            }

                        }
                        else
                        {
                            //Show for Goods In
                            vm.GenerateTestCertificate("HPGen10");
                        }

                    }
                    break;
                case 9:
                    flipview.BannerText = "Set Dip Switch 1 to the Off Position";

                    if (vm != null)
                    {
                        //Set previous slide to help skip Test Cert for GOT
                        previousSlide = 9;

                    }
                    break;
            }
        }





        //SERVER1
        private void SelectAllBtnS1_Click(object sender, RoutedEventArgs e)
        {


            //GIQ1CBS1.IsChecked = true;
            //GIQ2CBS1.IsChecked = true;
            ////GIQ3CBS1.IsChecked = true;
            //GIQ4CBS1.IsChecked = true;
            //GIQ5CBS1.IsChecked = true;
            //GIQ6CBS1.IsChecked = true;
            //GIQ7CBS1.IsChecked = true;
            //GIQ8CBS1.IsChecked = true;
            //GIQ9CBS1.IsChecked = true;
            //GIQ10CBS1.IsChecked = true;
            //GIQ11CBS1.IsChecked = true;
            //GIQ12CBS1.IsChecked = true;
            //GIQ13CBS1.IsChecked = true;
            //GIQ14CBS1.IsChecked = true;
            //GIQ15CBS1.IsChecked = true;
            ////GIQ16CBS1.IsChecked = true;
            //GIQ17CBS1.IsChecked = true;
            ////GIQ18CBS1.IsChecked = true;
            //GIQ19CBS1.IsChecked = true;
            //GIQ20CBS1.IsChecked = true;
            ////GIQ21CBS1.IsChecked = true;
            //GIQ22CBS1.IsChecked = true;
            ////GIQ23CBS1.IsChecked = true;
            //GIQ24CBS1.IsChecked = true;
            //GIQ25CBS1.IsChecked = true;
            ////MessageTB.Text = String.Empty;
        }

        private void ClearAllBtnS1_Click(object sender, RoutedEventArgs e)
        {
            //GIQ1CBS1.IsChecked = false;
            //GIQ2CBS1.IsChecked = false;
            ////GIQ3CBS1.IsChecked = false;
            //GIQ4CBS1.IsChecked = false;
            //GIQ5CBS1.IsChecked = false;
            //GIQ6CBS1.IsChecked = false;
            //GIQ7CBS1.IsChecked = false;
            //GIQ8CBS1.IsChecked = false;
            //GIQ9CBS1.IsChecked = false;
            //GIQ10CBS1.IsChecked = false;
            //GIQ11CBS1.IsChecked = false;
            //GIQ12CBS1.IsChecked = false;
            //GIQ13CBS1.IsChecked = false;
            //GIQ14CBS1.IsChecked = false;
            //GIQ15CBS1.IsChecked = false;
            ////GIQ16CBS1.IsChecked = false;
            //GIQ17CBS1.IsChecked = false;
            ////GIQ18CBS1.IsChecked = false;
            //GIQ19CBS1.IsChecked = false;
            //GIQ20CBS1.IsChecked = false;
            ////GIQ21CBS1.IsChecked = false;
            //GIQ22CBS1.IsChecked = false;
            ////GIQ23CBS1.IsChecked = false;
            //GIQ24CBS1.IsChecked = false;
            //GIQ25CBS1.IsChecked = false;
            ////MessageTB.Text = String.Empty;
        }




        private void GenCheck1_Click(object sender, RoutedEventArgs e)
        {



            //vm.DialogMessage("BB", "CCCC");
            //Close already opened remoteconsole
            CloseAppByPid(vm.HPRemoteConsolePIDS1);
            Gen10Tab1.SelectedIndex = 0;
            //reset flipview to start
            FlipView1.SelectedIndex = 0;
            //Add to Database
            //vm.ServersProcessedCRUDCommand.Execute(vm);
            // PurchaseOrderTBX.Text = "";

            //clear server ip
            //vm.Server1IPv4 = string.Empty;
            //remove Remote Console Panel
            if (WFHS1.Child != null)
                WFHS1.Child = null;
            // AddTestMsg1.Text = string.Empty;


            vm.TabColorS1 = Brushes.MediumPurple;
            vm.WarningMessage1 = string.Empty;
            vm.ProgressMessage1 = string.Empty;
            vm.ProgressIsActive1 = false;
            vm.BIOSUpgradePath1 = string.Empty;
            vm.ILOUpgradePath1 = string.Empty;
            vm.SPSUpgradePath1 = string.Empty;
            vm.IENGUpgradePath1 = string.Empty;

            //Set the ip in the Master ViewModel, needed for free enclosure slots
            switch (vm.WhichTab)
            {
                case "1":
                    vmMain.Server1IPv4 = "";
                    break;
                case "2":
                    vmMain.Server2IPv4 = "";
                    break;
                case "3":
                    vmMain.Server3IPv4 = "";
                    break;
                case "4":
                    vmMain.Server4IPv4 = "";
                    break;
                case "5":
                    vmMain.Server5IPv4 = "";
                    break;
                case "6":
                    vmMain.Server6IPv4 = "";
                    break;
                case "7":
                    vmMain.Server7IPv4 = "";
                    break;
                case "8":
                    vmMain.Server8IPv4 = "";
                    break;
                case "9":
                    vmMain.Server9IPv4 = "";
                    break;
                case "10":
                    vmMain.Server10IPv4 = "";
                    break;
                case "11":
                    vmMain.Server11IPv4 = "";
                    break;
                case "12":
                    vmMain.Server12IPv4 = "";
                    break;
                case "13":
                    vmMain.Server13IPv4 = "";
                    break;
                case "14":
                    vmMain.Server14IPv4 = "";
                    break;
                case "15":
                    vmMain.Server15IPv4 = "";
                    break;
                case "16":
                    vmMain.Server16IPv4 = "";
                    break;

            }


            //  //tab header set back to default
            //vm.Server1TabHeader = "Server" + vm.WhichTab;

            //Reset Test Certificate properties
            vm.TestCertificateS1 = null;
            vm.CertificateView1 = null;
            vm.TestCertificatePrinted1 = false;
            vm.UpdatesRun = "";

            TabItem ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + vm.WhichTab) as TabItem;
            if (ServerTab != null)
            {
                ServerTab.Header = "Server" + vm.WhichTab;
                ServerTab.Foreground = Brushes.MediumPurple;
            }

        }



        private void SkipBTN1_Click(object sender, RoutedEventArgs e)
        {
            // vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
            //reset flipview to start
            FlipView1.SelectedIndex = 4;
        }



        private void ReadSvrBTN_Click(object sender, RoutedEventArgs e)
        {

        }
        private void CancelScriptS1_Click(object sender, RoutedEventArgs e)
        {
            vm.CancelScriptS1 = true;
        }



        private void LaunchRemoteConsoleS1_Click(object sender, RoutedEventArgs e)
        {
            //Select the remote console tab
            Gen10Tab1.SelectedIndex = 9;
            //close previous if there
            CloseAppByPid(vm.HPRemoteConsolePIDS1);
            //MessageBox.Show(vm.HPRemoteConsolePIDS1.ToString());
            //await PutTaskDelay(3000);

            vm.HostExternalEXE(@"" + vm.myDocs + @"\HPTools\HP iLO Integrated Remote Console\HPLOCONS.exe", @"-addr " + vm.Server1IPv4 + ":443 -name Administrator -password " + vm.OverridePasswordS1.Trim() + " -lang en", WFHS1);
        }


        private void CloseAppByPid(int processID)
        {
            try
            {
                Process proc = Process.GetProcessById(processID);
                //MessageBox.Show(proc.ProcessName);
                if (proc.ProcessName.Contains("HPLOCONS"))
                {
                    //MessageBox.Show(proc.ProcessName);

                    proc.Kill();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        private void CloneUpdatesBTN_Click(object sender, RoutedEventArgs e)
        {
            //Duplicate Updates Across The Board

            // MessageBox.Show(vm.WhichTab);

            //Clear Collection
            vmMain.UserControlCollection.Clear();

            //Load collection with all instances
            vmMain.UserControlCollection.Add(vmMain.CurrentView1);
            vmMain.UserControlCollection.Add(vmMain.CurrentView2);
            vmMain.UserControlCollection.Add(vmMain.CurrentView3);
            vmMain.UserControlCollection.Add(vmMain.CurrentView4);
            vmMain.UserControlCollection.Add(vmMain.CurrentView5);
            vmMain.UserControlCollection.Add(vmMain.CurrentView6);
            vmMain.UserControlCollection.Add(vmMain.CurrentView7);
            vmMain.UserControlCollection.Add(vmMain.CurrentView8);
            vmMain.UserControlCollection.Add(vmMain.CurrentView9);
            vmMain.UserControlCollection.Add(vmMain.CurrentView10);
            vmMain.UserControlCollection.Add(vmMain.CurrentView11);
            vmMain.UserControlCollection.Add(vmMain.CurrentView12);
            vmMain.UserControlCollection.Add(vmMain.CurrentView13);
            vmMain.UserControlCollection.Add(vmMain.CurrentView14);
            vmMain.UserControlCollection.Add(vmMain.CurrentView15);
            vmMain.UserControlCollection.Add(vmMain.CurrentView16);



            //Loop through collection
            foreach (HPG10AMDServerView itm in vmMain.UserControlCollection)
            {
                if (itm.vm.GenGenericServerInfo1 != null)
                {
                    //Update inner vm with select file name
                    itm.vm.BIOSUpgradePath1 = vm.BIOSUpgradePath1;
                    itm.vm.ILOUpgradePath1 = vm.ILOUpgradePath1;
                    itm.vm.SPSUpgradePath1 = vm.SPSUpgradePath1;
                    itm.vm.IENGUpgradePath1 = vm.IENGUpgradePath1;

                    //Check if update file exists and make a copy of the update on the other servers


                    //BIOS UPDATE
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + itm.vm.Server1IPv4 + @"BIOSUpdate.bin"))
                    {
                        //delete file before attempting a copy


                    }
                    else
                    {
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.bin"))
                            File.Copy(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.bin", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + itm.vm.Server1IPv4 + @"BIOSUpdate.bin");
                    }


                    //ILO UPDATE
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + itm.vm.Server1IPv4 + @"ILOUpdate.bin"))
                    {


                    }
                    else
                    {
                        //Copy the Update file and rename with cloned server ip
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"ILOUpdate.bin"))
                            File.Copy(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"ILOUpdate.bin", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + itm.vm.Server1IPv4 + @"ILOUpdate.bin");
                    }




                    //CONTROLLER
                    //SPS UPDATE
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + itm.vm.Server1IPv4 + @"SPSUpdate.bin"))
                    {


                    }
                    else
                    {
                        //Copy the Update file and rename with cloned server ip
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"SPSUpdate.bin"))
                            File.Copy(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"SPSUpdate.bin", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + itm.vm.Server1IPv4 + @"SPSUpdate.bin");
                    }

                    //IENG UPDATE

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + itm.vm.Server1IPv4 + @"IENGUpdate.bin"))
                    {


                    }
                    else
                    {
                        //Copy the Update file and rename with cloned server ip
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"IENGUpdate.bin"))
                            File.Copy(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"IENGUpdate.bin", @"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + itm.vm.Server1IPv4 + @"IENGUpdate.bin");
                    }



                    //RUN Update Scripts
                    //itm.vm.GetHPServerDataCommand.Execute(new String[] { "WipeScripts", "Gen10" });

                }
                // MessageBox.Show(itm.vm.DellServerS1.ServiceTag);
            }


        }




        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }


        private void RunUpdatesBTN_Click(object sender, RoutedEventArgs e)
        {

            //Run Wipe Scripts
            RunScripts("WipeScripts", "Gen10");

        }

        private void RunScripts(string Option, string Manufacturer)
        {
            //Clear Collection
            vmMain.UserControlCollection.Clear();

            //Load collection with all instances
            vmMain.UserControlCollection.Add(vmMain.CurrentView1);
            vmMain.UserControlCollection.Add(vmMain.CurrentView2);
            vmMain.UserControlCollection.Add(vmMain.CurrentView3);
            vmMain.UserControlCollection.Add(vmMain.CurrentView4);
            vmMain.UserControlCollection.Add(vmMain.CurrentView5);
            vmMain.UserControlCollection.Add(vmMain.CurrentView6);
            vmMain.UserControlCollection.Add(vmMain.CurrentView7);
            vmMain.UserControlCollection.Add(vmMain.CurrentView8);
            vmMain.UserControlCollection.Add(vmMain.CurrentView9);
            vmMain.UserControlCollection.Add(vmMain.CurrentView10);
            vmMain.UserControlCollection.Add(vmMain.CurrentView11);
            vmMain.UserControlCollection.Add(vmMain.CurrentView12);
            vmMain.UserControlCollection.Add(vmMain.CurrentView13);
            vmMain.UserControlCollection.Add(vmMain.CurrentView14);
            vmMain.UserControlCollection.Add(vmMain.CurrentView15);
            vmMain.UserControlCollection.Add(vmMain.CurrentView16);



            //Loop through collection
            foreach (HPG10AMDServerView itm in vmMain.UserControlCollection)
            {
                if (itm.vm.GenGenericServerInfo1 != null)
                {
                    //RUN Update Scripts
                    itm.vm.GetHPServerDataCommand.Execute(new String[] { Option, Manufacturer });

                }
                // MessageBox.Show(itm.vm.DellServerS1.ServiceTag);
            }
        }


        private void ServerIPTXTB1_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (vm.Server1IPv4 != string.Empty)
                {
                    //RUN Update Scripts
                    vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen10" });
                    //MessageBox.Show(itm.vm.ChassisSerialNo);
                }
            }
        }

        private void RunGetServerInfo(string Option, string Manufacturer)
        {
            try
            {


                //Clear Collection
                vmMain.UserControlCollection.Clear();

                //Load collection with all instances
                vmMain.UserControlCollection.Add(vmMain.CurrentView1);
                vmMain.UserControlCollection.Add(vmMain.CurrentView2);
                vmMain.UserControlCollection.Add(vmMain.CurrentView3);
                vmMain.UserControlCollection.Add(vmMain.CurrentView4);
                vmMain.UserControlCollection.Add(vmMain.CurrentView5);
                vmMain.UserControlCollection.Add(vmMain.CurrentView6);
                vmMain.UserControlCollection.Add(vmMain.CurrentView7);
                vmMain.UserControlCollection.Add(vmMain.CurrentView8);
                vmMain.UserControlCollection.Add(vmMain.CurrentView9);
                vmMain.UserControlCollection.Add(vmMain.CurrentView10);
                vmMain.UserControlCollection.Add(vmMain.CurrentView11);
                vmMain.UserControlCollection.Add(vmMain.CurrentView12);
                vmMain.UserControlCollection.Add(vmMain.CurrentView13);
                vmMain.UserControlCollection.Add(vmMain.CurrentView14);
                vmMain.UserControlCollection.Add(vmMain.CurrentView15);
                vmMain.UserControlCollection.Add(vmMain.CurrentView16);

                //int TabID = 0;

                //Loop through collection
                foreach (HPG10AMDServerView itm in vmMain.UserControlCollection)
                {

                    if (itm.vm.IsScriptRunningS1 != true && !string.IsNullOrEmpty(vm.Server1IPv4))
                    {

                        //RUN Update Scripts
                        itm.vm.GetHPServerDataCommand.Execute(new String[] { Option, Manufacturer });
                        //MessageBox.Show(itm.vm.ChassisSerialNo);
                    }

                    // MessageBox.Show(itm.vm.DellServerS1.ServiceTag);
                }


            }
            catch (Exception ex)
            {

                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (vm.WarningMessage1 != "")
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Green;
                    }

                }
            }
        }



        private void GetInfoBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                //Run get server info across all active servers
                RunGetServerInfo("ServerInfo", "Gen10");
            }
            catch (Exception ex)
            {
                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (vm.WarningMessage1 != "")
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Green;
                    }

                }
            }
        }

        private void RunFinalAllScripts_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Run Wipe Scripts
                RunScripts("FinalScripts", "Gen10");
            }
            catch (Exception ex)
            {
                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (vm.WarningMessage1 != "")
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Green;
                    }

                }
            }
        }

        private void RunIloUpdatesBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Run Wipe Scripts
                RunScripts("IloOnly", "Gen10");
            }
            catch (Exception ex)
            {
                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (vm.WarningMessage1 != "")
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Green;
                    }

                }
            }
        }

        private void OPS1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void OPS1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //RUN Server Info Scripts
                vm.GetHPServerDataCommand.Execute(new String[] { "ServerInfo", "Gen10" });

            }
        }

        private void ChkFWBTN_Click(object sender, RoutedEventArgs e)
        {
            //Check Firmware against Server Values
            FirmwareUpdateCheck();
        }

        public void FirmwareUpdateCheck()
        {


            string bios = vm.BIOSUpgradePath1?.Replace(".signed.bin", "").Replace(".bin", "");

            string ilo = vm.ILOUpgradePath1?.Replace(".signed.bin", "").Replace(".bin", "");

            string sps = vm.SPSUpgradePath1?.Replace(".signed.bin", "").Replace(".bin", "");

            string ie = vm.IENGUpgradePath1?.Replace(".signed.bin", "").Replace(".bin", "");

            vm.WarningMessage1 = "";



            //Check for Server Gen to determine 
            //bios
            if (!string.IsNullOrEmpty(bios) && !string.IsNullOrEmpty(vm.GenGenericServerInfo1.BiosCurrentVersion))
            {



                var biosArray = bios.Split('_');
                bool biosMatch = false;
                int indexPosition = 0;
                //Gen8 Uses date only in Firmware Filename no version info
                if (vm.GenGenericServerInfo1.Model.Contains("Gen8"))
                {

                    //Assign Selected Update
                    string stringWithDate = vm.BIOSUpgradePath1;

                    //Check if hyphen present
                    if (stringWithDate.Contains("-"))
                    {
                        //GEN8 BIOS
                        Match match = Regex.Match(stringWithDate, @"\d{2}-\d{2}-\d{4}");
                        string date = match.Value;

                        DateTime dateValue = new DateTime();

                        if (!string.IsNullOrEmpty(date))
                        {
                            var usCulture = "en-US";

                            dateValue = DateTime.ParseExact(date, "dd-MM-yyyy", new CultureInfo(usCulture, false)).Date;


                            var uk = dateValue.ToString(new CultureInfo("en-GB", true));
                            var us = dateValue.ToString(new CultureInfo("en-US", true).DateTimeFormat.ShortDatePattern);


                            //MessageBox.Show(us.ToString().Replace("00:00:00", "").Trim());


                            if (vm.GenGenericServerInfo1 != null)
                                if (vm.GenGenericServerInfo1.BiosCurrentVersion.Contains(us.ToString().Replace("00:00:00", "").Trim()))
                                {
                                    //Clear Update
                                    vm.BIOSUpgradePath1 = String.Empty;

                                    //CLear Flash fie 
                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin"))
                                    {
                                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin");
                                    }

                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin"))
                                    {
                                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin");
                                    }

                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.bin"))
                                    {
                                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.bin");
                                    }

                                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin"))
                                    {
                                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin");
                                    }

                                    vm.UpdatesRun += @" [BIOS] ";
                                    //MessageBox.Show("Match");
                                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.BIOSUpgradePath1 + " doesn't appear to have applied. Please run Get Server Info again to double check. If not please run the update again. ");

                                }
                                else
                                {
                                    vm.WarningMessage1 += "[Firmware " + vm.BIOSUpgradePath1 + " failed! Please rerun the update.]";
                                    //MessageBox.Show("No Match");
                                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.BIOSUpgradePath1 + " doesn't appear to have applied. Please run Get Server Info again to double check. If not please run the update again. ");
                                }
                        }
                    }

                }
                else if (vm.GenGenericServerInfo1.Model.Contains("Gen9") || vm.GenGenericServerInfo1.Model.Contains("Gen10") || vm.GenGenericServerInfo1.Model.Contains("Gen 10") || vm.GenGenericServerInfo1.Model.Contains("Gen11") || vm.GenGenericServerInfo1.Model.Contains("Gen 11"))
                {


                    //Loop through array and find a match
                    foreach (var arrayStr in biosArray)
                    {
                        if (indexPosition > 0)
                        {
                            Console.WriteLine(arrayStr);
                            if (vm.GenGenericServerInfo1 != null)
                                if (vm.GenGenericServerInfo1.BiosCurrentVersion.Contains("v" + arrayStr))
                                {
                                    //As soon as a match is found exit
                                    biosMatch = true;
                                    break;

                                }
                        }

                        indexPosition += 1;


                    }
                    //Check if match was found 
                    if (biosMatch == true)
                    {
                        //Clear Update
                        vm.BIOSUpgradePath1 = String.Empty;

                        //CLear Flash fie 
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.bin");
                        }

                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin");
                        }

                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.bin"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.bin");
                        }

                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin"))
                        {
                            File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"BIOSUpdate.signed.bin");
                        }

                        vm.UpdatesRun += @" [BIOS] ";
                    }
                    else
                    {
                        vm.WarningMessage1 += "[Firmware " + vm.BIOSUpgradePath1 + " failed! Please rerun the update.]";
                        //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.BIOSUpgradePath1 + " failed to apply!!! Please rerun the update.");
                    }

                }


            }




            //ILO
            if (!string.IsNullOrEmpty(ilo) && !string.IsNullOrEmpty(vm.GenGenericServerInfo1.iLOVersion))
            {
                //Split update into array
                var iloArray = ilo.Split('_');
                bool iloMatch = false;
                //Loop through array and find a match
                foreach (var arrayStr in iloArray)
                {

                    Console.WriteLine(arrayStr);
                    if (vm.GenGenericServerInfo1 != null)
                        //Take out full stop in version to help match
                        if (vm.GenGenericServerInfo1.iLOVersion.Replace(".", "").Contains(arrayStr))
                        {
                            //As soon as a match is found exit
                            iloMatch = true;
                            break;
                        }

                }

                //Check if match was found
                if (iloMatch == true)
                {
                    //Clear Update 
                    vm.ILOUpgradePath1 = String.Empty;

                    //CLear Flash fie 
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.signed.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"ILOUpdate.signed.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"ILOUpdate.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"ILOUpdate.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"ILOUpdate.signed.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"ILOUpdate.signed.bin");
                    }

                    vm.UpdatesRun += @" [CONTROLLER] ";
                }
                else
                {
                    vm.WarningMessage1 += "[Firmware " + vm.ILOUpgradePath1 + " failed! Please rerun the update.]";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.CONTUpgradePath1 + " failed to apply!!! Please rerun the update.");
                }

            }






            //SPS

            if (!string.IsNullOrEmpty(sps) && !string.IsNullOrEmpty(vm.GenGenericServerInfo1.SPSVersion))
            {


                //Split update into array
                var spsArray = sps.Split('_');
                bool spsMatch = false;
                //Loop through array and find a match
                foreach (var arrayStr in spsArray)
                {
                    //Strip leading zeros out  (?<=.\d+.)0   ^(?!0\d)\d+(?:\.\d{1,2})?$
                    string arrayReplace = Regex.Replace(arrayStr, @"(?<=.\d+.)0", String.Empty);
                    arrayReplace = arrayReplace.Replace(".0", ".");

                    Console.WriteLine(arrayStr);
                    Console.WriteLine(arrayReplace);
                    Console.WriteLine(arrayReplace.TrimStart('0'));
                    if (vm.GenGenericServerInfo1 != null)//Strip leading zeros out
                        if (vm.GenGenericServerInfo1.SPSVersion.Contains(arrayReplace.TrimStart('0')))
                        {
                            //As soon as a match is found exit
                            spsMatch = true;
                            break;
                        }

                }

                //Check if match was found
                if (spsMatch == true)
                {
                    //Clear Update 
                    vm.SPSUpgradePath1 = String.Empty;

                    //Clear Flash files
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.signed.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.signed.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"SPSUpdate.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"SPSUpdate.signed.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"SPSUpdate.signed.bin");
                    }

                    //Update Certificate Field 
                    vm.UpdatesRun += @" [SPS] ";
                }
                else
                {
                    vm.WarningMessage1 += "[Firmware " + vm.SPSUpgradePath1 + " failed! Please rerun the update.]";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.CONT2UpgradePath1 + " failed to apply!!! Please rerun the update.");
                }
            }

            //Innovation Engine
            if (!string.IsNullOrEmpty(ie) && !string.IsNullOrEmpty(vm.GenGenericServerInfo1.InnovationEngine))
            {
                var ieArray = ie.Split('_');
                bool ieMatch = false;
                //Loop through array and find a match
                foreach (var arrayStr in ieArray)
                {
                    Console.WriteLine(arrayStr);
                    if (vm.GenGenericServerInfo1 != null)
                        if (vm.GenGenericServerInfo1.InnovationEngine == arrayStr)
                        {
                            //As soon as a match is found exit
                            ieMatch = true;
                            break;

                        }
                }

                //Check if match was found
                if (ieMatch == true)
                {


                    //CLEAR Flash File INFORMATION
                    vm.IENGUpgradePath1 = String.Empty;

                    //Clear Flash files
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"IENGUpdate.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"IENGUpdate.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"IENGUpdate.signed.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\HPCONFIGUTIL\" + vm.Server1IPv4 + @"IENGUpdate.signed.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"IENGUpdate.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"IENGUpdate.bin");
                    }

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"IENGUpdate.signed.bin"))
                    {
                        File.Delete(@"" + vm.myDocs + @"\HPTOOLS\RESTful Interface Tool\" + vm.Server1IPv4 + @"IENGUpdate.signed.bin");
                    }
                    //for test certificate
                    vm.UpdatesRun += @" [IEngine] ";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.IDRACUpgradePath1 + " has updated successfully!");
                }
                else
                {
                    vm.WarningMessage1 += "[Firmware " + vm.IENGUpgradePath1 + " failed! Please rerun the update.]";
                    //vm.SystemMessageDialog("Nebula Firmware Notification", "Selected Firmware " + vm.IDRACUpgradePath1 + " failed to apply!!! Please rerun the update.");
                }
            }












        }
    }
}

