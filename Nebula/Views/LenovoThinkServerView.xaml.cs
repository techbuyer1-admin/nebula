﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Nebula.ViewModels;
using Nebula.Models;
using MahApps.Metro.Controls;
using Nebula.Commands;
using System.IO;
using Nebula.Paginators;
using System.Windows.Media;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Net.Security;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Net.Http;
using Nebula.LenovoServers;
using Nebula.Helpers;
using MahApps.Metro.Controls.Dialogs;
using System.Windows.Input;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for LenovoThinkServerView.xaml
    /// </summary>
    public partial class LenovoThinkServerView : UserControl
    {

        TabItem ServerTab = null;

        //declare viewmodel
        MainViewModel vm = new MainViewModel(DialogCoordinator.Instance);    //
        MainViewModel vmMain;


        public LenovoThinkServerView()
        {
            InitializeComponent();

            this.DataContext = vm;

            // vm = new MainViewModel();

            //CLEAR SERVER PROPERTIES ON LOAD
            //reset flipview to start
            //FlipView1.SelectedIndex = 0;
            //delete server object
            //vm.LenovoServerS1 = null;
            // tab header set back to default
            vm.Server1TabHeader = "Server1";
            //clear server ip
            vm.Server1IPv4 = string.Empty;
            //reset flipview to start

            vm.OverridePasswordS1 = "PASSW0RD";

        }


        public LenovoThinkServerView(MainViewModel PassedViewModel, string whichTab)
        {
            InitializeComponent();
            //get the passed in viewmodel and assign to the local vm variable
            vmMain = PassedViewModel;

            this.DataContext = vm;

            //Set Tab
            vm.WhichTab = whichTab;
            //LenovoServerUC.Name = InstanceName;


            //CLEAR SERVER PROPERTIES ON LOAD
            //reset flipview to start
            //FlipView1.SelectedIndex = 0;
            //delete server object
            vm.LenovoServerS1 = null;
            // tab header set back to default
            vm.Server1TabHeader = "Server1";
            //clear server ip
            vm.Server1IPv4 = string.Empty;
            //reset flipview to start

            // Wire up event for webbrowsers
            WebBrowse1.LoadCompleted += WebBrowse1_LoadCompleted;


            // RunServerScriptsAllS1.Visibility = Visibility.Collapsed;


            vm.OverridePasswordS1 = "PASSW0RD";







            // ADD THIS TO GET SERVER INFO SCRIPT


        }



        public static void AddDocument(FlowDocument from, FlowDocument to)
        {
            TextRange range = new TextRange(from.ContentStart, from.ContentEnd);
            MemoryStream stream = new MemoryStream();
            System.Windows.Markup.XamlWriter.Save(range, stream);
            range.Save(stream, DataFormats.XamlPackage);
            TextRange range2 = new TextRange(to.ContentEnd, to.ContentEnd);
            range2.Load(stream, DataFormats.XamlPackage);
        }


        private void DoThePrint(System.Windows.Documents.FlowDocument document)
        {
            // Clone the source document's content into a new FlowDocument.
            // This is because the pagination for the printer needs to be
            // done differently than the pagination for the displayed page.
            // We print the copy, rather that the original FlowDocument.
            System.IO.MemoryStream s = new System.IO.MemoryStream();
            TextRange source = new TextRange(document.ContentStart, document.ContentEnd);
            source.Save(s, DataFormats.Xaml);
            FlowDocument copy = new FlowDocument();
            TextRange dest = new TextRange(copy.ContentStart, copy.ContentEnd);
            dest.Load(s, DataFormats.Xaml);

            // Create a XpsDocumentWriter object, implicitly opening a Windows common print dialog,
            // and allowing the user to select a printer.

            // get information about the dimensions of the seleted printer+media.
            System.Printing.PrintDocumentImageableArea ia = null;
            System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);

            if (docWriter != null && ia != null)
            {
                DocumentPaginator paginator = ((IDocumentPaginatorSource)copy).DocumentPaginator;

                // Change the PageSize and PagePadding for the document to match the CanvasSize for the printer device.
                paginator.PageSize = new Size(ia.MediaSizeWidth, ia.MediaSizeHeight);
                Thickness t = new Thickness(72);  // copy.PagePadding;
                copy.PagePadding = new Thickness(
                                 Math.Max(ia.OriginWidth, t.Left),
                                   Math.Max(ia.OriginHeight, t.Top),
                                   Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
                                   Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));

                copy.ColumnWidth = double.PositiveInfinity;
                //copy.PageWidth = 528; // allow the page to be the natural with of the output device

                // Send content to the printer.
                docWriter.Write(paginator);
            }

        }


        //private void PrintBTNS1_Click(object sender, RoutedEventArgs e)
        //{

        //    ////Set the serial number to text on the clipboard  Use control v
        //    if (vm.LenovoServerS1 != null)
        //    {
        //        //MessageBox.Show(vm.LenovoServerS1.SystemSerialNumber);

        //        if (!string.IsNullOrEmpty(vm.LenovoServerS1.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS1.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);


        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS1);
        //    }



        //}










        //Previous slide to help skip Test Cert for GOT
        int previousSlide = 0;

        private void FlipView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var flipview = ((FlipView)sender);
            switch (flipview.SelectedIndex)
            {
                case 0:
                    flipview.BannerText = "Lenovo Procedure";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                    }

                    break;
                case 1:
                    flipview.BannerText = "Obtain the DHCP Address";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                    }
                    break;
                case 2:
                    flipview.BannerText = "Enter Server IP & Run Scripts";
                    if (vm != null)
                    {

                        //vm.ILORestOutput1 = "";

                        //Runs a check every 20 seconds for LCD and Info
                        if (string.IsNullOrWhiteSpace(vm.Server1IPv4) != true || vm.Server1IPv4 != "")
                        {
                            //await CheckLCDDisplay(vm.Server1IPv4, vm.myDocs, vm.IsScriptRunningS1, vm.IsServerThere1, "S1");
                        }
                        //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);
                        //Load Ultima URL
                        if (vm.LenovoServerS1 != null)
                        {
                            if (vm.LenovoServerS1.Manufacturer != null && vm.LenovoServerS1.Model != null)
                                vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS1.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS1.Model + " Server &go=Go";
                        }


                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";

                    }
                    break;

                case 3:
                    flipview.BannerText = "Server Reports & Checklist";
                    if (vm != null)
                    {

                        if (vm.LenovoServerS1 != null)
                        {
                            //await PutTaskDelay(500);


                            //ADD GOT Specific items
                            if (StaticFunctions.Department == "GoodsOutTech")
                            {
                                flipview.BannerText = "Server Certificate";
                                //Create instance of Checklist view and pass in the instance of the mainview model
                                vm.ChecklistView1 = new ServerCertReportView(vm);
                                if (vm != null)
                                {
                                    vm.GenerateTestCertificate("Lenovo");
                                }
                                //Create instance of HP Test Report View
                                vm.ServerCertView1 = new TestCertificateView(vm);
                                //Create instance of Lenovo Test Report View
                                vm.ReportView1 = new LenovoReportsView(vm);

                            }
                            else
                            {
                                //Goods In
                                flipview.BannerText = "Server Checklist";
                                //Create instance of Checklist view and pass in the instance of the mainview model
                                vm.ChecklistView1 = new CheckListView(vm);
                                //Create instance of Lenovo Test Report View
                                vm.ReportView1 = new LenovoReportsView(vm);

                            }


                            //vm.ProgressVisibility = Visibility.Hidden;
                            //vm.ProgressIsActive = false;
                            //vm.ProgressPercentage = 0;
                            //vm.ProgressMessage = "";
                            vm.ProgressMessage1 = "";
                            //vm.ILORestOutput1 = "";
                            //Load Ultima URL
                            if (vm.LenovoServerS1 != null)
                            {
                                if (vm.LenovoServerS1.Manufacturer != null && vm.LenovoServerS1.Model != null)
                                    vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS1.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS1.Model + " Server &go=Go";
                            }

                            // Clear QR bitmap image.
                            vm.ChecklistQRCode = null;



                        }
                    }
                    break;
                case 4:
                    flipview.BannerText = "Final Wipe Scripts";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                        vm.ProgressMessage1 = "";
                        vm.ILORestOutput1 = "";
                        //Set previous slide to help skip Test Cert for GOT
                        previousSlide = 4;

                    }
                    break;
                case 5:
                    flipview.BannerText = "Test Certificate";
                    if (vm != null)
                    {
                        vm.ILORestOutput1 = "";

                        //ADD GOT Specific items
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //GOT Skip the Test Cert Sliide
                            switch (previousSlide)
                            {
                                case 4:
                                    vm.CertificateView1 = null;
                                    flipview.SelectedIndex = 6;
                                    break;
                                case 6:
                                    vm.CertificateView1 = null;
                                    flipview.SelectedIndex = 4;
                                    break;
                            }

                        }
                        else
                        {
                            //Show for Goods In
                            vm.GenerateTestCertificate("LenovoThink");
                        }

                    }
                    break;
                case 6:
                    flipview.BannerText = "Completion";

                    if (vm != null)
                    {
                        //Reset license properties
                        vm.GetServerInfoInitialRun = false;

                        //Set previous slide to help skip Test Cert for GOT
                        previousSlide = 6;

                    }
                    break;
            }
        }







        private void RefreshDataS1BTN_Click(object sender, RoutedEventArgs e)
        {
            if (vm.LenovoServerS1 != null)
            {

                vm.GetLenovoDataCommand.Execute(new String[] { "ServerInfo", "Lenovo" });
                // MessageBox.Show("Dropped Through");
                vm.LenovoServerS1.TestedBy = vm.CurrentUser.Replace("Checked By:", "");
                //vm.LenovoGenerateReport(LenovoServerReportS1, vm.LenovoServerS1);
            }
        }






        private async Task CheckLCDDisplay(string ServerIP, string myDocs, bool OtherScript, string ServerThere, string ServerNumber)
        {

            ProcessPiper pp = new ProcessPiper(vm);


            if (ServerIP != string.Empty)
                while (true)
                {

                    if (OtherScript != true)
                    {


                        if (ServerThere == "Yes")
                        {


                            switch (ServerNumber)
                            {
                                case "S1":
                                    //get LCD display
                                    //MessageBox.Show("Yep");
                                    await System.Threading.Tasks.Task.Run(() => pp.StartSensorInventoryRACDM1(new System.IO.FileInfo(@"" + myDocs + @"\HPTOOLS\Lenovo\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + ServerIP + @" -u root -p calvin --nocertwarn get System.LCD", @"" + myDocs + @"\HPTOOLS\Lenovo\SysMgt\iDRACTools\racadm"));
                                    break;

                            }




                        }

                    }

                    //MessageBox.Show("Fire");
                    await Task.Delay(TimeSpan.FromSeconds(20));
                }
        }


        private async Task RefreshServerInfo(string ServerIP, string myDocs)
        {
            ProcessPiper pp = new ProcessPiper(vm);

            while (true)
            {
                if (vm.IsServerThere1 == "Yes")
                {
                    if (vm.IsScriptRunningS1 != true)
                    {
                        //MessageBox.Show("Fire1");
                        //acquire the server inventory 


                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDMGeneric1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Lenovo\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p calvin --nocertwarn hwinventory export -f " + vm.Server1IPv4 + @"Inventory.xml", @"" + vm.myDocs + @"\HPTOOLS\Lenovo\SysMgt\iDRACTools\racadm"));
                    }


                }

                //MessageBox.Show("Fire");
                await Task.Delay(TimeSpan.FromSeconds(80));
            }
        }




        //SERVER1
        private void SelectAllBtnS1_Click(object sender, RoutedEventArgs e)
        {


            //GIQ1CBS1.IsChecked = true;
            //GIQ2CBS1.IsChecked = true;
            ////GIQ3CBS1.IsChecked = true;
            //GIQ4CBS1.IsChecked = true;
            //GIQ5CBS1.IsChecked = true;
            //GIQ6CBS1.IsChecked = true;
            //GIQ7CBS1.IsChecked = true;
            //GIQ8CBS1.IsChecked = true;
            //GIQ9CBS1.IsChecked = true;
            //GIQ10CBS1.IsChecked = true;
            //GIQ11CBS1.IsChecked = true;
            //GIQ12CBS1.IsChecked = true;
            //GIQ13CBS1.IsChecked = true;
            //GIQ14CBS1.IsChecked = true;
            //GIQ15CBS1.IsChecked = true;
            ////GIQ16CBS1.IsChecked = true;
            //GIQ17CBS1.IsChecked = true;
            ////GIQ18CBS1.IsChecked = true;
            //GIQ19CBS1.IsChecked = true;
            //GIQ20CBS1.IsChecked = true;
            ////GIQ21CBS1.IsChecked = true;
            //GIQ22CBS1.IsChecked = true;
            ////GIQ23CBS1.IsChecked = true;
            //GIQ24CBS1.IsChecked = true;
            //GIQ25CBS1.IsChecked = true;
            ////MessageTB.Text = String.Empty;
        }

        private void ClearAllBtnS1_Click(object sender, RoutedEventArgs e)
        {
            //GIQ1CBS1.IsChecked = false;
            //GIQ2CBS1.IsChecked = false;
            ////GIQ3CBS1.IsChecked = false;
            //GIQ4CBS1.IsChecked = false;
            //GIQ5CBS1.IsChecked = false;
            //GIQ6CBS1.IsChecked = false;
            //GIQ7CBS1.IsChecked = false;
            //GIQ8CBS1.IsChecked = false;
            //GIQ9CBS1.IsChecked = false;
            //GIQ10CBS1.IsChecked = false;
            //GIQ11CBS1.IsChecked = false;
            //GIQ12CBS1.IsChecked = false;
            //GIQ13CBS1.IsChecked = false;
            //GIQ14CBS1.IsChecked = false;
            //GIQ15CBS1.IsChecked = false;
            ////GIQ16CBS1.IsChecked = false;
            //GIQ17CBS1.IsChecked = false;
            ////GIQ18CBS1.IsChecked = false;
            //GIQ19CBS1.IsChecked = false;
            //GIQ20CBS1.IsChecked = false;
            ////GIQ21CBS1.IsChecked = false;
            //GIQ22CBS1.IsChecked = false;
            ////GIQ23CBS1.IsChecked = false;
            //GIQ24CBS1.IsChecked = false;
            //GIQ25CBS1.IsChecked = false;
            ////MessageTB.Text = String.Empty;
        }




        private void GenCheck1_Click(object sender, RoutedEventArgs e)
        {

            //reset flipview to start
            FlipView1.SelectedIndex = 0;
            //add to database
            //vm.ServersProcessedCRUDCommand.Execute(vm);
            vm.LenovoFrontLCDS1 = string.Empty;
            //ChassisNoTXTBS1.Text = "";

            //clear server ip
            //vm.Server1IPv4 = string.Empty;
            vm.BIOSUpgradePath1 = string.Empty;
            vm.CONTUpgradePath1 = string.Empty;
            vm.IDRACUpgradePath1 = string.Empty;
            vm.ILORestOutput1 = string.Empty;
            vm.ProgressIsActive1 = false;
            vm.ProgressMessage1 = string.Empty;
            vm.ProgressPercentage1 = 0;
            //AddTestMsg1.Text = string.Empty;


            //Reset Test Certificate properties
            vm.TestCertificateS1 = null;
            vm.CertificateView1 = null;
            vm.TestCertificatePrinted1 = false;

            //Set Server Tab to Default
            TabItem ServerTab = Helper.GetDescendantFromName(Application.Current.MainWindow, "TabItemS" + vm.WhichTab) as TabItem;
            if (ServerTab != null)
                ServerTab.Header = "Server" + vm.WhichTab;
            ServerTab.Foreground = Brushes.MediumPurple;


            vm.OverridePasswordS1 = "PASSW0RD";

        }







        private void CancelScriptS1_Click(object sender, RoutedEventArgs e)
        {
            vm.CancelScriptS1 = true;
        }




        static readonly Guid SID_SWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");


        public async void Login2ServerWebPage(string ServerIP)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            //WebClient client = new WebClient();
            //Byte[] pageData = client.DownloadData("https://192.168.101.35");
            //string pageHtml = Encoding.ASCII.GetString(pageData);
            //Console.WriteLine(pageHtml);




            //Launch Lenovo Webpage   + "/console"
            //WebBrowse1.Navigate("https://" + vm.Server1IPv4 + "/console");
            WebBrowse1.Navigate("https://" + ServerIP + "");
            await PutTaskDelay(100);
            WebBrowse1.Focus();
            await PutTaskDelay(1000);
            WebBrowse1.Refresh();
            WebBrowse1.Focus();
            await PutTaskDelay(4000);
            WebBrowse1.Focus();


            // ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);


            //Need to use Microsoft HTML reference, to pass the WebBrowse Document to it
            mshtml.HTMLDocument sslpage = (mshtml.HTMLDocument)WebBrowse1.Document;

            if (sslpage.getElementById("overridelink") != null)
            {
                mshtml.IHTMLElement gohome = sslpage.getElementById("overridelink");
                //MessageBox.Show(gohome.id);
                gohome.click();
                await PutTaskDelay(4000);
            }


            //LOGIN
            //Need to use Microsoft HTML reference, to pass the WebBrowse Document to it 
            mshtml.HTMLDocument document = (mshtml.HTMLDocument)WebBrowse1.Document;

            //Get the user and password input boxes
            if (document.getElementById("user") != null)
            {
                mshtml.IHTMLElement userInput = document.getElementById("user");
                mshtml.IHTMLElement passwordInput = document.getElementById("password");
                userInput.innerText = "root";
                passwordInput.innerText = "calvin";
                mshtml.IHTMLElement OKButton = document.getElementById("btnOK");
                ////click the ok button
                OKButton.click();
            }

            await PutTaskDelay(20000);
            //////for frames embedded in main document
            // mshtml.HTMLDocument doc = this.WebBrowse1.Document as mshtml.HTMLDocument;
            //mshtml.HTMLWindow2 win = doc.frames.item(1) as mshtml.HTMLWindow2;
            //mshtml.HTMLDocument innerdoc = win.document as mshtml.HTMLDocument;
            //string str = innerdoc.body.innerText;
            //MessageBox.Show(str);
            // END OF FRAMES CAPTURE

            await PutTaskDelay(3000);

            //Need to use Microsoft HTML reference, to pass the WebBrowse Document to it 
            mshtml.HTMLDocument preview = (mshtml.HTMLDocument)WebBrowse1.Document;


            if (preview.getElementById("remoteConLaunch_link") != null)
            {
                mshtml.IHTMLElement remoteConsole = document.getElementById("remoteConLaunch_link");

                remoteConsole.click();

            }


            await PutTaskDelay(5000);


            //Get the user and password input boxes
            if (document.getElementById("user") != null)
            {
                mshtml.IHTMLElement userInput = document.getElementById("user");
                mshtml.IHTMLElement passwordInput = document.getElementById("password");
                userInput.innerText = "root";
                passwordInput.innerText = "calvin";
                mshtml.IHTMLElement OKButton = document.getElementById("btnOK");
                ////click the ok button
                OKButton.click();
            }

            await PutTaskDelay(2000);

            //Need to use Microsoft HTML reference, to pass the WebBrowse Document to it
            mshtml.HTMLDocument sslpage2 = (mshtml.HTMLDocument)WebBrowse1.Document;

            if (sslpage2.getElementById("overridelink") != null)
            {
                mshtml.IHTMLElement gohome = sslpage.getElementById("overridelink");
                //MessageBox.Show(gohome.id);
                gohome.click();
                await PutTaskDelay(4000);
            }

            //WebBrowse2.Navigate(WebBrowse1.Document);

            //Get the user and password input boxes
            if (preview.getElementById("console_preview_img_id") != null)
            {
                //MessageBox.Show("Console Preview");

                mshtml.IHTMLElement previewImg = document.getElementById("console_preview_img_id");




                var imgWidth = previewImg.getAttribute("width");
                var imgHeight = previewImg.getAttribute("height");
                imgWidth = 800;
                imgHeight = 450;


                //Path to preview image https://192.168.101.42/capconsole/scapture0.png?1607510739495
                //mshtml.IHTMLElement passwordInput = document.getElementById("password");
                //previewImg.innerHTML.Contains("width=") = "400";
                //passwordInput.innerText = "calvin";<img name="console_preview_img_id" id="console_preview_img_id" src="https://192.168.101.42/capconsole/scapture0.png?1607510739495" height="200" width="400" onerror="javascript:imageLoadError(this);" alt="">
                //mshtml.IHTMLElement OKButton = document.getElementById("btnOK");
                //////click the ok button
                //OKButton.click();
            }
        }



        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private void LaunchRemoteConsoleS1_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(@"https://" + vm.Server1IPv4.ToString() + @"/console");
            //Login to webpage
            Login2ServerWebPage(vm.Server1IPv4);
        }



        private async void LaunchVirtualConsoleS2_Click(object sender, RoutedEventArgs e)
        {
            //Need to use Microsoft HTML reference, to pass the WebBrowse Document to it 
            mshtml.HTMLDocument preview = (mshtml.HTMLDocument)WebBrowse1.Document;

            //Get the user and password input boxes
            //if (preview.getElementById("console_preview_img_id") != null)
            //{

            mshtml.IHTMLElement previewImg = preview.getElementById("da");

            //mshtml.IHTMLElement remoteConsole = preview.getElementById("remoteconlaunch_link");
            //mshtml.IHTMLElement previewImg = preview.getElementById("console_preview_img_id");
            //mshtml.IHTMLElement remoteConsole = preview.getElementById("remoteconlaunch_link");
            //MessageBox.Show(previewImg.getAttribute("class"));
            await PutTaskDelay(5000);

            //var imgWidth = previewImg.getAttribute("width");
            //var imgHeight = previewImg.getAttribute("height");
            //imgWidth = 500;
            //imgHeight = 250;


            //  MessageBox.Show(previewImg.getAttribute("width"));
            //   //remoteConsole.click();

            //Path to preview image https://192.168.101.42/capconsole/scapture0.png?1607510739495
            //mshtml.IHTMLElement passwordInput = document.getElementById("password");
            //previewImg.innerHTML.Contains("width=") = "400";
            //passwordInput.innerText = "calvin";<img name="console_preview_img_id" id="console_preview_img_id" src="https://192.168.101.42/capconsole/scapture0.png?1607510739495" height="200" width="400" onerror="javascript:imageLoadError(this);" alt="">
            //mshtml.IHTMLElement OKButton = document.getElementById("btnOK");
            //////click the ok button
            //OKButton.click();
            //}
        }


        private void SkipBTN1_Click(object sender, RoutedEventArgs e)
        {

            //reset flipview to start
            FlipView1.SelectedIndex = 2;
        }




        private void CloseAppByPid(int processID)
        {
            try
            {
                Process proc = Process.GetProcessById(processID);
                if (proc.ProcessName.Contains("HPLOCONS"))
                {
                    proc.Kill();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }



        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

        private void LaunchWebPage1_Click(object sender, RoutedEventArgs e)
        {
            //System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object sender1, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

            //System.Diagnostics.Process.Start(@"https://" + vm.Server1IPv4.ToString() + @"/console");



            //var myClient = new WebClient();
            //Stream response = myClient.OpenRead(@"https://" + vm.Server1IPv4.ToString() + @"/console");

            ////WebBrowse1.scr
            //// The stream data is used here.
            //response.Close();


            //String url = @"https://" + vm.Server1IPv4.ToString();
            //HttpWebRequest request = HttpWebRequest.CreateHttp(url);
            //request.ServerCertificateValidationCallback += (sender2, certificate, chain, sslPolicyErrors) => true;

            //MessageBox.Show(request.GetResponse().ToString());
            //client.
        }


        //WPF WEBBROWSER RELATED ITEMS
        private void WebBrowse1_Navigating(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            ////WORKS
            //////Clear any javacript warnings in webbrowser control
            dynamic activeX = this.WebBrowse1.GetType().InvokeMember("ActiveXInstance",
                BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null, this.WebBrowse1, new object[] { });

            activeX.Silent = true;
            //WORKS

            HideScriptErrors(WebBrowse1, true);

        }

        private void WebBrowse1_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            //TO LOAD ANY LINKS BACK INTO THE WEBBROWSER WINDOW
            //IServiceProvider serviceProvider = (IServiceProvider)WebBrowse1.Document;
            //Guid serviceGuid = SID_SWebBrowserApp;
            //Guid iid = typeof(SHDocVw.IWebBrowser2).GUID;
            //SHDocVw.IWebBrowser2 myWebBrowser2 = (SHDocVw.IWebBrowser2)serviceProvider.QueryService(ref serviceGuid, ref iid);
            //SHDocVw.DWebBrowserEvents_Event wbEvents = (SHDocVw.DWebBrowserEvents_Event)myWebBrowser2;
            //wbEvents.NewWindow += new SHDocVw.DWebBrowserEvents_NewWindowEventHandler(OnWebBrowserNewWindow);
        }



        void OnWebBrowserNewWindow(string URL, int Flags, string TargetFrameName, ref object PostData, string Headers, ref bool Processed)
        {
            Processed = true;
            WebBrowse1.Navigate(URL);
        }

        private void WebBrowse1_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            // double Zoom = 0.5;
            // mshtml.IHTMLDocument2 doc = WebBrowse1.Document as mshtml.IHTMLDocument2;
            ////doc.
            // doc.parentWindow.execScript("document.body.style.zoom=" + Zoom.ToString().Replace(",", ".") + ";");
        }

        public void HideScriptErrors(WebBrowser wb, bool Hide)
        {

            FieldInfo fiComWebBrowser = typeof(WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fiComWebBrowser == null) return;
            object objComWebBrowser = fiComWebBrowser.GetValue(wb);

            if (objComWebBrowser == null) return;

            objComWebBrowser.GetType().InvokeMember(
            "Silent", BindingFlags.SetProperty, null, objComWebBrowser, new object[] { Hide });

        }


        private void CloneUpdatesBTN_Click(object sender, RoutedEventArgs e)
        {
            //Duplicate Updates Across The Board

            // MessageBox.Show(vm.WhichTab);

            //Clear Collection
            vmMain.UserControlCollection.Clear();

            //Load collection with all instances
            vmMain.UserControlCollection.Add(vmMain.CurrentView1);
            vmMain.UserControlCollection.Add(vmMain.CurrentView2);
            vmMain.UserControlCollection.Add(vmMain.CurrentView3);
            vmMain.UserControlCollection.Add(vmMain.CurrentView4);
            vmMain.UserControlCollection.Add(vmMain.CurrentView5);
            vmMain.UserControlCollection.Add(vmMain.CurrentView6);
            vmMain.UserControlCollection.Add(vmMain.CurrentView7);
            vmMain.UserControlCollection.Add(vmMain.CurrentView8);
            vmMain.UserControlCollection.Add(vmMain.CurrentView9);
            vmMain.UserControlCollection.Add(vmMain.CurrentView10);
            vmMain.UserControlCollection.Add(vmMain.CurrentView11);
            vmMain.UserControlCollection.Add(vmMain.CurrentView12);
            vmMain.UserControlCollection.Add(vmMain.CurrentView13);
            vmMain.UserControlCollection.Add(vmMain.CurrentView14);
            vmMain.UserControlCollection.Add(vmMain.CurrentView15);
            vmMain.UserControlCollection.Add(vmMain.CurrentView16);



            //Loop through collection
            foreach (LenovoThinkServerView itm in vmMain.UserControlCollection)
            {
                if (itm.vm.LenovoServerS1 != null)
                {
                    //Update inner vm with select file name
                    itm.vm.BIOSUpgradePath1 = vm.BIOSUpgradePath1;
                    itm.vm.IMMUpgradePath1 = vm.IMMUpgradePath1;
                    itm.vm.DSAUpgradePath1 = vm.DSAUpgradePath1;


                    //Check if update file exists and make a copy of the update on the other servers

                    //BIOS
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\Lenovo One CLI Tool\" + itm.vm.Server1IPv4 + @"BIOSUpdate.exe"))
                    {
                        //delete file before attempting a copy


                    }
                    else
                    {
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\Lenovo One CLI Tool\" + vm.Server1IPv4 + @"BIOSUpdate.exe"))
                            File.Copy(@"" + vm.myDocs + @"\HPTOOLS\Lenovo One CLI Tool\" + vm.Server1IPv4 + @"BIOSUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\Lenovo One CLI Tool\" + itm.vm.Server1IPv4 + @"BIOSUpdate.exe");
                    }


                    //IMM EXE
                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\Lenovo One CLI Tool\" + itm.vm.Server1IPv4 + @"IMMUpdate.exe"))
                    {


                    }
                    else
                    {
                        //Copy the Update file and rename with cloned server ip
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\Lenovo One CLI Tool\" + vm.Server1IPv4 + @"IMMUpdate.exe"))
                            File.Copy(@"" + vm.myDocs + @"\HPTOOLS\Lenovo One CLI Tool\" + vm.Server1IPv4 + @"IMMUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\Lenovo One CLI Tool\" + itm.vm.Server1IPv4 + @"IMMUpdate.exe");
                    }





                    //DSA UPDATES

                    if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\Lenovo One CLI Tool\" + itm.vm.Server1IPv4 + @"DSAUpdate.exe"))
                    {


                    }
                    else
                    {
                        //Copy the Update file and rename with cloned server ip
                        if (File.Exists(@"" + vm.myDocs + @"\HPTOOLS\Lenovo One CLI Tool\" + vm.Server1IPv4 + @"DSAUpdate.exe"))
                            File.Copy(@"" + vm.myDocs + @"\HPTOOLS\Lenovo One CLI Tool\" + vm.Server1IPv4 + @"DSAUpdate.exe", @"" + vm.myDocs + @"\HPTOOLS\Lenovo One CLI Tool\" + itm.vm.Server1IPv4 + @"DSAUpdate.exe");
                    }


                }
                // MessageBox.Show(itm.vm.DellServerS1.ServiceTag);
            }


        }

        private void RunUpdatesBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                //Run Wipe Scripts
                RunScripts("WipeScripts", "Lenovo");

            }
            catch (Exception ex)
            {
                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (vm.WarningMessage1 != "")
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Green;
                    }

                }
            }

        }

        private void RunScripts(string Option, string Manufacturer)
        {

            try
            {


                //Clear Collection
                vmMain.UserControlCollection.Clear();

                //Load collection with all instances
                vmMain.UserControlCollection.Add(vmMain.CurrentView1);
                vmMain.UserControlCollection.Add(vmMain.CurrentView2);
                vmMain.UserControlCollection.Add(vmMain.CurrentView3);
                vmMain.UserControlCollection.Add(vmMain.CurrentView4);
                vmMain.UserControlCollection.Add(vmMain.CurrentView5);
                vmMain.UserControlCollection.Add(vmMain.CurrentView6);
                vmMain.UserControlCollection.Add(vmMain.CurrentView7);
                vmMain.UserControlCollection.Add(vmMain.CurrentView8);
                vmMain.UserControlCollection.Add(vmMain.CurrentView9);
                vmMain.UserControlCollection.Add(vmMain.CurrentView10);
                vmMain.UserControlCollection.Add(vmMain.CurrentView11);
                vmMain.UserControlCollection.Add(vmMain.CurrentView12);
                vmMain.UserControlCollection.Add(vmMain.CurrentView13);
                vmMain.UserControlCollection.Add(vmMain.CurrentView14);
                vmMain.UserControlCollection.Add(vmMain.CurrentView15);
                vmMain.UserControlCollection.Add(vmMain.CurrentView16);



                //Loop through collection
                foreach (LenovoThinkServerView itm in vmMain.UserControlCollection)
                {
                    if (itm.vm.LenovoServerS1 != null)
                    {
                        //RUN Update Scripts
                        itm.vm.RunLenovoScriptsCommandS1.Execute(new String[] { Option, Manufacturer });

                    }
                    // MessageBox.Show(itm.vm.DellServerS1.ServiceTag);
                }

            }
            catch (Exception ex)
            {
                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (vm.WarningMessage1 != "")
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Green;
                    }

                }
            }
        }


        private void ServerIPTXTB1_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //RUN Update Scripts
                vm.GetLenovoDataCommand.Execute(new String[] { "ServerInfo", "Lenovo" });

            }
        }



        [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("6d5140c1-7436-11ce-8034-00aa006009fa")]
        internal interface IServiceProvider
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            object QueryService(ref Guid guidService, ref Guid riid);
        }

        private void RunGetServerInfo(string Option, string Manufacturer)
        {
            try
            {


                //Clear Collection
                vmMain.UserControlCollection.Clear();

                //Load collection with all instances
                vmMain.UserControlCollection.Add(vmMain.CurrentView1);
                vmMain.UserControlCollection.Add(vmMain.CurrentView2);
                vmMain.UserControlCollection.Add(vmMain.CurrentView3);
                vmMain.UserControlCollection.Add(vmMain.CurrentView4);
                vmMain.UserControlCollection.Add(vmMain.CurrentView5);
                vmMain.UserControlCollection.Add(vmMain.CurrentView6);
                vmMain.UserControlCollection.Add(vmMain.CurrentView7);
                vmMain.UserControlCollection.Add(vmMain.CurrentView8);
                vmMain.UserControlCollection.Add(vmMain.CurrentView9);
                vmMain.UserControlCollection.Add(vmMain.CurrentView10);
                vmMain.UserControlCollection.Add(vmMain.CurrentView11);
                vmMain.UserControlCollection.Add(vmMain.CurrentView12);
                vmMain.UserControlCollection.Add(vmMain.CurrentView13);
                vmMain.UserControlCollection.Add(vmMain.CurrentView14);
                vmMain.UserControlCollection.Add(vmMain.CurrentView15);
                vmMain.UserControlCollection.Add(vmMain.CurrentView16);

                //int TabID = 0;

                //Loop through collection
                foreach (LenovoThinkServerView itm in vmMain.UserControlCollection)
                {

                    if (itm.vm.IsScriptRunningS1 != true && !string.IsNullOrEmpty(vm.Server1IPv4))
                    {

                        //RUN Update Scripts
                        itm.vm.GetLenovoDataCommand.Execute(new String[] { Option, Manufacturer });

                    }

                    // MessageBox.Show(itm.vm.DellServerS1.ServiceTag);
                }


            }
            catch (Exception ex)
            {

                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (vm.WarningMessage1 != "")
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Green;
                    }

                }
            }
        }



        private void GetInfoBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                //Run get server info across all active servers
                RunGetServerInfo("ServerInfo", "Lenovo");
            }
            catch (Exception ex)
            {
                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (vm.WarningMessage1 != "")
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Green;
                    }

                }
            }
        }

        private void OPS1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //RUN Update Scripts
                vm.GetLenovoDataCommand.Execute(new String[] { "ServerInfo", "Lenovo" });

            }
        }

        private void RunFinalAllScripts_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Run Wipe Scripts
                RunScripts("FinalScripts", "Lenovo");
            }
            catch (Exception ex)
            {
                vm.WarningMessage1 = "Error: " + ex.Message;
                vm.ProgressPercentage1 = 0;
                vm.ProgressIsActive1 = false;
                vm.ProgressVisibility1 = Visibility.Hidden;
                vm.ProgressMessage1 = "Scripts Cancelled!";
                vm.CancelScriptS1 = false;

                //set colours
                if (vm.WarningMessage1 != "")
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.MediumPurple;
                    }
                }
                else
                {
                    if (ServerTab != null)
                    {
                        ServerTab.Foreground = Brushes.Green;
                    }

                }
            }
        }
    }
}