﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for ITADToolsView.xaml
    /// </summary>
    public partial class ITADToolsView : UserControl
    {
        public ITADToolsView()
        {
            //set static variable Department for use when select server processing views for goods in goods out tech
            StaticFunctions.Department = "ITAD";
            InitializeComponent();
        }
    }
}
