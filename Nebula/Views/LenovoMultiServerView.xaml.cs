﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Nebula.ViewModels;
using Nebula.Models;
using MahApps.Metro.Controls;
using Nebula.Commands;
using System.IO;
using Nebula.Paginators;
using System.Windows.Media;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Net.Security;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Net.Http;
using Nebula.LenovoServers;
using Nebula.Helpers;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for LenovoMultiServerProcessingView.xaml
    /// </summary>
    public partial class LenovoMultiServerProcessingView : UserControl
    {


        //declare viewmodel
        MainViewModel vm;     //

        public LenovoMultiServerProcessingView()
        {
            InitializeComponent();



            //TabItem tabitem1 = Helper.FindVisualChildren<TabItem>(this).;

            //MessageBox.Show(tabitem1.Header.ToString());

            // Check Lenovo Server Type
            switch (vm.LenovoServerType)
            {
                case "XSystem":
                    //Redfish not implemented
                    vm.CurrentView1 = new LenovoServerView(vm, "1");
                    vm.CurrentView2 = new LenovoServerView(vm, "2");
                    vm.CurrentView3 = new LenovoServerView(vm, "3");
                    vm.CurrentView4 = new LenovoServerView(vm, "4");
                    vm.CurrentView5 = new LenovoServerView(vm, "5");
                    vm.CurrentView6 = new LenovoServerView(vm, "6");
                    vm.CurrentView7 = new LenovoServerView(vm, "7");
                    vm.CurrentView8 = new LenovoServerView(vm, "8");
                    vm.CurrentView9 = new LenovoServerView(vm, "9");
                    vm.CurrentView10 = new LenovoServerView(vm, "10");
                    vm.CurrentView11 = new LenovoServerView(vm, "11");
                    vm.CurrentView12 = new LenovoServerView(vm, "12");
                    vm.CurrentView13 = new LenovoServerView(vm, "13");
                    vm.CurrentView14 = new LenovoServerView(vm, "14");
                    vm.CurrentView15 = new LenovoServerView(vm, "15");
                    vm.CurrentView16 = new LenovoServerView(vm, "16");

                    break;
                case "XClarity":
                    vm.CurrentView1 = new LenovoThinkServerView(vm, "1");
                    vm.CurrentView2 = new LenovoThinkServerView(vm, "2");
                    vm.CurrentView3 = new LenovoThinkServerView(vm, "3");
                    vm.CurrentView4 = new LenovoThinkServerView(vm, "4");
                    vm.CurrentView5 = new LenovoThinkServerView(vm, "5");
                    vm.CurrentView6 = new LenovoThinkServerView(vm, "6");
                    vm.CurrentView7 = new LenovoThinkServerView(vm, "7");
                    vm.CurrentView8 = new LenovoThinkServerView(vm, "8");
                    vm.CurrentView9 = new LenovoThinkServerView(vm, "9");
                    vm.CurrentView10 = new LenovoThinkServerView(vm, "10");
                    vm.CurrentView11 = new LenovoThinkServerView(vm, "11");
                    vm.CurrentView12 = new LenovoThinkServerView(vm, "12");
                    vm.CurrentView13 = new LenovoThinkServerView(vm, "13");
                    vm.CurrentView14 = new LenovoThinkServerView(vm, "14");
                    vm.CurrentView15 = new LenovoThinkServerView(vm, "15");
                    vm.CurrentView16 = new LenovoThinkServerView(vm, "16");

                    break;
            }


            //CLEAR SERVER PROPERTIES ON LOAD
            //reset flipview to start
            //FlipView1.SelectedIndex = 0;
            //delete server object
            vm.LenovoServerS1 = null;
            // tab header set back to default
            vm.Server1TabHeader = "Server1";
            //clear server ip
            vm.Server1IPv4 = string.Empty;
         

            vm.OverridePasswordS1 = "PASSW0RD";
          

        }




        public LenovoMultiServerProcessingView(MainViewModel PassedViewModel)
        {
            InitializeComponent();
            //get the passed in viewmodel and assign to the local vm variable
            vm = PassedViewModel;

            // Check Lenovo Server Type
            switch (vm.LenovoServerType)
            {
                case "XSystem":
                    //Redfish not implemented
                    //MessageBox.Show("XSystem");
                    vm.CurrentView1 = new LenovoServerView(vm, "1");
                    vm.CurrentView2 = new LenovoServerView(vm, "2");
                    vm.CurrentView3 = new LenovoServerView(vm, "3");
                    vm.CurrentView4 = new LenovoServerView(vm, "4");
                    vm.CurrentView5 = new LenovoServerView(vm, "5");
                    vm.CurrentView6 = new LenovoServerView(vm, "6");
                    vm.CurrentView7 = new LenovoServerView(vm, "7");
                    vm.CurrentView8 = new LenovoServerView(vm, "8");
                    vm.CurrentView9 = new LenovoServerView(vm, "9");
                    vm.CurrentView10 = new LenovoServerView(vm, "10");
                    vm.CurrentView11 = new LenovoServerView(vm, "11");
                    vm.CurrentView12 = new LenovoServerView(vm, "12");
                    vm.CurrentView13 = new LenovoServerView(vm, "13");
                    vm.CurrentView14 = new LenovoServerView(vm, "14");
                    vm.CurrentView15 = new LenovoServerView(vm, "15");
                    vm.CurrentView16 = new LenovoServerView(vm, "16");

                    break;
                case "XClarity":
                    //Redfish
                    //MessageBox.Show("XClarity");
                    vm.CurrentView1 = new LenovoThinkServerView(vm, "1");
                    vm.CurrentView2 = new LenovoThinkServerView(vm, "2");
                    vm.CurrentView3 = new LenovoThinkServerView(vm, "3");
                    vm.CurrentView4 = new LenovoThinkServerView(vm, "4");
                    vm.CurrentView5 = new LenovoThinkServerView(vm, "5");
                    vm.CurrentView6 = new LenovoThinkServerView(vm, "6");
                    vm.CurrentView7 = new LenovoThinkServerView(vm, "7");
                    vm.CurrentView8 = new LenovoThinkServerView(vm, "8");
                    vm.CurrentView9 = new LenovoThinkServerView(vm, "9");
                    vm.CurrentView10 = new LenovoThinkServerView(vm, "10");
                    vm.CurrentView11 = new LenovoThinkServerView(vm, "11");
                    vm.CurrentView12 = new LenovoThinkServerView(vm, "12");
                    vm.CurrentView13 = new LenovoThinkServerView(vm, "13");
                    vm.CurrentView14 = new LenovoThinkServerView(vm, "14");
                    vm.CurrentView15 = new LenovoThinkServerView(vm, "15");
                    vm.CurrentView16 = new LenovoThinkServerView(vm, "16");

                    break;
            }

          


            //CLEAR SERVER PROPERTIES ON LOAD
            //reset flipview to start
            //FlipView1.SelectedIndex = 0;
            //delete server object
            vm.LenovoServerS1 = null;
            // tab header set back to default
            vm.Server1TabHeader = "Server1";
            //clear server ip
            vm.Server1IPv4 = string.Empty;
            //reset flipview to start
           
            vm.OverridePasswordS1 = "PASSW0RD";


            // ADD THIS TO GET SERVER INFO SCRIPT


        }



        public static void AddDocument(FlowDocument from, FlowDocument to)
        {
            TextRange range = new TextRange(from.ContentStart, from.ContentEnd);
            MemoryStream stream = new MemoryStream();
            System.Windows.Markup.XamlWriter.Save(range, stream);
            range.Save(stream, DataFormats.XamlPackage);
            TextRange range2 = new TextRange(to.ContentEnd, to.ContentEnd);
            range2.Load(stream, DataFormats.XamlPackage);
        }


        private void DoThePrint(System.Windows.Documents.FlowDocument document)
        {
            // Clone the source document's content into a new FlowDocument.
            // This is because the pagination for the printer needs to be
            // done differently than the pagination for the displayed page.
            // We print the copy, rather that the original FlowDocument.
            System.IO.MemoryStream s = new System.IO.MemoryStream();
            TextRange source = new TextRange(document.ContentStart, document.ContentEnd);
            source.Save(s, DataFormats.Xaml);
            FlowDocument copy = new FlowDocument();
            TextRange dest = new TextRange(copy.ContentStart, copy.ContentEnd);
            dest.Load(s, DataFormats.Xaml);

            // Create a XpsDocumentWriter object, implicitly opening a Windows common print dialog,
            // and allowing the user to select a printer.

            // get information about the dimensions of the seleted printer+media.
            System.Printing.PrintDocumentImageableArea ia = null;
            System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);

            if (docWriter != null && ia != null)
            {
                DocumentPaginator paginator = ((IDocumentPaginatorSource)copy).DocumentPaginator;

                // Change the PageSize and PagePadding for the document to match the CanvasSize for the printer device.
                paginator.PageSize = new Size(ia.MediaSizeWidth, ia.MediaSizeHeight);
                Thickness t = new Thickness(72);  // copy.PagePadding;
                copy.PagePadding = new Thickness(
                                 Math.Max(ia.OriginWidth, t.Left),
                                   Math.Max(ia.OriginHeight, t.Top),
                                   Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
                                   Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));

                copy.ColumnWidth = double.PositiveInfinity;
                //copy.PageWidth = 528; // allow the page to be the natural with of the output device

                // Send content to the printer.
                docWriter.Write(paginator);
            }

        }


        //private void PrintBTNS1_Click(object sender, RoutedEventArgs e)
        //{

        //    ////Set the serial number to text on the clipboard  Use control v
        //    if (vm.LenovoServerS1 != null)
        //    {
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS1.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS1.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);


        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS1);
        //    }



        //}



        //private void PrintBTNS2_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS2 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS2.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS2.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();



        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS2);
        //    }





        //}

        //private void PrintBTNS3_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS3 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS3.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS3.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS3);
        //    }





        //}

        //private void PrintBTNS4_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS4 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS4.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS4.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS4);
        //    }





        //}



        //private void PrintBTNS5_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS5 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS5.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS5.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS5);
        //    }





        //}

        //private void PrintBTNS6_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS6 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS6.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS6.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS6);
        //    }





        //}



        //private void PrintBTNS7_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS7 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS7.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS7.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS7);
        //    }





        //}


        //private void PrintBTNS8_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS8 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS8.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS8.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS8);
        //    }





        //}



        //private void PrintBTNS9_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS9 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS9.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS9.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS9);
        //    }





        //}


        //private void PrintBTNS10_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS10 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS10.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS10.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS10);
        //    }





        //}


        //private void PrintBTNS11_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS11 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS11.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS11.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS11);
        //    }





        //}


        //private void PrintBTNS12_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS12 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS12.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS12.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS12);
        //    }





        //}


        //private void PrintBTNS13_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS13 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS13.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS13.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS13);
        //    }





        //}


        //private void PrintBTNS14_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS14 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS14.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS14.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS14);
        //    }





        //}


        //private void PrintBTNS15_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS15 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS15.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS15.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS15);
        //    }





        //}


        //private void PrintBTNS16_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS16 != null)
        //    {
        //        ////Set the serial number to text on the clipboard  Use control v
        //        if (!string.IsNullOrEmpty(vm.LenovoServerS16.SystemSerialNumber))
        //            Clipboard.SetText(vm.LenovoServerS16.SystemSerialNumber.Trim() + "_Report", TextDataFormat.UnicodeText);

        //        //OLD METHOD

        //        DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();


        //        ////Get the text from the clipboard
        //        //Clipboard.GetText(TextDataFormat.UnicodeText);

        //        dpw.SaveDocPDF(LenovoServerReportS16);
        //    }





        //}







        //OLD METHOD



        //// DoThePrint(LenovoServerReportS1);


        //// return;

        // //Clone the source document
        // var str = XamlWriter.Save(LenovoServerReportS1);
        // var stringReader = new System.IO.StringReader(str);
        // var xmlReader = XmlReader.Create(stringReader);
        // //var AnotherCloneDoc = XamlReader.Load(xmlReader) as FlowDocument;
        // var CloneDoc = LenovoServerReportS1; //  Works but locks the control
        // //vm.FDS1 = LenovoServerReportS1;

        //// var CloneDoc = vm.FDS1;
        // //String copyString = XamlWriter.Save(FDViewerS1.Document);
        // //FlowDocument CloneDoc = XamlReader.Parse(copyString) as FlowDocument;


        // //CloneDoc.DataContext = vm.LenovoServerS1;
        // // get information about the dimensions of the seleted printer+media.
        // System.Printing.PrintDocumentImageableArea ia = null;

        // // passes in the printqueue CreateXPSDocumentWriter that calls upon a standar print dialog 
        // System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);

        // //MemoryStream stream = new MemoryStream();
        // //TextRange CloneDoc = new TextRange(LenovoServerReportS1.ContentStart, LenovoServerReportS1.ContentEnd);
        // //CloneDoc.Save(stream, DataFormats.Xaml);

        // //FlowDocument flowDocumentCopy = new FlowDocument();
        // //TextRange copyDocumentRange = new TextRange(flowDocumentCopy.ContentStart, flowDocumentCopy.ContentEnd);
        // //copyDocumentRange.Load(stream, DataFormats.Xaml);

        // if (docWriter != null && ia != null)
        // {
        //     DocumentPaginator paginator = ((IDocumentPaginatorSource)CloneDoc).DocumentPaginator;

        //     // Change the PageSize and PagePadding for the document to match the CanvasSize for the printer device.
        //     paginator.PageSize = new Size(ia.MediaSizeWidth, ia.MediaSizeHeight);
        //     Thickness t = new Thickness(72);  // copy.PagePadding;
        //     CloneDoc.PagePadding = new Thickness(
        //                      Math.Max(ia.OriginWidth, t.Left),
        //                        Math.Max(ia.OriginHeight, t.Top),
        //                        Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
        //                        Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));



        //     CloneDoc.ColumnWidth = double.PositiveInfinity;
        //     //copy.PageWidth = 528; // allow the page to be the natural with of the output device

        //     docWriter.Write(paginator);

        //     //FDViewerS1.Document = AnotherCloneDoc;

        // }



        // PrintPDFTest();

        // // dpw.PrintDocs(System.Printing.PageMediaSizeName.ISOA4);

        // //Get the current Default Printer
        // string DefPrinter = DocumentPaginatorWrapper.GetDefaultPrinter();

        // //MessageBox.Show(DocumentPaginatorWrapper.GetDefaultPrinter());

        ////Set Default temp to the Microsoft PDF Virtual printer
        //DocumentPaginatorWrapper.SetDefaultPrinterQueue("Microsoft Print to PDF");

        //// print to PDF
        // dpw.PrintFlowDoc(TestReport1);

        // //delay between switching of default printers
        // await PutTaskDelay(2000);


        // //MessageBox.Show(DocumentPaginatorWrapper.GetDefaultPrinter());

        // //Set default printer back to Original
        // DocumentPaginatorWrapper.SetDefaultPrinterQueue(DefPrinter);

        // //print again, this time to a printer
        // dpw.PrintFlowDoc(TestReport1);

        // // PrintPDFTest();



        //private void FlipView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput1 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server1IPv4) != true || vm.Server1IPv4 != "")
        //                {
        //                    //await CheckLCDDisplay(vm.Server1IPv4, vm.myDocs, vm.IsScriptRunningS1, vm.IsServerThere1, "S1");
        //                }
        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);
        //                //Load Ultima URL
        //                if (vm.LenovoServerS1 != null)
        //                {
        //                    if (vm.LenovoServerS1.Manufacturer != null && vm.LenovoServerS1.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS1.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS1.Model + " Server &go=Go";
        //                }


        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS1 != null)
        //                {
        //                    //Clear if already added
        //                    if (LenovoServerReportS1.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS1.Blocks.Remove(LenovoServerReportS1.Blocks.LastBlock);
        //                    }
        //                    vm.LenovoServerS1.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS1, vm.LenovoServerS1);
        //                    if (vm.LenovoServerS1 != null)
        //                    {
        //                        if (vm.LenovoServerS1.Manufacturer != null && vm.LenovoServerS1.Model != null)
        //                            vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS1.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS1.Model + " Server &go=Go";
        //                    }


        //                    //END CPU SECTION
        //                    //TextRange rangeOfWord = new TextRange(LenovoServerReportS1.ContentEnd, LenovoServerReportS1.ContentEnd);
        //                    //rangeOfWord.Text = "[CPU.Socket.1]";
        //                    //rangeOfWord.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.White);
        //                    //rangeOfWord.ApplyPropertyValue(TextElement.BackgroundProperty, Brushes.Green);

        //                    //TextRange rangeOfWord2 = new TextRange(LenovoServerReportS1.ContentEnd, LenovoServerReportS1.ContentEnd);
        //                    //rangeOfWord2.Text = "[CPU.Socket.2]";
        //                    //rangeOfWord2.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.White);
        //                    //rangeOfWord2.ApplyPropertyValue(TextElement.BackgroundProperty, Brushes.Green);

        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage1 = "";
        //                //vm.ILORestOutput1 = "";
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS1 != null)
        //                    ChassisNoTXTBS1.Text = vm.LenovoServerS1.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage1 = "";
        //                //vm.ILORestOutput1 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS1 != null)
        //                {
        //                    if (vm.LenovoServerS1.Manufacturer != null && vm.LenovoServerS1.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS1.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS1.Model + " Server &go=Go";
        //                }


        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage1 = "";
        //                vm.ILORestOutput1 = "";

        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}




        //private void FlipView2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput2 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server2IPv4) != true || vm.Server2IPv4 != "")
        //                {
        //                    //await CheckLCDDisplay(vm.Server2IPv4, vm.myDocs, vm.IsScriptRunningS2, vm.IsServerThere2, "S2");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);
        //                //Load Ultima URL
        //                if (vm.LenovoServerS2 != null)
        //                {
        //                    if (vm.LenovoServerS2.Manufacturer != null && vm.LenovoServerS2.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS2.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS2.Model + " Server &go=Go";
        //                }


        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS2 != null)
        //                {
        //                    if (LenovoServerReportS2.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS2.Blocks.Remove(LenovoServerReportS2.Blocks.LastBlock);
        //                    }
        //                    vm.LenovoServerS2.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS2, vm.LenovoServerS2);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage2 = "";
        //                //vm.ILORestOutput2 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS2 != null)
        //                {
        //                    if (vm.LenovoServerS2.Manufacturer != null && vm.LenovoServerS2.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS2.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS2.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS2 != null)
        //                    ChassisNoTXTBS2.Text = vm.LenovoServerS2.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage2 = "";
        //                //vm.ILORestOutput2 = "";
        //                if (vm.LenovoServerS2 != null)
        //                {
        //                    if (vm.LenovoServerS2.Manufacturer != null && vm.LenovoServerS2.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS2.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS2.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage2 = "";
        //                vm.ILORestOutput2 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}

        //private async void FlipView3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput3 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server3IPv4) != true || vm.Server3IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server3IPv4, vm.myDocs, vm.IsScriptRunningS3, vm.IsServerThere3, "S3");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);
        //                //Load Ultima URL
        //                if (vm.LenovoServerS3 != null)
        //                {
        //                    if (vm.LenovoServerS3.Manufacturer != null && vm.LenovoServerS3.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS3.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS3.Model + " Server &go=Go";
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS3 != null)
        //                {
        //                    if (LenovoServerReportS3.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS3.Blocks.Remove(LenovoServerReportS3.Blocks.LastBlock);
        //                    }
        //                    vm.LenovoServerS3.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS3, vm.LenovoServerS3);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage3 = "";
        //                //vm.ILORestOutput3 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS3 != null)
        //                {
        //                    if (vm.LenovoServerS3.Manufacturer != null && vm.LenovoServerS3.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS3.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS3.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS3 != null)
        //                    ChassisNoTXTBS3.Text = vm.LenovoServerS3.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage3 = "";
        //                //vm.ILORestOutput3 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS3 != null)
        //                {
        //                    if (vm.LenovoServerS3.Manufacturer != null && vm.LenovoServerS3.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS3.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS3.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage3 = "";
        //                vm.ILORestOutput3 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}

        //private async void FlipView4_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput4 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server4IPv4) != true || vm.Server4IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server4IPv4, vm.myDocs, vm.IsScriptRunningS4, vm.IsServerThere4, "S4");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS4 != null)
        //                {
        //                    if (vm.LenovoServerS4.Manufacturer != null && vm.LenovoServerS4.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS4.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS4.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS4 != null)
        //                {
        //                    if (LenovoServerReportS4.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS4.Blocks.Remove(LenovoServerReportS4.Blocks.LastBlock);
        //                    }
        //                    //vm.LenovoServerS4
        //                    vm.LenovoServerS4.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS4, vm.LenovoServerS4);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                vm.ProgressMessage4 = "";
        //                //vm.ILORestOutput4 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS4 != null)
        //                {
        //                    if (vm.LenovoServerS4.Manufacturer != null && vm.LenovoServerS4.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS4.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS4.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS4 != null)
        //                    ChassisNoTXTBS4.Text = vm.LenovoServerS4.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage4 = "";
        //                //vm.ILORestOutput4 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS4 != null)
        //                {
        //                    if (vm.LenovoServerS4.Manufacturer != null && vm.LenovoServerS4.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS4.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS4.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage4 = "";
        //                vm.ILORestOutput4 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}

        //private async void FlipView5_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {



        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server5IPv4) != true || vm.Server5IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server5IPv4, vm.myDocs, vm.IsScriptRunningS5, vm.IsServerThere5, "S5");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS5 != null)
        //                {
        //                    if (vm.LenovoServerS5.Manufacturer != null && vm.LenovoServerS5.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS5.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS5.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS5 != null)
        //                {
        //                    if (LenovoServerReportS5.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS5.Blocks.Remove(LenovoServerReportS5.Blocks.LastBlock);
        //                    }
        //                    //vm.LenovoServerS5
        //                    vm.LenovoServerS5.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS5, vm.LenovoServerS5);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                vm.ProgressMessage5 = "";
        //                //vm.ILORestOutput5 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS5 != null)
        //                {
        //                    if (vm.LenovoServerS5.Manufacturer != null && vm.LenovoServerS5.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS5.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS5.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS5 != null)
        //                    ChassisNoTXTBS5.Text = vm.LenovoServerS5.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage5 = "";
        //                //vm.ILORestOutput5 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS5 != null)
        //                {
        //                    if (vm.LenovoServerS5.Manufacturer != null && vm.LenovoServerS5.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS5.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS5.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage5 = "";
        //                vm.ILORestOutput5 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}

        //private async void FlipView6_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput6 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server6IPv4) != true || vm.Server6IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server6IPv4, vm.myDocs, vm.IsScriptRunningS6, vm.IsServerThere6, "S6");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS6 != null)
        //                {
        //                    if (vm.LenovoServerS6.Manufacturer != null && vm.LenovoServerS6.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS6.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS6.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS6 != null)
        //                {
        //                    if (LenovoServerReportS6.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS6.Blocks.Remove(LenovoServerReportS6.Blocks.LastBlock);
        //                    }
        //                    //vm.LenovoServerS6
        //                    vm.LenovoServerS6.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS6, vm.LenovoServerS6);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                vm.ProgressMessage6 = "";
        //                //vm.ILORestOutput6 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS6 != null)
        //                {
        //                    if (vm.LenovoServerS6.Manufacturer != null && vm.LenovoServerS6.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS6.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS6.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS6 != null)
        //                    ChassisNoTXTBS6.Text = vm.LenovoServerS6.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage6 = "";
        //                //vm.ILORestOutput6 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS6 != null)
        //                {
        //                    if (vm.LenovoServerS6.Manufacturer != null && vm.LenovoServerS6.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS6.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS6.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage6 = "";
        //                vm.ILORestOutput6 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}


        //private async void FlipView7_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput7 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server7IPv4) != true || vm.Server7IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server7IPv4, vm.myDocs, vm.IsScriptRunningS7, vm.IsServerThere7, "S7");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS7 != null)
        //                {
        //                    if (vm.LenovoServerS7.Manufacturer != null && vm.LenovoServerS7.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS7.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS7.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS7 != null)
        //                {
        //                    if (LenovoServerReportS7.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS7.Blocks.Remove(LenovoServerReportS7.Blocks.LastBlock);
        //                    }
        //                    //vm.LenovoServerS7
        //                    vm.LenovoServerS7.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS7, vm.LenovoServerS7);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                vm.ProgressMessage7 = "";
        //                //vm.ILORestOutput7 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS7 != null)
        //                {
        //                    if (vm.LenovoServerS7.Manufacturer != null && vm.LenovoServerS7.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS7.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS7.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS7 != null)
        //                    ChassisNoTXTBS7.Text = vm.LenovoServerS7.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage7 = "";
        //                //vm.ILORestOutput7 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS7 != null)
        //                {
        //                    if (vm.LenovoServerS7.Manufacturer != null && vm.LenovoServerS7.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS7.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS7.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage7 = "";
        //                vm.ILORestOutput7 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}


        //private async void FlipView8_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput8 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server8IPv4) != true || vm.Server8IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server8IPv4, vm.myDocs, vm.IsScriptRunningS8, vm.IsServerThere8, "S8");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS8 != null)
        //                {
        //                    if (vm.LenovoServerS8.Manufacturer != null && vm.LenovoServerS8.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS8.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS8.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS8 != null)
        //                {
        //                    if (LenovoServerReportS8.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS8.Blocks.Remove(LenovoServerReportS8.Blocks.LastBlock);
        //                    }
        //                    //vm.LenovoServerS8
        //                    vm.LenovoServerS8.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS8, vm.LenovoServerS8);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                vm.ProgressMessage8 = "";
        //                //vm.ILORestOutput8 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS8 != null)
        //                {
        //                    if (vm.LenovoServerS8.Manufacturer != null && vm.LenovoServerS8.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS8.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS8.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS8 != null)
        //                    ChassisNoTXTBS8.Text = vm.LenovoServerS8.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage8 = "";
        //                //vm.ILORestOutput8 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS8 != null)
        //                {
        //                    if (vm.LenovoServerS8.Manufacturer != null && vm.LenovoServerS8.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS8.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS8.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage8 = "";
        //                vm.ILORestOutput8 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}

        //private async void FlipView9_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput9 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server9IPv4) != true || vm.Server9IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server9IPv4, vm.myDocs, vm.IsScriptRunningS9, vm.IsServerThere9, "S9");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS9 != null)
        //                {
        //                    if (vm.LenovoServerS9.Manufacturer != null && vm.LenovoServerS9.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS9.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS9.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS9 != null)
        //                {
        //                    if (LenovoServerReportS9.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS9.Blocks.Remove(LenovoServerReportS9.Blocks.LastBlock);
        //                    }
        //                    //vm.LenovoServerS9
        //                    vm.LenovoServerS9.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS9, vm.LenovoServerS9);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                vm.ProgressMessage9 = "";
        //                //vm.ILORestOutput9 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS9 != null)
        //                {
        //                    if (vm.LenovoServerS9.Manufacturer != null && vm.LenovoServerS9.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS9.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS9.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS9 != null)
        //                    ChassisNoTXTBS9.Text = vm.LenovoServerS9.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage9 = "";
        //                //vm.ILORestOutput9 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS9 != null)
        //                {
        //                    if (vm.LenovoServerS9.Manufacturer != null && vm.LenovoServerS9.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS9.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS9.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage9 = "";
        //                vm.ILORestOutput9 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}

        //private async void FlipView10_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput10 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server10IPv4) != true || vm.Server10IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server10IPv4, vm.myDocs, vm.IsScriptRunningS10, vm.IsServerThere10, "S10");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS10 != null)
        //                {
        //                    if (vm.LenovoServerS10.Manufacturer != null && vm.LenovoServerS10.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS10.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS10.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS10 != null)
        //                {
        //                    if (LenovoServerReportS10.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS10.Blocks.Remove(LenovoServerReportS10.Blocks.LastBlock);
        //                    }
        //                    //vm.LenovoServerS10
        //                    vm.LenovoServerS10.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS10, vm.LenovoServerS10);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                vm.ProgressMessage10 = "";
        //                //vm.ILORestOutput10 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS10 != null)
        //                {
        //                    if (vm.LenovoServerS10.Manufacturer != null && vm.LenovoServerS10.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS10.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS10.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS10 != null)
        //                    ChassisNoTXTBS10.Text = vm.LenovoServerS10.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage10 = "";
        //                //vm.ILORestOutput10 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS10 != null)
        //                {
        //                    if (vm.LenovoServerS10.Manufacturer != null && vm.LenovoServerS10.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS10.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS10.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage10 = "";
        //                vm.ILORestOutput10 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}

        //private async void FlipView11_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput11 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server11IPv4) != true || vm.Server11IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server11IPv4, vm.myDocs, vm.IsScriptRunningS11, vm.IsServerThere11, "S11");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS11 != null)
        //                {
        //                    if (vm.LenovoServerS11.Manufacturer != null && vm.LenovoServerS11.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS11.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS11.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS11 != null)
        //                {
        //                    if (LenovoServerReportS11.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS11.Blocks.Remove(LenovoServerReportS11.Blocks.LastBlock);
        //                    }
        //                    //vm.LenovoServerS11
        //                    vm.LenovoServerS11.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS11, vm.LenovoServerS11);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                vm.ProgressMessage11 = "";
        //                //vm.ILORestOutput11 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS11 != null)
        //                {
        //                    if (vm.LenovoServerS11.Manufacturer != null && vm.LenovoServerS11.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS11.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS11.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS11 != null)
        //                    ChassisNoTXTBS11.Text = vm.LenovoServerS11.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage11 = "";
        //                //vm.ILORestOutput11 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS11 != null)
        //                {
        //                    if (vm.LenovoServerS11.Manufacturer != null && vm.LenovoServerS11.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS11.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS11.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage11 = "";
        //                vm.ILORestOutput11 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}



        //private async void FlipView12_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput12 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server12IPv4) != true || vm.Server12IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server12IPv4, vm.myDocs, vm.IsScriptRunningS12, vm.IsServerThere12, "S12");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS12 != null)
        //                {
        //                    if (vm.LenovoServerS12.Manufacturer != null && vm.LenovoServerS12.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS12.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS12.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS12 != null)
        //                {
        //                    if (LenovoServerReportS12.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS12.Blocks.Remove(LenovoServerReportS12.Blocks.LastBlock);
        //                    }
        //                    //vm.LenovoServerS12
        //                    vm.LenovoServerS12.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS12, vm.LenovoServerS12);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                vm.ProgressMessage12 = "";
        //                //vm.ILORestOutput12 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS12 != null)
        //                {
        //                    if (vm.LenovoServerS12.Manufacturer != null && vm.LenovoServerS12.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS12.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS12.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS12 != null)
        //                    ChassisNoTXTBS12.Text = vm.LenovoServerS12.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage12 = "";
        //                //vm.ILORestOutput12 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS12 != null)
        //                {
        //                    if (vm.LenovoServerS12.Manufacturer != null && vm.LenovoServerS12.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS12.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS12.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage12 = "";
        //                vm.ILORestOutput12 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}


        //private async void FlipView13_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput13 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server13IPv4) != true || vm.Server13IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server13IPv4, vm.myDocs, vm.IsScriptRunningS13, vm.IsServerThere13, "S13");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS13 != null)
        //                {
        //                    if (vm.LenovoServerS13.Manufacturer != null && vm.LenovoServerS13.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS13.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS13.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS13 != null)
        //                {
        //                    if (LenovoServerReportS13.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS13.Blocks.Remove(LenovoServerReportS13.Blocks.LastBlock);
        //                    }
        //                    //vm.LenovoServerS13
        //                    vm.LenovoServerS13.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS13, vm.LenovoServerS13);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                vm.ProgressMessage13 = "";
        //                //vm.ILORestOutput13 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS13 != null)
        //                {
        //                    if (vm.LenovoServerS13.Manufacturer != null && vm.LenovoServerS13.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS13.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS13.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS13 != null)
        //                    ChassisNoTXTBS13.Text = vm.LenovoServerS13.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage13 = "";
        //                //vm.ILORestOutput13 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS13 != null)
        //                {
        //                    if (vm.LenovoServerS13.Manufacturer != null && vm.LenovoServerS13.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS13.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS13.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage13 = "";
        //                vm.ILORestOutput13 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}



        //private async void FlipView14_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput14 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server14IPv4) != true || vm.Server14IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server14IPv4, vm.myDocs, vm.IsScriptRunningS14, vm.IsServerThere14, "S14");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS14 != null)
        //                {
        //                    if (vm.LenovoServerS14.Manufacturer != null && vm.LenovoServerS14.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS14.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS14.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS14 != null)
        //                {
        //                    if (LenovoServerReportS14.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS14.Blocks.Remove(LenovoServerReportS14.Blocks.LastBlock);
        //                    }
        //                    //vm.LenovoServerS14
        //                    vm.LenovoServerS14.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS14, vm.LenovoServerS14);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                vm.ProgressMessage14 = "";
        //                //vm.ILORestOutput14 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS14 != null)
        //                {
        //                    if (vm.LenovoServerS14.Manufacturer != null && vm.LenovoServerS14.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS14.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS14.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS14 != null)
        //                    ChassisNoTXTBS14.Text = vm.LenovoServerS14.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage14 = "";
        //                //vm.ILORestOutput14 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS14 != null)
        //                {
        //                    if (vm.LenovoServerS14.Manufacturer != null && vm.LenovoServerS14.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS14.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS14.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage14 = "";
        //                vm.ILORestOutput14 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}



        //private async void FlipView15_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput15 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server15IPv4) != true || vm.Server15IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server15IPv4, vm.myDocs, vm.IsScriptRunningS15, vm.IsServerThere15, "S15");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS15 != null)
        //                {
        //                    if (vm.LenovoServerS15.Manufacturer != null && vm.LenovoServerS15.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS15.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS15.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS15 != null)
        //                {
        //                    if (LenovoServerReportS15.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS15.Blocks.Remove(LenovoServerReportS15.Blocks.LastBlock);
        //                    }
        //                    //vm.LenovoServerS15
        //                    vm.LenovoServerS15.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS15, vm.LenovoServerS15);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                vm.ProgressMessage15 = "";
        //                //vm.ILORestOutput15 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS15 != null)
        //                {
        //                    if (vm.LenovoServerS15.Manufacturer != null && vm.LenovoServerS15.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS15.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS15.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS15 != null)
        //                    ChassisNoTXTBS15.Text = vm.LenovoServerS15.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage15 = "";
        //                //vm.ILORestOutput15 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS15 != null)
        //                {
        //                    if (vm.LenovoServerS15.Manufacturer != null && vm.LenovoServerS15.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS15.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS15.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage15 = "";
        //                vm.ILORestOutput15 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}


        //private async void FlipView16_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var flipview = ((FlipView)sender);
        //    switch (flipview.SelectedIndex)
        //    {
        //        case 0:
        //            flipview.BannerText = "Lenovo Procedure";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }

        //            break;
        //        case 1:
        //            flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //        case 2:
        //            flipview.BannerText = "Enter Server IP & Run Scripts";
        //            if (vm != null)
        //            {

        //                //vm.ILORestOutput16 = "";

        //                //Runs a check every 20 seconds for LCD and Info
        //                if (string.IsNullOrWhiteSpace(vm.Server16IPv4) != true || vm.Server16IPv4 != "")
        //                {
        //                    await CheckLCDDisplay(vm.Server16IPv4, vm.myDocs, vm.IsScriptRunningS16, vm.IsServerThere16, "S16");
        //                }

        //                //await RefreshServerInfo(vm.Server1IPv4, vm.myDocs);

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS16 != null)
        //                {
        //                    if (vm.LenovoServerS16.Manufacturer != null && vm.LenovoServerS16.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS16.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS16.Model + " Server &go=Go";
        //                }

        //            }
        //            break;
        //        case 3:
        //            flipview.BannerText = "Reports";
        //            if (vm != null)
        //            {


        //                if (vm.LenovoServerS16 != null)
        //                {
        //                    if (LenovoServerReportS16.Blocks.Count > 1)
        //                    {
        //                        LenovoServerReportS16.Blocks.Remove(LenovoServerReportS16.Blocks.LastBlock);
        //                    }
        //                    //vm.LenovoServerS16
        //                    vm.LenovoServerS16.TestedBy = vm.CurrentUser;
        //                    vm.LenovoGenerateReport(LenovoServerReportS16, vm.LenovoServerS16);
        //                }

        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                vm.ProgressMessage16 = "";
        //                //vm.ILORestOutput16 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS16 != null)
        //                {
        //                    if (vm.LenovoServerS16.Manufacturer != null && vm.LenovoServerS16.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS16.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS16.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 4:
        //            flipview.BannerText = "Server Checklist";
        //            if (vm != null)
        //            {
        //                if (vm.LenovoServerS16 != null)
        //                    ChassisNoTXTBS16.Text = vm.LenovoServerS16.SystemSerialNumber;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage16 = "";
        //                //vm.ILORestOutput16 = "";
        //                //Load Ultima URL
        //                if (vm.LenovoServerS16 != null)
        //                {
        //                    if (vm.LenovoServerS16.Manufacturer != null && vm.LenovoServerS16.Model != null)
        //                        vm.UltimaURL = "ultima.techbuyer.com/index.php?search=" + vm.LenovoServerS16.Manufacturer.Replace("Inc.", "") + " " + vm.LenovoServerS16.Model + " Server &go=Go";
        //                }
        //            }
        //            break;
        //        case 5:
        //            flipview.BannerText = "Final Wipe Scripts";
        //            if (vm != null)
        //            {
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //                vm.ProgressMessage16 = "";
        //                vm.ILORestOutput16 = "";
        //            }
        //            break;
        //        case 6:
        //            flipview.BannerText = "Completion";

        //            if (vm != null)
        //            {
        //                //if (vm.Gen9ServerInfo1 != null)
        //                //   ChassisNoTXTBS1.Text = vm.Gen9ServerInfo1.ChassisSerial;
        //                //vm.ProgressVisibility = Visibility.Hidden;
        //                //vm.ProgressIsActive = false;
        //                //vm.ProgressPercentage = 0;
        //                //vm.ProgressMessage = "";
        //            }
        //            break;
        //    }
        //}


        //private void RefreshDataS1BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS1 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES1.Execute(new String[] { "ServerInfo", "Dell" });
        //        // MessageBox.Show("Dropped Through");
        //        vm.LenovoServerS1.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS1, vm.LenovoServerS1);
        //    }
        //}
        //private void RefreshDataS2BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS2 != null)
        //    {
        //        vm.RunRACDMScriptsCommandDellPES2.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS2.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS2, vm.LenovoServerS2);
        //    }
        //}
        //private void RefreshDataS3BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS3 != null)
        //    {
        //        vm.RunRACDMScriptsCommandDellPES3.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS3.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS3, vm.LenovoServerS3);
        //    }
        //}
        //private void RefreshDataS4BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS4 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES4.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS4.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS4, vm.LenovoServerS4);
        //    }
        //}

        //private void RefreshDataS5BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS5 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES5.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS5.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS5, vm.LenovoServerS5);
        //    }
        //}


        //private void RefreshDataS6BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS6 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES6.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS6.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS6, vm.LenovoServerS6);
        //    }
        //}


        //private void RefreshDataS7BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS7 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES7.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS7.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS7, vm.LenovoServerS7);
        //    }
        //}


        //private void RefreshDataS8BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS8 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES8.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS8.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS8, vm.LenovoServerS8);
        //    }
        //}


        //private void RefreshDataS9BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS9 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES9.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS9.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS9, vm.LenovoServerS9);
        //    }
        //}


        //private void RefreshDataS10BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS10 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES10.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS10.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS10, vm.LenovoServerS10);
        //    }
        //}


        //private void RefreshDataS11BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS11 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES11.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS11.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS11, vm.LenovoServerS11);
        //    }
        //}


        //private void RefreshDataS12BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS12 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES12.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS12.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS12, vm.LenovoServerS12);
        //    }
        //}


        //private void RefreshDataS13BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS13 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES13.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS13.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS13, vm.LenovoServerS13);
        //    }
        //}


        //private void RefreshDataS14BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS14 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES14.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS14.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS14, vm.LenovoServerS14);
        //    }
        //}


        //private void RefreshDataS15BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS15 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES15.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS15.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS15, vm.LenovoServerS15);
        //    }
        //}

        //private void RefreshDataS16BTN_Click(object sender, RoutedEventArgs e)
        //{
        //    if (vm.LenovoServerS16 != null)
        //    {

        //        vm.RunRACDMScriptsCommandDellPES16.Execute(new String[] { "ServerInfo", "Dell" });
        //        vm.LenovoServerS16.TestedBy = vm.CurrentUser;
        //        vm.LenovoGenerateReport(LenovoServerReportS16, vm.LenovoServerS16);
        //    }
        //}







        private async Task CheckLCDDisplay(string ServerIP, string myDocs, bool OtherScript, string ServerThere, string ServerNumber)
        {

            ProcessPiper pp = new ProcessPiper(vm);


            if (ServerIP != string.Empty)
                while (true)
                {

                    if (OtherScript != true)
                    {


                        if (ServerThere == "Yes")
                        {


                            switch (ServerNumber)
                            {
                                case "S1":
                                    //get LCD display
                                    //MessageBox.Show("Yep");
                                    await System.Threading.Tasks.Task.Run(() => pp.StartSensorInventoryRACDM1(new System.IO.FileInfo(@"" + myDocs + @"\HPTOOLS\Dell\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + ServerIP + @" -u root -p calvin --nocertwarn get System.LCD", @"" + myDocs + @"\HPTOOLS\Dell\SysMgt\iDRACTools\racadm"));
                                    break;
                               
                            }


                          
                        }

                    }

                    //MessageBox.Show("Fire");
                    await Task.Delay(TimeSpan.FromSeconds(20));
                }
        }


        private async Task RefreshServerInfo(string ServerIP, string myDocs)
        {
            ProcessPiper pp = new ProcessPiper(vm);

            while (true)
            {
                if (vm.IsServerThere1 == "Yes")
                {
                    if (vm.IsScriptRunningS1 != true)
                    {
                        //MessageBox.Show("Fire1");
                        //acquire the server inventory 


                        //await System.Threading.Tasks.Task.Run(() => pp.StartRACDMGeneric1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\Dell\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.Server1IPv4 + @" -u root -p calvin --nocertwarn hwinventory export -f " + vm.Server1IPv4 + @"Inventory.xml", @"" + vm.myDocs + @"\HPTOOLS\Dell\SysMgt\iDRACTools\racadm"));
                    }


                }

                //MessageBox.Show("Fire");
                await Task.Delay(TimeSpan.FromSeconds(80));
            }
        }




        private void SkipBTN1_Click(object sender, RoutedEventArgs e)
        {

            //reset flipview to start
            //FlipView1.SelectedIndex = 2;
        }

  

      

        private void CancelScriptS1_Click(object sender, RoutedEventArgs e)
        {
            vm.CancelScriptS1 = true;
        }

       

        static readonly Guid SID_SWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");


       

        private void CloseAppByPid(int processID)
        {
            try
            {
                Process proc = Process.GetProcessById(processID);
                if (proc.ProcessName.Contains("HPLOCONS"))
                {
                    proc.Kill();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }



        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

      

        [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("6d5140c1-7436-11ce-8034-00aa006009fa")]
        internal interface IServiceProvider
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            object QueryService(ref Guid guidService, ref Guid riid);
        }

        private void CPS2_GotFocus(object sender, RoutedEventArgs e)
        {
        
        }
    }



}


