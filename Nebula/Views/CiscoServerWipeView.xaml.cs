﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TBTestTools.ViewModels;
using TBTestTools.Properties;
using TBTestTools.Models;
using TBTestTools.Helpers;
using MahApps.Metro.Controls;

namespace TBTestTools.Views
{
    /// <summary>
    /// Interaction logic for CiscoServerWipeView.xaml
    /// </summary>
    public partial class CiscoServerWipeView : UserControl
    {
        public CiscoServerWipeView()
        {
            InitializeComponent();
        }

        private void FlipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var flipview = ((FlipView)sender);
            switch (flipview.SelectedIndex)
            {
                case 0:
                    flipview.BannerText = "Locate dip switches";
                    break;
                case 1:
                    flipview.BannerText = "Set Dip Switch 1 & 6 to the On Position";
                    break;
                case 2:
                    flipview.BannerText = "Power On The Server!";
                    break;
                case 3:
                    flipview.BannerText = "Reset The Network Settings To DHCP";
                    break;
            }
        }
    }
}
