﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.ViewModels;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for CiscoReportsView.xaml
    /// </summary>
    public partial class CiscoReportsView : UserControl
    {
        MainViewModel vm;    //

        public CiscoReportsView()
        {
            InitializeComponent();
        }

        public CiscoReportsView(MainViewModel passedviewmodel)
        {
            InitializeComponent();

            vm = passedviewmodel;

            this.DataContext = vm;



            //Show Goods In or Goods Out Report
            if (StaticFunctions.Department == "GoodsOutTech" || StaticFunctions.Department == "GoodsIn" || StaticFunctions.Department == "TestDept" || StaticFunctions.Department == "ITAD")
            {

                //Clear if already added
                if (CiscoServerReportGOTS1.Blocks.Count > 1)
                {
                    CiscoServerReportGOTS1.Blocks.Remove(CiscoServerReportGOTS1.Blocks.LastBlock);
                }
                vm.CiscoServerS1.TestedBy = vm.CurrentUser;


                FDViewerS1.Visibility = Visibility.Visible;
                vm.CiscoGenerateReport(CiscoServerReportGOTS1, vm.CiscoServerS1);

                //Set FlowDocument for printing Test Report
                vm.FlowDocToPrint = CiscoServerReportGOTS1;
            }


        }
    }
}
