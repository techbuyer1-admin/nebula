﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TesseractSharp.Core;
using TesseractSharp;
using TesseractSharp.Hocr;

using System.IO;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for OCRDetectionView.xaml
    /// </summary>
    public partial class OCRDetectionView : UserControl
    {
        public OCRDetectionView()
        {
            InitializeComponent();

            //https://github.com/shift-technology/TesseractSharp




            var input = @"C:\Users\n.myers\Documents\Tesseract\Dip.png";

            // Also works with a Bitmap !
            var ouput5 = input.Replace(".png", ".pdf");
            var bitmap = (System.Drawing.Bitmap)System.Drawing.Image.FromFile(input);
            using (var stream = Tesseract.ImageToPdf(bitmap))  //, languages: new[] { Language.English, Language.French }
            using (var writer = File.OpenWrite(ouput5))
            {
                stream.CopyTo(writer);
            }
        }
    }
}
