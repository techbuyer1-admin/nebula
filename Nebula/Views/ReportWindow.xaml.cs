﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Nebula.Helpers;
using Nebula.Models;
using Nebula.ViewModels;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for ReportWindow.xaml
    /// </summary>
    public partial class ReportWindow : Window
    {

        MainViewModel vm;

        public ReportWindow()
        {
            InitializeComponent();
          
        }


        public ReportWindow(ListView lv)
        {
            InitializeComponent();
            //read the reports
            ReportLV1.ItemsSource = lv.ItemsSource;
            //ReportLV2.ItemsSource = diagReport.testLogRecord;


            //wait until data populated
            //await PutTaskDelay(500);

            ReportLV1.Height = ReportLV1.ActualHeight;
            //ReportLV2.Height = ReportLV2.ActualHeight;

            // Send both documents to the print functions
            PrintHelper.ShowPrintPreview(PrintHelper.GetFixedDocument(ReportLV1, ReportLV2, new PrintDialog(), this));
        }


        public ReportWindow(List<string> files)
        {
            InitializeComponent();
            //read the reports
            ReadGen10Reports(files);
        }

        public ReportWindow(MainViewModel passedViewModel)
        {
            InitializeComponent();
            vm = passedViewModel;

          
            //map to list view  vm.Gen10Report1.testLogRecord
        }

        public async void ReadGen10Reports(List<string> files)
        {
            //Holds all properties info
            ObservableCollection<Gen10SummaryReport.diagOutputCategoryStructureProperty> obvSummary = new ObservableCollection<Gen10SummaryReport.diagOutputCategoryStructureProperty>();
            ObservableCollection<Gen10DiagnosticReport.diagOutput> obvDiagNostic = new ObservableCollection<Gen10DiagnosticReport.diagOutput>();
           
            //report data
            Gen10SummaryReport.diagOutput summaryReport = null;
            Gen10DiagnosticReport.diagOutput diagReport = null;

            JsonParser xmlTest = new JsonParser(vm);


            //loop through any files in the list
            foreach(var file in files)
            {
               
                //check if summary or diagnostic
                if (file.Contains("-hvt-survey.xml"))
                {
                   summaryReport = xmlTest.ReadGen10SummaryXMLReports(file);
                }
                else if (file.Contains("-testlog.xml"))
                {
                   diagReport = xmlTest.ReadGen10DiagnosticXMLReports(file);
                }
            }



            ////check if summary or diagnostic
            //if (file.Contains("-hvt-survey.xml"))
            //{
            //    summaryReport = xmlTest.ReadGen10SummaryXMLReports(@"" + vm.dsktop + @"\Gen10 Test Reports\2020-12-11-10_26_17-CZ3032YX98-hvt-survey.xml");
            //}
            //else if (file.Contains("-testlog.xml"))
            //{
            //    diagReport = xmlTest.ReadGen10DiagnosticXMLReports(@"" + vm.dsktop + @"\Gen10 Test Reports\2020-12-11-10_26_17-CZ3032YX98-testlog.xml");
            //}


            //diagOutputCategoryDeviceStructureStructureProperty

            foreach (var itm in summaryReport.category)
            {


                if (itm.caption == "Server Information")
                {
                    var strSI = itm.structure;

                    foreach (var propitm in strSI.property)
                    {
                        if (propitm.GetType().ToString().Contains("diagOutputCategoryStructureProperty"))
                        {
                            obvSummary.Add(propitm);
                        }
                        //MessageBox.Show(propitm.caption + " : " + propitm.name + " : " + propitm.value);
                    }

                }

                if (itm.caption == "Health")
                {
                    var strSI = itm.structure;

                    foreach (var propitm in strSI.property)
                    {
                        if (propitm.GetType().ToString().Contains("diagOutputCategoryStructureProperty"))
                        {
                            obvSummary.Add(propitm);
                        }
                        //MessageBox.Show(propitm.caption + " : " + propitm.name + " : " + propitm.value);
                    }

                }
                if (itm.caption == "Processor")
                {
                    var strSI = itm.device;

                    foreach (var propitm in strSI)
                    {
                        if (propitm.Items != null)
                            foreach (var inneritm in propitm.Items)
                            {

                                //MessageBox.Show(inneritm.caption + " : " + propitm.name + " : " + propitm.value);
                                //Gen10SummaryReport.diagOutputCategoryStructureProperty innerProp = new Gen10SummaryReport.diagOutputCategoryStructureProperty();

                                if (inneritm.GetType().ToString().Contains("diagOutputCategoryDeviceProperty"))
                                {

                                    Gen10SummaryReport.diagOutputCategoryStructureProperty innerProp = new Gen10SummaryReport.diagOutputCategoryStructureProperty();
                                    Gen10SummaryReport.diagOutputCategoryDeviceProperty castitm = (Gen10SummaryReport.diagOutputCategoryDeviceProperty)inneritm;

                                    //wont cast so create a new object and set props like for like
                                    innerProp.caption = castitm.caption;
                                    innerProp.name = castitm.name;
                                    innerProp.value = castitm.value;

                                    //add to main collection
                                    obvSummary.Add(innerProp);

                                }
                                
                             
                            }


                    }

                }
                if (itm.caption == "Memory")
                {
                    var strSI = itm.device;

                    foreach (var propitm in strSI)
                    {


                        foreach (var inneritm in propitm.Items)
                        {

                            if (inneritm.GetType().ToString().Contains("diagOutputCategoryDeviceProperty"))
                            {
                                Gen10SummaryReport.diagOutputCategoryStructureProperty innerProp = new Gen10SummaryReport.diagOutputCategoryStructureProperty();
                                Gen10SummaryReport.diagOutputCategoryDeviceProperty castitm = (Gen10SummaryReport.diagOutputCategoryDeviceProperty)inneritm;
                                //wont cast so create a new object and set props like for like
                                innerProp.caption = castitm.caption;
                                innerProp.name = castitm.name;
                                innerProp.value = castitm.value;

                                //add to main collection
                                obvSummary.Add(innerProp);
                            }


                        }


                    }



                }
                if (itm.caption == "Firmware")
                {
                    var strSI = itm.structure;

                    foreach (var propitm in strSI.property)
                    {
                        if (propitm.GetType().ToString().Contains("diagOutputCategoryStructureProperty"))
                        {
                            obvSummary.Add(propitm);
                        }
                        //MessageBox.Show(propitm.caption + " : " + propitm.name + " : " + propitm.value);
                    }

                }


            }


            ReportLV1.ItemsSource = obvSummary;
            ReportLV2.ItemsSource = diagReport.testLogRecord;


            //wait until data populated
            await PutTaskDelay(500);

            ReportLV1.Height = ReportLV1.ActualHeight;
            ReportLV2.Height = ReportLV2.ActualHeight;

            // Send both documents to the print functions
            PrintHelper.ShowPrintPreview(PrintHelper.GetFixedDocument(ReportLV1, ReportLV2, new PrintDialog(),this));


           //this.Close();

        }




        public void PrintFixedDocument()
        {
            //RESET SCROLL
            //ReportLV1.ScrollIntoView(ReportLV1.Items[0]);
            //ReportLV2.ScrollIntoView(ReportLV2.Items[0]);

            //ReportLV1.UpdateLayout();
            //ReportLV2.UpdateLayout();

            //ReportLV1.ScrollIntoView(ReportLV1.View);
            //ReportLV2.ScrollIntoView(ReportLV2.View);

            ResetListViewScroll(ReportLV1);
            ResetListViewScroll(ReportLV2);

            //VisualTreeHelperEx.FindDescendantByType<ScrollViewer>(ReportLV1)?.ScrollToTop();

            //ReportLV2.ScrollIntoView(ReportLV1.SelectedItem);

            ReportLV1.Height = ReportLV1.ActualHeight;
            ReportLV2.Height = ReportLV2.ActualHeight;

            // Send both documents to the print functions
            PrintHelper.ShowPrintPreview(PrintHelper.GetFixedDocument(ReportLV1,ReportLV2, new PrintDialog(),this));



        }


        public void ResetListViewScroll(ListView LV)
        {
            //int childrenCount = VisualTreeHelper.GetChildrenCount(LV);
            //if (childrenCount > 0)
            //{
            //    DependencyObject depobj = VisualTreeHelper.GetChild(LV, 0);
            //    if (depobj is Decorator)
            //    {
            //        Decorator border = depobj as Decorator;
            //        ScrollViewer scroll = border.Child as ScrollViewer;
            //        scroll.ScrollToTop();
            //        scroll.ScrollToLeftEnd();
            //    }
            //}

            //try
            //{
            //    Decorator border = VisualTreeHelper.GetChild(LV, 0) as Decorator;
            //    ScrollViewer scroll = border.Child as ScrollViewer;
            //    scroll.ScrollToTop();
            //}
            //catch (Exception) { }
        }


        //private void BackButton_Click(object sender, RoutedEventArgs e)
        //{



        //    PrintFixedDocument();
        //}

        private void PrintBTN_Click(object sender, RoutedEventArgs e)
        {
            PrintFixedDocument();
        }







        // List<TextBlock> blockList = new List<TextBlock>();








        // select printer and get printer settings
        //PrintDialog pd = new PrintDialog();
        //if (pd.ShowDialog() != true) return;
        //// create a document
        ////FixedDocument document = new FixedDocument();
        ////document.DocumentPaginator.PageSize = new Size(pd.PrintableAreaWidth, pd.PrintableAreaHeight);
        //// 1ST PAGE
        ////FixedPage page1 = new FixedPage();
        ////page1.Width = document.DocumentPaginator.PageSize.Width;
        ////page1.Height = document.DocumentPaginator.PageSize.Height;

        //StackPanel sp = new StackPanel();
        //sp.Orientation = Orientation.Vertical;
        //sp.HorizontalAlignment = HorizontalAlignment.Center;

        //ListView lv1 = new ListView();
        //lv1.Width = 1200;
        //lv1.Height = 800;
        //lv1.ItemsSource = report.testLogRecord;
        ////GridView gv1 = new GridView();
        ////gv1.Columns.Add(new GridViewColumn();
        ////lv1.View = 





        //for (int i = 0; i < report.testLogRecord.Count(); i++)
        //{


        //    TextBlock tb1 = new TextBlock();
        //    tb1.Text = report.testLogRecord[i].component;
        //    sp.Children.Add(tb1);
        //    TextBlock tb2 = new TextBlock();
        //    tb2.Text = report.testLogRecord[i].device;
        //    sp.Children.Add(tb2);
        //    TextBlock tb3 = new TextBlock();
        //    tb3.Text = report.testLogRecord[i].test;
        //    sp.Children.Add(tb3);
        //    TextBlock tb4 = new TextBlock();
        //    tb4.Text = report.testLogRecord[i].testCaption;
        //    sp.Children.Add(tb4);
        //    TextBlock tb5 = new TextBlock();
        //    tb5.Text = report.testLogRecord[i].testTime;
        //    sp.Children.Add(tb5);
        //    TextBlock tb6 = new TextBlock();
        //    tb6.Text = report.testLogRecord[i].passedCount.ToString();
        //    sp.Children.Add(tb6);
        //    TextBlock tb7 = new TextBlock();
        //    tb7.Text = report.testLogRecord[i].failedCount.ToString();
        //    sp.Children.Add(tb7);
        //    TextBlock tb8 = new TextBlock();
        //    tb8.Text = report.testLogRecord[i].testCompletion.completionTime;
        //    sp.Children.Add(tb8);
        //    TextBlock tb9 = new TextBlock();
        //    tb9.Text = report.testLogRecord[i].testCompletion.result;
        //    sp.Children.Add(tb9);
        //    TextBlock tb10 = new TextBlock();
        //    tb10.Text = report.testLogRecord[i].testCompletion.testTime;
        //    sp.Children.Add(tb10);

        //}
        // PrintHelper.ShowPrintPreview(PrintHelper.GetFixedDocument(ReportLV2, new PrintDialog()));

        //page1.Children.Add(sp);

        ////foreach (var txtb in Content)
        ////{
        ////    TextBlock newtb = txtb;
        ////    page1.Children.Add(newtb);
        ////}

        //// add some text to the page
        ////TextBlock page1Text = new TextBlock();
        ////page1Text.Text = "This is the first page";
        ////page1Text.FontSize = 40; // 30pt text
        ////page1Text.Margin = new Thickness(96); // 1 inch margin
        ////page1.Children.Add(page1Text);
        //// add the page to the document
        //PageContent page1Content = new PageContent();
        //((IAddChild)page1Content).AddChild(page1);
        //document.Pages.Add(page1Content);
        //// 2ND PAGE
        ////FixedPage page2 = new FixedPage();
        ////page2.Width = document.DocumentPaginator.PageSize.Width;
        ////page2.Height = document.DocumentPaginator.PageSize.Height;
        ////TextBlock page2Text = new TextBlock();
        ////page2Text.Text = "This is NOT the first page";
        ////page2Text.FontSize = 40;
        ////page2Text.Margin = new Thickness(96);
        ////page2.Children.Add(page2Text);
        ////// add the page to the document
        ////PageContent page2Content = new PageContent();
        ////((IAddChild)page2Content).AddChild(page2);
        ////document.Pages.Add(page2Content);
        //// and print
        //pd.PrintDocument(document.DocumentPaginator, "Fixed Document");


        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }


    }
}
