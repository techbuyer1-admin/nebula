﻿using Nebula.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for SyncServerUpdatesView.xaml
    /// </summary>
    public partial class SyncServerUpdatesView : UserControl
    {

        MainViewModel vm;
        DispatcherTimer timer = new DispatcherTimer();
        Stopwatch stopWatch = new Stopwatch();

        string currentTime = string.Empty;

        public SyncServerUpdatesView()
        {
            InitializeComponent();
            //Clear SyncLog
            vm.SyncLog = "";
        }


        public SyncServerUpdatesView(MainViewModel passedViewModel)
        {
            InitializeComponent();

            timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            timer.Tick += timer_Tick;


            vm = passedViewModel;

            vm.SyncTimeElapsed = "Time Elapsed";
            //Clear SyncLog
            vm.SyncLog = "";
        }

        void timer_Tick(object sender, EventArgs e)
        {
            //Console.WriteLine("Tick");
            if (stopWatch.IsRunning)
            {
                TimeSpan ts = stopWatch.Elapsed;
                currentTime = String.Format("{0:00}:{1:00}:{2:00}:{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                vm.SyncTimeElapsed = currentTime;


            }
            //Check if property set to Finished by vm.SyncDirectory method
            if (vm.SyncServerUpdatesMessage == "Finished")
            {
                //Start Stop Watch
                stopWatch.Stop();
                //Start dispatcher timer
                timer.Stop();
                //Reset Stop Watch
                stopWatch.Reset();
            }

        }



        private async void FindServersBTN_Click(object sender, RoutedEventArgs e)
        {

            //Start Stop Watch
            stopWatch.Start();
            //Start dispatcher timer
            timer.Start();
            //Set message to Started
            vm.SyncServerUpdatesMessage = "Running";

            //SyncServerUpdates();
            //Call SyncMethod Once finished it will set the vm.SyncServerUpdatesMessage to Finished
            await System.Threading.Tasks.Task.Run(() => SyncServerUpdates());


        }




        private async void SyncServerUpdates()
        {


            //MessageBox.Show(vm.CurrentUser);
            //Sync Button For George, Access for me for Testing
            if (vm.CurrentUser.Contains("GG"))
            {
                //MessageBox.Show(vm.CurrentUser);
                if (Directory.Exists(@"\\pinnacle.local\tech_resources\Server_Updates") && Directory.Exists(@"E:\APP UPDATES"))
                {
                    //Run the Directory Sync

                    //Local Test
                    //await System.Threading.Tasks.Task.Run(() => vm.SyncDirectory(@"" + vm.dsktop + @"\SyncTest\Source", @"" + vm.dsktop + @"\SyncTest\Destination"));
                    //Production

                    await System.Threading.Tasks.Task.Run(() => vm.SyncDirectory(@"E:\APP UPDATES", @"\\pinnacle.local\tech_resources\Server_Updates", false));
                    //await System.Threading.Tasks.Task.Run(() => vm.SyncDirectory(@"" + vm.dsktop + @"\Server Updates", @"" + @"\\pinnacle.local\tech_resources\Nebula\Server Updates"));




                }
                else
                {
                    MessageBox.Show("A Required Folder path does not exist! Please contact application support.");
                }

                //UpdatesSync(@"" + vm.dsktop + @"\SyncTest\Source", @"" + vm.dsktop + @"SyncTest\Destination");

            }

            //My path for testing
            if (vm.CurrentUser.Contains("NM"))
            {

                if (Directory.Exists(@"" + vm.dsktop + @"\SyncTest\Source") && Directory.Exists(@"" + vm.dsktop + @"\SyncTest\Destination"))
                {


                    //Local Test
                    await System.Threading.Tasks.Task.Run(() => vm.SyncDirectory(@"" + vm.dsktop + @"\SyncTest\Source", @"" + vm.dsktop + @"\SyncTest\Destination", false));
                    //Local Test To Network Target
                    //await System.Threading.Tasks.Task.Run(() => vm.SyncDirectory(@"" + vm.dsktop + @"\SyncTest\Source", @"\\pinnacle.local\tech_resources\Nebula\Test\Destination", false));
                    //Production
                    //await System.Threading.Tasks.Task.Run(() => vm.SyncDirectoryTest(@"E:\APP UPDATES", @"\\pinnacle.local\tech_resources\Server_Updates")); \\pinnacle.local\tech_resources\Nebula\Test\Destination


                }
                else
                {
                    MessageBox.Show("A Required Folder path does not exist! Please contact application support.");
                }


            }

        }

        private void OutputTxtB_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Anytime Text is added scroll to end
            OutputTxtB.ScrollToEnd();

        }
    }
}
