﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.ViewModels;
using Nebula.Properties;
using Nebula.Models;
using Nebula.Helpers;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Nebula.Commands;
using System.Net;
using System.Net.Security;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for ServerInventoryView.xaml
    /// </summary>
    public partial class ServerInventoryView : UserControl
    {
        //declare viewmodel
        MainViewModel vm;
        public ServerInventoryView(MainViewModel PassedViewModel)
        {
            InitializeComponent();
            //get the passed in viewmodel and assign to the local vm variable
            vm = PassedViewModel;


        }

        private void ReadSvrBTN_Click(object sender, RoutedEventArgs e)
        {
           vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
        }



        private void FlipView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var flipview = ((FlipView)sender);
            switch (flipview.SelectedIndex)
            {
                case 0:
                    flipview.BannerText = "HP Gen8 Procedure";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                    }

                    break;
                case 1:
                    flipview.BannerText = "Remove Server cover & Locate dip switches";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                    }
                    break;
                case 2:
                    flipview.BannerText = "Set Dip Switch 1 to the On Position";
                    if (vm != null)
                    {
                        //    vm.ProgressVisibility = Visibility.Hidden;
                        //    vm.ProgressIsActive = false;
                        //    vm.ProgressPercentage = 0;
                        //    vm.ProgressMessage = "";
                    }
                    break;
                case 3:
                    flipview.BannerText = "Press F8 During POST & Reset The Network Settings To DHCP";
                    if (vm != null)
                    {
                        //vm.ProgressVisibility = Visibility.Hidden;
                        //vm.ProgressIsActive = false;
                        //vm.ProgressPercentage = 0;
                        //vm.ProgressMessage = "";
                    }
                    break;
             
        
            }
        }



        private void GenCheck1_Click(object sender, RoutedEventArgs e)
        {

            //vm.DialogMessage("BB", "CCCC");

            //reset flipview to start
            FlipView1.SelectedIndex = 0;
            //delete server object
            vm.GenGenericServerInfo1 = null;
            //tab header set back to default
            vm.Server1TabHeader = "Server1";
            //clear server ip
            vm.Server1IPv4 = string.Empty;
        }

      
        private void SkipBTN1_Click(object sender, RoutedEventArgs e)
        {
            vm.DialogMessage("System Message", "Please select a brand from the first dropdown list");
            //reset flipview to start
            FlipView1.SelectedIndex = 4;
        }

     

      
    }
}



 