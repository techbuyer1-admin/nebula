﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.ViewModels;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for LenovoReportsView.xaml
    /// </summary>
    public partial class LenovoReportsView : UserControl
    {
        MainViewModel vm;    //

        public LenovoReportsView()
        {
            InitializeComponent();
        }

        public LenovoReportsView(MainViewModel passedviewmodel)
        {
            InitializeComponent();

            vm = passedviewmodel;

            this.DataContext = vm;





            //Show Goods In or Goods Out Report
            if (StaticFunctions.Department == "GoodsOutTech" || StaticFunctions.Department == "GoodsIn" || StaticFunctions.Department == "TestDept" || StaticFunctions.Department == "ITAD")
            {

                //Clear if already added
                if (LenovoServerReportGOTS1.Blocks.Count > 1)
                {
                    LenovoServerReportGOTS1.Blocks.Remove(LenovoServerReportGOTS1.Blocks.LastBlock);
                }
                vm.LenovoServerS1.TestedBy = vm.CurrentUser;

          
                FDViewerS1.Visibility = Visibility.Visible;
                vm.LenovoGenerateReport2(LenovoServerReportGOTS1, vm.LenovoServerS1);

                //Set FlowDocument for printing Test Report
                vm.FlowDocToPrint = LenovoServerReportGOTS1;
            }
        

        }
    }
}
