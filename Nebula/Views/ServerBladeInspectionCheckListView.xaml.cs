﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Paginators;
using Nebula.ViewModels;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for ServerBladeInspectionCheckListView.xaml
    /// </summary>
    public partial class ServerBladeInspectionCheckListView : UserControl
    {

         GoodsInOutViewModel vm;


        public ServerBladeInspectionCheckListView(GoodsInOutViewModel PassedViewModel)
        {
            InitializeComponent();
            //get the passed in viewmodel and assign to the local vm variable
            vm = PassedViewModel;
        }

        private void ToggleSwitch_Checked(object sender, RoutedEventArgs e)
        {
            //Display The Logo
            if (ChecklistLogo.Visibility == Visibility.Collapsed)
            {
                //Display The Logo
                ChecklistLogo.Visibility = Visibility.Visible;
            }
            
        
        
        }

        private void ToggleSwitch_Unchecked(object sender, RoutedEventArgs e)
        {
            if (ChecklistLogo.Visibility == Visibility.Visible)
            {
                //Display The Logo
                ChecklistLogo.Visibility = Visibility.Collapsed;
            }
        }

    private void ToggleSwitch_IsCheckedChanged(object sender, EventArgs e)
        {
          
         
        }

     




        ////MessageBox.Show(vm.ReportBody);

        //DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

        //    ////Set the serial number to text on the clipboard  Use control v
        //   if(! string.IsNullOrEmpty(vm.SerialNo))
        //   Clipboard.SetText(vm.SerialNo.Replace("Serial No:","").Trim(), TextDataFormat.UnicodeText);


        //    ////Get the text from the clipboard
        //    //Clipboard.GetText(TextDataFormat.UnicodeText);

        //    dpw.SaveDocAndPrint(TestReport1);
    }
}
