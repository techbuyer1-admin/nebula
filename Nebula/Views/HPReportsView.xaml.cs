﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.ViewModels;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for HPReportsView.xaml
    /// </summary>
    public partial class HPReportsView : UserControl
    {

        MainViewModel vm;    //

        public HPReportsView()
        {
            InitializeComponent();
        }

        public HPReportsView(MainViewModel passedviewmodel)
        {
            InitializeComponent();

            vm = passedviewmodel;

            this.DataContext = vm;

          



            //Show Goods In or Goods Out Report
            if (StaticFunctions.Department == "GoodsOutTech" || StaticFunctions.Department == "GoodsIn" || StaticFunctions.Department == "TestDept" || StaticFunctions.Department == "ITAD")
            {

                //HP Servers Launch the File Prompt 
                //GEN 8 GEN 9 GEN 10
                if (vm.GenGenericServerInfo1 != null)
                {
                    //HPServerReportGIS1.DataContext = vm.GenGenericServerInfo1;
                    HPServerReportGOTS1.DataContext = vm.GenGenericServerInfo1;
                    vm.GenGenericServerInfo1.TestedBy = vm.CurrentUser;
                    //vm.HPGenerateReport(HPServerReportGOTS1, vm.GenGenericServerInfo1);
                    vm.HPGenerateReport(HPServerReportGOTS1, vm.GenGenericServerInfo1);
                }
             
                //FDViewerS1.Visibility = Visibility.Collapsed;
                FDViewerS1.Visibility = Visibility.Visible;
              

                //Set FlowDocument for printing Test Report
                vm.FlowDocToPrint = HPServerReportGOTS1;

            }
           


        }


    }
}
