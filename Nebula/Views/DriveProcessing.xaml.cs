﻿using Microsoft.VisualBasic.FileIO;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Printing;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.Models;
using Nebula.Paginators;
using Nebula.Properties;
using Nebula.ViewModels;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for DriveProcessing.xaml
    /// </summary>
    public partial class DriveProcessing : UserControl
    {

        //MainViewModel Mainvm;
        DriveTestViewModel vm; // = new DriveTestViewModel();


        public DriveProcessing()
        {
            InitializeComponent();

          
        }

        public DriveProcessing(DriveTestViewModel PassedViewModel)
        {
            InitializeComponent();
            //Assign ViewModel
            vm = PassedViewModel;
           //Add the new vm to a specific region in the user control
            //DriveProcSP.DataContext = vm;

            DataContext = vm;
            //REQUIRED FOR INDIVIDUAL ITEM COUNTS AND ROLLING COUNTS
            //Subscribe items changed event to perform overall total when collection changes
            ((INotifyCollectionChanged)ImportLV.Items).CollectionChanged += vm.ImportLVHandler;
            ((INotifyCollectionChanged)ProcessedLV.Items).CollectionChanged += vm.ProcessedLVHandler;

           //SerialChangeTXT.

            //SerialScanTXTB.Focus();
            //////Focus on PO Textbox
            //PONumberTXTB.Focus();
        }
            

        private void MenuPopup_Opened(object sender, EventArgs e)
        {
            //Used to send focus to The Textbox required. Attatched property failed to change state
            if (vm.ShowVariousSection == Visibility.Visible)
            {

                PartCodeTXTB.Focus();
            }
            else
            {
                SerialScanTXTB.Focus();
            }
        }

        private void DriveTestUC_Loaded(object sender, RoutedEventArgs e)
        {
            ////Add Focus to Serial
            //SerialScanTXTB.Focus();
            //////Focus on PO Textbox
            PONumberTXTB.Focus();
            //vm.DPCapsLockONCommand.Execute("");

        }

        private void DownIndexBTN_Click(object sender, RoutedEventArgs e)
        {
           


            try
            {

           
            if (ImportLV.Items.Count != 0)
            {
            if (ImportLV.Items.Count -1  == ImportLV.SelectedIndex)
            {
                ImportLV.SelectedIndex = ImportLV.Items.Count - 1;
                vm.ImportLVSelectedItem.SerialsRemaining = ImportLV.SelectedIndex = -1;
            }
            else
            {
              
                    ImportLV.SelectedIndex = ImportLV.SelectedIndex + 1;
                    vm.ImportLVSelectedItem.SerialsRemaining = ImportLV.SelectedIndex = + 1;
            }

            }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        private void UpIndexBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {

          
            if (ImportLV.Items.Count != 0)
            {
                if(ImportLV.SelectedIndex == - 1)
                    {
                        ImportLV.SelectedIndex = 0;
                    }
                if (ImportLV.SelectedIndex == 0)
                {
                    ImportLV.SelectedIndex = 0;
                }
                else
                {

                    ImportLV.SelectedIndex = ImportLV.SelectedIndex - 1;

                }

            }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

        private void PrintMultiBTNS1_Click(object sender, RoutedEventArgs e)
        {
            PITabCtrl.SelectedIndex = 3;
            //load in barcode
            vm.UCView = new BarcodeListOnlyView(new MainViewModel(), vm.ScannedSerialCollection);
        }

        private void PITabCtrl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(PITabCtrl.SelectedIndex != 3)
            {
                vm.UCView = null;
            }
        }

        private void SavePOBTNS1_Click(object sender, RoutedEventArgs e)
        {
            // Create Netsuite Import CSV when committing to DB
            vm.DPExportCSVCommand.Execute(new Object[] { ImportLV, "NetsuiteImportCSV" });
        }

        private void AddExtrasBTN_Click(object sender, RoutedEventArgs e)
        {
           //vm.PartCode = "";
        }

        private void SerialScanTXTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Console.WriteLine(ProcessedLV.Items.Count.ToString());
            // scroll to last item
            if (ProcessedLV.Items.Count != 0)
            {
               // MessageBox.Show(lvValue.Items.Count.ToString());

              //  ProcessedLV.Items.
                ProcessedLV.ScrollIntoView(ProcessedLV.Items[ProcessedLV.Items.Count - 1]);
                //Select Item
                vm.ProcessedLVSelectedItem = (NetsuiteInventory)ProcessedLV.Items[ProcessedLV.Items.Count - 1];
            }
        }


        private void PassFailCount()
        {
            //Recount
            vm.PassCount = vm.TestedDriveCollection.Where(f => f.DriveResult == "PASS").Count().ToString();
            vm.FailCount = vm.TestedDriveCollection.Where(f => f.DriveResult == "FAIL").Count().ToString();
            vm.PendingCount = vm.TestedDriveCollection.Where(f => f.DriveResult == "PENDING").Count().ToString();
        }




        private void FaultCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //If Value selected is greater than
            if (FaultCombo.IsDropDownOpen)
            {
                //If Value selected is greater than
                if (vm.TestResultsLVSelectedItem != null)
                {


                    if (!string.IsNullOrEmpty(vm.TestResultsLVSelectedItem.DriveFailedReason))
                    {
                        //Loop to alter multi selected lines

                        if (vm.TestResultsLVSelectedItem.ReportPath.Contains("FAILURE"))
                        {

                            //Set back to PASS
                            foreach (NetsuiteInventory nsi in TestResultsLV.SelectedItems)
                            {

                                nsi.DriveFailedReason = vm.TestResultsLVSelectedItem.DriveFailedReason;
                                nsi.DriveFailedReasonSpecify = vm.TestResultsLVSelectedItem.DriveFailedReasonSpecify;
                                nsi.DriveResult = "FAIL";
                            }

                        }
                        else
                        {
                            //Set back to PASS
                            foreach (NetsuiteInventory nsi in TestResultsLV.SelectedItems)
                            {
                                nsi.DriveFailedReason = vm.TestResultsLVSelectedItem.DriveFailedReason;
                                nsi.DriveFailedReasonSpecify = vm.TestResultsLVSelectedItem.DriveFailedReasonSpecify;
                                nsi.DriveResult = "FAIL";
                            }

                        }


                        //Recount
                        PassFailCount();

                    }
                    else
                    {

                        foreach (NetsuiteInventory nsi in TestResultsLV.SelectedItems)
                        {
                            // Requiries LV SelectedItem Reason
                            if (string.IsNullOrEmpty(vm.TestResultsLVSelectedItem.DriveFailedReason))
                            {
                                nsi.DriveFailedReason = vm.TestResultsLVSelectedItem.DriveFailedReason;
                                SpecifyTXTB.Text = "";
                            }

                            // Any reports that are success, set status to pass
                            if (nsi.ReportPath.Contains("SUCCESS"))
                            {
                                nsi.DriveFailedReason = "";
                                nsi.DriveFailedReasonSpecify = "";
                                nsi.DriveResult = "PASS";
                            }

                            //Reset status back to PENDING if Failed Reson set to blank
                            if (string.IsNullOrWhiteSpace(nsi.DriveFailedReason))
                            {
                                //If reason blank, clear specify 
                                nsi.DriveFailedReasonSpecify = "";
                                //check if 
                                if (string.IsNullOrWhiteSpace(nsi.ReportPath) && string.IsNullOrWhiteSpace(nsi.DriveFailedReasonSpecify))
                                {
                                    nsi.DriveFailedReason = "";
                                    nsi.DriveFailedReasonSpecify = "";
                                    //nsi.TestedByOperative = "";
                                    nsi.DriveResult = "PENDING";
                                }

                            }


                        }

                        //Recount
                        PassFailCount();
                    }

                }
            }

           

        }


        private void SpecifyTXTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            //If Value selected is greater than
            if (vm.TestResultsLVSelectedItem != null)
            {




                if (string.IsNullOrEmpty(vm.TestResultsLVSelectedItem.DriveFailedReasonSpecify))
                {
                    //Loop to alter multi selected lines

                    //Set back to PASS
                    foreach (NetsuiteInventory nsi in TestResultsLV.SelectedItems)
                    {

                       // nsi.DriveFailedReason = vm.TestResultsLVSelectedItem.DriveFailedReason;
                        nsi.DriveFailedReasonSpecify = "";
                        //nsi.DriveResult = "FAIL";
                    }
                    //vm.TestResultsLVSelectedItem.DriveResult = "FAIL";
                    //Recount
                    PassFailCount();

                  

                }
                else
                {

                    foreach (NetsuiteInventory nsi in TestResultsLV.SelectedItems)
                    {

                        // nsi.DriveFailedReason = vm.TestResultsLVSelectedItem.DriveFailedReason;
                        nsi.DriveFailedReasonSpecify = vm.TestResultsLVSelectedItem.DriveFailedReasonSpecify;
                        //nsi.DriveResult = "FAIL";
                    }
                    //vm.TestResultsLVSelectedItem.DriveResult = "FAIL";
                    //Recount
                    PassFailCount();
                }
            }
        }

        private void MultiSelCB_Checked(object sender, RoutedEventArgs e)
        {
            vm.SelectMode = SelectionMode.Extended;
        }

        private void MultiSelCB_Unchecked(object sender, RoutedEventArgs e)
        {
            vm.SelectMode = SelectionMode.Single;
        }

        private void SerialChangeTXT_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Check if serial already present
            vm.SerialMessage2 = "";
            // NetsuiteInventory sItem = new NetsuiteInventory();
            if (vm.ScannedSerialCollection.Count > 0)
            {
                // List<string> duplicates;
                int duplicateSerialCount = 0;
                // List<string> duplicates;
                //int duplicateIndex = 0;
                foreach (NetsuiteInventory nis in vm.ScannedSerialCollection)
                {
                    ////Get index
                    //duplicateIndex += 1;

                    if (vm.ProcessedLVSelectedItem != null)
                    {
                        if (nis.SerialNumber == vm.ProcessedLVSelectedItem.SerialNumber)
                        {
                            
                            duplicateSerialCount += 1;
                         

                            if (duplicateSerialCount > 1)
                            {
                                if (ProcessedLV.Items.Count != 0)
                                {
                                    // MessageBox.Show(duplicateIndex.ToString());

                                    //  ProcessedLV.Items.
                                    //ProcessedLV.ScrollIntoView(ProcessedLV.Items[duplicateIndex]);
                                    //Select Item
                                    //vm.ProcessedLVSelectedItem = (NetsuiteInventory)ProcessedLV.Items[duplicateIndex];

                                    // vm.ProcessedLVSelectedItem = nis;

                                    //ProcessedLV.SelectedItem = nis;

                                    // ProcessedLV.SelectedIndex = duplicateIndex;




                                }


                                // vm.ProcessedLVSelectedItem.SerialNumber = vm.ProcessedLVSelectedItem.SerialNumber;
                                // MessageBox.Show("Duplicate Serial Detected, Please check the collection");
                                vm.SerialMessage2 = "Serial Number already present in the PO!";


                                break;

                            }
                    }

                }

              
                }
            }

        }
        //Checkboxes in Import Inventory Collection
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            //Increase by 1
            vm.PartCodesTotal += 1;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {

            if(vm.PartCodesTotal == 0)
            {

            }
            else
            {
                //Decrease by 1
                vm.PartCodesTotal -= 1;
            }
         
        }

       
    }
}



