﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.ViewModels;
using Nebula.Properties;
using Nebula.Models;
using Nebula.Helpers;
using MahApps.Metro.Controls;
using Nebula.Commands;
using System.IO;
using Renci.SshNet;
using Renci.SshNet.Common;
using System.Collections.ObjectModel;



namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for HPEnclosureView.xaml
    /// </summary>
    public partial class DELLEnclosureView : UserControl
    {
        //List<BladeItem> ipList = new List<BladeItem>();


        //declare viewmodel
        MainViewModel vm;     //
        public DELLEnclosureView()
        {
            InitializeComponent();
        }


        public DELLEnclosureView(MainViewModel PassedViewModel)
        {
            InitializeComponent();
            //get the passed in viewmodel and assign to the local vm variable
            vm = PassedViewModel;

           
            //new the backing properties
            vm.BladeipList = new ObservableCollection<BladeItem>();
            vm.SelectedBladeipList = new ObservableCollection<BladeItem>();
            vm.ILORestOutputEnclosure = "";

            //IF IP PRESENT RUN THE CHECK
            if (string.IsNullOrWhiteSpace(vm.DELLEnclosureIP))
            {

            }
            else
            {
                //vm.BladeipList.Add(new BladeItem { ServerIP = "192.168.101.115" });
                //vm.BladeipList.Add(new BladeItem { ServerIP = "192.168.101.116" });
                //vm.BladeipList.Add(new BladeItem { ServerIP = "192.168.101.117" });
                //Call checkenclosure method
                CheckEnclosure();
            }


            //vm.BladeipList.Add(new BladeItem { Bay = "1", ServerIP = "192.168.101.118" });
        }



        // public





        private void LoadServersBTN_Click(object sender, RoutedEventArgs e)
        {

            //MessageBox.Show(vm.ServerType);
            //Get all selected items from the list view
            //vm.ServerType = "DellBlade";

            //Get all selected items from the list view
            var selectedItems = EnclosureBaysLV.SelectedItems;


            foreach (BladeItem selectedItem in selectedItems)
            {
                // MessageBox.Show(selectedItem.ServerIP); ;
                vm.SelectedBladeipList.Add(selectedItem);

                //add
                if (vm.SelectedBladeipList.Count() > 0)
                {
                   //MessageBox.Show(vm.Server1IPv4);

                   // MessageBox.Show(vm.Server2IPv4);
                    //check if slot is free
                    if (vm.Server1IPv4 == "")
                    {
                         

                       vm.Server1IPv4 = selectedItem.ServerIP;

                       vm.Server1TabHeader = "Server1 " + selectedItem.Bay;
                        //MessageBox.Show("Before " + vm.Server1TabHeader);
                        vm.CurrentView1 = null;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if(vm.CurrentView1 == null)
                            vm.CurrentView1 = new DellServerView(vm, "1", vm.Server1IPv4, true, "Server1 " + selectedItem.Bay);
                            //vm.CurrentView1 = new DellServerViewGOT(vm, "1", vm.Server1IPv4, true, "Server1 " + selectedItem.Bay);
                        }
                        else
                        {
                           // MessageBox.Show(vm.Server1IPv4 + "  " + "Server1 " + selectedItem.Bay);
                            //Goods In
                            //if(vm.CurrentView1 == null)
                            vm.CurrentView1 = new DellServerView(vm, "1", vm.Server1IPv4, true, "Server1 " + selectedItem.Bay);
                        }

                        //MessageBox.Show("After " + vm.Server1TabHeader);

                    }
                    else if (vm.Server2IPv4 == "")
                    {
                      // MessageBox.Show("Empty Slot 2");
                        vm.Server2IPv4 = selectedItem.ServerIP;
                        //Set Tab
                        vm.Server2TabHeader = "Server2 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView2 == null)
                            vm.CurrentView2 = new DellServerView(vm, "2", vm.Server2IPv4, true, "Server2 " + selectedItem.Bay);
                            //vm.CurrentView2 = new DellServerViewGOT(vm, "2", vm.Server2IPv4, true, "Server2 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                          //if(vm.CurrentView2 == null)
                            vm.CurrentView2 = new DellServerView(vm, "2", vm.Server2IPv4, true, "Server2 " + selectedItem.Bay);
                        }
                     

                    }
                    else if (vm.Server3IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 3");
                        vm.Server3IPv4 = selectedItem.ServerIP;
                        vm.Server3TabHeader = "Server3 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            // if (vm.CurrentView3 == null)
                            vm.CurrentView3 = new DellServerView(vm, "3", vm.Server3IPv4, true, "Server3 " + selectedItem.Bay);
                            //vm.CurrentView3 = new DellServerViewGOT(vm, "3", vm.Server3IPv4, true, "Server3 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                            //if(vm.CurrentView3 == null)
                            vm.CurrentView3 = new DellServerView(vm, "3", vm.Server3IPv4, true, "Server3 " + selectedItem.Bay);
                        }

                    }
                    else if (vm.Server4IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 4");
                        vm.Server4IPv4 = selectedItem.ServerIP;
                        vm.Server4TabHeader = "Server4 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView4 == null)
                            vm.CurrentView4 = new DellServerView(vm, "4", vm.Server4IPv4, true, "Server4 " + selectedItem.Bay);
                            //vm.CurrentView4 = new DellServerViewGOT(vm, "4", vm.Server4IPv4, true, "Server4 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                           // if (vm.CurrentView4 == null)
                                vm.CurrentView4 = new DellServerView(vm, "4", vm.Server4IPv4, true, "Server4 " + selectedItem.Bay);
                        }

                    }
                    else if (vm.Server5IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 5");
                        vm.Server5IPv4 = selectedItem.ServerIP;
                        vm.Server5TabHeader = "Server5 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView5 == null)
                            vm.CurrentView5 = new DellServerView(vm, "5", vm.Server5IPv4, true, "Server5 " + selectedItem.Bay);
                            //vm.CurrentView5 = new DellServerViewGOT(vm, "5", vm.Server5IPv4, true, "Server5 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                            //if (vm.CurrentView5 == null)
                                vm.CurrentView5 = new DellServerView(vm, "5", vm.Server5IPv4, true, "Server5 " + selectedItem.Bay);
                        }

                    }
                    else if (vm.Server6IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 6");
                        vm.Server6IPv4 = selectedItem.ServerIP;
                        vm.Server6TabHeader = "Server6 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView6 == null)
                            vm.CurrentView6 = new DellServerView(vm, "6", vm.Server6IPv4, true, "Server6 " + selectedItem.Bay);
                            //vm.CurrentView6 = new DellServerViewGOT(vm, "6", vm.Server6IPv4, true, "Server6 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                            //if (vm.CurrentView6 == null)
                                vm.CurrentView6 = new DellServerView(vm, "6", vm.Server6IPv4, true, "Server6 " + selectedItem.Bay);
                        }

                    }
                    else if (vm.Server7IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 7");
                        vm.Server7IPv4 = selectedItem.ServerIP;
                        vm.Server7TabHeader = "Server7 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView7 == null)
                            vm.CurrentView7 = new DellServerView(vm, "7", vm.Server7IPv4, true, "Server7 " + selectedItem.Bay);
                            //vm.CurrentView7 = new DellServerViewGOT(vm, "7", vm.Server7IPv4, true, "Server7 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                            //if (vm.CurrentView7 == null)
                                vm.CurrentView7 = new DellServerView(vm, "7", vm.Server7IPv4, true, "Server7 " + selectedItem.Bay);
                        }

                    }
                    else if (vm.Server8IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 8");
                        vm.Server8IPv4 = selectedItem.ServerIP;
                        vm.Server8TabHeader = "Server8 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView8 == null)
                            vm.CurrentView8 = new DellServerView(vm, "8", vm.Server8IPv4, true, "Server8 " + selectedItem.Bay);
                            //vm.CurrentView8 = new DellServerViewGOT(vm, "8", vm.Server8IPv4, true, "Server8 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                            //if (vm.CurrentView8 == null)
                                vm.CurrentView8 = new DellServerView(vm, "8", vm.Server8IPv4, true, "Server8 " + selectedItem.Bay);
                        }

                    }
                    else if (vm.Server9IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 9");
                        vm.Server9IPv4 = selectedItem.ServerIP;
                        vm.Server9TabHeader = "Server9 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView9 == null)
                            vm.CurrentView9 = new DellServerView(vm, "9", vm.Server9IPv4, true, "Server9 " + selectedItem.Bay);
                            //vm.CurrentView9 = new DellServerViewGOT(vm, "9", vm.Server9IPv4, true, "Server9 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                            //if (vm.CurrentView9 == null)
                                vm.CurrentView9 = new DellServerView(vm, "9", vm.Server9IPv4, true, "Server9 " + selectedItem.Bay);
                        }

                    }
                    else if (vm.Server10IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 10");
                        vm.Server10IPv4 = selectedItem.ServerIP;
                        vm.Server10TabHeader = "Server10 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView10 == null)
                            vm.CurrentView10 = new DellServerView(vm, "10", vm.Server10IPv4, true, "Server10 " + selectedItem.Bay);
                            //vm.CurrentView10 = new DellServerViewGOT(vm, "10", vm.Server10IPv4, true, "Server10 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                            //if (vm.CurrentView10 == null)
                                vm.CurrentView10 = new DellServerView(vm, "10", vm.Server10IPv4, true, "Server10 " + selectedItem.Bay);
                        }

                    }
                    else if (vm.Server11IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 11");
                        vm.Server11IPv4 = selectedItem.ServerIP;
                        vm.Server11TabHeader = "Server11 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView11 == null)
                            vm.CurrentView11 = new DellServerView(vm, "11", vm.Server11IPv4, true, "Server11 " + selectedItem.Bay);
                            //vm.CurrentView11 = new DellServerViewGOT(vm, "11", vm.Server11IPv4, true, "Server11 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                            //if (vm.CurrentView11 == null)
                                vm.CurrentView11 = new DellServerView(vm, "11", vm.Server11IPv4, true, "Server11 " + selectedItem.Bay);
                        }

                    }
                    else if (vm.Server12IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 12");
                        vm.Server12IPv4 = selectedItem.ServerIP;
                        vm.Server12TabHeader = "Server12 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView12 == null)
                            vm.CurrentView12 = new DellServerView(vm, "12", vm.Server12IPv4, true, "Server12 " + selectedItem.Bay);
                            //vm.CurrentView12 = new DellServerViewGOT(vm, "12", vm.Server12IPv4, true, "Server12 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                            //if (vm.CurrentView12 == null)
                                vm.CurrentView12 = new DellServerView(vm, "12", vm.Server12IPv4, true, "Server12 " + selectedItem.Bay);
                        }

                    }
                    else if (vm.Server13IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 13");
                        vm.Server13IPv4 = selectedItem.ServerIP;
                        vm.Server13TabHeader = "Server13 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView13 == null)
                            vm.CurrentView13 = new DellServerView(vm, "13", vm.Server13IPv4, true, "Server13 " + selectedItem.Bay);
                            //vm.CurrentView13 = new DellServerViewGOT(vm, "13", vm.Server13IPv4, true, "Server13 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                            //if (vm.CurrentView13 == null)
                                vm.CurrentView13 = new DellServerView(vm, "13", vm.Server13IPv4, true, "Server13 " + selectedItem.Bay);
                        }

                    }
                    else if (vm.Server14IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 14");
                        vm.Server14IPv4 = selectedItem.ServerIP;
                        vm.Server14TabHeader = "Server14 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView14 == null)
                            vm.CurrentView14 = new DellServerView(vm, "14", vm.Server14IPv4, true, "Server14 " + selectedItem.Bay);
                            //vm.CurrentView14 = new DellServerViewGOT(vm, "14", vm.Server14IPv4, true, "Server14 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                            //if (vm.CurrentView14 == null)
                                vm.CurrentView14 = new DellServerView(vm, "14", vm.Server14IPv4, true, "Server14 " + selectedItem.Bay);
                        }

                    }
                    else if (vm.Server15IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 15");
                        vm.Server15IPv4 = selectedItem.ServerIP;
                        vm.Server15TabHeader = "Server15 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView15 == null)
                            vm.CurrentView15 = new DellServerView(vm, "15", vm.Server15IPv4, true, "Server15 " + selectedItem.Bay);
                            //vm.CurrentView15 = new DellServerViewGOT(vm, "15", vm.Server15IPv4, true, "Server15 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                            //if (vm.CurrentView15 == null)
                                vm.CurrentView15 = new DellServerView(vm, "15", vm.Server15IPv4, true, "Server15 " + selectedItem.Bay);
                        }

                    }
                    else if (vm.Server16IPv4 == "")
                    {
                        //  MessageBox.Show("Empty Slot 16");
                        vm.Server16IPv4 = selectedItem.ServerIP;
                        vm.Server16TabHeader = "Server16 " + selectedItem.Bay;
                        //Check if Goods in or Goods Out Needs Loading
                        if (StaticFunctions.Department == "GoodsOutTech")
                        {
                            //if (vm.CurrentView16 == null)
                            vm.CurrentView16 = new DellServerView(vm, "16", vm.Server16IPv4, true, "Server16 " + selectedItem.Bay);
                            //vm.CurrentView16 = new DellServerViewGOT(vm, "16", vm.Server16IPv4, true, "Server16 " + selectedItem.Bay);
                        }
                        else
                        {
                            //Goods In
                            //if (vm.CurrentView16 == null)
                                vm.CurrentView16 = new DellServerView(vm, "16", vm.Server16IPv4, true, "Server16 " + selectedItem.Bay);
                        }

                    }
                }

            }
           



            //vm.FlyOutContent2 = new DELLEnclosureView(vm);
           
            
            //Server1
            //if (vm.Server1IPv4 != string.Empty)
            //{
            //    vm.FlipView1Index = 2;
            //    if (vm.DellServerS1 == null)
            //        vm.RunRACDMScriptsCommandDellPES1.Execute(new String[] { "ServerInfo", "Dell" });

            //}

            ////Server2
            //if (vm.Server2IPv4 != string.Empty)
            //{
            //    vm.FlipView2Index = 2;
            //    if (vm.DellServerS2 == null)
            //        vm.RunRACDMScriptsCommandDellPES2.Execute(new String[] { "ServerInfo", "Dell" });

            //}


            ////Server3
            //if (vm.Server3IPv4 != string.Empty)
            //{
            //    vm.FlipView3Index = 2;
            //    if (vm.DellServerS3 == null)
            //        vm.RunRACDMScriptsCommandDellPES3.Execute(new String[] { "ServerInfo", "Dell" });

            //}


            ////Server4
            //if (vm.Server4IPv4 != string.Empty)
            //{
            //    vm.FlipView4Index = 2;
            //    if (vm.DellServerS4 == null)
            //        vm.RunRACDMScriptsCommandDellPES4.Execute(new String[] { "ServerInfo", "Dell" });

            //}


            ////Server5
            //if (vm.Server5IPv4 != string.Empty)
            //{
            //    vm.FlipView5Index = 2;
            //    if (vm.DellServerS5 == null)
            //        vm.RunRACDMScriptsCommandDellPES5.Execute(new String[] { "ServerInfo", "Dell" });

            //}


            ////Server6
            //if (vm.Server6IPv4 != string.Empty)
            //{
            //    vm.FlipView6Index = 2;
            //    if (vm.DellServerS6 == null)
            //        vm.RunRACDMScriptsCommandDellPES6.Execute(new String[] { "ServerInfo", "Dell" });

            //}


            ////Server7
            //if (vm.Server7IPv4 != string.Empty)
            //{
            //    vm.FlipView7Index = 2;
            //    if (vm.DellServerS7 == null)
            //        vm.RunRACDMScriptsCommandDellPES7.Execute(new String[] { "ServerInfo", "Dell" });

            //}


            ////Server8
            //if (vm.Server8IPv4 != string.Empty)
            //{
            //    vm.FlipView8Index = 2;
            //    if (vm.DellServerS8 == null)
            //        vm.RunRACDMScriptsCommandDellPES8.Execute(new String[] { "ServerInfo", "Dell" });

            //}

            ////Server9
            //if (vm.Server9IPv4 != string.Empty)
            //{
            //    vm.FlipView9Index = 2;
            //    if (vm.DellServerS9 == null)
            //        vm.RunRACDMScriptsCommandDellPES9.Execute(new String[] { "ServerInfo", "Dell" });

            //}


            ////Server10
            //if (vm.Server10IPv4 != string.Empty)
            //{
            //    vm.FlipView10Index = 2;
            //    if (vm.DellServerS10 == null)
            //        vm.RunRACDMScriptsCommandDellPES10.Execute(new String[] { "ServerInfo", "Dell" });

            //}


            ////Server11
            //if (vm.Server11IPv4 != string.Empty)
            //{
            //    vm.FlipView11Index = 2;
            //    if (vm.DellServerS11 == null)
            //        vm.RunRACDMScriptsCommandDellPES11.Execute(new String[] { "ServerInfo", "Dell" });

            //}

            ////Server12
            //if (vm.Server12IPv4 != string.Empty)
            //{
            //    vm.FlipView12Index = 2;
            //    if (vm.DellServerS12 == null)
            //        vm.RunRACDMScriptsCommandDellPES12.Execute(new String[] { "ServerInfo", "Dell" });

            //}

            ////Server13
            //if (vm.Server13IPv4 != string.Empty)
            //{
            //    vm.FlipView13Index = 2;
            //    if (vm.DellServerS13 == null)
            //        vm.RunRACDMScriptsCommandDellPES13.Execute(new String[] { "ServerInfo", "Dell" });

            //}

            ////Server14
            //if (vm.Server14IPv4 != string.Empty)
            //{
            //    vm.FlipView14Index = 2;
            //    if (vm.DellServerS14 == null)
            //        vm.RunRACDMScriptsCommandDellPES14.Execute(new String[] { "ServerInfo", "Dell" });

            //}


            ////Server15
            //if (vm.Server15IPv4 != string.Empty)
            //{
            //    vm.FlipView15Index = 2;
            //    if (vm.DellServerS15 == null)
            //        vm.RunRACDMScriptsCommandDellPES15.Execute(new String[] { "ServerInfo", "Dell" });

            //}


            ////Server16
            //if (vm.Server16IPv4 != string.Empty)
            //{
            //    vm.FlipView16Index = 2;
            //    if (vm.DellServerS16 == null)
            //        vm.RunRACDMScriptsCommandDellPES16.Execute(new String[] { "ServerInfo", "Dell" });

            //}




            vm.ShowDELLFlyout = false;


        }


        //public void RunGetServerInfo()
        //{
        //    switch (vm.ServerType)
        //    {
        //        case "HPGen8Blade":
        //            vm.RunRestfulScriptsCommandGenGeneric.Execute(new String[] { "ServerInfo", "Gen8" });
        //            break;
        //        case "HPGen9Blade":
        //            vm.RunRestfulScriptsCommandGenGeneric.Execute(new String[] { "ServerInfo", "Gen9" });
        //            break;
        //        case "HPGen10Blade":
        //            vm.RunRestfulScriptsCommandGenGeneric.Execute(new String[] { "ServerInfo", "Gen9" });
        //            break;
        //    }
        //}



        private void EnclosureBaysLV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void EnclosureBaysLV_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void FindServersBTN_Click(object sender, RoutedEventArgs e)
        {
            //Clear items before check
            vm.BladeipList.Clear();
            vm.SelectedBladeipList.Clear();

            //Call checkenclosure method
            CheckEnclosure();
        }





        //public string SSHCommand(string commandString)
        //{
        //    string output = "";

        //    try
        //    {


        //        //Set up the SSH connection
        //        using (var client = new SshClient(vm.HPEnclosureIP, "Administrator", "password"))
        //        {

        //            //Accept Host key
        //            client.HostKeyReceived += delegate (object sender, HostKeyEventArgs e)
        //            {
        //                e.CanTrust = true;
        //            };

        //            //Start the connection  var output = client.RunCommand("show device details").Result;
        //            client.Connect();

        //            //output = client.RunCommand("SHOW EBIPA").Result;
        //            //output = client.RunCommand("SHOW SERVER LIST").Result;
        //            output = client.RunCommand(commandString).Result;

        //            client.Disconnect();

        //            return output;
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //        Console.WriteLine(ex.Message);
        //        return output;
        //    }

        //}


        public async void CheckEnclosure()
        {

            try
            {


                string aLine = "";
                string BladePresent = "";
                string whichbay = "";
                List<string> whichbaycollection = new List<string>();
                vm.DELLBladeStatus = "";
                vm.DELLBladeIPInfo = "";
                vm.BladeipList.Clear();
                // string WhichBay = "";
                int EqualsPos = 0;
                BladeItem blade;

                ProcessPiper pp = new ProcessPiper(vm);


                ProgressBarDELL.IsIndeterminate = true;



                //Get the status of the blade
                await System.Threading.Tasks.Task.Run(() => pp.GetBladeStatus1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.DELLEnclosureIP + @" -u root -p calvin getmodinfo", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                //List<string> BladeCollection = new List<string>();
                StringReader strReaderBayStatus = new StringReader(vm.DELLBladeStatus);


                //Read through and only run the ip check on server that are present
                while ((BladePresent = strReaderBayStatus.ReadLine()) != null)
                {

                    if (BladePresent.Contains("Server-"))
                    {


                        //MessageBox.Show(BladePresent);


                        if (BladePresent.Contains("Not Present"))
                        {

                        }
                        else
                        {
                            //New Blade Object
                            blade = new BladeItem();

                            //Blade must be present
                            whichbay = BladePresent.Substring(0, 10);

                            whichbay = whichbay.Replace("Server-", "");// + aLine ;
                                                                       // whichbaycollection.Add(whichbay);
                            blade.Bay = "Bay" + whichbay.Trim();// + aLine ;
                            blade.PowerState = BladePresent.Substring(32, 10).Trim();
                            blade.Manufacturer = "DELL";
                            blade.Status = BladePresent.Substring(48, 10).Trim();
                            blade.ProductName = BladePresent.Substring(59, 20).Trim();

                            //MessageBox.Show(whichbay);
                            //pull ip for server
                            await System.Threading.Tasks.Task.Run(() => pp.GetBladeIP1(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.DELLEnclosureIP + @" -u root -p calvin getniccfg -m server-" + whichbay, @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                            StringReader strReaderBayInfo = new StringReader(vm.DELLBladeIPInfo);

                            while ((aLine = strReaderBayInfo.ReadLine()) != null)
                            {
                                //  MessageBox.Show(aLine.ToString());
                                // MessageBox.Show(aLine.Trim());
                                //if server there extract the info
                                if (aLine.Contains("IP Address"))
                                {



                                    // MessageBox.Show(aLine.Substring(30, 18).Trim());
                                    if (aLine.Contains("192.168.") || aLine.Contains("10.0.") || aLine.Contains("172.16.") || aLine.Contains("172.17.") || aLine.Contains("172.18.") || aLine.Contains("172.19.") || aLine.Contains("172.20.") || aLine.Contains("172.21.") || aLine.Contains("172.22.") || aLine.Contains("172.23.") || aLine.Contains("172.24.") || aLine.Contains("172.25.") || aLine.Contains("172.26.") || aLine.Contains("172.27.") || aLine.Contains("172.28.") || aLine.Contains("172.29.") || aLine.Contains("172.30.") || aLine.Contains("172.31."))
                                    {
                                        //Bay Number
                                        //bayCount = bayCount + 1;
                                        //Extract IP
                                        EqualsPos = aLine.IndexOf("=");                //Server IP
                                        blade.ServerIP = aLine.Substring(EqualsPos + 1).Trim();

                                    }
                                }

                            }

                            //Add blade here
                            vm.BladeipList.Add(blade);

                        }


                    }

                };


                ProgressBarDELL.IsIndeterminate = false;

           }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private async void FactoryResetBTN_Click(object sender, RoutedEventArgs e)
        {

            try
            {

           

            ProgressBarDELL.IsIndeterminate = true;
            ProcessPiper pp = new ProcessPiper();

            string Bays = "";

            foreach (BladeItem itm in EnclosureBaysLV.SelectedItems)
            {
               //MessageBox.Show("Factory Reset Implemented");
                await System.Threading.Tasks.Task.Run(() => pp.StartRACDMEnclosure(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.DELLEnclosureIP + @" -u root -p calvin racresetcfg -m server-" + itm.Bay.Replace("Bay", ""), @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                await PutTaskDelay(4000);

                Bays += "Bay" + itm.Bay.Replace("Bay", "") + " ";
            }

            vm.ILORestOutputEnclosure = App.Current.FindResource("SvrENCFactoryDefaultsDellP1MSG").ToString() + Bays + App.Current.FindResource("SvrENCFactoryDefaultsDellP2MSG").ToString();

            ProgressBarDELL.IsIndeterminate = false;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private async void DHCPBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {


            ProgressBarDELL.IsIndeterminate = true;

            ProcessPiper pp = new ProcessPiper();

            string Bays = "";

            foreach (BladeItem itm in EnclosureBaysLV.SelectedItems)
            {
                await System.Threading.Tasks.Task.Run(() => pp.StartRACDMEnclosure(new System.IO.FileInfo(@"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm\racadm.exe"), @"", @"-r " + vm.DELLEnclosureIP + @" -u root -p calvin setniccfg -m server-" + itm.Bay.Replace("Bay", "") + " -d", @"" + vm.myDocs + @"\HPTOOLS\DELL\SysMgt\iDRACTools\racadm"));

                await PutTaskDelay(4000);

                Bays +=  "Bay" + itm.Bay.Replace("Bay", "") + " ";
            }


            vm.ILORestOutputEnclosure = App.Current.FindResource("SvrENCDHCPDellP1MSG").ToString() + Bays;

            ProgressBarDELL.IsIndeterminate = false;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }


        //Delay
        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

        private void SelectAllDELLBTN_Click(object sender, RoutedEventArgs e)
        {
            EnclosureBaysLV.SelectAll();
        }

        private void PackIconSimpleIcons_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            vm.ShowDELLFlyout = false;
        }

        private void GroupBox_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            vm.ShowDELLFlyout = false;
        }

        private void HostTxtB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //Clear items before check
                vm.BladeipList.Clear();
                vm.SelectedBladeipList.Clear();

                //Call checkenclosure method
                CheckEnclosure();

            }
        }
    }
}
