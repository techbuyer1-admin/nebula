﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for TestToolsView.xaml
    /// </summary>
    public partial class TestToolsView : UserControl
    {
        public TestToolsView()
        {
            StaticFunctions.Department = "TestDept";
            InitializeComponent();
        }
    }
}
