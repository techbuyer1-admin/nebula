﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Nebula.ViewModels;
using Nebula.Models;
using MahApps.Metro.Controls;
using Nebula.Commands;
using System.IO;
using Nebula.Paginators;
using System.Windows.Media;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Net.Security;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Net.Http;
using Nebula.DellServers;
using Nebula.Helpers;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for HPMultiServerView.xaml
    /// </summary>
    public partial class HPMultiServerView : UserControl
    {


        //declare viewmodel
        MainViewModel vm;     //

        public HPMultiServerView()
        {
            InitializeComponent();


        }




        public HPMultiServerView(MainViewModel PassedViewModel, bool isGOT,string HPGen)
        {
            InitializeComponent();
            //get the passed in viewmodel and assign to the local vm variable
            vm = PassedViewModel;


            switch (HPGen)
            {
              case "Gen8":

                    if (isGOT == true)
                    {
                        //Goods Out Technical
                        vm.CurrentView1 = new HPG8ServerView(vm, "1", vm.Server1IPv4, false, "");
                        vm.CurrentView2 = new HPG8ServerView(vm, "2", vm.Server2IPv4, false, "");
                        vm.CurrentView3 = new HPG8ServerView(vm, "3", vm.Server3IPv4, false, "");
                        vm.CurrentView4 = new HPG8ServerView(vm, "4", vm.Server4IPv4, false, "");
                        vm.CurrentView5 = new HPG8ServerView(vm, "5", vm.Server5IPv4, false, "");
                        vm.CurrentView6 = new HPG8ServerView(vm, "6", vm.Server6IPv4, false, "");
                        vm.CurrentView7 = new HPG8ServerView(vm, "7", vm.Server7IPv4, false, "");
                        vm.CurrentView8 = new HPG8ServerView(vm, "8", vm.Server8IPv4, false, "");
                        vm.CurrentView9 = new HPG8ServerView(vm, "9", vm.Server9IPv4, false, "");
                        vm.CurrentView10 = new HPG8ServerView(vm, "10", vm.Server10IPv4, false, "");
                        vm.CurrentView11 = new HPG8ServerView(vm, "11", vm.Server11IPv4, false, "");
                        vm.CurrentView12 = new HPG8ServerView(vm, "12", vm.Server12IPv4, false, "");
                        vm.CurrentView13 = new HPG8ServerView(vm, "13", vm.Server13IPv4, false, "");
                        vm.CurrentView14 = new HPG8ServerView(vm, "14", vm.Server14IPv4, false, "");
                        vm.CurrentView15 = new HPG8ServerView(vm, "15", vm.Server15IPv4, false, "");
                        vm.CurrentView16 = new HPG8ServerView(vm, "16", vm.Server16IPv4, false, "");
                     
                    }
                    else
                    {
                        //Goods in
                        vm.CurrentView1 = new HPG8ServerView(vm, "1", vm.Server1IPv4, false, "");
                        vm.CurrentView2 = new HPG8ServerView(vm, "2", vm.Server2IPv4, false, "");
                        vm.CurrentView3 = new HPG8ServerView(vm, "3", vm.Server3IPv4, false, "");
                        vm.CurrentView4 = new HPG8ServerView(vm, "4", vm.Server4IPv4, false, "");
                        vm.CurrentView5 = new HPG8ServerView(vm, "5", vm.Server5IPv4, false, "");
                        vm.CurrentView6 = new HPG8ServerView(vm, "6", vm.Server6IPv4, false, "");
                        vm.CurrentView7 = new HPG8ServerView(vm, "7", vm.Server7IPv4, false, "");
                        vm.CurrentView8 = new HPG8ServerView(vm, "8", vm.Server8IPv4, false, "");
                        vm.CurrentView9 = new HPG8ServerView(vm, "9", vm.Server9IPv4, false, "");
                        vm.CurrentView10 = new HPG8ServerView(vm, "10", vm.Server10IPv4, false, "");
                        vm.CurrentView11 = new HPG8ServerView(vm, "11", vm.Server11IPv4, false, "");
                        vm.CurrentView12 = new HPG8ServerView(vm, "12", vm.Server12IPv4, false, "");
                        vm.CurrentView13 = new HPG8ServerView(vm, "13", vm.Server13IPv4, false, "");
                        vm.CurrentView14 = new HPG8ServerView(vm, "14", vm.Server14IPv4, false, "");
                        vm.CurrentView15 = new HPG8ServerView(vm, "15", vm.Server15IPv4, false, "");
                        vm.CurrentView16 = new HPG8ServerView(vm, "16", vm.Server16IPv4, false, "");
                    }

                    break;

              case "Gen9":

                    if (isGOT == true)
                    {
                        //Goods Out Technical
                        vm.CurrentView1 = new HPG9ServerView(vm, "1", vm.Server1IPv4, false, "");
                        vm.CurrentView2 = new HPG9ServerView(vm, "2", vm.Server2IPv4, false, "");
                        vm.CurrentView3 = new HPG9ServerView(vm, "3", vm.Server3IPv4, false, "");
                        vm.CurrentView4 = new HPG9ServerView(vm, "4", vm.Server4IPv4, false, "");
                        vm.CurrentView5 = new HPG9ServerView(vm, "5", vm.Server5IPv4, false, "");
                        vm.CurrentView6 = new HPG9ServerView(vm, "6", vm.Server6IPv4, false, "");
                        vm.CurrentView7 = new HPG9ServerView(vm, "7", vm.Server7IPv4, false, "");
                        vm.CurrentView8 = new HPG9ServerView(vm, "8", vm.Server8IPv4, false, "");
                        vm.CurrentView9 = new HPG9ServerView(vm, "9", vm.Server9IPv4, false, "");
                        vm.CurrentView10 = new HPG9ServerView(vm, "10", vm.Server10IPv4, false, "");
                        vm.CurrentView11 = new HPG9ServerView(vm, "11", vm.Server11IPv4, false, "");
                        vm.CurrentView12 = new HPG9ServerView(vm, "12", vm.Server12IPv4, false, "");
                        vm.CurrentView13 = new HPG9ServerView(vm, "13", vm.Server13IPv4, false, "");
                        vm.CurrentView14 = new HPG9ServerView(vm, "14", vm.Server14IPv4, false, "");
                        vm.CurrentView15 = new HPG9ServerView(vm, "15", vm.Server15IPv4, false, "");
                        vm.CurrentView16 = new HPG9ServerView(vm, "16", vm.Server16IPv4, false, "");
                        //vm.CurrentView1 = new HPG9ServerViewGOT(vm, "1", vm.Server1IPv4, false, "");
                        //vm.CurrentView2 = new HPG9ServerViewGOT(vm, "2", vm.Server2IPv4, false, "");
                        //vm.CurrentView3 = new HPG9ServerViewGOT(vm, "3", vm.Server3IPv4, false, "");
                        //vm.CurrentView4 = new HPG9ServerViewGOT(vm, "4", vm.Server4IPv4, false, "");
                        //vm.CurrentView5 = new HPG9ServerViewGOT(vm, "5", vm.Server5IPv4, false, "");
                        //vm.CurrentView6 = new HPG9ServerViewGOT(vm, "6", vm.Server6IPv4, false, "");
                        //vm.CurrentView7 = new HPG9ServerViewGOT(vm, "7", vm.Server7IPv4, false, "");
                        //vm.CurrentView8 = new HPG9ServerViewGOT(vm, "8", vm.Server8IPv4, false, "");
                        //vm.CurrentView9 = new HPG9ServerViewGOT(vm, "9", vm.Server9IPv4, false, "");
                        //vm.CurrentView10 = new HPG9ServerViewGOT(vm, "10", vm.Server10IPv4, false, "");
                        //vm.CurrentView11 = new HPG9ServerViewGOT(vm, "11", vm.Server11IPv4, false, "");
                        //vm.CurrentView12 = new HPG9ServerViewGOT(vm, "12", vm.Server12IPv4, false, "");
                        //vm.CurrentView13 = new HPG9ServerViewGOT(vm, "13", vm.Server13IPv4, false, "");
                        //vm.CurrentView14 = new HPG9ServerViewGOT(vm, "14", vm.Server14IPv4, false, "");
                        //vm.CurrentView15 = new HPG9ServerViewGOT(vm, "15", vm.Server15IPv4, false, "");
                        //vm.CurrentView16 = new HPG9ServerViewGOT(vm, "16", vm.Server16IPv4, false, "");
                    }
                    else
                    {
                        //Goods in
                        vm.CurrentView1 = new HPG9ServerView(vm, "1", vm.Server1IPv4, false, "");
                        vm.CurrentView2 = new HPG9ServerView(vm, "2", vm.Server2IPv4, false, "");
                        vm.CurrentView3 = new HPG9ServerView(vm, "3", vm.Server3IPv4, false, "");
                        vm.CurrentView4 = new HPG9ServerView(vm, "4", vm.Server4IPv4, false, "");
                        vm.CurrentView5 = new HPG9ServerView(vm, "5", vm.Server5IPv4, false, "");
                        vm.CurrentView6 = new HPG9ServerView(vm, "6", vm.Server6IPv4, false, "");
                        vm.CurrentView7 = new HPG9ServerView(vm, "7", vm.Server7IPv4, false, "");
                        vm.CurrentView8 = new HPG9ServerView(vm, "8", vm.Server8IPv4, false, "");
                        vm.CurrentView9 = new HPG9ServerView(vm, "9", vm.Server9IPv4, false, "");
                        vm.CurrentView10 = new HPG9ServerView(vm, "10", vm.Server10IPv4, false, "");
                        vm.CurrentView11 = new HPG9ServerView(vm, "11", vm.Server11IPv4, false, "");
                        vm.CurrentView12 = new HPG9ServerView(vm, "12", vm.Server12IPv4, false, "");
                        vm.CurrentView13 = new HPG9ServerView(vm, "13", vm.Server13IPv4, false, "");
                        vm.CurrentView14 = new HPG9ServerView(vm, "14", vm.Server14IPv4, false, "");
                        vm.CurrentView15 = new HPG9ServerView(vm, "15", vm.Server15IPv4, false, "");
                        vm.CurrentView16 = new HPG9ServerView(vm, "16", vm.Server16IPv4, false, "");
                    }

                    break;

              case "Gen10":

                    if (isGOT == true)
                    {
                        //Goods Out Technical
                        vm.CurrentView1 = new HPG10ServerView(vm, "1", vm.Server1IPv4, false, "");
                        vm.CurrentView2 = new HPG10ServerView(vm, "2", vm.Server2IPv4, false, "");
                        vm.CurrentView3 = new HPG10ServerView(vm, "3", vm.Server3IPv4, false, "");
                        vm.CurrentView4 = new HPG10ServerView(vm, "4", vm.Server4IPv4, false, "");
                        vm.CurrentView5 = new HPG10ServerView(vm, "5", vm.Server5IPv4, false, "");
                        vm.CurrentView6 = new HPG10ServerView(vm, "6", vm.Server6IPv4, false, "");
                        vm.CurrentView7 = new HPG10ServerView(vm, "7", vm.Server7IPv4, false, "");
                        vm.CurrentView8 = new HPG10ServerView(vm, "8", vm.Server8IPv4, false, "");
                        vm.CurrentView9 = new HPG10ServerView(vm, "9", vm.Server9IPv4, false, "");
                        vm.CurrentView10 = new HPG10ServerView(vm, "10", vm.Server10IPv4, false, "");
                        vm.CurrentView11 = new HPG10ServerView(vm, "11", vm.Server11IPv4, false, "");
                        vm.CurrentView12 = new HPG10ServerView(vm, "12", vm.Server12IPv4, false, "");
                        vm.CurrentView13 = new HPG10ServerView(vm, "13", vm.Server13IPv4, false, "");
                        vm.CurrentView14 = new HPG10ServerView(vm, "14", vm.Server14IPv4, false, "");
                        vm.CurrentView15 = new HPG10ServerView(vm, "15", vm.Server15IPv4, false, "");
                        vm.CurrentView16 = new HPG10ServerView(vm, "16", vm.Server16IPv4, false, "");
                        //vm.CurrentView1 = new HPG10ServerViewGOT(vm, "1", vm.Server1IPv4, false, "");
                        //vm.CurrentView2 = new HPG10ServerViewGOT(vm, "2", vm.Server2IPv4, false, "");
                        //vm.CurrentView3 = new HPG10ServerViewGOT(vm, "3", vm.Server3IPv4, false, "");
                        //vm.CurrentView4 = new HPG10ServerViewGOT(vm, "4", vm.Server4IPv4, false, "");
                        //vm.CurrentView5 = new HPG10ServerViewGOT(vm, "5", vm.Server5IPv4, false, "");
                        //vm.CurrentView6 = new HPG10ServerViewGOT(vm, "6", vm.Server6IPv4, false, "");
                        //vm.CurrentView7 = new HPG10ServerViewGOT(vm, "7", vm.Server7IPv4, false, "");
                        //vm.CurrentView8 = new HPG10ServerViewGOT(vm, "8", vm.Server8IPv4, false, "");
                        //vm.CurrentView9 = new HPG10ServerViewGOT(vm, "9", vm.Server9IPv4, false, "");
                        //vm.CurrentView10 = new HPG10ServerViewGOT(vm, "10", vm.Server10IPv4, false, "");
                        //vm.CurrentView11 = new HPG10ServerViewGOT(vm, "11", vm.Server11IPv4, false, "");
                        //vm.CurrentView12 = new HPG10ServerViewGOT(vm, "12", vm.Server12IPv4, false, "");
                        //vm.CurrentView13 = new HPG10ServerViewGOT(vm, "13", vm.Server13IPv4, false, "");
                        //vm.CurrentView14 = new HPG10ServerViewGOT(vm, "14", vm.Server14IPv4, false, "");
                        //vm.CurrentView15 = new HPG10ServerViewGOT(vm, "15", vm.Server15IPv4, false, "");
                        //vm.CurrentView16 = new HPG10ServerViewGOT(vm, "16", vm.Server16IPv4, false, "");
                    }
                    else
                    {
                        ////Goods in
                        vm.CurrentView1 = new HPG10ServerView(vm, "1", vm.Server1IPv4, false, "");
                        vm.CurrentView2 = new HPG10ServerView(vm, "2", vm.Server2IPv4, false, "");
                        vm.CurrentView3 = new HPG10ServerView(vm, "3", vm.Server3IPv4, false, "");
                        vm.CurrentView4 = new HPG10ServerView(vm, "4", vm.Server4IPv4, false, "");
                        vm.CurrentView5 = new HPG10ServerView(vm, "5", vm.Server5IPv4, false, "");
                        vm.CurrentView6 = new HPG10ServerView(vm, "6", vm.Server6IPv4, false, "");
                        vm.CurrentView7 = new HPG10ServerView(vm, "7", vm.Server7IPv4, false, "");
                        vm.CurrentView8 = new HPG10ServerView(vm, "8", vm.Server8IPv4, false, "");
                        vm.CurrentView9 = new HPG10ServerView(vm, "9", vm.Server9IPv4, false, "");
                        vm.CurrentView10 = new HPG10ServerView(vm, "10", vm.Server10IPv4, false, "");
                        vm.CurrentView11 = new HPG10ServerView(vm, "11", vm.Server11IPv4, false, "");
                        vm.CurrentView12 = new HPG10ServerView(vm, "12", vm.Server12IPv4, false, "");
                        vm.CurrentView13 = new HPG10ServerView(vm, "13", vm.Server13IPv4, false, "");
                        vm.CurrentView14 = new HPG10ServerView(vm, "14", vm.Server14IPv4, false, "");
                        vm.CurrentView15 = new HPG10ServerView(vm, "15", vm.Server15IPv4, false, "");
                        vm.CurrentView16 = new HPG10ServerView(vm, "16", vm.Server16IPv4, false, "");
                    }

                    break;
         
              case "Gen10AMD":

                    if (isGOT == true)
                    {
                        //Goods Out Technical
                        vm.CurrentView1 = new HPG10AMDServerView(vm, "1", vm.Server1IPv4, false, "");
                        vm.CurrentView2 = new HPG10AMDServerView(vm, "2", vm.Server2IPv4, false, "");
                        vm.CurrentView3 = new HPG10AMDServerView(vm, "3", vm.Server3IPv4, false, "");
                        vm.CurrentView4 = new HPG10AMDServerView(vm, "4", vm.Server4IPv4, false, "");
                        vm.CurrentView5 = new HPG10AMDServerView(vm, "5", vm.Server5IPv4, false, "");
                        vm.CurrentView6 = new HPG10AMDServerView(vm, "6", vm.Server6IPv4, false, "");
                        vm.CurrentView7 = new HPG10AMDServerView(vm, "7", vm.Server7IPv4, false, "");
                        vm.CurrentView8 = new HPG10AMDServerView(vm, "8", vm.Server8IPv4, false, "");
                        vm.CurrentView9 = new HPG10AMDServerView(vm, "9", vm.Server9IPv4, false, "");
                        vm.CurrentView10 = new HPG10AMDServerView(vm, "10", vm.Server10IPv4, false, "");
                        vm.CurrentView11 = new HPG10AMDServerView(vm, "11", vm.Server11IPv4, false, "");
                        vm.CurrentView12 = new HPG10AMDServerView(vm, "12", vm.Server12IPv4, false, "");
                        vm.CurrentView13 = new HPG10AMDServerView(vm, "13", vm.Server13IPv4, false, "");
                        vm.CurrentView14 = new HPG10AMDServerView(vm, "14", vm.Server14IPv4, false, "");
                        vm.CurrentView15 = new HPG10AMDServerView(vm, "15", vm.Server15IPv4, false, "");
                        vm.CurrentView16 = new HPG10AMDServerView(vm, "16", vm.Server16IPv4, false, "");
                        //vm.CurrentView1 = new HPG10AMDServerViewGOT(vm, "1", vm.Server1IPv4, false, "");
                        //vm.CurrentView2 = new HPG10AMDServerViewGOT(vm, "2", vm.Server2IPv4, false, "");
                        //vm.CurrentView3 = new HPG10AMDServerViewGOT(vm, "3", vm.Server3IPv4, false, "");
                        //vm.CurrentView4 = new HPG10AMDServerViewGOT(vm, "4", vm.Server4IPv4, false, "");
                        //vm.CurrentView5 = new HPG10AMDServerViewGOT(vm, "5", vm.Server5IPv4, false, "");
                        //vm.CurrentView6 = new HPG10AMDServerViewGOT(vm, "6", vm.Server6IPv4, false, "");
                        //vm.CurrentView7 = new HPG10AMDServerViewGOT(vm, "7", vm.Server7IPv4, false, "");
                        //vm.CurrentView8 = new HPG10AMDServerViewGOT(vm, "8", vm.Server8IPv4, false, "");
                        //vm.CurrentView9 = new HPG10AMDServerViewGOT(vm, "9", vm.Server9IPv4, false, "");
                        //vm.CurrentView10 = new HPG10AMDServerViewGOT(vm, "10", vm.Server10IPv4, false, "");
                        //vm.CurrentView11 = new HPG10AMDServerViewGOT(vm, "11", vm.Server11IPv4, false, "");
                        //vm.CurrentView12 = new HPG10AMDServerViewGOT(vm, "12", vm.Server12IPv4, false, "");
                        //vm.CurrentView13 = new HPG10AMDServerViewGOT(vm, "13", vm.Server13IPv4, false, "");
                        //vm.CurrentView14 = new HPG10AMDServerViewGOT(vm, "14", vm.Server14IPv4, false, "");
                        //vm.CurrentView15 = new HPG10AMDServerViewGOT(vm, "15", vm.Server15IPv4, false, "");
                        //vm.CurrentView16 = new HPG10AMDServerViewGOT(vm, "16", vm.Server16IPv4, false, "");
                    }
                    else
                    {
                        ////Goods in
                        vm.CurrentView1 = new HPG10AMDServerView(vm, "1", vm.Server1IPv4, false, "");
                        vm.CurrentView2 = new HPG10AMDServerView(vm, "2", vm.Server2IPv4, false, "");
                        vm.CurrentView3 = new HPG10AMDServerView(vm, "3", vm.Server3IPv4, false, "");
                        vm.CurrentView4 = new HPG10AMDServerView(vm, "4", vm.Server4IPv4, false, "");
                        vm.CurrentView5 = new HPG10AMDServerView(vm, "5", vm.Server5IPv4, false, "");
                        vm.CurrentView6 = new HPG10AMDServerView(vm, "6", vm.Server6IPv4, false, "");
                        vm.CurrentView7 = new HPG10AMDServerView(vm, "7", vm.Server7IPv4, false, "");
                        vm.CurrentView8 = new HPG10AMDServerView(vm, "8", vm.Server8IPv4, false, "");
                        vm.CurrentView9 = new HPG10AMDServerView(vm, "9", vm.Server9IPv4, false, "");
                        vm.CurrentView10 = new HPG10AMDServerView(vm, "10", vm.Server10IPv4, false, "");
                        vm.CurrentView11 = new HPG10AMDServerView(vm, "11", vm.Server11IPv4, false, "");
                        vm.CurrentView12 = new HPG10AMDServerView(vm, "12", vm.Server12IPv4, false, "");
                        vm.CurrentView13 = new HPG10AMDServerView(vm, "13", vm.Server13IPv4, false, "");
                        vm.CurrentView14 = new HPG10AMDServerView(vm, "14", vm.Server14IPv4, false, "");
                        vm.CurrentView15 = new HPG10AMDServerView(vm, "15", vm.Server15IPv4, false, "");
                        vm.CurrentView16 = new HPG10AMDServerView(vm, "16", vm.Server16IPv4, false, "");
                    }

                    break;
            }






            //CLEAR SERVER PROPERTIES ON LOAD
            //reset flipview to start
            //FlipView1.SelectedIndex = 0;
            //delete server object
            //vm.DellServerS1 = null;
            // tab header set back to default
            vm.Server1TabHeader = "Server1";
            //clear server ip
            vm.Server1IPv4 = string.Empty;
            //reset flipview to start
           


            // ADD THIS TO GET SERVER INFO SCRIPT


        }



        public static void AddDocument(FlowDocument from, FlowDocument to)
        {
            TextRange range = new TextRange(from.ContentStart, from.ContentEnd);
            MemoryStream stream = new MemoryStream();
            System.Windows.Markup.XamlWriter.Save(range, stream);
            range.Save(stream, DataFormats.XamlPackage);
            TextRange range2 = new TextRange(to.ContentEnd, to.ContentEnd);
            range2.Load(stream, DataFormats.XamlPackage);
        }


        private void DoThePrint(System.Windows.Documents.FlowDocument document)
        {
            // Clone the source document's content into a new FlowDocument.
            // This is because the pagination for the printer needs to be
            // done differently than the pagination for the displayed page.
            // We print the copy, rather that the original FlowDocument.
            System.IO.MemoryStream s = new System.IO.MemoryStream();
            TextRange source = new TextRange(document.ContentStart, document.ContentEnd);
            source.Save(s, DataFormats.Xaml);
            FlowDocument copy = new FlowDocument();
            TextRange dest = new TextRange(copy.ContentStart, copy.ContentEnd);
            dest.Load(s, DataFormats.Xaml);

            // Create a XpsDocumentWriter object, implicitly opening a Windows common print dialog,
            // and allowing the user to select a printer.

            // get information about the dimensions of the seleted printer+media.
            System.Printing.PrintDocumentImageableArea ia = null;
            System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);

            if (docWriter != null && ia != null)
            {
                DocumentPaginator paginator = ((IDocumentPaginatorSource)copy).DocumentPaginator;

                // Change the PageSize and PagePadding for the document to match the CanvasSize for the printer device.
                paginator.PageSize = new Size(ia.MediaSizeWidth, ia.MediaSizeHeight);
                Thickness t = new Thickness(72);  // copy.PagePadding;
                copy.PagePadding = new Thickness(
                                 Math.Max(ia.OriginWidth, t.Left),
                                   Math.Max(ia.OriginHeight, t.Top),
                                   Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
                                   Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));

                copy.ColumnWidth = double.PositiveInfinity;
                //copy.PageWidth = 528; // allow the page to be the natural with of the output device

                // Send content to the printer.
                docWriter.Write(paginator);
            }

        }



        private void KillRemConIP_Click(object sender, RoutedEventArgs e)
        {
            //StaticFunctions.KillProcess("HPLOCONS");
            CloseAppByPid(vm.HPRemoteConsolePIDS1);
        }



        private void CloseAppByPid(int processID)
        {
            try
            {
                Process proc = Process.GetProcessById(processID);
                if (proc.ProcessName.Contains("HPLOCONS"))
                {
                    proc.Kill();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }



        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

    

        [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("6d5140c1-7436-11ce-8034-00aa006009fa")]
        internal interface IServiceProvider
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            object QueryService(ref Guid guidService, ref Guid riid);
        }

        private void CPS2_GotFocus(object sender, RoutedEventArgs e)
        {

        }
    }



}

