﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.ViewModels;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for MainboardInspectionChecklistView.xaml
    /// </summary>
    /// 

  
    public partial class MainboardInspectionChecklistView : UserControl
    {
        GoodsInOutViewModel vm;

        public MainboardInspectionChecklistView(GoodsInOutViewModel PassedViewModel)
        {
            InitializeComponent();
            vm = PassedViewModel;
        }


        private void ToggleSwitch_Checked(object sender, RoutedEventArgs e)
        {
            //Display The Logo
            if (ChecklistLogo.Visibility == Visibility.Collapsed)
            {
                //Display The Logo
                ChecklistLogo.Visibility = Visibility.Visible;
            }



        }

        private void ToggleSwitch_Unchecked(object sender, RoutedEventArgs e)
        {
            if (ChecklistLogo.Visibility == Visibility.Visible)
            {
                //Display The Logo
                ChecklistLogo.Visibility = Visibility.Collapsed;
            }
        }

        private void ToggleSwitch_IsCheckedChanged(object sender, EventArgs e)
        {


        }


    }
}
