﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.ViewModels;
using Nebula.Properties;
using Nebula.Models;
using Nebula.Helpers;
using MahApps.Metro.Controls;
using Nebula.Commands;
using System.IO;
using Renci.SshNet;
using Renci.SshNet.Common;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Windows.Controls.Primitives;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for PartCodeGeneratorView.xaml
    /// </summary>
    public partial class PartCodeGeneratorView : UserControl
    {
        //Set String to DB Path
        public string cs = "";
        // Sql lite connection
        SQLiteConnection sqlite_conn;


        //declare viewmodel
        MainViewModel vm;     //
        public PartCodeGeneratorView()
        {
            InitializeComponent();

            //Point DB to Test Databases
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\PartCodeGeneratorDB.db";
            }
            else
            {
                //Set connection string to Live DB
                //cs =  @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\PartCodeGeneratorDB.db";
                //Use DFS Hosted DB for multiple site access
                cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/PartCodeGeneratorDB.db";
            }
        }

        public PartCodeGeneratorView(MainViewModel PassedViewModel)
        {
            InitializeComponent();

            vm = PassedViewModel;

            //Point DB to Test Databases
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\PartCodeGeneratorDB.db";
            }
            else
            {
                //Set connection string to Live DB
                //cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\PartCodeGeneratorDB.db";
                //Use DFS Hosted DB for multiple site access
                cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/PartCodeGeneratorDB.db";
            }

            //*** Function in vm for creating the partcode
            //vm.CreatePartCode(SelectedType);

           

            //Create db connection
            sqlite_conn = CreateConnection(cs);



       
            //Reset Message
            vm.PartCodeMessage = "";

            //Create 
            vm.GeneratorGradingAllInOne = new PartCodeGradingView(vm, "AllInOne");
            vm.GeneratorGradingApple = new PartCodeGradingView(vm, "Apple");
            vm.GeneratorGradingLaptops = new PartCodeGradingView(vm, "Laptops");
            vm.GeneratorGradingDesktops = new PartCodeGradingView(vm, "Desktops");

            //Set Title to first item
            vm.GeneratorTitle = "All In Ones";
            vm.SelectedType = "All In Ones";
            SelectTypeCMB.SelectedIndex = 1;
            //Clear Items
            vm.HPSparePartNumbersCollection.Clear();

            vm.LaptopDesktopCollection.Clear();

            //Load Any Operatives
            ReadPartData(sqlite_conn, "SELECT * FROM HPPartNumberCollection ORDER BY SparePartNumber");

            AddToolTip();

            //All Data
            //ReadAllData(sqlite_conn);
            //sqlite_conn.Close();

        }




        private void AddToolTip()
        {

            //All In Ones
            AllInOneModelCB.ToolTip = @"Select an All-In-One Model";
            AllInOneModelTB.ToolTip = @"Enter All-In-One Model No";
            AllInOneFFCB.ToolTip = @"Select All-In-One FormFactor";
            AllInOneAbbreviationTB.ToolTip = @"Abbreviation of Model (Forms the beginning of the Part Code)";
            AllInOneCPUGenTB.ToolTip = @"CPU Series Name (e.g Core 2 Duo, i5, i7)";
            AllInOneCPUModelTB.ToolTip = @"CPU Model Number (e.g 6700U)";
            AllInOneScreenSizeTB.ToolTip = @"Screen Dimensions (e.g 17, 22.5 Please include decimal place if listed)";
            AllInOneRAM.ToolTip = @"Enter the amount of system Ram & select the Unit";
            AllInOneHDD.ToolTip = @"Enter the capacity of the Hard Drive & select the Unit.";
            AllInOneGPU.ToolTip = @"Enter the premium Graphics Card Model if present.";

            //Apple
            LaunchWebBTN.ToolTip = "Opens the Every Mac Website. Paste the EMC Number into the search box and hit Look Up";
            AppleModeDescriptionlTB.ToolTip = @"Enter model Type here. (e.g Mac Book Pro)";
            AppleModelTB.ToolTip = @"Copy the Model returned from EveryMac.Com (Include the EMC Number in Brackets)";
            AppleYearTB.ToolTip = @"Enter the Model Year. (e.g Enter 17 for 2017)";
            AppleCPUGenTB.ToolTip = @"CPU Series Name (e.g Core 2 Duo, i5, i7)";
            AppleCPUModelTB.ToolTip = @"CPU Model Number (e.g 6700U)";
            AppleScreenSizeTB.ToolTip = @"Screen Dimensions (e.g 17, 22.5 Please include decimal place if listed)";
            LaptopKBLangTB.ToolTip = @"Enter Keyboard Layout. (e.g UK, US FR)";
            AppleRAM.ToolTip = @"Enter the amount of system Ram & select the Unit";
            AppleHDD.ToolTip = @"Enter the capacity of the Hard Drive & select the Unit.";
            AppleGPU.ToolTip = @"Enter the premium Graphics Card Model if present.";
            //Desktop
            DesktopModelCB.ToolTip = @"Select a Desktop Make\Model";
            DesktopModelTB.ToolTip = @"Enter Desktop Model No";
            DesktopFFCB.ToolTip = @"Select All In One FormFactor";
            DesktopAbbreviationTB.ToolTip = @"Abbreviation of Model (Forms the beginning of the Part Code)";
            DesktopCPUGenTB.ToolTip = @"CPU Series Name (e.g Core 2 Duo, i5, i7)";
            DesktopCPUModelTB.ToolTip = @"CPU Model Number (e.g 6700U)";
            DesktopRAM.ToolTip = @"Enter the amount of system Ram & select the Unit";
            DesktopHDD.ToolTip = @"Enter the capacity of the Hard Drive & select the Unit.";
            DesktopGPU.ToolTip = @"Enter the premium Graphics Card Model if present.";
            //Laptop
            LaptopModelCB.ToolTip = @"Select a Laptop Make\Model";
            LaptopModelTB.ToolTip = @"Enter Laptop Model No";
            LaptopAbbreviationTB.ToolTip = @"Abbreviation of Model (Forms the beginning of the Part Code)";
            LaptopCPUGenTB.ToolTip = @"CPU Series Name (e.g Core 2 Duo, i5, i7)";
            LaptopCPUModelTB.ToolTip = @"CPU Model Number (e.g 6700U)";
            LaptopScreenSizeTB.ToolTip = @"Screen Dimensions (e.g 17, 22.5 Please include decimal place if listed)";
            LaptopKBLangTB.ToolTip = @"Enter Keyboard Layout. (e.g UK, US FR)";
            LaptopRAM.ToolTip = @"Enter the amount of system Ram & select the Unit";
            LaptopHDD.ToolTip = @"Enter the capacity of the Hard Drive & select the Unit.";
            LaptopGPU.ToolTip = @"Enter the premium Graphics Card Model if present.";
            //Cisco Built
            CiscoBuiltModel.ToolTip = "1) Model Number:\n\nYou can find it on the top of the server.\nLabelled as PID VID.Example:PID VID:UCSC-C220M4S";
            CiscoBuiltVersion.ToolTip = "2) Version:(optional)\n\nThe basic system board version.\n\nType in Redwood the system board part number and if it states V2,\nit needs to be included in the part number.";
            CiscoBuiltQtyCPU.ToolTip = "3) Quantity of CPU's:\n\nNumber of CPUs added to the server.";
            CiscoBuiltTypeCPU.ToolTip = "4) Type of CPUs\n\n(not the CPUs PN)\n\nType in Redwood the CPUs part number.\nIt will show you in the description the type of the CPU.\n\nExamples:\nSR0LB – E5 - 2603\nSR1XS – E5 - 2670\nSR2N2 – E5 - 2690\nSR1A5 – E5 - 2690V2";
            CiscoBuiltAddInfo.ToolTip = "5) Additional information: (optional)\nWe use it for differentiate\n similar built servers.\n\nExample:\nWhen I have two servers\n with same configuration,\nbut one of them has 8GB memory inside, the other\n one has 16GB.\n\nOr same built servers, just\nwith different controllers.\nIn built servers’ PN, the\n controller just additional information.";
            CiscoBuiltCondition.ToolTip = "6) Condition:\n\nRetail(nothing)\nOpen Retail(OR)\nBulk(B)\nRefurb(B-REF)\nUsed(B-USED)\nEbay(EB)";
            //Cisco CTO
            CiscoCTOModel.ToolTip = "1) Model Number:\n\nYou can find it on the top of the server.\nLabelled as PID VID.Example:PID VID:UCSC-C220M4S";
            CiscoCTOVersion.ToolTip = "2) Version:(optional)\n\nThe basic system board version.\n\nType in Redwood the system board part number and if it states V2,\nit needs to be included in the part number.";
            CiscoCTOController.ToolTip = "3) Controller: (optional)\n\nMost of the CISCO servers have controllers inside.\nIf not, please make sure they have the right\n\ncabling for the integrated controller.\n\nBecause we don’t have too many CISCO servers in stock,\n\nwe decided to write just CTRL in the\n\nPN as the MRAID12G-2GB we found a bit too long.";
            CiscoCTOFormFactor.ToolTip = "4) Form Factor: (optional)\n\nWe use this option when we have two\n\nof the same servers, but the form factors are different.";
            CiscoCTOAddParts.ToolTip = "5) Additional parts: (optional)\nUse it when something unusual turns up,\nbut you need to differentiate it from the normal PN.\n Just make sure you add the\ndetails in the description.\nExample:\n\nYou want to keep a tape drive in the tower server,\nor the server has internal SSD slots.";
            CiscoCTOCondition.ToolTip = "6) Condition:\n\nRetail(nothing)\nOpen Retail(OR)\nBulk(B)\nRefurb(B-REF)\nUsed(B-USED)\nEbay(EB)";
            //HPE Built
            HPBuiltBasicConfig.ToolTip = "1) Basic Configuration Number:\n\nSearch in Redwood -> Information -> Quickspecs -> add the model number -> \n\nOpen the right Quickspecs and find the right basic configuration\nSKU number by form factors.\n\nCheck the drive bay(s) and count\nhow many and what kind of drives are\n fit in it. 8x 2,5” drives = 8 Small form factor(8SFF)";
            HPBuiltVersion.ToolTip = "2) Version:(optional)\n\nThe basic system board version.\n\nType in Redwood the system board’s spare (SP) number\n and it will show you the version.\n\nG8 can be V1 or V2 If the description specifies V2,\nthen you have to add it in the PN. (not upgraded!)\n\nG9 can be V3 or V4.\nIf the description specifies V4,\n then you have to add it in the PN. (not upgraded!)";
            HPBuiltQtyCPU.ToolTip = "3) Quantity of CPU's:\n\nNumber of CPUs added to the server.";
            HPBuiltTypeCPU.ToolTip = "4) Type of CPUs\n\n(not the CPUs PN)\n\nType in Redwood the CPUs part number.\nIt will show you in the description the type of the CPU.\n\nExamples:\nSR0LB – E5 - 2603\nSR1XS – E5 - 2670\nSR2N2 – E5 - 2690\nSR1A5 – E5 - 2690V2";
            HPBuiltAddInfo.ToolTip = "5) Additional information: (optional)\nWe use it for differentiate\nsimilar built servers.\n\nExample:\nWhen I have two servers\n with same configuration,\nbut one of them has 8GB memory inside, the other\n one has 16GB.\n\nOr same built servers, just\nwith different controllers.\nIn built servers’ PN, the\n controller just additional information.";
            HPBuiltCondition.ToolTip = "6) Condition:\n\nRetail(nothing)\nOpen Retail(OR)\nBulk(B)\nRefurb(B-REF)\nUsed(B-USED)\nEbay(EB)";
            //HPE CTO
            HPCTOBasicConfig.ToolTip = "1) Basic Configuration Number:\n\nSearch in Redwood -> Information -> Quickspecs -> add the model number -> \n\nOpen the right Quickspecs and find the right basic configuration\nSKU number by form factors.\n\nCheck the drive bay(s) and count\nhow many and what kind of drives are\n fit in it. 8x 2,5” drives = 8 Small form factor(8SFF)";
            HPCTOVersion.ToolTip = "2) Version:(optional)\n\nThe basic system board version.\n\nType in Redwood the system board’s spare (SP) number\n and it will show you the version.\n\nG8 can be V1 or V2 If the description specifies V2,\nthen you have to add it in the PN. (not upgraded!)\n\nG9 can be V3 or V4.\nIf the description specifies V4,\n then you have to add it in the PN. (not upgraded!)";
            HPCTOController.ToolTip = "3) Controller: (optional)\n\nHP servers should not be booked in with controllers.\n\nHowever, there are occasions when we may have to do it.\n\nWe leave all the SAS cables inside.";
            HPCTOFormFactor.ToolTip = "4) Form Factor: (optional)\n\nPlease do not add the form factors for the basic configuration.\n\nExample: 653200 - B21 is the basic configuration number of the DL380p G8 8SFF version.\n\nIf you expand it to 16SFF, then you can add the form factor\nin the PN as the 16SFF version doesn’t have its own basic conf. number\n\nPlease double check in Quickspecs or with an expert person\nthe actual model’s basic config.number possibilities.";
            HPCTOAddParts.ToolTip = "5) Additional parts: (optional)\nUse it when something unusual turns up,\nbut you need to differentiate it from the normal PN.\n Just make sure you add the\ndetails in the description.\nExample:\n\nYou want to keep a tape drive in the tower server,\nor the server has internal SSD slots.";
            HPCTOCondition.ToolTip = "6) Condition:\n\nRetail(nothing)\nOpen Retail(OR)\nBulk(B)\nRefurb(B-REF)\nUsed(B-USED)\nEbay(EB)";
            //DELL Built
            DellBuiltModel.ToolTip = " 1) Model Number:\n\nYou can find it on the server’s front.\n\nPER = Rack\nPET = Tower\n\nExample\n\nPower Edge R620 = PER620\nPower Edge T620 = PET620\n\nSometimes you’ll see PET servers which\nbeen converted to rack server.\nIn this case please add RACK in the PN’s 6th point.";
            DellBuiltVersion.ToolTip = "2) Version:(optional)\n\nThe basic system board version.\n\nType in Redwood the system board’s spare (SP) number\n and it will show you the version.\n\nG8 can be V1 or V2 If the description specifies V2,\nthen you have to add it in the PN. (not upgraded!)\n\nG9 can be V3 or V4.\nIf the description specifies V4,\n then you have to add it in the PN. (not upgraded!)";
            DellBuiltLicense.ToolTip = "3) License:(optional)\n\nCan be without License or can be:\n\nEXP = Express\nENT = Enterprise\nBasic = No License\n Licenses.";
            DellBuiltQtyCPU.ToolTip = "4) Quantity of CPU's:\n\nNumber of CPUs added to the server.";
            DellBuiltTypeCPU.ToolTip = "5) Type of CPUs\n\n(not the CPUs PN)\n\nType in Redwood the CPUs part number.\nIt will show you in the description the type of the CPU.\n\nExamples:\nSR0LB – E5 - 2603\nSR1XS – E5 - 2670\nSR2N2 – E5 - 2690\nSR1A5 – E5 - 2690V2";
            DellBuiltAddInfo.ToolTip = "6) Additional information: (optional)\nWe use it for differentiate\nsimilar built servers.\n\nExample:\nWhen I have two servers\n with same configuration,\nbut one of them has 8GB memory inside, the other\n one has 16GB.\n\nOr same built servers, just\nwith different controllers.\nIn built servers’ PN, the\n controller just additional information.";
            DellBuiltCondition.ToolTip = "7) Condition:\n\nRetail(nothing)\nOpen Retail(OR)\nBulk(B)\nRefurb(B-REF)\nUsed(B-USED)\nEbay(EB)";
            //DELL CTO
            DellCTOModel.ToolTip = "1) Model Number:\n\nYou can find it on the server’s front.\nPER = Rack\nPET = Tower\n\nExample\n\nPower Edge R620 = PER620\nPower Edge T620 = PET620\n\nSometimes you’ll see PET servers which\nbeen converted to rack server.\nIn this case please add RACK in the PN’s 6th point.";
            DellCTOVersion.ToolTip = "2) Version:(optional)\n\nThe basic system board version.\n\nType in Redwood the system board part number and if it states V2,\nit needs to be included in the part number.";
            DellCTOLicense.ToolTip = "3) License:(optional)\n\nCan be without License or can be:\n\nEXP = Express\nENT = Enterprise\nBasic = No License\n Licenses.";
            DellCTOController.ToolTip = "4) Controller: (optional)\n\nMost of the Dell servers have controllers inside.\nIf not, please make sure they have the right\n\ncabling for the integrated controller.\n\nBecause we don’t have too many Dell servers in stock,\n\nwe decided to write just CTRL in the\n\nPN as the MRAID12G-2GB we found a bit too long.";
            DellCTOFormFactor.ToolTip = "5) Form Factor: (optional)\n\nWe use this option when we have two\n\nof the same servers, but the form factors are different.";
            DellCTOAddParts.ToolTip = "6) Additional parts: (optional)\nUse it when something unusual turns up,\nbut you need to differentiate it from the normal PN.\n Just make sure you add the\ndetails in the description.\nExample:\n\nYou want to keep a tape drive in the tower server,\nor the server has internal SSD slots.";
            DellCTOCondition.ToolTip = "7) Condition:\n\nRetail(nothing)\nOpen Retail(OR)\nBulk(B)\nRefurb(B-REF)\nUsed(B-USED)\nEbay(EB)";
            //Lenovo Built
            LenovoBuiltTypeNumber.ToolTip = "1) Type Number:\n\nWe decided to change the way\nhow we book in IBM and Lenovo servers.\nEarlier we used the model number on\nthe server’s front.\n\n(example: X3550 M5)\nFrom now on please book in the servers by Type number\nand add the Model number in the description.\n\nYou can find it on the ear with little letters. (after MT:)\n\nExample:\nMT: 7915 - A2C or  7915A2C";
            LenovoBuiltQtyCPU.ToolTip = "2) Quantity of CPU's:\n\nNumber of CPUs added to the server";
            LenovoBuiltTypeCPU.ToolTip = "3) Type of CPUs\n\n(not the CPUs PN)\n\nType in Redwood the CPUs part number.\nIt will show you in the description the type of the CPU.\n\nExamples:\nSR0LB – E5 - 2603\nSR1XS – E5 - 2670\nSR2N2 – E5 - 2690\nSR1A5 – E5 - 2690V2";
            LenovoBuiltAddInfo.ToolTip = "4) Additional information: (optional)\nWe use it for differentiate\nsimilar built servers.\n\nExample:\nWhen I have two servers\n with same configuration,\nbut one of them has 8GB memory inside, the other\n one has 16GB.\n\nOr same built servers, just\nwith different controllers.\nIn built servers’ PN, the\n controller just additional information.";
            LenovoBuiltCondition.ToolTip = "5) Condition:\n\nRetail(nothing)\nOpen Retail(OR)\nBulk(B)\nRefurb(B-REF)\nUsed(B-USED)\nEbay(EB)";
            LenovoBuiltIBMLen.ToolTip = "6) IBM or Lenovo:(optional)\n\nAs you know Lenovo bought IBM.\nHowever, customers still think of them as two different company.\nIdentical servers can come into stock, but with different logo.\n\nIf we receive a server with Lenovo logo,\nthen we put LENOVO at the end of the PN.";

            //Lenovo CTO
            LenovoCTOTypeNumber.ToolTip = "1) Type Number:\n\nWe decided to change the way\nhow we book in IBM and Lenovo servers.\nEarlier we used the model number on\nthe server’s front.\n\n(example: X3550 M5)\nFrom now on please book in the servers by Type number\nand add the Model number in the description.\n\nYou can find it on the ear with little letters. (after MT:)\n\nExample:\nMT: 7915 - A2C or  7915A2C";
            LenovoCTOController.ToolTip = "2) Controller: (optional)\n\nMost of the Lenovo servers have controllers inside.\nIf not, please make sure they have the right\n\ncabling for the integrated controller.\n\nBecause we don’t have too many Lenovo servers in stock,\n\nwe decided to write just CTRL in the\n\nPN as the MRAID12G-2GB we found a bit too long.";
            LenovoCTOFormFactor.ToolTip = "3) Form Factor: (optional)\n\nWe use this option when we have two\n\nof the same servers, but the form factors are different.";
            LenovoCTOAddParts.ToolTip = "4) Additional parts: (optional)\nUse it when something unusual turns up,\nbut you need to differentiate it from the normal PN.\n Just make sure you add the\ndetails in the description.\nExample:\n\nYou want to keep a tape drive in the tower server,\nor the server has internal SSD slots.";
            LenovoCTOCondition.ToolTip = "5) Condition:\n\nRetail(nothing)\nOpen Retail(OR)\nBulk(B)\nRefurb(B-REF)\nUsed(B-USED)\nEbay(EB)";
            LenovoCTOIBMLen.ToolTip = "6) IBM or Lenovo:(optional)\n\nAs you know Lenovo bought IBM.\nHowever, customers still think of them as two different company.\nIdentical servers can come into stock, but with different logo.\n\nIf we receive a server with Lenovo logo,\nthen we put LENOVO at the end of the PN.";
        }




        static SQLiteConnection CreateConnection(string pathtodb)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            //sqlite_conn = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            //sqlite_conn = new SQLiteConnection("Data Source=" + pathtodb + "; Version = 3; New = True; Compress = True; ");
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sqlite_conn;
        }



        public void ReadPartData(SQLiteConnection conn, string sqlquery)
        {

            try
            {



                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {

                    //Create Instance of part numbers class
                    HPPartNumbers op = new HPPartNumbers();

                    op.SparePartNumber = sqlite_datareader.GetString(0);
                    op.OptionPartNumber = sqlite_datareader.GetString(1);
                    op.Obsolete = sqlite_datareader.GetInt32(2);

                    vm.HPSparePartNumbersCollection.Add(op);
                    
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message);
            }

        }

        //Laptop Desktop
        public void ReadLaptopDesktopData(SQLiteConnection conn, string sqlquery)
        {

            try
            {



                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {

                    //Create Instance of part numbers class
                    LaptopDesktopModel op = new LaptopDesktopModel();

                    op.Type = sqlite_datareader.GetString(0);
                    op.Manufacturer = sqlite_datareader.GetString(1);
                    op.Model = sqlite_datareader.GetString(2);
                    op.Abbreviations = sqlite_datareader.GetString(3);

                    //Add to Main Collection
                    vm.LaptopDesktopCollection.Add(op);
                    //Add to Combined String collection for Combo Box Selection, on selection Abbreviation needs to be pulled
                    vm.LaptopDesktopCombinedCollection.Add(op.Manufacturer + "_" + op.Model);

                   

                }

                // sqlite_conn.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message);
            }

        }

        public void ReadDesktopFormFactorData(SQLiteConnection conn, string sqlquery)
        {

            try
            {
                //Clear collections
                vm.FormFactorCombinedCollection.Clear();

                SQLiteDataReader sqlite_datareader;
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = sqlquery;

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {

                    //Create Instance of part numbers class
                    DesktopFormFactorModel op = new DesktopFormFactorModel();

                    op.Manufacturer = sqlite_datareader.GetString(0);
                    op.Description = sqlite_datareader.GetString(1);
                    op.Abbreviations = sqlite_datareader.GetString(2);

                    vm.DesktopFormFactorCollection.Add(op);
                    vm.AllInOneFormFactorCollection.Add(op);
                    //Add to Combined String collection for Combo Box Selection, on selection Abbreviation needs to be pulled
                    vm.FormFactorCombinedCollection.Add(op.Description);
                }

                //sqlite_conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message);
            }

        }



        private void SelectTypeCMB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            //MessageBox.Show(SelectTypeCMB.Text);
            if (AllInOnes != null)
            {
                AllInOnes.Visibility = Visibility.Collapsed;
                Apple.Visibility = Visibility.Collapsed;
                Laptops.Visibility = Visibility.Collapsed;
                Desktops.Visibility = Visibility.Collapsed;
                CISCOBUILTServer.Visibility = Visibility.Collapsed;
                CISCOCTOServer.Visibility = Visibility.Collapsed;
                DELLBUILTServer.Visibility = Visibility.Collapsed;
                DELLCTOServer.Visibility = Visibility.Collapsed;
                HPBUILTServer.Visibility = Visibility.Collapsed;
                HPCTOServer.Visibility = Visibility.Collapsed;
                IBMLENBUILTServer.Visibility = Visibility.Collapsed;
                IBMLENCTOServer.Visibility = Visibility.Collapsed;
                CiscoDrives.Visibility = Visibility.Collapsed;
                DELLDrives.Visibility = Visibility.Collapsed;
                EMCDrives.Visibility = Visibility.Collapsed;
                FujitsuDrives.Visibility = Visibility.Collapsed;
                HPDrives.Visibility = Visibility.Collapsed;
                IBMDrives.Visibility = Visibility.Collapsed;
                SUNOracleDrives.Visibility = Visibility.Collapsed;

                //Clear Collections
                vm.LaptopDesktopCollection.Clear();
                vm.LaptopDesktopCombinedCollection.Clear();
                //Show Keyboard field if Model not a Surface Pro
                LaptopKBLangSP.Visibility = Visibility.Visible;

                //Clear Title
                vm.GeneratorTitle = "";
                //Reset Properties
                ResetGenerators();
                //vm.GeneratorGrading = null;

              

                switch (vm.SelectedType)
                {
                    case "All In Ones":
                        //Call DB for Values
                        ReadLaptopDesktopData(sqlite_conn, "SELECT * FROM GeneratorTypes WHERE Type = 'Desktop' ORDER BY Manufacturer");
                        vm.GeneratorTitle = "All In One Part-Code Generator";
                        DescriptionTitleTB.Visibility = Visibility.Visible;
                        DescriptionCodeResultTB.Visibility = Visibility.Visible;
                        // vm.GeneratorGrading = null;
                        //New grading instance

                        AllInOnes.Visibility = Visibility.Visible;
                        break;
                    case "Apple":
                        vm.GeneratorTitle = "Apple Part-Code Generator";
                        DescriptionTitleTB.Visibility = Visibility.Visible;
                        DescriptionCodeResultTB.Visibility = Visibility.Visible;
                        //vm.GeneratorGrading = null;
                        //New grading instance
                        //vm.GeneratorGrading = new PartCodeGradingView(vm, "Apple");
                        Apple.Visibility = Visibility.Visible;
                        break;
                    case "Desktops":

                        //Call DB for Values
                        ReadLaptopDesktopData(sqlite_conn, "SELECT * FROM GeneratorTypes WHERE Type = 'Desktop' ORDER BY Manufacturer");
                        vm.GeneratorTitle = "Desktop Part-Code Generator";
                        DescriptionTitleTB.Visibility = Visibility.Visible;
                        DescriptionCodeResultTB.Visibility = Visibility.Visible;
                        //New grading instance
                        //vm.GeneratorGrading = new PartCodeGradingView(vm, "Desktops");
                        Desktops.Visibility = Visibility.Visible;
                        DesktopModelCB.SelectedIndex = -1;
                        vm.DesktopModel = "";

                        break;
                    case "Laptops":

                        //Call DB for Values
                        ReadLaptopDesktopData(sqlite_conn, "SELECT * FROM GeneratorTypes WHERE Type = 'Laptop' ORDER BY Manufacturer");
                        vm.GeneratorTitle = "Laptop Part-Code Generator";
                        DescriptionTitleTB.Visibility = Visibility.Visible;
                        DescriptionCodeResultTB.Visibility = Visibility.Visible;
                        //New grading instance
                        //vm.GeneratorGrading = new PartCodeGradingView(vm, "Laptops");
                        Laptops.Visibility = Visibility.Visible;
                        LaptopModelCB.SelectedIndex = -1;
                        vm.LaptopModel = "";

                        break;

                    case "Cisco BUILT Server":
                        vm.GeneratorTitle = "Cisco BUILT Server";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        CISCOBUILTServer.Visibility = Visibility.Visible;
                        break;
                    case "Cisco CTO Server":
                        vm.GeneratorTitle = "Cisco CTO Server";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        CISCOCTOServer.Visibility = Visibility.Visible;
                        break;
                    case "DELL BUILT Server":
                        vm.GeneratorTitle = "DELL BUILT Server";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        DELLBUILTServer.Visibility = Visibility.Visible;
                        break;
                    case "DELL CTO Server":
                        vm.GeneratorTitle = "DELL CTO Server";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        DELLCTOServer.Visibility = Visibility.Visible;
                        break;
                    case "HP BUILT Server":
                        vm.GeneratorTitle = "HP BUILT Server";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        HPBUILTServer.Visibility = Visibility.Visible;
                        break;
                    case "HP CTO Server":
                        vm.GeneratorTitle = "HP CTO Server";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        HPCTOServer.Visibility = Visibility.Visible;
                        break;
                    case "IBM/Lenovo BUILT Server":
                        vm.GeneratorTitle = "IBM/Lenovo BUILT Server";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        IBMLENBUILTServer.Visibility = Visibility.Visible;
                        break;
                    case "IBM/Lenovo CTO Server":
                        vm.GeneratorTitle = "IBM/Lenovo CTO Server";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        IBMLENCTOServer.Visibility = Visibility.Visible;
                        break;
                    case "HP Drives":
                        vm.GeneratorTitle = "HP Drives";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        HPDrives.Visibility = Visibility.Visible;
                        break;
                    case "Dell Drives":
                        vm.GeneratorTitle = "Dell Drives";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        DELLDrives.Visibility = Visibility.Visible;
                        break;
                    case "IBM Drives":
                        vm.GeneratorTitle = "IBM Drives";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        IBMDrives.Visibility = Visibility.Visible;
                        break;
                    case "Cisco Drives":
                        vm.GeneratorTitle = "Cisco Drives";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        CiscoDrives.Visibility = Visibility.Visible;
                        break;
                    case "EMC Drives":
                        vm.GeneratorTitle = "EMC Drives";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        EMCDrives.Visibility = Visibility.Visible;
                        break;
                    case "Sun/Oracle Drives":
                        vm.GeneratorTitle = "Sun/Oracle Drives";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        SUNOracleDrives.Visibility = Visibility.Visible;
                        break;
                    case "Fujitsu Drives":
                        vm.GeneratorTitle = "Fujitsu Drives";
                        DescriptionTitleTB.Visibility = Visibility.Hidden;
                        DescriptionCodeResultTB.Visibility = Visibility.Hidden;
                        CreateBTN.Visibility = Visibility.Collapsed;
                        PrintLabelBTN.Visibility = Visibility.Collapsed;
                        FujitsuDrives.Visibility = Visibility.Visible;
                        break;


                }
            }




        }

       



        private void ResetBTN_Click(object sender, RoutedEventArgs e)
        {

            //Reset all properties
            ResetGenerators();

        }

        //Function to reset values
        private void ResetGenerators()
        {
            //Message
            vm.SelectedModel = "";
            vm.PartCodeMessage = "";

            //Reset All Gradings
            vm.GradingFuncScreen = "";
            vm.GradingFuncTrackpad = "";
            vm.GradingFuncWifi = "";
            vm.GradingFuncPorts = "";
            vm.GradingFuncSpeakers = "";
            vm.GradingFuncKeyboard = "";
            vm.GradingFuncMicrophone = "";
            vm.GradingFuncWebcam = "";
            vm.GradingBattery = "";
            vm.GradingAestOuterCasingLid = "";
            vm.GradingAestOuterCasingBase = "";
            vm.GradingAestScreen = "";
            vm.GradingAestPalmRest = "";
            vm.GradingAestPorts = "";
            vm.GradingAestTrackpad = "";
            vm.GradingAestKeyboard = "";
            vm.GradingAestFrontBezel = "";
            vm.GradingGrade = "";

            //All In Ones
            vm.AllInOneAbbreviation = "";
            vm.AllInOneModel = "";
            vm.AllInOneFormFactor = "";
            vm.AllInOneTouchscreen = false;
            vm.AllInOneScreenSize = "";
            vm.AllInOneKeyboard = "";
            vm.AllInOneCPUGen = "";
            vm.AllInOneCPUModel = "";
            vm.AllInOneRAM = "";
            AllInOneRAMCB.SelectedIndex = -1;
            vm.AllInOneHDD = "";
            AllInOneHDDCB.SelectedIndex = -1;
            vm.AllInOneSSD = false;
            vm.AllInOneGPU = "";
            vm.AllInOneBattery = "";
            vm.AllInOneGrade = "";
            AllInOneModelCB.SelectedIndex = -1;
            AllInOneFFCB.SelectedIndex = -1;
            vm.AllInOneDVD = false;

            //Apple
            vm.AppleModel = "";
            vm.AppleTouchscreen = false;
            vm.AppleScreenSize = "";
            vm.AppleKeyboard = "";
            vm.AppleCPUGen = "";
            vm.AppleCPUModel = "";
            AppleRAMCB.SelectedIndex = -1;
            AppleHDDCB.SelectedIndex = -1;
            vm.AppleRAM = "";
            vm.AppleHDD = "";
            vm.AppleSSD = false;
            vm.AppleGPU = "";
            vm.AppleBattery = "";
            vm.AppleGrade = "";
            vm.AppleModel = "";
            vm.AppleModelDescription = "";
            vm.AppleYear = "";


            //Laptop
            vm.LaptopAbbreviation = "";
            vm.LaptopModel = "";
            vm.LaptopTouchscreen = false;
            vm.LaptopScreenSize = "";
            vm.LaptopKeyboard = "";
            vm.LaptopCPUGen = "";
            vm.LaptopCPUModel = "";
            vm.LaptopRAM = "";
            LaptopRAMCB.SelectedIndex = -1;
            vm.LaptopHDD = "";
            LaptopHDDCB.SelectedIndex = -1;
            vm.LaptopSSD = false;
            vm.LaptopGPU = "";
            vm.LaptopBattery = "";
            vm.LaptopGrade = "";
            LaptopModelCB.SelectedIndex = -1;
            vm.LaptopModel = "";
            LaptopKBLangSP.Visibility = Visibility.Visible;

            //Add Model properties
            vm.AddModelUnitType = "";
            vm.AddModelManufacturer = "";
            vm.AddModelModel = "";
            vm.AddModelAbbreviation = "";

            //Desktop
            vm.DesktopAbbreviation = "";
            vm.DesktopModel = "";
            vm.DesktopFormFactor = "";
            vm.DesktopCPUGen = "";
            vm.DesktopCPUModel = "";
            vm.DesktopRAM = "";
            DesktopRAMCB.SelectedIndex = -1;
            DesktopHDDCB.SelectedIndex = -1;
            vm.DesktopHDD = "";
            vm.DesktopSSD = false;
            vm.DesktopGPU = "";
            vm.DesktopGrade = "";
            DesktopModelCB.SelectedIndex = -1;
            vm.DesktopModel = "";
            DesktopFFCB.SelectedIndex = -1;

            //Servers & HDD's
            vm.TypeNumber = "";
            vm.Version = "";
            vm.CPUQty = "";
            vm.CPUType = "";
            vm.AdditionalPartsInfo = "";
            vm.Condition = "";
            vm.Controller = "";
            vm.License = "";
            vm.FormFactor = "";
            vm.IBMLenovo = "";
            vm.HPCaddySpareNumber = "";
            vm.DELLCaddyOptions = "";
            vm.DELLPN = "";
            vm.IBMOptionNumber = "";
            vm.FujitsuPN = "";
            vm.EMCCaddySpareNumber = "";
            vm.OraclePN = "";
            vm.CISCOCaddySpareNumber = "";
            vm.HPOtherOptions = "";
            vm.GeneratedPartCode = "";

            //CLEAR ALL DATA
            vm.GeneratedPartCode = "";
            vm.GeneratedPartDescription = "";
           
        }


        

        private void LaptopModelCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {

                if (LaptopModelCB.SelectedItem != null)
                {

                    //Get abbreviation based on 
                    //Split selection to an array and split on space
                    string[] laptopModels = LaptopModelCB.SelectedItem.ToString().Split('_');
                   
                    //Linq Search collection for matching Model
                    LaptopDesktopModel lapdesk = vm.LaptopDesktopCollection.Where(l => l.Model == laptopModels[1].ToString()).FirstOrDefault();

                   
                    if (lapdesk != null)
                    {
                        
                        //Check if selected item is a surface pro
                        if (LaptopModelCB.SelectedItem.ToString() == "Microsoft_Surface Pro")
                        {
                            //MessageBox.Show("Surface Pro Selected");
                            //Hide Keyboard as KB is an Optional feature on Surface Pros
                            LaptopKBLangSP.Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            //MessageBox.Show("Surface Pro Selected");
                            //Show Keyboard field if Model not a Surface Pro
                            LaptopKBLangSP.Visibility = Visibility.Visible;
                        }
                        // Return Abbreviation value
                        LaptopAbbreviationTB.Text = lapdesk.Abbreviations;
                    }


                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void DesktopModelCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //Clear form factor value
                vm.DesktopFormFactor = "";

                if (DesktopModelCB.SelectedItem != null)
                {
                    //Get abbreviation based on 
                    //Split selection to an array and split on space
                    string[] desktopModels = DesktopModelCB.SelectedItem.ToString().Split('_');
                    //Set Manafacturer property to get around duplicate abbreviations
                    vm.DesktopManufacturer = desktopModels[0].ToString();
                    //Linq Search collection for matching Model
                    LaptopDesktopModel lapdesk = vm.LaptopDesktopCollection.Where(d => d.Model == desktopModels[1].ToString()).FirstOrDefault();
                    if (lapdesk != null)
                    {


                        // Return Abbreviation value
                        DesktopAbbreviationTB.Text = lapdesk.Abbreviations;

                        // Populate form factors based on Manufacturer
                        ReadDesktopFormFactorData(sqlite_conn, "SELECT * FROM FormFactors WHERE Manufacturer = '" + desktopModels[0] + "' ORDER BY Description");
                    }

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void AllInOneModelCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //Clear form factor value
                vm.AllInOneFormFactor = "";

                if (AllInOneModelCB.SelectedItem != null)
                {
                    

                    //Get abbreviation based on 
                    //Split selection to an array and split on space
                    string[] desktopModels = AllInOneModelCB.SelectedItem.ToString().Split('_');
                    //Set Manafacturer property to get around duplicate abbreviations
                    //vm.DesktopManufacturer = desktopModels[0].ToString();
                    //Linq Search collection for matching Model
                    LaptopDesktopModel lapdesk = vm.LaptopDesktopCollection.Where(d => d.Model == desktopModels[1].ToString()).FirstOrDefault();
                    if (lapdesk != null)
                    {


                        // Return Abbreviation value
                        AllInOneAbbreviationTB.Text = lapdesk.Abbreviations;
                        // Populate form factors based on Manufacturer
                        ReadDesktopFormFactorData(sqlite_conn, "SELECT * FROM FormFactors WHERE Manufacturer = '" + desktopModels[0] + "' ORDER BY Description");
                    }

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }



        private  void CreateBTN_Click(object sender, RoutedEventArgs e)
        {
            //check for presence of previous part number before creating in netsuite e.g "LAT3400-14UK-I5-8265U-8-256-50-C2"
            NetsuiteInventory item = vm.NetsuiteInventoryData.FirstOrDefault(x => x.ProductCode == vm.GeneratedPartCode);


            if (item != null)
            {

                //TimedMessage("Part-Code Already Exists in Netsuite","Warning");
                vm.TimedMessage(App.Current.FindResource("PCGradeCodeExists").ToString(), "Warning", 7000);
            }
            else
            {
                //TimedMessage("Part-Code Already Exists in Netsuite","Warning");
                vm.TimedMessage(App.Current.FindResource("PCGradeNotImplemented").ToString(), "Warning", 7000);

                //TODO NETSUITE PART CODE CREATION
            }

          
        }

        private void LaunchWebBTN_Click(object sender, RoutedEventArgs e)
        {
            //Just open in Chrome or users default
            System.Diagnostics.Process.Start(@"https://everymac.com/ultimate-mac-lookup/");
        }

        private void AllInOneFFCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {


                if (AllInOneModelCB.SelectedItem != null)
                {


                    //Get abbreviation based on 
                    //Linq Search collection for matching Model
                    if (AllInOneFFCB.SelectedItem != null)
                    {
                        DesktopFormFactorModel lapdesk = vm.DesktopFormFactorCollection.Where(d => d.Description == AllInOneFFCB.SelectedItem.ToString()).FirstOrDefault();
                        if (lapdesk != null)
                        {

                            // Return Abbreviation value
                            vm.AllInOneFormFactor = lapdesk.Abbreviations;
                        }
                    }
                  

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void DesktopFFCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {


                if (DesktopModelCB.SelectedItem != null)
                {


                    //Get abbreviation based on 
                    //Linq Search collection for matching Model
                    if (DesktopFFCB.SelectedItem != null)
                    {
                        DesktopFormFactorModel lapdesk = vm.DesktopFormFactorCollection.Where(d => d.Description == DesktopFFCB.SelectedItem.ToString()).FirstOrDefault();
                        if (lapdesk != null)
                        {

                            // Return Abbreviation value
                            vm.DesktopFormFactor = lapdesk.Abbreviations;
                        }
                    }

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


      

        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }

        private void PrintLabelBTN_Click(object sender, RoutedEventArgs e)
        {
            //Check if part code & description are empty
            if(!string.IsNullOrEmpty(vm.GeneratedPartDescription) || !string.IsNullOrEmpty(vm.GeneratedPartCode))
            {

                BarCodePrinterView bpv = new BarCodePrinterView(vm);

                vm.BarcodeDescription = vm.GeneratedPartDescription;
                vm.BarcodeData = vm.GeneratedPartCode;
                NetsuiteInventory nsi = new NetsuiteInventory();
                nsi.PrintQuantity = "1";
                nsi.ProductDescription = vm.GeneratedPartDescription;
                nsi.ProductCode = vm.GeneratedPartCode;

                vm.NetsuiteMultiPrint.Add(nsi);
                //Supply message to user
                vm.TimedMessage(App.Current.FindResource("PCGradeLabelAdded").ToString(), "Info", 7000);


                //bpv.PrintBTNS1.Click(new object(), new RoutedEventArgs(ButtonBase.ClickEvent));
                //bpv.PrintBTNS1.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
                //bpv.PrintBTNS1.;
            }
            else
            {
                //Supply message to user
                vm.TimedMessage(App.Current.FindResource("PCGradeEmptyPartDescription").ToString(), "Warning", 7000);
            }

        }

       
    }
  }

