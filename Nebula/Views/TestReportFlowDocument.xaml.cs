﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nebula.Commands;
using Nebula.Paginators;
using Nebula.ViewModels;
using QRCoder;

namespace Nebula.Views
{
    /// <summary>
    /// Interaction logic for TestReportFlowDocument.xaml
    /// </summary>
    public partial class TestReportFlowDocument : UserControl
    {
        //declare viewmodel
        MainViewModel vm;

        //if all criteria met create the job
        //sql connection
        SQLiteConnection sqlite_conn;
       
        //Local test connection
        public string cs = "";

        //public List<SerialOverides> OveriddenSerialList = new List<SerialOverides>();

        //public string repBody { get; set; }  
        //start index of the documents loaded
        int docindex = 0;
        int currentpage = 1;
        //Def Constructor
        public TestReportFlowDocument()
        {
            InitializeComponent();

            //Point DB to Test Databases
            if (StaticFunctions.TestMode == true)
            {
                //Set connection string to Offline Test DB
                cs = @"URI=file:C:\Users\n.myers\Desktop\Data\TestDept_Reporting.db";
            }
            else
            {
                if (vm.CurrentUserLocation == "United Kingdom")
                {
                    //TEMP Workaround File Server issue
                    Console.WriteLine("Workaround DB");
                    // Set connection string to Live DB
                    cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/TestReporting/TestDept_Reporting.db";
                }
                else
                {

                    //Set connection string to Live DB
                    cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\TestDept_Reporting.db";
                }
            }

            ////Create DB Connection object
            //sqlite_conn = CreateConnection(cs);

            //get the passed in viewmodel and assign to the local vm variable
            // vm = PassedViewModel;
            //set the datacontext of the view to the passed vm
            //this.DataContext = vm;


        }

        public TestReportFlowDocument(MainViewModel PassedViewModel)
        {
            InitializeComponent();

            try
            {
                //get the passed in viewmodel and assign to the local vm variable
                vm = PassedViewModel;

                //Point DB to Test Databases
                if (StaticFunctions.TestMode == true)
                {
                    //Set connection string to Offline Test DB
                    cs = @"URI=file:C:\Users\n.myers\Desktop\Data\TestDept_Reporting.db";
                }
                else
                {
                    if (vm.CurrentUserLocation == "United Kingdom" && File.Exists("//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/TestReporting/TestDept_Reporting.db"))
                    {
                        //TEMP Workaround File Server issue
                        Console.WriteLine("Workaround DB");
                        // Set connection string to Live DB
                        cs = @"URI=//pinnacle.local/tech_resources/Nebula/Repository/Data/United Kingdom/TestReporting/TestDept_Reporting.db";
                    }
                    else
                    {

                        //Set connection string to Live DB
                        cs = @"URI=file:" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\TestDept_Reporting.db";
                    }
                }



             
                //set the datacontext of the view to the passed vm
                //this.DataContext = vm;
                //set initial value
                vm.PagesCount = @"1/" + vm.ReportCollection.Count().ToString();
                //select the report at postion zero in the collection
                vm.ReportBody = vm.ReportCollection.ElementAt(0);
                //extract the serial number from the selected report body
                vm.SerialNo = vm.ExtractSerialNumber(vm.ReportBody);
                //Check Report for Faults
                FaultDetection(vm.ReportBody);

               
                //Create new collection to hold options selected for each page loaded into the viewer
                vm.TestOptionsSelectedCollection = new System.Collections.ObjectModel.ObservableCollection<Models.TestOptionsSelected>();

                //Loop through and add number of items to options collection that mataches pages
                //Add boolean holding values, need to keep hold of state based on index
                for (int i = 0; i < vm.ReportCollection.Count(); i++)
                {
                    vm.TestOptionsSelectedCollection.Add(new Models.TestOptionsSelected((bool)HardResetCB.IsChecked, (bool)GreenLightCB.IsChecked, (bool)FaultyCB.IsChecked));
                }


               



                ////Create DB Connection object
                //sqlite_conn = CreateConnection(cs);

                //string mytext = "[24;0HE[24;1H[24;18H[24;1H[2K[24;1H[?25h[24;1H[1;24r[24;1H[1;24r[24;1H[24;1H[2K[24;1H[?25h[24;1H[24;1HHP-2620-48-PoEP# [24;1H[24;18H[24;1H[?25h[24;18H";

                ////string test = Regex.Replace(vm.ReportBody, @"\e\[\d *;?\d + m", "");
                ////string test = Regex.Replace(vm.ReportBody, @"\e\[\d *;?\d + m", "");
                //string test = Regex.Replace(vm.ReportBody, @"\e\[(\d+;)*(\d+)?[ABCDHJKfmsu]", "");
                //string source1 = "\x1b[2JThis text \x1b[3;20Hcontains several ANSI escapes\x1b[1;2;30;43m to format the text\x1b[K";
                //string source = "[2JThis text [3;20Hcontains several ANSI escapes[1;2;30;43m to format the text[K";
                ////string result = Regex.Replace(source, @"\e\[(\d+;)*(\d+)?[ABCDHJKfmsu]", "");
                //string result = Regex.Replace(mytext, @"\[(\d\d;\d\w\w)", ""); //(\[\d\w)(\[\d\d;\d\d\w)(\[\d\d;\d\w) (\d\d;\d\w)(\d\d;\d\d\w)
                //result = Regex.Replace(result, @"\[(\d\d;\d\w)", "");
                //result = Regex.Replace(result, @"\[(\d\d;\d\d\w)", "");
                //result = Regex.Replace(result, @"\[(\w\d\d\d\w)", "");
                //result = Regex.Replace(result, @"\[(\d;\d\d\w)", "");
                //result = Regex.Replace(result, @"\[(\d\w)", "");
                //result = Regex.Replace(result, @"\[(\W\d\d\w)", "");
                //MessageBox.Show(result);


                //Refresh Current User
                vm.ReportUser = "Technician Name: " + vm.CurrentUserInitials + " " + DateTime.Now.ToShortDateString();
                //Load Order Ref Session Variable PO SO Number
                POSOTXTB.Text = StaticFunctions.SessionOrderRefNumber;


                if (!string.IsNullOrEmpty(vm.SerialNo))
                {
                    //Create Serial QR Code
                    GenerateLabel();
                }

                ////Set message with uncpath & location. To help debunk the US Writing to our DB
                UserLocTB.Text = "DataPath: (" + StaticFunctions.UncPathToUse + ") Location: (" + StaticFunctions.UserLocation + ")";

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }

        //BUTTONS


        public void PrintPDFTest()
        {
            System.Windows.Forms.PrintDialog pd = new System.Windows.Forms.PrintDialog();
            System.Windows.Forms.DialogResult userResp = pd.ShowDialog();
            PrintDocument prtDoc = new PrintDocument();
            if (userResp == System.Windows.Forms.DialogResult.OK)
            {
                if (prtDoc.PrinterSettings.PrinterName == "Microsoft Print to PDF")
                {   // force a reasonable filename
                    //string basename = Path.GetFileNameWithoutExtension();
                    //string directory = Path.GetDirectoryName(myFileName);

                    string basename = vm.SerialNo;
                    string directory = @"C:\Users\N.Myers\Desktop\Invoice App\";


                    prtDoc.PrinterSettings.PrintToFile = true;
                    // confirm the user wants to use that name
                    System.Windows.Forms.SaveFileDialog pdfSaveDialog = new System.Windows.Forms.SaveFileDialog();
                    pdfSaveDialog.InitialDirectory = directory;
                    pdfSaveDialog.FileName = basename + ".pdf";
                    pdfSaveDialog.Filter = "PDF File|*.pdf";
                    userResp = pdfSaveDialog.ShowDialog();
                    if (userResp != System.Windows.Forms.DialogResult.Cancel)
                        prtDoc.PrinterSettings.PrintFileName = pdfSaveDialog.FileName;
                }

                if (userResp != System.Windows.Forms.DialogResult.Cancel)  // in case they canceled the save as dialog
                {
                    prtDoc.Print();
                }
            }
        }


        async Task PutTaskDelay(int millSecs)
        {
            await Task.Delay(millSecs);
        }



        //Highlights specific instances of different words
        public static void HighlightWords(TextPointer text, string searchWord, string stringText)
        {
            int instancesOfSearchKey = Regex.Matches(stringText.ToLower(), searchWord.ToLower()).Count;
            MessageBox.Show(instancesOfSearchKey.ToString());
            for (int i = 0; i < instancesOfSearchKey; i++)
            {
                int lastInstanceIndex = HighlightNextInstance(text, searchWord);
                if (lastInstanceIndex == -1)
                {
                    break;
                }
                text = text.GetPositionAtOffset(lastInstanceIndex);
            }
        }

        private static int HighlightNextInstance(TextPointer text, string searchWord)
        {
            int indexOfLastInstance = -1;

            while (true)
            {
                TextPointer next = text.GetNextContextPosition(LogicalDirection.Forward);
                if (next == null)
                {
                    break;
                }
                TextRange newText = new TextRange(text, next);

                int index = newText.Text.ToLower().IndexOf(searchWord.ToLower());
                if (index != -1)
                {
                    indexOfLastInstance = index;
                }

                if (index > 0)
                {
                    TextPointer start = text.GetPositionAtOffset(index);
                    TextPointer end = text.GetPositionAtOffset(index + searchWord.Length);
                    TextRange textRange = new TextRange(start, end);
                    textRange.ApplyPropertyValue(TextElement.BackgroundProperty, new SolidColorBrush(Colors.Gold));
                }
                text = next;
            }

            return indexOfLastInstance;
        }


        private void HighlightText(string search)
        {
            //Loop through Document Content
            for (TextPointer position = TestReport1.ContentStart; position != null && position.CompareTo(TestReport1.ContentEnd) <= 0; position = position.GetNextContextPosition(LogicalDirection.Forward))
            {
                if (position.CompareTo(TestReport1.ContentEnd) == 0)
                {
                    return;// newDocument;
                }

                String textRun = position.GetTextInRun(LogicalDirection.Forward);
                StringComparison stringComparison = StringComparison.CurrentCulture;
                Int32 indexInRun = textRun.IndexOf(search, stringComparison);

                //MessageBox.Show(indexInRun.ToString());

                if (indexInRun >= 0)
                {
                    //position = position.GetLineStartPosition(0);
                    position = position.GetPositionAtOffset(indexInRun);

                    if (position != null)
                    {

                        TextPointer nextPointer = position.GetPositionAtOffset(search.Length, LogicalDirection.Backward);
                        TextRange textRange = new TextRange(position, nextPointer);
                        //TextRange textRange = new TextRange(position, nextPointer);
                        //TextPointer nextPointer = position.GetLineStartPosition(0);


                        textRange.ApplyPropertyValue(TextElement.BackgroundProperty, new SolidColorBrush(Colors.Gold));
                    }
                }
            }
        }


        private async void PrintBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                //HighlightText("K9");


                //return;
                //Clear the message text box
                ReportMSGTB.Text = "";

                //Check that PO Number Was Entered
                if (string.IsNullOrEmpty(vm.POSONumber))
                {

                    //Display error message
                    vm.TestReportMSG = App.Current.FindResource("TestRepGBRequiredPOSONo").ToString();

                }
                else if (string.IsNullOrEmpty(vm.SerialNo))
                {
                    vm.TestReportMSG = App.Current.FindResource("TestRepGBEnterSerialNo").ToString();
                    //ReportMSGTB.Text = App.Current.FindResource("TestRepGBEnterSerialNo").ToString();
                }
                else
                {
                    //Reset any messages
                    vm.TestReportMSG = "";

                    //Refresh QR
                    GenerateLabel();

                    string currentDateTime = DateTime.Now.ToString().Replace(@"/", "-").Replace(":", "_");

                    //MessageBox.Show(vm.ReportBody);

                    DocumentPaginatorWrapper dpw = new DocumentPaginatorWrapper();

                    ////Set the serial number to text on the clipboard  Use control v
                    if (!string.IsNullOrEmpty(vm.SerialNo))
                        Clipboard.SetText(vm.SerialNo.Trim(), TextDataFormat.UnicodeText);

                  

                   
                    ////Get the text from the clipboard
                    //Clipboard.GetText(TextDataFormat.UnicodeText);

                    //dpw.SaveDocAndPrint(TestReport1);
                    dpw.PrintDoc(TestReport1);


                    //Get the directory for drives
                    string mydocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    //Get the root path location from current date
                    string rootpath = string.Empty;

                    // MessageBox.Show(vm.GetLocationSavePath(false) + vm.SerialNo.Trim() + " " + currentDateTime + ".pdf");

                    if (FaultyCB.IsChecked == true)
                    {
                        //check for directory
                        if (Directory.Exists(vm.GetLocationSavePath(vm.SaveToMyDocs) + @"\Faulty"))
                        {
                            //Save Message
                            SaveMessage.Foreground = System.Windows.Media.Brushes.Green;
                            //Create the PDF from the Flow Document and save to a fixed path.
                            dpw.FlowDocument2Pdf(TestReport1, vm.GetLocationSavePath(vm.SaveToMyDocs) + @"\Faulty\" + vm.SerialNo.Trim() + ".pdf");

                            //Save Message
                            SaveMessage.Text = "Network unavailable, File saved locally >>> " + vm.GetLocationSavePath(vm.SaveToMyDocs) + @"\Faulty\" + vm.SerialNo.Trim() + ".pdf";
                        }
                        else
                        {
                            //Save Message
                            SaveMessage.Foreground = System.Windows.Media.Brushes.Red;
                            //If not there create directory 
                            Directory.CreateDirectory(vm.GetLocationSavePath(vm.SaveToMyDocs) + @"\Faulty");

                            await PutTaskDelay(1000);

                            if (Directory.Exists(vm.GetLocationSavePath(vm.SaveToMyDocs) + @"\Faulty"))
                            {
                               
                                //Create the PDF from the Flow Document and save to a fixed path.
                                dpw.FlowDocument2Pdf(TestReport1, vm.GetLocationSavePath(vm.SaveToMyDocs) + @"\Faulty\" + vm.SerialNo.Trim() + ".pdf");

                                //Save Message
                                SaveMessage.Foreground = System.Windows.Media.Brushes.Red;
                                //Save Message
                                SaveMessage.Text = "Network unavailable, File saved locally >>> " + vm.GetLocationSavePath(true) + vm.SerialNo.Trim() + ".pdf";
                            }
                           
                        }

                       

                    }
                    else
                    {

                        //check for directory
                        if (Directory.Exists(vm.GetLocationSavePath(vm.SaveToMyDocs)))
                        {
                            //standard save
                            //Create the PDF from the Flow Document and save to a fixed path.
                            dpw.FlowDocument2Pdf(TestReport1, vm.GetLocationSavePath(vm.SaveToMyDocs) + vm.SerialNo.Trim() + ".pdf");

                            //Save Message
                            SaveMessage.Foreground = System.Windows.Media.Brushes.Green;
                            //Save Message
                            SaveMessage.Text = "File Saved >>> " + vm.GetLocationSavePath(vm.SaveToMyDocs) + vm.SerialNo.Trim() + ".pdf";
                        }
                        else
                        {
                           
                            //Divert to My Docs
                            //Create the PDF from the Flow Document and save to a fixed path.
                            dpw.FlowDocument2Pdf(TestReport1, vm.GetLocationSavePath(true) + vm.SerialNo.Trim() + ".pdf");

                            //Save Message
                            SaveMessage.Foreground = System.Windows.Media.Brushes.Red;
                            //Save Message
                            SaveMessage.Text = "Network unavailable, File saved locally >>> " + vm.GetLocationSavePath(true) + vm.SerialNo.Trim() + ".pdf";
                        }
                          

                    }


                    //MessageBox.Show("File Saved >>> " + vm.GetLocationSavePath(true) + vm.SerialNo.Trim() + " " + currentDateTime + ".pdf");


                    //ADD TO DATABASE
                    //Refresh Current User
                    //Refresh Current User
                    vm.ReportUser = "Technician Name: " + vm.CurrentUserInitials + " " + DateTime.Now.ToShortDateString(); ;
                    //ADD DB ENTRY HERE
                    //CreateTable(sqlite_conn);

                    //Check if DB File exists before inserting entry
                    if (File.Exists(@"" + StaticFunctions.UncPathToUse + @"Nebula\Repository\Data\TestDept_Reporting.db"))
                    {
                        //insert all items in table to DB
                        InsertData();
                    }

                    //Reset the password unlocks
                    vm.PasswordUnlockRun(null, "Disable");


                }




            }
            catch (Exception ex)
            {
                vm.DialogMessage("Save Message Exception", ex.Message);
               
                Console.WriteLine(ex.Message);
            }

            // PrintPDFTest();

            // // dpw.PrintDocs(System.Printing.PageMediaSizeName.ISOA4);

            // //Get the current Default Printer
            // string DefPrinter = DocumentPaginatorWrapper.GetDefaultPrinter();

            // //MessageBox.Show(DocumentPaginatorWrapper.GetDefaultPrinter());

            ////Set Default temp to the Microsoft PDF Virtual printer
            //DocumentPaginatorWrapper.SetDefaultPrinterQueue("Microsoft Print to PDF");

            //// print to PDF
            // dpw.PrintFlowDoc(TestReport1);

            // //delay between switching of default printers
            // await PutTaskDelay(2000);


            // //MessageBox.Show(DocumentPaginatorWrapper.GetDefaultPrinter());

            // //Set default printer back to Original
            // DocumentPaginatorWrapper.SetDefaultPrinterQueue(DefPrinter);

            // //print again, this time to a printer
            // dpw.PrintFlowDoc(TestReport1);

            // // PrintPDFTest();
        }



        //Create Connection to DB
        static SQLiteConnection CreateConnection(string pathtodb)
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            //sqlite_conn = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            //sqlite_conn = new SQLiteConnection("Data Source=" + pathtodb + "; Version = 3; New = True; Compress = True; ");
            sqlite_conn = new SQLiteConnection(pathtodb);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sqlite_conn;
        }



        public void InsertData()
        {

            try
            {

                //Create DB Connection object
                sqlite_conn = CreateConnection(cs);


                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                //sqlite_cmd.CommandText = "INSERT INTO SampleTable(Col1, Col2) VALUES('Test Text ', 1); ";
                sqlite_cmd.CommandText = "INSERT INTO ReportRecords (DateCreated, POSONumber, AssetNo, SerialNo, Manufacturer, ModelNo, DeviceCategory, ProcessPerformed, TestingPerformed, Notes, ReportBody, SavePath, TestedBy)" + " VALUES(@datecreated,@posonumber,@assetno,@serialno,@manufacturer,@modelno,@devicecategory,@processperformed,@testingperformed,@notes,@reportbody,@savepath,@testedby)";
                sqlite_cmd.CommandType = CommandType.Text;

                //Date must be in this format to work with SQL Lite
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@datecreated", DateTime.Now.ToString("yyyy-MM-dd H:mm:ss")));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@posonumber", vm.POSONumber.Replace(@"PO\SO:", "").Trim()));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@assetno", @" "));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@serialno", vm.SerialNo.Replace("Serial No:", "").Trim()));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@manufacturer", @" "));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@modelno", @" "));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@devicecategory", @" "));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@processperformed", @" "));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@testingperformed", @" "));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@notes", " "));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@reportbody", vm.ReportBody));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@savepath", " "));
                sqlite_cmd.Parameters.Add(new SQLiteParameter("@testedby", "Technician Name: " + vm.CurrentUserInitials + " " + DateTime.Now.ToShortDateString())); //Refresh Current User
                sqlite_cmd.ExecuteNonQuery();

                //Close Link
                sqlite_conn.Close();
            }
            catch (Exception ex)
            {
                vm.TestReportMSG = "Error saving to DB" + ex.Message;
            }

        }



        private void SaveBTN_Click(object sender, RoutedEventArgs e)
        {

        }



        private void CycleNext_Click(object sender, RoutedEventArgs e)
        {
            //Select through a collection of Serial and ReportBody
            //Assign the selected one to the single mapping option so the bind shows up on the report
            //Use element at to find item at a specific index
            //vm.ReportCollection.ElementAt();
            // MessageBox.Show(vm.ReportCollection.Count().ToString());
            //HardResetCB.IsChecked = false;
            //GreenLightCB.IsChecked = false;
            //FaultyCB.IsChecked = false;

         
            //Clear Serial Override
            //SerialOverrrideTXTB.Text = "";

            // MessageBox.Show(vm.ReportCollection.ElementAt(docindex));
            //Clear Save Message
            SaveMessage.Text = "";

            //Increase the docindex by 1
            //docindex = GetNextElementInt(vm.ReportCollection, docindex);

            GetNextElementInt(vm.ReportCollection);

            //Check Report for Faults
            FaultDetection(vm.ReportBody);

            if (!string.IsNullOrEmpty(vm.SerialNo))
            {
                //Create Serial QR Code
                GenerateLabel();
            }


            //set the report body to the new string at the new index
            //vm.ReportBody = vm.ReportCollection.ElementAt(docindex);


            //vm.ReportBody = vm.ReportBody + "\r Right Arrow Button Clicked";
        }

        private void CyclePrev_Click(object sender, RoutedEventArgs e)
        {
            //Select through a collection of Serial and ReportBody
            //Assign the selected one to the single mapping option so the bind shows up on the report
            //MessageBox.Show(vm.ReportCollection.Count().ToString());
            //Clear Serial Override
            //SerialOverrrideTXTB.Text = "";

            //vm.ReportCollection.FindIndex()
            // int i = vm.ReportCollection.FindIndex(x => x.StartsWith("762"));
            //MessageBox.Show(vm.ReportCollection.ElementAt(docindex));
            //Clear Save Message
            SaveMessage.Text = "";

            //Decrease the docindex by 1
            //docindex = GetPreviousElementInt(vm.ReportCollection, docindex);
            GetPreviousElementInt(vm.ReportCollection);

            //Check Report for Faults
            FaultDetection(vm.ReportBody);

            if (!string.IsNullOrEmpty(vm.SerialNo))
            {
                //Create Serial QR Code
                GenerateLabel();
            }

            //set the report body to the new string at the new index
            //vm.ReportBody = vm.ReportCollection.ElementAt(docindex);



            //vm.ReportBody = vm.ReportBody + "\r Left Arrow Button Clicked";
        }




        public void GenerateLabel()
        {
            if (vm != null)
            {

                try
                {

             

                //QRCODE Dimensions
                TESTStandardLabelZebraImage1.Width = 80;
                TESTStandardLabelZebraImage1.Height = 80;
                TESTStandardLabelZebraImage2.Width = 80;
                TESTStandardLabelZebraImage2.Height = 80;

                // Create QR CODE
                if (vm.SerialNo != null || vm.POSONumber != null)
                 {
                        if (vm.SerialNo != null)
                            vm.TESTQRCodeGI = CreateQRCode(vm.SerialNo.Replace("Serial No: ", "").Trim());

                        // Create QR CODE
                        if (vm.SerialNo != null && vm.POSONumber != null)
                            vm.TESTQRCodeGO = CreateQRCode(vm.POSONumber.Replace(@"PO\SO: ", "").Trim() + "-" + vm.SerialNo.Replace("Serial No: ", "").Trim());

                        //MessageBox.Show(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.SerialNo.Trim() + ".png");

                        //*** Checklist QR CODE ***
                        //Call Embed QR Saves & extracts QR Code to File. This is required for the cloning of the flowdocument as it requires a full path
                        if (vm.SerialNo != null)
                            vm.TESTQRCodeGI = vm.EmbedChecklistQR(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.SerialNo.Replace("Serial No: ", "").Trim() + ".png", vm.TESTQRCodeGI);
                        //GOODS OUT ORDER REF + 
                        //Call Embed QR Saves & extracts QR Code to File. This is required for the cloning of the flowdocument as it requires a full path
                        if (vm.SerialNo != null && vm.POSONumber != null)
                            vm.TESTQRCodeGO = vm.EmbedChecklistQR(vm.myDocs + @"\Nebula Logs\QR\QR_" + vm.POSONumber.Replace(@"PO\SO: ", "").Trim() + "_" + vm.SerialNo.Replace("Serial No: ", "").Trim() + ".png", vm.TESTQRCodeGO);
                    }
                    else
                    {
                        vm.DialogMessage("Generate Label", @"PO\SO or Serial No cannot be blank!!");
                    }
               
              

                }
                catch (Exception ex)
                {

                    vm.DialogMessage("Generate Label Exception", ex.Message);
                }

            }
        }


        public BitmapImage EmbedChecklistQR(string UriPath, BitmapImage QRProperty)
        {
            ////IMPORTANT AS QR THROWS EXCEPTION IF NOT A URI PATH
            ////Generate a file of the bitmap
            BitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(QRProperty));

            using (FileStream fileStream = new FileStream(UriPath, System.IO.FileMode.Create, FileAccess.Write, FileShare.Read))
            {

                encoder.Save(fileStream);
                fileStream.Flush();
                fileStream.Close();
            }


            // Create source.
            BitmapImage bi = new BitmapImage();
            // BitmapImage.UriSource must be in a BeginInit/EndInit block.
            bi.BeginInit();
            //*** Required not to lock file ***
            bi.CacheOption = BitmapCacheOption.OnLoad;
            bi.UriSource = new Uri(UriPath, UriKind.Absolute);
            bi.EndInit();

            //Return Bitmap Image with full absolute path
            return bi;
        }


        //Generate QRCODE Image
        public BitmapImage CreateQRCode(string stringData)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(stringData, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(40, System.Drawing.Color.Black, System.Drawing.Color.White, null, 1, 1, true);



            return ToBitmapImage(qrCodeImage);
        }

        //BITMAP TO BITMAP IMAGE
        public BitmapImage ToBitmapImage(Bitmap bitmap)
        {
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();

                return bitmapImage;
            }
        }

        ////BITMAP IMAGE TO BITMAP
        private Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            return new Bitmap(bitmapImage.StreamSource);
        }









        //FOR LIST
        //https://stackoverflow.com/questions/30258832/select-next-child-in-array-using-c-sharp


        // INT VERSION
        public int GetNextElementInt(IList<string> list)
        {
            if (docindex == list.Count - 1)
            {
                //MessageBox.Show("Next > list count 1 " + docindex.ToString());
                //docindex = docindex;
                //index = index;
                //pick from the collection the report body at that index
                //vm.ReportBody = vm.ReportCollection.ElementAt(docindex);
                //extract the serial number from the selected report body
                // vm.SerialNo = ExtractSerialNumber(vm.ReportBody);
            }
            else
            {

                // Set the checkboxes prior to index change. Captures user input
                var options = vm.TestOptionsSelectedCollection.ElementAt(docindex);
                options.HardReset = (bool)HardResetCB.IsChecked;
                options.GreenLight = (bool)GreenLightCB.IsChecked;
                options.Faulty = (bool)FaultyCB.IsChecked;

                //next report
                docindex++;

                // Set the checkboxes after index change
                options = vm.TestOptionsSelectedCollection.ElementAt(docindex);
                HardResetCB.IsChecked = options.HardReset;
                GreenLightCB.IsChecked = options.GreenLight;
                FaultyCB.IsChecked = options.Faulty;


                //increase current page int by one
                currentpage++;
                //Update the pagecount property
                vm.PagesCount = @"" + currentpage.ToString() + "/" + list.Count().ToString();
                //MessageBox.Show(vm.PagesCount);
                //pick from the collection the report body at that index
                vm.ReportBody = vm.ReportCollection.ElementAt(docindex);
                //extract the serial number from the selected report body
                vm.SerialNo = vm.ExtractSerialNumber(vm.ReportBody);
            }


            //return index;
            return docindex;
        }

        public int GetPreviousElementInt(IList<string> list)
        {
            if (docindex <= 0)
            {
                //MessageBox.Show("Prev index < zero " + docindex.ToString());
                docindex = 0;
                //pick from the collection the report body at that index
                //vm.ReportBody = vm.ReportCollection.ElementAt(docindex);
                //extract the serial number from the selected report body
                //vm.SerialNo = ExtractSerialNumber(vm.ReportBody);
            }
            else
            {
                // Set the checkboxes prior to index change. Captures user input
                var options = vm.TestOptionsSelectedCollection.ElementAt(docindex);
                options.HardReset = (bool)HardResetCB.IsChecked;
                options.GreenLight = (bool)GreenLightCB.IsChecked;
                options.Faulty = (bool)FaultyCB.IsChecked;


                //previous report
                docindex--;

                // Set the checkboxes
                options = vm.TestOptionsSelectedCollection.ElementAt(docindex);
                HardResetCB.IsChecked = options.HardReset;
                GreenLightCB.IsChecked = options.GreenLight;
                FaultyCB.IsChecked = options.Faulty;

          
                //decrease current page int by one
                currentpage--;
                //Update the pagecount property
                vm.PagesCount = currentpage.ToString() + "/" + list.Count().ToString();
                //MessageBox.Show(vm.PagesCount);
                //pick from the collection the report body at that index
                vm.ReportBody = vm.ReportCollection.ElementAt(docindex);
                //extract the serial number from the selected report body
                vm.SerialNo = vm.ExtractSerialNumber(vm.ReportBody);
            }


            //return index;
            return docindex;
        }



        //public string ExtractSerialNumber(string report)
        //{
        //    vm.ProductBrand = "cisco";
        //    switch (vm.ProductBrand)
        //    {
        //        case "cisco":


        //            //for Cisco Capture the Serial Number from the CLI 
        //            if (report.Contains("Processor board ID"))
        //            {
        //                var myint = report.IndexOf(" ID ", 0, StringComparison.OrdinalIgnoreCase);

        //                //MessageBox.Show(myint.ToString());

        //                string serialExtract = report.Substring(myint, 16).Replace(",", "").Replace("ID", "").Trim();
        //                myint = serialExtract.IndexOf(",", 0);
        //                vm.SerialNo = "Serial No: " + serialExtract;
        //                //MessageBox.Show(vm.SerialNo);

        //                // MessageBox.Show("Yup");
        //                //para.Background = Brushes.Yellow;
        //            }





        //            Console.WriteLine("Cisco");
        //            break;
        //        case "hp":
        //            Console.WriteLine("HP");
        //            break;
        //        case "juniper":
        //            Console.WriteLine("Juniper");
        //            break;
        //        case "dell":
        //            Console.WriteLine("Dell");
        //            break;

        //        case "ibm":
        //            Console.WriteLine("IBM");
        //            break;
        //    }


        //    return vm.SerialNo;
        //}


        public void FaultDetection(string report)
        {
            try
            {

                if (report != null)
                    if (report.Contains("is FAULTY"))
                    {
                        PrintBTN.IsEnabled = false;
                        vm.TestReportMSG = "Fault detected on the Test Report! Saving & Printing Disabled for this Unit, please check the report.";
                    }
                    else
                    {
                        PrintBTN.IsEnabled = true;
                        vm.TestReportMSG = "";
                    }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }


        // STRING VERSION
        public string GetNextElement(IList<string> list, int index)
        {
            if ((index > list.Count - 1) || (index < 0))
                throw new Exception("Invalid index");

            else if (index == list.Count - 1)
                index = 0;

            else
                index++;

            return list[index];
        }

        public string GetPreviousElement(IList<string> list, int index)
        {
            if ((index > list.Count - 1) || (index < 0))
                throw new Exception("Invalid index");

            else if (index == list.Count - 1)
                index = 0;

            else
                index--;

            return list[index];
        }




        //FOR ARRAY

        public string GetNextElement(string[] strArray, int index)
        {
            if ((index > strArray.Length - 1) || (index < 0))
                throw new Exception("Invalid index");

            else if (index == strArray.Length - 1)
                index = 0;

            else
                index++;

            return strArray[index];
        }

        public string GetPreviousElement(string[] strArray, int index)
        {
            if ((index > strArray.Length - 1) || (index < 0))
                throw new Exception("Invalid index");

            else if (index == strArray.Length - 1)
                index = 0;

            else
                index--;

            return strArray[index];
        }

        private void SerialOverrrideTXTB_TextChanged(object sender, TextChangedEventArgs e)
        {

            //Send Data To Serial Port
            vm.SerialNo = SerialOverrrideTXTB.Text.ToUpper();
            
            if (!string.IsNullOrEmpty(vm.SerialNo))
            {
                // Create QR CODE
                vm.TESTQRCodeGI = CreateQRCode(vm.SerialNo.Replace("Serial No: ", "").Trim());

            }

            if (!string.IsNullOrEmpty(vm.POSONumber) && !string.IsNullOrEmpty(vm.SerialNo))
            {
                // Create QR CODE
                vm.TESTQRCodeGO = CreateQRCode(vm.POSONumber.Replace(@"PO\SO: ", "").Trim() + "-" + vm.SerialNo.Replace("Serial No: ", "").Trim());
            }

        }

        private void SerialOverrrideTXTB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
              
                //Send Data To Serial Port
                vm.SerialNo = SerialOverrrideTXTB.Text.ToUpper();

                if (!string.IsNullOrEmpty(vm.SerialNo))
                {
                    //Refresh QR
                    GenerateLabel();
                }
            }
        }

        private void ChangeSerialBUT_Click(object sender, RoutedEventArgs e)
        {

            //Send Data To Serial Port
            vm.SerialNo = SerialOverrrideTXTB.Text.ToUpper();

            if (!string.IsNullOrEmpty(vm.SerialNo))
            {
                //Refresh QR
                GenerateLabel();
            }
        }

        private void ToggleSwitch_Checked(object sender, RoutedEventArgs e)
        {
            if (LogoHeadBRD.Visibility == Visibility.Collapsed)
            {
                //Display The Logo
                LogoHeadBRD.Visibility = Visibility.Visible;
                FooterTB.Visibility = Visibility.Visible;
            }

        }

        private void ToggleSwitch_Unchecked(object sender, RoutedEventArgs e)
        {
            if (LogoHeadBRD.Visibility == Visibility.Visible)
            {
                //Display The Logo
                LogoHeadBRD.Visibility = Visibility.Collapsed;
                FooterTB.Visibility = Visibility.Collapsed;
            }

        }

        private void POSOTXTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            vm.POSONumber = @"PO\SO: " + POSOTXTB.Text.ToUpper();
            //Set Order Ref Session Variable
            StaticFunctions.SessionOrderRefNumber = POSOTXTB.Text.ToUpper();

            if (!string.IsNullOrEmpty(vm.SerialNo))
            {
                // Create QR CODE
                vm.TESTQRCodeGI = CreateQRCode(vm.SerialNo.Replace("Serial No: ", "").Trim());
            }

            if (!string.IsNullOrEmpty(vm.POSONumber) && !string.IsNullOrEmpty(vm.SerialNo))
            {
                // Create QR CODE
                vm.TESTQRCodeGO = CreateQRCode(vm.POSONumber.Replace(@"PO\SO: ", "").Trim() + "-" + vm.SerialNo.Replace("Serial No: ", "").Trim());
            }
           

        }

        private void POSOTXTB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //MessageBox.Show("Boo");
                //Send Data To Serial Port

                if (!string.IsNullOrEmpty(POSOTXTB.Text))
                {
                    vm.POSONumber = @"PO\SO: " + POSOTXTB.Text.ToUpper();
                }
                   
            }
        }

        private void AddPONumberBUT_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(POSOTXTB.Text))
            {
                vm.POSONumber = @"PO\SO: " + POSOTXTB.Text.ToUpper();
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //On exit clear the temp QR Code images
            if (Directory.Exists(vm.myDocs + @"\Nebula Logs\QR"))
            {
                //Get list of all items in folder
                List<string> QRimages = Directory.GetFiles(vm.myDocs + @"\Nebula Logs\QR").ToList<string>();

                foreach (var qrimage in QRimages)
                {
                    if (File.Exists(qrimage))
                    {
                        //Delete
                        File.Delete(qrimage);
                    }


                }
            }
        }

        private void GreenLightCB_Checked(object sender, RoutedEventArgs e)
        {
            if (GreenLightCB.IsChecked == true)
            {
                //SHOW
                GreenLightTB.Visibility = Visibility.Visible;
            }


        }

        private void GreenLightCB_Unchecked(object sender, RoutedEventArgs e)
        {
            if (GreenLightCB.IsChecked == false)
            {
                //HIDE
                GreenLightTB.Visibility = Visibility.Collapsed;
            }
        }


        private void FaultyCB_Checked(object sender, RoutedEventArgs e)
        {
            if (FaultyCB.IsChecked == true)
            {
                //SHOW
                FaultyTB.Visibility = Visibility.Visible;
            }
        }

        private void FaultyCB_Unchecked(object sender, RoutedEventArgs e)
        {
            if (FaultyCB.IsChecked == false)
            {
                //SHOW
                FaultyTB.Visibility = Visibility.Collapsed;
            }
        }

        private void HardResetCB_Checked(object sender, RoutedEventArgs e)
        {
            if (HardResetCB.IsChecked == true)
            {
                //SHOW
                HardResetTB.Visibility = Visibility.Visible;
            }
        }

        private void HardResetCB_Unchecked(object sender, RoutedEventArgs e)
        {

            if (HardResetCB.IsChecked == false)
            {
                //SHOW
                HardResetTB.Visibility = Visibility.Collapsed;
            }
        }
    }

    //For use to keep track of Overidden Serials
    public class SerialOverides
    {
        public int Serialindex { get; set; }
        public string OverriddenSerial { get; set; }
    }
    




}
