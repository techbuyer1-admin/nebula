﻿using System;
using System.ComponentModel;
using System.IO.Ports;
using System.Security;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Nebula.Models;
using Nebula.Commands;
using Nebula.Commands.XTerminal;
using Microsoft.Win32;
using MahApps.Metro.Controls;
using System.IO;
using System.Collections.Generic;

namespace Nebula
{
    /// <summary>
    /// Interaction logic for XTerminalWindow.xaml
    /// </summary>
    public partial class XTerminalWindow : MetroWindow
    {
      
        public static RoutedCommand CustomRoutedF10Event = new RoutedCommand(); /**> Used to get F10 properly processed. Otherwise it is filtered by the WPF default usage (MainWindow Menu) */
        private const int MinimumWindowsVersion = 17763;
        private ConsoleWrapper console;
        private KeyHandler keyHandler;
        private Configuration configuration;
        SerialPort _serialPort1 = new SerialPort();

        /// <summary>
        /// Event handler to catch custom routed F10 event from MainWindow InputBindings and route it to OnKeyDown event handler we usually use
        /// </summary>
        /// <param name="sender">The Main Window</param>
        /// <param name="e">Nobody cares</param>
        private void ExecutedCustomF10Event(object sender, ExecutedRoutedEventArgs e)
        {
            /* F10 was pressed */
            this.keyHandler.OnKeyDown(sender, new KeyEventArgs(Keyboard.PrimaryDevice, Keyboard.PrimaryDevice.ActiveSource, 0, Key.F10) { RoutedEvent = Keyboard.KeyDownEvent });
        }

        /// <summary>
        /// CanExecuteCustomF10Event that only returns true if the source is a control.
        /// </summary>
        /// <param name="sender">Not used</param>
        /// <param name="e">e.Source -> MainWindow -> Which is a control</param>
        private void CanExecuteCustomF10Event(object sender, CanExecuteRoutedEventArgs e)
        {
            var target = e.Source as Control;

            if (target != null)
            {
                e.CanExecute = true;
            }
            else
            {
                e.CanExecute = false;
            }
        }

        private bool IsOSVersionSupported()
        {
            try
            {
                using (var key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", false))
                {
                    if (key?.GetValue("CurrentBuildNumber") is string version && int.TryParse(version, out int currentBuild))
                    {
                        return currentBuild >= MinimumWindowsVersion;
                    }
                }
            }
            catch (SecurityException) { }

            return false;
        }











        public XTerminalWindow()
        {
            InitializeComponent();
            this.Loaded += this.OnLoaded;

            string mytext = "[24;0HE[24;1H[24;18H[24;1H[2K[24;1H[?25h[24;1H[1;24r[24;1H[1;24r[24;1H[24;1H[2K[24;1H[?25h[24;1H[24;1HHP-2620-48-PoEP# [24;1H[24;18H[24;1H[?25h[24;18H";

            //string test = Regex.Replace(vm.ReportBody, @"\e\[\d *;?\d + m", "");
            //string test = Regex.Replace(vm.ReportBody, @"\e\[\d *;?\d + m", "");
            // string test = Regex.Replace(vm.ReportBody, @"\e\[(\d+;)*(\d+)?[ABCDHJKfmsu]", "");
            string source1 = "\x1b[2JThis text \x1b[3;20Hcontains several ANSI escapes\x1b[1;2;30;43m to format the text\x1b[K";
            string source = "[2JThis text [3;20Hcontains several ANSI escapes[1;2;30;43m to format the text[K";
            //string result = Regex.Replace(source, @"\e\[(\d+;)*(\d+)?[ABCDHJKfmsu]", "");
            //string result = Regex.Replace(mytext, @"\[(\d\d;\d\w\w)", ""); //(\[\d\w)(\[\d\d;\d\d\w)(\[\d\d;\d\w) (\d\d;\d\w)(\d\d;\d\d\w)
            //result = Regex.Replace(result, @"\[(\d\d;\d\w)", "");
            //result = Regex.Replace(result, @"\[(\d\d;\d\d\w)", "");
            //result = Regex.Replace(result, @"\[(\w\d\d\d\w)", "");
            //result = Regex.Replace(result, @"\[(\d;\d\d\w)", "");
            //result = Regex.Replace(result, @"\[(\d\w)", "");
            //result = Regex.Replace(result, @"\[(\W\d\d\w)", "");
            //MessageBox.Show(result);

            // screen
            this.screen.Buffer.Append(System.Text.Encoding.UTF8.GetBytes(mytext), mytext.Length);
            this.screen.Buffer.Append(System.Text.Encoding.UTF8.GetBytes(source), source.Length);
            this.screen.Buffer.Append(System.Text.Encoding.UTF8.GetBytes(source1), source1.Length);


            _serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Recieve1);

            //OPEN
            List<SerialPort> comports = new List<SerialPort>();
            comports.Add(_serialPort1);

            OpenSinglePort(comports);
        }

        // The main control for communicating through the RS-232 port
       

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var msg = System.Text.Encoding.UTF8.GetBytes(
                    "\x1b[24;0HE\x1b[24;1H\x1b[24;18H\x1b[24;1H\x1b[2K\x1b[24;1H\x1b[?25h\x1b[24;1H\x1b[1;24r\x1b[24;1H\x1b[1;24r\x1b[24;1H\x1b[24;1H\x1b[2K\x1b[24;1H\x1b[?25h\x1b[24;1H\x1b[24;1HHP-2620-48-PoEP# \x1b[24;1H\x1b[24;18H\x1b[24;1H\x1b[?25h\x1b[24;18H");






            //WRITE TOO

            if (_serialPort1.IsOpen)
            {
                _serialPort1.Write(CommandTXT.Text + "\r");
               // _serialPort1.Write("show system\r");

           

               // MessageBox.Show(data);

               
            }
           

            
            //READ
            //Read in Data from Serial
          
          
        }


        private void Recieve1(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {

            var data = _serialPort1.ReadExisting();
            this.screen.Buffer.Append(System.Text.Encoding.UTF8.GetBytes(data), data.Length);
        }



            public void OpenSinglePort(List<SerialPort> comlst)
        {

            bool error = false;

            // MessageBox.Show(portname + " " + baud1 + " " + parity1 + " " + databit1 + " " + stopbit1 + " " + handshake1);

            foreach (var cp in comlst)
            {
                //If the port is open, close it.
                if (cp.IsOpen)
                    cp.Close();
                else
                {
                    // Set the port's settings
                    cp.PortName = "COM4";
                    cp.BaudRate = 9600; //Convert.ToInt32(baud);
                    cp.Parity = (Parity)Enum.Parse(typeof(Parity), "None");
                    cp.DataBits = int.Parse("8");
                    cp.StopBits = (StopBits)Enum.Parse(typeof(StopBits), "1");
                    cp.Handshake = (Handshake)Enum.Parse(typeof(Handshake), "None");

                    //cp.PortName = portname;
                    //cp.BaudRate = Convert.ToInt32(baud1);
                    //cp.Parity = (Parity)Enum.Parse(typeof(Parity), parity1);
                    //cp.DataBits = Convert.ToInt32(databit1);
                    //cp.StopBits = (StopBits)Enum.Parse(typeof(StopBits), stopbit1);
                    //cp.Handshake = (Handshake)Enum.Parse(typeof(Handshake), handshake1);


                    try
                    {
                        // Open the port
                        cp.Open();
                    }
                    catch (UnauthorizedAccessException) { error = true; Console.WriteLine(error.ToString()); }
                    catch (IOException) { error = true; Console.WriteLine(error.ToString()); }
                    catch (ArgumentException) { error = true; Console.WriteLine(error.ToString()); }

                    //if (error) MessageBox.Show(this, "Could not open the COM port.  Most likely it is already in use, has been removed, or is unavailable.", "COM Port Unavalible", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //else
                    //{
                    //    // Show the initial pin states
                    //    UpdatePinState();
                    //    //chkDTR.Checked = comport.DtrEnable;
                    //    //chkRTS.Checked = comport.RtsEnable;
                    //}
                    // If the port is open, send focus to the send data box
                    if (cp.IsOpen)
                    {
                        //CommandTXT1.Focus();
                        ////if (chkClearOnOpen.Checked) ClearTerminal();
                        //ConsoleIncoming = "";
                        //ClearTerminal(ConsoleRTB1);
                    }
                }
            }


        }












        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (!this.IsOSVersionSupported())
            {
                var msg = System.Text.Encoding.UTF8.GetBytes(
                    "\x1b[2J\x1b[H" +
                    "ConPTY APIs required for this application are not available.\r\n" +
                    $"Please update to Windows 10 1809 (build {MinimumWindowsVersion}) or higher.\r\n" +
                    "Press any key to exit.\r\n");
                this.screen.Buffer.Append(msg, msg.Length);
                //this.KeyDown += (_, args) => this.Close();
                return;
            }

            this.InitializeWindowSizeHandling();
            this.HandleConfigurationChanged(null, null);

            this.console = TerminalManager.Instance.GetOrCreate(0, "wsl.exe");
            this.keyHandler = new KeyHandler(this.console);
            this.keyHandler.KeyboardShortcut += this.HandleKeyboardShortcut;

            this.screen.Buffer = this.console.Buffer;

            this.console.Buffer.PropertyChanged += (_, args) =>
            {
                if (args.PropertyName == "Title")
                {
                    //this.Dispatcher.InvokeAsync(() => this.Title = this.console.buffer.Title);
                }
            };

            this.KeyDown += this.keyHandler.OnKeyDown;
            this.KeyDown += (_, args) => this.screen.VerticalOffset = double.MaxValue; // force scroll on keypress.
            this.TextInput += this.keyHandler.OnTextInput;
            // XXX: a very hacky paste!
            this.MouseRightButtonDown += (_, args) =>
            {
                var text = Clipboard.GetData(DataFormats.Text) as string;
                if (!string.IsNullOrWhiteSpace(text))
                {
                    this.console.SendText(System.Text.Encoding.UTF8.GetBytes(text));
                }
            };

            this.console.PropertyChanged += this.HandleConsoleClosed;

            //this.Closing += this.HandleClosing;
        }

        private double windowFrameWidth, windowFrameHeight;
        private void InitializeWindowSizeHandling()
        {
            // XXX: maybe want to force a redraw of buffer too idk yet
            //this.DpiChanged += (sender, args) => VisualTreeHelper.SetRootDpi(this, args.NewDpi);

            this.windowFrameWidth = this.ActualWidth - this.grid.ActualWidth;
            this.windowFrameHeight = this.ActualHeight - this.grid.ActualHeight;

            this.MinWidth = this.ActualWidth;
            this.MinHeight = this.ActualHeight;
        }

        private void HandleClosing(object sender, CancelEventArgs e)
        {
            this.screen.Close();
            this.screen = null;

            this.console?.Dispose();
            this.console = null;
        }

        private void HandleKeyboardShortcut(object sender, KeyboardShortcutEventArgs args)
        {
            switch (args.Shortcut)
            {
                case KeyboardShortcut.OpenConfig:
                    this.configuration.ShellOpen();
                    break;
            }
        }

        private void HandleConfigurationChanged(object sender, EventArgs args)
        {
            var currentConfig = (Configuration)sender;

            this.configuration = Configuration.Load(currentConfig != null ? currentConfig.Filename : Configuration.GetDefaultFilename());
            this.configuration.Changed += this.HandleConfigurationChanged;
            this.screen.SetConfiguration(this.configuration);
            // XXX: this is hacky but keeps things from being default ugly. ideally want to snap to cells while resizing window.
            this.Dispatcher.BeginInvoke(new Action(() => this.grid.Background = new SolidColorBrush(new Color { R = this.configuration.Palette[0].R, G = this.configuration.Palette[0].G, B = this.configuration.Palette[0].B, A = 255 })));
            if (currentConfig != null)
            {
                currentConfig.Changed -= this.HandleConfigurationChanged;
                currentConfig.Dispose();
            }
        }

  

        private void HandleConsoleClosed(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == "Running" && this.console != null && this.console.Running == false)
            {
                if (this.configuration.ShowProcessExitOnClose)
                {
                    var msg = System.Text.Encoding.UTF8.GetBytes($"\r\n[process terminated with code {this.console.ProcessExitCode}, press <enter> to exit.]");
                    this.screen?.Buffer.Append(msg, msg.Length);

                    this.KeyDown += (keySender, keyArgs) =>
                    {
                        if (keyArgs.Key == System.Windows.Input.Key.Enter)
                        {
                            //this.Close();
                        }
                    };
                }
                else
                {
                    this.Dispatcher.Invoke(() => this.Close());
                }
            }
        }

    }
}
